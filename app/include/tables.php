<?php
 //SET UP CONSTANTS FOR TABLE NAMES
	DEFINE('ACCOUNTS', 'accounts');
	DEFINE('ACCOUNT_ADDRESSES', 'account_addresses');
	DEFINE('ACCOUNT_SETTINGS', 'account_settings');
	DEFINE('BEERS', 'beers');
	DEFINE('BREWERIES', 'breweries');
	DEFINE('BREWERY_ADDRESSES', 'brewery_addresses');
	DEFINE('DEVICE_NOTIFICATIONS', 'devicenotifications');
	DEFINE('DISTRIBUTORS', 'distributors');
	DEFINE('DIST_ADDRESSES', 'distributors_addresses');
	DEFINE('DIST_BEERS', 'distributors_beers');
	DEFINE('BREW_DIST', 'brew_dist');
	DEFINE('ENTERPRISE', 'enterprisecodes');
	DEFINE('HUBS', 'hubdevices');
	DEFINE('KEGDATA', 'kegdata');
	DEFINE('KEGDATARECEIVED', 'kegdatareceived');
	DEFINE('KEGDEVICES', 'kegdevices');
	DEFINE('KEGPRODUCTS', 'kegproducts');
	DEFINE('KEGTYPES', 'kegtypes');
	DEFINE('MAILKEYS', 'maillistkeys');
	DEFINE('NOTIFICATION_FREQ','notification_frequency');
	DEFINE('NOTIFICATION_HIST', 'notification_history');
	DEFINE('SITE_CONTENT', 'site_content');
	DEFINE('TIMEZONE', 'timezones');
	DEFINE('USER_LOGIN_HISTORY', 'user_login_history');
	DEFINE('USER_VERIFICATION', 'user_verification');
	DEFINE('USERS', 'users');
?>