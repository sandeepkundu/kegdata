<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection as Collection;
use App\KegData\Infrastructure\KegDataProcessor as Processor;
use App\KegData\Infrastructure\ProcessorLogger as ProcessorLogger;
use App\Events\DataWasProcessed as DataWasProcessed;
use Event;
use App;

class ProcessKegData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $processor = new Processor();
        $howMany = $processor->countUnprocessedData();
        $i = 0;
        $processed = new Collection();

        while($i < $howMany){
            $unprocessed = $processor->getUnprocessedData(20, 0);
            $pTwenty = new Collection();

            foreach($unprocessed as $u){
                $status = $processor->processData($u);
                if($status['status'] != 'no status' && !empty($status['data']) ){
                    $processed->push($status['data']);
                    $pTwenty->push($status);
                }
            } /* close foreach($unprocessed as $u) */

            if(App::environment('local')){
                var_dump($i." Lines Processed.... Out of ".$howMany);
            } /* close if(App::environment) */

            $processorlog = new ProcessorLogger();
            $processorlog->write('Data Processed '.$i.' out of '.$howMany.': '.$pTwenty);
            $i += 20;
        } /* Close while($i < $howMany) */

        Event::fire(new DataWasProcessed($processed));
    } /* Close public function handle() */

} /*Close class ProcessKegData */
