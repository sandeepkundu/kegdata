<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Stripe;
use App\KegData\Models\StripeEvent as StripeEvent;
use App\KegData\Infrastructure\StripeWebhooks as Webhooks;
use App\KegData\Infrastructure\Handlers\StripeWebhookHandlers as StripeHandler;
use Log;

class ProcessStripeEvent extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $eventID;
    private $stripe;
    private $eventType;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $eventID)
    {
        $this->eventID = $eventID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $event = Stripe::events()->find($this->eventID);
        }catch(\Stripe\Exception\NotFoundException $e){
            Log::warning('Tried to process stripe event '.$this->eventID.' but Stripe had no record of this event');
            $event = NULL;
        }

        if(empty($event) && $event['id'] != $this->eventID){
            Log::warning('Tried to process stripe event '.$this->eventID.' but Stripe had no record of this event');
        }else{
            $this->eventID = $event['id'];

            $exists = StripeEvent::where('stripe_event_id', $this->eventID)->first();

            /**
             * Stripe event didn't exist in the table, so lets create it
             */
            if(empty($exists)){

                $exists = StripeEvent::firstOrCreate(['stripe_event_id' => $this->eventID]);

            }

            $this->eventType = $event['type'];

            //@todo get the handle methods to work...
            //$toHandle = new Webhooks($event['type']);

            //var_dump($toHandle->getEvent()->first());
            // if($toHandle->getEvent()->first()->handle){
                try{
                    new StripeHandler($event);
                }catch(\Exception $e){
                    Log::error($e);
                }
            // }else{
            //     Log::warning($toHandle->getEvent()->first());
            // }
        }


    }
}
