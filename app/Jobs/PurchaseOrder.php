<?php namespace App\Jobs;

use App\Jobs\Command;
use Stripe;
use Carbon\Carbon;
use Cart;
use App\KegData\Models\Plan as Plan;
use App\KegData\Models\Product as Product;
use App\KegData\Models\Order as Order;
use App\KegData\Models\OrderItem as OrderItem;
use App\KegData\Models\OrderAddress as OrderAddress;
use App\KegData\Models\Subscription as Subscription;
use App\KegData\Infrastructure\StoreLogger as  StoreLogger;


class PurchaseOrder extends Command {

	// Allows it to work Queue
	// use InteractsWithQueue, SerializeModels;

	public $productCart, $planCart,$user,$account,$shippingAddress, $billingAddress, $stripeToken, $shipping, $tax, $total, $newOrder, $orderItemsArray, $newPlan, $customer, $newShippingAddress, $newBillingAddress, $newSub, $charge;
	protected $storeLogger;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($productCart, $planCart, $user, $account, $name, $address1, $address2, $city, $stateCode, $zipcode, $countryCode, $billing_name, $billing_address1, $billing_address2, $billing_city, $billing_stateCode, $billing_zipcode, $billing_countryCode, $stripeToken)
	{

		//These should be provided when the command is called
		$this->productCart = $productCart;
		$this->planCart = $planCart;
		$this->user = $user;
		$this->account = $account;
		$this->stripeToken = $stripeToken;
		$this->shippingAddress['name'] = $name;
		$this->shippingAddress['address1'] = $address1;
		$this->shippingAddress['address2'] = $address2;
		$this->shippingAddress['city'] = $city;
		$this->shippingAddress['stateCode'] = $stateCode;
		$this->shippingAddress['zipcode'] = $zipcode;
		$this->shippingAddress['countryCode'] = $countryCode;
		$this->billingAddress['name'] = $billing_name;
		$this->billingAddress['address1'] = $billing_address1;
		$this->billingAddress['address2'] = $billing_address2;
		$this->billingAddress['city'] = $billing_city;
		$this->billingAddress['stateCode'] = $billing_stateCode;
		$this->billingAddress['zipcode'] = $billing_zipcode;
		$this->billingAddress['countryCode'] = $billing_countryCode;
		$this->orderItemsArray = array();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(StoreLogger $storeLogger)
	{
		$this->storeLogger = $storeLogger;
		//Process the product cart - Calculate Subtotal, shipping,  tax, total
		$this->tax = ($this->billingAddress['stateCode'] == 'TX') ? ($this->productCart->total() * .0825) : 0;
       		$this->shipping = ($this->productCart->total() < 199) ? ($this->productCart->total() * .2) : ($this->productCart->total() * .1);
       		$this->total = number_format(($this->productCart->total() + $this->tax + $this->shipping), 2);
		//Create the order
       		$this->newOrder = $this->createOrder();
       		//Create order address
       		if($this->newOrder){
       			$this->newShippingAddress = $this->createOrderAddress($this->shippingAddress, 'S');
	   		$this->newBillingAddress = $this->createOrderAddress($this->billingAddress, 'B');
			//Create order items
	       		$this->newOrderItems = $this->createOrderItems();
			//Decrease Product Stock
	       		foreach($this->newOrderItems as $item){
	       			$this->decreaseStock($item->id);
	       		}
			//Create subscription
	       		if(!is_null($this->planCart->content()->first()->id)){
	       			$this->newSub = $this->createSubscription();
	       		}
			//Create/update stripe customer
	       		$this->customer = $this->stripeCustomer();
			//Create stripe charge
	       		if(is_null($this->customer->id)){

	       			$error = $this->customer;

	       			$error['order_id'] = $this->newOrder->id;
	       		}
	       		else{
	       			$this->charge = $this->stripeCharge();
	       		}

	       		if(is_null($this->charge->id)){
	       			$error = $this->charge;
	       			$error['order_id'] = $this->newOrder->id;
	       		}
       		}else{
       			$error['type'] = 'order';
       			$error['code'] = 'ORDER_ERROR';
       			$error['message'] = 'An error occurred while attempting to create the order. All information for this order has been discarded.';
       			$error['order_id'] = 0;
       		}

       		if(is_null($error)){
       			Event::fire(new OrderWasPlaced($this->user, $this->newOrder->id));
       		}
       		else{
       			Event::fire(new OrderWasPlaced($this->user, $error));
       		}

	}

	/**
	* @method Creates a new order and attaches it to the account
	* @var Cart $products, Account $account, String $stateCode
	*
	* @return Order $order
	*/
	public function createOrder(){
		$order = Order::create([
			'subtotal' => $this->productCart->total(),
			'shipping' => $this->shipping,
			'tax' => $this->tax,
			'total' => $this->total,
			'status' => 'new'
		]);

		$order->account()->associate($this->account);
		$order->user()->associate($this->user);
		$order->save();

		return $order;
	}

	/**
	* @method Order Addresses
	* @var Array $address
	*
	* @return Collection $orderAddress
	*/

	public function createOrderAddress($address, $type){
		$orderAddress = OrderAddress::create([]);
		$orderAddress->type = $type;
		$orderAddress->name = $address['name'];
		$orderAddress->address1 = is_null($address['address1']) ? '' : $address['address1'];
		$orderAddress->address2 = is_null($address['address2']) ? '' : $address['address2'];
		$orderAddress->city = $address['city'];
		$orderAddress->state = $address['stateCode'];
		$orderAddress->zipcode = $address['zipcode'];
		$orderAddress->country = $address['countryCode'];
		$orderAddress->account()->associate($this->account);
		$orderAddress->order()->associate($this->newOrder);
		$orderAddress->save();
		return $orderAddress;
	}

	/**
	* @method Create Order Items
	* @return collection $items
	*/

	public function createOrderItems(){
		foreach($this->productCart->content() as $item){
			$product = Product::find($item->id);

			$newItem = OrderItem::create([
				'price' => $item->price,
				'quantity' => $item->qty
			]);

			$newItem->product()->associate($product);
			$newItem->order()->associate($this->newOrder);
			$newItem->save();

			array_push($this->orderItemsArray, array(
				'product_id' => $item->id,
				'product_name' => $item->name,
				'price' => $item->price,
				'quantity' => $item->qty
			));
		}

		return(OrderItem::where('order_id', '=', $this->newOrder->id)->get());
	}

	/**
	*@method Create Subscription
	*@return Collection $subscription
	*/
	public function createSubscription(){
		$this->newPlan = Plan::find($this->planCart->content()->first()->id);

		$newSub = Subscription::create([
			'active' => 1, 'start' => Carbon::now()->toDateTimeString() ]);

		$newSub->plan()->associate($this->newPlan);
		$newSub->account()->associate($this->account);
		$newSub->order()->associate($this->newOrder);
		$newSub->save();

		return $newSub;
	}

	/**
	* @method Deactivate subscription
	* Deactivates old subscriptions in the subscription table (keeps things clean)
	* Must be called before createSubscription
	* @return Boolean
	*/
	public function deactivateSubscription(){
		return Subscription::where('account_id', '=', $this->account->id)->update(['active' => 0]);
	}

	/**
	* @method Decrease Product Stock
	* @var Product $id
	* @return Boolean
	*/
	public function decreaseStock($id){
		if(Product::where('id', '=', $id)->decrement('stock')){
			return true;
		}else{
			return false;
		}
	}

	/**
	* @method Create Stripe Charge
	* @return Stripe $charge
	*/
	public function stripeCharge(){
		try{
		$charge = Stripe::charges()->create(array(
			'amount' => $this->total,
			'currency' => 'USD',
			'customer' => $this->customer['id'],
			'metadata' => ['order_id' => $this->newOrder->id],
			'statement_descriptor' => 'iKeg, LLC Purchase',
			'receipt_email' => env('APP_ENV') == 'local' ? 'kevin@kegdata.com' : $user->email,
			'shipping' => [
				'name' => $this->newShippingAddress->name,
				'address' => [
				'city' => $this->newShippingAddress->city,
				'country' => $this->newShippingAddress->countryCode,
				'line1' => $this->newShippingAddress->address1,
				'line2' => $this->newShippingAddress->address2,
				'postal_code' => $this->newShippingAddress->zipcode,
				'state' => $this->newShippingAddress->stateCode
			]]
		));

		$this->newOrder->charge_id = $charge['id'];
		$this->newOrder->status = 'charged';
		$this->newOrder->save();

		return $charge;
		  } catch(\Stripe\Error\Card $e) {
			 //Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - PAYMENT FAILED';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - API ERROR';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - SYSTEM ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData SYSTEM Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - AUTH ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData AUTH Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
		  	// Network communication with Stripe failed
		  	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - STRIPE ERROR';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email

			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - STRIPE ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData STRIPE Error for Review');
			});

			//Return the Error
			return $err;
		} catch (Exception $e) {
		 	// Something else happened, completely unrelated to Stripe

		 	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - SYSTEM ERROR';
			$this->newOrder->save();
			//Return the Error
			return $err;
		}
		//End Catch Statements for Stripe Error
	}

	/**
	* @method Create/Find Stripe Customer
	* subscribe/update customer plan
	* Update the account with the stripe details
	*
	* @return Stripe $customer
	*/
	public function stripeCustomer(){
		try {
			//Check if this customer has already been created with Stripe
			if(is_null($this->account->stripe_customer_id)){
				$customer = Stripe::customers()->create(array(
					'source' => $stripeToken,
					'email' => $this->user->email,
					'description' => $this->account->account_name,
					'plan' => $this->newPlan->slug,
					'quantity' => 1,
					'tax_percent' => ($this->billingAddress['stateCode'] == 'TX') ? 8.25 : 0
				));

			}else{

				if(!is_null($this->newSub)){
					//Deactivate the old subscriptions in the subscriptions table
					$this->deactivateSubscription();
					/**
					* @todo If subscriptions ever have other intervals, proration should be used on monthly to yearly, yearly to month updates
					*/
					Stripe::subscriptions()->update($this->account->stripe_customer_id, $this->account->stripe_subscription_id,[
						'prorate' => false,
						'source' => $this->stripeToken,
						'plan' => $this->newPlan->slug
					]);
				}

				$customer = Stripe::customers()->update($account->stripe_customer_id, [
	    				'email' => $user->email,
	    				'source' => $stripeToken
				]);
			}

			$account->stripe_active = 1;
			$account->stripe_id = $stripeToken;
			$account->stripe_customer_id = $customer['id'];
			$account->stripe_subscription = $customer['subscriptions']['data'][0]['id'];
			$account->stripe_plan = $customer['subscriptions']['data'][0]['plan']['id'];
			$account->last_four = $customer['sources']['data'][0]['last4'];
			$account->trial_ends_at = $customer['subscriptions']['data'][0]['trial_end'];

			$account->save();

			return $customer;
			 // Use Stripe's library to make requests...
		  } catch(\Stripe\Error\Card $e) {
			 //Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - PAYMENT FAILED';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - API ERROR';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - SYSTEM ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData SYSTEM Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - AUTH ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData AUTH Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
		  	// Network communication with Stripe failed
		  	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - STRIPE ERROR';
			$this->newOrder->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email

			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - STRIPE ERROR';
			$this->newOrder->save();

			$emailAddress = $this->auth->user()->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData STRIPE Error for Review');
			});

			//Return the Error
			return $err;
		} catch (Exception $e) {
		 	// Something else happened, completely unrelated to Stripe

		 	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->kdlogger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$this->newOrder->status = 'PENDING - SYSTEM ERROR';
			$this->newOrder->save();
			//Return the Error
			return $err;
		}
		//End Catch Statements for Stripe Error
	}

}
