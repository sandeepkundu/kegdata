<?php
$action = isset($_GET['action']) ? $_GET['action'] : 'no-action';


switch($action){
	//Function to begin registering breweries
	case 'registerB':
		if(isset($_POST['zipCode']) && isset($_POST['regB'])){
			
			$data = array('Zip Code' => $_POST['zipCode']);
			$rules = array('Zip Code' => 'required|zipcode');

			$valid = $validate->validate($rules,$data);

			if($valid){
				$formZip = $_POST['zipCode'];

				$table = 'brew_dist';
				$data = array('id', 'name', 'primaryContactID', 'email1','email2',
					'phone','fax','address1','address2', 'city', 'stateCode', 'zipcode',
					'countryCode', 'type'
				);
				$where = array('zipcode' => $formZip);

				$results = $db->select($table,$data,$where);
				$error = $db->error();
				$lastquery = $db->last_query();

			}
		}
	break;
	case 'selectedB':
		if(isset($_POST['brewRadio'])){
			$radio = explode('|',$_POST['brewRadio']);
			$table = 'brew_dist';
			$data = array('name', 'address1', 'address2', 'city',
				'stateCode', 'zipcode', 'phone', 'fax');
			$where = array('AND' => array('id' => $radio[1], 'type' => $radio[0]));

			$brewery = $db->get($table,$data,$where);
			$dError = $db->error();
			$dQuery = $db->last_query();

			$table = 'Accounts';
			$data = array('id', 'modified');
			$where = array('accountName' => $gb->bake($honeyandthebee,$brewery['name']));

			$account = $db->get($table, $data, $where);
			$aError = $db->error();
			$aQuery = $db->last_query();

			$formfill['Company Name'] = strtoupper($brewery['name']);
			$formfill['Address 1'] = (empty($brewery['address1']))? ''  : $gb->eat($brewery['address1'], $honeyandthebee);
			$formfill['Address 2'] = (empty($brewery['address2']))? $brewery['address2'] : $gb->eat($brewery['address2'], $honeyandthebee);
			$formfill['City'] = $brewery['city'];
			$formfill['State'] = $brewery['stateCode'];
			$formfill['Zip Code'] = $brewery['zipcode'];
			$formfill['County'] = '';
			$formfill['Country'] = 'US';
			$formfill['Office Phone'] =(empty($brewery['phone']))? '' : $gb->eat( $brewery['phone'], $honeyandthebee);
			$formfill['Fax'] = '';
			$formfill['Currency'] = 'USD';
			$formfill['Enterprise Code'] = '';
			$formfill['First Name'] = '';
			$formfill['Last Name'] = '';
			$formfill['Home Phone'] = '';
			$formfill['Cell Phone'] = '';
			$formfill['Email Address'] = '';
			$formfill['Retype Email Address'] = '';
			$formfill['Password'] = '';
			$formfill['Retype Password'] = '';
			$formfill['mailListKey'] = '';
		}else{
			$formfill['Company Name'] = '';
			$formfill['Address 1'] = '';
			$formfill['Address 2'] = '';
			$formfill['City'] = '';
			$formfill['State'] = '';
			$formfill['Zip Code'] = '';
			$formfill['County'] = '';
			$formfill['Country'] = 'US';
			$formfill['Office Phone'] = '';
			$formfill['Fax'] = '';
			$formfill['Currency'] = 'USD';
			$formfill['Enterprise Code'] = '';
			$formfill['First Name'] = '';
			$formfill['Last Name'] = '';
			$formfill['Home Phone'] = '';
			$formfill['Cell Phone'] = '';
			$formfill['Email Address'] = '';
			$formfill['Retype Email Address'] = '';
			$formfill['Password'] = '';
			$formfill['Retype Password'] = '';
			$formfill['mailListKey'] = '';
		}

		$formZip = $_POST['zipCode'];
	break;
	case 'submitRegB':
	/*BREWERY DATA ARRAY*/
		$data['Company Name'] = isset($_POST['companyName']) ? $_POST['companyName'] : '';
		$data['Address 1'] = isset($_POST['address1']) ? $_POST['address1'] : '';
		$data['Address 2'] = isset($_POST['address2']) ? $_POST['address2'] : '';
		$data['City'] = isset($_POST['city']) ? $_POST['city'] : '';
		$data['State'] = isset($_POST['state']) ? $_POST['state'] : '';
		$data['Zip Code'] = isset($_POST['zipCode']) ? $_POST['zipCode'] : '';
		$data['County'] = isset($_POST['county']) ? $_POST['county'] : '';
		$data['Country'] = isset($_POST['country']) ? $_POST['country'] : 'US';
		$data['Office Phone'] = isset($_POST['officePhone']) ? $_POST['officePhone'] : '';
		$data['Fax'] = isset($_POST['fax']) ? $_POST['fax'] : '';
		$data['Currency'] = isset($_POST['currency']) ? $_POST['currency'] : 'USD';
		$data['Enterprise Code'] = isset($_POST['enterpriseCode']) ? $_POST['enterpriseCode'] : '';
		$data['First Name'] = isset($_POST['firstName']) ? $_POST['firstName'] : '';
		$data['Last Name'] = isset($_POST['lastName']) ? $_POST['lastName'] : '';
		$data['Home Phone'] = isset($_POST['homePhone']) ? $_POST['homePhone'] : '';
		$data['Cell Phone'] = isset($_POST['cellPhone']) ? $_POST['cellPhone'] : '';
		$data['Email Address'] = isset($_POST['emailAddress']) ? $_POST['emailAddress'] : '';
		$data['Retype Email Address'] = isset($_POST['emailAddress2']) ? $_POST['emailAddress2'] : '';
		$data['Password'] = isset($_POST['password']) ? md5($_POST['password']) : '';
		$data['Retype Password'] = isset($_POST['password2']) ? md5($_POST['password2']) : '';
		$data['mailListKey'] = isset($_POST['mailListKey']) ? $_POST['mailListKey'] : '';
	/*BREWERY RULES ARRAY*/
		$rules['Company Name'] = 'required|isExtText';
		$rules['Address 1'] = 'required|isExtText';
		$rules['Address 2'] = 'isExtText';
		$rules['City'] = 'required|isExtText';
		$rules['State'] = 'required|isState';
		$rules['Zip Code'] = 'required|zipcode';
		$rules['County'] = 'isExtText';
		$rules['Country'] = 'isState';
		$rules['Office Phone'] = 'required|phone';
		$rules['Fax'] = 'phone';
		$rules['Currency'] = 'isCurr';
		$rules['Enterprise Code'] = 'isECode';
		$rules['First Name'] = 'required|isExtText';
		$rules['Last Name'] = 'required|isExtText';
		$rules['Home Phone'] = 'phone';
		$rules['Cell Phone'] = 'phone';
		$rules['Email Address'] = 'required|email';
		$rules['Retype Email Address'] = 'required|email';
		$rules['Password'] = 'required|isHash';
		$rules['Retype Password'] = 'required|isHash';
		$rules['mailListKey'] = 'isMailKey';
	/*CHECK DATA*/
		$validEmail = ($data["Email Address"] == $data["Retype Email Address"])? true : false;

		$validPass = ($data['Password'] == $data['Retype Password'])? true : false;
	/*REGISTER BREWERY*/
		if($validEmail && $validPass){
			$validData = $validate->validate($rules,$data);
			if($validData){
				$mailListKey = $data['mailListKey'];
				$success = $user->newAccount($data,$rules, 'B');
				if($success > 0 && $success != false){
					//$reciepient, $subject, $htmlTemplate, $textTemplate, $data
					$columns = array('verificationHash');
					$where = array('id' => $success);

					$verify = $db->get(USER_VERIFICATION,$columns,$where);
					if(isset($verify['verificationHash'])){
						$verifyLink = 'http://www.kegdata.com/verify?e='.$data['Email Address'].'&v='.$verify['verificationHash'];

						$emailData = array($data['First Name'], $data['Last Name'], $data['Email Address'],$verifyLink);
						$mailer->sendMail($data['Email Address'], 'Thank you for joining KegData', 'new-brewery.html', 'new-user.txt', $emailData);

						header("Location:".WEB_ROOT."/thankyou");
						exit();
					}else{
						$err->setError('Error 1001: Account has been created but email could not be sent. You can login and resend the email.');
					}
				}else{
					$formfill = $data;

					$formZip = $data['Zip Code'];
					$err->setError('Please correct the form issues to continue.');
					$invalidFields = $validate->getInvalidFields();
				}

			}else{
				$err->setError('Please correct the form issues to continue.');
				$invalidFields = $validate->getInvalidFields();
			}
		}else{
			//Friendly message and form errors for invalid email addresses
			if(!$validEmail){
				$err->setError('Email Addresses do not match');
				$invalidFields = array('Email Address', 'Retype Email Address');
			}
			//Friendly message and form errors for invalid passwords
			if(!$validPassword){
				if(!empty($invalidFields)){
					array_push($invalidFields, 'Password', 'Retype Password');
				} else{
					$invalidFields = array('Password', 'Retype Password');
				}
			}
		}//close if($validEmail && $validPass)
		//End case 'submitRegD'
	break;
	//Function to begin registering distributors
	case 'registerD':
		if(isset($_POST['zipCode']) && isset($_POST['regD'])){
			
			$data = array('Zip Code' => $_POST['zipCode']);
			$rules = array('Zip Code' => 'required|zipcode');

			$valid = $validate->validate($rules,$data);
			if($valid){
				$formZip = $_POST['zipCode'];
				$table = 'brew_dist';
				$data = array('id', 'name', 'primaryContactID', 'email1','email2',
					'phone','fax','address1','address2', 'city', 'stateCode', 'zipcode',
					'countryCode', 'type'
				);
				$where = array('zipcode' => $formZip);

				$results = $db->select($table,$data,$where);
				$error = $db->error();
				$lastquery = $db->last_query();
			}
		}
	break;
	case 'selectedD':
		if(isset($_POST['distRadio'])){
			$radio = explode('|',$_POST['distRadio']);
			$table = 'brew_dist';
			$data = array('name', 'address1', 'address2', 'city',
				'stateCode', 'zipcode', 'phone', 'fax');
			$where = array('AND' => array('id' => $radio[1], 'type' => $radio[0]));

			$distributor = $db->get($table,$data,$where);

			$dError = $db->error();
			$dQuery = $db->last_query();

			$table = 'Accounts';
			$data = array('id', 'modified');
			$where = array('accountName' => $gb->bake($honeyandthebee,$distributor['name']));

			$account = $db->get($table, $data, $where);
			$aError = $db->error();
			$aQuery = $db->last_query();

			$formfill['Company Name'] = strtoupper($distributor['name']);
			$formfill['Address 1'] = (empty($distributor['address1']))? '' : $gb->eat($distributor['address1'], $honeyandthebee);
			$formfill['Address 2'] = (empty($distributor['address2']))? '' : $gb->eat($distributor['address2'], $honeyandthebee);
			$formfill['City'] = $distributor['city'];
			$formfill['State'] = $distributor['stateCode'];
			$formfill['Zip Code'] = $distributor['zipcode'];
			$formfill['County'] = '';
			$formfill['Country'] = 'US';
			$formfill['Office Phone'] =(empty($distributor['phone']))? '' : $gb->eat( $distributor['phone'], $honeyandthebee);
			$formfill['Fax'] = '';
			$formfill['Currency'] = 'USD';
			$formfill['Enterprise Code'] = '';
			$formfill['First Name'] = '';
			$formfill['Last Name'] = '';
			$formfill['Home Phone'] = '';
			$formfill['Cell Phone'] = '';
			$formfill['Email Address'] = '';
			$formfill['Retype Email Address'] = '';
			$formfill['Password'] = '';
			$formfill['Retype Password'] = '';
			$formfill['mailListKey'] = '';
		}else{
			$formfill['Company Name'] = '';
			$formfill['Address 1'] = '';
			$formfill['Address 2'] = '';
			$formfill['City'] = '';
			$formfill['State'] = '';
			$formfill['Zip Code'] = '';
			$formfill['County'] = '';
			$formfill['Country'] = 'US';
			$formfill['Office Phone'] = '';
			$formfill['Fax'] = '';
			$formfill['Currency'] = 'USD';
			$formfill['Enterprise Code'] = '';
			$formfill['First Name'] = '';
			$formfill['Last Name'] = '';
			$formfill['Home Phone'] = '';
			$formfill['Cell Phone'] = '';
			$formfill['Email Address'] = '';
			$formfill['Retype Email Address'] = '';
			$formfill['Password'] = '';
			$formfill['Retype Password'] = '';
			$formfill['mailListKey'] = '';
		}

		$formZip = $_POST['zipCode'];
	break;
	case 'submitRegD':
	/*DISTRIBUTOR DATA ARRAY*/
		$data['Company Name'] = isset($_POST['companyName']) ? $_POST['companyName'] : '';
		$data['Address 1'] = isset($_POST['address1']) ? $_POST['address1'] : '';
		$data['Address 2'] = isset($_POST['address2']) ? $_POST['address2'] : '';
		$data['City'] = isset($_POST['city']) ? $_POST['city'] : '';
		$data['State'] = isset($_POST['state']) ? $_POST['state'] : '';
		$data['Zip Code'] = isset($_POST['zipCode']) ? $_POST['zipCode'] : '';
		$data['County'] = isset($_POST['county']) ? $_POST['county'] : '';
		$data['Country'] = isset($_POST['country']) ? $_POST['country'] : 'US';
		$data['Office Phone'] = isset($_POST['officePhone']) ? $_POST['officePhone'] : '';
		$data['Fax'] = isset($_POST['fax']) ? $_POST['fax'] : '';
		$data['Currency'] = isset($_POST['currency']) ? $_POST['currency'] : 'USD';
		$data['Enterprise Code'] = isset($_POST['enterpriseCode']) ? $_POST['enterpriseCode'] : '';
		$data['First Name'] = isset($_POST['firstName']) ? $_POST['firstName'] : '';
		$data['Last Name'] = isset($_POST['lastName']) ? $_POST['lastName'] : '';
		$data['Home Phone'] = isset($_POST['homePhone']) ? $_POST['homePhone'] : '';
		$data['Cell Phone'] = isset($_POST['cellPhone']) ? $_POST['cellPhone'] : '';
		$data['Email Address'] = isset($_POST['emailAddress']) ? $_POST['emailAddress'] : '';
		$data['Retype Email Address'] = isset($_POST['emailAddress2']) ? $_POST['emailAddress2'] : '';
		$data['Password'] = isset($_POST['password']) ? md5($_POST['password']) : '';
		$data['Retype Password'] = isset($_POST['password2']) ? md5($_POST['password2']) : '';
		$data['mailListKey'] = isset($_POST['mailListKey']) ? $_POST['mailListKey'] : '';
	/*DISTRIBUTOR RULES ARRAY*/
		$rules['Company Name'] = 'required|isExtText';
		$rules['Address 1'] = 'required|isExtText';
		$rules['Address 2'] = 'isExtText';
		$rules['City'] = 'required|isExtText';
		$rules['State'] = 'required|isState';
		$rules['Zip Code'] = 'required|zipcode';
		$rules['County'] = 'isExtText';
		$rules['Country'] = 'isState';
		$rules['Office Phone'] = 'required|phone';
		$rules['Fax'] = 'phone';
		$rules['Currency'] = 'isCurr';
		$rules['Enterprise Code'] = 'isECode';
		$rules['First Name'] = 'required|isExtText';
		$rules['Last Name'] = 'required|isExtText';
		$rules['Home Phone'] = 'phone';
		$rules['Cell Phone'] = 'phone';
		$rules['Email Address'] = 'required|email';
		$rules['Retype Email Address'] = 'required|email';
		$rules['Password'] = 'required|isHash';
		$rules['Retype Password'] = 'required|isHash';
		$rules['mailListKey'] = 'isMailKey';
	/*CHECK IF VALID*/
		$validEmail = ($data["Email Address"] == $data["Retype Email Address"])? true : false;

		$validPass = ($data['Password'] == $data['Retype Password'])? true : false;
	/*NEW USER IF VALID*/
		if($validEmail && $validPass){
			$validData = $validate->validate($rules,$data);
			if($validData){
				$mailListKey = $data['mailListKey'];
				$success = $user->newAccount($data,$rules, 'D');
				if($success > 0 && $success != false){

					$columns = array('verificationHash');
					$where = array('id' => $success);

					$verify = $db->get(USER_VERIFICATION,$columns,$where);
					if(isset($verify['verificationHash'])){
						$verifyLink = 'http://www.kegdata.com/verify?e='.$data['Email Address'].'&v='.$verify['verificationHash'];
						$emailData = array($data['First Name'], $data['Last Name'], $data['Email Address'],$verifyLink);
						$mailer->sendMail($data['Email Address'], 'Thank you for joining KegData', 'new-distributor.html', 'new-user.txt', $emailData);
						header("Location:".WEB_ROOT."/thankyou");
						exit();
					}else{
						$err->setError('Error 1001: Account has been created but email could not be sent. You can login and resend the email.');
					}
				}else{
					$formfill = $data;

					$formZip = $data['Zip Code'];
					$err->setError('Please correct the form issues to continue.');
					$invalidFields = $validate->getInvalidFields();
				}

			}else{
				$err->setError('Please correct the form issues to continue.');
				$invalidFields = $validate->getInvalidFields();
			}
		}else{
			//Friendly message and form errors for invalid email addresses
			if(!$validEmail){
				$err->setError('Email Addresses do not match');
				$invalidFields = array('Email Address', 'Retype Email Address');
			}
			//Friendly message and form errors for invalid passwords
			if(!$validPassword){
				if(!empty($invalidFields)){
					array_push($invalidFields, 'Password', 'Retype Password');
				} else{
					$invalidFields = array('Password', 'Retype Password');
				}
			}
		}//close if($validEmail && $validPass)
		//End case 'submitRegD'
	break;
	//Function to begin registering restaurant/bar users
	case 'submitRegE':
	/*ENTERPRISE DATA*/
		$data['Company Name'] = isset($_POST['companyName']) ? $_POST['companyName'] : '';
		$data['Address 1'] = isset($_POST['address1']) ? $_POST['address1'] : '';
		$data['Address 2'] = isset($_POST['address2']) ? $_POST['address2'] : '';
		$data['City'] = isset($_POST['city']) ? $_POST['city'] : '';
		$data['State'] = isset($_POST['state']) ? $_POST['state'] : '';
		$data['Zip Code'] = isset($_POST['zipCode']) ? $_POST['zipCode'] : '';
		$data['County'] = isset($_POST['county']) ? $_POST['county'] : '';
		$data['Country'] = isset($_POST['country']) ? $_POST['country'] : 'US';
		$data['Office Phone'] = isset($_POST['officePhone']) ? $_POST['officePhone'] : '';
		$data['Fax'] = isset($_POST['fax']) ? $_POST['fax'] : '';
		$data['Currency'] = isset($_POST['currency']) ? $_POST['currency'] : 'USD';
		$data['Enterprise Code'] = isset($_POST['enterpriseCode']) ? $_POST['enterpriseCode'] : '';
		$data['First Name'] = isset($_POST['firstName']) ? $_POST['firstName'] : '';
		$data['Last Name'] = isset($_POST['lastName']) ? $_POST['lastName'] : '';
		$data['Home Phone'] = isset($_POST['homePhone']) ? $_POST['homePhone'] : '';
		$data['Cell Phone'] = isset($_POST['cellPhone']) ? $_POST['cellPhone'] : '';
		$data['Email Address'] = isset($_POST['emailAddress']) ? $_POST['emailAddress'] : '';
		$data['Retype Email Address'] = isset($_POST['emailAddress2']) ? $_POST['emailAddress2'] : '';
		$data['Password'] = isset($_POST['password']) ? md5($_POST['password']) : '';
		$data['Retype Password'] = isset($_POST['password2']) ? md5($_POST['password2']) : '';
		$data['mailListKey'] = '';
	/*ENTERPRISE RULES*/
		$rules['Company Name'] = 'required|isExtText';
		$rules['Address 1'] = 'required|isExtText';
		$rules['Address 2'] = 'isExtText';
		$rules['City'] = 'required|isExtText';
		$rules['State'] = 'required|isState';
		$rules['Zip Code'] = 'required|zipcode';
		$rules['County'] = 'isExtText';
		$rules['Country'] = 'isState';
		$rules['Office Phone'] = 'required|phone';
		$rules['Fax'] = 'phone';
		$rules['Currency'] = 'isCurr';
		$rules['Enterprise Code'] = 'isECode';
		$rules['First Name'] = 'required|isExtText';
		$rules['Last Name'] = 'required|isExtText';
		$rules['Home Phone'] = 'phone';
		$rules['Cell Phone'] = 'phone';
		$rules['Email Address'] = 'required|email';
		$rules['Retype Email Address'] = 'required|email';
		$rules['Password'] = 'required|isHash';
		$rules['Retype Password'] = 'required|isHash';
		$rules['mailListKey'] = 'isMailKey';
	/*IS ENTERPRISE VALID*/
		$validEmail = ($data["Email Address"] == $data["Retype Email Address"])? true : false;

		$validPass = ($data['Password'] == $data['Retype Password'])? true : false;
	/*NEW USER*/
		if($validEmail && $validPass){
			$validData = $validate->validate($rules,$data);
			if($validData){
				$mailListKey = $data['mailListKey'];
				$success = $user->newAccount($data,$rules, 'E');
				if($success > 0 && $success != false){

					$columns = array('verificationHash');
					$where = array('id' => $success);

					$verify = $db->get(USER_VERIFICATION,$columns,$where);
					if(isset($verify['verificationHash'])){
						$verifyLink = 'http://www.kegdata.com/verify?e='.$data['Email Address'].'&v='.$verify['verificationHash'];
						$emailData = array($data['First Name'], $data['Last Name'], $data['Email Address'],$verifyLink);
						$mailer->sendMail($data['Email Address'], 'Thank you for joining KegData', 'new-enterprise.html', 'new-user.txt', $emailData);
						header("Location:".WEB_ROOT."/thankyou");
					}else{
						$err->setError('Error 1001: Account has been created but email could not be sent. You can login and resend the email.');
					}
				}else{
					$formfill = $data;

					$formZip = $data['Zip Code'];
					$err->setError('Please correct the form issues to continue.');
					$invalidFields = $validate->getInvalidFields();
				}

			}else{
				$err->setError('Please correct the form issues to continue.');
				$invalidFields = $validate->getInvalidFields();
			}
		}else{
			//Friendly message and form errors for invalid email addresses
			if(!$validEmail){
				$err->setError('Email Addresses do not match');
				$invalidFields = array('Email Address', 'Retype Email Address');
			}
			//Friendly message and form errors for invalid passwords
			if(!$validPassword){
				if(!empty($invalidFields)){
					array_push($invalidFields, 'Password', 'Retype Password');
				} else{
					$invalidFields = array('Password', 'Retype Password');
				}
			}
		}//close if($validEmail && $validPass)
		//End case 'submitRegD'
	break;
	//Function to begin registering restaurant/bar users
	case 'submitRegR':
	/*RESTAURANT DATA*/
		$data['Company Name'] = isset($_POST['companyName']) ? $_POST['companyName'] : '';
		$data['Address 1'] = isset($_POST['address1']) ? $_POST['address1'] : '';
		$data['Address 2'] = isset($_POST['address2']) ? $_POST['address2'] : '';
		$data['City'] = isset($_POST['city']) ? $_POST['city'] : '';
		$data['State'] = isset($_POST['state']) ? $_POST['state'] : '';
		$data['Zip Code'] = isset($_POST['zipCode']) ? $_POST['zipCode'] : '';
		$data['County'] = isset($_POST['county']) ? $_POST['county'] : '';
		$data['Country'] = isset($_POST['country']) ? $_POST['country'] : 'US';
		$data['Office Phone'] = isset($_POST['officePhone']) ? $_POST['officePhone'] : '';
		$data['Fax'] = isset($_POST['fax']) ? $_POST['fax'] : '';
		$data['Currency'] = isset($_POST['currency']) ? $_POST['currency'] : 'USD';
		$data['Enterprise Code'] = isset($_POST['enterpriseCode']) ? $_POST['enterpriseCode'] : '';
		$data['First Name'] = isset($_POST['firstName']) ? $_POST['firstName'] : '';
		$data['Last Name'] = isset($_POST['lastName']) ? $_POST['lastName'] : '';
		$data['Home Phone'] = isset($_POST['homePhone']) ? $_POST['homePhone'] : '';
		$data['Cell Phone'] = isset($_POST['cellPhone']) ? $_POST['cellPhone'] : '';
		$data['Email Address'] = isset($_POST['emailAddress']) ? $_POST['emailAddress'] : '';
		$data['Retype Email Address'] = isset($_POST['emailAddress2']) ? $_POST['emailAddress2'] : '';
		$data['Password'] = isset($_POST['password']) ? md5($_POST['password']) : '';
		$data['Retype Password'] = isset($_POST['password2']) ? md5($_POST['password2']) : '';
		$data['mailListKey'] = '';
	/*RESTAURANT RULES*/
		$rules['Company Name'] = 'required|isExtText';
		$rules['Address 1'] = 'required|isExtText';
		$rules['Address 2'] = 'isExtText';
		$rules['City'] = 'required|isExtText';
		$rules['State'] = 'required|isState';
		$rules['Zip Code'] = 'required|zipcode';
		$rules['County'] = 'isExtText';
		$rules['Country'] = 'isState';
		$rules['Office Phone'] = 'required|phone';
		$rules['Fax'] = 'phone';
		$rules['Currency'] = 'isCurr';
		$rules['Enterprise Code'] = 'isECode';
		$rules['First Name'] = 'required|isExtText';
		$rules['Last Name'] = 'required|isExtText';
		$rules['Home Phone'] = 'phone';
		$rules['Cell Phone'] = 'phone';
		$rules['Email Address'] = 'required|email';
		$rules['Retype Email Address'] = 'required|email';
		$rules['Password'] = 'required|isHash';
		$rules['Retype Password'] = 'required|isHash';
		$rules['mailListKey'] = 'isMailKey';
	/*CHECK IF DATA IS VALID*/
		$validEmail = ($data["Email Address"] == $data["Retype Email Address"])? true : false;

		$validPass = ($data['Password'] == $data['Retype Password'])? true : false;
	/*IF VALID INSERT NEW USER*/
		if($validEmail && $validPass){
			$validData = $validate->validate($rules,$data);
			if($validData){
				$mailListKey = $data['mailListKey'];
				$success = $user->newAccount($data,$rules, 'R');
				if($success > 0 && $success != false){
	
					$columns = array('verificationHash');
					$where = array('id' => $succcess);

					$verify = $db->get(USER_VERIFICATION,$columns,$where);
					if(isset($verify['verificationHash'])){
						$verifyLink = 'http://www.kegdata.com/verify?e='.$data['Email Address'].'&v='.$verify['verificationHash'];
						$emailData = array($data['First Name'], $data['Last Name'], $data['Email Address'],$verifyLink);
						$mailer->sendMail($data['Email Address'], 'Thank you for joining KegData', 'new-bar.html', 'new-user.txt', $emailData);
						header("Location:".WEB_ROOT."/thankyou");
						exit();
					}else{
						$err->setError('Error 1001: Account has been created but email could not be sent. You can login and resend the email.');
					}
				}else{
					$formfill = $data;

					$formZip = $data['Zip Code'];
					$err->setError('Please correct the form issues to continue.');
					$invalidFields = $validate->getInvalidFields();
				}

			}else{
				$err->setError('Please correct the form issues to continue.');
				$invalidFields = $validate->getInvalidFields();
			}
		}else{
			//Friendly message and form errors for invalid email addresses
			if(!$validEmail){
				$err->setError('Email Addresses do not match');
				$invalidFields = array('Email Address', 'Retype Email Address');
			}
			//Friendly message and form errors for invalid passwords
			if(!$validPassword){
				if(!empty($invalidFields)){
					array_push($invalidFields, 'Password', 'Retype Password');
				} else{
					$invalidFields = array('Password', 'Retype Password');
				}
			}
		}//close if($validEmail && $validPass)
		//End case 'submitRegD'
	break;
	//Fucntion to begin registering Residential Users
	case 'submitRegS':
	/*RESIDENTIAL DATA*/
		$data['Company Name'] = isset($_POST['companyName']) ? $_POST['companyName'] : '';
		$data['Address 1'] = isset($_POST['address1']) ? $_POST['address1'] : '';
		$data['Address 2'] = isset($_POST['address2']) ? $_POST['address2'] : '';
		$data['City'] = isset($_POST['city']) ? $_POST['city'] : '';
		$data['State'] = isset($_POST['state']) ? $_POST['state'] : '';
		$data['Zip Code'] = isset($_POST['zipCode']) ? $_POST['zipCode'] : '';
		$data['County'] = isset($_POST['county']) ? $_POST['county'] : '';
		$data['Country'] = isset($_POST['country']) ? $_POST['country'] : 'US';
		$data['Office Phone'] = isset($_POST['officePhone']) ? $_POST['officePhone'] : '';
		$data['Fax'] = isset($_POST['fax']) ? $_POST['fax'] : '';
		$data['Currency'] = isset($_POST['currency']) ? $_POST['currency'] : 'USD';
		$data['Enterprise Code'] = '';
		$data['First Name'] = isset($_POST['firstName']) ? $_POST['firstName'] : '';
		$data['Last Name'] = isset($_POST['lastName']) ? $_POST['lastName'] : '';
		$data['Home Phone'] = isset($_POST['homePhone']) ? $_POST['homePhone'] : '';
		$data['Cell Phone'] = isset($_POST['cellPhone']) ? $_POST['cellPhone'] : '';
		$data['Email Address'] = isset($_POST['emailAddress']) ? $_POST['emailAddress'] : '';
		$data['Retype Email Address'] = isset($_POST['emailAddress2']) ? $_POST['emailAddress2'] : '';
		$data['Password'] = isset($_POST['password']) ? md5($_POST['password']) : '';
		$data['Retype Password'] = isset($_POST['password2']) ? md5($_POST['password2']) : '';
		$data['mailListKey'] = '';
	/*RESIDENTIAL RULES*/
		$rules['Company Name'] = 'required|isExtText';
		$rules['Address 1'] = 'required|isExtText';
		$rules['Address 2'] = 'isExtText';
		$rules['City'] = 'required|isExtText';
		$rules['State'] = 'required|isState';
		$rules['Zip Code'] = 'required|zipcode';
		$rules['County'] = 'isExtText';
		$rules['Country'] = 'isState';
		$rules['Office Phone'] = 'required|phone';
		$rules['Fax'] = 'phone';
		$rules['Currency'] = 'isCurr';
		$rules['Enterprise Code'] = 'isECode';
		$rules['First Name'] = 'required|isExtText';
		$rules['Last Name'] = 'required|isExtText';
		$rules['Home Phone'] = 'phone';
		$rules['Cell Phone'] = 'phone';
		$rules['Email Address'] = 'required|email';
		$rules['Retype Email Address'] = 'required|email';
		$rules['Password'] = 'required|isHash';
		$rules['Retype Password'] = 'required|isHash';
		$rules['mailListKey'] = 'isMailKey';
	/*VERIFY DATA*/
		$validEmail = ($data["Email Address"] == $data["Retype Email Address"])? true : false;

		$validPass = ($data['Password'] == $data['Retype Password'])? true : false;
	/*CREATE A NEW USER*/
		if($validEmail && $validPass){
			$validData = $validate->validate($rules,$data);
			if($validData){
				$mailListKey = $data['mailListKey'];
				$success = $user->newAccount($data,$rules, 'S');
				if($success > 0 && $success != false){

					$columns = array('verificationHash');
					$where = array('id' => $success);

					$verify = $db->get(USER_VERIFICATION,$columns,$where);
					if(isset($verify['verificationHash'])){
						$verifyLink = 'http://www.kegdata.com/verify?e='.$data['Email Address'].'&v='.$verify['verificationHash'];
						$emailData = array($data['First Name'], $data['Last Name'], $data['Email Address'],$verifyLink);
						$verifymail = $mailer->sendMail($data['Email Address'], 'Thank you for joining KegData', 'new-user.html', 'new-user.txt', $emailData);
						header("Location:".WEB_ROOT."/thankyou");
						exit();
					}else{
						$err->setError('Error 1001: Account has been created but email could not be sent. You can login and resend the email.');
					}
				}else{
					$formfill = $data;

					$formZip = $data['Zip Code'];
					$err->setError('Please correct the form issues to continue.');
					$invalidFields = $validate->getInvalidFields();
				}

			}else{
				$err->setError('Please correct the form issues to continue.');
				$invalidFields = $validate->getInvalidFields();
			}
		}else{
			//Friendly message and form errors for invalid email addresses
			if(!$validEmail){
				$err->setError('Email Addresses do not match');
				$invalidFields = array('Email Address', 'Retype Email Address');
			}
			//Friendly message and form errors for invalid passwords
			if(!$validPassword){
				if(!empty($invalidFields)){
					array_push($invalidFields, 'Password', 'Retype Password');
				} else{
					$invalidFields = array('Password', 'Retype Password');
				}
			}
		}//close if($validEmail && $validPass)
		//End case 'submitRegD'
	break;
}//close switch($action)