<?php
$action = isset($_GET['action']) ? $_GET['action'] : 'no-action';
switch($action){
	//action to add Hub to account
	case 'addHub':
			if(isset($_POST['acctNum']) && isset($_POST['hubMac'])){
				$data = array('acctNum' => $_POST['acctNum'],
					'mac' => $_POST['hubMac']
				);

				$rules = array('acctNum' => "required|isNumber",
					'mac' => 'required|isMac'
				);
				$valid = $validate->validate($rules,$data);

				if($valid){
					require_once(CLASS_PATH.'admin.php');
					$admin = new systemAdmin();
					$success = $admin->addHub($data,$rules);
					if($success){
						header('location: emp-admin');
						exit();
					}
				}else{
					$err->setError('Data entered is not valid');
				}

			}else{
				$err->setError('Selecting an account and entering a mac is required.');
			}
	break;
	//action to change user password
	case 'changepw':
		break;
	case 'deleteAccount':
		require_once(CLASS_PATH.'admin.php');
		$admin = new systemAdmin();
		$id = isset($_GET['id']) ? $_GET['id'] : -1;
		if($id == -1){
			echo false;
		}else{

			$rules = array('id' => 'isNumber');
			$data = array('id' => $id);

			$valid = $validate->validate($rules,$data);
			if($valid){
				if($admin->deleteAccount($id)){
					echo true;
					exit();
				}else{
					echo false;
					exit();
				}
			}else{
				echo false;
				exit();
			}

		}
	break;
	//action to delete a notification
	case 'deleteNotification':
		$id = isset($_GET['notification']) ? $_GET['notification'] : -1;
		if($id == -1){
			echo '{"success":false,"id":'.$id.'}';
		}else{
			$json = $kegs->deleteNotification($id);
			echo $json;
			exit();
		}
	break;
	//action to add a new notification or edit an existing one
	case 'editAlarms':
		if(empty($_POST['alarmKeg']) || empty($_POST['notification'])){
			$err->setError('Error 2040: Keg and Notification type are required to set a notification.');
		}else{
			$data = array('Keg' => $_POST['alarmKeg'],
					'Notification' => $_POST['notification']
			);

			$rules = array('Keg' => 'isNumber|required',
						   'Notification' => 'isNotification|required'
			);

			$success = true;
			switch($data['Notification']){
				case "L":
				case "E":
				case "T":
				if(empty($_POST['range']) || empty($_POST['value']) || empty($_POST['frequency'])){
					$err->setError('Error 2041: Range, Value and Frequency are required for this type of notifiction.');
					$success = false;
				}else{
					$data['Range'] = $_POST['range'];
					$data['Value'] = $_POST['value'];
					$data['Frequency'] = $_POST['frequency'];

					$rules['Range'] = 'isRange|required';
					$rules['Value'] = 'isNumber|required';
					$rules['Frequency'] = 'isNumber|required';
				}
				break;
				default:
					$data['Range'] = '<';
					$data['Value'] = 0;
					$data['Frequency'] = 0;

					$rules['Range'] = 'isRange|required';
					$rules['Value'] = 'isNumber';
					$rules['Frequency'] = 'isNumber';
				break;
			}

			if($success){
				if($_POST['notificationID'] == 0){
					$success = $validate->validate($rules,$data);
					if($success){
						if(!$kegs->newNotification($data,$rules)){
							$err->setError('Error 2044: Could create notification.');
						}
					}else{
						$err->setError('Error 2042: You have form errors that need to be corrected.');
					}
					
				}else{
					$data['notificationID'] = $_POST['notificationID'];
					$rules['notificationID'] = 'isNumber|required';
					$success = $validate->validate($rules,$data);
					if($success){
						if(!$kegs->updateNotification($data,$rules)){
							$err->setError('Error 2043: Could not update notification.');
						}else{
							header('location: keg-dashboard?tab=notifications');
							exit();
						}
					}else{
						$err->setError('Error 2042: You have form errors that need to be corrected.');
					}
				}
			}
 		}
	break;
	//action to update keg device
	case 'editDevice':
		if(isset($_POST['chooseKeg'])){
			$data['device'] = $_POST['chooseKeg'];
			$data['kegType'] = isset($_POST['kegType']) ? $_POST['kegType'] : null;
			$data['prodNum'] = isset($_POST['contentsID']) ? $_POST['contentsID'] : null;
			$data['kegName'] = isset($_POST['kegName']) ? $_POST['kegName'] : null;
			$data['showDevice'] = isset($_POST['showDevice']) ? $_POST['showDevice'] : 1;
			$data['distbrew'] = isset($_POST['selectedDistID']) ? $_POST['selectedDistID'] : 0;
			$data['distType'] = (isset($_POST['distType']) && $_POST['distType'] != "0") ? $_POST['distType'] : null;
			$rules = array('device' => 'required|isNumber', 'showDevice' => 'isBool');
			if(isset($data['kegType'])) $rules['kegType'] = 'isNumber';
			if(isset($data['prodNum'])) $rules['prodNum'] = 'isNumber';
			if(isset($data['kegName'])) $rules['kegName'] = 'isExtText';
			if(isset($data['distbrew'])) $rules['distbrew'] = 'isNumber';
			if(isset($data['distType'])) $rules['distType'] = 'isAccountType';

			$isValid = $validate->validate($rules,$data);
			$updated = false;

			if($isValid){
				$updated = $kegs->updateKeg($data,$rules);
			}

			if($updated){
				header('location: keg-dashboard?tab=keg-setup');
				exit();
			}else{
				$err->setError('Error 2001: Could not update keg');
			}
		}else{
			$err->setError('Error 2002: Could not update keg');
		}
	break;
	//get Hubs for an Account
	case 'getHubs':
			require_once(CLASS_PATH.'admin.php');
			$admin = new systemAdmin();
			$acct = isset($_GET['acctNum']) ? $_GET['acctNum'] : -1;
			if($acct == -1){
				echo "{Error: No Account Selected}";
				exit();
			}else{
				$jsonHubs = $admin->getHubs($acct);
					echo $jsonHubs;
				exit();
			}
	break;
	//get Account for Admin Functions
	case 'getAdminAccount':
		require_once(CLASS_PATH.'admin.php');
			$admin = new systemAdmin();
			$acct = isset($_GET['id']) ? $_GET['id'] : -1;
			if($acct == -1){
				echo "{Error: No Account Selected}";
				exit();
			}else{
				$account = $admin->getAccountData($acct);
					echo $account;
				exit();
			}
	break;
	//action to update user form
	case 'updateuser':
			if(isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['email1'])){
				$data = array(
					'firstName' => $_POST['firstName'],
					'lastName' => $_POST['lastName'],
					'email1' => $_POST['email1'],
				);

				$data['email2'] = (isset($_POST['email2'])) ? $_POST['email2'] : null;
				$data['phone1'] = (isset($_POST['phone1'])) ? $_POST['phone1'] : null;
				$data['phone2'] = (isset($_POST['phone2'])) ? $_POST['phone2'] : null;

				$rules = array(
					'firstName' => 'required|name',
					'lastName' => 'required|name',
					'email1' => 'required|email'
				);

				if(isset($data['email2'])) $rules['email2'] = 'email';
				if(isset($data['phone1'])) $rules['phone1'] = 'phone';
				if(isset($data['phone2'])) $rules['phone2'] = 'phone';

				$isValid = $validate->validate($rules,$data);
				$updated = false;
				if($isValid){
					$updated = $user->updateUser($data, $rules);
				}

				if($updated){
					header('location: my-account?tab=user-info');
					exit();
				}else{
					$err->setError('Could not update user');
				}

 			}else{
 					$err->setError('Could not update user');
 			}
		break;
	//START JSON ACTIONS
	//json Contents of Keg
	case 'jsonKegContents':
	//getKegContents($prodNum = 0, $limit = -1, $isJson = 0, $search = '')
		$search = isset($_GET['search']) ? $_GET['search'] : '';
		$limit = isset($_GET['limit']) ? $_GET['limit'] : -1;
		$jsonKegs = $kegs->getKegContents(0, $limit, 1, $search);
		echo $jsonKegs;
		exit();
		break;
	//json individual keg
	case 'retrieveKeg':
		$keg = isset($_GET['kegNum']) ? $_GET['kegNum'] : -1;
		if($keg == -1){
			echo "{Error: No Keg Selected}";
			exit();
		}else{
			$jsonKeg = $kegs->getKegs($keg,1);
			echo $jsonKeg;
			exit();
		}
	break;
	//update account settings
	case 'updateAccountSettings':
		if(isset($_POST['currencies']) && isset($_POST['amEnd']) && isset($_POST['dayEnd']) && isset($_POST['amStart']) && isset($_POST['dayStart']) && isset($_POST['weekStart']) && isset($_POST['tz'])){
			$data = array('Currency' => $_POST['currencies'],
				'Day Start' => $_POST['dayStart'],
				'AM/PM Start' => $_POST['amStart'],
				'Day End' => $_POST['dayEnd'],
				'AM/PM End' => $_POST['amEnd'],
				'Week Start' => $_POST['weekStart'],
				'Timezone' => $_POST['tz']
			);

			$rules = array('Currency' => 'required|isCurr',
				'Day Start' => 'required|time',
				'AM/PM Start' => 'required|isAMPM',
				'Day End' => 'required|time',
				'AM/PM End' => 'required|isAMPM',
				'Week Start' => 'required|weekday',
				'Timezone' => 'required|isNumber'
			);

			if(isset($_POST['dst']) && $_POST['dst'] != 1){
				$err->setError('Daylight Savings is invalid.');
			}else{
				$isValid = $validate->validate($rules,$data);

				if($isValid){
					if(isset($_POST['dst'])){
						$data['dst'] = 1;
						$rules['dst'] = 'isBool';
					}else{
						$data['dst'] = 0;
						$rules['dst'] = 'isBool';
					}

					$update = $user->updateAccountSettings($data,$rules);
					if($update){
						header('location: my-account?tab=user-settings');
						exit();
					}else{
						$err->setError('Error 4001: Could not update Account Settings');
					}
				}else{
					$err->setError('Error 4002: Account settings are not valid');
				}
			}

		}else{
			$err->setError('Error 4003: Please set all fields');
		}
	break;
	//update keg settings
	case 'updateKegSettings':
		if(isset($_POST['kegSettings']) && ((isset($_POST['volume']) && $_POST['volume']  != "") || isset($_POST['temperature']) )){
			$data = array('Volume' => $_POST['volume'], 'Temperature' => $_POST['temperature']);
			if(isset($data['Volume'])) $rules['Volume'] = 'volume';
			if(isset($data['Temperature'])) $rules['Temperature'] = 'temperature';
			$isValid = $validate->validate($rules,$data);

			if($isValid){

				$update = $kegs->updateKegSettings($_POST['kegSettings'],$data,$rules);
				if($update){
					header('location: keg-dashboard?tab=set-units');
					exit();
				}else{
					$err->setError('Error 3001: Could not update keg settings');
				}
			}else{
				$err->setError('Error 3002: Keg Settings are not valid');
			}
		}else{
			$err->setError('Please set either volume or temperature');
		}
	break;
	//sent verification email again
	case 'verification':
		$newVer = $user->newVerification();

		if($newVer)
		switch($user->getAccountType()){
			case 'S':
				$verifyLink = WEB_ROOT.'/verify?e='.$user->getUserID().'&v='.$newVer;
				$emailData = array($user->getUserFirstName(), $user->getUserLastName(), $user->getUserID(), $verifyLink);
				$mailer->sendMail($user->getUserID(), 'Thank you for joining KegData', 'new-user.html', 'new-user.txt', $emailData);
			break;
			case 'R':
				$verifyLink = WEB_ROOT.'/verify?e='.$user->getUserID().'&v='.$newVer;
				$emailData = array($user->getUserFirstName(), $user->getUserLastName(), $user->getUserID(), $verifyLink);
				$mailer->sendMail($user->getUserID(), 'Thank you for joining KegData', 'new-bar.html', 'new-user.txt', $emailData);
			break;
			case 'D':
				$verifyLink = WEB_ROOT.'/verify?e='.$user->getUserID().'&v='.$newVer;
				$emailData = array($user->getUserFirstName(), $user->getUserLastName(), $user->getUserID(), $verifyLink);
				$mailer->sendMail($user->getUserID(), 'Thank you for joining KegData', 'new-distributor.html', 'new-user.txt', $emailData);
			break;
			case 'B':
				$verifyLink = WEB_ROOT.'/verify?e='.$user->getUserID().'&v='.$newVer;
				$emailData = array($user->getUserFirstName(), $user->getUserLastName(), $user->getUserID(), $verifyLink);
				$mailer->sendMail($user->getUserID(), 'Thank you for joining KegData', 'new-brewery.html', 'new-user.txt', $emailData);
			break;
			case 'E':
				$verifyLink = WEB_ROOT.'/verify?e='.$user->getUserID().'&v='.$newVer;
				$emailData = array($user->getUserFirstName(), $user->getUserLastName(), $user->getUserID(), $verifyLink);
				$mailer->sendMail($user->getUserID(), 'Thank you for joining KegData', 'new-enterprise.html', 'new-user.txt', $emailData);
			break;
		}
	break;
	}

?>