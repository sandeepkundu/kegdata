<?php
/*
 *KegData.com
 *Session Functions
 *Version 1 (2014-11-16)
*/
function startSession($result){
    global $gb;
    global $honeyandthebee;

	session_set_cookie_params(0);
	if(session_id() == ''){
		session_start();
	}

	$_SESSION['login_username']=$gb->eat($result['login'], $honeyandthebee);
    $_SESSION['login_friend']=$gb->eat($result['firstName'], $honeyandthebee).' '.$gb->eat($result['lastName'], $honeyandthebee);
    $_SESSION['login_id']=$result['account_id'];
    $_SESSION['ip_address']=$_SERVER['REMOTE_ADDR'];
    $_SESSION['user-agent']=$_SERVER['HTTP_USER_AGENT'];
    $_SESSION['nonce'] = md5(microtime(true));
    $_SESSION['OBSOLETE'] = FALSE;
    $_SESSION['EXPIRES'] = time() + 60;
}

function regenerateSession($reload = false)
{
	global $user;

    // This token is used by forms to prevent cross site forgery attempts
    if(!isset($_SESSION['nonce']) || $reload)
        $_SESSION['nonce'] = md5(microtime(true));

    if(!isset($_SESSION['IPaddress']) || $reload)
        $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];

    if(!isset($_SESSION['userAgent']) || $reload)
        $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

    $_SESSION['login_id'] = $user->getUserId();

    // Set current session to expire in 1 minute
    $_SESSION['OBSOLETE'] = true;
    $_SESSION['EXPIRES'] = time() + 60;

    // Create new session without destroying the old one
    session_regenerate_id(false);

    // Grab current session ID and close both sessions to allow other scripts to use them
    $newSession = session_id();
    session_write_close();

    // Set session ID to the new one, and start it back up again
    session_id($newSession);
    session_start();

    // Don't want this one to expire
    //unset($_SESSION['OBSOLETE']);
    //unset($_SESSION['EXPIRES']);
}

function checkSession()
{
	global $user;
    global $err;
    
    if(isset($_SESSION['OBSOLETE']) && isset($_SESSION['login_id'])){

        try{
            if($_SESSION['OBSOLETE'] && ($_SESSION['EXPIRES'] < time()))
                throw new Exception('Attempt to use expired session.');

            if(!is_numeric($_SESSION['login_id']))
                throw new Exception('No session started.');
            if($_SESSION['ip_address'] != $_SERVER['REMOTE_ADDR'])
                throw new Exception('IP Address mixmatch (possible session hijacking attempt).');

            if($_SESSION['user-agent'] != $_SERVER['HTTP_USER_AGENT'])
                throw new Exception('Useragent mixmatch (possible session hijacking attempt).');

            if(!$user->loadUser($_SESSION['login_username']))
                throw new Exception('Login attempt failed for ' . $_SESSION['login_username']);

            if(!$_SESSION['OBSOLETE'] && mt_rand(1, 100) == 1)
            {
                regenerateSession();
            }

            return true;

        }catch(Exception $e){
            $err->setError($e->getMessage());
            return false;
        }
    }else{
        return false;
    }
}

function destroySession(){
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    }
    session_destroy();
    session_unset();
}

?>