<?php 
require_once(FUNCTION_PATH."session.php");
require_once(CLASS_PATH."validation.php");

$user_name = urldecode($_POST['username']);
$pass = urldecode($_POST['password']);

//Set validation rules
$rules = array(
    'User Name' => 'username|required',
    'Password' => 'required'
);

$data = array(
    'User Name' => $user_name,
    'Password'  => $pass
);


$valid = $validate->validate($rules,$data);

if($valid){


    $data = array('id', 'login', 'firstName', 'lastName', 'account_id');
    $where = array("AND" => array("password" => md5($pass), "login" => $gb->bake($honeyandthebee, $user_name), "isActive" => 1));

    $result = $db->get(USERS,$data,$where);

    $error = $db->error();

}

if( isset($result) && $result != "" ){
    $isSet = $user->setUser($user_name, md5($pass));
    
    if($isSet){
        startSession($result);
        if($user->getAccountNumber() > 0){
            header("Location:".WEB_ROOT."/keg-dashboard"); 
        }else{
            header("Location:".WEB_ROOT.'/emp-admin');
        }
        exit();
    }else{
        $err->setError('1111: User Not Set. Please Try Again.');
    }
}
else{

    $err->setError('User Name or Password Incorrect. Please try again.');

}
?>