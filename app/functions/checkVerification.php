<?php
$noVerification=<<<EOD
	<p>Your registration with KegData is incomplete. You will need to click the link below to receive the completion email.</p><p><a href="my-account?action=verification">Send Completion email</a>.</p>
EOD;
$notComplete=<<<EOD
	<p>Your registration with KegData is incomplete. You should have received an email with a link to complete registration. It can take a few hours for the email to arrive</p><p>If you need to resend the email, <a href="my-account?action=verification">Send Completion email</a></p>
EOD;
$expired=<<<EOD
	<p>The previous email to complete registration is expired and you will need to resend the email to complete Verification.</p><p><a href="my-account?action=verification">Send Complete Registration Email</a></p>
EOD;
	if(isset($user) && is_null($user->getVerificationSent()) && is_null($user->getVerificationDate())){
		$err->setError($noVerification);
	}elseif(isset($user) && is_null($user->getVerificationDate()) && !is_null($user->getVerificationSent())){
		$date = new DateTime($user->getVerificationSent());
		$interval = new DateInterval('P3D');
		if(date("Y-m-d H:i:s") > $date->add($interval)){
			$err->setError($expired);
		}else{
			$err->setError($notComplete);
		}
	}


?>