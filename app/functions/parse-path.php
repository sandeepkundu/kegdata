<?php

/*Parse the path of the friendly url
 * returns array based on URL
 if URL = http://localhost/user/matthew/edit?language=en&hobbies=art&sport=football
   Array
  (
  [base] => /
  [call_utf8] => user/matthew/edit
  [call] => user/matthew/edit
  [call_parts] => Array
     (
       [0] => user
       [1] => matthew
       [2] => edit
     )
  [query_utf8] => language=en&hobbies=art&sport=football
  [query] => language=en&hobbies=art&sport=football
  [query_vars] => Array
     (
       [language] => en
       [hobbies] => art
       [sport] => football
     )
  )
*/

function parse_path() {
  $path = array();
  if (isset($_SERVER['REQUEST_URI'])) {
    $request_path = explode('?', $_SERVER['REQUEST_URI']);

    $path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
    $path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);
    $path['call'] = utf8_decode($path['call_utf8']);
    if ($path['call'] == basename($_SERVER['PHP_SELF'])) {
      $path['call'] = '';
    }
    $path['call_parts'] = explode('/', $path['call']);

    if(count($request_path) > 1){
      $path['query_utf8'] = urldecode($request_path[1]);
      $path['query'] = utf8_decode(urldecode($request_path[1]));
      $vars = explode('&', $path['query']);

      foreach ($vars as $var) {
        $t = explode('=', $var);
        $path['query_vars'][$t[0]] = $t[1];
      }

    }

    return $path;
  }
}

function find_page($whatpage){
  global $db;
  global $err;

  $data = array("page", "page_title", "requires_login");
  $where = array("path" => $whatpage);

  $page = $db->get(SITE_CONTENT,$data,$where);
  $error = $db->error();
  $query = $db->last_query();
  if(IS_DEBUG){
    //var_dump($page);
    //var_dump($query);
  }
  if($error[0] != '00000'){
    
    if(IS_DEBUG){
      $msg = 'ERROR IN QUERY  '.$query;
      $err->setMsg($msg);
    }else{
      $err->setMsg(PROD_MSG);
    }
  }
  return ($error[0] == '00000') ? $page : false;
}

?>