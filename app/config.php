<?php
/*Config file for kegdata.com
	Define Globals
	Set up Page Calls
	Set up DB
*/
	date_default_timezone_set('UTC');
	//Set debug mode
	//Set to False for Production
	DEFINE('IS_DEBUG', false);
	DEFINE('PROD_MSG', "We apologize for the issue - the hamsters ran out of beer and stopped working. This has been reported.");


	if($_SERVER['SERVER_NAME'] == 'kegdata.loc'){
		require_once('G:'.DIRECTORY_SEPARATOR.'Web'.DIRECTORY_SEPARATOR.'localhost'.DIRECTORY_SEPARATOR.'htdocs'.DIRECTORY_SEPARATOR.'kegdata'.DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'functions'.DIRECTORY_SEPARATOR.'utility.php');
	}else{
		require_once('var/www/html/kegdata/app/functions/utility.php');
	}

	//useful variables for globals & constants
	// $_SERVER['PHP_SELF'] : filename relative to document path
	// $_SERVER['SERVER_NAME'] : server host or virtual host name
	// $_SERVER['REQUEST_METHOD'] : For security
	// $_SERVER['REQUEST_URI'] : The URI given to access the page

	if($_SERVER['SERVER_NAME'] == 'kegdata.loc'){
		define_once('HOME_PATH', 'G:'.DIRECTORY_SEPARATOR.'Web'.DIRECTORY_SEPARATOR.'localhost'.DIRECTORY_SEPARATOR.'htdocs'.DIRECTORY_SEPARATOR.'kegdata'.DIRECTORY_SEPARATOR.'web');
		define_once('WEB_ROOT', 'http://kegdata.loc');
		define_once('dbServer', 'localhost');	
		define_once('dbUserName', 'ikegaccess');
		define_once('dbName', 'kegdata');
	}else{
		define_once('HOME_PATH', '/var/www/html/kegdata');
		define_once('WEB_ROOT', 'http://localhost:1114');
		define_once('dbServer', 'localhost');
		define_once('dbUserName', 'root');
		define_once('dbName', 'kegdata');
	}

	define_once('kegsalt', "RI+vN:Qo&dQbEX@DN%nEY%/!H!|xQ#W_mc");
	define_once('INCLUDE_PATH', HOME_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
	define_once('CLASS_PATH', INCLUDE_PATH.'class'.DIRECTORY_SEPARATOR);
	define_once('FUNCTION_PATH', INCLUDE_PATH.'functions'.DIRECTORY_SEPARATOR);
	define_once('VENDOR_PATH', INCLUDE_PATH.'vendor'.DIRECTORY_SEPARATOR);
	define_once('CSS_PATH', INCLUDE_PATH.'css'.DIRECTORY_SEPARATOR);
	define_once('JS_PATH', INCLUDE_PATH.'js'.DIRECTORY_SEPARATOR);
	define_once('IMG_PATH', INCLUDE_PATH.'img'.DIRECTORY_SEPARATOR);
	define_once('MAINART', HOME_PATH.DIRECTORY_SEPARATOR.'mainart'.DIRECTORY_SEPARATOR);
	define_once('MAIL_PATH', HOME_PATH.DIRECTORY_SEPARATOR.'email'.DIRECTORY_SEPARATOR );
	define_once('COMPANY_NAME', 'KegData&trade;');
	define_once('TYPE_RESIDENTIAL', 'S');
	define_once('TYPE_RESTAURANT', 'R');
	define_once('TYPE_BREWERY', 'B');
	define_once('TYPE_DISTRIBUTOR', 'D');
	define_once('TYPE_ENTERPRISE', 'E');
	define_once('VOLTAGE',.001221);

	require_once(INCLUDE_PATH.'include'.DIRECTORY_SEPARATOR.'tables.php');
	require_once(HOME_PATH.DIRECTORY_SEPARATOR.'beerkegs'.DIRECTORY_SEPARATOR.'config.php');
	require_once(CLASS_PATH."gingerbread.php");

	$gb = new gingerbread();
	$honeyandthebee = md5(beersalt.kegsalt);

	$acct_type = array('S' => 'Residential',
		'R' => 'Restaurant',
		'B' => 'Brewery',
		'D' => 'Distributor',
		'E' => 'Enterprise');


	require_once(VENDOR_PATH."medoo.min.php");
	$db = new medoo();

	require_once(CLASS_PATH."error.php");
	$err = new errors();

	require_once(CLASS_PATH."validation.php");
	$validate = new validation();

	require_once(FUNCTION_PATH."parse-path.php");
	$path = parse_path();

	require_once(CLASS_PATH."user.php");
	$user = new user();

	require_once(CLASS_PATH."mail.php");
	$mailer = new kegDataMailer();

?>