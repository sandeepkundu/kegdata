<?php
	require_once('vendor/PHPExcel.php');
	//  Include PHPExcel_IOFactory
	require_once 'vendor/PHPExcel/IOFactory.php';
	require_once 'vendor/medoo.min.php';

$inputFileName = '../lists/Beer Distributors 6-09-14.xls';
$db = new medoo();

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$table = 'Distributors';
$insert = array('')
//  Loop through each row of the worksheet in turn
for ($row = 1; $row <= $highestRow; $row++){ 
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
    var_dump($rowData);
    //  Insert row data array into your database of choice here
}


?>