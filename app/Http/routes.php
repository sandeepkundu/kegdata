<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Main Static Pages Controls
Route::get('/', 'WelcomeController@index');

Route::filter('csrf', function()
{
   $token = Request::header('X-CSRF-Token') ?: Input::get('_token');
   if (Session::token() !== $token) {
      throw new Illuminate\Session\TokenMismatchException;
   }
});

//Pages Routes
Route::group(["namespace" => 'Pages'], function(){
	Route::get('/about-keg-data', array('as' => 'about-keg-data', 'uses' => 'AboutController@indexAboutProduct'));
	Route::get('/about-us', array('as' => 'about-us', 'uses' => 'AboutController@indexAboutCompany'));
	Route::get('/news', array('as' => 'news', 'uses' => 'NewsController@index'));
	Route::get('/contact-us', array('as' => 'contact-us', 'uses' => 'ContactController@index'));
	Route::get('/support', array('as' => 'support', 'uses' => 'SupportController@support'));
	Route::get('/faq', array('as' => 'faq', 'uses' => 'SupportController@faq'));
	Route::get('/install', array('as' => 'install', 'uses' => 'InstallController@index'));
	Route::get('/register/{tab?}', array("uses" => 'RegisterController@index'))->where(['tab' => '[a-z]+']);
	Route::post('/register', array("uses" => 'RegisterController@postRegister'));
	Route::get('/store', array("as" => 'store', "uses" => 'StoreController@index')); //paypal
});


//stripe Store Routes
//Route::group(['prefix' => 'store', "middleware" => 'role', "namespace"=> 'Store'], function(){
Route::group(['prefix' => 'store', "namespace"=> 'Store'], function(){
	//Route::get('/plans', array('as' => 'plans', 'uses' => 'ShopController@showPlans'));
	Route::get('/stripe', 'ShopController@start');
	//Route::get('/start', 'ShopController@start');
	 //Route::get('/stripe', array('as' => 'store.checkout.pay', 'uses' => 'ShopController@showPlans'));
	Route::get('/thankyou/{orderID}', array('as' => 'store.thankyou', 'uses' => 'CheckoutController@showOrder'));
	Route::get('/checkout', array('as' => 'store.checkout', 'uses' => 'CheckoutController@index'));
	Route::post('/checkout/pay', array('as' => 'store.checkout.pay', 'uses' => 'CheckoutController@submitPayment'));
	Route::get('/plans', array('as' => 'store.checkout.pay', 'uses' => 'ShopController@showPlans'));
	Route::get('/cart', array('as' => 'store.cart', 'uses' => 'ShopController@showCart'));
	Route::post('/cart/update', array('as' => 'store.cart.update', 'uses' => 'ShopController@updateCart'));
	Route::post('/cart/promo', array('as' => 'store.cart.promo', 'uses' => 'ShopController@promoCart'));
	Route::get('/cart/cancel', array('as' => 'store.cart.cancel', 'uses' => 'ShopController@destroyCart'));
	Route::get('/cart/remove/{cart}/{rowID}', array('as' => 'store.cart.remove', 'uses' => 'ShopController@removeItem'));
	Route::post('/cart/add/{type}', array('as' => 'store.cart.add', 'uses' => 'ShopController@addCart'));
	Route::get('/stripe/{category}', array('as' => 'store.category', 'uses' => 'ShopController@showProducts'));

});



Route::get('verification/{hash}', 'Auth\VerificationController@index')->where(['hash' => '[a-z0-9A-Z]+']);
Route::get('verification/{id}/{hash?}', 'Auth\VerificationController@create')->where(['id' => '[0-9]+', '[a-z0-9A-Z]+']);

//API Routes
Route::group(['prefix' => 'api', 'namespace' => 'API'], function()
{
    Route::get('brewdist/{column}/{term}', 'BrewDistController@getByColumn')->where(['column' => '[a-zA-Z_]+', 'term' => '[0-9a-zA-Z\'-()]+']);
    Route::get('keg/{id}', 'KegController@getByID')->where(['id' => '[0-9]+']);
    Route::get('hub/account/{id}', 'HubController@getByAccountID')->where(['id' => '[0-9]+']);
    Route::get('beer/{term?}', 'BeerController@getByName')->where(['term' => '[a-zA-Z/s]+']);
    Route::get('timezone/{region?}', 'TimezoneController@getByRegion')->where(['term' => '[a-zA-Z]+']);
    Route::get('data/time', 'DataController@getTime');
    Route::get('hub/info', 'DataController@getHubInfo');
    Route::get('hub/{startrow}/{numrows}', 'DataController@getHubFirmware')->where(['startrow' => '[0-9]+', 'numrows' => '[0-9]+']);
    Route::get('sensor/info', 'DataController@getSensorInfo');
    Route::get('sensor/{startbyte}/{numbytes}', 'DataController@getSensorFirmware')->where(['startbyte' => '[0-9]+', 'numbytes' => '[0-9]+']);
    Route::post('data', 'DataController@uploadData');
});

//Store Controls

//Stripe Webhook Routes
Route::group(['prefix' => 'stripe', 'namespace' => 'Stripe'], function(){
Route::post('webhooks', 'WebhooksController@index');
});

//Login Controls
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);




//DashboardControls

Route::group(["prefix" => 'dashboard'],function(){
	Route::patch('/kegsetup', 'DashboardController@updateKeg');
	Route::patch('/settings', 'DashboardController@updateSettings');
	Route::put('/notifications', 'DashboardController@storeNotifications');
	Route::delete('/notifications/{id}', 'DashboardController@destroyNotifications');
	Route::patch('/account/address/{id}', 'DashboardController@updateAddress');
	Route::patch('/account/user/{id}', 'DashboardController@updateUser');
	Route::put('/account/user', 'DashboardController@storeUser');
	Route::delete('/account/user/{id}', "DashboardController@destroyUser");
	Route::patch('/account/{id}', "DashboardController@updateAccount");
	Route::patch('/account/notification/{id}', "DashboardController@updateSmsEmail");
	Route::get('/reports/excel', "DashboardController@getExcel");
	Route::get('/reports/getReport', "DashboardController@getReport");
	Route::get('/reports/getReportPour', "DashboardController@getReportPour");
	Route::any('/{tab?}/{page?}', 'DashboardController@index');
	Route::get('/updatedevice/user/{id}', "DashboardController@updatekeglsm");

	Route::get('/calling_console_via_ajax/user/{id}', "DashboardController@callingConsoleViaAjax");
	
	//Changes
	Route::get('/graph_reports_demo', 'DashboardController@index');
	Route::get('/graph_reports_test', 'DashboardController@index');
	Route::get('/graph_reports2', 'DashboardController@index');
	/* New route for lsm Levels on dashboord*/
	Route::get('/levels_newlsm', 'DashboardController@index');

});	



//Employee Controls
Route::group(["middleware" => 'role'],function(){
	//Route::resource('/employee/products', 'Employees\ProductsController');
	Route::get('/employee/orders/excel', array('as'=>'employee.excelorders', 'uses'=> 'Employees\OrdersController@getOrdersExcel'));
	Route::get('/employee/user/excel', array('as'=>'employee.excelusers', 'uses'=> 'Employees\UsersController@getUserExcel'));
	Route::get('/employee/user/hub/excel', array('as'=>'employee.excelusers', 'uses'=> 'Employees\UsersController@getUserExcelHub'));
	Route::post('/employee/hub/account', array('as' => 'employee/hub', 'uses' => 'Employees\EmployeeController@postAccount'));
	Route::get('/employee/hub/account/{accountID}',  array('as' => 'employee/hub', 'uses' => 'Employees\EmployeeController@hubsByAccount'));
	Route::post('/employee/hub/account/{accountID}', array('as' => 'employee/hub', 'uses' => 'Employees\EmployeeController@addHub'));
	Route::patch('/employee/hub/account/{accountID}', array('as' => 'employee/hub', 'uses' => 'Employees\EmployeeController@updateHub'));
	Route::delete('/employee/hub/account/{accountID}', array('as' => 'employee/hub', 'uses' => 'Employees\EmployeeController@deleteHub'));
	Route::patch('/employee/user', array('as' => 'employee.user', 'uses' => 'Employees\UsersController@updateUser'));

	Route::patch('/employee/users', array('as' => 'employee.user', 'uses' => 'Employees\UsersController@updateUserHub'));
	Route::delete('/employee/users/{id}', "Employees\UsersController@destroyUserHub");

	Route::patch('/employee/user/{id}', 'UsersController@updateUser');
	Route::get('/employee/{tab?}', array('as' => 'employee', 'uses' => 'Employees\EmployeeController@index'));

	Route::get('/employee/addhubbymodal/{hubname}', array('as' => 'employee', 'uses' => 'Employees\EmployeeController@addHubByModal'));

	Route::get('/employee/fetch_device_info/{hubname}', array('uses' => 'Employees\EmployeeController@fetchDeviceInfo'));

	//Store Overview
	Route::get('/employee/store', array('as' => 'employee.store', 'uses' => 'Employees\StoreController@index'));

	//Category Routes
	Route::get('/employee/store/category/{id?}', array('as' => 'employee.store.category', 'uses' => 'Employees\StoreController@categoryIndex'));
	Route::put('/employee/store/category', array('as' => 'employee.store.category', 'uses' => 'Employees\StoreController@storeCategory'));
	Route::patch('/employee/store/category/{id}', array('as' => 'employee.store.category',  'uses' => 'Employees\StoreController@updateCategory'));
	Route::delete('/employee/store/category/{id}', array('as' => 'employee.store.category',  'uses' => 'Employees\StoreController@deleteCategory'));

	//Tax Routes
	Route::get('/employee/store/tax/{id?}', array('as' => 'employee.store.tax', 'uses' => 'Employees\StoreController@taxIndex'));
	Route::put('/employee/store/tax', array('as' => 'employee.store.tax', 'uses' => 'Employees\StoreController@storeTax'));
	Route::patch('/employee/store/tax/{id}', array('as' => 'employee.store.tax',  'uses' => 'Employees\StoreController@updateTax'));
	Route::delete('/employee/store/tax/{id}', array('as' => 'employee.store.tax',  'uses' => 'Employees\StoreController@deleteTax'));

	//Product Routes
	Route::get('/employee/store/product/{id?}', array('as' => 'employee.store.product', 'uses' => 'Employees\StoreController@productIndex'));
	Route::put('/employee/store/product', array('as' => 'employee.store.product', 'uses' => 'Employees\StoreController@storeProduct'));
	Route::patch('/employee/store/product/{id}', array('as' => 'employee.store.product',  'uses' => 'Employees\StoreController@updateProduct'));
	Route::delete('/employee/store/product/{id}', array('as' => 'employee.store.product',  'uses' => 'Employees\StoreController@deleteProduct'));

	//Promotion Routes
	Route::get('/employee/store/promotion/{id?}', array('as' => 'employee.store.promotion', 'uses' => 'Employees\StoreController@promotionIndex'));
	Route::put('/employee/store/promotion', array('as' => 'employee.store.promotion', 'uses' => 'Employees\StoreController@storePromotion'));
	Route::patch('/employee/store/promotion/{id}', array('as' => 'employee.store.promotion',  'uses' => 'Employees\StoreController@updatePromotion'));
	Route::delete('/employee/store/promotion/{id}', array('as' => 'employee.store.promotion',  'uses' => 'Employees\StoreController@deletePromotion'));

	//Plan Routes
	Route::get('/employee/store/plan/{id?}', array('as' => 'employee.store.plan', 'uses' => 'Employees\StoreController@PlanIndex'));
	Route::put('/employee/store/plan', array('as' => 'employee.store.plan', 'uses' => 'Employees\StoreController@storePlan'));
	Route::patch('/employee/store/plan/{id}', array('as' => 'employee.store.plan',  'uses' => 'Employees\StoreController@updatePlan'));
	Route::delete('/employee/store/plan/{id}', array('as' => 'employee.store.plan',  'uses' => 'Employees\StoreController@deletePlan'));

	//Order Routes
	Route::get('employee/orders', array('as' => 'employee.order', 'uses' => 'Employees\OrdersController@getOrders'));
	Route::get('employee/order/{id}', array('as' => 'employee.order', 'uses' => 'Employees\OrdersController@getOrder'));
	Route::get('employee/order/edit/{id}', array('as' => 'employee.order', 'uses' => 'Employees\OrdersController@getOrder'));
	Route::patch('employee/order', array('as' => 'employee.order', 'uses' => 'Employees\OrdersController@updateOrder'));
	Route::delete('/employee/order/{id}', array('as' => 'employee.order',  'uses' => 'Employees\OrdersController@deleteOrder'));

	//misc route
	Route::patch('employee/updatekegtype', array('as' => 'employee.order', 'uses' => 'Employees\EmployeeController@updatekegtype'));



});


//Test pages
Route::get('test', 'TestController@index');
Route::get('/job/data', function () {
    dispatch(new App\Jobs\ProcessKegData);
    return 'Done!';
});

