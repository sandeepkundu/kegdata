<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class NotificationsFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'id' => array('sometimes', 'required','numeric'),
            'kegdevice_id' => array('required', 'numeric'),
            'notification' => array('required', 'in:L,E,U,P,T,K,B'),
            'value' => array('sometimes', 'numeric'),
            'frequency' => array('required', 'numeric'),
            'operator' => array('sometimes', 'in:<,>'),
        ];
    }

    public function messages(){
    	return [
    		'id.required' => "Something has gone wrong with the selected notification. Please select the notification again to edit it",
                                'id.numeric' => "Something has gone wrong with the selected notification. Please select the notification again to edit it!!",
                                'kegdevice_id.required' => "Please select a keg to apply the notification to",
                                'kegdevice_id.numeric' => "Something is wrong with keg selection. Please refresh the page.",
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}