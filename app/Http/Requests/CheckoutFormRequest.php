<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class CheckoutFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            /*'stripeToken' => array('required'),
            'address1' => array('required'),
            'address2' => '',
            'city' => array('required'),
            'stateCode' => array('required','size:2','alpha'), 
            'zipcode' => array('required', 'between:5,10', 'regex:/(\d{5}-\d{4})|(\d{5})/'),
            'countryCode' => array('required','size:2','alpha'),
            'billing_name' => array('required'),
            'billing_address1' => array('required'),
            'billing_address2' => '',
            'billing_city' => array('required'),
            'billing_stateCode' => array('required','size:2','alpha'), 
            'billing_zipcode' => array('required', 'between:5,10', 'regex:/(\d{5}-\d{4})|(\d{5})/'),
            'billing_countryCode' => array('required','size:2','alpha'),*/
        ];
    }

    public function messages(){
    	return [
           // 'zipcode.regex' => 'Zip Code must be in format 77777 or 77777-7777'

    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}