<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
use App\KegData\Repositories\EloquentAccountSettingRepository;

class SettingsFormRequest extends FormRequest {

    protected $redirect;
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'id' => array('required', 'numeric'),
            'volumeType' => array('required', 'in:oz,g,l,ml'),
            'temperatureType' => array('required', 'in:C,F'),
            'timezone' => array('sometimes', 'numeric'),
            'currency' => array('required', 'regex:/[A-Z]{3}/'),
            'weekStart' => array('sometimes', 'in:Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday'),
            'daystart12' => array('sometimes', 'regex:/[0-1]?[0-9]:[0-5][0-9]:[0-5][0-9]/'), 
            'amstart' => array('sometimes', 'in:am,pm'),
            'dayend12' => array('sometimes', 'regex:/[0-1]?[0-9]:[0-5][0-9]:[0-5][0-9]/'),
            'pmstart' => array('sometimes', 'in:am,pm'),
        ];
    }

    public function messages(){
    	return [
                                'id.required' => "Something has gone horribly wrong with the form. Please refresh the page to reset.",
    		'currency.regex' => "The value provided for currency is not a valid currency.",
                                'daystart12.regex' => 'The Day Start field is not a valid time.',
                                'dayend12.regex' => 'The Day End Field is not a valid time.'
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}