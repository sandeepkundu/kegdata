<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class EmpTaxFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

       //  $id = $this->route()->id;
        // $redirect = $this->segments();

       // $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'name' => array('required', 'between:1,255'),
            'rate' => array('required', 'numeric'),
        ];
    }

    public function messages(){
    	return [	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}