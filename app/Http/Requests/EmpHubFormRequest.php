<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class EmpHubFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $id = $this->route()->id;
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1].'/'.$redirect[2].'/'.$redirect[3];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'account_id' => array('required', 'numeric'),
            'hubMac' => array('required', 'regex:/([0-9a-f]{2}:){5}[0-9a-f]{2}|[0-9a-f]{12}/'),
        ];
    }

    public function messages(){
    	return [
                                'accountID.required' => 'Please select an account for an address to be added to. You must submit the account selection separately.',
                                'accountID.numeric' => 'For some reason, the account provided is not matching a valid account. Please select a new account.',
                                'hubMac.required' => 'Please enter a Hub Mac Address to add it to the account',
                                'hubMac.regex' => 'Please enter a hub mac address as XX:XX:XX:XX:XX:XX or XXXXXXXXXXXX'
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}