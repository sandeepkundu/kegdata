<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
use GingerBread;
use App\KegData\Repositories\EloquentAccountRepository;
use App\KegData\Repositories\EloquentUserRepository;
use App\KegData\Repositories\EloquentBrewDistRepository;

class AccountFormRequest extends FormRequest {

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        // Optionally customize this version using new ->after()
        $validator->after(function() use ($validator) {
            $user = new EloquentUserRepository;
            $account = new EloquentAccountRepository;
            $GingerBread = new GingerBread;
            
            $input = $this->input();
            $account = $account->getByColumn($GingerBread->preformEncrypt($input['accountName']), 'accountName')->count();
            $user = $user->getByColumn($GingerBread->preformEncrypt($input['email']),'email')->count();
            if($user > 0){
                $validator->errors()->add('email', 'Email Address requested is already registered with KegData');
                
                if($account > 0 ){
                    $validator->errors()->add('accountName', 'Account Name is already registered with KegData');
                }
            }

        });

        if(!empty($input['brewDistID']) && ($input['accountType'] == 'B' || $input['accountType'] == 'D')){
            $brewDist = new EloquentBrewDistRepository;
            $brewDist = $brewDist->where('id', '=', $input['brewDistID'])->where('type', '=', $input['accountType'])->where('account_id', '!=', '0')->count();
            if($brewDist > 0){
                    $validator->errors()->add('Unrecovable Error', 'Oh No! An unidentified error has been caught.');
            }       
        }


        return $validator;
    }

    public function rules()
    {

        return [
            'brewDistID' => array('numeric'),
            'accountType' => array('sometimes', 'required', 'exists:account_types,type'),
            'accountName' => array('sometimes','required', 'unique:accounts'),
            'address1' => array('sometimes', 'required'),
            'address2' => '',
            'city' => array('sometimes', 'required'),
            'stateCode' => array('sometimes','required','size:2','alpha'), 
            'zipcode' => array('sometimes', 'required', 'between:5,10', 'regex:/(\d{5}-\d{4})|(\d{5})/'),
            'county' => array('sometimes','required','alpha'),
            'countryCode' => array('sometimes','required','size:2','alpha'),
            'currency' => array('size:3','alpha'),
            'phone1' => array('sometimes','required','between:10,20'),
            'phone2' => array('between:10,20'),
            'firstName' => array('sometimes','required'),
            'lastName' => array('sometimes','required'),
            'email' => array('sometimes','required','email','unique:users'),
            'password' => array('sometimes','required','between:8,20','confirmed'),
        ];
    }

    public function messages(){
        return [
            'zipcode.regex' => 'Zip Code must be in format 77777 or 77777-7777'
        ];
    }
 
    public function authorize(GingerBread $GingerBread)
    {
        return true;
    }
 
}