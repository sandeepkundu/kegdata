<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php

use Response;
use Illuminate\Foundation\Http\FormRequest;

class EmpPlanFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

       //  $id = $this->route()->id;
        // $redirect = $this->segments();

       // $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {

        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'name' => array('required', 'between:1,255'),
            'price' => array('required', 'regex:/[\d]{1,3}\.[\d]{2}/'),
            'min' => array('required', 'numeric'),
            'max' => array('required', 'numeric'),
            'interval' => array('required'),
            'plan_type' => array('required'),
            'term_description' => array('required', 'between:1,22'),
            'slug' => array('required', 'between:1,50'),
            'short_description' => array('between:1,255'),
            'meta_description' => array('between:1,255'),
            'meta_keywords' => array('between:1,255'),
            'image' => array( 'image'),
            'is_quantity' => array('between:0,1'),
            'quantity_price' => array('required_if:is_quantity,1', 'regex:/[\d]{1,3}\.[\d]{2}/'),
        ];
    }

    public function messages(){
    	return ['price.regex' => 'Price must be in dollar form XX.XX', 'quantity_price.regex' => 'Price must be in dollar form XX.XX'];

    }

    public function authorize()
    {
    	return true;
    }

}
