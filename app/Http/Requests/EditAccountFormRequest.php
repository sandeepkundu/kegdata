<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class EditAccountFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $id = $this->route()->id;
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'accountName' => array('sometimes', 'required'),
            'accountEmail1' => array('email'),
            'accountEmail2' => array('email'),
            'accountPhone1' =>array('between:10,20', 'required_without:accountPhone2'),
            'accountPhone2' =>array('between:10,20', 'required_without:accountPhone1'),
            'website' => array('URL'),
        ];
    }

    public function messages(){
    	return [	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}