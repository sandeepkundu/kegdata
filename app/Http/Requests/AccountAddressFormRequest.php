<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class AccountAddressFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $id = $this->route()->id;
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'address1' => array('sometimes', 'required'),
            'address2' => '',
            'city' => array('sometimes', 'required'),
            'stateCode' => array('size:2','alpha'), 
            'zipcode' => array('sometimes', 'required', 'between:5,10', 'regex:/(\d{5}-\d{4})|(\d{5})/'),
            'county' => array('sometimes','required','alpha'),
            'countryCode' => array('sometimes','required','size:2','alpha'),
        ];
    }

    public function messages(){
    	return [
                                'zipcode.regex' => 'Zip Code must be in format 77777 or 77777-7777'
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}