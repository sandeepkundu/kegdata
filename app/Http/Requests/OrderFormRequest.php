<?php namespace App\Http\Requests;
//file: app/Http/Requests/OrderFormRequest.php

use Response;
use Illuminate\Foundation\Http\FormRequest;
use App\KegData\Repositories\EloquentOrderRepository;
use GingerBread;

class OrderFormRequest extends FormRequest {

public function rules()
{
  return[
    'firstName' => array('sometimes','required'),
    'lastName' => array('sometimes','required'),
    'address1' => array('required'),
    'address2' => '',
    'city' => array('required'),
    'stateCode' => array('required','size:2','alpha'),
    'zipcode' => array('required', 'between:5,10', 'regex:/(\d{5}-\d{4})|(\d{5})/'),
    'countryCode' => array('required','size:2','alpha'),
    'name' => array('required', 'between:1,255'),
    'part_id' => array('required', 'between:1,255'),
    'price' => array('required', 'regex:/[\d]{1,3}\.[\d]{2}/'),
    'stock' => array('required', 'numeric'),
    'slug' => array('required', 'between:1,50'),
    'short_description' => array('between:1,255'),
    'meta_description' => array('between:1,255'),
    'meta_keywords' => array('between:1,255'),
    'image' => array( 'image')
  ];
}
public function messages(){
 return true;
}


}
