<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class EmpUserFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

/*        $id = $this->route()->id;
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1].'/'.$redirect[2].'/'.$redirect[3];*/
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'user_id' => array('required', 'numeric'),
            'firstName' => array('required'),
            'lastName' => array('required'),
            'email' => array('required','email')
        ];
    }

    public function messages(){
    	return [
                                'user_name.required' => 'Oops! Something went wrong and we don\'t know what user to update - please select the user again.',
                                'firstName.required' => 'An user has to have a first name.',
                                'lastName.required' => 'An user has to have a last name.'
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}