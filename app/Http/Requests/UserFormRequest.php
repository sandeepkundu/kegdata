<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
use App\KegData\Repositories\EloquentUserRepository;
use GingerBread;


class UserFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $id = $this->route()->id;
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];

        $validator->after(function() use ($validator, $id) {
            $user = new EloquentUserRepository;
            $GingerBread = new GingerBread;
            $input = $this->input();
            $email = empty($input['email']) ? $input['userEmail'] : $input['email'];
            $query = $user->query();
            $query->where('email', '=', $GingerBread->preformEncrypt($email));
            if(!empty($id)){
                $query->where('id', '!=', $id);
            }
            if($query->count()){
                    $validator->errors()->add('email', 'The email requested is already in use.');
            }
        });



        return $validator;
    }

    public function rules()
    {

        return [
            'firstName' => array('sometimes','required'),
            'lastName' => array('sometimes','required'),
            'email' => array('sometimes','required','email'),
            'password' => array('between:8,20','confirmed,'),
            'userEmail' => array('sometimes', 'required', 'email'),
            'userPassword' => array('sometimes', 'required', 'between:8,20', 'confirmed'),
            'userFirstName' => array('sometimes', 'required'),
            'userLastName' => array('sometimes', 'required'),
            'isActive' => array('boolean'),
            'isAdmin' => array('boolean')
        ];
    }

    public function messages(){
    	return [
                                'zipcode.regex' => 'Zip Code must be in format 77777 or 77777-7777'
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}