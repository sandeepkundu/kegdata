<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;

class EmpPromotionFormRequest extends FormRequest {

    protected $redirect;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

       //  $id = $this->route()->id;
        // $redirect = $this->segments();

       // $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'name' => array('required', 'between:1,255'),
            'slug' => array('required', 'between:1,50'),
            'start_date' => array('date'),
            'end_date' => array('date'),
            'apply_to_products' => array('digits:1'),
            'apply_to_subscriptions' => array('digits:1'),
            'percentage' => array('numeric','digits_between:1,2', 'numeric'),
            'short_description' => array('between:1,255'),
            'meta_description' => array('between:1,255'),
            'meta_keywords' => array('between:1,255'),
            'image' => array( 'image')
        ];
    }

    public function messages(){
    	return [	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}