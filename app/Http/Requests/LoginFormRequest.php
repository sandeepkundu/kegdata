<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
 
class LoginFormRequest extends FormRequest {
 
    public function rules()
    {

        return [
            'email' => array('required','email'),
            'password' => array('required'),
        ];
    }

    public function authorize()
    {
        return true;
    }
 
}