<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
use App\KegData\Repositories\EloquentKegDeviceRepository;

class KegSetupFormRequest extends FormRequest {

    protected $redirect;
    
    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $redirect = $this->segments();

        $this->redirect = '/'.$redirect[0].'/'.$redirect[1];
        // Optionally customize this version using new ->after()
        /*$validator->after(function() use ($validator) {
         
        });*/

        return $validator;
    }

    public function rules()
    {

        return [
            'id' => array('numeric', 'required'),
            'kegType' => array('sometimes', 'required', 'numeric'),
            'showDevice' => array('sometimes', 'boolean'),
            'product_id' => array('sometimes', 'numeric'),
            'style' => array('sometimes', 'regex:/^[^<>]+/'),
            'description' => array('sometimes', 'regex:/^[^<>]+/'),
            //'selectedDistID' => array('sometimes', 'numeric'), 
            //'distType' => array('sometimes', 'in:B,D'),
            'deviceName' => array('sometimes', 'required_with:product_id','regex:/([[:alnum:]\s,."\'-_])+/'),
        ];
    }

    public function messages(){
    	return [
    		'id.required' => "You must choose a keg to setup.",
    		'beerstyle.regex' => "The Beer Style Field has invalid characters. Please use alphanumeric and basic punctuation only",
    		'description.regex' => "The description field has invalid characters. Please use alphanumeric and basic punctuation only",
    		'deviceName.regex' => "The device name field has invalid characters. Please use alphanumeric and basic punctuation only",
    	];
    }
 
    public function authorize()
    {
    	return true;
    }
 
}