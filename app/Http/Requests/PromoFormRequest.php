<?php namespace App\Http\Requests;
//file: app/Http/Requests/UserFormRequest.php
 
use Response;
use Illuminate\Foundation\Http\FormRequest;
 
class PromoFormRequest extends FormRequest {
 
    public function rules()
    {

        return [
            'slug' => array('required','alpha_dash','exists:promotions,slug,deleted_at,NULL'),
        ];
    }

 public function messages(){
        return [
            'slug.required' => 'The promo code entered is not valid.',
            'slug.alpha_dash' => "The promo code entered is not valid.",
            'slug.exists' => "The promo code entered is not valid."
        ];
    }

    public function authorize()
    {
        return true;
    }
 
}