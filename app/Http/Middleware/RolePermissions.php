<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Routing\ResponseFactory;
use Redirect;

class RolePermissions implements Middleware {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth, ResponseFactory $response)
	{
		$this->auth = $auth;
		$this->response = $response;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$routeName = $request->route()->getName();


		$routePermission = config('role_perms');
		//dd($routePermission);
		//$routePermission[$routeName]
		if ( ! $this->auth->user() || ! $this->auth->user()->hasRole( $routePermission['employee'] ) )
		{
			return Redirect::intended('/');
		}

		return $next($request);
	}

}
