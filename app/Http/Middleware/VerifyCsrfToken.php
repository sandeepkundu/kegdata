<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 private $openRoutes = ['api/data'];
	public function handle($request, Closure $next)
	{
		if(in_array($request->path(), $this->openRoutes)){
		            return $next($request);
		        }else{
		        	
					return $next($request);
		            //return parent::handle($request, $next);
		        }
	}

}
