<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use App\KegData\Models\Timezone as Timezone;

class BeforeMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public $user_timezone=null;
	public function handle($request, Closure $next)
	{
		/*if(isset(Auth::user()->id)) {
			$accountSettings=Auth::user()->account->accountSetting->first();

			$timezone = ($accountSettings->timezone == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings->timezone);

			$this->user_timezone=$timezone->timezone;
			

		} else {
			$this->user_timezone='UTC';
		}
		
		date_default_timezone_set($this->user_timezone);*/

		/*if (date_default_timezone_get()) {
		    echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';
		} */


		return $next($request);
	}

}
