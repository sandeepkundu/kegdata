<?php namespace App\Http\Middleware;
use Closure;
use App;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Http\Response;

class KegDataAgent implements Middleware {

 /**
  * Handle an incoming request.
  *
  * @param \Illuminate\Http\Request $request
  * @param \Closure $next
  * @return mixed
  */
 public function handle($request, Closure $next)
 {
 	$userAgent = $request->header('user-agent');
 	$connection = $request->header('connection');

 	if($userAgent !=  'kegdata-agent' && $connection != 'close'){
 		App::abort(404);
 	}

  	return $next($request);
 }
}