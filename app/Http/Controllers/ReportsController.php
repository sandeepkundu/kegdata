<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KegData\Repositories\AccountUserRepository as AccountUser;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\MessageBag;
use Response;

class ReportsController extends Controller {

	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth, AccountUser $accountUser)
	{
		$this->middleware('auth');
		$this->auth = $auth;
		$this->accountUser = $accountUser;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request, $page = 1)
	{
		$tab = 'reports';
		$input = $request->input();
		$accountType = $this->auth->user()->account->accountType;
		switch($accountType){
			case 'D':
				return view($this->layout, ['content'=> view('dashboard.distributor.dashboard')->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'B':
				return view($this->layout, ['content'=> view('dashboard.brewery.dashboard')->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'E':
				return view($this->layout, ['content'=> view('dashboard.enterprise.dashboard')->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'R':
				return view($this->layout, ['content' => view('dashboard.dashboard')->withTab($tab)->withInput($input)->withPage($page)]);
			break;
		}
		return view($this->layout, ['content'=> view('dashboard.dashboard')->withTab($tab)->withInput($input)->withPage($page)]);
	}

}
