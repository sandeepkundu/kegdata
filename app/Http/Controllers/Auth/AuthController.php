<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests\AccountFormRequest;
use App\Http\Requests\LoginFormRequest;
use App\KegData\Repositories\AccountUserRepository;
use App\KegData\Models\User;
use Request;
use GingerBread;
use Hash;
use Redirect;
use Illuminate\Support\MessageBag;
use Config;
use Cart;
use DB;
use Auth;
use Carbon\Carbon;
class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login ControllerWhy
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors.  don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	protected $layout = 'layouts.default';


	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar, AccountUserRepository $accountUser){
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->accountUser = $accountUser;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function getRegister(Request $request){
		$input = Request::old();

		return view($this->layout, ['content'=> view('auth.register')->with('input', $input)]);
	}

	public function putRegister(AccountFormRequest $request){
		$input = $request->input();
		$register = $this->accountUser->create($input);
		if($register){
			return redirect('auth/success');
		}else{
			$messages = new MessageBag;
			$messages->add('Registration', 'Apologies, but it appears registration failed.');
			return redirect('auth/register')->withErrors($messages)->withInput();
		}
	}

	public function getLogin(){
		return view($this->layout, ['content'=> view('auth.login')]);
	}

	public function postLogin(LoginFormRequest $request){

		$Gingerbread = new Gingerbread;

		$credentials = $request->only('email', 'password');

		//$credentials['email'] = $Gingerbread->preformEncrypt($credentials['email']);
		$credentials['isActive'] = 1;

		if ($this->auth->attempt($credentials, $request->has('remember'))){
			DB::table('users')
			->where('id', Auth::user()->id)
			->update(['lastlogin' => Carbon::now()]);
			if(Cart::count()){
				return Redirect::intended('/store/checkout');
			}else{
				return Redirect::intended('dashboard');
			}
		}else{
			$user = User::where('email', $credentials['email'])->first();

			if( $user && $user->password == md5($credentials['password']) ){
				$user->password = Hash::make($credentials['password']);
				$user->save();
				$this->auth->login($user);
				DB::table('users')
				->where('id', Auth::user()->id)
				->update(['lastlogin' => Carbon::now()]);
				if(Cart::count()){
					
					return Redirect::intended('/store/checkout');
				}else{
					
					return Redirect::intended('dashboard');
				}
				
			}else{
				$messages = new MessageBag;
				$messages->add('Login', 'Username or password not correct');
				return Redirect::intended('auth/login')->withErrors($messages);
			}	
		}
	}

	public function getSuccess(){
		return view($this->layout, ['content' => view('auth.success')]);
	}

}


