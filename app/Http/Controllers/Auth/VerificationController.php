<?php namespace App\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Models\UserVerification as Verification;
use Carbon\Carbon;
use Mail;

class VerificationController extends Controller{

	protected $layout = 'layouts.default';

	public function __construct(Verification $verification, User $user){
		$this->verification = $verification;
		$this->user = $user;
	}

	public function index($hash){

		$success = false;

		$verification = $this->verification->where('verificationHash', $hash)->first();

		if(count($verification) > 0){

			$verification->verificationConfirmation = Carbon::now();
			$verification->save();
			$success = true;
			$title = 'Your account is now verified';
		}else{
			$title = 'Verification failed';
		}

		return view($this->layout, ['content'=> view('auth.verify')->with('success', $success)->with('title', $title)]);
	}

	public function create($id, $hash=""){
		$user = $this->user->find($id);
		$verification = $this->verification->find($id);

		if(empty($hash) && empty($verification)){
			$verification = new Verification;
			$verification->id = $user->id;
			$verification->verificationHash = md5(rand(1,1000));
			$verification = $verification->save();
		}

		
		if( isset($verification) && ($verification->updated_at->diffInMinutes() > 60)){
			$verification->verificationHash = md5(rand(1,1000));
			$verification = $verification->save();
		}

		if(isset($verification) && ($verification->updated_at->diffInMinutes() > 3)){

			$emailArray = array(
				'firstName' => $user->firstName, 
				'lastName' => $user->lastName, 
				'link' => 'http://www.kegdata.com/verification/'.$verification->verificationHash,  
				'email' => $user->email
			);

			Mail::send(['emails.welcome', 'emails.welcomeText'], $emailArray, function($message) use ($user){

	    			$message->to($user->email)->bcc('latisha.mcneel@gmail.com', 'kevin@kegdata.com', 'lou@kegdata.com')->subject('Welcome to KegData!');

			});

		
			if(count(Mail::failures())){
				return "Mail Failure";
			}else{
				return "Mail Success";
			}
		}else{
			return "Mail Spam";
		}
	}

}
