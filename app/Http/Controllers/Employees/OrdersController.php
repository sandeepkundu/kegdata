<?php

namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentOrderRepository as Orders;
use Excel as Excel;
use Carbon\Carbon as Carbon;
use App\Http\Requests\OrderFormRequest;
class OrdersController extends Controller
{
    /**
     * ----------------------------
     * Employees Orders Controller
     * ----------------------------
     *
     * This controller manages the display of Orders
     * and updating the order status
     *
     *
     */

    protected $layout = 'layouts.dashboard';
    /**
     * Create a new OrdersController instance
     * Create the addresses of the orders on 2nd tab of excel file
     */
    public function __construct(Orders $orders){
        $this->orders = $orders;


    }

    /**
     * Get all the orders and return the order list view
     */
    public function getOrders(Request $request, $tab = 'order'){
        $orders = $this->orders->getAllForEmployees();


        return view($this->layout, ['content'=> view('employees.orders')
            ->withTab($tab)
            ->withRequest($request)
            ->withOrders($orders)]);
    }


    /**
     * Get a single order and display the details
     */
    public function getOrder(Request $request, $id, $tab = 'order'){

        $order = $this->orders->withProducts($id);

        $subPrice = 0;

        if(!is_null($order->subscription)){
            if($order->subscription->plan->is_quantity){
                $subPrice = $order->subscription->plan->base_price * $order->subscription->quantity;
            }else{
                $subPrice = $order->subscription->plan->price;
            }
        }

        return view($this->layout, ['content'=> view('employees.order')
            ->withTab($tab)
            ->withRequest($request)
            ->withOrder($order)->with('subPrice', $subPrice)]);
    }

    /**
     * Update the order status
     */
    public function updateOrder(Request $request){
        $this->validate($request, [
            'id' => 'required|integer',
            'status' => 'required|in:new,charged,fulfilled,processing,returned,canceled'
            ]);

        $order = $this->orders->find($request->id);
        $order->status = $request->status;
        $order->save();
        return redirect('employee/orders');
    }
    /**
    *@method getOrderForm(Request $request,$id)
    *return the order form view
    *
    *@return view*/

    public function getOrderForm(Request $request, $id){
        $order = $this->orders->withProducts ($id);
        return view($this->layout,['content'=>view($id)
          ->withTab($tab)
          ->withOrders($orders)]);
    }


    /**
     * @method getOrdersExcel
     * Export $orders model to an excel document
     *
     * @return Excel
     */
    public function getOrdersExcel()
    {
        $now = Carbon::now();

        $orders = $this->orders->getAllForEmployees();






        /**
        *this shoud create first tab in spreadsheet with order information
        */

        Excel::create('KegData'.$now, function ($excel) use ($orders) {
            $excel->sheet('First sheet', function ($sheet) use ($orders) {
                $sheet->loadView('employees.excel.excel-orders')
                ->withOrders($orders)
                ->with('sheetTitle','Orders')
                ->freezeFirstRow()
                ->setAutoFilter();

            }); /* Close $excel->sheet */

            /*this shoud create second tab in spreadsheet with order address information*/
            $excel->sheet('Second sheet', function ($sheet) use ($orders) {
                $sheet->loadView('employees.excel.excel-orderaddress')
                ->withOrders($orders)
                ->with('sheetTitle','Order-Address')
                ->freezeFirstRow()
                ->setAutoFilter();
            }); /* Close $excel->sheet */


        })->export('xlsx'); /* Close Excel::create(...)*/

        // return view('employees.excel.excel-orders')->with($orders)->with('sheetTitle', 'My Sheet');
        // return view('employees.excel.excel-ordersaddress')->with($orders)->with('sheetTitle', 'My Sheet');
        /*End Public function getUserExcel() */
    }


    /**
        *this shoud delete order 
    */

    public function deleteOrder(Request $request, $id, $tab = 'order')
    {

        if($this->orders->deleteOrder($id)){
            return redirect('employee/orders');
        }else{
            $messages = new MessageBag;
            $messages->add('Store Order', 'Apologies, but it appears deleting the order failed.');
            return redirect('employee/orders')->withErrors($messages)->withInput();
        }
    }



}
