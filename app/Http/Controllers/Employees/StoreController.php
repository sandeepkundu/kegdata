<?php namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\EmpCategoryFormRequest;
use App\Http\Requests\EmpPromotionFormRequest;
use App\Http\Requests\EmpTaxFormRequest;
use App\Http\Requests\EmpPlanFormRequest;
use App\Http\Requests\EmpProductFormRequest;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\KegData\Repositories\EloquentPromotionRepository as Promotion;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentTaxRepository as Tax;
use App\KegData\Repositories\EloquentProductRepository as Product;
use App\KegData\Models\Timezone as Timezone;
use Illuminate\Contracts\Auth\Guard;

class StoreController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Store Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the employees store area and handles updating
	| the database with the store items.
	|
	|
	*/
	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth, Timezone $timezone, Category $category, Promotion $promotion, Tax $tax, Product $product, Plan $plan)
	{

		$this->middleware('auth');
		$this->middleware('role');
		$this->auth = $auth;
		$this->category = $category;
		$this->promotion = $promotion;
		$this->tax = $tax;
		$this->product = $product;
		$this->plan = $plan;
		$this->accountSettings = $this->auth->user()->account->accountSetting->first();
		$this->timezone = ($this->accountSettings->timezone == 0 ) ? $timezone->where('timezone', '=', 'UTC')->first() : $timezone->find($this->accountSettings->timezone);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request, $tab = 'store')
	{
		return view($this->layout, ['content'=> view('employees.store')->withTab($tab)->withRequest($request)]);
	}


	/**
	* Show the Store Category dashboard to the user
	*
	* @return Response
	*/
	public function categoryIndex(Request $request, $id = 0, $tab = 'store'){
		return view($this->layout, ['content' => view('employees.categories')->withTab($tab)->withRequest($request)->with('id',$id)->withTimezone($this->timezone->timezone)]);
	}

	public function storeCategory(EmpCategoryFormRequest $request){
		$input = $request->input();
		$image = $request->file('image');
		if($this->category->createCategory($input, $image)){
			return redirect('employee/store/category');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Category', 'Apologies, but it appears adding a category failed.');
			return redirect('employee/store/category')->withErrors($messages)->withInput();
		}
	}

	public function updateCategory(EmpCategoryFormRequest $request, $id){
		$input = $request->input();
		$image = $request->file('image');
		if($this->category->updateCategory($input, $image, $id)){
			return redirect('employee/store/category');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Category', 'Apologies, but it appears editing the category failed.');
			return redirect('employee/store/category/'.$id)->withErrors($messages)->withInput();
		}
	}

	public function deleteCategory(Request $request, $id, $tab = 'store'){
		if($this->category->deleteCategory($id)){
			return redirect('employee/store/category');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Category', 'Apologies, but it appears deleting the category failed.');
			return redirect('employee/store/category')->withErrors($messages)->withInput();
		}
	}

	/**
	* Show the Store Tax dashboard to the user
	*
	* @return Response
	*/
	public function taxIndex(Request $request, $id = 0, $tab = 'store'){
		return view($this->layout, ['content' => view('employees.tax')->withTab($tab)->withRequest($request)->with('id',$id)]);
	}

	public function storeTax(EmpTaxFormRequest $request){
		$input = $request->input();
		if($this->tax->createTax($input)){
			return redirect('employee/store/tax');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Tax', 'Apologies, but it appears adding a tax failed.');
			return redirect('employee/store/tax')->withErrors($messages)->withInput();
		}
	}

	public function updateTax(EmpTaxFormRequest $request, $id){
		$input = $request->input();
		if($this->tax->updateTax($input, $id)){
			return redirect('employee/store/tax');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Tax', 'Apologies, but it appears editing the tax failed.');
			return redirect('employee/store/tax/'.$id)->withErrors($messages)->withInput();
		}
	}

	public function deleteTax(Request $request, $id, $tab = 'store'){
		if($this->tax->deleteTax($id)){
			return redirect('employee/store/tax');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Tax', 'Apologies, but it appears deleting the tax failed.');
			return redirect('employee/store/tax')->withErrors($messages)->withInput();
		}
	}

	/**
	* Show the Store Plans dashboard to the user
	*
	* @return Response
	*/
	public function planIndex(Request $request, $id = 0, $tab = 'store'){
		return view($this->layout, ['content' => view('employees.plans')->withTab($tab)->withRequest($request)->with('id',$id)->with('timezone', $this->timezone)]);
	}

	public function storePlan(EmpPlanFormRequest $request){
		$input = $request->input();
		$image = $request->file('image');
		if($this->plan->createPlan($input, $image)){
			return redirect('employee/store/plan');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Plan', 'Apologies, but it appears adding a planIndex failed.');
			return redirect('employee/store/plan')->withErrors($messages)->withInput();
		}
	}

	public function updatePlan(EmpPlanFormRequest $request, $id){
		$input = $request->input();
		$image = $request->file('image');
		if($this->plan->updatePlan($input, $image, $id)){
			return redirect('employee/store/plan');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Plan', 'Apologies, but it appears editing the plan failed.');
			return redirect('employee/store/plan/'.$id)->withErrors($messages)->withInput();
		}
	}

	public function deletePlan(Request $request, $id, $tab = 'store'){
		if($this->plan->deletePlan($id)){
			return redirect('employee/store/plan');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Plan', 'Apologies, but it appears deleting the plan failed.');
			return redirect('employee/store/plan')->withErrors($messages)->withInput();
		}
	}

	/**
	* Show the Store Products dashboard to the user
	*
	* @return Response
	*/
	public function productIndex(Request $request, $id = 0, $tab = 'store'){
		return view($this->layout, ['content' => view('employees.products')->withTab($tab)->withRequest($request)->with('id',$id)]);
	}

	public function storeProduct(EmpProductFormRequest $request){
		$input = $request->input();
		$image = $request->file('image');
		if($this->product->createProduct($input, $image)){
			return redirect('employee/store/product');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Product', 'Apologies, but it appears adding a product failed.');
			return redirect('employee/store/product')->withErrors($messages)->withInput();
		}
	}

	public function updateProduct(EmpProductFormRequest $request, $id){
		$input = $request->input();
		$image = $request->file('image');
		if($this->product->updateProduct($input, $image, $id)){
			return redirect('employee/store/product');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Product', 'Apologies, but it appears editing the product failed.');
			return redirect('employee/store/product/'.$id)->withErrors($messages)->withInput();
		}
	}

	public function deleteProduct(Request $request, $id, $tab = 'store'){
		if($this->product->deleteProduct($id)){
			return redirect('employee/store/product');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Product', 'Apologies, but it appears deleting the product failed.');
			return redirect('employee/store/product')->withErrors($messages)->withInput();
		}
	}

	/**
	* Show the Store Promotions dashboard to the user
	*
	* @return Response
	*/
	public function promotionIndex(Request $request, $id = 0, $tab = 'store'){
		return view($this->layout, ['content' => view('employees.promotions')->withTab($tab)->withRequest($request)->with('id',$id)->with('timezone', $this->timezone->timezone)]);
	}

	public function storePromotion(EmpPromotionFormRequest $request){
		$input = $request->input();
		$image = $request->file('image');
		if($this->promotion->createPromotion($input, $this->timezone->timezone, $image)){
			return redirect('employee/store/promotion');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Promotion', 'Apologies, but it appears adding a promotion failed.');
			return redirect('employee/store/promotion')->withErrors($messages)->withInput();
		}
	}

	public function updatePromotion(EmpPromotionFormRequest $request, $id){
		$input = $request->input();
		$image = $request->file('image');

		if($this->promotion->updatePromotion($input, $this->timezone->timezone, $image, $id)){
			return redirect('employee/store/promotion');
		}else{
			$messages = new MessageBag;
			$messages->add('StorePromotion', 'Apologies, but it appears editing the promotion failed.');
			return redirect('employee/store/promotion/'.$id)->withErrors($messages)->withInput();
		}
	}

	public function deletePromotion(Request $request, $id, $tab = 'store'){
		if($this->promotion->deletePromotion($id)){
			return redirect('employee/store/promotion');
		}else{
			$messages = new MessageBag;
			$messages->add('Store Promotion', 'Apologies, but it appears deleting the promotion failed.');
			return redirect('employee/store/promotion')->withErrors($messages)->withInput();
		}
	}

}
