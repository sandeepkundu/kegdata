<?php namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller; 
use App\Http\Requests\EmpHubFormRequest;
use App\KegData\Repositories\EloquentHubDeviceRepository as Hub;

class HubController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Hub $hub)
	{
		//$this->middleware('auth');
		$this->middleware('auth');
		$this->middleware('role');
		$this->hub = $hub;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request, $tab = 'hub')
	{
		return view($this->layout, ['content'=> view('employees.hub')]);
	}

	public function hubsByAccount(Request $request,$accountID){
		$tab = 'hub';
		return view($this->layout, ['content' => view('employees.hub')->withTab($tab)->with('accountID', $accountID)]);
	}

	public function postAccount(Request $request){
		$tab = 'hub';
		$input = $request->all();
		return redirect('employee/hub/account/'.$input['selectAccount']);
	}

	public function addHub(EmpHubFormRequest $request){
		$input = $request->input();
		if($this->hub->createOrUpdate($input)){
			return redirect('employee/hub/account/'.$input['account_id']);
		}else{
			$messages = new MessageBag;
			$messages->add('Hub Devices', 'Apologies, but it appears adding a hub device failed.');
			return redirect('employee/hub/account/'.$input['account_id'])->withErrors($messages)->withInput();
		}
	}
}
