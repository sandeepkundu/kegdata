<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpUserFormRequest;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\AccountUserRepository as AccountUser;
use Excel as Excel;
use Carbon\Carbon as Carbon;
use Illuminate\Support\MessageBag;
use Illuminate\Contracts\Auth\Guard;
use Response;
class UsersController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your applicatio


    ree to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

        /**
         * Create a new controller instance.
         */
        public function __construct(Guard $auth, AccountUser $accountUser, User $user, Account $account)
        {
            $this->middleware('auth');
            $this->auth = $auth;
            $this->middleware('role');

            $this->accountUser = $accountUser;
            $this->user = $user;
            $this->account = $account;
        }



		/**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
        public function index(User $user)
        {
            $user = $user->find(1);
            dd($user);
        }


        public function updateUser(EmpUserFormRequest $request)
        {

            $input = $request->input();

            if ($this->accountUser->empUpdateUser($input)) {
            //$messages = new MessageBag();
            // $messages->add('Users', 'Apologies, but it appears editing a user has failed.');
            //->withSuccess($messages)
                return redirect('employee/user');


            } else {
                $messages = new MessageBag();
                $messages->add('Users', 'Apologies, but it appears editing a user has failed.');

                return redirect('employee/hub/user/'.$input['user_id'])->withErrors($messages)->withInput();
            }

        }

        public function updateUserHub(EmpUserFormRequest $request)
        {

            $input = $request->input();

            if ($this->accountUser->empUpdateUserHub($input)) {
                return redirect('employee/hub');


            } else {
                $messages = new MessageBag();
                $messages->add('Users', 'Apologies, but it appears editing a user has failed.');

                return redirect('employee/hub'.$input['user_id'])->withErrors($messages)->withInput();
            }

        }

        public function destroyUserHub($id){
        $deletedUser = $this->accountUser->destroyUserHubDuplicate($id, $this->auth->user()->account_id);
       
        if( $deletedUser == 1 ){
            $deleted = ['Success' => true, 'id' => $id];
            \Session::flash('msg','User deleted successfully!');
           return redirect('employee/hub');
        }else{
            $deleted = ['Success' => $deletedUser, 'id' => $id];
            //return Response::json($deleted);
            \Session::flash('msg','Cannot delete the user!');
           return redirect('employee/hub');
        }

    }

    /**
     * @method getUserExcel
     * Export $users model to an excel document
     *
     * @return Excel
     */
    public function getUserExcel()
    {
        $now = Carbon::now();
        $users = $this->user->all();
        $accounts = $this->account->all();

        //$users = $this->user->sort('lastName')->get();

        Excel::create('KegData'.$now, function ($excel) use ($users, $accounts) {
            $excel->sheet('First sheet', function ($sheet) use ($users) {
                $sheet->loadView('employees.excel.excel-users')
                ->withUsers($users)
                ->with('sheetTitle','Users')
                ->freezeFirstRow()

                ->setAutoFilter();

            }); /* Close $excel->sheet */

            $excel->sheet('Second sheet', function ($sheet) use ($accounts) {
                $sheet->loadView('employees.excel.excel-accounts')
                ->withAccounts($accounts)
                ->with('sheetTitle','Accounts')
                ->freezeFirstRow()
                ->setAutoFilter();
            }); /* Close $excel->sheet */

        })->export('xlsx'); /* Close Excel::create(...)*/

        // return view('employees.excel.excel-users')->with($users);
        // return view('employees.excel.excel-accounts')->with('accounts', $accounts)->with('sheetTitle', 'My Sheet');

    }/*End Public function getUserExcel() */

    /**
     * @method getUserExcel on Hub
     * Export $users model to an excel document
     *
     * @return Excel
     */

    public function getUserExcelHub()
    {
        $now = Carbon::now();
        $users = $this->user->all();
        $accounts = $this->account->all();

        //$users = $this->user->sort('lastName')->get();

        Excel::create('KegData'.$now, function ($excel) use ($users, $accounts) {
            $excel->sheet('First sheet', function ($sheet) use ($users) {
                $sheet->loadView('employees.excel.excel-user')
                ->withUsers($users)
                ->with('sheetTitle','Users')
                ->freezeFirstRow()

                ->setAutoFilter();

            }); /* Close $excel->sheet */

            $excel->sheet('Second sheet', function ($sheet) use ($accounts) {
                $sheet->loadView('employees.excel.excels-accounts')
                ->withAccounts($accounts)
                ->with('sheetTitle','Accounts')
                ->freezeFirstRow()
                ->setAutoFilter();
            }); /* Close $excel->sheet */

        })->export('xlsx'); /* Close Excel::create(...)*/

        // return view('employees.excel.excel-users')->with($users);
        // return view('employees.excel.excel-accounts')->with('accounts', $accounts)->with('sheetTitle', 'My Sheet');

    }/*End Public function getUserExcel() */


    /**
     * @method getUserExcel
     * Export $users model to an excel document
     *
     * @return Excel
     */
    public function getVerticalUserExcel()
    {
        $now = Carbon::now();

        $users = $this->user->all();
        $accounts = $this->account->all();

        //$users = $this->user->sort('lastName')->get();

        Excel::create('KegData'.$now, function ($excel) use ($users, $accounts) {
            $excel->sheet('First sheet', function ($sheet) use ($users) {
                $sheet->loadView('employees.excel.verticaltestexcel-users')
                ->withUsers($users)
                ->with('sheetTitle','Users')
                ->freezeFirstRow()
                ->freezeFirstColumn()
                ->setAutoFilter()
                ->getStyle('B1:AI1')
                ->getAlignment()
                ->setTextRotation(90);






            }); /* Close $excel->sheet */

            // $excel->sheet('Second sheet', function ($sheet) use ($accounts) {
            //     $sheet->loadView('employees.excel.excel-accounts')
            //             ->withAccounts($accounts)
            //             ->with('sheetTitle','Accounts')
            //             ->freezeFirstRow()
            //             ->setAutoFilter();
            // }); /* Close $excel->sheet */

        })->export('xlsx'); /* Close Excel::create(...)*/

        // return view('employees.excel.excel-users')->with($users);
        // return view('employees.excel.excel-accounts')->with('accounts', $accounts)->with('sheetTitle', 'My Sheet');

    }  /*End Public function getUserExcel() */

}
