<?php namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller; 
use App\Http\Requests\EmpHubFormRequest;
use App\KegData\Repositories\EloquentHubDeviceRepository as Hub;
use App\KegData\Models\HubDevice as HubDevice;
use App\KegData\Models\KegType as KegType;


class EmployeeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function __construct(Hub $hub,HubDevice $hubdevice,KegType $kegtype)
	{
		//$this->middleware('auth');
		$this->middleware('auth');
		$this->middleware('role');
		$this->hub = $hub;
		$this->hubdevice = $hubdevice;
		$this->kegtype = $kegtype;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request, $tab = 'hub')
	{	
		
		$request_order=(isset($_GET['order'])) ? $_GET['order'] : 'firstName';
		$order_by=(isset($_GET['terms'])) ? $_GET['terms'] : 'dsc';
		
		$types = $this->kegtype->where("kegTypeMeasurement","=","oz")->get(['id', 'kegTypeDesc','adc_full','emptyValue']);

		$typesForSelect = array('' => 'Choose Type of Keg') + $types->lists('kegTypeDesc', 'id');
		
        
		return view($this->layout, ['content'=> view('employees.employee')->withTab($tab)->with('keg',$typesForSelect)->with('kegType',$types)->with('order_by',$order_by)]);
	}

	public function hubsByAccount(Request $request,$accountID){
		$tab = 'hub';
		$request_order=(isset($_GET['order'])) ? $_GET['order'] : 'firstName';
		$order_by=(isset($_GET['terms'])) ? $_GET['terms'] : 'dsc';
		return view($this->layout, ['content' => view('employees.employee')->withTab($tab)->with('accountID', $accountID)->with('order_by',$order_by)]);
	}

	public function postAccount(Request $request){
		$tab = 'hub';
		$input = $request->all();
		return redirect('employee/hub/account/'.$input['selectAccount']);
	}

	public function updatekegtype(Request $request){
		$input = $request->input();
		$id = $input['id'];
		$kegtype = $this->kegtype::find($id);
		$kegtype->adc_full = $input['adc_full'];
		$kegtype->emptyValue = $input['emptyValue'];
		$kegTypeDesc=$kegtype->kegTypeDesc;

		//update for ml also
		$kegforml = $this->kegtype::where('kegTypeDesc','=',$kegTypeDesc)->where('kegTypeMeasurement','=','ml')->first();
		if(isset($kegforml->id)) {
			$kegforml->adc_full = $input['adc_full'];
			$kegforml->emptyValue = $input['emptyValue'];
			$kegforml->save();
		}
		
		if($kegtype->save()){
			\Session::flash('message', 'Keg Type Value Updated Successfully!');
			return redirect('/employee/misc');
		}
		
	}


	public function addHub(EmpHubFormRequest $request){
		$input = $request->input();
		//dd($input);
		if($this->hub->createOrUpdate($input)){
			return redirect('employee/hub/account/'.$input['account_id']);
		}else{
			$messages = new MessageBag;
			$messages->add('Hub Devices', 'Apologies, but it appears adding a hub device failed.');
			return redirect('employee/hub/account/'.$input['account_id'])->withErrors($messages)->withInput();
		}
	}
	//update hub
	 public function updateHub(EmpHubFormRequest $request)
    {
        $input = $request->input();

        //dd($input);

        $hub  = $this->hubdevice->find($input['id']);

        $hubMac=$input['hubMac'];
        if(preg_match("/:/", $hubMac)) {
        	$hubMac=preg_replace("/:/", "", $hubMac);
        }
        //dd($hubMac);

		$hub->hubMac = $hubMac;
		$account_id = $input['account_id'];
		$updated = $hub->save();
	
        if ($updated) {
          
            return redirect('employee/hub/account/'.$account_id);
           
            
        } else {
          $messages = new MessageBag;
			$messages->add('Hub Devices', 'Apologies, but it appears adding a hub device failed.');
			return redirect('employee/hub/account/'.$account_id)->withErrors($messages)->withInput();
		}

    }

     public function deleteHub(Request $request)
    {
        $input = $request->input();

        //dd($input);

        $hub  = $this->hubdevice->find($input['id']);
		$hub->account_id = 0;
		$account_id = $input['account_id'];
		$updated = $hub->save();
	
        if ($updated) {
            return redirect('employee/hub/account/'.$account_id);

        } else {
          $messages = new MessageBag;
			$messages->add('Hub Devices', 'Apologies, but it appears adding a hub device failed.');
			return redirect('employee/hub/account/'.$account_id)->withErrors($messages)->withInput();
		}

    }

    public function addHubByModal(Request $request, $hubname = '') {
    	//dd($request->account_id);
    	//dd($hubname);
    	$account_id=$request->account_id;
    	$input=array('hubMac'=>$hubname,'account_id' => $account_id);
        
    	if($t=$this->hub->createOrUpdate($input)){
    		if(! is_null($account_id)){
            	$hubs = $this->hub->getByAccount($account_id);
	        	$devices = $this->hub->getDeviceByAccount($account_id);
	        }else{
	            $hubs = null;
	            $devices=null;
	        }
			echo json_encode(array('status'=>'success','data' => $input,'hubs' => $hubs,'devices' => $devices));
			die;
		} else {
			echo json_encode(array('status'=>'fail','data' => $input,'devices' => $devices));
			die;
		}
    	
    }


    public function fetchDeviceInfo(Request $request,$hubname = '') {
    	$account_id=$request->account_id;
    	
    	if($account_id) {
    		$hubs = $this->hub->getByAccount($account_id);
	        $devices = $this->hub->getDeviceByAccount($account_id);
        }else{
            $hubs = null;
            $devices=null;
        }
        
        echo json_encode(array('status'=>'success','hubs' => $hubs,'devices' => $devices, 'account_id' => $account_id));
		die;
    	
    }



}
