<?php namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller; 

class ProductsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
		$this->middleware('auth');
		$this->middleware('role');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request, $tab = 'products')
	{
		return view($this->layout, ['content'=> view('employees.employee')->withTab($tab)]);
	}

	public function create(){

	}

	public function store(){

	}

	public function show($product){

	}

	public function edit($product){

	}

	public function update($product){

	}

	public function destroy($product){
		
	}

}
