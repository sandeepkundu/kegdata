<?php namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart as Cart;
use App\KegData\Repositories\EloquentProductRepository as Product;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentPromotionRepository as Promo;
use App\Http\Requests\PromoFormRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\KegData\Models\Account as Account;
use Auth;
use App\KegData\Models\Plan as PlanModel;
class ShopController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Store Controller
	|--------------------------------------------------------------------------
	|
	} This controller registers the "Store" pages and manages
	| the checkout process
	|
	*/

	protected $layout = 'layouts.default';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// These lines should be commented out or removed for public store
		//$this->middleware('auth');
		//$this->middleware('role');
	}

	/**
	 * Show the store to the user
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		return view($this->layout, ['content'=> view('store.store')->withRequest($request)]);
	}
	public function start(Request $request)
	{
		return view($this->layout, ['content'=> view('store.start')->withRequest($request)]);
	}
	/**
	* Show the subscription plans to the user
	*
	* @return Response
	*/
	public function showPlans(Request $request)
	{
		
		return view($this->layout, ['content' => view('store.plans')->withRequest($request)]);
	}

	/**
	* Show the subscription plans to the user
	*
	* @return Response
	*/
	public function showProducts(Request $request, $category)
	{
		
		return view($this->layout, ['content' => view('store.product')->withRequest($request)->withCategory($category)]);
	}

	/**
	* Show the subscription plans to the user
	*
	* @return Response
	*/
	public function showCart(Request $request)
	{

		return view($this->layout, ['content' => view('store.cart')->withRequest($request)]);
	}


	public function addCart(Request $request, Product $product, Plan $plan, $type){

		if($type == 'product'){


			$this->validate($request, [
				'id' => 'required',
				'quantity' => 'required',
				]);
			
		
			
			$item = $product->find($request->id);
			

			/* Code for assign subscryption plan acording to number of coupler 
		    *  automaticaly
		    */
		    //dd($item->category[0]->slug);
			if(($request->quantity) && ($item->category[0]->slug == 'couplers')) {
				if(Cart::instance('subscription')->count() == 0){
					$kegcount=$request->quantity;
					
					if($kegcount){
						if( $kegcount > 2){
							$operator = ">=";
							$count =3;
						}else{
							$operator = "=";
							$count = $kegcount;
						}
						$my_plan =  PlanModel::where('max',$operator, $count)->first();
						
						Cart::instance('subscription')->destroy();
						Cart::instance('subscription')->add($my_plan->id, $my_plan->name,1, $my_plan->price, array('slug'=>$my_plan->slug,'term' => $my_plan->interval, 'is_quantity' => $my_plan->is_quantity, 'quantity_price' => $my_plan->quantity_price));
					}

				}
			}
			
			Cart::instance('shopping')->associate('App\KegData\Models\Product')->add($item->id, $item->name, $request->quantity, $item->price);

			
			

			//$content = Cart::content();
			//dd($content);
			
			\Session::flash('msg','Item add in the cart successfully!');
			return redirect()->back();

		}elseif($type == 'plan'){

			$this->validate($request, [
				'id' => 'required|integer',
				]);

			// $item = $plan->find($request->id);
			// Cart::instance('subscription')->destroy();
			// $item_price =  $item->price;
			// Cart::instance('subscription')->associate('App\KegData\Models\Plan')->add($item->id, $item->name,$item_quantity, $item_price, array('term' => $item->interval, 'is_quantity' => $item->is_quantity, 'plan_type' => $item->plan_type ,'quantity_price' => $item->quantity_price));
			$item = $plan->find($request->id);
			Cart::instance('subscription')->destroy();
			Cart::instance('subscription')->add($item->id, $item->name,1, $item->price, array('slug'=>$item->slug,'term' => $item->interval, 'is_quantity' => $item->is_quantity, 'quantity_price' => $item->quantity_price));


			\Session::flash('msg','Item add in the cart successfully!');
			return Redirect::to('/store/plans/#sub-btn');
		}


		//	return redirect()->back();
	}

	public function cancelCart(Request $request){
		Cart::destroy();

		return redirect()->back();
	}

	public function promoCart(PromoFormRequest $request, Promo $promo){
		$input = $request->input();
		$promotion = $promo->where('slug', '=', $input['slug'])->first();
		$now = Carbon::now();
		if($now->lte($promotion->end_date) && $now->gte($promotion->start_date)){
			$cart = Cart::content();
			foreach($cart as $row){
				if($promotion->apply_to_products && !$row->options->has('term') && !$row->options->has('promotion')){
					$newPrice = $row->price*((100-$promotion->percentage)/100);
					Cart::update($row->rowID,  array('price'=>$newPrice, 'options' => array('promotion' => $promotion->slug) ) );
				}

				if($promotion->apply_to_plans && $row->options->has('term') && !$row->options->has('promotion')){
					$newPrice = $row->price*((100-$promotion->percentage)/100);
					Cart::update($row->rowID,  array('price'=>$newPrice, 'options' => array('promotion' => $promotion->slug) ) );
				}
			}
			dd(Cart::content());

		}else{
			$messages = new MessageBag;
			$messages->add('slug', 'This promotion code is not valid.');
			return redirect()->back()->withErrors($messages)->withInput();
		}
	}

	public function removeItem(Request $request, $cart, $rowID){
		Cart::instance($cart)->remove($rowID);

		return redirect()->back();
	}

	public function updateCart(Request $request){
		$input = $request->input();

		foreach($input as $key => $quantity){
			if($key != '_token'){
				$keySplit = explode ( '_', $key );
				if(count($keySplit) == 2){
					$rowID = $keySplit[1];
					$instance = $keySplit[0];
					Cart::instance($instance)->update($rowID, $quantity);
				}


			}

		}

		return redirect()->back();
	}

}
