<?php namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\KegData\Repositories\EloquentOrderRepository as Order;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\Http\Requests\CheckoutFormRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\MessageBag;
use App\Jobs\PurchaseOrder;
use Cart as Cart;
use Carbon\Carbon;
use Mail as Mail;
use App\KegData\Infrastructure\KegDataOrder as KegDataOrder;

class CheckoutController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	protected $layout = 'layouts.default';
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Order $order, Guard $auth, Plan $plan, Category $category, KegDataOrder $kegdataOrder)
	{
		$this->middleware('auth');
		//$this->middleware('role');
		$this->auth = $auth;
		$this->order = $order;
		$this->plan = $plan;
		$this->kegdataOrder = $kegdataOrder;
		$this->category=$category;
	}

	/**
	 * Show Checkout Stage 2 to the user
	 * Addresses, Totals, Etc
	 * Performs basic check on items in cart
	 * @todo Move some of the checks to the Cart Add Method; user must be logged in to perform the checks
	 *
	 * @return Redirect or View
	 */
	public function index(Request $request)
	{

		$messages = new MessageBag;
		$account = $this->auth->user()->account;
		
		// When a user is purchasing equipment, a user must own a system to purchase parts; easiest check is to check for the existence of a hub device
	
		if(is_null($account->hubDevice)){
				
			//If the account does not have a hub device, check to see if a system is in the cart
			$systems = $this->category->with('product')->where('slug', '=', 'systems');
			foreach($systems->product as $product){
				if( Cart::instance('shopping')->search(array('id' => $product->id))){
					$cartItems = Cart::instance('shopping')->search(array('id' => $product->id));
					break;
				}
			}
		}
		

		// An account has to have a subscription
		/**
		 * @todo
		 * A check should be added here for devices vs plan
		 * Only a basic warning can be provided; nothing forced as our calculations could be incorrect
		 * Subscription limits the visibility of the devices reporting
		 */
		$cartPlan = Cart::instance('subscription')->content();
		$plan = $cartPlan->first();

		if(is_null($plan) && is_null($account->plan)){
			
			$messages->add('plan', 'A purchase of a system requires a monthly subscription plan');
		}
	
		if( is_null($account->hubDevice) && is_null($cartItems) ){

			$messages->add('system', 'A purchase of a system is required for Keg Monitoring');
		}
			

		// Checks to see if the user has added the same plan that they are already subscribed to
		/**
		 * @todo
		 * UX: This check would go well in the add plan to cart method in ShopController
		 * if the user is logged in
		 * @todo
		 * UX: On the plan page, when messages exist highlight recommended plans
		 */
		
		/*if(!is_null($plan) && !is_null($account->plan) && $plan->id == $account->plan->id){

			try{
				Cart::instance('subscription')->remove($plan->rowid);
			}catch(\Exception $e){
				if(config('app.debug')){
					\Session::flash("Subscription already selected error:". $e->getMessage(). " Production Message: You are already subscribed to the plan selected. Are you trying to upgrade or downgrade your subscription?", 'danger');
				}else{
					\Session::flash("You are already subscribed to the plan selected. Are you trying to upgrade or downgrade your subscription?", 'danger');
				}

				Cart::instance('subscription')->destroy();
				return redirect('store/plans');
			}

			\Session::flash('msg','You are already subscribed to this plan. If you wish to change the amount of couplers on your plan then please visit the "Plans" page');
			$messages->add('plan', 'You are already subscribed to this plan. If you wish to change the amount of couplers on your plan then please visit the "Plans" page');
		}*/
	
		if($messages->first('system')){
			return redirect('store/systems')->withErrors($messages);
		}elseif($messages->first('plan')){
			return redirect('store/cart')->withErrors($messages);
		}

		return view($this->layout, ['content' => view('store.checkout')->withRequest($request)]);
	}






	/**
	* @method submitPayment
	* Processes the payment information -> Stripe Token
	* Queues an Email  to the user and to sales
	* @return redirect to thank you or back with errors
	*
	*/

	public function submitPayment(CheckoutFormRequest $request){
		
		ini_set('max_execution_time', 0);
		$input = $request->input();
		
		//$order_id = $this->order->submitOrder($input);
		$order = $this->kegdataOrder->handleOrder($input['stripeToken'],
			$this->auth->user(), $this->auth->user()->account,
			$input,  Cart::instance('shopping')->content(), Cart::instance('subscription')->content()->first(), Cart::instance('shopping')->total(2,'.',''), $input['orderID']);

		//dd($order);
		if(isset($order->id)){
			Cart::instance('subscription')->destroy();
			Cart::instance('shopping')->destroy();
			//Queue Email to send for order completion
			//commit by S0
			/*$emailAddress = $this->auth->user()->email;
			$emailData['subPrice'] = 0;
			$emailData['user'] = serialize($this->auth->user());
			$emailData['order'] = serialize($order);

			if(!empty($order->subscription)){
				if($order->subscription->plan->is_quantity){
					$subPrice = $order->subscription->plan->base_price * $order->subscription->quantity;
				}else{
					$subPrice = $order->subscription->plan->price;
				}
				$emailData['subPrice'] = $subPrice;
			}


			Mail::queue('emails.order', $emailData, function($message) use ($emailAddress)
			{
			    $message->to($emailAddress)->bcc('latisha.mcneel@gmail.com')->from('sales@kegdata.com')->subject('Thank you for your purchase with KegData');
			});*/
			return redirect('store/thankyou/'.$order->id);
		}else{
			if(config('app.debug')){
				flash($order['code'].": ".$order['message'], 'danger');
			}else{
				flash($order['message'], 'danger');
			}

			return redirect()->back()->withInput()->with('orderID', $order['order_id']);
		}
	}

	/**
	* @method showOrder
	* Thank you page for placing an order
	* @return View store.thankyou
	*
	*/
	public function showOrder(Request $request, $orderID){

		return view($this->layout, ['content' => view('store.thankyou')->withRequest($request)->with('order_id', $orderID)]);
	}

}
