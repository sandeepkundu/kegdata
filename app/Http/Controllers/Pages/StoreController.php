<?php namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class StoreController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| News Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "News"
	| pages, no redirect if user is authenticated
	| 
	|
	*/

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.default';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the News Page to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view($this->layout, ['content'=> view('pages.store')]);
	}

}

?>