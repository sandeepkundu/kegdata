<?php namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class AboutController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| About Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "About KegData - The Product" and the
	| "About Us - The Company" pages, no redirect if user is authenticated
	| 
	|
	*/

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.default';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the About KegData Page to the user.
	 *
	 * @return Response
	 */
	public function indexAboutProduct()
	{
		return view($this->layout, ['content'=> view('pages.aboutKegData')]);
	}

	/**
	 * Show the About Us Page to the user.
	 *
	 * @return Response
	 */
	public function indexAboutCompany()
	{
		return view($this->layout, ['content'=> view('pages.aboutUs')]);
	}

}

?>