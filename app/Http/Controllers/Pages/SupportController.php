<?php namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class SupportController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Support Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "Support" and the
	| "FAQ" pages, no redirect if user is authenticated
	| 
	|
	*/

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.default';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Support Page to the user.
	 *
	 * @return Response
	 */
	public function support()
	{
		return view($this->layout, ['content'=> view('pages.support')]);
	}

	/**
	 * Show the FAQ Page to the user.
	 *
	 * @return Response
	 */
	public function faq()
	{
		return view($this->layout, ['content'=> view('pages.faq')]);
	}



}

?>