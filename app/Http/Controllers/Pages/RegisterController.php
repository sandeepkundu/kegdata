<?php namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentAccountTypeRepository as AccountType;
use Request;

class RegisterController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the :Register for KegData"
	| 
	| 
	|
	*/

	/**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.default';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(AccountType $AccountType)
	{
		$this->accountType = $AccountType;
		//$this->middleware('guest');
	}

	/**
	 * Show the Register for KegData Page to the user.
	 *
	 * @return Response
	 */
	public function index($tab='bar')
	{
		$AccountList = $this->accountType->getForSelect('description', 'type');

		return view($this->layout, ['content'=> view('pages.register.register')->with('tab',$tab)->with('Account_Types', $AccountList)]);
	}

	/**
	 * Posting the side response
	 *
	 * @return Response
	 */
	public function postRegister(Request $request)
	{
		$input = Request::all();

		return redirect('auth/register')->withInput();
	}


}

?>