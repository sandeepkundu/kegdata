<?php namespace App\Http\Controllers\Stripe;

use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;
use Stripe;
use Log;

class WebhooksController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Stripe $stripe)
	{
		//$this->middleware('auth');
		$this->stripe = $stripe;
	}

	public function index(Request $request){
		//Get the event and spawn a job to process it in the background and return a 200 response to stripe

        //Get the post request body of the request
        //$event = $request->getContent();

        $eventID = $request->input('id');

		$this->dispatch(new \App\Jobs\ProcessStripeEvent($eventID));

        $response = Response::make('', 200);
        $response->headers->set('connection', 'close');
        $response->headers->set('content-type','text/plain');
        return $response;
	}

}
