<?php namespace App\Http\Controllers\Scripts;

use Illuminate\Routing\Controller; 
use App\KegData\Repositories\EloquentUserRepository as User;

class UsersController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Scripts/UsersController
	|--------------------------------------------------------------------------
	|
	| This controller is used for scripts that may need to run outside
	| of the normal website. These routes require the user to have a role
	| of Developer.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->middleware('auth');
        $this->middleware('role');

        $this->user = $user;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}


    /**
     * Script to ensure that the email is in all lower case...
     *
     * @return Response
     */
    public function runUserCase()
    {
        $users = $this->user->all();

        foreach($users as $user){
            $email = $user->email;
            $email = strtolower($email);
            $user->email = $email;
            $user->save();
        }

        return "Done!";
    }

    /**
     * Script to generate user api token...
     *
     * @return Response
     */
    public function runUserToken()
    {
        $users = $this->user->all();

        foreach($users as $user){
            do{
                $api = str_random(100);
                $check = $this->user->where('api_token', '=', $api)->count();
            }while($check);

            $user->api_token = $api;
            $user->save();
        }

        return "Done!";
    }
}
