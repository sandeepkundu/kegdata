<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentTimezoneRepository as Timezone;
use Response;

class TimezoneController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Timezone $timezone)
	{
		//$this->middleware('auth');
		$this->timezone = $timezone;
	}

	public function getByRegion($region){
		$timezones = $this->timezone->getByRegion($region);		

		return Response::json($timezones);
	}
}
