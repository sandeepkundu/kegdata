<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use Response;
use DB;
use Exception;

class BrewDistController extends Controller {

	protected $brewDist;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(BrewDist $brewDist)
	{
		//$this->middleware('auth');
		$this->brewDist = $brewDist;
	}

	public function getByColumn($column,$term){
		// Retrieve the user's input and escape it

		$term=trim($term);
		$column=trim($column);
		if(preg_match("/ooooo/", $term)) {
			$term=preg_replace("/ooooo/", " ", $term);
		}

		$zip_code_api_key = 'fT0Q0U9qiKzdLBM8YKxFsZTAaUw8lNIBGJrGEB7XlGa7iXfgINVbMPLWJh555OGL';
		if($column == 'name'){
			$data = $this->brewDist->where('name', 'like', '%'.e($term).'%')->get();
		}else{
			//if this is zipcode then search from zip api
			if($column == 'zipcode') {
				$zip_code_api_url="https://www.zipcodeapi.com/rest/$zip_code_api_key/radius.json/$term/50/mile?minimal";
				//$get_zip_result=file_get_contents($zip_code_api_url);

				$get_zip_result = @file_get_contents($zip_code_api_url);
				//if api fails
			    if ($get_zip_result === FALSE) {
			        $data = $this->brewDist->getByColumn(e($term), e($column));
			    } else { // if api search result
			        $decode_result=json_decode($get_zip_result);
					 $result_zip = $decode_result->zip_codes;
					$data = $this->brewDist->getByColumnByZipArray($result_zip, e($column));	
			    }

			} else { // else search as it was working
				
				$data = $this->brewDist->getByColumn(e($term), e($column));	
			}
					
		}

		return Response::json(array(
			'data'=>$data
		));
	}
}
