<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KegData\Models\KegDevice as Keg;
use Response;
class KegController extends Controller {

	protected $brewDist;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Keg $keg)
	{
		//$this->middleware('auth');
		$this->keg = $keg;
	}

	public function getByID($id){
		// Retrieve the user's input and escape it

		$data = $this->keg->with(['beer', 'distributor.distributorAddress', 'brewery.breweryAddress'])->find($id);
		return Response::json(array(
			'data'=>$data
		));
	}
}
