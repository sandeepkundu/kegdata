<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KegData\Models\HubDevice as HubDevice;
use Response;

class HubController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(HubDevice $hubdevice)
	{
		//$this->middleware('auth');
		$this->hub = $hubdevice;
	}

	public function getByAccountID($id = ''){
		// Retrieve the user's input and escape it
		if(empty($id)){
			$data = $this->hub->all();
		}else{
			$data = $this->hub->where('account_id','=', $id)->get();
		}
		
		return Response::json($data);
	}
}
