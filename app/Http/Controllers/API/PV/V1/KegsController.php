<?php

namespace App\Http\Controllers\API\PV\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentKegDeviceRepository as KegDevice;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use Auth;

class KegsController extends Controller
{

    protected $auth_user;

    public function __construct(){
        $this->auth_user = Auth::guard('api')->user();
    }

    /**
     * @method getTempHistory
     * getTempHistory retreives the temperature history for a keg or
     * an account
     * Request must container a user api token for verification
     *
     * @var $request
     *
     * @return JSON tempHistory
     */
    public function getTempHistory(Request $request){
        $content = json_encode("");
        $status = 200;
        $content_type = "application/json";
        $format = 'MM/DD/YY hh:MM meridian';
        $filters = [];

        $this->validate($request, [
          'type' => array('required', 'string', 'in:keg,account'),
          'id' => array('required_if:type,keg'),
          'take' => array('integer', 'min:0','max:5000'),
          'skip' => array('integer', 'min:0', 'max:5000'),
          'start_date' => array('date', 'date_format:'.$format),
          'end_date' => array('date', 'date_format:'.$format)
        ]);

        $skip = (empty($request->input('skip'))) ? 0 : $request->input('skip');
        $take = (empty($request->input('request'))) ? 0 : $request->input('take');

        if(!empty($request->input('start_date'))){
            $filters['sent_at_start'] = $request->input('start_date');
        }

        if(!empty($request->input('end_date'))){
            $filters['sent_at_end'] = $request->input('end_date');
        }

        if($request->input('type') == 'keg'){

            $keg = KegDevice::where('id', '=', $request->input('id'))
                            ->where('account_id', '=', $this->user->account_id)->first();

            if($keg->count()){
                $tempHistory = KegData::getTempHistory($request->input('id'), $skip, $take, $filters);
                $keg->setRelation('tempHistory', $tempHistory);
            }else{
                return (new Response(json_encode(['id' => 'Invalid Device ID']), 420));
            }

        }

        if($request->input('type') == 'account'){

            $kegs = KegDevice::where('account_id', '=', $this->user->account_id)
                            ->where('showDevice', '=', 1)
                            ->get();

            $kegs = $kegs->each(function($keg) use($skip, $take, $filters){
                $tempHistory = KegData::getTempHistory($keg->id, $skip, $take, $filters);
                $keg->setRelation('tempHistory', $tempHistory);
            });
        }
        dd($kegs);

        return (new Response($kegs->toJson, $status))
              ->header('Content-Type', $content_type);

    }
}
