<?php

namespace App\Http\Controllers\API\PV\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentDeviceNotificationRepository as Notification;
use App\KegData\Repositories\Notification_Send_To as SendTo;
use Auth;

class KegsController extends Controller
{

    protected $auth_user;

    public function __construct(Notification $notification, SendTo $sendTo){
        $this->auth_user = Auth::guard('api')->user();
        $this->notification = $notification;
        $this->sendTo = $sendTo;
    }

    /**
     * @method addEmail
     * adds an email address to a notification
     * spawns a job to send a verification email
     * Request must contain a user api token for verification
     *
     * @var $request
     *
     * @return JSON collection notification or validation errors
     */
    public function addEmail(Request $request){
        $this->validate($request, [
            'api_token' => 'required|exists:users',
            'email' => 'required|email',
            'id' => 'required|numeric',
            'account_id' => 'required|numeric|exists:accounts,id',
            'notification' => array('in:L,E,U,P,T'),
            'value' => array('numeric'),
            'frequency' => array('numeric'),
            'operator' => array('in:<,>')
        ]);
        if($request->input('account_id') == $this->auth_user->account_id){
            $notification = $this->notification->createOrUpdate($request->all(), $this->auth_user->account_id);

            $send_to = $this->sendTo
                        ->where('sendTo', '=', $request->input->email)
                        ->where('type','=','e')
                        ->where('notification_id','=',$notification->id)
                        ->first();

            if(!empty($send_to)){
                return response()->json(['errors' => [
                        'Oops! This email already exists for this notification.'
                ]]);
            }else{
                try{
                    $hash = str_random(32);
                    $send = $this->send_to->create(['email' => $email, 'type' => 'e', 'verificationHash' => $hash]);

                    $send->attach($notification);
                    $send->attach($this->auth_user->account);
                    $send->save();
                }catch(\Exception $e){
                    return response()->json(['errors' => [
                            'Oops! This email could not be added to the notification. Please try again later.'
                    ]]);
                }


                if(!empty($send)){
                    $isVerified = $this->send_to->where('send_to', '=', $send->send_to)->where('account_id', '=', $send->account_id)->whereNotNull('verified_at')->first();
                    if(empty($isVerified)){
                        $emailArray = [
                            'notification' => $notification,
                            'sendTo' => $send,
                            'link' => 'http://kegdata.com/verify/notifications/'.$hash
                        ];

                        Mail::queue(['emails.notificationVerification', 'emails.notificationVerificationText'], $emailArray, function($message) use ($sendTo){
                                $message->to($sendTo->send_to)->subject('Please verify your email for notifications');
                        });

                        return response()->json(['success' => [
                                'You will start to receive this notification at '.$send->send_to.' the next time its triggered. Please use the <strong>\'Add Notification\' Button</strong> to finish adding this notification',
                                'notification' => $notification,
                                'send_to' => $send_to
                        ]]);
                    }else{
                        return response()->json(['success' => [
                                'You should receive an email to verify this address in a few minutes. After you verify this address, you will start to receive this notification at that address when it is triggered. Please use the <strong>\'Add Notification\' Button</strong> to finish adding this notification',
                                'notification' => $notification,
                                'send_to' => $send_to
                        ]]);
                    }

                }
            }
        }else{
            return response()->json(['errors' => [
                    'Oops! Something went wrong. Please refresh the page.'
            ]]);
        }



    }
}
