<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Request;
use Response;
use Carbon\Carbon;
use App\KegData\Infrastructure\KegDataLogger as  KegDataLogger;
use App\KegData\Repositories\EloquentKegDataTransmitRepository as KegDataTransmit;
use SplFileObject;
//use Artisan;
class DataController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(KegDataLogger $kdlogger, KegDataTransmit $kdTransmit)
	{
		//$this->middleware('kegdataagent');
		$this->kdlogger = $kdlogger;
		$this->kdTransmit = $kdTransmit;
	}

	public function uploadData(){
		ini_set('max_execution_time', 300);
		// First we fetch the Request instance
		$request = Request::instance();

		// Now we can get the content from it
		$content = $request->getContent();
		
		$message = $this->kdTransmit->parseContent($content);
		//$consoleResponse = Artisan::call('notify:tempetature');
		$this->kdlogger->write($message);
		$response = Response::make('', 200);
		$response->headers->set('connection', 'close');
		$response->headers->set('content-type','text/plain');
		return $response;
	}

	public function getTime(){
		$time = Carbon::now();
		$response = Response::make($time->toDateTimeString(), 200);
		$response->headers->set('connection', 'close');
		$response->headers->set('content-type','text/plain');
		return $response;
	}

	public function getHubInfo(){
		$filepath = app_path().'/KegData/firmware/';
		$hubinfoFile = 'HubInfo.txt';
		if(file_exists($filepath.$hubinfoFile)){
			$hubinfotxt = fopen($filepath.$hubinfoFile, "r");
			$filename = trim(fgets($hubinfotxt));
			$fwMajorVer = trim(fgets($hubinfotxt));
			$fwMinorVer = trim(fgets($hubinfotxt));
			fclose($hubinfotxt);
			$linecount = 0;

			if(file_exists($filepath.$filename)){
				$hubbin = fopen($filepath.$filename, "r");
				while(!feof($hubbin)){
					$line=fgets($hubbin);
					$linecount++;
				}	
			}else{
				$message = 'Hub Firmware does not exist';
				$this->kdlogger->write($message);
				$response = Response::make($message, 200);
				$response->headers->set('connection', 'close');
				$response->headers->set('content-type','text/plain');
				return $response;
			}

			$content = $filename.PHP_EOL.$fwMajorVer.PHP_EOL.$fwMinorVer.PHP_EOL.$linecount.PHP_EOL;
			$message = 'Request for hub info succeeded';
			$this->kdlogger->write($message);
			$response = Response::make($content, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;
		}else{
			$message = 'Hub Info Does Not Exist';
			$this->kdlogger->write($message);
			$response = Response::make($message, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;	
		}

	}

	public function getSensorInfo(){
		$filepath = app_path().'/KegData/firmware/';
		$sensorinfoFile = 'SensorInfo.txt';
		if(file_exists($filepath.$sensorinfoFile)){
			$sensorinfotxt = fopen($filepath.$sensorinfoFile, "r");
			$filename = trim(fgets($sensorinfotxt));
			$fwMajorVer = trim(fgets($sensorinfotxt));
			$fwMinorVer = trim(fgets($sensorinfotxt));
			$fwCrc = trim(fgets($sensorinfotxt));
			$fwSize = trim(fgets($sensorinfotxt));
			fclose($sensorinfotxt);
			$linecount = 0;

			$content = $filename.PHP_EOL.$fwMajorVer.PHP_EOL.$fwMinorVer.PHP_EOL.$fwCrc.PHP_EOL.$fwSize.PHP_EOL;
			$message = 'Request for sensor info succeeded';
			$this->kdlogger->write($message);
			$response = Response::make($content, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;
		}else{
			$message = 'Sensor Info Does Not Exist';
			$this->kdlogger->write($message);
			$response = Response::make($message, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;	
		}
	}

	public function getHubFirmware($startrow, $numrows){
		$filepath = app_path().'/KegData/firmware/';
		$hubinfoFile = 'HubInfo.txt';
		if(file_exists($filepath.$hubinfoFile)){
			$hubinfotxt = fopen($filepath.$hubinfoFile, "r");
			$filename = trim(fgets($hubinfotxt));
			fclose($hubinfotxt);
			if(file_exists($filepath.$filename)){
				$hubbin = new SplFileObject($filepath.$filename);
				$hubbin->seek($startrow);
				$linecount = 0;
				$content = '';
				while(! $hubbin ->eof() && $linecount < $numrows){
					$content .= $hubbin->current();
					$hubbin->next();
					$linecount++;
				}
				$hubbin = null;
				$message = 'Request for hub info succeeded';
				$this->kdlogger->write($message);
				$response = Response::make($content, 200);
				$response->headers->set('connection', 'close');
				$response->headers->set('content-type','text/plain');
				return $response;

			}else{
				$message = 'Hub Firmware does not exist';
				$this->kdlogger->write($message);
				$response = Response::make($message, 200);
				$response->headers->set('connection', 'close');
				$response->headers->set('content-type','text/plain');
				return $response;
			}
		}else{
			$message = 'Hub Info Does Not Exist';
			$this->kdlogger->write($message);
			$response = Response::make($message, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;	
		}
	}

	public function getSensorFirmware($startbyte, $numbytes){
		$filepath = app_path().'/KegData/firmware/';
		$sensorinfoFile = 'SensorInfo.txt';
		if(file_exists($filepath.$sensorinfoFile)){
			$sensorinfotxt = fopen($filepath.$sensorinfoFile, "r");
			$filename = trim(fgets($sensorinfotxt));
			fclose($sensorinfotxt);

			if(file_exists($filepath.$filename)){
				$sensorbin = fopen($filepath.$filename, 'rb');
				fseek($sensorbin, intval($startbyte, 10));
				$content = fread($sensorbin, $numbytes);
				fclose($sensorbin);
				$message = 'Sensor Firmware Download Succeeded';
				$this->kdlogger->write($message);
				$response = Response::make($content, 200);
				$response->headers->set('connection', 'close');
				$response->headers->set('content-type','text/plain');
				return $response;	
			}else{
				$message = 'Sensor Firmware Does Not Exist';
				$this->kdlogger->write($message);
				$response = Response::make($message, 200);
				$response->headers->set('connection', 'close');
				$response->headers->set('content-type','text/plain');
				return $response;	
			}
		}else{
			$message = 'Sensor Info Does Not Exist';
			$this->kdlogger->write($message);
			$response = Response::make($message, 200);
			$response->headers->set('connection', 'close');
			$response->headers->set('content-type','text/plain');
			return $response;	
		}
	}
}
