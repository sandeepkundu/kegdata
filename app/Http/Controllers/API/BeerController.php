<?php namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\KegData\Models\Beer as Beer;
use Response;

class BeerController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Beer $beer)
	{
		//$this->middleware('auth');
		$this->beer = $beer;
	}

	public function getByName($term = ''){
		// Retrieve the user's input and escape it
		if(empty($term)){
			$data = $this->beer->take(50)->get(['id', 'name']);
		}else{
			$data = $this->beer->where('name', 'LIKE', '%'.e($term).'%')->take(50)->get(['id', 'name']);
		}
		
		return Response::json($data);
	}
}
