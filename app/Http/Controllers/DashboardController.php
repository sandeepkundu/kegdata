<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KegSetupFormRequest;
use App\Http\Requests\SettingsFormRequest;
use App\Http\Requests\NotificationsFormRequest;
use App\Http\Requests\AccountAddressFormRequest;
use App\Http\Requests\EditAccountFormRequest;
use App\Http\Requests\UserFormRequest;
use App\KegData\Repositories\EloquentKegDeviceRepository as KegDevice;
use App\KegData\Repositories\EloquentAccountSettingRepository as AccountSetting;
use App\KegData\Repositories\EloquentDeviceNotificationRepository as DeviceNotification;
use App\KegData\Repositories\AccountUserRepository as AccountUser;
use App\KegData\Models\Timezone as Timezone;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\MessageBag;
use Response;
use Config;
use Excel;
use Carbon\Carbon;
use Artisan;


class DashboardController extends Controller {

	protected $layout = 'layouts.dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth, KegDevice $kegDevice, AccountSetting $accountSetting, DeviceNotification $notifications, AccountUser $accountUser, Timezone $timezone, Carbon $carbon)
	{
		$this->middleware('auth');
		$this->auth = $auth;
		$this->kegdevice = $kegDevice;
		$this->accountSettings = $accountSetting;
		$this->notifications = $notifications;
		$this->accountUser = $accountUser;
		$this->carbon = $carbon;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function index(Request $request, $tab = 'default', $page = 1)
	{
		$input = $request->input();
		//dd($input);
		
		$accountType = $this->auth->user()->account->accountType;
		$data['tab'] = $tab;
		$data['input'] = $input;
		$data['page'] = $page;
		//echo $accountType;
		if($accountType == 'D' || $accountType == 'B')
			$tab = ($tab == 'default') ? 'reports' : $tab;
		elseif($accountType == 'P')
			$tab = 'account';
		else
			$tab = ($tab == 'default') ? 'levels' : $tab;
		

		//redirect condition for rendering levels_newlsm page 
		//when calling dashboard
		if($tab == 'levels') {
			$tab = 'levels_newlsm';
		}
		//end if condition for redirection changes

		//redirect condition for rendering graph_reports_demo page 
		//when calling dashboard
		if($tab == 'graph_reports') {
			$tab = 'graph_reports_demo';
		}
		//end if condition for redirection changes


		switch($accountType){
			case 'D':
				return view($this->layout, ['content' => view('dashboard.distributor.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'B':
				return view($this->layout, ['content'=> view('dashboard.brewery.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'E':
				return view($this->layout, ['content'=> view('dashboard.enterprise.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'P':
				return view($this->layout, ['content'=> view('dashboard.serviceprovider.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'R':
				//return view($this->layout, ['content' => view('dashboard.restaurant.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			ini_set('memory_limit', '-1');
				return view($this->layout, ['content' => view('dashboard.residential.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
			break;
			case 'S':
				ini_set('memory_limit', '-1');
				return view($this->layout, ['content' => view('dashboard.residential.'.$tab)->withTab($tab)->withInput($input)->withPage($page)]);
				//return view($this->layout, ['content' => view('dashboard.residential.'.$tab)->with($data)]);
			break;
		}

		return view($this->layout, ['content' => view('errors.404')])->withTab($tab);
		
	}

	/**
	 * Edit the settings for a keg
	 *
	 * @return Response
	 */

	public function updateKeg(KegSetupFormRequest $request){
		$input = $request->input();
		if($this->kegdevice->update($input)){
			return redirect('/dashboard/kegsetup');
		}else{
			$messages = new MessageBag;
			$messages->add('Keg Setup', 'Apologies, but it appears setting up this keg failed.');
			return redirect('/dashboard/kegsetup')->withErrors($messages)->withInput();
		}
	}



	/**
	 * Update the lsm_m value for a keg
	 *
	 * @return Response
	 */

	public function updatekeglsm($lsm_m) {

		if($lsm_m) {
			$arr=explode("~",$lsm_m);
			$lsm=$arr[0];
			$id=$arr[1];
		} else {
			$lsm=0;
			$id=0;
		}
		if($this->kegdevice->update_lsm($id,$lsm_m)){ 
			echo json_encode(['Success' => true]);
		} else {
			echo json_encode(['Success' => false]);
		}
		die;

		/*if($this->kegdevice->update_lsm($id,$lsm_m)){ 
			$return = ['Success' => true, 'id' => $id];
		} else {
			$return = ['Success' => false, 'id' => $id];
		}
		
		
		return Response::json($return);*/
		
    }

    /**
	 * Update the lsm_m value for a keg
	 *
	 * @return Response
	 */

	public function callingConsoleViaAjax($id=null) {
		//echo json_encode(['Success' => true]);
		if($this->auth->user()->id) {
			Artisan::call('notify:onlyone', [
				'id' => $this->auth->user()->id,
			]);
		}
		
		die('hi');
		
    }

	
	/**
	* Edit the settings for an account
	* @return Response
	*/
	public function updateSettings(SettingsFormRequest $request){
		$input = $request->input();
		if($this->accountSettings->update($input)){
			return redirect('/dashboard/settings');
		}else{
			$messages = new MessageBag;
			$messages->add('Account Settings', 'Apologies, but it appears updating the account settings failed.');
			return redirect('/dashboard/settings')->withErrors($messages)->withInput();
		}
	}

	public function storeNotifications(NotificationsFormRequest $request){

		$input = $request->input();

		if($input['notification'] == 'T') {
			$input['frequency'] = '8';
		}
		
			$this->kegdevice;
		
		$all_keg_notification =$this->kegdevice->where(['account_id'=> $this->auth->user()->account->id])->select('id')->get();
		/*if($input['notification'] == 'P' || $input['notification'] == 'U' ) {
			$input['frequency']='11'; 
		}*/

		// Set notification if user select all kegdevice for notification
		// 2 means all kegdevice
		if($input['kegdevice_id'] == 2){

			$input['sms_notification']=(isset($input['sms_notification'])) ? 1:0;
			$input['email_notification']=(isset($input['email_notification'])) ? 1:0;
			$input['email_notification_optional']=(isset($input['email_notification_optional'])) ? 1:0;

			$input['email_notification_optional_2']=(isset($input['email_notification_optional_2'])) ? 1:0;
			// loop for select and check each keg for notification 
			foreach($all_keg_notification as $mykegdevice){

				if(($input['id']==0) || empty($input['id'])) { 
					$a=$this->notifications->model->where(['kegdevice_id'=>$mykegdevice->id,'notification'=>$input['notification'],'account_id'=>$this->auth->user()->account->id])->get()->count();
					if($a) {

						continue ;
					}
					else{

						$this->notifications->createOrUpdateAll($input, $this->auth->user()->account->id, $mykegdevice );					
					}

				}

			}

			return redirect('/dashboard/notifications');
			
		}
		else{
		$input['sms_notification']=(isset($input['sms_notification'])) ? 1:0;
		$input['email_notification']=(isset($input['email_notification'])) ? 1:0;
		$input['email_notification_optional']=(isset($input['email_notification_optional'])) ? 1:0;
		$input['email_notification_optional_2']=(isset($input['email_notification_optional_2'])) ? 1:0;
	
		//check on save time that there can not add more then one frequency on one notification
		if(($input['id']==0) || empty($input['id'])) { 
			$a=$this->notifications->model->where(['kegdevice_id'=>$input['kegdevice_id'],'notification'=>$input['notification'],'account_id'=>$this->auth->user()->account->id])->get()->count();
			if($a) {
				$messages = new MessageBag;
				$messages->add('Notifications', 'Apologies, but You have already set a frequency upon selected notification type, Please click edit to change the frequency if needed.');
				return redirect('/dashboard/notifications')->withErrors($messages)->withInput();	
			}
			
		}
		
		if($this->notifications->createOrUpdate($input, $this->auth->user()->account->id)){
			return redirect('/dashboard/notifications');
		}else{
			$messages = new MessageBag;
			$messages->add('Notifications', 'Apologies, but it appears creating a new notification failed.');
			return redirect('/dashboard/notifications')->withErrors($messages)->withInput();
		}


	}

	}

	public function destroyNotifications( $id){
		$what = $this->notifications->find($id);
		if($what->delete()){
			$deleted = ['Success' => true, 'id' => $id];
			return Response::json($deleted);
		}else{
			return Response::json('Failed');
		}
	}

	public function updateAddress(AccountAddressFormRequest $request, $id){
		$input = $request->input();
		if($this->accountUser->updateAddress($input, $id)){
			return redirect('/dashboard/account');	
		}else{
			$messages = new MessageBag;
			$messages->add('Account Address', 'Apologies, but it appears we could not update your account at this time. Please try again in a few minutes.');
			return redirect('/dashboard/account')->withErrors($messages)->withInput();
		}
	}

	public function updateUser(UserFormRequest $request, $id){
		$input = $request->input();
		if($this->accountUser->updateUser($input, $id)){
			return redirect('/dashboard/account');
		}else{
			$messages = new MessageBag;
			$messages->add('User', 'Apologies, but it appears updating the user information failed. Please try again in a few minutes.');
			return redirect('/dashboard/account')->withErrors($messages)->withInput();
		}
	}

	public function storeUser(UserFormRequest $request) {
		$input = $request->input();
		if($this->accountUser->storeUser($input, $this->auth->user()->account_id)){
			return redirect('/dashboard/account');
		}else{
			$messages = new MessageBag;
			$messages->add('New User', 'Apologies, but it appears creating a new user has failed. Please try again in a few minutes.');
		}
	}

	public function destroyUser($id){
		$deletedUser = $this->accountUser->destroyUser($id, $this->auth->user()->account_id);
		if( $deletedUser == true ){
			$deleted = ['Success' => true, 'id' => $id];
			return Response::json($deleted);
		}else{
			$deleted = ['Success' => $deletedUser, 'id' => $id];
			return Response::json($deleted);
		}

	}

	public function updateAccount(EditAccountFormRequest $request){
	
		$input = $request->input();
		if($this->accountUser->updateAccount($input, $this->auth->user()->account->id)){
			return redirect('/dashboard/account');
		}else{
			$messages = new MessageBag;
			$messages->add('Update Account', 'Apologies, but it appears we were unable to update this account.Please try again in a few minutes.');
		}
	}

	public function updateSmsEmail(Request $request){
		
		$input = $request->input();
		
		if(isset($input['sms_alert'])) {
			$input['sms_alert'] = preg_replace('/[^A-Za-z0-9\-]/', '', $input['sms_alert']);
			$input['sms_alert'] = preg_replace('/-/', '', $input['sms_alert']);
		}
		
		if($this->accountUser->updateAccount($input, $this->auth->user()->account->id)){
			return redirect('/dashboard/account');
		}else{
			$messages = new MessageBag;
			$messages->add('Update Account', 'Apologies, but it appears we were unable to update this account.Please try again in a few minutes.');
		}
	}
	//action for generate report via excel
	public function getReport(Request $request,$page = 1){
		$input = $request->input();
		$data['page'] = $page;
		$data['input'] = $input;

		//dd($input);
		$accountType = $this->auth->user()->account->accountType;
		$accountName = $this->auth->user()->account->accountName;
		$fileName = $accountName.$this->carbon->now();
		
		Excel::create($fileName, function($excel)  use ($accountType,$input,$data) {
			$excel->sheet('Report', function($sheet) use ($accountType,$input,$data) {
				$sheet->loadView('dashboard.residential.reportsexcel')->withInput($input)->with($data);
				/*switch($accountType){
					case 'D':
						 $sheet->loadView('dashboard.distributor.reports-excel');
					break;
					case 'B':
						 $sheet->loadView('dashboard.brewery.reports-excel');
					break;
					case 'E':
						 $sheet->loadView('dashboard.enterprise.reports-excel');
					break;
					case 'R':
						 $sheet->loadView('dashboard.restaurant.reportsexcel');
					break;
					case 'S':
						 $sheet->loadView('dashboard.residential.reportsexcel')->withInput($input)->with($data);
					break;
				}*/
		    	});
		})->export();
		//return view('dashboard.residential.reportsexcel');
	}

	//action for generate report via excel for pour data
	public function getReportPour(Request $request,$page = 1){
		$input = $request->input();
		$data['page'] = $page;
		$data['input'] = $input;

		//dd($input);
		$accountType = $this->auth->user()->account->accountType;
		$accountName = $this->auth->user()->account->accountName;
		$fileName = $accountName.$this->carbon->now();
		
		Excel::create($fileName, function($excel)  use ($accountType,$input,$data) {
			$excel->sheet('Report', function($sheet) use ($accountType,$input,$data) {
				$sheet->loadView('dashboard.residential.reportsexcelpour')->withInput($input)->with($data);
				/*switch($accountType){
					case 'D':
						 $sheet->loadView('dashboard.distributor.reports-excel');
					break;
					case 'B':
						 $sheet->loadView('dashboard.brewery.reports-excel');
					break;
					case 'E':
						 $sheet->loadView('dashboard.enterprise.reports-excel');
					break;
					case 'R':
						 $sheet->loadView('dashboard.restaurant.reportsexcel');
					break;
					case 'S':
						 $sheet->loadView('dashboard.residential.reportsexcel')->withInput($input)->with($data);
					break;
				}*/
		    	});
		})->export();
		//return view('dashboard.residential.reportsexcel');
	}


	public function getExcel(Request $request){
		$input = $request->input();
		$accountType = $this->auth->user()->account->accountType;
		$accountName = $this->auth->user()->account->accountName;
		$fileName = $accountName.$this->carbon->now();
		/*Excel::create($fileName, function($excel)  use ($accountType) {

			$excel->sheet('Report', function($sheet) use ($accountType) {
				switch($accountType){
					case 'D':
						 $sheet->loadView('dashboard.distributor.reports-excel');
					break;
					case 'B':
						 $sheet->loadView('dashboard.brewery.reports-excel');
					break;
					case 'E':
						 $sheet->loadView('dashboard.enterprise.reports-excel');
					break;
					case 'R':
						 $sheet->loadView('dashboard.restaurant.reportsexcel');
					break;
					case 'S':
						 $sheet->loadView('dashboard.residential.reportsexcel');
					break;
				}
		    	});
		})->export();*/
		return view('dashboard.residential.reportsexcel');
	}
}
