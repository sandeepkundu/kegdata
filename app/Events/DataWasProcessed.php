<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
Use Log;

class DataWasProcessed extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * All public properties will be broadcasted with the event
     */
    public $processed;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->processed = $data;
        Log::info('Event was processed!!!');
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['dataTransmitted'];
    }
}
