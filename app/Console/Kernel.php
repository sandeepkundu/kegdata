<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\LowLevel',
		'App\Console\Commands\KagEmpty',
		'App\Console\Commands\SendAllEmail',
		'App\Console\Commands\OnlyOneNotification',
		'App\Console\Commands\Temperature'

	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();
	 
		//low level means this cron job will run daily for all notification type
		$schedule->command('notify:lowlevel')
				 ->dailyAt('17:00');
		
		//keg empty means this cron job will run daily for all notification type weekly		 
		$schedule->command('notify:kagempty')
				 ->weekly()
				 ->mondays()
				 ->at('17:00');

		$schedule->command('notify:sendallemail')
				 ->everyTenMinutes();
		//this cron job will run only one tome  for all notification 	
		$schedule->command('notify:onlyone')
				 ->dailyAt('17:00');
		// this cron job will  run for only temperature 
		//$schedule->command('notify:tempetature')
				// ->hourly();

	}

}
