<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//use DB;
use Mail;
use App\KegData\Models\User;
use App\KegData\Models\Account;
use App\KegData\Models\Device_Notification;
use App\KegData\Models\Brewery_Distributor;
use App\KegData\Models\AccountSetting;
use App\KegData\Models\EmailTemplate;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\KegData as kegdataview;

use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class Temperature extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notify:tempetature';
	public $frequency='8';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cron for only temperature';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		
		$users = User::where(['isActive'=> 1,'deleted_at'=>null]) ->groupBy('account_id')->get();

		foreach ($users as $user)
		{
			$user_id=$user->id;
			$account=$user->account;
			$account_id=$account['id'];


			//fetch account settings
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$volumeType=$accountSettings['volumeType'];

			$accountType = $account['accountType'];

			$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);



			$keg=new Keg;
			$kegData=new KegData;
			//$kegs = $keg->getForNotifications($account_id, 'account_id');

			
			if($account_id) { // check if there is account id

				//fetch device notification for frequency weekly
				$temp_notification_row=Device_Notification::where(['account_id'=>$account_id,'frequency'=>$this->frequency,'deleted_at'=>null])->first();



				

				if(!empty($temp_notification_row['id'])) { //check if user set tempreture notification

					//fetch users kegs for fetch level
					$kegs  = $keg->getForLevels($account_id, 'account_id', $account_id);
					foreach($kegs as $keg) { //keg loop start of user

						

					    //calling fetch tempreture function if it is set
						$this->fetch_temperature($keg,$account_id,'T',$kegData,$volumeType,$user_id,$accountSettings,$timezone);



						
					} // end foreach for keg

				}


				
			} else { // end if 
				//echo 'there is no account set';
				//echo '\n\n\n';
			}  

		} // end user foreach


		$this->info('The low level notifications were sent successfully!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		//	['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}



	


	/**
	 * Create own defined function for fetch Temperature
	 *
	 * @return array
	 */

	protected function fetch_temperature($keg=array(),$account_id=0,$notification='T',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];
			$recent_pour_device_id=$keg->recentPour->deviceID;

			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
			
			
			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			//echo "=====>".$this->frequency;
			//echo "<========";


			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				
				$email_notification=$check_device_exist_in_notification['email_notification'];

				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				
				// query to get last inserted record date time for email
				$emailData = EmailTemplate::select('created_at')->where('cron_name','TemperatureAPI')->where('type','email')->where('device_id',$kegdevice_id)->orderBy('id', 'desc')->first();
				if($emailData){

					$date = Carbon::parse($emailData->created_at);
					$now = Carbon::now();
					$diffForEmail = $date->diffInMinutes($now);
				} else { // if comes first times
					$diffForEmail = 250;
				}

				//query to get last inserted record date time for sms
				$smsData = EmailTemplate::select('created_at')->where('cron_name','TemperatureAPI')->where('type','sms')->where('device_id',$kegdevice_id)->orderBy('id', 'desc')->first();
				if($smsData){
					$smsdate = Carbon::parse($smsData->created_at);
					$now = Carbon::now();
					$diffForSms = $smsdate->diffInMinutes($now);
				} else{ // if comes first time
					$diffForSms = 250;
				}
				//starting formulas for fetching keg tempreture
				if(!is_null($keg->temperature)){

					$keg->temperature->tempFormatted = $objectkegData->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);
					$fetched_tempreture=$keg->temperature->tempFormatted;

	              //echo '===>operator is '.$operator .' value is '.$value.' temperature is '.$fetched_tempreture;
					if($operator == '>') {
						if($fetched_tempreture > $value) {

							if($email_notification==1) {

								// save into email template
								$cureent_date =date('M d, Y g:i a');
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'email';
								$EmailTemplate->subject = "High Level Alert!";
								$EmailTemplate->cron_name = "TemperatureAPI";
								$EmailTemplate->device_id = $kegdevice_id;
								if($diffForEmail > 240){
									$EmailTemplate->save();

									echo 'insert into email_template successfully for Temperature Alert!';
									echo "========> ";
								}
							} 

							if($sms_notification==1) {

							// save into email template
								$cureent_date =date('M d, Y g:i a');
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'sms';
								$EmailTemplate->subject = "Temperature Alert!";
								$EmailTemplate->cron_name = "TemperatureAPI";
								$EmailTemplate->device_id = $kegdevice_id;
								if($diffForSms > 240) {

									$EmailTemplate->save();
									echo 'insert into email_template successfully for Temperature Alert!';
									echo "========> ";
								}
							} 

						} 
				} else if($operator == '<') { // else set < lesss then

					if($value > $fetched_tempreture) {

						if($email_notification==1) {

								// save into email template
							$cureent_date =date('M d, Y g:i a');
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'email';
							$EmailTemplate->subject = "Temperature Alert!";
							$EmailTemplate->cron_name = "TemperatureAPI";
							$EmailTemplate->device_id = $kegdevice_id;
							if($diffForEmail > 240){
								$EmailTemplate->save();

								echo 'insert into email_template successfully for Temperature Alert!';
								echo "========> ";
							}
						} 	

						if($sms_notification==1) {

								// save into email template
							$cureent_date =date('M d, Y g:i a');
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'sms';
							$EmailTemplate->subject = "Temperature Alert!";
							$EmailTemplate->cron_name = "TemperatureAPI";
							$EmailTemplate->device_id = $kegdevice_id;
							if($diffForSms > 240) {
								$EmailTemplate->save();

								echo 'insert into email_template successfully for Temperature Alert!';
								echo "========> ";
							}
						} 

					}
				}

			}


		}


	}

}

}






