<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//use DB;
use Mail;
use App\KegData\Models\User;
use App\KegData\Models\Account;
use App\KegData\Models\Device_Notification;
use App\KegData\Models\Brewery_Distributor;
use App\KegData\Models\AccountSetting;
use App\KegData\Models\EmailTemplate;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\KegData as kegdataview;

use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon as Carbon;

/*use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Models\Notification_Frequency as Freq;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;*/


class LowLevel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notify:lowlevel';
	public $frequency='11';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cron job will run once a day for all notification type';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$users = User::where(['isActive'=> 1,'deleted_at'=>null]) ->groupBy('account_id')->get();

		foreach ($users as $user)
		{
			$user_id=$user->id;
			$account=$user->account;
			$account_id=$account['id'];


			//fetch account settings
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$volumeType=$accountSettings['volumeType'];

			$accountType = $account['accountType'];

			$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);



			$keg=new Keg;
			$kegData=new KegData;
			//$kegs = $keg->getForNotifications($account_id, 'account_id');

			
			if($account_id) { // check if there is account id

				//fetch device notification for frequency Once a day
				$temp_notification_row=Device_Notification::where(['account_id'=>$account_id,'frequency'=>'11','deleted_at'=>null])->first();
				

				if(!empty($temp_notification_row['id'])) { //check if user set tempreture notification

					//fetch users kegs for fetch level
					$kegs  = $keg->getForLevels($account_id, 'account_id', $account_id);
					foreach($kegs as $keg) { //keg loop start of user

						//calling fetch level function if it is set
						$this->fetch_level($keg,$account_id,'L',$kegData,$volumeType,$user_id,$accountSettings,$timezone);

						//calling fetch empty function if it is set
						$this->fetch_empty($keg,$account_id,'E',$kegData,$volumeType,$user_id,$accountSettings,$timezone);

					    //calling fetch tempreture function if it is set
						$this->fetch_temperature($keg,$account_id,'T',$kegData,$volumeType,$user_id,$accountSettings,$timezone);

					    //calling fetch untap event function if it is set
						$this->fetch_untap($keg,$account_id,'U',$kegData,$volumeType,$user_id,$accountSettings,$timezone);

					    //calling pour conducted after hour event function if it is set
						$this->after_hour($keg,$account_id,'P',$kegData,$volumeType,$user_id,$accountSettings,$timezone);

						
					} // end foreach for keg

				}


				
			} else { // end if 
				//echo 'there is no account set';
				//echo '\n\n\n';
			}  
			
		} // end user foreach


		$this->info('The low level notifications were sent successfully!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

	/**
	 * Create own defined function for fetch level
	 *
	 * @return array
	 */

	protected function fetch_level($keg=array(),$account_id=0,$notification='L',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];

			$recent_pour_device_id=$keg->recentPour->deviceID;
			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
			
			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				//starting formulas for fetching keg percentage
				$keg->recentPour->ounces =  $objectkegData->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $volumeType);

				if($keg->recentPour->ounces  < 0){
					$keg->recentPour->ounces = 0;
				}

				$keg->recentPour->total = $keg->recentPour->ounces.' '.$volumeType;

				$keg->recentPour->percentage =  $objectkegData->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);

                 //echo "account_id _id==>".$account_id."<=======";
				 //echo "====>".$keg->recentPour->percentage; /// fetch level of every user keg

				$percentage=$keg->recentPour->percentage; /// fetch level of every user keg

				if($operator == '>') {

					$cur_date = Carbon::now();
					$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y g:i a');	

					if($percentage > $value) {

						if($email_notification) {

								// save into email template
								//$cureent_date =date('M d, Y g:i a');
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "High Level Alert! You are being notified that your $device_name High Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'email';
							$EmailTemplate->subject = "High Level Alert!";
							$EmailTemplate->cron_name = "low LEvel.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for keg level';
							echo "========> ";
						} 

						if($sms_notification) {

							// save into email template
								//$cureent_date =date('M d, Y g:i a');
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "High Level Alert! You are being notified that your $device_name High Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'sms';
							$EmailTemplate->subject = "High Level Alert!";
							$EmailTemplate->cron_name = "low LEvel.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for keg level';
							echo "========> ";
						} 

					}
				} else { // else set < lesss then

					if($value > $percentage) {
						$cur_date = Carbon::now();
						$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y g:i a');	


						if($email_notification) {

							// save into email template
								//$cureent_date =date('M d, Y g:i a');
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'email';
							$EmailTemplate->subject = "Low Level Alert!";
							$EmailTemplate->cron_name = "low LEvel.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for keg level';
							echo "========> ";
						} 

						if($sms_notification) {
								//$cureent_date =date('M d, Y g:i a');
								// save into email template
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'sms';
							$EmailTemplate->subject = "Low Level Alert!";
							$EmailTemplate->cron_name = "low Level.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for keg level';
							echo "========> ";
						} 

					}
				}



			}


		}

	}


	/**
	 * Create own defined function for fetch empty level
	 *
	 * @return array
	 */

	protected function fetch_empty($keg=array(),$account_id=0,$notification='E',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];

			$recent_pour_device_id=$keg->recentPour->deviceID;
			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');

			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				//starting formulas for fetching keg percentage
				$keg->recentPour->ounces =  $objectkegData->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $volumeType);

				if($keg->recentPour->ounces  < 0){
					$keg->recentPour->ounces = 0;
				}

				$keg->recentPour->total = $keg->recentPour->ounces.' '.$volumeType;

				$keg->recentPour->percentage =  $objectkegData->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);

                 //echo "account_id _id==>".$account_id."<=======";
				 //echo "====>".$keg->recentPour->percentage; /// fetch level of every user keg

				$percentage=$keg->recentPour->percentage; /// fetch level of every user keg

				$percentage = (float) $percentage;
				
				if(empty($percentage)) {
					$cur_date = Carbon::now();
					$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y g:i a');	


					if($email_notification) {

							// save into email template
							//$cureent_date =date('M d, Y g:i a');
						$EmailTemplate = new EmailTemplate;
						$EmailTemplate->user_id = $user_id;

						$EmailTemplate->content = "Empty Keg Alert! You are being notified that your $device_name  alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'email';
						$EmailTemplate->subject = "Empty Keg Alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for empty level';
						echo "========> ";
					} 

					if($sms_notification) {

						// save into email template
						$EmailTemplate = new EmailTemplate;
							//$cureent_date =date('M d, Y g:i a');
						$EmailTemplate->user_id = $user_id;

						$EmailTemplate->content = "Empty Keg Alert! You are being notified that your $device_name  alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'sms';
						$EmailTemplate->subject = "Empty Keg Alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for empty level';
						echo "========> ";
					} 

				}

			}

		}

	}


	/**
	 * Create own defined function for fetch Temperature
	 *
	 * @return array
	 */

	protected function fetch_temperature($keg=array(),$account_id=0,$notification='T',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];

			$recent_pour_device_id=$keg->recentPour->deviceID;
			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
			
			
			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				//print_r($keg->temperature);

				//starting formulas for fetching keg tempreture
				if(!is_null($keg->temperature)){

					$keg->temperature->tempFormatted = $objectkegData->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);
					$fetched_tempreture=$keg->temperature->tempFormatted;

	              //echo '===>operator is '.$operator .' value is '.$value.' temperature is '.$fetched_tempreture;
					if($operator == '>') {
						if($fetched_tempreture > $value) {
							$cur_date = Carbon::now();
							$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y g:i a');	


							if($email_notification) {

								// save into email template
								
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'email';
								$EmailTemplate->subject = "High Level Alert!";
								$EmailTemplate->cron_name = "low LEvel.php";
								$EmailTemplate->save();

								echo 'insert into email_template successfully for Temperature Alert!';
								echo "========> ";
							} 

							if($sms_notification) {

							// save into email template
								$EmailTemplate = new EmailTemplate;
								
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'sms';
								$EmailTemplate->subject = "Temperature Alert!";
								$EmailTemplate->cron_name = "low LEvel.php";
								$EmailTemplate->save();

								echo 'insert into email_template successfully for Temperature Alert!';
								echo "========> ";
							} 

						} 
				} else if($operator == '<') { // else set < lesss then

					if($value > $fetched_tempreture) {
						$cur_date = Carbon::now();
						$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y g:i a');	

						if($email_notification) {

								// save into email template
							
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'email';
							$EmailTemplate->subject = "Temperature Alert!";
							$EmailTemplate->cron_name = "low LEvel.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for Temperature Alert!';
							echo "========> ";
						} 

						if($sms_notification) {

								// save into email template
							
							$EmailTemplate = new EmailTemplate;
							$EmailTemplate->user_id = $user_id;

							$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

							$EmailTemplate->status = 0;
							$EmailTemplate->type = 'sms';
							$EmailTemplate->subject = "Temperature Alert!";
							$EmailTemplate->cron_name = "low LEvel.php";
							$EmailTemplate->save();

							echo 'insert into email_template successfully for Temperature Alert!';
							echo "========> ";
						} 

					}
				}
				
			}


		}


	}

}



	/**
	 * Create own defined function for fetch untap
	 *
	 * @return array
	 */

	protected function fetch_untap($keg=array(),$account_id=0,$notification='E',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];

			$recent_pour_device_id=$keg->recentPour->deviceID;
			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');

			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				//starting formulas for fetching keg untap 

				$tapped_at_time=$keg->tapped->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
				

				
				if(!empty($tapped_at_time)) {

					if($email_notification) {

							// save into email template
						$EmailTemplate = new EmailTemplate;
						$EmailTemplate->user_id = $user_id;

						$EmailTemplate->content = "Your Keg was Untapped. You are being notified that your $device_name has been untapped at $tapped_at_time. You are being sent this notification according to your settings under the notifications settings at KegData.";

						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'email';
						$EmailTemplate->subject = "Untapped Alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for Untapped Alert!';
						echo "========> ";
					} 

					if($sms_notification) {

						// save into email template
						$EmailTemplate = new EmailTemplate;
						$EmailTemplate->user_id = $user_id;

						$EmailTemplate->content = "Your Keg was Untapped. You are being notified that your $device_name has been untapped at $tapped_at_time. You are being sent this notification according to your settings under the notifications settings at KegData.";

						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'sms';
						$EmailTemplate->subject = "Untapped Alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for Untapped Alert!';
						echo "========> ";
					} 

				}

			}

		}

	}


	/**
	 * Create own defined function for after hour pour conducted
	 *
	 * @return array
	 */

	protected function after_hour($keg=array(),$account_id=0,$notification='E',$objectkegData=null,$volumeType=null,$user_id=null,$accountSettings=null,$timezone=null) {
		
		if(!is_null($keg->recentPour)){
			$device_name=$keg['deviceName'];

			$recent_pour_device_id=$keg->recentPour->deviceID;
			//$sent_at=date('m-d-Y H:i:s',strtotime($keg->recentPour->sent_at)); // date of poured
			$sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');

			//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$recent_pour_device_id,'notification'=>$notification,'deleted_at'=>null,'frequency'=>$this->frequency])->first();

			$check_device_exist_in_notification['id'];
			

			if(!empty($check_device_exist_in_notification['id'])) {

				//echo 'yes set for '.$recent_pour_device_id;

				$value=$check_device_exist_in_notification['value'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$operator=$check_device_exist_in_notification['operator'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];

				//starting formulas for fetching keg untap
				$time_start=$accountSettings->dayStart;
				$time_end=$accountSettings->dayEnd;

				

				$query = kegdataview::select(['deviceID', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount'])->where('deviceID', '=', $kegdevice_id)->where('status', '=', '11')->where(DB::raw('Date(sent_at)'), '=', date('Y-m-d'));
				
				$query=$query->where(function($query) use ($time_end,$time_start){

					$query->orWhere(function($query) use ($time_end,$time_start){

						$query->Where(DB::raw('TIME(sent_at)'), '>', $time_end);

					});

					$query->orWhere(function($query) use ($time_end,$time_start){
						$query->Where(DB::raw('TIME(sent_at)'), '<', $time_start);

					});
				});
				$query->orderBy('sent_at', 'DESC');

				$pourHistory = $query->get();

				

				$pour_count_after_hour=$pourHistory->count();

				
				if(!empty($pour_count_after_hour)) {

					if($email_notification) {

							// save into email template
						$EmailTemplate = new EmailTemplate;
						$EmailTemplate->user_id = $user_id;

					        //$EmailTemplate->content = "There was an after hours pour.";
						$EmailTemplate->content = "Here are After Hours pours detected based on your notification criteria:";

						foreach ($pourHistory as $v) {
							
							$sent_at=$v['sent_at'];
							$event='Pour Event';

							echo '<br>';
							$EmailTemplate->content .=date('m/d/Y H:i:s', (strtotime($sent_at))). "- ".$device_name."- ".$event;
							echo '<br>';

						}

						$EmailTemplate->content .=" You are being notified that your $device_name has shown a pour after your business hours. You are being sent this notification according to your settings under the notifications settings at KegData.";
						
						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'email';
						$EmailTemplate->subject = "After Hours pour alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for After hours pour alert!';
						echo "========> ";
					} 

					if($sms_notification) {

						// save into email template
						$EmailTemplate = new EmailTemplate;
						$EmailTemplate->user_id = $user_id;

						$EmailTemplate->content = "There was $pour_count_after_hour pours recorded After Hours. You are being notified that your $device_name has shown a pour after your business hours. You are being sent this notification according to your settings under the notifications settings at KegData.";

						$EmailTemplate->status = 0;
						$EmailTemplate->type = 'sms';
						$EmailTemplate->subject = "After Hours pour alert!";
						$EmailTemplate->cron_name = "low LEvel.php";
						$EmailTemplate->save();

						echo 'insert into email_template successfully for After hours pour alert!';
						echo "========> ";
					} 

				}

			}

		}

	}


}
