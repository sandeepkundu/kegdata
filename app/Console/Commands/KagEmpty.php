<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//use DB;
use Mail;
use App\KegData\Models\User;
use App\KegData\Models\Account;
use App\KegData\Models\Device_Notification;
use App\KegData\Models\Brewery_Distributor;
use App\KegData\Models\AccountSetting;
use App\KegData\Models\EmailTemplate;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\KegData as kegdataview;

use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon as Carbon;
use Utilities;
use App\KegData\Models\LsmValue as LsmValue;


class KagEmpty extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notify:kagempty';
	public $frequency='9';
	public $count=0;
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Backgroun process for insert data into lsm_values';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$users = User::where(['isActive'=> 1,'deleted_at'=>null]) ->groupBy('account_id')->get();

		foreach ($users as $user)
		{
			$user_id=$user->id;
			$account=$user->account;
			$account_id=$account['id'];
			//fetch account settings
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$volumeType=$accountSettings['volumeType'];

			$accountType = $account['accountType'];

			$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

			//create necessary object
			$keg=new Keg;
			$kegdata=new KegData;
			$utilities=new Utilities;
			
			//$kegs = $keg->getForNotifications($account_id, 'account_id');
			
			if($account_id) { // check if there is account id

				//fetch users kegs for fetch level
				$kegs  = $keg->getForLevels($account_id, 'account_id', $account_id);
				$deviceID = $kegs->fetch('id')->toArray();
				$pourHistory = $kegdata->getPourHistory($deviceID);
				
				//loop started for each keg
				$this->count = 1;
				$kegs->each(function($keg) use ($timezone, $accountSettings, $pourHistory,$kegdata,$utilities){
					//calling object for lsm_values
					
					
					$lsmvalue = new LsmValue;
						
      				//get last break event from lsm_values table
					$last_break_from_lsmvaue_table = $kegdata->lastBreakFromLsmVaue($keg->id);
					//dd($last_break_from_lsmvaue_table);
					$lsm_values_last_break_event_date= isset($last_break_from_lsmvaue_table->break_event_date) ? new Carbon($last_break_from_lsmvaue_table->break_event_date) : "";

					$last_break_m_value = isset($last_break_from_lsmvaue_table->m) ? $last_break_from_lsmvaue_table->m: 0.15; 

					//if first time lsm_values are empty
					$break_date_save_forlsm_table = '';
					if(empty($lsm_values_last_break_event_date)) {

						/* Function for get all pour after recent break date  */
						//get 15 days back date
						$first_date =Carbon::now()->subDays(9);
						//echo "==>".$first_date;
						//get 13 days back date
						//$second_date =Carbon::now()->subDays(7);
						$second_date =Carbon::now();
						//dd($second_date);
						$dataAfterLastBreak = $kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$first_date,$second_date);
						$break_date_save_forlsm_table = $first_date;

						//echo "==keg id==>".$keg->id;
						//print_r($dataAfterLastBreak);
					} else { //find out next break event date and its data

			          //find out next date of break event from kegdata
						
						$result = $kegdata->find_next_date_of_break($keg->id,$lsm_values_last_break_event_date);

						$next_break_date = (isset($result->sent_at)) ?new Carbon($result->sent_at) : null;
						//dd($next_break_date);

						/* Function for get all pour after recent break date  */

			         if(empty($next_break_date) ){ // check if next break event is NULL 

			         	//find out data till today
			         	$dataAfterLastBreak = $kegdata->pourInCaseOfNoNextBreakFound($keg->id,$lsm_values_last_break_event_date,new Carbon());
			         	//dd($dataAfterLastBreak);

			         	$break_date_save_forlsm_table = $lsm_values_last_break_event_date;

			         	$keg->is_next_to_next_is_blow_event =null;
			         	$keg->next_to_next_break_date =null;

			         }
			         else { //else next break event found
			         	//echo "====>".$next_break_date;
			         	//find out next to next break event date
			         	$result_next_to_next = $kegdata->find_next_date_of_break($keg->id,$next_break_date);
			         	
			         	//if there is no break date found from next to next then
			         	//fetch data from till date
						$next_to_next_break_date = (isset($result_next_to_next->sent_at)) ? new Carbon($result_next_to_next->sent_at) : new Carbon();
						//end code for find out next to next break event date

						// IF ending breakevent is blow keg then count it as a pour etc etc
						$keg->next_to_next_break_date =$next_to_next_break_date;
						//check if next to next break event status is blow event
						$next_to_next_is_blow_event_satus = (isset($result_next_to_next->status)) ? $result_next_to_next->status : null;

			         	echo "====next to next event====>".$keg->is_next_to_next_is_blow_event =$next_to_next_is_blow_event_satus;
			         	
						
			         	$dataAfterLastBreak = $kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$next_break_date,$next_to_next_break_date);
			          
			         	$break_date_save_forlsm_table = $next_break_date;

			         }
			     }
			     /* End if else condition */
			     
			     //find result on this break_event_Date because we need to check that if the status is 10011 then we add this data in the lsm use further in code
			     $result_break_date_check = $kegdata->check_status_of_break_date($keg->id,$break_date_save_forlsm_table);

			     // if count of the data set is zero then only save break date

			     if($dataAfterLastBreak->count() == 0) {

			     	$brek_evnt_status = (!empty($result_break_date_check)) ? $result_break_date_check->status : null;

			     	if( $brek_evnt_status){

		     			$data_exist  = $lsmvalue->where('break_event_date' , '=', $break_date_save_forlsm_table)->first();

		     			if(empty($data_exist)) {

		     				if( !empty($keg->is_next_to_next_is_blow_event) && ($keg->is_next_to_next_is_blow_event == '10011')){
				     			echo "==save blow event if next to next is blow like after 101 found 10011=>";
				     			$result_break_date_next = $kegdata->check_status_of_break_date($keg->id,$keg->next_to_next_break_date);

				     			$value_of_b=($result_break_date_next->adc_pour_end) - (($last_break_m_value)*($result_break_date_next->pour_length));
				     			$new_brek_date = new Carbon($result_break_date_next->sent_at);
			     				$new_brek_date = $new_brek_date->toDateTimeString();

				     			$t_s_x['json_x']= [$result_break_date_next->pour_length];
				     			$t_s_d['pour_date']= [$new_brek_date];
				     			$t_s_adc['pour_y']= [$result_break_date_next->adc_pour_end];
				     			$t_s_x=json_encode($t_s_x);
				     			$t_s_d=json_encode($t_s_d);
				     			$t_s_adc=json_encode($t_s_adc);


				     		} else {
				     			$value_of_b =0;
				     			$t_s_x=null;
				     			$t_s_d=null;
				     			$t_s_adc =null;
				     		}	


		     				$lsmvalue->kegdevice_id  = $keg->id;
				   			$lsmvalue->m =$last_break_m_value;
				   			$lsmvalue->b =$value_of_b;
				   			$lsmvalue->simple_x = $t_s_x;
				   			$lsmvalue->pour_date = $t_s_d;
				   			$lsmvalue->break_event_date = new Carbon($break_date_save_forlsm_table);
				   			$lsmvalue->adc_pour_end = $t_s_adc;
				   			$lsmvalue->status="1" ;
				   			$lsmvalue->save();
				   			echo "===Save and exit break event data for single record==>";
		     			}
		     			exit();	
		     		}

			     	
			     }
			     
			     //end code for find break date status

			     $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));

			     $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
			     $this->count++;
			     $keg->calibration =  $kegdata->getCalibratedValue($keg->kegMac);
			         //$last_break=   new Carbon($getLastBreak->created_at);
			     
			     $keg->lastBreak = $break_date_save_forlsm_table;

			     //check if there is pour data come start lsm
			     if($dataAfterLastBreak){
	                //dd($dataAfterLastBreak);
	                $is_keg_blow ="no";
			     	$dataAfterLastBreak->each(function($pour) use($is_keg_blow,$keg, $timezone, $accountSettings,$kegdata,$utilities,$lsmvalue,$last_break_m_value){
			     		$pour->name =  $keg->deviceName;
			     		//echo "======>".$pour->status;
			     		 //convert data in timezone
			     		//$pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
			     		//convert data in Utc
			     		$date_in_utc = new Carbon($pour->sent_at);
			     		$pour->date = $date_in_utc->toDateTimeString();

			     		$pour->length = $pour->pour_length;
			     		$pour->adc_pour = $pour->adc_pour;

			     		$pour->level = $kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

			     		$pour->percentage = $kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);

			     		//$lsm_m='';
			     		$lsm_m = (object) array("lsm_m"=>$last_break_m_value);
			     		
			     		//echo "====Pour date==>".$pour->date;
			     		
			     		$rerurn_data=$kegdata->pseudo_steps_first($pour,$lsm_m);
			     		
			     		if($rerurn_data==1 || $rerurn_data==true){
	                    //push level for lsm calculation input in lsm
			     			$this->graph_arr[$pour->name][] =$pour->level;
			     			$this->graph_arr_x_axis[$pour->name][] =$pour->length;
			     			$this->adc_pour_end_lsm[$pour->name][] =$pour->adc_pour_end;

			     			$this->graph_arr_x_axis_date[$pour->name][] =$pour->date;
			     			$pour->is_valid_pour=$rerurn_data;

			     		} 

			     		//$pour->is_valid_pour=$rerurn_data;
			     	});
	        	} //End to check if there is pour data come start lsm
	        	
	        	// now to check if the break_date status is 10011 then save it also in lsm
	        	$brek_event_status = (!empty($result_break_date_check)) ? $result_break_date_check->status : null;
	        	//dd($brek_event_status);
	        	//echo "==in blow event=>".$brek_event_status;
	        	//send blow event in lsm
	     		/*if( $brek_event_status == '10011'){
	     			$this->graph_arr[$keg->deviceName][] =0;
	     			$this->graph_arr_x_axis[$keg->deviceName][] =$result_break_date_check->pour_length;
	     			$this->adc_pour_end_lsm[$keg->deviceName][] =$result_break_date_check->adc_pour_end;

	     			//convert date in timezone
	     			//$new_brek_date = $result_break_date_check->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
	     			//convert date in utc
	     			$new_brek_date = new Carbon($result_break_date_check->sent_at);
			     	$new_brek_date = $new_brek_date->toDateTimeString();
	     			$this->graph_arr_x_axis_date[$keg->deviceName][] = $new_brek_date;
	     			
	     		}*/
	     		// end code to send blow event in lsm
	     		//send blow event in lsm if next to next is break event
	     		//save blow event if next to next is break event
	     		if( !empty($keg->is_next_to_next_is_blow_event) && ($keg->is_next_to_next_is_blow_event == '10011')){
	     			echo "==in blow event=>".$brek_event_status;
	     			$result_break_date_next = $kegdata->check_status_of_break_date($keg->id,$keg->next_to_next_break_date);

	     			$this->graph_arr[$keg->deviceName][] =0;
	     			$this->graph_arr_x_axis[$keg->deviceName][] =$result_break_date_next->pour_length;
	     			$this->adc_pour_end_lsm[$keg->deviceName][] =$result_break_date_next->adc_pour_end;

	     			//convert date in timezone
	     			//$new_brek_date = $result_break_date_check->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
	     			//convert date in utc
	     			$new_brek_date = new Carbon($result_break_date_next->sent_at);
			     	$new_brek_date = $new_brek_date->toDateTimeString();
	     			$this->graph_arr_x_axis_date[$keg->deviceName][] = $new_brek_date;
	     			
	     		}
	     		//end code for blow event
	     		//dd($this->graph_arr_x_axis);

	     		//end code to check break_event status

	        	//find m and b from graph array 
	        	if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {

	        		$keg->valid_pour_count =count($this->graph_arr[$keg->deviceName]);

	        		$lsm_pour_date['pour_date'] = $this->graph_arr_x_axis_date[$keg->deviceName] ;
	        		$pour_date = json_encode($lsm_pour_date);
	        		//send lms for sstep 1
	        		$lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);
	        		//end code send lms for sstep 1
		           //dd($lms_data);

	        		/* New m and b for find value of y for new formula */
	        		//send lms for sstep 2 this return actual ma and b array which we use in system
	        		//dd($brek_event_status);
	        		$lms_data_new  = $this->linear_regression_adc_pour_end($this->graph_arr_x_axis[$keg->deviceName],$this->adc_pour_end_lsm[$keg->deviceName],$keg->is_next_to_next_is_blow_event);
	        		$keg->lms_data_for_adc_end=$lms_data_new;
	        		//end code for step 2
		      

	        		$positive_m = abs($lms_data['m']) ;
	        		$comm_x = $lms_data['json_x'];
	        		$y = floor($positive_m*$lms_data['commulative_x']);

		       $keg_level = $lms_data['b']- $y  ;// Find keg level according to lsm 
		       
		       $keg->value_y= $keg_level ;// Show keg leve acording to lsm
		       $keg->lms_data=$lms_data;
		   } else {
		   	$keg->lms_data=null;
		   } 
    		//end to find m and b

		   if(!is_null($keg->recentPour)){

		   	/* Condition for check if Pour less grater then 2 then show level and Percetage according to this*/
		   	//echo "==valid pour count===".$keg->valid_pour_count;
		   	if($keg->valid_pour_count){

		   		$lsm_b =   $keg->lms_data['b'] ;

		   		$keg->recentPour->total =  floor($keg->value_y).' '.$accountSettings->volumeType;
		   		
		   		//$new_adc_pour_end = $kegdata->adcPourEnd($keg); 

		   		/* End code */

		   		$exist_lsm_value_data  = $lsmvalue->where('break_event_date' , '=', $lsm_values_last_break_event_date)->first();


		   		//code start here to check if there is greater then 10 valid pour and cum_x is greater then 1000
		   		//then use previous m if condition is not true
		   		$m_val=0;
	   			//code to check 1000 millisecond for cum_x
	   			$simple_x_array = json_decode($comm_x);
	   			$t=0;
				$temp=[];
				$data_arr=$simple_x_array->pour_x;
				for ($i=0;$i < (count($data_arr));$i++) {
					$t=$t+$data_arr[$i];
					$temp[]=$t;
				}
				$total_of_cum_x = array_sum($temp);
	   			//end code for cum_x
	   			//echo "=====Last break===>".$keg->lastBreak;
				//echo "===KegCount===>".$keg->valid_pour_count;
				//echo "===total_cum_x===>".$total_of_cum_x;

	   			if( ($keg->valid_pour_count < 10) || ($total_of_cum_x < 1000) ) {
	   				//dd($last_break_m_value);
	   				echo "==aplly last break Value=>".$last_break_m_value;
	   				$m_val = $last_break_m_value;

	   			} else {
	   				//echo "<=====in else======>";
	   				$m_val = $keg->lms_data_for_adc_end['m'];
	   			}
	   			//echo "===value of_m===>".$m_val;
	   			//end code for check valid pour for previous m

	   			/*Calculate b*/
	   			//If there is only 1 pour event then here is the formula for B  b=adc_end_of_pour - [(m)(x)] 
	   			if($keg->valid_pour_count==1) {
	   				echo '====>Save b  valueif Valid pour is 1====>';
	   				$adc_val=end($this->adc_pour_end_lsm[$keg->deviceName]);
	   				$x_val=end($this->graph_arr_x_axis[$keg->deviceName]);
	   				$b_val = $adc_val- (abs($m_val) * $x_val);
	   			} else {
	   				$b_val = isset($keg->lms_data_for_adc_end['b']) ? abs($keg->lms_data_for_adc_end['b']) : 0;
	   			}
	   			
	   			/* check if row already exist  then update the row */
		   		if(empty($next_break_date) && isset($exist_lsm_value_data)){
		   			
		   			$lsmvalue->where('break_event_date', '=', $lsm_values_last_break_event_date)
		   			->where('kegdevice_id','=',$keg->id)
		   			->update(['kegdevice_id' => $keg->id,
		   				'm'=>abs($m_val),
		   				'b'=>$b_val,
		   				'simple_x' => $comm_x ,
		   				'pour_date' => $pour_date ,
		   				'adc_pour_end' => $lms_data_new['json_y'] ,
		   				'break_event_date' => $lsm_values_last_break_event_date ,
		   				'status'=>"1"
		   				]);

		   		}else{

		   			$lsmvalue->kegdevice_id  = $keg->id;
		   			$lsmvalue->m =abs($m_val);
		   			$lsmvalue->b =$b_val;
		   			$lsmvalue->simple_x = $comm_x ;
		   			$lsmvalue->pour_date = $pour_date ;
		   			$lsmvalue->break_event_date = $keg->lastBreak ;
		   			$lsmvalue->adc_pour_end = $lms_data_new['json_y'] ;
		   			$lsmvalue->status="1" ;
		   			$lsmvalue->save(); 

		   		}

		   	} 



		   }


		}); // end loop for keg

}  

		} // end user foreach


		$this->info('Background for fill up data in lsm_values successfully');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

	/**
	 * Create own defined function for fetch level
	 *
	 * @return array
	 */

	//protected function fetch_level($keg=array()) {
	//}

	/**
     * linear regression function
     * @param $x array x-coords
     * @param $y array y-coords
     * @returns array() m=>slope, b=>intercept
     */
	protected function linear_regression($y,$x) {

		$y = array_values($y);

		$new_x['pour_x'] = array_values($x);
		$x =  array_values($x);

         /* 
        For demo only 23-May-2017
          */
        $new_y['pour_y'] = array_values($y);
        $json_y = json_encode( $new_y);
     //   dd($json_y); 
        /* End demo 23-May */


        $json_x = json_encode($new_x);

        $commulative_x = array_sum($x);          

        $count=count($y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
        	$add=$t+$x[$i];
        	$t=$add;
        	$temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($y)) {

        	trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

        	$xy_sum+=($x[$i]*$y[$i]);
        	$xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
        	$m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
        	$m=0;
        }


  // calculate intercept
        if($n) {
        	$b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
        	$b=0;
        }

  // return result
        return array("m"=>$m, "b"=>$b,"commulative_x"=>$commulative_x,"json_x"=>$json_x,' json_y'=> $json_y );

    }


       /**
 * linear regression adc pour end  function 
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
       protected function linear_regression_adc_pour_end($x,$new_y,$if_blow_event_set) {
       	//code start to check if blow event set 10011 then we will pop last elements in both array because we will not send blow event value in lsm
       	if(!empty($if_blow_event_set) && $if_blow_event_set == '10011')
       	{
       		 array_pop($x);
       		 array_pop($new_y);
       	}
       	$new_y = array_values($new_y);

       	$new_x['pour_x'] = array_values($x);
       	$new_y1['pour_y'] =  $new_y  ;
       	$x =  array_values($x);

       	$json_x = json_encode($new_x);
       	$json_y  = json_encode(  $new_y1);
        //$commulative_x = array_sum($x);          

       	$count=count($new_y);

   // do x axis comulnative
       	$t=0;
    //assume $temp as x axis array
       	$temp=array();
       	for($i=0;$i<count($x);$i++) {
       		$add=$t+$x[$i];
       		$t=$add;
       		$temp[$i]=$t;
       	}
    //assign temp into x
       	$x=$temp;

    // calculate number points
       	$n = count($x);

  // ensure both arrays of points are the same size
       	if ($n != count($new_y)) {

       		trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

       	}

  // calculate sums
       	$x_sum = array_sum($x);
       	$y_sum = array_sum($new_y);

       	$xx_sum = 0;
       	$xy_sum = 0;

       	for($i = 0; $i < $n; $i++) {

       		$xy_sum+=($x[$i]*$new_y[$i]);
       		$xx_sum+=($x[$i]*$x[$i]);

       	}

  // calculate slope
       	if((($n * $xx_sum) - ($x_sum * $x_sum))) {
       		$m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
       	} else {
       		$m=0;
       	}


  // calculate intercept
       	if($n) {
       		$b = ($y_sum - ($m * $x_sum)) / $n;  
       	} else {
       		$b=0;
       	}


       	return array("m"=>$m, "b"=>$b,"json_x"=>$json_x,"json_y"=>$json_y);

       }



   }
