<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\KegData\Models\EmailTemplate;
use App\KegData\Models\User;
use App\KegData\Models\Account;
use App\KegData\Models\AccountSetting;
use DB;
use Mail;

class SendAllEmail extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notify:sendallemail';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Cron job for sending email running every 5 minutes!';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//$email =EmailTemplate::where('status',0)->first();

		$all_email =EmailTemplate::where('status',0)->where('type','email')->orderBy('id','desc')->take(2)->get();
		
		foreach ($all_email as $email) {
			if($email){

			//echo "===user email==>".$email->id;

				$user = User::where('id',$email->user_id)->first();

				$account=$user->account;
				$account_id=$account['id'];
				$user_email=$account['email_alert'];
				$user_sms=$account['sms_alert'];
				$cc_1=$account['optional_1'];
				$cc_2=$account['optional_2'];
			//$accountSettings=AccountSetting::where('account_id', $account_id)->first();
				$subject=$email->subject;
				$content=$email->content;

				//because they add third optional email so if first is empty then assiogn optional 1 i/e cc1 to main email
				$check_email_empty=$user_email;
				if(empty($check_email_empty)) {
					$check_email_empty=$cc_1;
				}
				//because they add third optional email so if first is empty then assiogn optional 1 i/e cc2 to main email
				if(empty($check_email_empty)) {
					$check_email_empty=$cc_2;
				}
				

				if(empty($check_email_empty))	{

					$delete_email =EmailTemplate::where('id', $email->id)->delete();	
					continue;
				}

				$to_array=array();
				if($user_email) {
					array_push($to_array, $user_email);
				}
				if($cc_1) {
					array_push($to_array, $cc_1);
				}
				if($cc_2) {
					array_push($to_array, $cc_2);
				}

				if(($email->type == 'email') && (count($to_array)>0)){

					Mail::raw($content, function($message) use ($subject,$content,$cc_1,$cc_2,$email,$user_email,$to_array)
					{

						try { 
							$message->from('accounts@kegdata.com', 'Kegdata notification');

							$message->to($to_array);
							$message->subject($subject);
							/*if($cc_1) {
								$message->cc($cc_1);
							}
							if($cc_2) {
								$message->cc($cc_2);
							}*/

						}

						catch(Exception $e) {

							echo 'Message: ' .$e->getMessage();
						}


					});

					if(count(Mail::failures())){
						echo "Mail Failure";
					}else{
					  //update email template status column
						$ret=EmailTemplate::where('id', $email->id)->update(array('status' => 1));
						if($ret) {
							echo 'email status update successfully';
						}
					}


			} //end if

		}
		}//end foreach


		//run for sms
		$all_email =EmailTemplate::where('status',0)->where('type','sms')->orderBy('id','desc')->take(2)->get();
		
		foreach ($all_email as $email) {
			if($email){

			//echo "===user email==>".$email->id;

				$user = User::where('id',$email->user_id)->first();

				$account=$user->account;
				$account_id=$account['id'];
				$user_email=$account['email_alert'];
				$user_sms=$account['sms_alert'];
				$cc_1=$account['optional_1'];
				$cc_2=$account['optional_2'];
			//$accountSettings=AccountSetting::where('account_id', $account_id)->first();
				$subject=$email->subject;
				$content=$email->content;
				

				if(strlen($user_sms) < 10 ){ // if phone number digit is less than 0
					$delete_sms =EmailTemplate::where('id', $email->id)->delete();	
					continue;
				}
				if(($email->type == 'sms') && strlen($user_sms) > 9){

					try { 
						$http_code = $this->send_sms($content,$user_sms);

						if($http_code == '202' ||  $http_code == '200') {
						//update email template status column
							$ret=EmailTemplate::where('id', $email->id)->update(array('status' => 1));
							if($ret) {
								echo 'sms status update successfully';
							}

						} else {
							echo 'sms_not _send';
						}


					}

					catch(Exception $e) {
						echo 'Message: ' .$e->getMessage();
					}

			} //end if //end if
			
		}
		}//end foreach
		
		
		
		$this->info('Email and SMS send successfully!');

	}

	protected function send_sms($content,$sender_number) {

		if(strlen($content) > 160) {
			$content = substr($content,0,160);
		}

		//echo "=====>".$sender_number;
		# Plivo AUTH ID
		$AUTH_ID = 'MAYTG0ODGZNDQ0YJGZMW';
		# Plivo AUTH TOKEN
		$AUTH_TOKEN = 'YzE3Njg0YjMyOTJkZTJiMGNhMjMzZDJmMWU2ODIx';
		# SMS sender ID.
		$src = '18063059022';
		//$src = '18323108065';//we need src 832-310-8065. 
		# SMS destination number
		$dst = $sender_number;//919457624855 - $sender_number
		# SMS text
		$text = $content;
		$url = 'https://api.plivo.com/v1/Account/'.$AUTH_ID.'/Message/';
		$data = array("src" => "$src", "dst" => "$dst", "text" => "$text");
		$data_string = json_encode($data);
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_USERPWD, $AUTH_ID . ":" . $AUTH_TOKEN);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$response = curl_exec( $ch );
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//if 202 or 200 then sms send
		return $httpcode;

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		//	['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
