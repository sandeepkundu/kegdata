<?php namespace App\KegData\Composers;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentProductRepository as Product;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\KegData\Repositories\EloquentTaxRepository as Tax;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use DB ;

class EmployeeProductComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Product $products, Tax $tax, Utilities $utilities, Category $category)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->products = $products;
        $this->tax = $tax;
        $this->utilities = $utilities;
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 
        
        $viewData = $view->getData();
        !empty($viewData['id'])?  $productID = $viewData['id'] : $productID = $viewData['tab'];
      
        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $products = $this->products->with('photos')->with('category')->get();
        $categories = $this->category->get();
        $tax = $this->utilities->withEmpty( $this->tax->getForSelect('name'), 'Select Tax');

        if($productID != 0){
            $product = $products->find($productID);
            $prodCat = DB::table('category_product')
                     ->where('product_id', '=', $productID)
                     ->get();
           
        }else{
            $product = null;
             $prodCat = null;
        }
     
        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->withProducts($products)->withCategories($categories)->withProduct($product)->withTax($tax)->with('prodCat',$prodCat);
    }

}