<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
class GraphReportsTestComposer {

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, BrewDist $brewDist)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->brewDist = $brewDist;
      $this->graph_arr = [];
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        

      $newGraph = 0;
      $action = Input::get();
      if(isset($action['submit'])){
       if($action['submit']== 'Run Graph'){

        $newGraph = 1;
        }
       }



        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        if($accountType == 'B' || $accountType == 'D'){
            $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
            $query = $brewDist->id;
            $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
        }else{
            $query = $accountID;
            $column = 'account_id';
        }
        

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $default_date_start=date('Y-m-d', strtotime('-30 days'))." ".$accountSettings->dayStart;
        $default_date_end=date('Y-m-d')." ".$accountSettings->dayEnd;
        
        //dd($view->input);
        $ident_hdn = (empty($view->input['ident_hdn'])) ? 'level' : $view->input['ident_hdn'];

        if(isset($action['chart'])) {
            $ident_hdn=$action['chart'];
        }


        $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $reportStatus = (empty($view->input['status'])) ? null : $view->input['status'];
        $reportDates = $this->utils->reportDates();


        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $kegs_for_filters  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
        $keg_list=$kegs_for_filters->lists('KegFullName', 'id');
        

         $device_id_for_default=null;
         if(empty($view->input['deviceID']) && (count($keg_list)) ) {
            //$deviceID=$kegsForSelect[0];
            $device_id_for_default=array_keys($keg_list)[0];
            
         }

        //print_r($device_id_for_default);

         

        $filter_device_id = (empty($view->input['deviceID'])) ? null : $view->input['deviceID'];


        
       // dd($view->input);
        // Return data for first tab
        $reportData  = $this->kegdata->getForReports($query, $column, $timezone->timezone, $view->input,$default_date_start,$default_date_end);

/*        $reportData->each(function($data) use ($timezone, $accountSettings){

        });*/


        $view->with('newGraph',$newGraph)->with('ident_hdn',$ident_hdn)->with('reportData', $reportData)->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)->with('quickDates', $quickDates)->with('reportDates', $reportDates)->with('reportStatus', $reportStatus)->with('status', $reportStatus)->with('deviceID', $filter_device_id);



        //start code for tabs
        //$user = $this->auth->user();
        //$account = $this->auth->user()->account;
        //$accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        // code
       // dd($filter_device_id);
        $kegs_for_filter  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
         $kegs  = $this->kegs->getForLevelsCharts($accountID, 'account_id', $accountID,$filter_device_id);


        $deviceID = $kegs->fetch('id')->toArray();

        $filter['receivedDateTime_start'] = empty($reportStart)?$default_date_start:$reportStart;
        $filter['receivedDateTime_end'] = empty($reportEnd)? $default_date_end:$reportEnd;
        
        $tempHistory = $this->kegdata->getTempHistory($deviceID,0, 60 ,$filter);
        //$pourHistory = $this->kegdata->getPourHistory($deviceID,0, 60 ,$filter);
        $pourHistory = $this->kegdata->getPourHistoryDemo($deviceID,0, 60 ,$filter);




        //get date interval to send it to level js
        $date_interval_array = $this->kegdata->getDateIntervals();

        $kegsForSelect = array('' => 'Select Keg') + $kegs_for_filter->lists('KegFullName', 'id');
        $this->count = 1;
        $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

             $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
            $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));


            $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
            $this->count++;
            $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
            if($keg->showDevice == 1){

              /* if(!is_null($keg->tapped) && !is_null($keg->recentPour)){
                    $keg->tappedDiff = $keg->tapped->sent_at->diffInSeconds($keg->recentPour->sent_at, false);
                    if($keg->tappedDiff < 0){
                        $keg->recentPour->adc_pour_end = 2250;
                    }
               }else{
                   $keg->tappedDiff  = null;
               }*/

                if(!is_null($keg->tapped)){
                    $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->recentPour)){
                    $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                    if($keg->recentPour->ounces  < 0){
                        $keg->recentPour->ounces = 0;
                    }
                    $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                    $keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                    $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }

               if(!is_null($keg->temperature)){
                $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                    $temp->name = $keg->deviceName;
                    $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                });
            }

            if(!is_null($keg->pourHistory)){

                //empty session
                Session::set('last_pour', null);
                Session::set('last_pour_status', null);

                $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){

                  
                    $pour->name =  $keg->deviceName;
                    $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                   
                    $pour->length = $pour->pour_length;
                   $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);
                   

                    $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);
                    //$this->graph_arr[$pour->name][] =$pour->level;


                     //to check lsm algo
                    $rerurn_data=$this->kegdata->pseudo_steps_first($pour);
                    if($rerurn_data){
                        //push level for lsm calculation
                       $this->graph_arr[$pour->name][] =$pour->level;
                    }
                    $pour->is_valid_pour=$rerurn_data;

                    

                });
            }


            //find m and b from graph array 
            if(isset($this->graph_arr[$keg->deviceName])) {
                 $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName]);
                 $keg->lms_data=$lms_data;
            } else {
                $keg->lms_data=null;
            }
           

            }


        });
       
        $view->withKegs($kegs)->with('kegdata', $this->kegdata )->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);

        ////end code for tabs



    }

    /**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
  public function linear_regression($y) {

  
    $count=count($y);

    if($count>9) {
        $median=floor($count/2);    
        $temp=array();
        $temp[]=$y[0];
        $temp[]=$y[1];
        $temp[]=$y[2];
        
        if($median) {
         $temp[]=$y[$median];
         $temp[]=$y[$median+1];
         $temp[]=$y[$median+2];
        }
        
        $temp[]=$y[$count-3];
        $temp[]=$y[$count-2];
        $temp[]=$y[$count-1];
        
        unset($y);
        $y=$temp;
        
    }

  //find x axis array
  $x= range(1,count($y));

  // calculate number points
  $n = count($x);


  


  
  // ensure both arrays of points are the same size
  if ($n != count($y)) {

    trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
  
  }

  // calculate sums
  $x_sum = array_sum($x);
  $y_sum = array_sum($y);

  $xx_sum = 0;
  $xy_sum = 0;
  
  for($i = 0; $i < $n; $i++) {
  
    $xy_sum+=($x[$i]*$y[$i]);
    $xx_sum+=($x[$i]*$x[$i]);
    
  }
  
  // calculate slope
  $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
  
  // calculate intercept
  $b = ($y_sum - ($m * $x_sum)) / $n;
    
  // return result
  return array("m"=>$m, "b"=>$b);

}





}