<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
use DB;
use App\KegData\Models\LsmValue as LsmValue;
class GraphReportsSecondComposer {

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth,LsmValue $lsmvalue, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, BrewDist $brewDist)
    {
      $this->auth = $auth;
      $this->kegdata = $kegData;
      $this->collection = $collection;
      $this->kegs = $kegs;
      $this->timezone = $timezone;
      $this->count = 0;        
      $this->accountSetting = $accountSetting;
      $this->utils = $utils;
      $this->brewDist = $brewDist;
      $this->graph_arr = [];
      $this->graph_arr_x_axis = [];
      $this->lsmvalue = $lsmvalue;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

      $newGraph = 0;
      $action = Input::get();
      if(isset($action['submit'])){
       $newGraph = 1;
     }
    
     //print_r($view->input);

         //for setting chart pagination
     $page_limit = isset($view->input['page_limit']) ? $view->input['page_limit']:50;
     $offset_setting = isset($view->input['offset_setting']) ? $view->input['offset_setting']:0;

     $user = $this->auth->user();
     $account = $this->auth->user()->account;
     $accountID = $account->id;
     $accountType = $this->auth->user()->account->accountType;
     $accountSettings = $this->auth->user()->account->accountSetting->first();
        //dd($accountSettings);
     $accountAddress = $this->auth->user()->account->accountAddress->first();

     if($accountType == 'B' || $accountType == 'D'){
      $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
      $query = $brewDist->id;
      $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
    }else{
      $query = $accountID;
      $column = 'account_id';
    }


    $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

    $default_date_start=date('Y-m-d', strtotime('-1 days'))." ".$accountSettings->dayStart;
    $default_date_end=date('Y-m-d')." ".$accountSettings->dayEnd;

        //dd($view->input);
    $ident_hdn = (empty($view->input['ident_hdn'])) ? 'pour' : $view->input['ident_hdn'];

    if(isset($action['chart'])) {
      $ident_hdn=$action['chart'];
    }


    $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
    $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
    $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
    $reportStatus = (empty($view->input['status'])) ? null : $view->input['status'];
    $reportDates = $this->utils->reportDates();



    $user = $this->auth->user();
    $account = $this->auth->user()->account;
    $accountID = $account->id;
    $kegs_for_filters  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
    $keg_list=$kegs_for_filters->lists('KegFullName', 'id');


    $device_id_for_default=null;
    if(empty($view->input['deviceID']) && (count($keg_list)) ) {
            //$deviceID=$kegsForSelect[0];
      $device_id_for_default=array_keys($keg_list)[0];

    }

        //print_r($device_id_for_default);



    $filter_device_id = (empty($view->input['deviceID'])) ? null : $view->input['deviceID'];




    $view->with('newGraph',$newGraph)->with('ident_hdn',$ident_hdn)->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)->with('quickDates', $quickDates)->with('reportDates', $reportDates)->with('reportStatus', $reportStatus)->with('status', $reportStatus)->with('deviceID', $filter_device_id)->with('offset_setting',$offset_setting)->with('page_limit',$page_limit);



        //start code for tabs
        //$user = $this->auth->user();
        //$account = $this->auth->user()->account;
        //$accountID = $account->id;
    $accountType = $this->auth->user()->account->accountType;
    $accountSettings = $this->auth->user()->account->accountSetting->first();
    $accountAddress = $this->auth->user()->account->accountAddress->first();

    $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        // code
       // dd($filter_device_id);
    $kegs_for_filter  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
    $kegs  = $this->kegs->getForLevelsCharts($accountID, 'account_id', $accountID,$filter_device_id);


    $deviceID = $kegs->fetch('id')->toArray();

    $reportStart = new Carbon($reportStart, $timezone->timezone);
    $reportEnd = new Carbon($reportEnd, $timezone->timezone);
    

    $filter['receivedDateTime_start'] = empty($reportStart)?$default_date_start:$reportStart;
    $filter['receivedDateTime_end'] = empty($reportEnd)? $default_date_end:$reportEnd;



    $tempHistory = $this->kegdata->getTempHistory($deviceID,$offset_setting, $page_limit ,$filter);
      
    $pourHistory = $this->kegdata->getPourHistoryDemo($deviceID,0, 300 ,$filter);

        //get date interval to send it to level js
    $date_interval_array = $this->kegdata->getDateIntervals();

    $kegsForSelect = array('' => 'Select Keg') + $kegs_for_filter->lists('KegFullName', 'id');
    $this->count = 1;
    $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

     $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
     $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));


     $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
     $this->count++;
     $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
     if($keg->showDevice == 1){

              /* if(!is_null($keg->tapped) && !is_null($keg->recentPour)){
                    $keg->tappedDiff = $keg->tapped->sent_at->diffInSeconds($keg->recentPour->sent_at, false);
                    if($keg->tappedDiff < 0){
                        $keg->recentPour->adc_pour_end = 2250;
                    }
               }else{
                   $keg->tappedDiff  = null;
                 }*/

                 if(!is_null($keg->tapped)){
                  $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->recentPour)){
                  $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                  if($keg->recentPour->ounces  < 0){
                    $keg->recentPour->ounces = 0;
                  }
                  $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                  $keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                  $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->temperature)){
                  $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                  $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                    $temp->name = $keg->deviceName;
                    $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                  });
                }

                if(!is_null($keg->pourHistory)){

                //empty session

               // Session::set('last_pour', null);
               // Session::set('last_pour_status', null);

                  $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                    $pour->name =  $keg->deviceName;
                    $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y - g:i a');
                    $pour->length = $pour->pour_length;
                    $pour->adc_pour = $pour->adc_pour;
                    $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

                    $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);
                    //$this->graph_arr[$pour->name][] =$pour->level;

                    //start pesudo step from here it return true false

                   // Select the value of lsm_m from kegdevice table
                    $lsm_m = DB::table('kegdevices')->select('lsm_m') 
                    ->where('id', '=',$pour->deviceID)->first();
                    $rerurn_data=$this->kegdata->pseudo_steps_first($pour,$lsm_m);


                    if($rerurn_data==1){

                        //push level for lsm calculation input in lsm
                     $this->graph_arr[$pour->name][] =$pour->level;
                     $this->graph_arr_x_axis[$pour->name][] =$pour->length;
                     
                   } else if($rerurn_data=='clear') {

                       //Session::set('last_pour', null);
                       //Session::set('last_pour_status', null);
                      //unset array
                      //for ($i = 0; $i < count($this->graph_arr[$pour->name]); $i++) { unset($this->graph_arr[$pour->name][$i]); }
                    if(isset($this->graph_arr[$pour->name])) {
                      while(count($this->graph_arr[$pour->name]) >0) {
                        array_pop($this->graph_arr[$pour->name]);
                      }
                    }

                    if(isset($this->graph_arr_x_axis[$pour->name])) {
                      while(count($this->graph_arr_x_axis[$pour->name]) >0) {
                        array_pop($this->graph_arr_x_axis[$pour->name]);
                      }
                    }


                  }
                  


                  $pour->is_valid_pour=$rerurn_data;



                });
}

            //dd($this->graph_arr[$keg->deviceName]);
       
        $search_data = Input::get();
        if(isset($search_data['submit']) && $search_data['reportStart'] != null){

          $search_start_date = new Carbon($search_data['reportStart']);
          $search_end_date =new Carbon($search_data['reportEnd']) ;
          $main_date_array_after_filter  ="";
          $main_pour_array_after_filter = "";
          $data_for_filter  = $this->lsmvalue-> where('kegdevice_id',$keg->id)
          ->orderBy('created_at', 'desc')
          ->first();
          if(isset($data_for_filter) && !empty($data_for_filter)){


          $filter_array =  json_decode($data_for_filter->pour_date);
          $filter_array_simple_x  =  json_decode($data_for_filter->simple_x);
           
          $filter_date_end_key = "";
          $filter_date_start_key= "";
          foreach ($filter_array->pour_date as $key => $value) {
            if( new Carbon($value) >= $search_start_date  ){
              if(empty($filter_date_start_key)){

               $filter_date_start_key  = $key;

             }
           }
           if( new Carbon($value) >= $search_end_date  ){
            if(empty($filter_date_end_key)){
              $filter_date_end_key  = $key;

            }

          }

        }

        if(empty( $filter_date_end_key)){
          $filter_date_end_key = count($filter_array->pour_date);
        }

        foreach ($filter_array->pour_date as $key => $my_array ) {

          if( $filter_date_start_key  <= $key &&  $filter_date_end_key >= $key  ){
            $main_date_array_after_filter['pour_date'][]= $my_array ;
          } 
        }

        foreach ($filter_array_simple_x->pour_x as $key => $simpel_x) {

          if( $filter_date_start_key  <= $key &&  $filter_date_end_key >= $key){

           $main_pour_array_after_filter['pour_x'][]= $simpel_x ;

         }

       }



        }

      }
         $lsmvalue_all  = $this->lsmvalue-> where('kegdevice_id',$keg->id)
               ->orderBy('created_at', 'desc')
                ->take(1)
               ->get();
              
             // dd(json_decode($lsmvalue_all['0']['simple_x']));
              if(isset( $lsmvalue_all)){
                foreach ($lsmvalue_all as $lsmValue ) {
                  $lsm_data1['b'] = $lsmValue->b;
                  $lsm_data1['m'] = $lsmValue->m;
                  /* Check if filter set */
                  if(!empty($main_date_array_after_filter)){
                    $lsm_data1['simple_x']= json_encode($main_pour_array_after_filter) ; 
                    $lsm_data1['pour_date']= json_encode($main_date_array_after_filter); 

                  }else{
                    $lsm_data1['simple_x']= $lsmValue->simple_x ; 
                    $lsm_data1['pour_date']= $lsmValue->pour_date ;  
                  }

                  $keg->lsm_data= $lsm_data1;
                }
            }
             
// if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {
//                  //dd($this->graph_arr[$keg->deviceName]);
//  $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);
//  $keg->lms_data= $lms_data1;
// } else {
//   $keg->lms_data=$lms_data1;
// }


}


});


$view->withKegs($kegs)->with('kegdata', $this->kegdata )->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);

        ////end code for tabs



}

    /**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
    public function linear_regression($y,$x) {

      $y = array_values($y);
      $x = array_values($x);

      $count=count($y);



    /*if($count>9) {
        $median=floor($count/2);    
        $temp=array();
        $temp[]=$y[0];
        $temp[]=$y[1];
        $temp[]=$y[2];
        
        if($median) {
         $temp[]=$y[$median];
         $temp[]=$y[$median+1];
         $temp[]=$y[$median+2];
        }
        
        $temp[]=$y[$count-3];
        $temp[]=$y[$count-2];
        $temp[]=$y[$count-1];
        
        unset($y);
        $y=$temp;
        
      }*/
      
  //find x axis array
  //$x= range(1,count($y));



   // do x axis comulnative
      $t=0;
    //assume $temp as x axis array
      $temp=array();
      for($i=0;$i<count($x);$i++) {
        $add=$t+$x[$i];
        $t=$add;
        $temp[$i]=$t;
      }
    //assign temp into x
      $x=$temp;




    // calculate number points
      $n = count($x);

  // ensure both arrays of points are the same size
      if ($n != count($y)) {

        trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

      }

  // calculate sums
      $x_sum = array_sum($x);
      $y_sum = array_sum($y);

      $xx_sum = 0;
      $xy_sum = 0;

      for($i = 0; $i < $n; $i++) {

        $xy_sum+=($x[$i]*$y[$i]);
        $xx_sum+=($x[$i]*$x[$i]);

      }

  // calculate slope
      if((($n * $xx_sum) - ($x_sum * $x_sum))) {
        $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
      } else {
        $m=0;
      }


  // calculate intercept
      if($n) {
        $b = ($y_sum - ($m * $x_sum)) / $n;  
      } else {
        $b=0;
      }


  // return result
      return array("m"=>$m, "b"=>$b);

    }





  }