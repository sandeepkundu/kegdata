<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentPromotionRepository as Promotion;
use Utilities;
use Illuminate\Contracts\Auth\Guard;

class EmployeePromotionComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Promotion $promotion)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->promotions = $promotion;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 
         $viewData = $view->getData();
        $promotionID = $viewData['id'];

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $promotions = $this->promotions->with('photos')->get();

        if($promotionID != 0){
            $promotion = $promotions->find($promotionID);
        }else{
            $promotion = null;
        }

        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->withPromotions($promotions)->withPromotion($promotion);
    }

}