<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use Utilities;
use Illuminate\Contracts\Auth\Guard;

class EmployeeUserComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }
        $accounts = $this->account->getAll();
        $users = $this->user->getAll();
        
        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->with('selectAccts', $this->account->getForSelect('accountName'))->withAccounts($accounts)->withUsers($users);
    }

}