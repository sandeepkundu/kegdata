<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Models\Notification_Frequency as Freq;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use Illuminate\Contracts\Auth\Guard;

class NotificationsComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Freq $freq, Keg $keg ,Guard $auth, KegData $kegdata, BrewDist $brewDist)
    {
                $this->auth = $auth;
     	          $this->freq = $freq;
                $this->kegs = $keg;
                $this->kegdata = $kegdata;
                 $this->count = 1;       
                 $this->brewDist = $brewDist;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $temperatureType=$account->accountSetting->first()->temperatureType;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        if($accountType == 'B' || $accountType == 'D'){
             $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
            $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
            $kegs = $this->kegs->getForNotifications($brewDist->id, $column);
        }else{
            $kegs = $this->kegs->getForNotifications($user->account->id, 'account_id');
        }
        

        $kegs->each(function($keg){
            $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
            $this->count++;
        });

         $kegsForSelect = array('' => 'Select Keg','2'=>'All') + $kegs->lists('KegFullName', 'id');


       // $freqs = $this->freq->where('id', '>=', '6')->orderBy('id', 'DESC')->get(['id', 'freqDesc'])->lists('freqDesc','id');

          $freqs = $this->freq->whereIn('id',[11,9,8,16])->orderBy('id', 'DESC')->get(['id', 'freqDesc'])->lists('freqDesc','id');


        $freqTypes = [
            'L' => 'Low Level',
            'E' => 'Empty Keg',
            'U' => 'Keg was untapped',
            'P' => 'Pour Conducted after hours',
            'T' => 'Temperature',
            'K' => 'Keg blow',
            'B' => 'Boot event'
        ];


        $view->with('freqs', $freqs)->with('freqTypes', $freqTypes)->with('kegs', $kegs)->with('user', $user)->with('kegdata', $this->kegdata)->with('kegsForSelect', $kegsForSelect)->with('temperatureType',$temperatureType);
    }


}       



