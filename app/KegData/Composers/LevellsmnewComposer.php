<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use App\KegData\Models\HubDevice as HubDevice;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
use DB;
use App\KegData\Models\LsmValue as LsmValue;
use App\KegData\Models\BeerStyle as BeerStyle;

class LevellsmnewComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, LsmValue $lsmvalue, Utilities $utils,BeerStyle $beerStyle,HubDevice $HubDevice)
    {
      $this->auth = $auth;
      $this->kegdata = $kegData;
      $this->collection = $collection;
      $this->kegs = $kegs;
      $this->timezone = $timezone;
      $this->count = 0;
      $this->accountSetting = $accountSetting;
      $this->lsmvalue= $lsmvalue;
      $this->utils = $utils;
      $this->beerStyle = $beerStyle;
      $this->HubDevice = $HubDevice;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
      $user = $this->auth->user();
      $lastlogin = $user->lastlogin;
      $account = $this->auth->user()->account;
      $accountID = $account->id;
      $accountType = $this->auth->user()->account->accountType;
      $accountSettings = $this->auth->user()->account->accountSetting->first();
      
      $accountAddress = $this->auth->user()->account->accountAddress->first();

      $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

      $kegs  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);

      $deviceID = $kegs->fetch('id')->toArray();
      $tempHistory = $this->kegdata->getTempHistory($deviceID);
      $pourHistory = $this->kegdata->getPourHistory($deviceID);

      $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id');
      $this->count = 1;

       //loop started for each keg
      $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){
        
        if(isset($keg->style)) {
          $style_result=$this->beerStyle->where('id','=',$keg->style)->select('name')->first();
          $keg->style_name = $style_result;
          
        } 

        //set hubMac
        $account_id = $keg->account_id;
        $hubMac=$this->HubDevice->where('account_id', '=', $account_id)->select('hubMac')->first();
        //echo "====>".$hubMac->hubMac;
        $keg->hubMac = isset($hubMac->hubMac) ? $hubMac->hubMac : '' ;
        if(!empty($keg->hubMac)) {
          $keg->hubMac=preg_replace("/:/", "", $keg->hubMac);
          $keg->hubMac = substr($keg->hubMac, -4);
        }
        

        //calling utility helper functions to get full volume
        //function params keg_type_id and measurement value oz or ml
        $keg_type_result=$this->utils->get_keg_type_as_per_measurement($keg->kegType,$accountSettings->volumeType);
        $keg_type_amount_value=1984;
        $keg_type_empty_value=2252;
        if(!empty($keg_type_result)) {
          $keg_type_amount_value=$keg_type_result->amount;
          $keg_type_empty_value=$keg_type_result->emptyValue;
        }
       $keg->keg_type_amount_value=$keg_type_amount_value;
        $keg->keg_type_empty_value=$keg_type_empty_value;
        // end utility function 


        //start last most recent untap event code
        $getLastDataEvent  = $this->kegdata->mostRecentUntapEvent($keg->id);
        $keg->getLastDataEvent= $getLastDataEvent;
        //echo "===last event==>".$keg->getLastDataEvent->status;

        //get last break event formlsm_values table
        $last_break_from_lsmvaue_table = $this->kegdata->lastBreakFromLsmVaue($keg->id);
        $lsm_values_last_break_event_date= isset($last_break_from_lsmvaue_table->break_event_date) ? new Carbon($last_break_from_lsmvaue_table->break_event_date) : "";

        $last_break_m_value = isset($last_break_from_lsmvaue_table->m) ? $last_break_from_lsmvaue_table->m: 0.15;
       
        //if first time lsm_values are empty
        $break_date_save_forlsm_table = '';

        if(empty($lsm_values_last_break_event_date)) {

          /* Function for get all pour after recent break date  */
            //get 15 days back date
            $first_date =Carbon::now()->subDays(9);
            //echo "==>".$first_date;
            //get 13 days back date
            //$second_date =Carbon::now()->subDays(9);
            $second_date =Carbon::now();
            
            $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$first_date,$second_date);
            $break_date_save_forlsm_table = $first_date;

        } else { //find out next break event date and its data

          //find out next date of break event from kegdata
         $result = $this->kegdata->find_next_date_of_break($keg->id,$lsm_values_last_break_event_date);

         $next_break_date = (isset($result->sent_at)) ?new Carbon($result->sent_at) : '';
         //dd($next_break_date);

         /* Function for get all pour after recent break date  */

         if(empty($next_break_date) ){ // check if next break event is NULL 
          
           //find out data till today
          $dataAfterLastBreak = $this->kegdata->pourInCaseOfNoNextBreakFound($keg->id,$lsm_values_last_break_event_date,new Carbon());
          //dd($dataAfterLastBreak);

          $break_date_save_forlsm_table = $lsm_values_last_break_event_date;

         }
         else {

          ////find out next to next break event date
          $result_next_to_next = $this->kegdata->find_next_date_of_break($keg->id,$next_break_date);
           //dd($result_next_to_next);
          //if there is no break date found from next to next then
          //fetch data from till date
          $next_to_next_break_date = (isset($result_next_to_next->sent_at)) ? new Carbon($result_next_to_next->sent_at) : new Carbon();
          //check if next to next break event status is blow event
          $next_to_next_is_blow_event_satus = (isset($result_next_to_next->status)) ? $result_next_to_next->status : null;
          $keg->is_next_to_next_is_blow_event =$next_to_next_is_blow_event_satus;
          //end code for find out next to next break event date

          $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$next_break_date,$next_to_next_break_date);

          $break_date_save_forlsm_table = $next_break_date;

        }
      }
      /* End */
      //dd($break_date_save_forlsm_table);
      //find result on this break_event_Date because we need to check that if the status is 10011 then we add this data in the lsm use further in code

      $result_break_date_check = $this->kegdata->check_status_of_break_date($keg->id,$break_date_save_forlsm_table);
      
       //end code for find break date status

      $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
      $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));
         //$keg->setRelation('dataAfterLastBreak', $dataAfterLastBreak ->where('deviceID', $keg->id));
         //$keg->setRelation('getLastBreak', $getLastBreak ->where('deviceID', $keg->id));


      $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
      $this->count++;
      $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
         //$last_break=   new Carbon($getLastBreak->created_at);
       $keg->lastBreak = $break_date_save_forlsm_table;
       //$keg->lastBreak = $break_date_save_forlsm_table;

        //check if there is pour data come start lsm
              if($dataAfterLastBreak->count()){
                //dd($dataAfterLastBreak);
                $is_keg_blow ="no";
                $dataAfterLastBreak->each(function($pour) use($is_keg_blow,$keg, $timezone, $accountSettings,$last_break_m_value){
                  $pour->name =  $keg->deviceName;

                  $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                  
                  $pour->length = $pour->pour_length;
                  $pour->adc_pour = $pour->adc_pour;
                  $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

                  $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);

                  $lsm_m = (object) array("lsm_m"=>$last_break_m_value);
                  $rerurn_data=$this->kegdata->pseudo_steps_first($pour,$lsm_m);

                  if($rerurn_data==1 || $rerurn_data==true){
                    //push level for lsm calculation input in lsm
                    $this->graph_arr[$pour->name][] =$pour->level;
                    $this->graph_arr_x_axis[$pour->name][] =$pour->length;
                    $this->adc_pour_end_lsm[$pour->name][] =$pour->adc_pour_end;

                    $this->graph_arr_x_axis_date[$pour->name][] =$pour->date;
                    $pour->is_valid_pour=$rerurn_data;

                  } 

                  //$pour->is_valid_pour=$rerurn_data;
                });
        }//End IF to check if there is pour data come start lsm

        // now to check if the break_date status is 10011 then save it also in lsm

        $brek_event_status = (!empty($result_break_date_check)) ? $result_break_date_check->status : null;
        
        if( $brek_event_status == '10011'){
          $this->graph_arr[$keg->deviceName][] =0;
          $this->graph_arr_x_axis[$keg->deviceName][] =$result_break_date_check->pour_length;
          $this->adc_pour_end_lsm[$keg->deviceName][] =$result_break_date_check->adc_pour_end;

          $new_brek_date = $result_break_date_check->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
          $this->graph_arr_x_axis_date[$keg->deviceName][] = $new_brek_date;

        }

        //find m and b from graph array 
        if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {

         $keg->valid_pour_count =count($this->graph_arr[$keg->deviceName]);

         $lsm_pour_date['pour_date'] = $this->graph_arr_x_axis_date[$keg->deviceName] ;
         $pour_date = json_encode($lsm_pour_date);
         $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);
         //print_r($lms_data);
         //dd($brek_event_status);
         /* New m and b for find value of y for new formula */
         $lms_data_new  = $this->linear_regression_adc_pour_end($this->graph_arr_x_axis[$keg->deviceName],$this->adc_pour_end_lsm[$keg->deviceName],$keg->is_next_to_next_is_blow_event);
         $keg->lms_data_for_adc_end=$lms_data_new;
      

        $positive_m = abs($lms_data['m']) ;
        $comm_x = $lms_data['json_x'];
        $y = floor($positive_m*$lms_data['commulative_x']);

       $keg_level = $lms_data['b']- $y  ;// Find keg level according to lsm 
       
       $keg->value_y= $keg_level ;// Show keg leve acording to lsm
       $keg->lms_data=$lms_data;
     } else {
      $keg->lms_data=null;
    }

    /* End*/
    if($keg->showDevice == 1){  
                 
                if(!is_null($keg->recentPour)){

                  //fetch last untapped date
                  $tapped_in_utc=$keg->tapped->sent_at;
                  $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                  
                  //fetch next tapped date after untapp
                  $tapped_result = $this->kegdata->get_most_event_result($keg->id,'1001',$tapped_in_utc);

                  $keg->tapped->second_var = isset($tapped_result->sent_at) ? $tapped_result->sent_at->timezone($timezone->timezone):'';

                  $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                  if($keg->recentPour->ounces  < 0){
                    $keg->recentPour->ounces = 0;
                  }


                  $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                  //comment this code as client provide new formula
                  //$keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);

                  //new percentage formula
                  $keg->recentPour->percentage = $this->kegdata->kegPercentageChanged($keg->recentPour->adc_pour_end,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984,isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252,$accountSettings->volumeType);

                  //new percentage formula for image
                  $keg->image = $keg->recentPour->percentage;

                  /* Condition for check if Pour less grater then 2 then show level ang Percetage according to this*/

                  if($keg->valid_pour_count){

                    $lsm_b =   $keg->lms_data['b'] ;
                    
                    //$keg->recentPour->total =  floor($keg->value_y).' '.$accountSettings->volumeType;
                    //code start here to check if there is greater then 10 valid pour and cum_x is greater then 1000
                    //then use previous m if condition is not true
                    $m_val=0;
                    //code to check 1000 millisecond for cum_x
                    $simple_x_array = json_decode($comm_x);
                    $t=0;
                    $temp=[];
                    $data_arr=$simple_x_array->pour_x;
                    for ($i=0;$i < (count($data_arr));$i++) {
                      $t=$t+$data_arr[$i];
                      $temp[]=$t;
                    }
                    $total_of_cum_x = array_sum($temp);

                    if( ($keg->valid_pour_count < 10) || ($total_of_cum_x < 1000) ) {
                    //dd($last_break_m_value);
                    //echo "==aplly last break Value=>".$last_break_m_value;
                    $m_val = $last_break_m_value;

                  } else {
                    //echo "<=====in else======>";
                    $m_val = $keg->lms_data_for_adc_end['m'];
                  }
                  
                  //set value of b on view with some formulas
                  //formula is (full keg volume in desired units)/(ADCempty-ADCfull)*m_adc
                   if(isset($keg->recentPour)) {

                  //calling utility helper functions to get full volume
                    //$keg_type_amount_value = $this->utils->get_volume_value($accountSettings->volumeType);
                    $keg_type_amount_value=$keg->keg_type_amount_value;
                    $keg->actual_value_b_from_step_2 = !empty($keg->lms_data_for_adc_end['b']) ? $keg->lms_data_for_adc_end['b']:0;

                    $first_calculation =($keg_type_amount_value)/((($keg->keg_type_empty_value) - 350));
                    
                    $keg->new_value_of_b = $first_calculation *  $keg->lms_data_for_adc_end['b'];

                    $keg->new_flow_rate = $first_calculation *  $m_val;

                  } else {
                    $keg->new_value_of_b = 0;
                    $keg->new_flow_rate = 0;
                  }

                  /* function for Beer remaining using LSM + New Formul on May 05 2017 */
                  
                  $ouncess = $this->kegdata->getOuncesLsm($keg->recentPour->amount,$keg->volumeType); 

                  $new_adc_pour_end = $this->kegdata->adcPourEnd($keg); 

                  $beerRem = $this->kegdata->beerRemainningUsingLsmNewFormula($keg,$ouncess,$new_adc_pour_end); 
                  
                  $keg->beerlft= $beerRem;

                  /* End code */

                } else { //else there are no valid pours
                  
                  //find flow rate
                  //calling utility helper functions to get full volume
                  //$keg_type_amount_value = $this->utils->get_volume_value($accountSettings->volumeType);
                  $keg_type_amount_value=$keg->keg_type_amount_value;
                  $first_calculation =($keg_type_amount_value)/((($keg->keg_type_empty_value) - 350));
                    
                  $keg->new_value_of_b = 0;
                  $keg->new_flow_rate = $first_calculation *  $last_break_m_value;
                    
                } //end else conditon for no valid pour

                  $all_lsm_data = $this->lsmvalue->where('kegdevice_id', '=', $keg->id)->get();
                  $keg->allLsmData =  $all_lsm_data;
                  
                    $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                  }

                  if(!is_null($keg->temperature)){
                    $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                    $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                      $temp->name = $keg->deviceName;
                      $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                      $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                    });
                  }

                  if(!is_null($keg->pourHistory)){
                    $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                      $pour->name =  $keg->deviceName;
                      $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                      $pour->length = $pour->pour_length;
                      $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);
                    });
                  }
                }

                
              }); 

     
      $view->withKegs($kegs)->with('utills',$this->utils)->with('kegdata', $this->kegdata)->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);
}

      /**
     * linear regression function
     * @param $x array x-coords
     * @param $y array y-coords
     * @returns array() m=>slope, b=>intercept
     */
      public function linear_regression($y,$x) {

        $y = array_values($y);
       
        $new_x['pour_x'] = array_values($x);
         $x =  array_values($x);

         /* 
        For demo only 23-May-2017
          */
       $new_y['pour_y'] = array_values($y);
       $json_y = json_encode( $new_y);
     //   dd($json_y); 
      /* End demo 23-May */


       $json_x = json_encode($new_x);
      
        $commulative_x = array_sum($x);          
       
        $count=count($y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
          $add=$t+$x[$i];
          $t=$add;
          $temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($y)) {

          trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

          $xy_sum+=($x[$i]*$y[$i]);
          $xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
          $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
          $m=0;
        }


  // calculate intercept
        if($n) {
          $b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
          $b=0;
        }

  // return result
        return array("m"=>$m, "b"=>$b,"commulative_x"=>$commulative_x,"json_x"=>$json_x,' json_y'=> $json_y );

      }



        /**
 * linear regression adc pour end  function 
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
      public function linear_regression_adc_pour_end($x,$new_y,$if_blow_event_set) {
        if(!empty($if_blow_event_set) && $if_blow_event_set == '10011')
        {
           array_pop($x);
           array_pop($new_y);
        }
        $new_y = array_values($new_y);
        
        $new_x['pour_x'] = array_values($x);
        $new_y1['pour_y'] =  $new_y  ;
         $x =  array_values($x);
      
        $json_x = json_encode($new_x);
        $json_y  = json_encode(  $new_y1);
        //$commulative_x = array_sum($x);          

        $count=count($new_y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
          $add=$t+$x[$i];
          $t=$add;
          $temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($new_y)) {

          trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($new_y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

          $xy_sum+=($x[$i]*$new_y[$i]);
          $xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
          $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
          $m=0;
        }


  // calculate intercept
        if($n) {
          $b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
          $b=0;
        }
    
  
        return array("m"=>$m, "b"=>$b,"json_x"=>$json_x,"json_y"=>$json_y);

      }

    }
