<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;

class BDLevelComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, BrewDist $brewDist)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->brewDist = $brewDist;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 
        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $accountID = $this->auth->user()->account->id;
        $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
       $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
        $kegs  = $this->kegs->getForBrewDist($brewDist->id, $column, $accountID);

        $deviceID = $kegs->fetch('id')->toArray();
        

        $tempHistory = $this->kegdata->getTempHistory($deviceID);
        $pourHistory = $this->kegdata->getPourHistory($deviceID);

        $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id');

        $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

            $this->count = 1;
             $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
            $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));
            
            $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
            $this->count++;
            $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
            if($keg->showDevice == 1){
               
            /*   if(!is_null($keg->tapped) && !is_null($keg->recentPour)){
                    $keg->tappedDiff = $keg->tapped->sent_at->diffInSeconds($keg->recentPour->sent_at, false);
                    if($keg->tappedDiff < 0){
                        $keg->recentPour->adc_pour_end = 2250;
                    }
               }else{
                   $keg->tappedDiff  = null;
               }*/

                if(!is_null($keg->tapped)){
                    $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                
                if(!is_null($keg->recentPour)){

                    $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                    if($keg->recentPour->ounces  < 0){
                        $keg->recentPour->ounces = 0;
                    }
                    $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                    $keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                    $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }


                $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType);

                $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                    $temp->name = $keg->deviceName;
                    $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType);
                });

                $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                    $pour->name =  $keg->deviceName;
                    $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $pour->length = $pour->pour_length;
                    $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);
                });
            }
            
           

        });

        $view->withKegs($kegs)->with('kegdata', $this->kegdata)->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);
    }

}