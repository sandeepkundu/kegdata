<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\MessageBag;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use Utilities;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentOrderRepository as Order;
use App\KegData\Repositories\EloquentKegDeviceRepository as KegDevice;
use Cart as Cart;
use Carbon\Carbon;
use App\KegData\Models\OrderItem as OrderItem;
use App\KegData\Models\Product as Product;
use App\KegData\Models\Plan as PlanModel;
use DB ;
class CheckoutComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth,  Utilities $utils,OrderItem $OrderItem, Category $categories, Account $account, Plan $plan, Order $order, KegDevice $kegdevice)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->utils = $utils;
            $this->categories = $categories;
            $this->plans = $plan;
            $this->orders = $order;
            $this->kegdevice = $kegdevice;
            $this->orderItem =$OrderItem;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = $this->auth->user();
        $account = $user->account;
        $accountAddress = $user->account->accountAddress->first();

        $categories = $this->categories
                        ->where('is_navigation', '=', 1)
                        ->get();
        $cartItems = Cart::instance('shopping')->content();

        $sys_count = 0;
       //to check if system taken already
       if($this->auth->user()) {
          $order = $this->auth->user()->account->Order;
           $categorySlug = 'systems';
           $current_cat = $this->categories->where('slug', $categorySlug)->first();
           
           if( ($current_cat) && ($current_cat->name == "Systems")) {
              $system_id = $current_cat->products['0']['id'];

              foreach ( $order as $orderId) {
                  if($orderId->status == 'charged') {

                     $model = OrderItem::where('order_id', '=', $orderId->id)
                     ->where('product_id', '=', $system_id)->get();

                    //check if system add in cart
                     $cart_product_array=array();
                     foreach($cartItems as $item){
                       $cart_product_array[] = $item->id;
                     }


                     if($model && in_array($system_id,$cart_product_array) ){
                       $sys_count++;
                     }


                  }
                 

             }
           } 
           

       }






        $cartSub = Cart::instance('subscription')->content();
        $cartCount = Cart::instance('shopping')->count() + Cart::instance('subscription')->count();
        $cartTotal = Cart::instance('shopping')->total(2,'.','');

        $subscription = $user->account->subscription;
        /*$kegs = $this->kegdevice
                    ->where('showDevice', '=', 1)
                    ->where('account_id', '=', $user->account_id)
                    ->count();

        $kegcount = $kegs;*/

        //find kegcount
       $kegcount=0;
       foreach($cartItems as $items){
          $item = Product::find($items->id); 
          $category=$item->category; 
          if($category->where('slug','couplers')->count()){
              $kegcount += $items->qty;
          }
        }

         /* Code for totalm coupler purchage in last order*/

      $userOrder_coupler_total= 0;
      if($this->auth->user()){
        $categorySlug = 'couplers';
        $current_cat = $this->categories->where('slug', $categorySlug)->get();

        $prodCat = DB::table('category_product')
        ->where('category_id', '=', $current_cat['0']['id'])
        ->get();
        foreach ($prodCat as $prodCat_id) {
          $current_user_order =  $this->auth->user()->account->Order()
            ->where('status', '=', 'charged')
            ->orderBy('id','desc')
            ->limit('1')->get(); 
       
          foreach ($current_user_order as $currentUseOredr) {

            $userOrder_item= $this->orderItem
                       // ->select('quantity')
            ->where('order_id','=', $currentUseOredr->id)
            ->where('product_id','=',$prodCat_id->product_id)
            ->orderBy('id','desc')
            ->limit('1')
            ->get();
           
            foreach ($userOrder_item as $qty) {
              if ($qty) {

               $userOrder_coupler_total+= $qty->quantity;
             }
           }


         }   

       }
     }




      /* Code for  get coupler which already purchaged by thr user
      */
        if(!empty($this->auth->user())){
         $user_subscribe_plan = $this->auth->user()->account->Plan;
         if($user_subscribe_plan) {
           if($user_subscribe_plan->max == 1 && $kegcount >0){
            $kegcount =  $kegcount  +  $userOrder_coupler_total;
          }else if($user_subscribe_plan->max == 2 && $kegcount >0){
            $kegcount =  $kegcount  +  $userOrder_coupler_total;
          }else if($user_subscribe_plan->max == 3 && $kegcount >0){
            $kegcount =  $kegcount  +  $userOrder_coupler_total;
          }
        }// End $user_subscribe_plan if
      }

      //check if the plan is commercial then minus keg count by three
      
      foreach ($cartSub as $sub) {
        $plan=PlanModel::find($sub->id);
        if( ($plan) && ($plan->plan_type == 'Commercial') ) {

          if($kegcount > 3) {
            $kegcount = $kegcount;
          }
        }
      }

      
        



        //foreach($cartItems as $item){
          //  $category = $item->model->category()->get();
            /**
             * Total Keg Count between active kegs & cart couplers
             */
            //if($category->where('slug','couplers')->count()|| $category->where('slug','systems')->count()){
              //   $kegcount += $item->qty;
            //}
        //}

        $messages = new MessageBag;

        $correctPlan = null;

        $tax = ($accountAddress->stateCode == 'TX') ? ($cartTotal * .0825) : 0;
        $shipping = ($cartTotal < 199) ? ($cartTotal * .2) : ($cartTotal * .1);


        $view->with('user', $user)
            ->with('now', Carbon::now())
            ->with('cartCount', $cartCount)
            ->with('cartTotal', $cartTotal)
            ->with('cartItems',$cartItems)
            ->withCategories($categories)
            ->withAccount($account)
            ->with('accountAddress', $accountAddress)
            ->withSubscription($subscription)
            ->with('correctPlan', $correctPlan)
            ->withErrors($messages)
            ->with('countries', $this->utils->get_countries())
            ->with('states', $this->utils->get_states())
            ->with('cartSub',$cartSub)
            ->withTax($tax)
            ->withShipping($shipping)
            ->with('kegcount', $kegcount)
            ->with('sys_count', $sys_count);

}

}
