<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
use DB;
use App\KegData\Models\LsmValue as LsmValue;

class Levelv1Composer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, LsmValue $lsmvalue, Utilities $utils)
    {
      $this->auth = $auth;
      $this->kegdata = $kegData;
      $this->collection = $collection;
      $this->kegs = $kegs;
      $this->timezone = $timezone;
      $this->count = 0;
      $this->accountSetting = $accountSetting;
      $this->lsmvalue= $lsmvalue;
      $this->utils = $utils;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
      $user = $this->auth->user();
      $lastlogin = $user->lastlogin;
      $account = $this->auth->user()->account;
      $accountID = $account->id;
      $accountType = $this->auth->user()->account->accountType;
      $accountSettings = $this->auth->user()->account->accountSetting->first();
      
      $accountAddress = $this->auth->user()->account->accountAddress->first();

      $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

      $kegs  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);

      $deviceID = $kegs->fetch('id')->toArray();
      $tempHistory = $this->kegdata->getTempHistory($deviceID);
      $pourHistory = $this->kegdata->getPourHistory($deviceID);

      $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id');
      $this->count = 1;

       //loop started for each keg
      $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

        //get last break event formlsm_values table
        $last_break_from_lsmvaue_table = $this->kegdata->lastBreakFromLsmVaue($keg->id);
        $lsm_values_last_break_event_date= isset($last_break_from_lsmvaue_table->break_event_date) ? new Carbon($last_break_from_lsmvaue_table->break_event_date) : "";

        //start last break event code
        $getLastBreak = $this->kegdata->lastBrekEvent($keg->id);

        $lastBreakBackDate = isset($getLastBreak->created_at) ? new Carbon($getLastBreak->created_at) : new Carbon();

        //if first time lsm_values are empty
        $break_date_save_forlsm_table = '';

        if(empty($lsm_values_last_break_event_date)) {
          
          /* Function for get all pour after recent break date  */
          $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakLSMValue($keg->id,$lastBreakBackDate);
          $break_date_save_forlsm_table = $lastBreakBackDate;

        } else { //find out next break event date and its data

          //find out next date of break event
         $result = $this->kegdata->find_next_date_of_break($keg->id,$lsm_values_last_break_event_date);


         $next_break_date = (isset($result->created_at)) ?new Carbon($result->created_at) : '';
         //dd($next_break_date);

         /* Function for get all pour after recent break date  */

         if(empty($next_break_date) ){ // check if next break event is NULL 

           $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$lsm_values_last_break_event_date,new Carbon());

           $break_date_save_forlsm_table = $lsm_values_last_break_event_date;

         }
         else {

          $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$lsm_values_last_break_event_date,$next_break_date);

          //because on less then five result we can not find valid pour
          if($dataAfterLastBreak->count() < 5) {
            //find out next date of break event
            $result = $this->kegdata->find_next_date_of_break($keg->id,$next_break_date);
            
            $next_break_date = (isset($result->created_at)) ?new Carbon($result->created_at) : '';
            
            $dataAfterLastBreak = $this->kegdata->pourAfterLastBreakBetweenTwoDates($keg->id,$lsm_values_last_break_event_date,$next_break_date);
          }
          $break_date_save_forlsm_table = $next_break_date;

        }
      }
      /* End */


      $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
      $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));
         //$keg->setRelation('dataAfterLastBreak', $dataAfterLastBreak ->where('deviceID', $keg->id));
         //$keg->setRelation('getLastBreak', $getLastBreak ->where('deviceID', $keg->id));


      $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
      $this->count++;
      $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
         //$last_break=   new Carbon($getLastBreak->created_at);
       $keg->lastBreak = $break_date_save_forlsm_table;
       //$keg->lastBreak = $break_date_save_forlsm_table;

        //check if there is pour data come start lsm
              if($dataAfterLastBreak){
                //dd($dataAfterLastBreak);
                $dataAfterLastBreak->each(function($pour) use($keg, $timezone, $accountSettings){
                  $pour->name =  $keg->deviceName;

                  $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                  
                  $pour->length = $pour->pour_length;
                  $pour->adc_pour = $pour->adc_pour;
                  $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

                  $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);

                  $lsm_m='';

                  $rerurn_data=$this->kegdata->pseudo_steps_first($pour,$lsm_m);

                  if($rerurn_data==1){
                    //push level for lsm calculation input in lsm
                    $this->graph_arr[$pour->name][] =$pour->level;
                    $this->graph_arr_x_axis[$pour->name][] =$pour->length;
                    $this->adc_pour_end_lsm[$pour->name][] =$pour->adc_pour_end;

                    $this->graph_arr_x_axis_date[$pour->name][] =$pour->date;

                  } 

                  $pour->is_valid_pour=$rerurn_data;
                });
        }

        //find m and b from graph array 
        if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {

         $keg->valid_pour_count =count($this->graph_arr[$keg->deviceName]);

         $lsm_pour_date['pour_date'] = $this->graph_arr_x_axis_date[$keg->deviceName] ;
         $pour_date = json_encode($lsm_pour_date);
         $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);
         //print_r($lms_data);

         /* New m and b for find value of y for new formula */
         $lms_data_new  = $this->linear_regression_adc_pour_end($this->graph_arr_x_axis[$keg->deviceName],$this->adc_pour_end_lsm[$keg->deviceName]);
         $keg->lms_data_for_adc_end=$lms_data_new;

        //set value of b on view with some formulas
        //formula is (full keg volume in desired units)/(ADCempty-ADCfull)*m_adc
         if(isset($keg->recentPour)) {

        //calling utility helper functions to get full volume
          $keg_type_amount_value = $this->utils->get_volume_value($accountSettings->volumeType);

          $keg->actual_value_b_from_step_2 = !empty($keg->lms_data_for_adc_end['b']) ? $keg->lms_data_for_adc_end['b']:0;

          $first_calculation =($keg_type_amount_value)/((($keg->recentPour->empty) - 350));
          
          $keg->new_value_of_b = $first_calculation *  $keg->lms_data_for_adc_end['b'];

          $keg->new_flow_rate = $first_calculation *  $keg->lms_data_for_adc_end['m'];


        } else {
          $keg->new_value_of_b = 0;
          $keg->new_flow_rate = 0;
        }
      //end set value of b on view with some formulas

        $positive_m = abs($lms_data['m']) ;
        $comm_x = $lms_data['json_x'];
        $y = floor($positive_m*$lms_data['commulative_x']);

       $keg_level = $lms_data['b']- $y  ;// Find keg level according to lsm 
       
       $keg->value_y= $keg_level ;// Show keg leve acording to lsm
       $keg->lms_data=$lms_data;
     } else {
      $keg->lms_data=null;
    }

    /* End*/
    if($keg->showDevice == 1){  
                 if(!is_null($keg->tapped)){
                  $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->recentPour)){
                  $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                  if($keg->recentPour->ounces  < 0){
                    $keg->recentPour->ounces = 0;
                  }


                  $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                  //comment this code as client provide new formula
                  //$keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);

                  //new percentage formula
                  $keg->recentPour->percentage = $this->kegdata->kegPercentageChanged($keg->recentPour->adc_pour_end,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984,isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252,$accountSettings->volumeType);

                  $keg->recentPour->percentage;

                  //new percentage formula for image
                  $keg->image = $keg->recentPour->percentage;

                  /* Condition for check if Pour less grater then 2 then show level ang Percetage according to this*/

                  if($keg->valid_pour_count){

                    $lsm_b =   $keg->lms_data['b'] ;
                    
                    $keg->recentPour->total =  floor($keg->value_y).' '.$accountSettings->volumeType;

                    /* function for Beer remaining using LSM + New Formul on May 05 2017 */
                    
                    $ouncess = $this->kegdata->getOuncesLsm($keg->recentPour->amount,$keg->volumeType); 

                    $new_adc_pour_end = $this->kegdata->adcPourEnd($keg); 

                    $beerRem = $this->kegdata->beerRemainningUsingLsmNewFormula($keg,$ouncess,$new_adc_pour_end); 
                    
                    $keg->beerlft= $beerRem;

                    /* End code */

                    $exist_lsm_value_data  = $this->lsmvalue->where('break_event_date' , '=', $lsm_values_last_break_event_date)->first();


                    /* check if row already exist  then update the row */

                    

                } 

                  $all_lsm_data = $this->lsmvalue->where('kegdevice_id', '=', $keg->id)->get();
                  $keg->allLsmData =  $all_lsm_data;


                    /*if($keg->valid_pour_count >= 10 ){

                      $b = $keg->lms_data['b'];
                      $m = $keg->lms_data['m'];
                      $a=  DB::table('kegdevices')
                      ->where('kegMac', $keg->kegMac)
                      ->where('account_id', $keg->account_id)
                      ->where('id', $keg->id)
                      ->update(['current_m' =>  $m,'current_b' =>  $b]);
                    }*/

                    $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                  }

                  if(!is_null($keg->temperature)){
                    $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                    $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                      $temp->name = $keg->deviceName;
                      $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                      $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                    });
                  }

                  if(!is_null($keg->pourHistory)){
                    $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                      $pour->name =  $keg->deviceName;
                      $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                      $pour->length = $pour->pour_length;
                      $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);
                    });
                  }
                }

                
              }); 

  
      $view->withKegs($kegs)->with('utills',$this->utils)->with('kegdata', $this->kegdata)->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);
}

      /**
     * linear regression function
     * @param $x array x-coords
     * @param $y array y-coords
     * @returns array() m=>slope, b=>intercept
     */
      public function linear_regression($y,$x) {

        $y = array_values($y);
       
        $new_x['pour_x'] = array_values($x);
         $x =  array_values($x);

         /* 
        For demo only 23-May-2017
          */
       $new_y['pour_y'] = array_values($y);
       $json_y = json_encode( $new_y);
     //   dd($json_y); 
      /* End demo 23-May */


       $json_x = json_encode($new_x);
      
        $commulative_x = array_sum($x);          
       
        $count=count($y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
          $add=$t+$x[$i];
          $t=$add;
          $temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($y)) {

          trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

          $xy_sum+=($x[$i]*$y[$i]);
          $xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
          $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
          $m=0;
        }


  // calculate intercept
        if($n) {
          $b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
          $b=0;
        }

  // return result
        return array("m"=>$m, "b"=>$b,"commulative_x"=>$commulative_x,"json_x"=>$json_x,' json_y'=> $json_y );

      }



        /**
 * linear regression adc pour end  function 
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
      public function linear_regression_adc_pour_end($x,$new_y) {

        $new_y = array_values($new_y);
        
        $new_x['pour_x'] = array_values($x);
        $new_y1['pour_y'] =  $new_y  ;
         $x =  array_values($x);
      
        $json_x = json_encode($new_x);
        $json_y  = json_encode(  $new_y1);
        //$commulative_x = array_sum($x);          

        $count=count($new_y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
          $add=$t+$x[$i];
          $t=$add;
          $temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($new_y)) {

          trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($new_y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

          $xy_sum+=($x[$i]*$new_y[$i]);
          $xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
          $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
          $m=0;
        }


  // calculate intercept
        if($n) {
          $b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
          $b=0;
        }
    
  
        return array("m"=>$m, "b"=>$b,"json_x"=>$json_x,"json_y"=>$json_y);

      }

    }
