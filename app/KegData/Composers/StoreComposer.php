<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use Utilities;

class StoreComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Utilities $utils, Category $categories)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->utils = $utils;
            $this->category = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $user = $this->auth->user();

      $categories = $this->category->with('plans')->with('plans.photos')->with('products')->with('products.photos')->get();

      //$cat = $this->category->find(1);
      //dd($cat->products->first()->photos);

       $view->with('user', $user)->withCategories($categories);


    }


}
