<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
use DB;

class LevelComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting)
    {
      $this->auth = $auth;
      $this->kegdata = $kegData;
      $this->collection = $collection;
      $this->kegs = $kegs;
      $this->timezone = $timezone;
      $this->count = 0;
      $this->accountSetting = $accountSetting;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user =
      $user = $this->auth->user();

      $account = $this->auth->user()->account;
      $accountID = $account->id;
      $accountType = $this->auth->user()->account->accountType;
      $accountSettings = $this->auth->user()->account->accountSetting->first();
      $accountAddress = $this->auth->user()->account->accountAddress->first();

      $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

      $kegs  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);

      $deviceID = $kegs->fetch('id')->toArray();



        /*  
        find out what the most recent break event was (not 11 and not 1001).  
        */

        /*$getLastBreak = $this->kegdata->lastBrekEvent($deviceID);
        dd($getLastBreak);
        if(!$getLastBreak->isEmpty()  ) {  
          $lastBreakBackDate = new Carbon($getLastBreak['0']['created_at']);  
        }else{

         $lastBreakBackDate =  new Carbon('');
       }*/

       /* Function for get all pour after recent break date  */
      /* $dataAfterLastBreak = $this->kegdata->pourAfterLastBreak($deviceID,$lastBreakBackDate);
       dd($dataAfterLastBreak);*/
       /* End */

       $tempHistory = $this->kegdata->getTempHistory($deviceID);
       $pourHistory = $this->kegdata->getPourHistory($deviceID);

       $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id');
       $this->count = 1;

       //loop started for each keg
       $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

        //start last break event code
        $getLastBreak = $this->kegdata->lastBrekEvent($keg->id);
        //now fetch pour after break event
        if($getLastBreak) {  
          $lastBreakBackDate =  new Carbon($getLastBreak->created_at); 
        }else{
         $lastBreakBackDate =  '';
       }
      
         /* Function for get all pour after recent break date  */
        $dataAfterLastBreak = $this->kegdata->pourAfterLastBreak($keg->id,$lastBreakBackDate);


       
       /* End */
       


         $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
         $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));
         //$keg->setRelation('dataAfterLastBreak', $dataAfterLastBreak ->where('deviceID', $keg->id));
         //$keg->setRelation('getLastBreak', $getLastBreak ->where('deviceID', $keg->id));


         $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
         $this->count++;
         $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);
         //$last_break=   new Carbon($getLastBreak->created_at);
         $keg->lastBreak = new Carbon($getLastBreak->created_at);

        //check if there is pour data come start lsm
         if($dataAfterLastBreak){

          $dataAfterLastBreak->each(function($pour) use($keg, $timezone, $accountSettings){
            $pour->name =  $keg->deviceName;

            $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y - g:i a');
          
            $pour->length = $pour->pour_length;
            $pour->adc_pour = $pour->adc_pour;
            $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

            $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);

                  //$lsm_m = DB::table('kegdevices')->select('lsm_m') 
                  //->where('id', '=',$pour->deviceID)->first();
            $lsm_m='';

            $rerurn_data=$this->kegdata->pseudo_steps_first($pour,$lsm_m);

            if($rerurn_data==1){
                        //push level for lsm calculation input in lsm
             $this->graph_arr[$pour->name][] =$pour->level;
             $this->graph_arr_x_axis[$pour->name][] =$pour->length;

           } 

           $pour->is_valid_pour=$rerurn_data;



         });
        }

        //find m and b from graph array 
      if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {

       $keg->valid_pour_count =count($this->graph_arr[$keg->deviceName]);
       $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);

       $positive_m = abs($lms_data['m']) ;

       $y = floor($positive_m*$lms_data['commulative_x']);

       $keg_level = $lms_data['b']- $y  ;// Find keg level according to lsm 
       
       $keg->value_y= $keg_level ;// Show keg leve acording to lsm
       $keg->lms_data=$lms_data;
      } else {
        $keg->lms_data=null;
      }

/* End*/
if($keg->showDevice == 1){

              /* if(!is_null($keg->tapped) && !is_null($keg->recentPour)){
                    $keg->tappedDiff = $keg->tapped->sent_at->diffInSeconds($keg->recentPour->sent_at, false);
                    if($keg->tappedDiff < 0){
                        $keg->recentPour->adc_pour_end = 2250;
                    }
               }else{
                   $keg->tappedDiff  = null;
                 }*/

                 if(!is_null($keg->tapped)){
                  $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->recentPour)){
                  $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                  if($keg->recentPour->ounces  < 0){
                    $keg->recentPour->ounces = 0;
                  }


                  $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                  $keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                    $keg->image = $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);

                  /* Condition for check if Pour less grater then 2 then show level ang Percetage according to this*/
                  if($keg->valid_pour_count >= 2 ){

                    $lsm_b =   $keg->lms_data['b'] ;

                     $keg->recentPour->percentage = floor($keg->value_y /($lsm_b/100));

                    $keg->image =$this->kegdata->kegPercentLsm(floor($keg->value_y /($lsm_b/100)),1984); // Calulate Percent

                    $keg->recentPour->total =  floor($keg->value_y).' '.$accountSettings->volumeType;
                   

                    }


                    if($keg->valid_pour_count >= 10 ){

                      $b = $keg->lms_data['b'];
                      $m = $keg->lms_data['m'];
                      $a=  DB::table('kegdevices')
                      ->where('kegMac', $keg->kegMac)
                      ->where('account_id', $keg->account_id)
                      ->where('id', $keg->id)
                      ->update(['current_m' =>  $m,'current_b' =>  $b]);
                    }

                  $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->temperature)){
                  $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                  $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                    $temp->name = $keg->deviceName;
                    $temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                  });
                }

                if(!is_null($keg->pourHistory)){
                  $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                    $pour->name =  $keg->deviceName;
                    $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $pour->length = $pour->pour_length;
                    $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);
                  });
                }
              }


            }); 

$view->withKegs($kegs)->with('kegdata', $this->kegdata)->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);
}

      /**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
      public function linear_regression($y,$x) {

        $y = array_values($y);
        $x = array_values($x);
        $commulative_x = array_sum($x);          
   
        $count=count($y);

   // do x axis comulnative
        $t=0;
    //assume $temp as x axis array
        $temp=array();
        for($i=0;$i<count($x);$i++) {
          $add=$t+$x[$i];
          $t=$add;
          $temp[$i]=$t;
        }
    //assign temp into x
        $x=$temp;

    // calculate number points
        $n = count($x);

  // ensure both arrays of points are the same size
        if ($n != count($y)) {

          trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

        }

  // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;

        for($i = 0; $i < $n; $i++) {

          $xy_sum+=($x[$i]*$y[$i]);
          $xx_sum+=($x[$i]*$x[$i]);

        }

  // calculate slope
        if((($n * $xx_sum) - ($x_sum * $x_sum))) {
          $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
        } else {
          $m=0;
        }


  // calculate intercept
        if($n) {
          $b = ($y_sum - ($m * $x_sum)) / $n;  
        } else {
          $b=0;
        }

  // return result
        return array("m"=>$m, "b"=>$b,"commulative_x"=>$commulative_x);

    }
    
  }
