<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentOrderRepository as Order;
use App\KegData\Models\Subscription;
use Utilities;

class OrderComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Order $order)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->order = $order;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $user = $this->auth->user();
    //    $plan = $this->auth->user()->account->plan;
        $account = $this->auth->user()->account;
       $subscription = Subscription::where('account_id','=',$user->account_id)
                            ->where('active','=','1');
       $orders = $this->order->getAllWithProducts();
       //~> dd($orders);
        $view->with('user', $user)->withOrders($orders)->withPlan($subscription)->withAccount($account);
    }


}
