<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Infrastructure\KegDataProcessor as Processor;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;

class ChartsComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, Processor $processor)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->processor = $processor;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user =
        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();
        $tempLimit = 0;
        $pourLimit = 0;

        $type = $view->type;
        $chartStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $chartEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $chartDates = $this->utils->reportDates();


        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $kegs  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);

        $deviceID = $kegs->pluck('id')->toArray();

        //with filters         $tempHistory = $this->kegdata->getTempHistory($deviceID, 0, 100, ['receivedDateTime_start' => new Carbon('today'), 'receivedDateTime_end' => new Carbon('tomorrow')]);
        if($type == 'temp'){
            $tempLimit = 500;
        }else{
            $pourLimit = 500;
        }

        $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id')->all();
        $this->count = 1;

        $kegs->each(function($keg) use ($timezone, $accountSettings){
            if($keg->showDevice == 1){

                $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
                $this->count++;
                $keg->calibration =  $this->kegdata->getCalibratedValue($keg->id);
                $keg->tapped_at = $this->kegdata->getTappedDate($keg->id);

                if($keg->tapped_at->toDateTimeString() < '2000-01-01 00:00:00' ){
                    $keg->tapped_at = 'Keg not tapped...';
                }


                if(!is_null($keg->recentPour)){

                    if(is_null($keg->recentPour->remaining_ounces)){
                        $keg->recentPour->converted_ounces = 'Calculating...';
                        $keg->recentPour->total = "Calculating...";
                        $keg->recentPour->percentage = "00";

                    }else{
                        $keg->recentPour->converted_ounces =  $this->processor->convertOunces($keg->recentPour->remaining_ounces, $accountSettings->volumeType, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                        $keg->recentPour->total = $keg->recentPour->converted_ounces.' '.$accountSettings->volumeType;
                        if(!is_null($keg->recentPour->remaining_ounces)){
                            $keg->recentPour->percentage =  $this->processor->percentage($keg->recentPour->remaining_ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                        }else{
                            $keg->recentPour->percentage = "00";
                        }

                    }

                    $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }


               if(!is_null($keg->temperature)){
                    $keg->temperature->tempFormatted = $this->processor->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                }

            }/* Close ShowDevice */
        });


        $view->withKegs($kegs)->with('kegdata', $this->kegdata)
        ->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)
        ->with('user', $user)->with('settings', $accountSettings)
        ->with('chartDates', $chartDates)->with('chartStart', $chartStart)
        ->with('chartEnd', $chartEnd)->with('quickDates', $quickDates)
        ->withType($type);
    }

}
