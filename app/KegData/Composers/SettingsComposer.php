<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentTimezoneRepository as Timezone;
use App\KegData\Infrastructure\Utilities as Utilities;
use Illuminate\Contracts\Auth\Guard;

class SettingsComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Timezone $timezone, Utilities $utils, Guard $auth)
    {

     	$this->timezone = $timezone;
                $this->utils = $utils;
                $this->auth = $auth;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = $this->auth->user();
        $settings = $settings = $this->auth->user()->account->accountSetting->first();
        $regionsForSelect = $this->timezone->getRegions()->lists('region', 'region');
        array_forget($regionsForSelect, 'America');
        $regionsForSelect = array_reverse($regionsForSelect);
        $regionsForSelect['America'] = 'America';
        $regionsForSelect = array_reverse($regionsForSelect);

        $zonesForSelect = array('' => 'Choose Timezone') + $this->timezone->getByRegion('America')->lists('offsetTimezone', 'id');
        $currencies = $this->utils->get_currencies();


        $view->with('regionsForSelect', $regionsForSelect)->with('zonesForSelect', $zonesForSelect)->withCurrencies($currencies)->with('timeArray', $this->timeArray())->with('user', $user)->with('settings', $settings);
    }

    public function timeArray(){
        return [
            '12:00:00' => '12:00',
            '12:30:00' => '12:30',
            '1:00:00' => '1:00',
            '1:30:00' => '1:30',
            '2:00:00' => '2:00',
            '2:30:00' => '2:30',
            '3:00:00' => '3:00',
            '3:30:00' => '3:30',
            '4:00:00' => '4:00',
            '4:30:00' => '4:30',
            '5:00:00' => '5:00',
            '5:30:00' => '5:30',
            '6:00:00' => '6:00',
            '6:30:00' => '6:30',
            '7:00:00' => '7:00',
            '7:30:00' => '7:30',
            '8:00:00' => '8:00',
            '8:30:00' => '8:30',
            '9:00:00' => '9:00',
            '9:30:00' => '9:30',
            '10:00:00' => '10:00',
            '10:30:00' => '10:30',
            '11:00:00' => '11:00',
            '11:30:00' => '11:30',
        ];
    }
}       



