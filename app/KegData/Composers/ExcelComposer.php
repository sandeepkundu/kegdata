<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;

class ExcelComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, Carbon $carbon)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->carbon = $carbon;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $reportDates = $this->utils->reportDates();
        $reportData  = $this->kegdata->getForReports($accountID, 'account_id', $timezone->timezone, $view->input);

        

        $kegs = $this->kegs->getForExcel($accountID, 'account_id');
        
        $now = $this->carbon->now();
        $serverDay = Carbon::createFromDate($now->year, $now->month, $now->day, 'UTC');
        $localDay = Carbon::createFromDate($now->year, $now->month, $now->day, $timezone->timezone);
        $reportDT = Carbon::now($timezone->timezone);
        $dayStart = Carbon::createFromTime(substr($accountSettings->dayStart, 0, 2), substr($accountSettings->dayStart, 3,2), '00', $timezone->timezone);
        $dayStart = $dayStart->setTimezone('UTC');
        $weekStart = new Carbon('last '.strtolower($accountSettings->weekStart));
        $yesterday = Carbon::createFromTime(substr($accountSettings->dayStart, 0, 2), substr($accountSettings->dayStart, 3,2), '00', $timezone->timezone);
        $month = Carbon::now();
        $year = Carbon::now();
        

        $timeArray = [
            'today' => $dayStart,
            'yesterday' => $yesterday->subDays(1)->hour($dayStart->hour)->minute($dayStart->minute),
            'week' => $weekStart->hour($dayStart->hour)->minute($dayStart->minute),
            'month' => $month->startOfMonth()->setTimezone($timezone->timezone),
            'year' => $year->startOfYear()->setTimezone($timezone->timezone),
            'serverDay' => $serverDay,
            'localDay' => $localDay,

        ];

        $kegs->each(function($keg) use($timeArray){
            $data = $keg->kegdata;
            $calibration = $this->kegdata->getCalibratedValue($keg->kegMac);
            $keg->calibration = $calibration;

            //Calculate Todays Information
            $today = $data->filter(function ($item) use($timeArray){
                if($item->receivedDateTime >= $timeArray['today']->toDateTimeString()){
                    return $item;
                }
            });

            $ounces = $today->map(function($item) use($calibration){
                if($item->status == '10000011'){
                   $item->ounces = $this->kegdata->getOunces($item->endVal, $calibration, $item->amount, $item->empty, $item->volumeType);
                    $item->percentage = $this->kegdata->kegPercent($item->ounces, $item->amount);
                }
               else{
                    $item->ounces = NULL;
                    $item->percentage = NULL;
                }
               return $item;
            });
            $keg->setRelation('today', $ounces);

            //Calculate Yesterdays Information
            $yesterday = $data->filter(function ($item) use($timeArray){
                if($item->receivedDateTime >= $timeArray['yesterday']->toDateTimeString() && $item->receivedDateTime >= $timeArray['yesterday']->toDateTimeString()){
                    return $item;
                }
            });

          $ounces = $yesterday->map(function($item) use($calibration){
               if($item->status == '10000011'){
                   $item->ounces = $this->kegdata->getOunces($item->endVal, $calibration, $item->amount, $item->empty, $item->volumeType);
                    $item->percentage = $this->kegdata->kegPercent($item->ounces, $item->amount);
                }
               else{
                    $item->ounces = NULL;
                    $item->percentage = NULL;
                }
               return $item;
            });
          $keg->setRelation('yesterday', $ounces);

          //Calculate Weeks Information
              $week = $data->filter(function ($item) use($timeArray){
                if($item->receivedDateTime >= $timeArray['week']->toDateTimeString()){
                    return $item;
                }
            });

            $ounces = $week->map(function($item) use($calibration){
                if($item->status == '10000011'){
                   $item->ounces = $this->kegdata->getOunces($item->endVal, $calibration, $item->amount, $item->empty, $item->volumeType);
                    $item->percentage = $this->kegdata->kegPercent($item->ounces, $item->amount);
                }
               else{
                    $item->ounces = NULL;
                    $item->percentage = NULL;
                }
               return $item;
            });
            $keg->setRelation('week', $ounces);
                        dd($ounces->toJson());
            
            //Calculate Months Information
              $month = $data->filter(function ($item) use($timeArray){
                if($item->receivedDateTime >= $timeArray['month']->toDateTimeString()){
                    return $item;
                }
            });

            $ounces = $month->map(function($item) use($calibration){
                if($item->status == '10000011'){
                   $item->ounces = $this->kegdata->getOunces($item->endVal, $calibration, $item->amount, $item->empty, $item->volumeType);
                    $item->percentage = $this->kegdata->kegPercent($item->ounces, $item->amount);
                }
               else{
                    $item->ounces = NULL;
                    $item->percentage = NULL;
                }
               return $item;
            });
            $keg->setRelation('month', $ounces);

            //Calculate Year Information
              $year = $data->filter(function ($item) use($timeArray){
                if($item->receivedDateTime >= $timeArray['year']->toDateTimeString()){
                    return $item;
                }
            });

            $ounces = $year->map(function($item) use($calibration){
                if($item->status == '10000011'){
                   $item->ounces = $this->kegdata->getOunces($item->endVal, $calibration, $item->amount, $item->empty, $item->volumeType);
                   $item->percentage = $this->kegdata->kegPercent($item->ounces, $item->amount);
                }
               else{
                       $item->ounces = NULL;
                       $item->percentage = NULL;
                }
               return $item;
            });


            $keg->setRelation('year', $ounces);


       //END $kegs->each
        });



        $view->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)
        ->with('account', $account)->with('accountAddress', $accountAddress)
        ->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)
        ->with('quickDates', $quickDates)->with('reportDates', $reportDates)
        ->with('carbon', $this->carbon)->with("reportDT", $reportDT);
    }

}