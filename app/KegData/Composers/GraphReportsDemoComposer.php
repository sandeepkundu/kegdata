<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
use Session;
use DB;
use App\KegData\Models\LsmValue as LsmValue;

class GraphReportsDemoComposer {

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData,LsmValue $lsmvalue, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, BrewDist $brewDist)
    {
      $this->auth = $auth;
      $this->kegdata = $kegData;
      $this->collection = $collection;
      $this->kegs = $kegs;
      $this->timezone = $timezone;
      $this->count = 0;        
      $this->accountSetting = $accountSetting;
      $this->utils = $utils;
      $this->brewDist = $brewDist;
      $this->graph_arr = [];
      $this->graph_arr_x_axis = [];
      $this->lsmvalue= $lsmvalue;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

      $newGraph = 0;
      $action = Input::get();
      if(isset($action['submit'])){
       $newGraph = 1;
     }

    //for setting chart pagination
     $page_limit = isset($view->input['page_limit']) ? $view->input['page_limit']:50;
     $page_limit_temp = isset($view->input['page_limit_temp']) ? $view->input['page_limit_temp']:50;
     $offset_setting = isset($view->input['offset_setting']) ? $view->input['offset_setting']:0;
     $offset_setting_pour = isset($view->input['offset_setting_pour']) ? $view->input['offset_setting_pour']:0;

     $user = $this->auth->user();
     $account = $this->auth->user()->account;
     $accountID = $account->id;
     $accountType = $this->auth->user()->account->accountType;
     $accountSettings = $this->auth->user()->account->accountSetting->first();
        //dd($accountSettings);
     $accountAddress = $this->auth->user()->account->accountAddress->first();

     if($accountType == 'B' || $accountType == 'D'){
      $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
      $query = $brewDist->id;
      $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
    }else{
      $query = $accountID;
      $column = 'account_id';
    }


    $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

    $default_date_start=date('Y-m-d', strtotime('-1 days'))." ".$accountSettings->dayStart;
    $default_date_end=date('Y-m-d')." ".$accountSettings->dayEnd;

        //dd($view->input);
    $ident_hdn = (empty($view->input['ident_hdn'])) ? 'pour' : $view->input['ident_hdn'];

    if(isset($action['chart'])) {
      $ident_hdn=$action['chart'];
    }

    $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
    $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
    $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
    $reportStatus = (empty($view->input['status'])) ? null : $view->input['status'];
    $reportDates = $this->utils->reportDates();



    $user = $this->auth->user();
    $account = $this->auth->user()->account;
    $accountID = $account->id;
    $kegs_for_filters  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
    $keg_list=$kegs_for_filters->lists('KegFullName', 'id');


    $device_id_for_default=null;
    if(empty($view->input['deviceID']) && (count($keg_list)) ) {
            //$deviceID=$kegsForSelect[0];
      $device_id_for_default=array_keys($keg_list)[0];

    }

    $filter_device_id = (empty($view->input['deviceID'])) ? null : $view->input['deviceID'];

    $view->with('newGraph',$newGraph)->with('ident_hdn',$ident_hdn)->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)->with('quickDates', $quickDates)->with('reportDates', $reportDates)->with('reportStatus', $reportStatus)->with('status', $reportStatus)->with('deviceID', $filter_device_id)->with('offset_setting',$offset_setting)->with('page_limit',$page_limit)->with('page_limit_temp',$page_limit_temp)->with('offset_setting_pour',$offset_setting_pour);


    $accountType = $this->auth->user()->account->accountType;
    $accountSettings = $this->auth->user()->account->accountSetting->first();
    $accountAddress = $this->auth->user()->account->accountAddress->first();

    $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

    $kegs_for_filter  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
    $kegs  = $this->kegs->getForLevelsCharts($accountID, 'account_id', $accountID,$filter_device_id);


    $deviceID = $kegs->fetch('id')->toArray();

    $reportStart = new Carbon($reportStart, $timezone->timezone);
    $reportEnd = new Carbon($reportEnd, $timezone->timezone);
      //dd($reportStart);

    $filter['receivedDateTime_start'] = empty($reportStart)?$default_date_start:$reportStart;
    $filter['receivedDateTime_end'] = empty($reportEnd)? $default_date_end:$reportEnd;
    //dd($filter);
    $tempHistory = $this->kegdata->getTempHistory($deviceID,$offset_setting, $page_limit_temp ,$filter);
      
    $pourHistory = $this->kegdata->getPourHistoryDemo($deviceID,$offset_setting_pour, $page_limit,$filter);

        //get date interval to send it to level js
    $date_interval_array = $this->kegdata->getDateIntervals();

    $kegsForSelect = array('' => 'Select Keg') + $kegs_for_filter->lists('KegFullName', 'id');
    $this->count = 1;
    $kegs->each(function($keg) use ($timezone, $accountSettings, $tempHistory, $pourHistory){

     $keg->setRelation('tempHistory', $tempHistory->where('deviceID', $keg->id));
     $keg->setRelation('pourHistory', $pourHistory->where('deviceID', $keg->id));


     $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
     $this->count++;
     $keg->calibration =  $this->kegdata->getCalibratedValue($keg->kegMac);

     //calling utility helper functions to get full volume
    //function params keg_type_id and measurement value oz or ml
    $keg_type_result=$this->utils->get_keg_type_as_per_measurement($keg->kegType,$accountSettings->volumeType);
    $keg_type_amount_value=1984;
    $keg_type_empty_value=2252;
    if(!empty($keg_type_result)) {
      $keg_type_amount_value=$keg_type_result->amount;
      $keg_type_empty_value=$keg_type_result->emptyValue;
    }
    $keg->keg_type_amount_value=$keg_type_amount_value;
    $keg->keg_type_empty_value=$keg_type_empty_value;
    // end utility function 

     if($keg->showDevice == 1){

                if(!is_null($keg->tapped)){
                  $keg->tapped->sent_at = $keg->tapped->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->recentPour)){
                  $keg->recentPour->ounces =  $this->kegdata->getOunces($keg->recentPour->adc_pour_end, $keg->calibration, isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984, isset($keg->recentPour->empty) ? $keg->recentPour->empty : 2252, $accountSettings->volumeType);
                  if($keg->recentPour->ounces  < 0){
                    $keg->recentPour->ounces = 0;
                  }
                  $keg->recentPour->total = $keg->recentPour->ounces.' '.$accountSettings->volumeType;
                  $keg->recentPour->percentage =  $this->kegdata->kegPercent( $keg->recentPour->ounces,isset($keg->recentPour->amount) ? $keg->recentPour->amount : 1984);
                  $keg->recentPour->sent_at = $keg->recentPour->sent_at->timezone($timezone->timezone);
                }

                if(!is_null($keg->temperature)){
                  $keg->temperature->tempFormatted = $this->kegdata->getTemp($keg->temperature->temperature, $accountSettings->temperatureType, $keg->tempOffset);

                  $keg->tempHistory->each(function($temp) use ($keg, $timezone, $accountSettings){
                    $temp->name = $keg->deviceName;
                    //$temp->date = $temp->sent_at->timezone($timezone->timezone)->format('M d, Y g:i a');
                    $temp->date = $temp->sent_at->format('M d, Y g:i a');
                    $temp->tempFormatted  = $this->kegdata->getTemp($temp->temperature, $accountSettings->temperatureType, $keg->tempOffset);
                  });
                }

                if(!is_null($keg->pourHistory)){

               // Session::set('last_pour_status', null);

                  //fetch latest m and b values
                  $getrecentBreakDate = $this->kegdata->lastBreakFromLsmVaue($keg->id);

                  $keg->lms_data=$getrecentBreakDate;


                  $keg->pourHistory->each(function($pour) use($keg, $timezone, $accountSettings){
                    $pour->name =  $keg->deviceName;
                    $pour->date = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y - g:i a');
                    //change in timezone
                    //$pour->date_new = $pour->sent_at->timezone($timezone->timezone)->format('M d, Y  g:i a');
                    //change in utc
                    $pour_date_new = new Carbon($pour->sent_at);
                    $pour->date_new = $pour_date_new->toDateTimeString();

                    ######step to fetch data from lsm_values#########
                    if(isset($pour->date_new ) && !empty($pour->date_new )){

                      //$main_pour_date  = new Carbon($pour->date_new);
                     $main_pour_date  = $pour->date_new;
                      $search_date_string= preg_replace("!\s+!", " ", $pour->date_new);
                      
                      $lsm_values_table_data = DB::select("SELECT m , b
                      FROM lsm_values
                      WHERE pour_date LIKE '%$search_date_string%' 
                      LIMIT 1");
                     
                      if(isset($lsm_values_table_data) && !empty($lsm_values_table_data)){
                        $lsm_values_table_data['0']->m;
                        
                       $m_new_val= $lsm_values_table_data['0']->m;
                       $b_new_val= $lsm_values_table_data['0']->b;
                       $pour->new_lsm_m_from_new = $m_new_val;
                       $pour->new_lsm_b_from_new = $b_new_val;

                      }else{
                        $pour->new_lsm_m_from_new = null;
                        $pour->new_lsm_b_from_new = null;
                      }


                  }
                    //1. query in the lsm_values table to fetch m and b from pour date
                    //2. then find out pour length and their m,b,x like $pour->lsm_values_m,$pour->lsm_values_b,$pour->lsm_values_x

                    ################end###############################
                    $pour->length = $pour->pour_length;
                    $pour->adc_pour = $pour->adc_pour;
                    $pour->empty=empty($pour->empty) ? 0.15 : $pour->empty;
                    $pour->level = $this->kegdata->getOunces($pour->adc_pour_end, $keg->calibration, $pour->amount, $pour->empty, $accountSettings->volumeType);

                    //calling utility helper functions to get full volume
                    //$keg_type_amount_value = $this->utils->get_volume_value($accountSettings->volumeType);
                    $pour->full_volume = $keg->keg_type_amount_value;

                    //code start for fetch flow rate according to formula
                    //formula is: (full keg volume in desired units)/(ADCempty-ADCfull)*m_adc
                    
                    $m_adc = empty($pour->new_lsm_m_from_new) ? 0.15 : $pour->new_lsm_m_from_new ;
                    
                    $pour->flow_rate = (($pour->full_volume)/($pour->empty-350))*($m_adc);

                    //echo "===>".($pour->length) * ($pour->flow_rate);
                    //End code start for fetch flow rate of each pour

                    $pour->percentage = $this->kegdata->kegPercent( $pour->level,isset($pour->amount) ? $pour->amount: 1984);
                    //$this->graph_arr[$pour->name][] =$pour->level;

                    //start pesudo step from here it return true false

                   // Select the value of lsm_m from kegdevice table
                   // $lsm_m = DB::table('kegdevices')->select('lsm_m') 
                    //->where('id', '=',$pour->deviceID)->first();

                    if(empty($keg->lms_data)) {
                      $lsm_m = (object)array("m"=>"0.15");
                      $lsm_m->lsm_m = $lsm_m->m;
                    } else {
                       $lsm_m=$keg->lms_data;
                       $lsm_m->lsm_m = $keg->lms_data->m;
                    }
                    
                    
                    $rerurn_data=$this->kegdata->pseudo_steps_first($pour,$lsm_m);


                  /*  if($rerurn_data==1){

                        //push level for lsm calculation input in lsm
                     $this->graph_arr[$pour->name][] =$pour->level;
                     $this->graph_arr_x_axis[$pour->name][] =$pour->length;
                     
                   }*/
                   
                  $pour->is_valid_pour=$rerurn_data;



                });
            }


            //dd($this->graph_arr[$keg->deviceName]);

            //find m and b from graph array 
       /* if(isset($this->graph_arr[$keg->deviceName]) && isset($this->graph_arr_x_axis[$keg->deviceName])) {
                         //dd($this->graph_arr[$keg->deviceName]);
         $lms_data = $this->linear_regression($this->graph_arr[$keg->deviceName],$this->graph_arr_x_axis[$keg->deviceName]);
         $keg->lms_data=$lms_data;
        } else {
          $keg->lms_data=null;
        }*/


}


});


$view->withKegs($kegs)->with('kegdata', $this->kegdata )->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);

        ////end code for tabs



}

    /**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
    public function linear_regression($y,$x) {

      $y = array_values($y);
      $x = array_values($x);

      $count=count($y);



    /*if($count>9) {
        $median=floor($count/2);    
        $temp=array();
        $temp[]=$y[0];
        $temp[]=$y[1];
        $temp[]=$y[2];
        
        if($median) {
         $temp[]=$y[$median];
         $temp[]=$y[$median+1];
         $temp[]=$y[$median+2];
        }
        
        $temp[]=$y[$count-3];
        $temp[]=$y[$count-2];
        $temp[]=$y[$count-1];
        
        unset($y);
        $y=$temp;
        
      }*/
      
  //find x axis array
  //$x= range(1,count($y));



   // do x axis comulnative
      $t=0;
    //assume $temp as x axis array
      $temp=array();
      for($i=0;$i<count($x);$i++) {
        $add=$t+$x[$i];
        $t=$add;
        $temp[$i]=$t;
      }
    //assign temp into x
      $x=$temp;




    // calculate number points
      $n = count($x);

  // ensure both arrays of points are the same size
      if ($n != count($y)) {

        trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);

      }

  // calculate sums
      $x_sum = array_sum($x);
      $y_sum = array_sum($y);

      $xx_sum = 0;
      $xy_sum = 0;

      for($i = 0; $i < $n; $i++) {

        $xy_sum+=($x[$i]*$y[$i]);
        $xx_sum+=($x[$i]*$x[$i]);

      }

  // calculate slope
      if((($n * $xx_sum) - ($x_sum * $x_sum))) {
        $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));  
      } else {
        $m=0;
      }


  // calculate intercept
      if($n) {
        $b = ($y_sum - ($m * $x_sum)) / $n;  
      } else {
        $b=0;
      }


  // return result
      return array("m"=>$m, "b"=>$b);

    }





  }