<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use Utilities;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\KegData\Repositories\EloquentKegDeviceRepository as KegDevice;
use Cart as Cart;
use App\KegData\Models\OrderItem as OrderItem;
use App\KegData\Models\Product as Product;
use App\KegData\Models\Plan as Plan;
use App\KegData\Models\Category as CategoryModel;
//use App\KegData\Repositories\EloquentPlanRepository as Plan;
use DB ;

class CartComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Plan $plan,OrderItem $OrderItem, Utilities $utils, Category $categories, KegDevice $kegdevice)
    {
            $this->auth = $auth;
            $this->utils = $utils;
            $this->categories = $categories;
            $this->kegdevice = $kegdevice;
            $this->plan =$plan;
            $this->orderItem =$OrderItem;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $kegcount = 0;
        if($this->auth->guest()){
            $user = $this->auth->guest();
            //$kegcount = 0;
        }else{
            $user = $this->auth->user();
            //$kegcount = $this->kegdevice->where('account_id', '=',$user->account_id)->where('showDevice', '=', 1)->count();
        }


      
      $categories = $this->categories
                        ->where('is_navigation','=',1)
                        ->get();

       $cartItems = Cart::instance('shopping')->content();

        $sys_count = 0;
       //to check if system taken already
       if($this->auth->user()) {
          $order = $this->auth->user()->account->Order;
           $categorySlug = 'systems';
           $current_cat = $this->categories->where('slug', $categorySlug)->first();
           
           if( ($current_cat) && ($current_cat->name == "Systems")) {
              $system_id = $current_cat->products['0']['id'];
              foreach ( $order as $orderId) {
                  
                   if($orderId->status == 'charged') {
                      $model = OrderItem::where('order_id', '=', $orderId->id)
                  ->where('product_id', '=', $system_id)->get();
                    if($model){
                         $sys_count++;
                     }

                   }

             }

           } 

           
       }
      
        //end code to check if system taken already

    

       $couplerCount = 0;
       $splitterCount = 0;
       $mainpowerwires = 0;
       $couplerpowerwire = 0;
       $cpwProducts = NULL;
       $splitterProducts = NULL;
       $mpwProducts = NULL;
       $relatedProducts = false;


       //find kegcount
       foreach($cartItems as $items){
        $item = Product::find($items->id); 
        $category=$item->category; 
        if($category->where('slug','couplers')->count()){
        
            $kegcount += $items->qty;
        }
      }

     
       //check if there is subscribe a plan by a user
      //then automatically assign him 1 couplar
       if(Cart::instance('subscription')->count() > 0){ 
            if($kegcount == 0) {

               $cat=CategoryModel::where('slug','couplers')->first();
                 if($cat->products->first()) {
                    $item = $cat->products->first();
                    $quantity = 1;
                    $kegcount = $kegcount+$quantity;
                    
                    Cart::instance('shopping')->associate('App\KegData\Models\Product')->add($item->id, $item->name, $quantity, $item->price);
                    $cartItems = Cart::instance('shopping')->content();

                 }

            }

       }

        //dd($kegcount);


       //find system count in cart
       foreach($cartItems as $items){
        $item = Product::find($items->id); 
        $category=$item->category; 
        if($category->where('slug','systems')->count()){
            $couplerCount += $items->qty;
        }
      }



     
      /* Code for total coupler purchage in last order*/

      $userOrder_coupler_total= 0;
      if($this->auth->user()){
        $categorySlug = 'couplers';
        $current_cat = $this->categories->where('slug', $categorySlug)->get();

        $prodCat = DB::table('category_product')
        ->where('category_id', '=', $current_cat['0']['id'])
        ->get();
        foreach ($prodCat as $prodCat_id) {
          $current_user_order =  $this->auth->user()->account->Order()
            ->orderBy('id','desc')
            ->limit('1')->get(); 
        
          foreach ($current_user_order as $currentUseOredr) {

            if($currentUseOredr->status == 'charged') {
              $userOrder_item= $this->orderItem
                       // ->select('quantity')
              ->where('order_id','=', $currentUseOredr->id)
              ->where('product_id','=',$prodCat_id->product_id)
              ->orderBy('id','desc')
              ->limit('1')
              ->get();
             
              foreach ($userOrder_item as $qty) {
                if ($qty) {

                 $userOrder_coupler_total+= $qty->quantity;
               }
             }

            }

         }   

       }
     }
          

          /* Here not change */
      /* Code for  get coupler which already purchaged by the user
      */ 
      if(!empty($this->auth->user())){
       $user_subscribe_plan = $this->auth->user()->account->Plan;

       if($user_subscribe_plan) {
         if($user_subscribe_plan->max == 1 && $kegcount >0 ){
          $kegcount =  $kegcount  + $userOrder_coupler_total;
        }else if($user_subscribe_plan->max == 2  && $kegcount >0 ){
          $kegcount =  $kegcount  + $userOrder_coupler_total;
        }else if($user_subscribe_plan->max == 3  && $kegcount >0 ){
         $kegcount =  $kegcount  + $userOrder_coupler_total;
       }
        }// End $user_subscribe_plan if
      }else {
       $user_subscribe_plan = "";
      }
      //dd($kegcount);
      /* Code for change subscryption plan acording to number of coupler 
      *  
      */

   
      if(Cart::instance('subscription')->count() > 0){

      //assign plan on the basis of kegcount 
      if($kegcount){

        if( $kegcount > 2){
          $operator = ">=";
          $count =3;
        }else{
          $operator = "=";
          $count = $kegcount;
        }
        $my_plan =  $this->plan->where('max',$operator, $count)->first();
        Cart::instance('subscription')->destroy();
        Cart::instance('subscription')->add($my_plan->id, $my_plan->name,1, $my_plan->price, array('slug'=>$my_plan->slug,'term' => $my_plan->interval, 'is_quantity' => $my_plan->is_quantity, 'quantity_price' => $my_plan->quantity_price));
      } 
     
    }


       /**
        * Users need 1 coupler power wire/coupler
        * Users need a splitter large enough for the # of couplers
        * Users need 1 main power wire for every splitter
        */

       // foreach($cartItems as $item){
       //      $item = $this->product->find($item->id); //change by sir
       //      $category=$item->category;                //change by sir
       //      dd();
       //     $category = $item->model->category()->get();

       //     /**
       //      * Total Keg Count between active kegs & cart couplers
       //      */
       //     if($category->where('slug','couplers')->count()|| $category->where('slug','systems')->count()){
       //          $kegcount += $item->qty;
       //     }

       //     *
       //      * Coupler count just in cart not counting the system
            
       //     if($category->where('slug','couplers')->count() ){
       //         $couplerCount += $item->qty;
       //     }

       //     /* Splitter count */
       //     if($category->where('slug','splitters')->count()){
       //         $splitterCount += $item->qty;
       //     }

       //     if($category->where('slug','mainpowerwires')->count()){
       //         $mainpowerwires += $item->qty;
       //     }

       //     if($category->where('slug','couplerpowerwire')->count()){
       //         $couplerpowerwire += $item->qty;
       //     }
       // }

      /* if($couplerCount > $couplerpowerwire){
           $cpwProducts = $this->categories
                            ->with('products')
                            ->where('slug', '=', 'couplerpowerwire')
                            ->first();
            $relatedProducts = true;
       }

       if($kegcount > 3 && $splitterCount == 0){
           $splitterProducts = $this->categories
                                ->with('products')
                                ->where('slug','=','splitters')
                                ->first();
            $relatedProducts = true;
       }

       if($splitterCount > 0 || ($kegcount > 3 && $splitterCount == 0)){
           $mpwProducts = $this->categories
                            ->with('products')
                            ->where('slug','=','mainpowerwires')
                            ->first();
            $relatedProducts = true;
       }*/



       $subscription = Cart::instance('subscription')->content();

       //dd($subscription);

      //check if the plan is commercial then minus keg count by three
      $subs_total=0;
      foreach ($subscription as $sub) {
        $plan=Plan::find($sub->id);
        if( ($plan) && ($plan->plan_type == 'Commercial') ) {

          if($kegcount > 3) {
            $kegcount = $kegcount;
          }
        }

        $subs_total += $sub->price;

      }



       /*$subs_total=0;
        foreach ($subscription as $sub) {
          $subs_total += $sub->price;
        }*/
       

        if(!empty($this->auth->user())){
         $account = $this->auth->user()->account;

         $current_subscription = $account->subscription;

         if(isset($current_subscription) && $current_subscription != null ){
          $subsc =$current_subscription->all()->last();
          $current_plan_id = $subsc->plan_id;
          $current_plan= $account->Plan->find($current_plan_id);

        }
        if(isset($current_plan) && $current_plan != null){
          $current_plan_name =$current_plan->name;
        }else{
         $current_plan_name ="No_plan" ;
       }

     }else{
        $current_plan_name ="" ;
    }

       
        $cartCount = Cart::instance('shopping')->count() + Cart::instance('subscription')->count();
        $subscription_count  = Cart::instance('subscription')->count();

        $cartTotal = Cart::instance('shopping')->total();
        $newTotalCount =  Cart::instance('shopping')->total() + $subs_total;
        //echo "====>".$newTotalCount;
        //dd($newTotalCount);
        //dd($kegcount);
        $view->with('user', $user)
            ->with('userOrder_coupler_total',$userOrder_coupler_total)
            ->with('user_subscribe_plan',$user_subscribe_plan)
            ->with('subscription_count',$subscription_count)
            ->with('current_plan',$current_plan_name)
            ->with('newTotalCount',$newTotalCount)
            ->with('cartCount', $cartCount)
            ->with('cartTotal', $cartTotal)
            ->with('cartItems',$cartItems)
            ->withCategories($categories)
            ->withSubscription($subscription)
            ->with('kegcount', $kegcount)
            ->with('cpwProducts', $cpwProducts)
            ->with('mpwProducts', $mpwProducts)
            ->with('splitterProducts', $splitterProducts)
            ->with('relatedProducts', $relatedProducts)
            ->with('couplerCount', $couplerCount)
            ->with('sys_count', $sys_count);
    }


}
