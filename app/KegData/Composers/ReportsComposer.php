<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;
use Input;
class ReportsComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, BrewDist $brewDist)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->brewDist = $brewDist;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        
      $newGraph = 0;
      $action = Input::get();
      if(isset($action['submit'])){
       if($action['submit']== 'Run Graph'){

        $newGraph = 1;
        }
       }


        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        if($accountType == 'B' || $accountType == 'D'){
            $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
            $query = $brewDist->id;
            $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
        }else{
            $query = $accountID;
            $column = 'account_id';
        }
        

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $default_date_start=date('Y-m-d')." ".$accountSettings->dayStart;
        $default_date_end=date('Y-m-d')." ".$accountSettings->dayEnd;

        
        

        $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $reportStatus = (empty($view->input['status'])) ? null : $view->input['status'];
        $reportDates = $this->utils->reportDates();

        $filter_device_id = (empty($view->input['deviceID'])) ? null : $view->input['deviceID'];
        
       // dd($view->input);
        // Return data for first tab
        $reportData  = $this->kegdata->getForReports($query, $column, $timezone->timezone, $view->input,$default_date_start,$default_date_end);

/*        $reportData->each(function($data) use ($timezone, $accountSettings){

        });*/


        $view->with('newGraph',$newGraph)->with('reportData', $reportData)->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)->with('quickDates', $quickDates)->with('reportDates', $reportDates)->with('reportStatus', $reportStatus)->with('status', $reportStatus)->with('deviceID', $filter_device_id);



        //start code for tabs
        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        // code
       // dd($filter_device_id);
        $kegs_for_filter  = $this->kegs->getForLevels($accountID, 'account_id', $accountID);
         $kegs  = $this->kegs->getForLevelsCharts($accountID, 'account_id', $accountID,$filter_device_id);


        $deviceID = $kegs->fetch('id')->toArray();

        $filter['receivedDateTime_start'] = empty($reportStart)?$default_date_start:$reportStart;
        $filter['receivedDateTime_end'] = empty($reportEnd)? $default_date_end:$reportEnd;
        
       

        $kegsForSelect = array('' => 'Select Keg') + $kegs_for_filter->lists('KegFullName', 'id');
  $this->count = 1;
        

        $view->withKegs($kegs)->with('kegdata', $this->kegdata )->with('kegsForSelect',$kegsForSelect)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings);

        ////end code for tabs



    }

}