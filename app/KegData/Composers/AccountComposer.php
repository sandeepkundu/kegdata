<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use Utilities;

class AccountComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Utilities $utils)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->utils = $utils;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $user = $this->auth->user();
       $account = $user->account;
       $accountAddress = $user->account->accountAddress->first();

       $accountID = $this->auth->user()->account->id;
       $accountUsers = $this->account->with('user')->find($accountID);



        $view->with('accountUsers', $accountUsers)->with('user', $user)->with('account', $account)->with('accountAddress', $accountAddress)
        ->with('countries', $this->utils->get_countries())->with('states', $this->utils->get_states());
    }


}       



