<?php namespace App\KegData\Composers;

 /**
     * Created this composer to user for excel reports
     *
     */

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentUserRepository as Users;

class EmployeeUsersExcelComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Users $users)
    {

        $this->users =$users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $users = $this->users->all();

        $view->withUsers($users)->with('sheetTitle','Title1', );
    }

}
