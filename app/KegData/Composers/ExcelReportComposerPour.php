<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;

class ExcelReportComposerPour {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, Carbon $carbon)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->carbon = $carbon;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 
        //dd('hello');
      
        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        $accountAddress = $this->auth->user()->account->accountAddress->first();

        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

        $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $reportDates = $this->utils->reportDates();

        $default_date_start=date('Y-m-d')." ".$accountSettings->dayStart;
        $default_date_end=date('Y-m-d')." ".$accountSettings->dayEnd;
		
        $reportData  = $this->kegdata->getForReportExcelPour($accountID, 'account_id', $timezone->timezone, $view->input,$default_date_start,$default_date_end);
			
        
        //$pourHistory = $this->kegdata->getPourHistoryDemo($deviceID,$offset_setting_pour, $page_limit,$filter);


        $view->with('kegdata', $this->kegdata)->with('reportData', $reportData)->with('timezone', $timezone)->with('user', $user)
        ->with('account', $account)->with('accountAddress', $accountAddress)
        ->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)
        ->with('quickDates', $quickDates)->with('reportDates', $reportDates)
        ->with('carbon', $this->carbon);
    }

}
