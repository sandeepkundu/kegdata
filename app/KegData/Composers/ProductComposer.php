<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use Utilities;
use Cart as Cart;
use App\KegData\Models\OrderItem as OrderItem;
class ProductComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Utilities $utils, Category $categories)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->utils = $utils;
            $this->category = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
     $user = $this->auth->user();
     $viewData = $view->getData();

     $categorySlug = $viewData['category'];

     $categories = $this->category
     ->with('plans')
     ->with('plans.photos')
     ->with('products')
     ->with('products.photos')
     ->where('is_navigation','=', 1)
     ->get();


     $current_cat = $categories->where('slug', $categorySlug)->first();
     $product_system = Cart::instance('shopping')->count();
     $subscription = Cart::instance('subscription')->content();
     $subscription_qty = 0;
     $max= "";
     $sys_count = 0;

     //to check if system taken already
     if($this->auth->user()) {
        $order = $this->auth->user()->account->Order;
         if($current_cat->name == "Systems"){
            foreach ( $order as $orderId) {

                if($orderId->status == 'charged') {
                     $model = OrderItem::where('order_id', '=', $orderId->id)
                     ->where('product_id', '=', $current_cat->products['0']['id'])->get();
                     
                     if($model){
                         $sys_count++;
                     } 
                 }
                

           }

         } 
     }
     //end code to check if system taken already
   
      
     foreach ($subscription as $sub) {
         //dd($sub);
        if($current_cat->name == "Couplers"){

            $subscription_qty= $sub->qty; 
            if($sub['options']->plan_type == "Residential"){
                    $subscription_qty= $sub->qty ;
                $max ="max=".$subscription_qty; 
            }else{
                $max= " ";
            }

        }else{
            $subscription_qty = 0;
            $max= " ";
        }

    }
    if(!empty($this->auth->user())){
         $account = $this->auth->user()->account;

        $current_subscription = $account->subscription;

        if(isset($current_subscription) && $current_subscription != null ){
            $subsc =$current_subscription->all()->last();
        $current_plan_id = $subsc->plan_id;
        $current_plan= $account->Plan->find($current_plan_id);
       
        }

        
        if(isset($current_plan) && $current_plan != null){
            $current_plan_name =$current_plan->name;
        }else{
             $current_plan_name ="No_plan" ;
        }

    }else{
        $current_plan_name ="" ;
    }
        
     $product_system_data = Cart::instance('shopping')->content();

      
     // foreach ($product_system_data as $system) {
         
     // }
   $view->with('user', $user)->withCategories($categories)->with('sys_count',$sys_count)->with('current_plan',$current_plan_name)->with('max',$max)->with('subscription_qty',$subscription_qty)->with('current_cat',$current_cat)->with('product_system',$product_system);


}


}
