<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Models\AccountAddress;
use App\KegData\Models\AccountSetting;
use Illuminate\Support\Collection;
use App\KegData\Models\Timezone;

class DashboardComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Timezone $timezone, Utilities $utilities, AccountSetting $accountSetting, AccountAddress $accountAddress)
    {
        $this->auth = $auth;
        $this->timezone = $timezone;
        $this->utilities = $utilities;
        $this->accountSetting = $accountSetting;
        $this->accountAddress = $accountAddress;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        } 

        $account = $this->auth->user()->account;
        $accountType = $this->auth->user()->account->accountType;

        $verification = $this->auth->user()->userVerification;

        switch($accountType){
            case 'S':
                $tabContent = array('levels' => 'dashboard.residential.levels',
                                    'account' => 'dashboard.residential.account',
                                    'kegsetup' => 'dashboard.residential.kegsetup',
                                    'notifications' => 'dashboard.residential.notifications',
                                    'reports' => 'dashboard.residential.reports',
                                    'settings' => 'dashboard.residential.settings',
                                    'accountjs' => 'dashboard.residential.accountjs',
                                    'kegsetupjs' => 'dashboard.residential.kegsetupjs',
                                    'notificationsjs' => 'dashboard.residential.notificationsjs',
                                    'levelsjs' => 'dashboard.residential.levelsjs',
                                    'settingsjs' => 'dashboard.residential.settingsjs',
                                    'reportsjs' => 'dashboard.residential.reportsjs');
            break;
            case 'B':
                $tabContent = array('levels' => 'dashboard.brewery.levels',
                                    'account' => 'dashboard.brewery.account',
                                    'kegsetup' => 'dashboard.brewery.kegsetup',
                                    'notifications' => 'dashboard.brewery.notifications',
                                    'reports' => 'dashboard.brewery.reports',
                                    'settings' => 'dashboard.brewery.settings',
                                    'accountjs' => 'dashboard.brewery.accountjs',
                                    'kegsetupjs' => 'dashboard.brewery.kegsetupjs',
                                    'notificationsjs' => 'dashboard.brewery.notificationsjs',
                                    'levelsjs' => 'dashboard.brewery.levelsjs',
                                    'settingsjs' => 'dashboard.brewery.settingsjs',
                                    'reportsjs' => 'dashboard.brewery.reportsjs');
            break;
            case 'D':
                $tabContent = array('levels' => 'dashboard.distributor.levels',
                                    'account' => 'dashboard.distributor.account',
                                    'kegsetup' => 'dashboard.distributor.kegsetup',
                                    'notifications' => 'dashboard.distributor.notifications',
                                    'reports' => 'dashboard.distributor.reports',
                                    'settings' => 'dashboard.distributor.settings',
                                    'accountjs' => 'dashboard.distributor.accountjs',
                                    'kegsetupjs' => 'dashboard.distributor.kegsetupjs',
                                    'notificationsjs' => 'dashboard.distributor.notificationsjs',
                                    'levelsjs' => 'dashboard.distributor.levelsjs',
                                    'settingsjs' => 'dashboard.distributor.settingsjs',
                                    'reportsjs' => 'dashboard.distributor.reportsjs');
            break;
            case 'R':
                $tabContent = array('levels' => 'dashboard.restaurant.levels',
                                    'account' => 'dashboard.restaurant.account',
                                    'kegsetup' => 'dashboard.restaurant.kegsetup',
                                    'notifications' => 'dashboard.restaurant.notifications',
                                    'reports' => 'dashboard.restaurant.reports',
                                    'settings' => 'dashboard.restaurant.settings',
                                    'accountjs' => 'dashboard.restaurant.accountjs',
                                    'kegsetupjs' => 'dashboard.restaurant.kegsetupjs',
                                    'notificationsjs' => 'dashboard.restaurant.notificationsjs',
                                    'levelsjs' => 'dashboard.restaurant.levelsjs',
                                    'settingsjs' => 'dashboard.restaurant.settingsjs',
                                    'reportsjs' => 'dashboard.restaurant.reportsjs');
            break;
            case 'E':
                $tabContent = array('levels' => 'dashboard.enterprise.levels',
                                    'account' => 'dashboard.enterprise.account',
                                    'kegsetup' => 'dashboard.enterprise.kegsetup',
                                    'notifications' => 'dashboard.enterprise.notifications',
                                    'reports' => 'dashboard.enterprise.reports',
                                    'settings' => 'dashboard.enterprise.settings',
                                    'accountjs' => 'dashboard.enterprise.accountjs',
                                    'kegsetupjs' => 'dashboard.enterprise.kegsetupjs',
                                    'notificationsjs' => 'dashboard.enterprise.notificationsjs',
                                    'levelsjs' => 'dashboard.enterprise.levelsjs',
                                    'settingsjs' => 'dashboard.enterprise.settingsjs',
                                    'reportsjs' => '');
            break;
        }


       /* $view->withUser($this->auth->user())->withRoles($roles)->
        withAccount($this->auth->user()->account)->
        withTabcontent($tabContent)->withSettings($accountSettings)->
        with('accountAddress', $accountAddress)->
        withVerification($verification)->withStates($this->utilities->get_states())
        ->withCountries($this->utilities->get_countries())->withTimezone($timezone); */

        $view->withUser($this->auth->user())->withRoles($roles)-> withAccount($this->auth->user()->account)->
        withVerification($verification);
    }

}