<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentTaxRepository as Tax;
use Utilities;
use Illuminate\Contracts\Auth\Guard;

class EmployeeTaxComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Tax $taxes)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->taxes = $taxes;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

         $viewData = $view->getData();
        $taxID = $viewData['id'];

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $taxes = $this->taxes->get();

        if($taxID != 0){
            $tax = $taxes->find($taxID);
        }else{
            $tax = null;
        }

        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->withTaxes($taxes)->withTax($tax);
    }

}