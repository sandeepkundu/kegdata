<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use Utilities;
use Cart as Cart;

class GuestBlockComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth,  Utilities $utils)
    {
            $this->auth = $auth;
            $this->utils = $utils;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $user = $this->auth->user();
        $cartCount = Cart::instance('shopping')->count() + Cart::instance('subscription')->count();
        $cartTotal = Cart::total();
        $view->with('user', $user)->with('cartCount', $cartCount)->with('cartTotal', $cartTotal);
    }


}       



