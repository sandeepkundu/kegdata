<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentOrderRepository as Order;
use App\KegData\Repositories\EloquentOrderItemRepository as OrderItem;
use App\KegData\Repositories\EloquentSubscriptionRepository as Subscription;
use Utilities;

class StoreThankYouComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Utilities $utils, Order $orders, OrderItem $orderitem)
    {
            $this->auth = $auth;
            $this->account = $account;
            $this->utils = $utils;
            $this->order = $orders;
            $this->orderitem = $orderitem;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $viewData = $view->getData();
        $user = $this->auth->user();
        $account = $user->account();
        $subPrice = 0;

        $order = $this->order->withProducts($viewData['order_id']);

        if(!empty($order->subscription)){
            if($order->subscription->plan->is_quantity){
                $subPrice = $order->subscription->plan->base_price * $order->subscription->quantity;
            }else{
                $subPrice = $order->subscription->plan->price;
            }
        }



        $view->with('user', $user)->withOrder($order)->with('subPrice', $subPrice);


    }


}
