<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\Account as Account;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use Utilities;
use Cart as Cart;
class PlanComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */

    public function __construct(Guard $auth, Account $account, Utilities $utils, Plan $plans, Category $categories)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->utils = $utils;
        $this->plan = $plans;
        $this->category = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
     $user = $this->auth->user();

       //$plans = $this->plan->with('photos')->orderBy('price', 'asc')->get();
     $plans = $this->plan->orderBy('price', 'asc')->get();

     $categories = $this->category->where('is_navigation','=',1)->get();
     
     $subscription = Cart::instance('subscription')->count();
     
     $view->with('user', $user)->withPlans($plans)->withCategories($categories)->with('subscription', $subscription);


 }


}
