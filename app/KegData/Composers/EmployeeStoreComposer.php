<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use App\KegData\Repositories\EloquentPromotionRepository as Promotion;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentProductRepository as Product;
use Carbon\Carbon;
use Utilities;
use Illuminate\Contracts\Auth\Guard;

class EmployeeStoreComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Category $category, Product $product, Plan $plan, Promotion $promotion)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->category = $category;
        $this->product = $product;
        $this->plan = $plan;
        $this->promotion = $promotion;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $categories = $this->category->all();
        $promotions = $this->promotion->where('start_date', '<=', Carbon::now()->toDateTimeString())->where('end_date', '>=', Carbon::now()->toDateTimeString())->get();
        $products = $this->product->all();
        $plans = $this->plan->all();

        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->withCategories($categories)->withProducts($products)->withPlans($plans)->withPromotions($promotions);
    }

}