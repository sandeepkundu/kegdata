<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountTypeRepository as AccountType;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use Utilities;

class RegisterComposer {

    protected $accountType;
    protected $utilities;
    protected $brewDist;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(AccountType $accountType, BrewDist $brewDist, Utilities $utilities)
    {
        // Dependencies automatically resolved by service container...
        $this->accountType = $accountType;
        $this->utilities = $utilities;
        $this->brewDist = $brewDist;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = $view->getData();
        $input = $data['input'];

        if(!empty($input) && ($input['accountType'] == 'B' || $input['accountType'] == 'D')){
            $brewDistSearch = $this->brewDist->getByColumn($input['zipcode'], 'zipcode');
        }else{
            $brewDistSearch = array();
        }
        $view->with('accountTypeSelect', $this->accountType->getForSelect('description','type'))->withStates($this->utilities->get_states())->withCountries($this->utilities->get_countries())->withCurrencies($this->utilities->get_currencies())->with('brewDistSearch', $brewDistSearch);
    }

}