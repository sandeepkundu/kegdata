<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentBrewDistRepository as BrewDist;
use App\KegData\Repositories\EloquentDistributorRepository as Dist;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Config;
use Carbon\Carbon;

class BDReportsComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Dist $dist, KegData $kegData, Keg $kegs, Collection $collection, Timezone $timezone, AccountSetting $accountSetting, Utilities $utils, BrewDist $brewDist)
    {
        $this->auth = $auth;
        $this->kegdata = $kegData;
        $this->collection = $collection;
        $this->kegs = $kegs;
        $this->timezone = $timezone;
        $this->count = 0;        
        $this->accountSetting = $accountSetting;
        $this->utils = $utils;
        $this->brewDist = $brewDist;
        $this->dist = $dist;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

        $user = $this->auth->user();
        $account = $this->auth->user()->account;
        $accountID = $account->id;
        $accountType = $this->auth->user()->account->accountType;
        $accountSettings = $this->auth->user()->account->accountSetting->first();
        
        $timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);

       $reportStart = (empty($view->input['reportStart'])) ? null : $view->input['reportStart'];
        $reportEnd = (empty($view->input['reportEnd'])) ? null : $view->input['reportEnd'];
        $quickDates = (empty($view->input['quickDates'])) ? null : $view->input['quickDates'];
        $reportDates = $this->utils->reportDates();

        $brewDist = $this->brewDist->getByColumn($accountID, 'account_id')->first();
       $column = ($brewDist->type == 'B') ? 'brewery_id' : 'distributor_id';
        $kegs  = $this->kegs->getForBrewDist($brewDist->id, $column, $accountID);

        $view->with('kegs', $kegs)->with('kegdata', $this->kegdata)->with('timezone', $timezone)->with('user', $user)->with('settings', $accountSettings)->with('reportStart', $reportStart)->with('reportEnd', $reportEnd)->with('quickDates', $quickDates)->with('reportDates', $reportDates);
    }

}