<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class EmployeeCategoryComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Category $category)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 
        $viewData = $view->getData();
        $categoryID = $viewData['id'];
        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $categories = $this->category->with('photos')->get();

        if($categoryID != 0){
            $category = $categories->find($categoryID);
        }else{
            $category = null;
        }


        $view->withUser($this->auth->user())->withRoles($roles)->withAccount($this->auth->user()->account)->withCategories($categories)->withCategory($category);
    }

}