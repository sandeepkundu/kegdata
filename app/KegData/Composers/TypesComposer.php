<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Models\KegType as KegType;
use App\KegData\Repositories\EloquentKegDeviceRepository as Keg;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Models\BeerStyle;

class TypesComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    protected $beer_style;
    public function __construct(KegType $kegtype, Keg $keg, KegData $kegdata, Guard $auth)
    {

     	$this->kegtype = $kegtype;
                $this->kegs = $keg;
                $this->kegdata = $kegdata;
                $this->auth = $auth;
                $this->count = 1;
        $this->beer_style=new BeerStyle;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = $this->auth->user();

        $kegs = $this->kegs->getForSetup($user->account->id, 'account_id');

        $kegs->each(function($keg){
            $keg->deviceName = empty($keg->deviceName) ? 'Keg '.$this->count : $keg->deviceName;
            $this->count++;
        });
        
        $beer_style = array('' => 'Select Beer Style') + $this->beer_style->lists('name','id');
         $kegsForSelect = array('' => 'Select Keg') + $kegs->lists('KegFullName', 'id');
        //$types = $this->kegtype->get(['id', 'kegTypeDesc']);
        $types = $this->kegtype->where("kegTypeMeasurement","=","oz")->get(['id', 'kegTypeDesc']);
        $typesForSelect = array('' => 'Choose Type of Keg') + $types->lists('kegTypeDesc', 'id');
        $view->with('typesForSelect', $typesForSelect)->with('user', $user)->with('kegdata', $this->kegdata)
        ->with('kegs', $kegs)
        ->with('beerStyleForSelect', $beer_style)
        ->with('kegsForSelect', $kegsForSelect);
    }


}       



