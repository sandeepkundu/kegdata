<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentTaxRepository as Tax;
use App\KegData\Repositories\EloquentCategoryRepository as Category;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use DB ;

class EmployeePlanComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct( Guard $auth, Account $account, User $user, Plan $plans, Tax $tax, Utilities $utilities, Category $categories)
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->plans = $plans;
        $this->tax = $tax;
        $this->utilities = $utilities;
        $this->categories = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //$this->user = 

         $viewData = $view->getData();
        $planID = $viewData['id'];

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $plans = $this->plans->with('photos')->with('category')->get();
       $category = $this->categories->get();
        $tax = $this->utilities->withEmpty( $this->tax->getForSelect('name'), 'Select Tax');

        if($planID != 0){
            $plan = $plans->find($planID);
            $planCat = DB::table('category_plan')
                     ->where('plan_id', '=', $planID)
                     ->get();
        }else{
            $plan = null;
             $planCat=null;
        }

        $view->withUser($this->auth->user())->with('planCat',$planCat)->withRoles($roles)->withAccount($this->auth->user()->account)->withPlans($plans)->withPlan($plan)->withTax($tax)->withCategories($category);
    }

}