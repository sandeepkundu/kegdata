<?php namespace App\KegData\Composers;

use Illuminate\Contracts\View\View;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentHubDeviceRepository as Hubs;
use Utilities;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Models\Account as Accounts;
use App\KegData\Models\AccountAddress ;
use Input;
use GingerBread;

class EmployeeHubComposer {


    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    protected $GingerBread;
    public function __construct( Guard $auth, AccountAddress $address , Account $account, User $user,  Hubs $hubs, Utilities $utilities )
    {
        $this->auth = $auth;
        $this->account = $account;
        $this->user = $user;
        $this->hub = $hubs;
        $this->utilities = $utilities;
         $this->address = $address;
         $this->GingerBread = new GingerBread;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       $accountID = $view->accountID;

       $action = Input::get();
        $request_order=(isset($action['order'])) ? $action['order'] : 'firstName';
        if($request_order == 'firstName' || $request_order == 'lastName' || $request_order == 'email') {

            $request_order = "users.".$request_order;
        } else {
            $request_order = "account_addresses.".$request_order;
        }
        $order_by=(isset($action['terms'])) ? $action['terms'] : 'dsc';

        //$this->user = 

        $roles = array();
        foreach ($this->auth->user()->role as $role){
            array_push($roles, $role->name);
        }

        $accounts = $this->account->getByID($accountID);
        if(isset($action['keyword']) && !empty($action['keyword'])){
           $search_value =trim($action['keyword']);
           $chr = mb_substr($search_value, 0, 1, "UTF-8");
           $caseinsence = mb_strtolower($chr, "UTF-8") != $chr;
            if($caseinsence == 1){
                $encryted_search_value=$this->GingerBread->preformEncrypt($search_value);
                $encryted_search_value_c = $this->GingerBread->preformEncrypt(lcfirst($search_value));
           } else{
                $encryted_search_value=$this->GingerBread->preformEncrypt($search_value);
                $encryted_search_value_c = $this->GingerBread->preformEncrypt(ucfirst($search_value));
           }

          // $encryted_search_value=$this->GingerBread->preformEncrypt($search_value);
           
           $users = $this->user
           ->leftJoin('account_addresses', 'account_addresses.account_id', '=', 'users.account_id')
           ->select('users.*')
            ->Where(function ($query) use ($search_value,$encryted_search_value,$encryted_search_value_c) {
                $query->where('users.firstName', 'like','%'.$encryted_search_value.'%')
                ->orWhere('users.lastName', 'like','%'.$encryted_search_value.'%')
                ->orWhere('users.email', 'like','%'.$encryted_search_value.'%')

                ->orWhere('users.firstName', 'like','%'.$encryted_search_value_c.'%')
                ->orWhere('users.lastName', 'like','%'.$encryted_search_value_c.'%')
                ->orWhere('users.email', 'like','%'.$encryted_search_value_c.'%')
                
                ->orWhere('account_addresses.city', 'like','%'.$search_value.'%')
                ->orWhere('account_addresses.stateCode', 'like','%'.$search_value.'%')
                ->orWhere('account_addresses.zipcode', 'like','%'.$search_value.'%')
                ->orWhere('account_addresses.countryCode', 'like','%'.$search_value.'%');
            })

           ->orderBy($request_order,$order_by)->get();
        } else {
            $search_value='';
           // $users = $this->user->orderBy($request_order,$order_by)->get();    

            $users = $this->user
           ->leftJoin('account_addresses', 'account_addresses.account_id', '=', 'users.account_id')
           ->select('users.*')
           ->orderBy($request_order,$order_by)->get();


        }

        
        
        $add = $this->address->all();
        if(! is_null($accountID)){
            $hubs = $this->hub->getByAccount($accountID);
            $devices = $this->hub->getDeviceByAccount($accountID);
        }else{
            $hubs = null;
            $devices=null;
        }

        $view->withUser($this->auth->user())
        ->withRoles($roles)
        ->withAccount($this->auth->user()->account)
        ->with('selectAccts', $this->utilities->withEmpty( $this->account->getForSelect('accountName'), 'Select Account'))
        ->withAccounts($accounts)
        ->with('keyword',$search_value)
        ->withHubs($hubs)->withUsers($users)->with('devices',$devices)->with('address',$add);
    }

}