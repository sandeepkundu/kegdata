<?php namespace App\KegData\Facades;

class KegDataProcessor extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'kegdataprocessor';
    }
}
