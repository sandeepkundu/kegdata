<?php namespace App\KegData\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \KegData\Infrastructure\GingerBread
 */
class GingerBreadFacade extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'GingerBread'; }

}
