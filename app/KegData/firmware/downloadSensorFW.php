<?php

  if (! isset($_GET['a']))
  {
    echo "ERR|no data";
    exit;
  }
  $dataIn = $_GET['a'];
  if ($dataIn == "GETSensorInfo")
  {
    if (! file_exists("SensorInfo.txt"))
    {
      echo "ERR|no data";
      exit;
    }
    else
    {
     
      $fh = fopen("SensorInfo.txt", 'r') or
        die("File does not exist or bad permissions");
      $filename = trim(fgets($fh));
      $fwMajorVer = trim(fgets($fh));
      $fwMinorVer = trim(fgets($fh));
      $fwCrc = trim(fgets($fh));
      $fwSize = trim(fgets($fh));
      echo $filename;
      echo "\n";
      echo $fwMajorVer;
      echo "\n";
      echo $fwMinorVer;
      echo "\n";
      echo $fwCrc;
      echo "\n";
      echo $fwSize;
      //echo filesize($filename);
      echo "\n";
      fclose($fh);
      exit;
    }
  }
  else
  {
    // Data stram should look like this
    // 16bit address|Number of records to read after address|K
    $dataParts = explode("|",$dataIn);
    if (Count($dataParts) != 3)
    {
        echo "ERR|Incomplete Data";
        exit;
    }
    if (strtoupper($dataParts[2])!= "K")
    {
        echo "ERR|Incomplete Data";
        exit;
    }
    // SensorInfo file to get name of IHex file to read
    if (! file_exists("SensorInfo.txt"))
    {
      echo "ERR|No SensorInfo File";
      exit;
    }
    else
    {
      $fh = fopen("SensorInfo.txt", 'r') or
        die("File does not exist or bad permissions");
      $line = fgets($fh);
      fclose($fh);
    }

    $filename = trim($line);
    if (! file_exists($filename))
    {
        echo $filename;
        echo "does not exist";
    }
    $fp1 = fopen($filename, 'rb') or
        die("File does not exist or bad permissions");
    // Read the binary file at the offset.
    fseek($fp1, intval($dataParts[0], 10));
    $data = fread($fp1, $dataParts[1]);
    echo($data);
    if(feof($fp1))
    {
    }
    fclose($fp1);
  }
?>
