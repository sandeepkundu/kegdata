<?php

  if (! isset($_GET['a']))
  {
    echo "ERR|no data";
    exit;
  }
  $dataIn = $_GET['a'];
  if ($dataIn == "GETHubInfo")
  {
    if (! file_exists("HubInfo.txt"))
    {
      echo "ERR|no data";
      exit;
    }
    else
    {
      $fh = fopen("HubInfo.txt", 'r') or
        die("File does not exist or bad permissions");
      $filename = trim(fgets($fh));
      $fwMajorVer = trim(fgets($fh));
      $fwMinorVer = trim(fgets($fh));
      echo $filename;
      echo "\n";
      echo $fwMajorVer;
      echo "\n";
      echo $fwMinorVer;
      echo "\n";
      fclose($fh);

      $fp1 = fopen($filename, 'r+') or
          die("File does not exist or bad permissions");
      $lineNum = 0;
      while (!feof($fp1))
      {
          $rec = fgets($fp1);
          $lineNum += 1;
      }
      $lineNum -= 1;

      echo $lineNum;
      echo "\n";

      fclose($fp1);
      exit;
    }
  }
  else
  {
    // Data stream should look like this
    // StartRecord(line) number|Number of records to read after|K
    $dataParts = explode("|",$dataIn);
    if (Count($dataParts) != 3)
    {
        echo "ERR|Incomplete Data";
        exit;
    }
    if (strtoupper($dataParts[2])!= "K")
    {
        echo "ERR|Incomplete Data";
        exit;
    }
    // HubInfo file to get name of IHex file to read
    if (! file_exists("HubInfo.txt"))
    {
      echo "ERR|No HubInfo File";
      exit;
    }
    else
    {
      $fh = fopen("HubInfo.txt", 'r') or
        die("File does not exist or bad permissions");
      $recline = fgets($fh);
      fclose($fh);
    }

    $filename = trim($recline);
    if (! file_exists($filename))
    {
        echo $filename;
        echo "does not exist";
    }
    $fp1 = fopen($filename, 'r+') or
        die("File does not exist or bad permissions");
    // Read the IHex file until we find the correct line number 
    $found = FALSE;
    $lineNum = 0;
    $startLine = intval($dataParts[0]);
    while (!feof($fp1))
    {
        if ($lineNum == $startLine)
        {
            $found = TRUE;
            break;
	}
        $rec = fgets($fp1);
        $lineNum += 1;
    }
    // Continue reading until we exhaust the number of IHex records
    // or we reach the end of file
    if ($found == TRUE)
    {
        for ($lineNum = 0; $lineNum < intval($dataParts[1]); $lineNum++ )
        {
            if (!feof($fp1))
            {
                $rec = fgets($fp1);
                echo $rec;
            }
            else
            {
                break;
            }
        }
    }
    fclose($fp1);
  }
?>
