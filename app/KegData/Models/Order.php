<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {

	use SoftDeletes;
	//
	protected $table = 'orders';
	protected $fillable = array('subtotal', 'shipping', 'tax', 'total', 'status');

	/**
	* Set up relationships
	*/
	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}

	public function user(){
		return $this->belongsTo('App\KegData\Models\User');
	}

	public function tax(){
		return $this->belongsTo('App\KegData\Models\Tax');
	}

	public function orderAddress(){
		return $this->hasMany('App\KegData\Models\OrderAddress');
	}

	public function orderItem(){
		return $this->hasMany('App\KegData\Models\OrderItem');
	}

	public function product(){
		return $this->hasManyThrough('App\KegData\Models\Product', 'App\KegData\Models\OrderItem', 'order_id', 'product_id');
	}

	public function subscription(){
          		return $this->hasOne('App\KegData\Models\Subscription');
        	}

}
