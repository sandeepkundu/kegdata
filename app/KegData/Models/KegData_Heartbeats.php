<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class KegData_Heartbeats extends Model
{
    protected $table = 'kd_heartbeats';

    protected $fillable = array('');

    /**
	 * Set up relationships
	 *
	 */

	 public function account(){
       return $this->belongsTo('App\KegData\Models\Account');
     }

     public function kegDevice(){
       return $this->belongsTo('App\KegData\Models\KegDevice', 'deviceID', 'id');
     }

     public function brewery(){
         return $this->belongsTo('App\KegData\Models\Brewery', 'brwery_id', 'id');
     }

     public function distributor(){
         return $this->belongsTo('App\KegData\Models\Distributor', 'distributor_id', 'id');
     }

     public function beer(){
       return $this->belongsTo('App\KegData\Models\Beer', 'product_id');
     }

     public function kegtype(){
       return $this->belongsTo('App\KegData\Models\KegType', 'kegType', 'kegType');
     }

     public function getDates(){
       return ['sent_at'];
     }

}
