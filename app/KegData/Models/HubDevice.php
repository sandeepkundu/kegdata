<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HubDevice extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'hubdevices';

	protected $dates = ['deleted_at'];

	protected $fillable = array('hubMac','deviceName');


	/**
	 * Set up relationships
	 *
	 */

	  public function account(){
	    return $this->belongsTo('App\KegData\Models\Account');
	  }

	  public function kegdatatransmit(){
	    return $this->hasMany('App\KegData\Models\KegData_Transmit', 'hubmac', 'hubmac');
	  }


	  /**
	  * Accessors & Mutators
	  */
	    public function getHubMacAttribute(){
	    $mac = $this->attributes['hubMac'];
	    $formatted = substr($mac, 0, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 2, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 4, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 6, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 8, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 10, 2);

	    return $formatted;
	  }
}

?>