<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VariantOption extends Model {

	use SoftDeletes;
	//

	protected $table = 'variant_options';
	protected $fillable = array('name', 'description', 'price', 'sort_order');

	/**
	* Set up relationships
	*/

	public function orderItem(){
		return $this->hasMany('App\KegData\Models\OrderItem');
	}

	public function variant(){
		return $this->belongsTo('App\KegData\Models\Variant');
	}
}
