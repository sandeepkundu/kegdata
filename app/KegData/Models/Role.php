<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'roles';

	protected $fillable = array('name');


	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function user(){
		$this->belongsToMany('App/KegData/Models/User', 'user_roles', 'role_id', 'user_id');
	}

}

?>