<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model {

	use SoftDeletes;
	//
	protected $table = 'variants';
	protected $fillable = array('name', 'description', 'type', 'price');

	/**
	* Set up relationships
	*/

	public function orderItem(){
		return $this->hasMany('App\KegData\Models\OrderItem');
	}

	public function products(){
		return $this->belongsToMany('App\KegData\Models\Product');
	}

	public function variantOption(){
		return $this->hasMany('App\KegData\Models\VariantOption');
	}
}
