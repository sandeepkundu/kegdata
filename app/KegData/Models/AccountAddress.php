<?php namespace App\KegData\Models;

use Eloquent;
use GingerBread;

class AccountAddress extends Eloquent {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'account_addresses';

	protected $fillable = array('address1', 'address2', 'city', 'state', 'zip', 'county', 'countryCode', 'latitude', 'longitude');

	protected $encryption = array('address1', 'address2');

	protected $GingerBread;


	public function __construct(){
		parent::__construct();
		$this->GingerBread = new GingerBread;
	}

	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}

	public function orderAddress(){
		return $this->hasOne('App\KegData\Models\OrderAddress');
	}

	/**
	* Set Accessors and Mutators
	*
	*/

	public function getAddress1Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['address1']);
	}

	public function getAddress2Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['address2']);
	}

	public function setAddress1Attribute($value){
		$this->attributes['address1'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setAddress2Attribute($value){
		$this->attributes['address2'] = $this->GingerBread->preformEncrypt($value);
	}



}

?>