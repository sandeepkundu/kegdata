<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EnterpriseCode extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'enterprisecodes';

	protected $dates = ['deleted_at'];

	protected $fillable = array('enterprisecode');


	/**
	 * Set up relationships
	 *
	 */

    public function account(){
      return $this->belongsTo('App\KegData\Models\Account');
    }

    public function primaryAccount(){
    	return $this->belongsTo('App\KegData\Models\Account','primary_account_id');
    }


}

?>