<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeerStyle extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'beer_styles';
	protected $primaryKey='id';
	//protected $dates = ['deleted_at'];

	protected $fillable = array('id', 'name', 'status');


	/**
	 * Set up relationships
	 *
	 */
  



}

?>