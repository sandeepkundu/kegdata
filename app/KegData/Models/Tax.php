<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model {

	use SoftDeletes;
	//

	protected $table = 'tax';
	protected $fillable = array('name', 'rate');

	/**
	* Set Up Relationships
	*/

	public function order(){
		return $this->hasMany('App\KegData\Models\Order');
	}

	public function plan(){
		return $this->hasMany('App\KegData\Models\Plan');
	}

	public function product(){
		return $this->hasMany('App\KegData\Models\Product');
	}
}
