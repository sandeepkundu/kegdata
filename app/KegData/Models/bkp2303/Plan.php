<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model {

	use SoftDeletes;
	//

	protected $table = 'plans';
	protected $fillable = array('name', 'slug', 'description', 'short_description', 'meta_description', 'meta_keywords', 'price', 'term', 'term_description','interval');

	
	public function tax(){
		return $this->belongsTo('App\KegData\Models\Tax');
	}

	public function photos(){
		return $this->morphToMany('App\KegData\Models\Photo', 'photoable');
	}

	public function subscription(){
		return $this->hasMany('App\KegData\Models\Subscription');
	}

	public function category(){
		return $this->belongsToMany('App\KegData\Models\Category');
	}

	public function account(){
		return $this->belongsTo('App\KegData\Models\Account', 'stripe_plan', 'slug');
	}
}
