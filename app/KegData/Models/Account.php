<?php namespace App\KegData\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GingerBread;
class Account extends Eloquent {

    use SoftDeletes;
    public static $snakeAttributes = false;


  
	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'accounts';

  protected $dates = ['deleted_at'];

	protected $fillable = array('accountName', 'accountType', 'accountEmail1', 'accountEmail2', 'accountPhone1', 'accountPhone2', 'website','email_alert','sms_alert','optional_1','optional_2','stripe_active','stripe_id','stripe_customer_id','stripe_subscription','stripe_plan','last_four','trial_ends_at','sms_alert_country');

  protected $encryption = array('accountName', 'accountEmail1', 'accountEmail2', 'accountPhone2', 'accountPhone2');

  protected $GingerBread;


  public function __construct(){
    parent::__construct();
    $this->GingerBread = new GingerBread;
  }
	/**
	 * Set up relationships
	 *
	 */
	public function accountAddress(){
		return $this->hasMany('App\KegData\Models\AccountAddress');
	}

        public function accountSetting(){
          return $this->hasMany('App\KegData\Models\AccountSetting');
        }

        public function account_type(){
          return $this->belongsTo('App\KegData\Models\Account_Type', 'accountType', 'type');
        }

        public function user(){
          return $this->hasMany('App\KegData\Models\User');
        }

        public function primaryAccount(){
          return $this->belongsTo('App\KegData\Models\User', 'primaryContact', 'id');
        }

        public function hubDevice(){
          return $this->hasMany('App\KegData\Models\HubDevice');
        }

        public function kegDevice(){
          return $this->hasMany('App\KegData\Models\KegDevice');
        }

        public function enterpriseCode(){
          return $this->hasOne('App\KegData\Models\EnterpriseCode');
        }

        public function primaryEnterpriseCode(){
        	return $this->hasMany('App\KegData\Models\EnterpriseCode', 'primary_account_id');
        }

        public function deviceNotification(){
          return $this->hasMany('App\KegData\Models\Device_Notification');
        }

        public function kegdata(){
          return $this->hasMany('App\KegData\Models\KegData');
        }

        public function mailListKey(){
          return $this->hasOne('App\KegData\Models\MailListKey', 'accountID');
        }

        public function brewery_distributor(){
          return $this->hasOne('App\KegData\Models\Brewery_Distributor');
        }

        public function brewery(){
          return $this->hasOne('App\KegData\Models\Brewery');
        }

        public function distributor(){
          return $this->hasOne('App\KegData\Models\Distributor');
        }

        public function role(){
          return $this->hasManyThrough('App\KegData\Models\Role', 'user', 'role_id', 'user_id');
        }

        public function order(){
          return $this->hasMany('App\KegData\Models\Order');
        }

        public function orderAddress(){
          return $this->hasMany('App\KegData\Models\OrderAddress');
        }

        public function plan(){
          return $this->hasOne('App\KegData\Models\Plan', 'slug', 'stripe_plan');
        }

        public function reportLog(){
          return $this->hasMany('App\KegData\Models\Report_Log');
        }

        public function subscription(){
          return $this->hasOne('App\KegData\Models\Subscription');
        }


  /*
  *
  * Accessors & Mutators
  *
  */
  public function getAccountNameAttribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['accountName']);
  }

  public function getAccountEmail1Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['accountEmail1']);
  }

  public function getAccountEmail2Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['accountEmail2']);
  }

  public function getAccountPhone1Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['accountPhone1']);
  }

  public function getAccountPhone2Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['accountPhone2']);
  }

  public function setAccountNameAttribute($value){
    $this->attributes['accountName'] = $this->GingerBread->preformEncrypt($value);
  }

  public function setAccountEmail1Attribute($value){
    $this->attributes['accountEmail1'] = $this->GingerBread->preformEncrypt($value);
  }

  public function setAccountEmail2Attribute($value){
    $this->attributes['accountEmail2'] = $this->GingerBread->preformEncrypt($value);
  }

  public function setAccountPhone1Attribute($value){
    $this->attributes['accountPhone1'] = $this->GingerBread->preformEncrypt($value);
  }

  public function setAccountPhone2Attribute($value){
    $this->attributes['accountPhone2'] = $this->GingerBread->preformEncrypt($value);
  }

  /*Helpful Functions*/
  public function prettyAccountName(){
    return strtolower(preg_replace('/[^a-zA-Z0-9]+/', '', $this->GingerBread->preformDecrypt($this->attributes['accountName'])));
  }
}

?>