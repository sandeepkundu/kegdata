<?php namespace App\KegData\Models; 

use Eloquent;
use GingerBread;

class Distributor_Address extends Eloquent {


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'distributors_addresses';

	protected $fillable = array('address1','address2','city','stateCode','zipcode','countryCode','latitude','longitude');

	protected $encryption = array('address1','address2');

	protected $GingerBread;

	public function __construct(){
		parent::__construct();
		$this->GingerBread = new GingerBread;
	}

	/**
	 * Set up relationships
	 *
	 */

    public function distributor(){
      return $this->belongsTo('App\KegData\Models\distributor');
    }

    /**
    *
    * Accessors
    *
    */

    public function getAddress1Attribute(){
    	return strtoupper($this->GingerBread->preformDecrypt($this->attributes['address1']));
    }

    public function getAddress2Attribute(){
    	return strtoupper($this->GingerBread->preformDecrypt($this->attributes['address2']));
    }

    public function getCityAttribute(){
    	return strtoupper($this->attributes['city']);
    }

    public function getStateCodeAttribute(){
    	return strtoupper($this->attributes['stateCode']);
    }

    public function getCountyAttribute(){
    	return strtoupper($this->attributes['county']);
    }

    public function getCountryCodeAttribute(){
    	return strtoupper($this->attributes['countryCode']);
    }

    /**
    *
    * Mutators
    *
    */

    public function setAddress1Attribute($value){
    	$this->attributes['address1'] = $this->GingerBread->preformEncrypt($value);
    }

    public function setAddress2Attribute($value){
    	$this->attributes['address2'] = $this->GingerBread->preformEncrypt($value);
    }

    public function setCityAttribute($value){
    	$this->attributes['city'] = strtoupper($value);
    }

    public function setStateCodeAttribute($value){
    	$this->attributes['stateCode'] = strtoupper($value);
    }

    public function setCountyAttribute($value){
    	$this->attributes['county'] = strtoupper($value);
    }

    public function setCountryCodeAttribute($value){
    	$this->attributes['countryCode'] = strtoupper($value);
    }


}

?>