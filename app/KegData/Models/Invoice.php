<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model {

	use SoftDeletes;
	//
	protected $table = 'invoices';
	protected $fillable = array('subtotal', 'tax', 'total', 'status');

	/**
	* Set up relationships
	*/
	public function order(){
		return $this->belongsTo('App/KegData/Models/Order');
	}

}
