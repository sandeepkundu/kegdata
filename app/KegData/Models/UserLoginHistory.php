<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class UserLoginHistory extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'user_login_history';

	protected $fillable = array('ipAddress', 'login_date', 'success');

	/**
	* Relationships
	* 
	*
	*/

	public function user(){
		$this->belongsTo('App/KegData/Models/User', 'id', 'id');
	}

}

?>