<?php  namespace App\KegData\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GingerBread;

class Brewery extends Eloquent {

    use SoftDeletes;

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'breweries';

  protected $dates = ['deleted_at'];

	protected $fillable = array('name', 'primaryContactName', 'email1', 'email2', 'phone', 'fax');

	protected $encryption = array('email1', 'email2','phone','fax');

  protected $GingerBread;


  public function __construct(){
    parent::__construct();
    $this->GingerBread = new GingerBread;
  }


	/**
	 * Set up relationships
	 *
	 */

    public function breweryAddress(){
      return $this->hasOne('App\KegData\Models\Brewery_Address');
    }

    public function account(){
      return $this->belongsTo('App\KegData\Models\Account');
    }
    
    public function user(){
      return $this->belongsTo('App\KegData\Models\User','primaryContactID');
    }

    public function beer(){
      return $this->hasMany('App\KegData\Models\Beer');
    }

    public function kegDevice(){
    	return $this->hasMany('App\KegData\Models\KegDevice');
    }

    public function mailListKey(){
      return $this->hasOne('App\KegData\Models\MailListKey');
    }

    /**
     *
     * Mutators
     *
     */

    public function getNameAttribute(){
      return strtoupper($this->attributes['name']);
    }

    public function getEmail1Attribute(){
      return $this->GingerBread->preformDecrypt($this->attributes['email1']);
    }

    public function getEmail2Attribute(){
      return $this->GingerBread->preformDecrypt($this->attributes['email2']);
    }

    public function getPhoneAttribute(){
       return $this->GingerBread->preformDecrypt($this->attributes['phone']);
    }

    public function getFaxAttribute(){
       return $this->GingerBread->preformDecrypt($this->attributes['fax']);
    }

    /**
    *
    * Accessors
    *
    */

    public function setNameAttribute($value){

      $this->attributes['name'] = strtoupper($value);
    }

    public function setEmail1Attribute($value){
      $this->attributes['email1'] = $this->GingerBread->preformEncrypt($value);
    }

    public function setEmail2Attribute($value){
      $this->attributes['email2'] = $this->GingerBread->preformEncrypt($value);
    }

    public function setPhoneAttribute($value){
      $this->attributes['phone'] = $this->GingerBread->preformEncrypt($value);
    }

    public function setFaxAttribute($value){
      $this->attributes['fax'] = $this->GingerBread->preformEncrypt($value);
    }

}

?>