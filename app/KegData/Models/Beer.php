<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Beer extends Model {

	use SoftDeletes;


	 protected $dates = ['deleted_at'];
	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'beers';

	protected $fillable = array('name','style','location','description');


	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function brewery(){
		return $this->belongsTo('App\KegData\Models\Brewery');
	}

	public function distributor(){
		return $this->belongsToMany('App\KegData\Models\Distributor', 'distributors_beers', 'beer_id', 'distributor_id');
	}

	public function kegDevice(){
		return $this->hasMany('App\KegData\Models\KegDevice');
	}


}

?>