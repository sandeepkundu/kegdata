<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KegSpec extends Model
{
    /**
     * The database table used by the Model
     * @var string
     */

    protected $table = 'keg_specs';
    protected $dates = ['deleted_at'];
    protected $fillable = array('diameter', 'dipTubeHeight',
        'finalGravity', 'abv', 'ibu', 'srm', 'upc', 'style',
        'description');

    /**
     * Set up relationships
     */

    public function account(){
        return $this->belongsTo('App\KegData\Models\Account');
    }

    public function kegdevice(){
        return $this->belongsTo('App\KegData\Models\KegDevice', 'id', 'kegdevice_id');
    }

    public function kegtypes(){
        return $this->belongsTo('App\KegData\Models\KegType', 'kegtype', 'id');
    }

    public function beer(){
        return $this->belongsTo('App\KegData\Models\Beer', 'product_id');
    }

    public function distributor(){
        return $this->belongsTo('App\KegData\Models\Distributor');
    }

    public function brewery(){
        return $this->belongsTo('App\KegData\Models\Brewery');
    }

    /**
     * Set up accessors
     */

    /**
     * Set up mutators
     */
} /* Close class KegSpec */
