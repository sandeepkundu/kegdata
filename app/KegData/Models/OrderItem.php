<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrderItem extends Model {

	use SoftDeletes;
	//
	protected $table = 'order_items';
	protected $fillable = array('price', 'quantity');

	/**
	* Set up Relationships
	*/
	public function order(){
		return $this->belongsTo('App\KegData\Models\Order');
	}

	public function product(){
		return $this->belongsTo('App\KegData\Models\Product');
	}

	public function variant(){
		return $this->belongsTo('App\KegData\Models\Variant');
	}

	public function variantOption(){
		return $this->belongsTo('App\KegData\Models\VariantOption');
	}
}
