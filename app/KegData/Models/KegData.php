<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
ini_set('memory_limit', '-1');
class KegData extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'kegdata';


	protected $fillable = array('');


	/**
	 * Set up relationships
	 *
	 */

  public function account(){
    return $this->belongsTo('App\KegData\Models\Account');
  }

  public function hubDevice(){
  	return $this->belongsTo('App\KegData\Models\HubDevice', 'hubMac', 'hubMac');
  }

  public function kegDevice(){
    return $this->belongsTo('App\KegData\Models\KegDevice', 'deviceID', 'id');
  }

  public function beer(){
  	return $this->belongsTo('App\KegData\Models\Beer', 'product_id');
  }

  public function kegtype(){
  	return $this->belongsTo('App\KegData\Models\KegType', 'kegType', 'kegType');
  }

  public function getDates(){
    return ['sent_at'];
  }

}

?>