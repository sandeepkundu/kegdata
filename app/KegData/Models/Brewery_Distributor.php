<?php namespace App\KegData\Models;

use Eloquent;
use GingerBread;
class Brewery_Distributor extends Eloquent {

  /**
   * The database table used by the model
   *
   * @var string
   *
   */
  
  protected $table = 'brew_dist';
  protected $fillable = array('');

  protected $encryption = ['primaryContactName','email1','email2','phone','fax','address1','address2']; 
  protected $GingerBread;

  public function __construct(){
    parent::__construct();
    $this->GingerBread = new GingerBread;
  }

  /**
  *
  * Set up relationships on Brewery_Distribtor Vew
  *
  */
  public function account(){
    return $this->belongsTo('App\KegData\Models\Account');
  }

  public function user(){
    return $this->belongsTo('App\KegData\Models\User', 'primaryContactID');
  }

  /**
  *
  * Mutators and Accessors
  *
  */
    /*
  *
  * Accessors
  *
  */

  public function getNameAttribute(){
    return strtoupper($this->attributes['name']);
  }

  public function getPrimaryContactNameAttribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['primaryContactName']);
  }

  public function getEmail1Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['email1']);
  }

  public function getEmail2Attribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['email2']);
  }

  public function getPhoneAttribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['phone']);
  }

  public function getFaxAttribute(){
    return $this->GingerBread->preformDecrypt($this->attributes['fax']);
  }

  public function getAddress1Attribute(){
    return strtoupper($this->GingerBread->preformDecrypt($this->attributes['address1']));
  }

  public function getAddress2Attribute(){
    return strtoupper($this->GingerBread->preformDecrypt($this->attributes['address2']));
  }

  public function getCityAttribute(){
    return strtoupper($this->attributes['city']);
  }

  public function getStateCodeAttribute(){
    return strtoupper($this->attributes['stateCode']);
  }

}

?>