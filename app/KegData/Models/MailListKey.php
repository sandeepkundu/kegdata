<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class MailListKey extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'maillistkeys';

	protected $fillable = array('key','accountID', 'breweryID', 'distributorID');


	/**
	 * Set up relationships
	 *
	 */
	public function account(){
		$this->belongsTo('App\KegData\Models\Account');
	}

	public function brewery(){
		$this->belongsTo('App\KegData\Models\Brewery');
	}

	public function distributor(){
		$this->belongsTo('App\KegData\Models\Distributor');
	}

}

?>