<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class Report_Log extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'report_logs';

	protected $fillable = array('account_id', 'user_id', 'query_meta');


	/**
	 * Set up relationships
	 *
	 */
  public function account(){
    $this->belongsTo('App/KegData/Models/Account', 'account_id', 'id')
  }

  public function user(){
    $this->belongsTo('App/KegData/Models/User', 'user_id', 'user');
  }




}

?>