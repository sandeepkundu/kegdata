<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

use GingerBread;

class OrderAddress extends Model {

	protected $table = 'order_addresses';
	protected $fillable = array('type', 'name', 'address1', 'address2', 'city', 'state', 'zipcode', 'phone1', 'phone2', 'country');
	protected $encryption = array('address1', 'address2');
	protected $GingerBread;

	public function __construct(){
		parent::__construct();
		$this->GingerBread = new GingerBread;
	}
	
	/**
	* Set up relationships
	*/
	public function order(){
		return $this->belongsTo('App\KegData\Models\Order');
	}

	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}


	/**
	* Set Accessors
	*
	*/
	public function getNameAttribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['name']);
	}


	public function getAddress1Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['address1']);
	}

	public function getAddress2Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['address2']);
	}

	public function getPhone1Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['phone1']);
	}

	public function getPhone2Attribute(){
		return $this->GingerBread->preformDecrypt($this->attributes['phone2']);
	}

	/**
	* Set mutators
	*/

	public function setNameAttribute($value){
		$this->attributes['name'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setAddress1Attribute($value){
		$this->attributes['address1'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setAddress2Attribute($value){
		$this->attributes['address2'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setPhone1Attribute($value){
		$this->attributes['phone1'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setPhone2Attribute($value){
		$this->attributes['phone2'] = $this->GingerBread->preformEncrypt($value);
	}


}
