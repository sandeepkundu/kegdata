<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

	use SoftDeletes;
	//
	protected $table = 'categories';
	protected $fillable = array('name', 'slug', 'description', 'short_description', 'meta_description', 'meta_keywords','is_navigation');

	/**
	*  Set up relationships
	*
	*/
	public function products(){
		return $this->belongsToMany('App\KegData\Models\Product');
	}

	public function plans(){
		return $this->belongsToMany('App\KegData\Models\Plan');
	}

	public function photos(){
		return $this->morphToMany('App\KegData\Models\Photo', 'photoable');
	}
}
