<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class Notification_Frequency extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'notification_frequency';

	protected $fillable = array('freqDesc');


	/**
	 * Set up relationships
	 * 
	 *
	 */

	public function device_notification(){
		$this->hasMany('App/KegData/Models/Device_Notification');
	}

}

?>