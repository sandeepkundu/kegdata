<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'user_verification';

	protected $fillable = array('verificationHash', 'verificationSent', 'verificationConfirmation');

	/**
	* Relationships
	*
	*
	*/

	public function user(){
		$this->belongsTo('App\KegData\Models\User', 'id', 'id');
	}

}

?>