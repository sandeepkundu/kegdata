<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gingerbread;

class Notifications_Send_To extends Model
{

    protected $table = 'notifications_send_to';
    protected $dates = ['verified_at'];
    protected $fillable = ['send_to', 'type', 'verified_at'];
    protected $encryption = ['send_to'];
    protected $GingerBread;


    public function __construct(){
          parent::__construct();
          $this->GingerBread = new GingerBread;
    }

    //Relationships

    public function notifications(){
        return $this->belongsToMany('App\KegData\Models\Device_Notification', 'devicenotification_notifications_send_to', 'notifications_send_to_id', 'devicenotification_id');
    }

    public function history(){
        return $this->belongsTo('App\KegData\Models\Notification_History', 'id','sent_to');
    }

    public function account(){
        return $this->belongsTo('App\KegData\Models\Account', 'id', 'account_id');
    }



    //Accessors & Mutators

    public function getSendToAttribute(){
      return $this->GingerBread->preformDecrypt($this->attributes['send_to']);
    }

    public function setSendToAttribute($value){
        $this->attributes['send_to'] = $this->GingerBread->preformEncrypt($value);
    }
}
