<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class KegData_Transmit extends Model{

	protected $table = 'kegdata_transmit';
	protected $fillable = array('hubmac', 'kegmac', 'adc_pour_start', 'adc_pour', 'adc_pour_end', 'pour_length', 'temperature', 'status', 'sent_at');
	protected $dates = ['sent_at'];

	public function hubdevice(){
		return $this->belongsTo('App\KegData\Models\HubDevice', 'hubmac', 'hubmac');
	}

	public function kegdevice(){
		return $this->belongsTo('App\KegData\Models\KegDevice', 'kegmac', 'kegmac');
	}

	  /**
	  * Accessors & Mutators
	  */
	    public function getHubMacAttribute(){
	    $mac = $this->attributes['hubmac'];
	    $formatted = substr($mac, 0, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 2, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 4, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 6, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 8, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 10, 2);

	    return $formatted;
	  }

	  public function getKegMacAttribute(){
	    $mac = $this->attributes['kegmac'];
	    $formatted = substr($mac, 0, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 2, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 4, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 6, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 8, 2);
	    $formatted .= ':';
	    $formatted .= substr($mac, 10, 2);

	    return $formatted;
	  }

	  public function setKegMacAttribute($value){
	  	 $this->attributes['kegmac'] = trim(str_replace(':', '', $value));
	  }

	  public function setHubMacAttribute($value){
	  	$this->attributes['hubmac'] = trim(str_replace(':', '', $value));
	  }



}