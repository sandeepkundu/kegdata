<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model {

	use SoftDeletes;
	//

	protected $table = 'subscriptions';
	protected $fillable = array('active', 'start', 'expire');

	/**
	* Set up relationships
	*/
	public function plan(){
		return $this->belongsTo('App\KegData\Models\Plan');
	}

	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}
	public function order(){
		return $this->belongsTo('App\KegData\Models\Order');
	}

	/**
	* Set up accessors
	*/

	public function getDates(){
		return array('start', 'expire');
	}
}
