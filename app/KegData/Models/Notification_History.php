<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class Notification_History extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'Notification_History';

	protected $fillable = array('notification_id','sentDateTime', 'type');


	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function deviceNotification(){
		$this->belongsTo('App\KegData\Models\Device_Notification', 'notification_id');
	}
}

?>