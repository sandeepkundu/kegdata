<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'timezones';

	protected $fillable = array('');

	protected $appends = array('offsetTimezone');

	public function accountSetting(){
		return $this->belongsTo('App/KegData/Models/AccountSetting', 'timezone', 'id');
	}


	/**
	 *Mutator to get timezone with GMT offset and assign to
	 * offsetTimezone field
	 * @return string
	 */

	public function getOffsetTimezoneAttribute(){
		$offset = $this->offset/60/60;
		if($offset > 0){
			$offset = '+'.$offset;
		}

		return $this->timezone." (GMT ".$offset.')';
	}


}

?>