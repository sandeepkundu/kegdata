<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class StripeEvent extends Model
{
    protected $table = 'stripe_events';
    protected $fillable = array('stripe_event_id');
}
