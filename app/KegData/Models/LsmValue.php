<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class LsmValue extends Model {

	//
	protected $table = 'lsm_values';

    protected $fillable = array('kegdevice_id',
    	'm','b','comm_x','pour_date','simple_x','break_event_date','event_date',
    	'status','adc_pour_end',
    	'created_at',
    	'updated_at',
    	'deleted_at');
}