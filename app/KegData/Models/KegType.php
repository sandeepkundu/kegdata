<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KegType extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'kegtypes';

	protected $dates = ['deleted_at'];

	protected $fillable = array('kegType', 'kegTypeDesc', 'kegTypeMeasurement', 'amount', 'emptyValue', 'pic_prefix', 'imageName','adc_full');


	/**
	 * Set up relationships
	 *
	 */
  public function kegdevice(){
    $this->hasMany('App/KegData/Models/KegDevice', 'kegType');
  }

  public function kegdata(){
    $this->hasMany('App/KegData/Models/KegData', 'kegType', 'kegType');
  }




}

?>