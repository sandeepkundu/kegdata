<?php namespace App\KegData\Models;

use Illuminate\Auth\Authenticatable;
use App\Services\Eloquenttended;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;
use GingerBread;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;

	public static $snakeAttributes = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = array('email', 'password', 'firstName', 'lastName', 'email2', 'phone1', 'phone2', 'isActive', 'isAdmin', 'api_token','lastlogin');

	protected $encryption = array();//array('email', 'firstName', 'lastName', 'email2', 'phone1', 'phone2');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	protected $GingerBread;

	public function __construct(){
		parent::__construct();
		$this->GingerBread = new GingerBread;
	}

	public function account(){
		return $this->belongsTo('App\KegData\Models\Account', 'account_id', 'id');
	}

	public function primaryAccount(){
		return $this->hasOne('App\KegData\Models\Account', 'primaryContact', 'id');
	}

	public function accountAddress(){
		return $this->hasManyThrough('App\KegData\Models\AccountAddress', 'App\KegData\Models\Account', 'id', 'account_id');
	}

	public function accountSettings(){
		return $this->hasManyThrough('App\KegData\Models\AccountSetting', 'App\KegData\Models\Account', 'id', 'account_id');
	}

	public function brewery(){
		return $this->hasOne('App\KegData\Models\Brewery', 'primaryContactID', 'id');
	}

	public function distributor(){
		return $this->hasOne('App\KegData\Models\Distributor', 'primaryContactID', 'id');
	}

	public function breweryDistributor(){
		return $this->hasOne('App\KegData\Models\Brewery_Distributor', 'primaryContactID');
	}

	public function kegDevice(){
		return $this->hasManyThrough('App\KegData\Models\KegDevice', 'App\KegData\Models\Account', 'id', 'account_id');
	}

	public function role(){
		return $this->belongsToMany('App\KegData\Models\Role', 'user_roles', 'user_id', 'role_id');
	}

	public function userLoginHistory(){
		return $this->hasMany('App\KegData\Models\UserLoginHistory', 'id', 'id');
	}

	public function userVerification(){
		return $this->hasOne('App\KegData\Models\UserVerification', 'id', 'id');
	}

	public function order(){
		return $this->hasMany('App\KegData\Models\Order');
	}
	public function timezone(){
		return $this->hasManyThrough('App\KegData\Models\Timezone', 'App\KegData\Models\AccountSetting', 'timezone', 'id')->select(['id','timezone']);
	}

	public function reportLog(){
		return $this->hasMany('App\KegData\Models\Report_Log');
	}

	/**
	*
	* Accessors
	*
	*/

	public function getEmailAttribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	public function getFirstNameAttribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	public function getLastNameAttribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	public function getEmail2Attribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	public function getPhone1Attribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	public function getPhone2Attribute($value){
		return $this->GingerBread->preformDecrypt($value);
	}

	/**
	* Mutators
	*
	*/

	public function setEmailAttribute($value){
		$this->attributes['email'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setFirstNameAttribute($value){
		$this->attributes['firstName'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setLastNameAttribute($value){
		$this->attributes['lastName'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setEmail2Attribute($value){
		$this->attributes['email2'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setPhone1Attribute($value){
		$this->attributes['phone1'] = $this->GingerBread->preformEncrypt($value);
	}

	public function setPhone2Attribute($value){
		$this->attributes['phone2'] = $this->GingerBread->preformEncrypt($value);
	}


	public function hasRole($value){
		foreach($this->role as $role){
			if($role->name == $value) return TRUE;
		}

		return false;
	}

}
