<?php namespace App\KegData\Models; 



use Illuminate\Database\Eloquent\Model;




class EmailTemplate extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';

    protected $fillable = array('user_id','content','status','type','subject','cron_name');
}