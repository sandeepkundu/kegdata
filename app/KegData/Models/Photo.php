<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model {

	use SoftDeletes;
	//

	protected $table = 'photos';
	protected $fillable = array('path', 'width', 'height', 'mime');

	public function category(){
		return $this->morphedByMany('App\KegData\Models\Category', 'photoable');
	}

	public function product(){
		return $this->morphedByMany('App\KegData\Models\Product', 'photoable');
	}

	public function promotion(){
		return $this->morphedByMany('App\KegData\Models\Promotion', 'photoable');
	}

	public function plan(){
		return $this->morphedByMany('App\KegData\Models\Plan', 'photoable');
	}

}
