<?php namespace App\KegData\Models; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KegDevice extends Model{


	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'kegdevices';

	protected $dates = ['deleted_at'];

	protected $fillable = array('kegMac', 'account_id', 'deviceName','kegType','product_id','distributor_id','brewery_id','volumeType','temperatureType','showDevice', 'style', 'description','lsm_m');


	/**
	 * Set up relationships
	 *
	 */

  public function account(){
    return $this->belongsTo('App\KegData\Models\Account');
  }

  public function beer(){
  	return $this->belongsTo('App\KegData\Models\Beer', 'product_id');
  }

  public function distributor(){
  	return $this->belongsTo('App\KegData\Models\Distributor');
  }

  public function brewery(){
  	return $this->belongsTo('App\KegData\Models\Brewery');
  }

  public function kegtype(){
  	return $this->belongsTo('App\KegData\Models\KegType', 'kegType', 'id');
  }

  public function kegdata(){
    return $this->hasMany('App\KegData\Models\KegData', 'deviceID', 'id');
  }

    public function kegdata_transmit(){
    return $this->hasMany('App\KegData\Models\KegData_Transmit', 'kegmac', 'kegmac');
  }

  public function breweryAddress(){
      return $this->hasManyThrough('App\KegData\Models\Brewery_Address', 'App\KegData\Models\Brewery', 'id', 'brewery_id');
  }

  public function distributorAddress(){
    return $this->hasManyThrough('App\KegData\Models\Distributor_Address', 'App\KegData\Models\Distributor', 'id', 'distributor_id');
  }

  public function notifications(){
    return $this->hasMany('App\KegData\Models\Device_Notification', 'kegdevice_id', 'id');
  }


public function tapped(){
  return $this->hasOne('App\KegData\Models\KegData', 'deviceID', 'id')->select(['deviceID', 'sent_at'])->where('status', '=', '101');
}

public function recentPour(){
  return $this->hasOne('App\KegData\Models\KegData', 'deviceID', 'id')->select(['deviceID', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount', 'kegType', 'kegTypeDesc', 'pic_prefix'])->where('status', '=', '11');
}

public function temperature(){
  return $this->hasOne('App\KegData\Models\KegData', 'deviceID', 'id')->select(['deviceID', 'temperature', 'sent_at','temperatureType'])->where('status', '=', '1001')->orWhere('status', '=', '11');
}

public function tempHistory(){
  return $this->hasMany('App\KegData\Models\KegData', 'deviceID', 'id')->select(['deviceID', 'temperature', 'sent_at', 'temperatureType'])->where('status', '=', '1001')->orWhere('status', '=', '11');
}

public function pourHistory(){
  return $this->hasMany('App\KegData\Models\KegData', 'deviceID', 'id')->select(['deviceID', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount'])->where('status', '=', '11');
}

  public function getKegFullNameAttribute(){
    $mac = $this->attributes['kegMac'];
    $formatted = substr($mac, 6, 2);
    $formatted .= ':';
    $formatted .= substr($mac, 8, 2);
    $formatted .= ':';
    $formatted .= substr($mac, 10, 2);

    return $formatted.' '.$this->attributes['deviceName'];
  }

}

?>