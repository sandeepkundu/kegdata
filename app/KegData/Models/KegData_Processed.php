<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class KegData_Processed extends Model
{
    protected $table = 'kegdata_processed';
    protected $fillable = array('hubmac', 'kegmac', 'kegdevice_id', 'adc_pour_start', 'adc_pour', 'adc_pour_end', 'pour_length', 'temperature', 'status', 'kegreset', 'reset_code', 'remaining_ounces', 'slope', 'yIntercept', 'sent_at');
    protected $dates = ['sent_at'];

    public function hubdevice()
    {
        return $this->belongsTo('App\KegData\Models\KegDevice', 'hubmac', 'hubmac');
    }

    public function kegdevice()
    {
        return $this->belongsTo('App\KegData\Models\KegDevice', 'kegmac', 'kegMac');
    }

}
