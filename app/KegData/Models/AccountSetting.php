<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AccountSetting extends Model {

	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'account_settings';

	protected $fillable = array('currency', 'volumeType', 'temperatureType', 'daylightSavings', 'timezone', 'weekStart', 'dayStart', 'dayEnd');

	protected $appends = array('daystart12', 'amstart', 'dayend12', 'pmend');
	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}

	public function timezone(){
		return $this->hasOne('App\KegData\Models\Timezone', 'id', 'timezone');
	}

	public function getDaystart12Attribute(){
		$str = explode(':', $this->attributes['dayStart']);
		return Carbon::createFromTime($str[0], $str[1], $str[2])->format('g:i:s');
	}

	public function getDayend12Attribute(){
		$str = explode(':', $this->attributes['dayEnd']);
		return Carbon::createFromTime($str[0], $str[1], $str[2])->format('g:i:s');
	}

	public function getAmstartAttribute(){
		$str = explode(':', $this->attributes['dayStart']);
		return Carbon::createFromTime($str[0], $str[1], $str[2])->format('a');
	}

	public function getAmendAttribute(){
		$str = explode(':', $this->attributes['dayEnd']);
		return Carbon::createFromTime($str[0], $str[1], $str[2])->format('a');
	}

}

?>