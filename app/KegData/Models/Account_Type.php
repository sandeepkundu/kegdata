<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class Account_Type extends Model{

	protected $table = 'account_types';
	protected $fillable = array('type', 'description');

	public function account(){
		return $this->hasMany('App\KegData\Models\Account', 'accountType', 'type');
	}

}