<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model {

	use SoftDeletes;
	//
	protected $table = 'promotions';
	protected $fillable = array('name', 'description', 'slug', 'short_description', 'meta_description', 'meta_keywords', 'start_date', 'end_date', 'percentage', 'apply_to_products', 'apply_to_subscriptions');

	/**
	* Set up Relationships
	*/
	public function product(){
		return $this->belongsToMany('App\KegData\Models\Product');
	}

	public function photos(){
		return $this->morphToMany('App\KegData\Models\Photo', 'photoable');
	}

	/**
	 * Set up  accessors
	 */
	public function getDates(){
	    return ['start_date', 'end_date'];
	  }
}
