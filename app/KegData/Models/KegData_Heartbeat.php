<?php

namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;

class KegData_Heartbeat extends Model
{
    protected $table = 'kegdata_heartbeats';
    protected $fillable = array('hubmac', 'kegmac', 'adc_pour_start', 'adc_pour', 'adc_pour_end', 'pour_length', 'temperature', 'status', 'sent_at');
    protected $dates = ['sent_at'];

    public function hubdevice()
    {
        return $this->belongsTo('App\KegData\Models\HubDevice', 'hubmac', 'hubmac');
    }

    public function kegdevice()
    {
        return $this->belongsTo('App\KegData\Models\KegDevice', 'kegmac', 'kegMac');
    }

}
