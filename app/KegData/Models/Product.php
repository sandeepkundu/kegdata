<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {

	use SoftDeletes;
	//

	protected $table = 'products';
	protected $fillable = array('part_id', 'name', 'slug', 'description', 'short_description', 'meta_description', 'meta_keywords', 'price',  'stock');

	/**
	* Set up relationships
	*/

	public function orderItem(){
		return $this->hasMany('App\KegData\Models\OrderItem');
	}

	public function photos(){
		return $this->morphToMany('App\KegData\Models\Photo', 'photoable');
	}

	public function tax(){
		return $this->belongsTo('App\KegData\Models\Tax');
	}

	public function promotion(){
		return $this->belongsToMany('App\KegData\Models\Promotion');
	}

	public function variant(){
		return $this->belongsToMany('App\KegData\Models\Variant');
	}
	
	public function category(){
		return $this->belongsToMany('App\KegData\Models\Category');
	}
}
