<?php

namespace App\KegData\models;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = 'shoppingcart';

    protected $fillable = ['instance','content'];
}
