<?php namespace App\KegData\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device_Notification extends Model {
	use SoftDeletes;
	/**
	 * The database table used by the model
	 *
	 * @var string
	 *
	 */
	protected $table = 'devicenotifications';

	protected $fillable = array( 'notification','value','frequency','operator','sms_notification','email_notification','email_notification_optional','email_notification_optional_2');


	/**
	 * Set up relationships
	 * Each address belongs to one account
	 *
	 */
	public function account(){
		return $this->belongsTo('App\KegData\Models\Account');
	}

	public function kegdevice(){
		return $this->belongsTo('App\KegData\Models\KegDevice');
	}

	public function notificationFrequency(){
		return $this->belongsTo('App\KegData\Models\Notification_Frequency', 'frequency', 'id');
	}

	public function notificationHistory(){
		return $this->hasMany('App\KegData\Models\Notification_History', 'notification_id');
	}

}

?>