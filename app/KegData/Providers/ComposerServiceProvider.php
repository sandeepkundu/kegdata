<?php namespace App\KegData\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            'App\KegData\Composers\RegisterComposer' => 'auth.register',
            'App\KegData\Composers\GuestBlockComposer' => 'components.guestBlock',
            'App\KegData\Composers\DashboardComposer' => ['layouts.dashboard'],
            'App\KegData\Composers\LevelComposer' => ['dashboard.residential.levels', 'dashboard.restaurant.levels', 'dashboard.enterprise.levels'],
            /* New composer for LSM level */
            'App\KegData\Composers\LevellsmnewComposer' => ['dashboard.residential.levels_newlsm', 'dashboard.restaurant.levels_newlsm', 'dashboard.enterprise.levels_newlsm'],
             'App\KegData\Composers\StorelsmComposer' => ['dashboard.residential.storelsm'],
             'App\KegData\Composers\Levelv1Composer' => ['dashboard.residential.levels_v1'],
            'App\KegData\Composers\GraphReportsV1Composer' => ['dashboard.residential.graph_reports_v1'],// graph for demo reports on 20/12/2016
           /* End new composer link*/
             'App\KegData\Composers\ReportsComposer' => ['dashboard.residential.reports', 'dashboard.restaurant.reports', 'dashboard.enterprise.reports'],
            'App\KegData\Composers\GraphReportsComposer' => ['dashboard.residential.graph_reports'],//for graph reports
            'App\KegData\Composers\GraphReportsDemoComposer' => ['dashboard.residential.graph_reports_demo'],// graph for demo reports on 20/12/2016
            'App\KegData\Composers\GraphReportsSecondComposer' => ['dashboard.residential.graph_reports2'],// graph2
            'App\KegData\Composers\GraphReportsTestComposer' => ['dashboard.residential.graph_reports_test'],// graph for test reports on 27/12/2016
             'App\KegData\Composers\ExcelComposer' => ['dashboard.restaurant.reportsexcel'],
            'App\KegData\Composers\BDLevelComposer' => ['dashboard.brewery.levels', 'dashboard.distributor.levels', 'dashboard.distributor.reports', 'dashboard.brewery.reports'],
            'App\KegData\Composers\BDReportsComposer' => ['dashboard.distributor.reports', 'dashboard.brewery.reports'],
            'App\KegData\Composers\TypesComposer' => ['dashboard.residential.kegsetup', 'dashboard.restaurant.kegsetup'],
            'App\KegData\Composers\SettingsComposer' => ['dashboard.residential.settings', 'dashboard.restaurant.settings', 'dashboard.brewery.settings', 'dashboard.distributor.settings', 'dashboard.enterprise.settings'],
            'App\KegData\Composers\NotificationsComposer' => ['dashboard.residential.notifications', 'dashboard.restaurant.notifications', 'dashboard.brewery.notifications', 'dashboard.distributor.notifications', 'dashboard.enterprise.notifications'],
            'App\KegData\Composers\AccountComposer' => ['dashboard.residential.account', 'dashboard.restaurant.account', 'dashboard.brewery.account', 'dashboard.distributor.account', 'dashboard.enterprise.account', 'dashboard.serviceprovider.account'],
            'App\KegData\Composers\ChartsComposer' => ['dashboard.residential.charts', 'dashboard.restaurant.charts'],
            'App\KegData\Composers\OrderComposer' => ['dashboard.residential.orders', 'dashboard.restaurant.orders'],
            'App\KegData\Composers\StoreComposer' => 'store.store',
            'App\KegData\Composers\PlanComposer' => 'store.plans',
            'App\KegData\Composers\ProductComposer' => 'store.product',
            'App\KegData\Composers\CartComposer' => 'store.cart',
            'App\KegData\Composers\CheckoutComposer' => 'store.checkout',
            'App\KegData\Composers\StoreThankYouComposer' => ['store.thankyou'],
            'App\KegData\Composers\EmployeeHubComposer' => 'employees.hub',
            'App\KegData\Composers\EmployeeAccountComposer' => 'employees.account',
            'App\KegData\Composers\EmployeeOrdersComposer' => 'employees.orders',
            'App\KegData\Composers\EmployeeStoreComposer' => 'employees.store',
            'App\KegData\Composers\EmployeeCategoryComposer' => 'employees.categories',
            'App\KegData\Composers\EmployeePlanComposer' => 'employees.plans',
            'App\KegData\Composers\EmployeePromotionComposer' => 'employees.promotions',
            'App\KegData\Composers\EmployeeProductComposer' => 'employees.products',
            'App\KegData\Composers\EmployeeTaxComposer' => 'employees.tax',
            'App\KegData\Composers\EmployeeUserComposer' => 'employees.user',
            //'App\KegData\Composers\EmployeeUserComposer' => 'employee.user',//new
            'App\KegData\Composers\EmployeeReportsComposer' => 'employees.reports',
            'App\KegData\Composers\ExcelReportComposer' => ['dashboard.residential.reportsexcel'],
            'App\KegData\Composers\ExcelReportComposerPour' => ['dashboard.residential.reportsexcelpour'],
        ]);
    }

    public function register(){
        
    }

}