<?php namespace App\KegData\providers;

use Illuminate\Support\ServiceProvider;
use App\KegData\Services\CustomValidator;
use Validator;

class ValidatorServiceProvider extends ServiceProvider{

	public function boot()
	{
	    Validator::resolver(function($translator, $data, $rules, $messages)
		{
		    return new CustomValidator($translator, $data, $rules, $messages);
		});
	}

	public function register(){

	}

}

?>