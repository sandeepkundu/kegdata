<?php namespace App\KegData\providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider{

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\KegData\Interfaces\BaseRepositoryInterface', 'App\KegData\Repositories\BaseEloquentRepository');
		$this->app->bind('App\KegData\Interfaces\AccountRepositoryInterface', 'App\KegData\Repositories\EloquentAccountRepository');
	}

}