<?php namespace App\KegData\providers;

use Illuminate\Support\ServiceProvider;

class InfrastructureServiceProvider extends ServiceProvider{

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\KegData\Infrastructure\GingerBreadInterface', 'App\KegData\Infrasturcture\GingerBread');
	}

	public function boot(){

	}

	public function provides(){
		return ['App\KegData\Infrastructure\GingerBread', 'App\KegData\Infrastructure\Utilities', 'App\KegData\Infrastructure\KegDataLogger'];
	}
} //end of class

//EOF