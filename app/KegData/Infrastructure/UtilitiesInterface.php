<?php namespace App\KegData\Infrastructure;

interface UtilitiesInterface{
/**
 * 
 * @return array of US States
 */
public function get_states();

/**
 *
 * @return array of countries
 */
public function get_countries();

/**
 *
 * @return array of currencies
 */
public function get_currencies();

/**
*
* @return array for select list with empty first option
*/ 
public function withEmpty($list);
}
