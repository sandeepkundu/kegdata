<?php namespace App\KegData\Infrastructure;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\LineFormatter;
use Request;
use Config;

/**
 * StoreLog
 *
 * Custom monolog logger for Store activity logging
 *
 * @author     Barna Szalai <sz.b@devartpro.com>
 */
class StoreLogger {

    /**
   * write
   * @return void
   */
  public function write($message)
  {
    // if feature is enabled..
    if (Config::get('app.storelog'))
    {
      // logger instance
      $log = new Logger('storelog');
      // handler init, making days separated logs
      $handler = new RotatingFileHandler(storage_path().'/logs/store.log', 0, Logger::INFO);
      // formatter, ordering log rows
      $handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message% %extra% %context%\n"));
      // add handler to the logger
      $log->pushHandler($handler);
      // processor, adding URI, IP address etc. to the log
      $log->pushProcessor(new WebProcessor);
      // processor, memory usage
      $log->pushProcessor(new MemoryUsageProcessor);

      $log->addInfo($message.' |');
    }
  }
}
