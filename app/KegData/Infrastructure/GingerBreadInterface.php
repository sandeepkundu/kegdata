<?php namespace App\KegData\Infrastructure;

interface GingerBreadInterface{

	/**
	*
	* Return the encrypted value
	*
	* @param string key
	* @param string value
	*
	* @return string 
	*/
	public function bake($key, $value);

	/**
	*
	* Return the encrypted value
	*
	* @param string value
	* @param string key
	*
	* @return string 
	*/
	public function eat($value, $key);

	/**
	*
	* Decrypt a collection based on an array defining the encrypted fields
	*
	* @param Collection $collection
	* @param Array $encryption
	*
	* @return Collection $collection
	*/

	public function decrypt($collection, $encryption);

	/**
	*
	* Preforms a decrypt based on the defined function of eat
	*
	* @param string value
	*
	* @return string 
	*/
	public function preformDecrypt($value);

	/**
	*
	* Encrypt a collection based on an array defining the encrypted fields
	*
	* @param Collection $collection
	* @param Array $encryption
	*
	* @return Collection $collection
	*/

	public function encrypt($collection, $encryption);

	/**
	*
	* Preforms an encrypt based on the defined function of bake
	*
	* @param string value
	*
	* @return string 
	*/

	public function preformEncrypt($value);


}