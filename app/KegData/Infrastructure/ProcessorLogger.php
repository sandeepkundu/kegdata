<?php namespace App\KegData\Infrastructure;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\LineFormatter;
use Request;
use Config;

/**
 * ProcessorLog
 *
 * Custom monolog logger for KegData Processing activity logging
 *
 * @author     Barna Szalai <sz.b@devartpro.com>
 */
class ProcessorLogger {

    /**
   * write
   * @return void
   */
  public function write($message)
  {
    // if feature is enabled..
    if (Config::get('app.processorlog'))
    {
      // logger instance
      $log = new Logger('processorlog');
      // handler init, making days separated logs
      $handler = new RotatingFileHandler(storage_path().'/logs/processor.log', 0, Logger::INFO);
      // formatter, ordering log rows
      $handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message% %extra% %context%\n"));
      // add handler to the logger
      $log->pushHandler($handler);
      // processor, adding URI, IP address etc. to the log
      $log->pushProcessor(new WebProcessor);
      // processor, memory usage
      $log->pushProcessor(new MemoryUsageProcessor);

      $log->addInfo($message.' |');
    }
  }
}
