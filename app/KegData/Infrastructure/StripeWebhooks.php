<?php

namespace App\KegData\Infrastructure;

use Illuminate\Support\Collection as Collection;
use Stripe;
use Log;

 class StripeWebhooks
 {
     protected $eventType;

     protected $eventTypesArray;

     protected $stripe_api_version;

     /**
      * Stripe Webhooks class.
      *
      * Logic for handling the various events that stripe produces
      */
     public function __construct(string $eventType)
     {
         $this->eventType = $eventType;
         $this->eventTypesCollection = $this->setEventTypesCollection();
     }

     public function getEvent(){
         dd($this->eventTypesCollection->where('type', '=', $this->eventType));
       return $this->eventTypesCollection->where('type', '=', $this->eventType)->all();
    }

    /**
     * @method setEventTypesCollection
     * Creates a collection with all the events types
     * @return Illuminate/Support/collection $stripeEventCollection
     */
    public function setEventTypesCollection():\Illuminate\Support\Collection
    {
         // $method = 'handle'.studly_case(str_replace('.', '_', $payload['type']));

       $et = explode('.', $this->eventType);

       $method = 'set'.studly_case($et[0]).'EventTypes';

       $stripeEventCollection = collect();


       if (method_exists($this, $method)) {
           $array = $this->{$method}();
           foreach($array as $i){
                $stripeEventCollection->push($i);
           }

       } else {
           $stripeEventCollection->push($this->missingMethod());
       }

         return $stripeEventCollection;
    }

    /**
     * @method missingMethod()
     *
     * This method is called when the method requested does not
     * exist in the class
     *
     * @return array
     */
    public function missingMethod():array{
        $missingMethodArray = [
            'type' => 'missing.method',
            'description' => 'A request to handle '.$this->eventType.' has been made but this applicatin does not recongize that event. Perhaps the stripe api has been updated?',
            'handle' => false
        ];

        return $missingMethodArray;
    } /*Close missing method */

     /**
      * @method setAccountEventTypes
      * Creates an array filled with the stripe account events
      *
      * @return array
      */
     public function setAccountEventTypes():array
     {
         $accountEventTypesArray = [
            ['type' => 'account.updated',
            'description' => 'Occurs whenever an account status or property has changed.',
            'handle' => false ],
            ['type' => 'account.application.deauthorized',
            'description' => 'Occurs whenever a user deauthorizes an application. Sent to the related application only.',
            'handle' => false ],
            ['type' => 'account.external_account.created',
            'description' => 'Occurs whenever an external account is created.',
            'handle' => false ],
            ['type' => 'account.external_account.deleted',
            'description' => 'Occurs whenever an external account is deleted.',
            'handle' => false ],
            ['type' => 'account.external_account.updated',
            'description' => 'Occurs whenever an external account is updated.',
            'handle' => false ]
         ];

         return $accountEventTypesArray;
     }

    /**
     * @method setApplicationEventTypes
     * Creates an array filled with stripe application events
     * @return array applicationEventArray
     */
     public function setApplicationFeeEventTypes():array{
        $applicationEventArray = [
            ['type' => 'application_fee.created',
            'description' => 'Occurs whenever an application fee is created on a charge.',
            'handle' => false, ],
            ['type' => 'application_fee.refunded',
            'description' => 'Occurs whenever an application fee is refunded, whether from refunding a charge or from refunding the application fee directly, including partial refunds.',
            'handle' => false, ],
            ['type' => 'application_fee.refund.updated',
            'description' => 'Occurs whenever an application fee is updated.',
            'handle' => false ]
        ];

        return $applicationEventArray;
     }

    /**
     * @method setBalanceEventTypes
     * Creates an array filled with stripe balance events
     * @return array balanceEventArray
     */
     public function setBalanceEventTypes():array{
        $balanceEventArray = [
            ['type' => 'balance.available',
            'description' => 'Occurs whenever your Stripe balance has been updated (e.g. when a charge collected is available to be pain out). By default, Stripe will automatically tranfer any funds in your balance to your bank account on a daily basis.',
            'handle' => false ]
        ];

        return $balanceEventArray;
     }


    /**
     * @method setBitcoinEventTypes
     * Creates an array filled with stripe bitcoin events
     * @return array bitCoinEventArray
     */
    public function setBitcoinEventTypes():array{
        $bitCoinEventArray = [
            ['type' => 'bitcoin.receiver.created',
            'description' => 'Occurs whenever a receiver has been created.',
            'handle' => false ],
            ['type' => 'bitcoin.receiver.filled',
            'description' => 'Occurs whenever a receiver is filled (that is, when it has received enough bitcoin to process a payment of the same amount).',
            'handle' => false ],
            ['type' => 'bitcoin.receiver.updated',
            'description' => 'Occurs whenever a receiver is updated.',
            'handle' => false ],
            ['type' => 'bitcoin.receiver.transaction.created',
            'description' => 'Occurs whenever bitcoin is pushed to a receiver.',
            'handle' => false ]
        ];

        return $bitCoinEventArray;
    }

    /**
     * @method setChargeEventTypes
     * creates an array filled with stripe charge events
     * @return array chargeEventArray
     */
     public function setChargeEventTypes():array{
         $chargeEventArray = [
             ['type' => 'charge.captured',
             'description' => 'Occurs whenever a previously uncaptured charge is captured.',
             'handle' => false ],
             ['type' => 'charge.failed',
             'description' => 'Occurs whenever a failed charge attempt occurs.',
             'handle' => false ],
             ['type' => 'charge.refunded',
             'description' => 'Occurs whenever a charge is refunded, including partial refunds.',
             'handle' => true ],
             ['type' => 'charge.succeeded',
             'description' => 'Occurs whenever a new charge is created and is successful.',
             'handle' => false ],
             ['type' => 'charge.updated',
             'description' => 'Occurs whenver a charge description or metadata is updated.',
             'handle' => false ],
             ['type' => 'charge.dispute.closed',
             'description' => 'Occurs when the dispute is closed and the dispute status changes to charge_refunded, lost, warning_closed,  or won.',
             'handle' => false ],
             ['type' => 'charge.dispute.created',
             'description' => 'Occurs whenver a customer disputes a charge with their bank (chargeback).',
             'handle' => false ],
             ['type' => 'charge.dispute.funds_reinstated',
             'description' => 'Occurs when funds are reinstated to your account after a dispute is won.',
             'handle' => false ],
             ['type' => 'charge.dispute.funds_withdrawn',
             'description' => 'Occurs when funds are removed from your account due to a dispute.',
             'handle' => false ],
             ['type' => 'charge.dispute.updated',
             'description' => 'Occurs when the dispute is updated (usually with evidence).',
             'handle' => false ]
         ];

         return $chargeEventArray;
     }

    /**
     * @method setCouponEventTypes
     * creates an array filled with stripe coupon events
     * @return array couponEventArray
     */
    public function setCouponEventTypes():array{
        $couponEventArray = [
            ['type' => 'coupon.created',
            'description' => 'Occurs whenever a coupon is created.',
            'handle' => false ],
            ['type' => 'coupon.deleted',
            'description' => 'Occurs whenever a coupon is deleted.',
            'handle' => false ],
            ['type' => 'charge.updated',
            'description' => 'Occurs whenever a coupon is updated.',
            'handle' => false ]
        ];

        return $couponEventArray;
    }

    /**
     * @method setCustomerEventTypes
     * creates an array filled with stripe customer events
     * @return array customerEventArray
     */
    public function setCustomerEventTypes():array{
        $customerEventArray = [
            ['type' => 'customer.created',
            'description' => 'Occurs whenever a new customer is created.',
            'handle' => false ],
            ['type' => 'customer.deleted',
            'description' => 'Occurs whenever a customer is deleted.',
            'handle' => false ],
            ['type' => 'customer.updated',
            'description' => 'Occurs whenever any property of a customer changes.',
            'handle' => false ],
            ['type' => 'customer.discount.created',
            'description' => 'Occurs whenever a coupon is attached to a customer.',
            'handle' => false ],
            ['type' => 'customer.discount.deleted',
            'description' => 'Occurs whenever a customer\'s discount is removed.',
            'handle' => false ],
            ['type' => 'customer.discount.updated',
            'description' => 'Occurs whenever a customer is switched from one coupon to another.',
            'handle' => false ],
            ['type' => 'customer.source.created',
            'description' => 'Occurs whenever a new source is created for the customer.',
            'handle' => false ],
            ['type' => 'customer.source.deleted',
            'description' => 'Occurs whenever a source is removed from a customer.',
            'handle' => false ],
            ['type' => 'customer.source.updated',
            'description' => 'Occurs whenever a source\'s details are changed.',
            'handle' => false ],
            ['type' => 'customer.subscription.created',
            'description' => 'Occurs whenever a customer with no subscription plan is signed up for a plan.',
            'handle' => false ],
            ['type' => 'customer.subscription.deleted',
            'description' => 'Occurs whenever a customer ends their subscription.',
            'handle' => True ],
            ['type' => 'customer.subscription.trial_will_end',
            'description' => 'Occurs three days before the trial period of a subscription is scheduled to end.',
            'handle' => false ],
            ['type' => 'customer.subscription.updated',
            'description' => 'Occurs whenever a subscription changes. Examples would include switching from one plan to another, or switch status from trial to active.',
            'handle' => false ]
        ];

        return $customerEventArray;
    }

    /**
     * @method setInvoiceEventTypes
     * creates an array filled with stripe invoice events
     * @return array invoiceEventArray
     */
    public function setInvoiceEventTypes():array{
        $invoiceEventArray = [
            ['type' => 'invoice.created',
            'description' => 'Occurs whenever a new invoice is created. If you are using webhooks, Stripe will wait one hour after they have all succeeded to attempt to pay the invoice; the only exception here is on the first invoice, which gets created and paid immediately when you subscribe a customer to a plan. If your webhooks do not all respond successfully, Stripe will continue retrying the webhooks every hour and will not attempt to pay the invoice. After 3 days, Stripe will attempt to pay the invoice regardless of whether or not your webhooks have succeeded.',
            'handle' => false ],
            ['type' => 'invoice.payment_failed',
            'description' => 'Occurs whenever an invoice attempts to be paid, and the payment fails. This can occur either due to a declined payment, or because the customer has no active card. A particular case of note is that if a customer with no active card reaches the end of its free trial, an invoice.payment_failed notification will occur.',
            'handle' => True ],
            ['type' => 'invoice.payment_succeeded',
            'description' => 'Occurs whenever an invoice attempts to be paid, and the payment succeeds.',
            'handle' => True ],
            ['type' => 'invoice.updated',
            'description' => 'Occurs whenever an invoice changed (for example, the amount could change)',
            'handle' => false ],

        ];

        return $invoiceEventArray;
    }

    /**
     * @method setInvoiceItemEventTypes
     * creates an array filled with stripe invoice events
     * @return array invoiceItemEventArray
     */
    public function setInvoiceItemEventTypes():array{
        $invoiceItemEventArray = [
            ['type' => 'invoiceitem.created',
            'description' => 'Occurs whenever an invoice changes (for example, the could change).',
            'handle' => false ],
            ['type' => 'invoiceitem.deleted',
            'description' => 'Occurs whenever an invoice item is deleted.',
            'handle' => false ],
            ['type' => 'invoiceitem.updated',
            'description' => 'Occurs whenever an invoice item is updated.',
            'handle' => false ]
        ];

        return $invoiceItemEventArray;
    }

    /**
     * @method setOrderEventTypes
     * create an array filled with stripe order events
     * @return array orderEventArray
     */
    public function setOrderEventTypes():array{
        $orderEventArray = [
            ['type' => 'order.created',
            'description' => 'Occurs whenver an order is created.',
            'handle' => false ],
            ['type' => 'order.payment_failed',
            'description' => 'Occurs whenever payment is attempted on an order, and the payment fails.',
            'handle' => false ],
            ['type' => 'order.payment_succeeded',
            'description' => 'Occurs whenever payment is attempted on an order, and the payment succeeds.',
            'handle' => false ],
            ['type' => 'order.updated',
            'description' => 'Occurs whenever an order is updated.',
            'handle' => false ],
            ['type' => 'order.return_created',
            'description' => 'Occurs whenever order return is created.',
            'handle' => false ],
        ];


        return $orderEventArray;
    }

    /**
     * @method setPlanEventTypes
     * create an array filled with stripe plan events
     * @return array planEventArray
     */
    public function setPlanEventTypes():array{
        $planEventArray = [
          ['type' => 'plan.created',
          'description' => 'Occurs whenver a plan is created.',
          'handle' => false ],
          ['type' => 'plan.deleted',
          'description' => 'Occurs whenver a plan is deleted.',
          'handle' => false ],
          ['type' => 'plan.updated',
          'description' => 'Occurs whenver a plan is updated.',
          'handle' => false ],
        ];

        return $planEventArray;
    }

    /**
     * @method setProductEventTypes
     * create an array filled with stripe product events
     * @return array productEventArray
     */
    public function setProductEventTypes():array{
        $productEventArray = [
          ['type' => 'product.created',
          'description' => 'Occurs whenver a product is created.',
          'handle' => false ],
          ['type' => 'plan.deleted',
          'description' => 'Occurs whenver a plan is deleted.',
          'handle' => false ],
          ['type' => 'plan.updated',
          'description' => 'Occurs whenver a plan is updated.',
          'handle' => false ],
        ];

        return $productEventArray;
    }

    /**
     * @method setRecipientEventTypes
     * creates an array filled with stripe recipient events
     * @return recipientEventArray
     */
    public function setRecipientEventTypes():array{
        $recipientEventArray = [
          ['type' => 'recipient.created',
          'description' => 'Occurs whenver a recipient is created.',
          'handle' => false ],
          ['type' => 'recipient.deleted',
          'description' => 'Occurs whenver a recipient is deleted.',
          'handle' => false ],
          ['type' => 'recipient.updated',
          'description' => 'Occurs whenver a recipient is updated.',
          'handle' => false ],        ];

        return $recipientEventArray;
    }

    /**
     * @method setSkuEventTypes
     * creates an array filled with sku events
     * @return skuEventArray
     */
    public function setSkuEventTypes():array{
        $skuEventArray = [
          ['type' => 'sku.created',
          'description' => 'Occurs whenver a SKU is created.',
          'handle' => false ],
          ['type' => 'sku.deleted',
          'description' => 'Occurs whenver a SKU is deleted.',
          'handle' => false ],
          ['type' => 'sku.updated',
          'description' => 'Occurs whenver a SKU is updated.',
          'handle' => false ],
        ];

        return $skuEventArray;
    }

    /**
     * @method setSourceEventTypes
     * creates an array filled with source events
     * @return sourceEventArray
     */
    public function setSourceEventTypes():array{
        $sourceEventArray = [
            ['type' => 'source.canceled',
             'description' => 'Occurs whenever a source is canceled.',
             'handle' => false
            ],
            ['type' => 'source.chargeable',
             'description' => 'Occurs whenever a source transitions to chargeable.',
             'handle' => false
           ],
             ['type' => 'source.failed',
              'description' => 'Occurs whenever a source is failed.',
              'handle' => false
          ],

        ];

        return $sourceEventArray;
    }

    /**
     * @method setTransferEventTypes
     * creates an array filled with transfer events
     * @return transferEventArray
     */
    public function setTransferEventTypes():array{
        $transferEventArray = [
            ['type' => 'transfer.created',
             'description' => 'Occurs whenever a new transfer is created.',
             'handle' => false
            ],
            ['type' => 'transfer.failed',
             'description' => 'Occurs whenever Stripe attempts to send a transfer and that transfer fails.',
             'handle' => false
            ],
            ['type' => 'transfer.paid',
             'description' => 'Occurs whenever a sent transfer is expected to be available in the destination bank account. If the transfer failed, a transfer.failed webhook will additionally be sent at a later time. Note to Connect users: this event is only created for transfers from your connected Stripe accounts to their bank accounts, not for transfers to the connected accounts themselves.',
             'handle' => false
            ],
            ['type' => 'transfer.reversed',
             'description' => 'Occurs whenever a transfer is reversed, including partial reversals.',
             'handle' => false
            ],
            ['type' => 'transfer.updated',
             'description' => 'Occurs whenever the description or metadata of a transfer is updated.',
             'handle' => false
            ],
        ];

        return $transferEventArray;
    }

    /**
     * @method setPingEventTypes
     * creates an array filled with ping events
     * @return pingEventArray
     */
     public function setPingEventTypes():array{
         $pingEventArray = [
            ['type' => 'ping',
             'description' => 'May be sent by Stripe at any time to see if a provided webhook URL is working.',
             'handle' => false
            ]
         ];

         return $pingEventArray;
     }

 } /* Close StripeWebhooks */
