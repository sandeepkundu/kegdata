<?php

namespace App\KegData\Infrastructure\Handlers;

use Illuminate\Support\Collection as Collection;
use App\KegData\Models\Order as Order;
use App\KegData\Models\Account as Account;
use App\KegData\Models\Subscription as Subscription;
use App\KegData\Models\Plan as Plan;
use Stripe;
use Mail;
use Carbon;
use Log;

/**
 * StipeWebhookHandlers class
 *
 * This class contains the logic for actually processing
 * the event we are handling
 */


class StripeWebhookHandlers
{
    protected $eventType;
    protected $payload;

    public function __construct($payload){
        $this->eventType = $payload['type'];
        $this->payload = $payload;

        $method = 'handle'.studly_case(str_replace('.', '_', $this->eventType));
        if (method_exists($this, $method)) {
            $this->{$method}($payload);
        } else {
            throw new Exception('No method to handle '.$this->eventType, 1);
        }
    }

    public function handleChargeRefunded(){
        $refund = $this->payload['data']['object'];

        $customer = Stripe::customers()->find($refund['customer']);
        $order_id = $refund['metadata']['order_id'];
        $order = Order::find($order_id)->with('account');

        if(empty($order)){
            $order = Order::where('charge_id', '=', $refund['id'])->get();
        }

        if(!empty($order)){
            $order->status = 'refunded';
            $order->save();
        }else{
            throw new Exception('Order '.$order_id.' with charge '.$refund['id'].' does not exist in the KegData Database');
        }

        try{

            $url = config('app.url');
            $company  = config('constants.COMPANY_NAME');
            $support = config('constants.SUPPORT_EMAIL');
            $message = <<<"EOT"
            <p>The order placed with <a href="$url">$company</a> on $order->created_at->toFormattedDateString() was refunded on $order->updated_at->toFormattedDateString().</p>
            <p>We are sorry that the order did not work out. Please feel free to contact us by emailing <a href="mailto:$support">$support</a> if there is anything we can do to assist you or if this refunded made in error.<p>
            <p>This refund does not affect any subscription you have with KegData. You can view these details under the KegData Dashboard</p>
EOT;
            $this->sendOrderMail($order, $message, $customer);
            return true;
        }catch( \Exception $e){
            Log::error($e);
            return false;
        }

    }/* Close handle Charge refunded */

    public function handleCustomerSubscriptionDeleted(){
        $data = $this->payload['data'];
        $subscription_end = Carbon::createFromTimestamp($this->payload['ended_at']);

        $account = Account::where('stripe_customer_id', '=', $this->payload['customer'])->with('plan')->first();

        $account->stripe_active = 0;
        $account->subscription_ends_at = $subscription_end->toDateTimeString();

        $account->save();

        $customer = Stripe::customers()->find($this->payload['customer']);


        $company  = config('constants.COMPANY_NAME');
        $support = config('constants.SUPPORT_EMAIL');
        $message = <<<"EOT"
        <p>We are sorry to inform you that your subscription, $account->plan->description, with $company will be canceled on $subscription_end->toFormattedDateString() . If this was made in error or you would like to resubscribe, please visit <a href="http://kegdata.com/store">$company Store</a> and resubscribe.</p>
        <p>You will not be able to see the data for your kegs until you resubscribe.</p>
EOT;

        $this->sendSubscritionMail($account, $message, $customer);

    }

    public function handleInvoicePaymentSucceeded(){
        $data = $this->payload['data'];

        $account = Account::where('stripe_customer_id', '=', $this->payload['customer'])->with('plan')->first();

        $customer = Stripe::customers()->find($this->payload['customer']);

        $date = Carbon::createFromTimestamp($this->payload['date']);

        $company  = config('constants.COMPANY_NAME');
        $support = config('constants.SUPPORT_EMAIL');
        $amount = number_format($this->payload['amount']/1);
        $message = <<<"EOT"
        <p>We are writing to let you know that your payment for $account->plan->description in the amount of $amount was successfully paid on $date->toFormattedDateString()</p>
        <p>Thank you!</p>
EOT;

        $this->sendSubscritionMail($account, $message, $customer);

    }

    public function handleInvoicePaymentFailed(){
        $data = $this->payload['data'];

        $data = $this->payload['data'];

        $account = Account::where('stripe_customer_id', '=', $this->payload['customer'])->with('plan')->first();

        $customer = Stripe::customers()->find($this->payload['customer']);

        $date = Carbon::createFromTimestamp($this->payload['date']);
        $attempt = $this->payload['attempt_count'];
        $next_attempt = Carbon::createFromTimestamp($this->payload['next_payment_attempt']);

        $next_atttempt_in = Carbon::now()->diffForHumans($next_attempt);
        $attempts_left = 3 - $attempt;

        switch($attempts_left){
            case 1:{
                $time = '1 more time';
                break;
            }case 2:{
                $time = '2 more times';
            }
        }

        $company  = config('constants.COMPANY_NAME');
        $support = config('constants.SUPPORT_EMAIL');
        $amount = number_format($this->payload['amount']/1);
        $message = <<<"EOT"
        <p>We are writing to let you know that your payment for $account->plan->description in the amount of $amount was failed on $date->toFormattedDateString()</p>
        <p>You can check your credit card details and update your subscription details through the $company Dashboard</p>
        <p>We will try to charge this card $time with the next attempt in $next_atttempt_in.</p>
        <p>If your payment continues to fail, your subscription will be canceled and you will not be able to access the $company Dashboards.<p>
        <p>Thank you!</p>
EOT;

        $this->sendSubscritionMail($account, $message, $customer);

    }

    public function sendOrderMail($order, $message, $customer){
        $emailData['customMessage'] = $message;
        $emailData['order'] = $order;
        $emailData['name'] = $customer['description'];


        Mail::send('emails.orderNotification', $emailData, function($m) use($customer){
            $m->from('store@kegdata.com', 'KegData Store');
            $m->to($customer['email'], $customer['description'])->subject('KegData Order Status');

        });
    }

    public function sendSubscriptionMail($account, $message, $customer){
        $emailData['customMessage'] = $message;
        $emailData['account'] = $account;
        $emailData['name'] = $customer['description'];


        Mail::send('emails.subscriptionNotification', $emailData, function($m) use($customer){
            $m->from('store@kegdata.com', 'KegData Store');
            $m->to($customer['email'], $customer['description'])->subject('KegData Subscription Status');

        });
    }


} /* Close StripeWebhookHandlers */
