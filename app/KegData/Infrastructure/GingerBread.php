<?php namespace App\Kegdata\Infrastructure;

use Illuminate\Database\Eloquent\Collection;
use Config;
use App\KegData\Infrastructure\GingerBreadInterface as GingerBreadInterface;

class GingerBread implements GingerBreadInterface{

	protected $honeyandthebee;

	public function __construct(){
		$this->honeyandthebee = Config::get('constants.KEY');
	}

	public function bake($honey,$bee){
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($honey), $bee, MCRYPT_MODE_CBC, md5(md5($honey))));
	}

	public function eat($cake,$icing){
		return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($icing), base64_decode($cake), MCRYPT_MODE_CBC, md5(md5($icing))), "\0");
	}


	public function decrypt($collection, $encryption){
		
		$new = $collection->map(function($row) use ($encryption){
			foreach($encryption as $value){
				$row[$value] = $this->preformDecrypt($row[$value]);
			}
			return $row;			
		});
		return $new;
	}

	public function preformDecrypt($value){
		return empty($value) ? '' : $this->eat($value, $this->honeyandthebee);
	}

	public function encrypt($collection, $encryption){
		$new = $collection->map(function($row) use ($encryption){
			foreach($encryption as $value){
				$row[$value] = $this->preformEncrypt($row[$value]);
			}
			return $row;
		});

		return $new;
	}

	public function preformEncrypt($value){
		return empty($value) ? '' : $this->bake($this->honeyandthebee, $value);
	}

}/*Close class gingerbread*/

?>