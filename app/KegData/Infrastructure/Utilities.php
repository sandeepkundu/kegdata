<?php namespace App\KegData\Infrastructure;

use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\KegType as KegType;
use Illuminate\Contracts\Auth\Guard;
use Auth;
class Utilities implements UtilitiesInterface{
/**
 *
 * @return array of US States
 */
public function get_states(){
	$us_state_abbrevs_names = array(
		'AL'=>'ALABAMA',
		'AK'=>'ALASKA',
		'AZ'=>'ARIZONA',
		'AR'=>'ARKANSAS',
		'CA'=>'CALIFORNIA',
		'CO'=>'COLORADO',
		'CT'=>'CONNECTICUT',
		'DE'=>'DELAWARE',
		'DC'=>'DISTRICT OF COLUMBIA',
		'FL'=>'FLORIDA',
		'GA'=>'GEORGIA',
		'HI'=>'HAWAII',
		'ID'=>'IDAHO',
		'IL'=>'ILLINOIS',
		'IN'=>'INDIANA',
		'IA'=>'IOWA',
		'KS'=>'KANSAS',
		'KY'=>'KENTUCKY',
		'LA'=>'LOUISIANA',
		'ME'=>'MAINE',
		'MD'=>'MARYLAND',
		'MA'=>'MASSACHUSETTS',
		'MI'=>'MICHIGAN',
		'MN'=>'MINNESOTA',
		'MS'=>'MISSISSIPPI',
		'MO'=>'MISSOURI',
		'MT'=>'MONTANA',
		'NE'=>'NEBRASKA',
		'NV'=>'NEVADA',
		'NH'=>'NEW HAMPSHIRE',
		'NJ'=>'NEW JERSEY',
		'NM'=>'NEW MEXICO',
		'NY'=>'NEW YORK',
		'NC'=>'NORTH CAROLINA',
		'ND'=>'NORTH DAKOTA',
		'OH'=>'OHIO',
		'OK'=>'OKLAHOMA',
		'OR'=>'OREGON',
		'PA'=>'PENNSYLVANIA',
		'RI'=>'RHODE ISLAND',
		'SC'=>'SOUTH CAROLINA',
		'SD'=>'SOUTH DAKOTA',
		'TN'=>'TENNESSEE',
		'TX'=>'TEXAS',
		'UT'=>'UTAH',
		'VT'=>'VERMONT',
		'VA'=>'VIRGINIA',
		'WA'=>'WASHINGTON',
		'WV'=>'WEST VIRGINIA',
		'WI'=>'WISCONSIN',
		'WY'=>'WYOMING'
		);

return $us_state_abbrevs_names;
}

/**
 *
 * @return array of countries
 */
public function get_countries(){
	$countries = array(
		'AD' => 'Andorra',
		'AE' => 'United Arab Emirates',
		'AF' => 'Afghanistan',
		'AG' => 'Antigua &amp; Barbuda',
		'AI' => 'Anguilla',
		'AL' => 'Albania',
		'AM' => 'Armenia',
		'AN' => 'Netherlands Antilles',
		'AO' => 'Angola',
		'AQ' => 'Antarctica',
		'AR' => 'Argentina',
		'AS' => 'American Samoa',
		'AT' => 'Austria',
		'AU' => 'Australia',
		'AW' => 'Aruba',
		'AZ' => 'Azerbaijan',
		'BA' => 'Bosnia and Herzegovina',
		'BB' => 'Barbados',
		'BD' => 'Bangladesh',
		'BE' => 'Belgium',
		'BF' => 'Burkina Faso',
		'BG' => 'Bulgaria',
		'BH' => 'Bahrain',
		'BI' => 'Burundi',
		'BJ' => 'Benin',
		'BM' => 'Bermuda',
		'BN' => 'Brunei Darussalam',
		'BO' => 'Bolivia',
		'BR' => 'Brazil',
		'BS' => 'Bahama',
		'BT' => 'Bhutan',
		'BU' => 'Burma (no longer exists)',
		'BV' => 'Bouvet Island',
		'BW' => 'Botswana',
		'BY' => 'Belarus',
		'BZ' => 'Belize',
		'CA' => 'Canada',
		'CC' => 'Cocos (Keeling) Islands',
		'CF' => 'Central African Republic',
		'CG' => 'Congo',
		'CH' => 'Switzerland',
		'CI' => 'Côte D\'ivoire (Ivory Coast)',
		'CK' => 'Cook Iislands',
		'CL' => 'Chile',
		'CM' => 'Cameroon',
		'CN' => 'China',
		'CO' => 'Colombia',
		'CR' => 'Costa Rica',
		'CS' => 'Czechoslovakia (no longer exists)',
		'CU' => 'Cuba',
		'CV' => 'Cape Verde',
		'CX' => 'Christmas Island',
		'CY' => 'Cyprus',
		'CZ' => 'Czech Republic',
		'DD' => 'German Democratic Republic (no longer exists)',
		'DE' => 'Germany',
		'DJ' => 'Djibouti',
		'DK' => 'Denmark',
		'DM' => 'Dominica',
		'DO' => 'Dominican Republic',
		'DZ' => 'Algeria',
		'EC' => 'Ecuador',
		'EE' => 'Estonia',
		'EG' => 'Egypt',
		'EH' => 'Western Sahara',
		'ER' => 'Eritrea',
		'ES' => 'Spain',
		'ET' => 'Ethiopia',
		'FI' => 'Finland',
		'FJ' => 'Fiji',
		'FK' => 'Falkland Islands (Malvinas)',
		'FM' => 'Micronesia',
		'FO' => 'Faroe Islands',
		'FR' => 'France',
		'FX' => 'France, Metropolitan',
		'GA' => 'Gabon',
		'GB' => 'United Kingdom (Great Britain)',
		'GD' => 'Grenada',
		'GE' => 'Georgia',
		'GF' => 'French Guiana',
		'GH' => 'Ghana',
		'GI' => 'Gibraltar',
		'GL' => 'Greenland',
		'GM' => 'Gambia',
		'GN' => 'Guinea',
		'GP' => 'Guadeloupe',
		'GQ' => 'Equatorial Guinea',
		'GR' => 'Greece',
		'GS' => 'South Georgia and the South Sandwich Islands',
		'GT' => 'Guatemala',
		'GU' => 'Guam',
		'GW' => 'Guinea-Bissau',
		'GY' => 'Guyana',
		'HK' => 'Hong Kong',
		'HM' => 'Heard &amp; McDonald Islands',
		'HN' => 'Honduras',
		'HR' => 'Croatia',
		'HT' => 'Haiti',
		'HU' => 'Hungary',
		'ID' => 'Indonesia',
		'IE' => 'Ireland',
		'IL' => 'Israel',
		'IN' => 'India',
		'IO' => 'British Indian Ocean Territory',
		'IQ' => 'Iraq',
		'IR' => 'Islamic Republic of Iran',
		'IS' => 'Iceland',
		'IT' => 'Italy',
		'JM' => 'Jamaica',
		'JO' => 'Jordan',
		'JP' => 'Japan',
		'KE' => 'Kenya',
		'KG' => 'Kyrgyzstan',
		'KH' => 'Cambodia',
		'KI' => 'Kiribati',
		'KM' => 'Comoros',
		'KN' => 'St. Kitts and Nevis',
		'KP' => 'Korea, Democratic People\'s Republic of',
		'KR' => 'Korea, Republic of',
		'KW' => 'Kuwait',
		'KY' => 'Cayman Islands',
		'KZ' => 'Kazakhstan',
		'LA' => 'Lao People\'s Democratic Republic',
		'LB' => 'Lebanon',
		'LC' => 'Saint Lucia',
		'LI' => 'Liechtenstein',
		'LK' => 'Sri Lanka',
		'LR' => 'Liberia',
		'LS' => 'Lesotho',
		'LT' => 'Lithuania',
		'LU' => 'Luxembourg',
		'LV' => 'Latvia',
		'LY' => 'Libyan Arab Jamahiriya',
		'MA' => 'Morocco',
		'MC' => 'Monaco',
		'MD' => 'Moldova, Republic of',
		'MG' => 'Madagascar',
		'MH' => 'Marshall Islands',
		'ML' => 'Mali',
		'MN' => 'Mongolia',
		'MM' => 'Myanmar',
		'MO' => 'Macau',
		'MP' => 'Northern Mariana Islands',
		'MQ' => 'Martinique',
		'MR' => 'Mauritania',
		'MS' => 'Monserrat',
		'MT' => 'Malta',
		'MU' => 'Mauritius',
		'MV' => 'Maldives',
		'MW' => 'Malawi',
		'MX' => 'Mexico',
		'MY' => 'Malaysia',
		'MZ' => 'Mozambique',
		'NA' => 'Namibia',
		'NC' => 'New Caledonia',
		'NE' => 'Niger',
		'NF' => 'Norfolk Island',
		'NG' => 'Nigeria',
		'NI' => 'Nicaragua',
		'NL' => 'Netherlands',
		'NO' => 'Norway',
		'NP' => 'Nepal',
		'NR' => 'Nauru',
		'NT' => 'Neutral Zone (no longer exists)',
		'NU' => 'Niue',
		'NZ' => 'New Zealand',
		'OM' => 'Oman',
		'PA' => 'Panama',
		'PE' => 'Peru',
		'PF' => 'French Polynesia',
		'PG' => 'Papua New Guinea',
		'PH' => 'Philippines',
		'PK' => 'Pakistan',
		'PL' => 'Poland',
		'PM' => 'St. Pierre &amp; Miquelon',
		'PN' => 'Pitcairn',
		'PR' => 'Puerto Rico',
		'PT' => 'Portugal',
		'PW' => 'Palau',
		'PY' => 'Paraguay',
		'QA' => 'Qatar',
		'RE' => 'Réunion',
		'RO' => 'Romania',
		'RU' => 'Russian Federation',
		'RW' => 'Rwanda',
		'SA' => 'Saudi Arabia',
		'SB' => 'Solomon Islands',
		'SC' => 'Seychelles',
		'SD' => 'Sudan',
		'SE' => 'Sweden',
		'SG' => 'Singapore',
		'SH' => 'St. Helena',
		'SI' => 'Slovenia',
		'SJ' => 'Svalbard &amp; Jan Mayen Islands',
		'SK' => 'Slovakia',
		'SL' => 'Sierra Leone',
		'SM' => 'San Marino',
		'SN' => 'Senegal',
		'SO' => 'Somalia',
		'SR' => 'Suriname',
		'ST' => 'Sao Tome &amp; Principe',
		'SU' => 'Union of Soviet Socialist Republics (no longer exists)',
		'SV' => 'El Salvador',
		'SY' => 'Syrian Arab Republic',
		'SZ' => 'Swaziland',
		'TC' => 'Turks &amp; Caicos Islands',
		'TD' => 'Chad',
		'TF' => 'French Southern Territories',
		'TG' => 'Togo',
		'TH' => 'Thailand',
		'TJ' => 'Tajikistan',
		'TK' => 'Tokelau',
		'TM' => 'Turkmenistan',
		'TN' => 'Tunisia',
		'TO' => 'Tonga',
		'TP' => 'East Timor',
		'TR' => 'Turkey',
		'TT' => 'Trinidad &amp; Tobago',
		'TV' => 'Tuvalu',
		'TW' => 'Taiwan, Province of China',
		'TZ' => 'Tanzania, United Republic of',
		'UA' => 'Ukraine',
		'UG' => 'Uganda',
		'UM' => 'United States Minor Outlying Islands',
		'US' => 'United States of America',
		'UY' => 'Uruguay',
		'UZ' => 'Uzbekistan',
		'VA' => 'Vatican City State (Holy See)',
		'VC' => 'St. Vincent &amp; the Grenadines',
		'VE' => 'Venezuela',
		'VG' => 'British Virgin Islands',
		'VI' => 'United States Virgin Islands',
		'VN' => 'Viet Nam',
		'VU' => 'Vanuatu',
		'WF' => 'Wallis &amp; Futuna Islands',
		'WS' => 'Samoa',
		'YD' => 'Democratic Yemen (no longer exists)',
		'YE' => 'Yemen',
		'YT' => 'Mayotte',
		'YU' => 'Yugoslavia',
		'ZA' => 'South Africa',
		'ZM' => 'Zambia',
		'ZR' => 'Zaire',
		'ZW' => 'Zimbabwe'
		);
return $countries;
}

/**
 *
 * @return array of currencies
 */
public function get_currencies(){
	$currencies = array (
		'ALL' => 'Albania Lek',
		'AFN' => 'Afghanistan Afghani',
		'ARS' => 'Argentina Peso',
		'AWG' => 'Aruba Guilder',
		'AUD' => 'Australia Dollar',
		'AZN' => 'Azerbaijan New Manat',
		'BSD' => 'Bahamas Dollar',
		'BBD' => 'Barbados Dollar',
		'BDT' => 'Bangladeshi taka',
		'BYR' => 'Belarus Ruble',
		'BZD' => 'Belize Dollar',
		'BMD' => 'Bermuda Dollar',
		'BOB' => 'Bolivia Boliviano',
		'BAM' => 'Bosnia and Herzegovina Convertible Marka',
		'BWP' => 'Botswana Pula',
		'BGN' => 'Bulgaria Lev',
		'BRL' => 'Brazil Real',
		'BND' => 'Brunei Darussalam Dollar',
		'KHR' => 'Cambodia Riel',
		'CAD' => 'Canada Dollar',
		'KYD' => 'Cayman Islands Dollar',
		'CLP' => 'Chile Peso',
		'CNY' => 'China Yuan Renminbi',
		'COP' => 'Colombia Peso',
		'CRC' => 'Costa Rica Colon',
		'HRK' => 'Croatia Kuna',
		'CUP' => 'Cuba Peso',
		'CZK' => 'Czech Republic Koruna',
		'DKK' => 'Denmark Krone',
		'DOP' => 'Dominican Republic Peso',
		'XCD' => 'East Caribbean Dollar',
		'EGP' => 'Egypt Pound',
		'SVC' => 'El Salvador Colon',
		'EEK' => 'Estonia Kroon',
		'EUR' => 'Euro Member Countries',
		'FKP' => 'Falkland Islands (Malvinas) Pound',
		'FJD' => 'Fiji Dollar',
		'GHC' => 'Ghana Cedis',
		'GIP' => 'Gibraltar Pound',
		'GTQ' => 'Guatemala Quetzal',
		'GGP' => 'Guernsey Pound',
		'GYD' => 'Guyana Dollar',
		'HNL' => 'Honduras Lempira',
		'HKD' => 'Hong Kong Dollar',
		'HUF' => 'Hungary Forint',
		'ISK' => 'Iceland Krona',
		'INR' => 'India Rupee',
		'IDR' => 'Indonesia Rupiah',
		'IRR' => 'Iran Rial',
		'IMP' => 'Isle of Man Pound',
		'ILS' => 'Israel Shekel',
		'JMD' => 'Jamaica Dollar',
		'JPY' => 'Japan Yen',
		'JEP' => 'Jersey Pound',
		'KZT' => 'Kazakhstan Tenge',
		'KPW' => 'Korea (North) Won',
		'KRW' => 'Korea (South) Won',
		'KGS' => 'Kyrgyzstan Som',
		'LAK' => 'Laos Kip',
		'LVL' => 'Latvia Lat',
		'LBP' => 'Lebanon Pound',
		'LRD' => 'Liberia Dollar',
		'LTL' => 'Lithuania Litas',
		'MKD' => 'Macedonia Denar',
		'MYR' => 'Malaysia Ringgit',
		'MUR' => 'Mauritius Rupee',
		'MXN' => 'Mexico Peso',
		'MNT' => 'Mongolia Tughrik',
		'MZN' => 'Mozambique Metical',
		'NAD' => 'Namibia Dollar',
		'NPR' => 'Nepal Rupee',
		'ANG' => 'Netherlands Antilles Guilder',
		'NZD' => 'New Zealand Dollar',
		'NIO' => 'Nicaragua Cordoba',
		'NGN' => 'Nigeria Naira',
		'NOK' => 'Norway Krone',
		'OMR' => 'Oman Rial',
		'PKR' => 'Pakistan Rupee',
		'PAB' => 'Panama Balboa',
		'PYG' => 'Paraguay Guarani',
		'PEN' => 'Peru Nuevo Sol',
		'PHP' => 'Philippines Peso',
		'PLN' => 'Poland Zloty',
		'QAR' => 'Qatar Riyal',
		'RON' => 'Romania New Leu',
		'RUB' => 'Russia Ruble',
		'SHP' => 'Saint Helena Pound',
		'SAR' => 'Saudi Arabia Riyal',
		'RSD' => 'Serbia Dinar',
		'SCR' => 'Seychelles Rupee',
		'SGD' => 'Singapore Dollar',
		'SBD' => 'Solomon Islands Dollar',
		'SOS' => 'Somalia Shilling',
		'ZAR' => 'South Africa Rand',
		'LKR' => 'Sri Lanka Rupee',
		'SEK' => 'Sweden Krona',
		'CHF' => 'Switzerland Franc',
		'SRD' => 'Suriname Dollar',
		'SYP' => 'Syria Pound',
		'TWD' => 'Taiwan New Dollar',
		'THB' => 'Thailand Baht',
		'TTD' => 'Trinidad and Tobago Dollar',
		'TRY' => 'Turkey Lira',
		'TRL' => 'Turkey Lira',
		'TVD' => 'Tuvalu Dollar',
		'UAH' => 'Ukraine Hryvna',
		'GBP' => 'United Kingdom Pound',
		'USD' => 'United States Dollar',
		'UYU' => 'Uruguay Peso',
		'UZS' => 'Uzbekistan Som',
		'VEF' => 'Venezuela Bolivar',
		'VND' => 'Viet Nam Dong',
		'YER' => 'Yemen Rial',
		'ZWD' => 'Zimbabwe Dollar'
		);

return $currencies;
}

/**
*
* @return select list with empty first option
*/
public function withEmpty($list, $emptyLabel = ''){
	
	return  array('0' => $emptyLabel) + $list;

}

public function reportDates(){
	return array(
		'today' => 'Today',
		'thisweek' => 'Past 07 Days',
		'thisweektodate' => 'This Week-To-Date',
		'thismonth' => 'Past 30 Days',
		'thismonthtodate' => 'This Month-To-Date',
		'thisyear' => 'Past 365 Days',
		'thisyeartodate' => 'This Year-To-Date',
		'yesterday' => 'Yesterday',
		'lastweek' => 'Last Week-To-Date',
		'lastmonth' => 'Last Month',
		'lastmonthtodate' => 'Last Month-To-Date',
		'lastyear' => 'Last Year',
		'lastyeartodate' => 'Last Year-To-Date',
		'custom' => 'custom'
		);
}

/*convert amount type
 * returns amount type value according to ml or oz basis
 */
function get_volume_value($volume_type){
	
	$volume_value = 1984;
	switch ($volume_type) {
		case "oz":
		$volume_value = 1984;
		break;
		case "ml":
		$volume_value = 58674;
		break;
		default:
		$volume_value = 1984;
	}

	return $volume_value;
}


	/*
		Function for chage date from UTC time zone to America/Chicago
		@return date in America timeZone 

	*/
	function changeTimeZone($date){

		$accountSettings = Auth::user()->account->accountSetting->first();
		$this->timezone = new Timezone();
		$timezone = ($accountSettings->timezone == 0 ) ? $this->timezone->where('timezone', '=', 'UTC')->first() : $this->timezone->find($accountSettings->timezone);
		$conDate = $date->timezone($timezone->timezone);
		return $conDate;
	}

	/*function return keg type row on the basis of params
	**$keg_type_id (1/2 half or 1/4 full),$measurement_type (ml or oz)
	*/
	function get_keg_type_as_per_measurement($keg_type_id =null,$measurement_type=null) {
		$this->kegtype = new KegType();
		//first find out keg result on the basis of id
		$keg_type_result=$this->kegtype->where('id', '=', $keg_type_id)->first();
		if($keg_type_result) {
			$keg_type_name=$keg_type_result->kegType;
			//first find out keg result on the basis of id
			$keg_result = $this->kegtype->where('KegType', '=', $keg_type_name)->where('kegTypeMeasurement', '=', $measurement_type)->first();
			return $keg_result;

		} else {
			return null;
		}


	}

}
