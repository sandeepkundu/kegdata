<?php

namespace app\KegData\Infrastructure;

use App\KegData\Repositories\EloquentKegDeviceRepository as KegDevice;
use App\KegData\Repositories\EloquentKegDataTransmitRepository as KegData_Transmit;
use App\KegData\Repositories\EloquentKegDataHeartbeatRepository as KegData_Heartbeat;
use App\KegData\Repositories\EloquentKegDataProcessedRepository as KegData_Processed;
use App\KegData\Models\HubDevice AS HubDevice;
use Carbon\Carbon as Carbon;

/**
 * Class kegdataprocessor
 * Collection of methods for processing, formatted and mutating the data
 */
class KegDataProcessor
{
    protected $transmit;
    protected $device;

    public function __construct()
    {
        $this->transmit = new KegData_Transmit();
        $this->device = new KegDevice();
        $this->processed = new KegData_Processed();
        $this->heartbeat = new KegData_Heartbeat();
    }


    /**
     * @method convertOunces
     * returns converted ounces value
     *
     * @var Double $ounces
     * @var String $volumeType
     * @var INT $capacity
     *
     * @return Double $convertedOunces
     **/

    public function convertOunces(float $ounces, string $volume, int $capacity): float{

        // Lets make sure that the ounces are within the capacity of the keg
        if ($ounces > $capacity) {
            $ounces = $capacity;
        }

        if ($ounces < 0){
            $ounces = 0;
        }

        // Now lets change it if its not suppose to be ounces
        switch ($volume) {
            case 'g':
                $ounces = round($ounces * .0078125, 2, PHP_ROUND_HALF_DOWN);
            break;
            case 'l':
                $ounces = round($ounces * .0295735, 2, PHP_ROUND_HALF_DOWN);
            break;
            case 'ml':
                $ounces = round($ounces * 29.5735, 2, PHP_ROUND_HALF_DOWN);
            break;
        }


        // Return the new value
        return $ounces;
    }
    /**
     * @method countUnprocessedData
     * returns a count of unprocessed data...
     * @return int
     */
    public function countUnprocessedData() : int{
        return $this->transmit->whereNull('processed_at')->count();
    }

     /**
      * @method combinePours
      * Combine data line & last pours
      *
      * @var collection
      * @var collection lastPours
      */
     public function combinePours($dataLine, $lastPours)
     {
        $pourData = collect([]);

        $lastPours->each(function ($pour) use ($pourData) {
            $pour->name = $pour->kegdevice->deviceName;
            $pour->date = $pour->sent_at;
            $pour->length = $pour->pour_length;

            $pour->volume = $this->getVolume($pour, $pour->kegdevice->voltageOffset);

            $pourData->push(['name' => $pour->name,
                        'date' => $pour->date,
                        'length' => $pour->length,
                        'volume' => $pour->volume ]);
         }); /* Close lastPours->each() */

         $pourData->push(['name' => $dataLine->name,
                          'date' => $dataLine->date,
                          'length' => $dataLine->length,
                          'volume' => $dataLine->volume]);

         return $pourData;
     }

     /**
      * Get the formmatted mac address (only the last 6 numbers).
      *
      * @var string mac address
      *
      * @return string
      */
     public function getFormattedMac(string $mac):string
     {
         $formatted = substr($mac, 6, 2);
         $formatted .= ':';
         $formatted .= substr($mac, 8, 2);
         $formatted .= ':';
         $formatted .= substr($mac, 10, 2);

         return $formatted;
     }

    /**
     * getSlope
     *
     * @var Array $x -> array of x values for slope calculation
     * @var Array $y -> array of y values for slope calculation
     *
     * $x = sigma pour_length values
     * $y = raw calculated volume
     *
     * @return float $slope;
     */
    public function getSlope(array $x,array $y): float
    {
        // $x = cumulative pour time
        // $y = $volume
        // n = count($data)
        // a = n * ((x 1* y1) + (x2 * y2))
        // b = (x1 + x2) * (y1 + y2)
        // c = n * (x1^2 + x2^2)
        // d = (x1 + x2)^2
        // m (slope) = (a - b)/(c - d)
        try {
            $slope = 0;

            // x & y should be equal length
            if(count($x) != count($y)){
                throw new \InvalidArgumentException("X array should be equal length to Y array");

            }

            $n = count($x);
            $xTotal = 0;

            /**
             * Lets find $a
             */
            $sum = 0;
            for($i = 0; $i < $n; $i++){
                $sum += $x[$i] * $y[$i];
            }

            $a = $n * $sum;

            /**
             * Lets find $b
             */
            $sumX = array_sum($x);
            $sumY = array_sum($y);
            $b = $sumX * $sumY;

            /**
             * Lets find $c
             */
            $sum = 0;
            foreach($x as $i){
                $sum += $i**2;
            }
            $c = $n * $sum;


            /**
             * Lets find $d
             */
            $d = (array_sum($x))**2;

            /**
             * Lets get the slope now
             */
            $dividing = $a - $b;
            $dividingBy = $c - $d;

            if($dividingBy == 0){
                return 0;
            }else{
                $slope = ($a - $b) / ($c - $d);
            }

            return $slope;
        } catch (Exception $e) {
            return 0;
        }
    } /*close getSlope()*/

    /**
     * Get the converted status.
     *
     * @var int status
     *
     * @return string
     */
    public function getStatus($status)
    {
        /*
        (Bit 0 is least significant bit).
        Bit 0: Set if pressure sensor is installed.
        Bit 1: Set if reporting pour event.
        Bit 2: Set if keg is untapped.
        Bit 3: Tags it as heart beat packet containing latest temperature reading.
        Bit 4: Blow event.
        */
        $statusMessage = array(
          'sensorOK' => 0, 'untapped' => 0,
          'pour' => 0, 'heartbeat' => 0, 'blow' => 0);

        $status = str_pad($status, 5, '0', STR_PAD_LEFT);
        $statusArray = str_split($status);

        if ($statusArray[4] == 1) {
            $statusMessage['sensorOK'] = Config::get('constants.SENSOR');
        }

        if ($statusArray[3] == 1) {
            $statusMessage['pour'] = Config::get('constants.POUR_EVENT');
        }

        if ($statusArray[2] == 1) {
            $statusMessage['untapped'] = Config::get('constants.UNTAP');
        }

        if ($statusArray[1] == 1) {
            $statusMessage['heartbeat'] = Config::get('constants.HEARTBEAT');
        }

        if($statusArray[0] == 1){
            $statusMessage['blow'] = Config::get('constants.BLOW');
        }

        return $statusMessage;
    }

    /**
     * Get the converted temperature.
     *
     * @var int
     * @var string $tempType (C OR F)
     *
     * @return float
     */
    public function getTemp($rawValue, $tempType, $tempOffset)
    {
        if ($tempType == 'C') {
            return $this->getTempCelcius($rawValue, $tempOffset);
        } else {
            return $this->getTempFahrenheit($rawValue, $tempOffset);
        }
    }

    /**
     * Get the temperature from the analog value in celcius.
     *
     * @var int temp
     *
     * @return float $temp
     */
    public function getTempCelcius($rawValue, $tempOffset)
    {
        $temp = ($rawValue) ? round((((.659 - ((5 - ($rawValue * .001221)) / 4)) / .00132) - 40), 1, PHP_ROUND_HALF_DOWN) : 'N/A';

        return $temp + $tempOffset;
    }

    /**
     * Get the Temperature from the Analog Value and Convert to Fahrenheit.
     *
     * @var int temp
     *
     * @return float $temp
     */
    public function getTempFahrenheit($rawValue, $tempOffset)
    {
        $temp = ($rawValue) ? round((((.659 - ((5 - ($rawValue * .001221)) / 4)) / .00132) - 40), 1, PHP_ROUND_HALF_DOWN) : 'N/A';
        $temp = ($temp * (9 / 5)) + 32;
        $temp = round($temp, 1, PHP_ROUND_HALF_DOWN);

        return $temp + $tempOffset;
    }



    /**
     * getTrendline
     *
     * $slope, $yIntercept, cumulative pour length.
     *
     * @return correctedOunces
     */
    public function getTrendline(float $slope, float $yIntercept, float $pour): float
    {
        $ounces = $slope * $pour + $yIntercept;
        return $ounces;
    } /*Close getTrendline */

    /**
     * Get Volume.
     * This is the function being used to calculate
     *
     * @var collection
     *                 $data collection should contain (diameter of keg, height of dip tube, raw value, gravity of beer, calibration)
     *
     * @return volume left in keg
     *                OZlft = .5541 π [c/(2 π)-.048]  2 [HTDT – (44.6 *(ADC-calibration)*.001221 / Vs -.1.784)/ SBX]
     */
    public function getVolume($data, $calibration)
    {

        if (is_null($data->diameter)) {
            $radius = $data->kegdevice->kegspecs->kegtypes->diameter / 2;
            $diameter = $data->kegdevice->kegspecs->kegtypes->diameter;
        } else {
            $radius = $data->diameter / 2;
            $diameter = $data->kegdevice->kegspecs->kegtypes->diameter;
        }

        if (is_null($data->dipTubeHeight)) {
            $dth = $data->kegdevice->kegspecs->kegtypes->dipTubeHeight;
        } else {
            $dth = $data->dipTubeHeight;
        }
        //OZlft = .5541 π [c/(2 π)-.048]  2 [HTDT – (44.6 *(ADC-calibration)*.001221 / Vs -.1.784)/ SBX]

        $inToOz = .5541;
        $cs = M_PI*(((($diameter * M_PI)/(2*M_PI))-0.048)**2);
        $brHt = ($dth-(((44.6*.001221)*($data->adc_pour_end-(-121)))/(5*1.01)-(1.784/1.01)));

        $volume =  $cs * $inToOz * $brHt;

        return $volume;
    }


    /**
     * @method getXArray
     *
     * X are culmative sum values; so x1 = x1, x2 = x1 + x2;
     * The calculated sum values are the values returned in the array
     *
     * @var $data: Collection
     * @var $column: string
     *
     * @return array
     */
    public function getXArray(\Illuminate\Support\Collection $data, string $column):array{
        $dArray = $data->pluck($column)->toArray();

        // Set previous value to -1 in case the values are 0
        $previousValue = -1;
        $newValue = 0;

        // Lets sum the values and place them in xArray
        foreach($dArray as $d){

            if(is_numeric($d)){

                if($previousValue == -1){
                    $xArray[] = $d;
                    $previousValue = $d;
                }else{
                    $newValue = $previousValue + $d;
                    $xArray[] = $newValue;
                    $previousValue = $newValue;
                } /* End if($previousValue == -1) */

            }else{
                throw new \InvalidArgumentException('getXArray should be passed integers. Invalid input was ' . gettype($d) );
            } /* End if(is_int)...else... */

        } /* End foreach($dArray...) */
        return $xArray;
    } /* End public function getXArray() */

    /**
     * @method getYArray
     *
     * Retrieves a column and converts it to an array from a Collection
     * @todo: laravel provides this with easy methods;
     * consider whether this is really needed or if it should just be collapsed
     *
     * @var Collection $data
     * @var String $column
     *
     * @return array
     */
    public function getYArray(\Illuminate\Support\Collection $data, string $column):array{

        $yArray = $data->pluck($column)->toArray();

        return $yArray;
    }

    /**
     * @method getUnprocessedData
     * Select all the data that has been inserted into kegdata_transmit but has not been processed
     *
     * @return Eloquent Collection: KegData_Transmit with KegDevice & KegDevice.KegSpec Relations
     */
    public function getUnprocessedData(int $take = 100, int $skip = 0): \Illuminate\Database\Eloquent\Collection
    {
        $data = $this->transmit->whereNull('processed_at')
                ->skip($skip)->take($take)
                ->orderBy('created_at', 'asc')->get();

        return $data;
    }

    /**
     * Get the percentage based on the number of ources.
     *
     * @var int
     * @var $capacity int
     */
    public function percentage($ounces,$capacity = 1984): string
    {
        $value = $ounces;

        //if not set keg capacity assume full size keg
        $cap = $capacity;

        //should calculate the percentage of beer left in a keg
        if ($cap == 0) {
            $perc = (100 * $value) / 1984;
        } else {
            $perc = (100 * $value) / $cap;
        }

        $perc = ($perc < 5) ? '00' : $perc;
        $perc = ($perc < 10 && $perc >= 5) ? '05' : $perc;
        $perc = ($perc < 15 && $perc >= 10) ? '10' : $perc;
        $perc = ($perc < 20 && $perc >= 15) ? '15' : $perc;
        $perc = ($perc < 25 && $perc >= 20) ? '20' : $perc;
        $perc = ($perc < 30 && $perc >= 25) ? '25' : $perc;
        $perc = ($perc < 35 && $perc >= 30) ? '30' : $perc;
        $perc = ($perc < 40 && $perc >= 35) ? '35' : $perc;
        $perc = ($perc < 45 && $perc >= 40) ? '40' : $perc;
        $perc = ($perc < 50 && $perc >= 45) ? '45' : $perc;
        $perc = ($perc < 55 && $perc >= 50) ? '50' : $perc;
        $perc = ($perc < 60 && $perc >= 55) ? '55' : $perc;
        $perc = ($perc < 65 && $perc >= 60) ? '60' : $perc;
        $perc = ($perc < 70 && $perc >= 65) ? '65' : $perc;
        $perc = ($perc < 75 && $perc >= 70) ? '70' : $perc;
        $perc = ($perc < 80 && $perc >= 75) ? '75' : $perc;
        $perc = ($perc < 85 && $perc >= 80) ? '80' : $perc;
        $perc = ($perc < 90 && $perc >= 85) ? '85' : $perc;
        $perc = ($perc < 95 && $perc >= 90) ? '90' : $perc;
        $perc = ($perc < 99 && $perc >= 95) ? '95' : $perc;
        $perc = ($perc > 99) ? '99' : $perc;

        return $perc;
    }//close keg percent function

    /**
     * @method moveLine
     * moves the data line from kegdata_transmit to kegdata_processed
     * or kegdata_heartbeats
     *
     * @var dataLine
     * @var table    -> what table it should go into to
     *
     * @return boolean: true/false -> was the dataline sucessfully saved
     */
    public function moveLine($data, string $table, $reset = 0, $kegdevice = 0, $reset_code = NULL)
    {
        $saved = true;

        if($table == 'kegdata_processed'){
            $line = $this->processed->firstOrCreate([
                 'hubmac' => $data->hubmac
                , 'kegmac' => $data->kegmac
                , 'adc_pour_start' => $data->adc_pour_start
                , 'adc_pour_end' => $data->adc_pour_end
                , 'adc_pour' => $data->adc_pour
                , 'pour_length' => $data->pour_length
                , 'temperature' => $data->temperature
                , 'status' => $data->status
                , 'kegreset' => $reset
                , 'reset_code' => $reset_code
                , 'remaining_ounces' => (is_null($data->remaining_ounces)) ? NULL : $data->remaining_ounces
                , 'slope' => (is_null($data->slope)) ? NULL : $data->slope
                , 'yIntercept' => (is_null($data->yIntercept)) ? NULL : $data->yIntercept
                , 'sent_at' => $data->sent_at
                ]);
        }else{
            $line = $this->heartbeat->firstOrCreate([
                  'hubmac' => $data->hubmac
                , 'kegmac' => $data->kegmac
                , 'adc_pour_start' => $data->adc_pour_start
                , 'adc_pour_end' => $data->adc_pour_end
                , 'adc_pour' => $data->adc_pour
                , 'pour_length' => $data->pour_length
                , 'temperature' => $data->temperature
                , 'status' => $data->status
                , 'sent_at' => $data->sent_at
                ]);
        }


        $line->sent_at = $data->sent_at;
        $line->created_at = $data->created_at;

        $hub = HubDevice::where('hubmac', '=', $data->hubmac)->first();
        $device = $this->device->where('kegmac', '=', $data->kegmac)
                    ->where('account_id', '=', $hub->account_id)->first();

        if($reset == 1){
            // Values in kegdevice need to be reset to NULL
            $device->slope = NULL;
            $device->yIntercept = NULL;
            $device->remaining_ounces = NULL;
        }else{
            $device->slope = (is_null($data->slope)) ? $device->slope : $data->slope;
            $device->yIntercept = (is_null($data->yIntercept)) ? $device->yIntercept : $data->yIntercept;
            $device->remaining_ounces = (is_null($data->remaining_ounces)) ? $device->remaining_ounces : $data->remaining_ounces;
        }

        $device->save();

        if($line->save()){
            // $data->processed_at = new Carbon('now');
            // $data->save();
            $updateTransmit = $this->transmit->find($data->id);
            $updateTransmit->processed_at = new Carbon('now');
            $updateTransmit->save();
            return $line;
        }
        // Pour events should come with remaining_ounces, kegdevice relation with slope & yIntercept updated.
        return 0;
    }

     /**
      * @method processData
      * This function is meant to determine if the line of data is
      * valid by comparing the flow rate to line of change
      * If the data is valid, the function will move the line to
      * the heartbeat table or the processed table,
      * recalculate the ounce
      * @todo : move pour difference comparison and adc difference comparison to constants
      *
      * @var data: collection -> single line
      *
      * @return boolean: true/false -> keep the data or throw it out
      */
     public function processData($data): array
     {
        $toKeep['status'] = 'no status';

        //Get up to the last 20 pours for the device
        $lastPours = $this->processed->getLastPours($data->hubmac, $data->kegmac, 20, 0);

        // If the device is almost empty and the line we are now processing is a blow || tap event -> reset the device...
        if(!is_null($lastPours->first()) && !is_null($lastPours->first()->remaining_ounces) && $lastPours->first()->remaining_ounces < 100){
           $kegreset = 1;
           $kegdevice = 0;
       }else{
           $kegreset = 0;
           $kegdevice = 1;
       }

        /*
        * Lets check what status code we are processing to start the
        * logic chain
        * BITS are read from right to left
        * (Bit 0 is least significant bit).
        * Bit 0: Set if pressure sensor is installed.
        * Bit 1: Set if reporting pour event.
        * Bit 2: Set if keg is untapped.
        * Bit 3: Tags it as heart beat packet Array Position 1
        * Bit 4: Blow event.
        */
        $status = $data->status;
        //Pad the status so we have all 5 bits to play with
        $status = str_pad($status, 5, '0', STR_PAD_LEFT);
        //Split the string and reverse the array so the the array positions
        //match the bit positions
        $statusArray = array_reverse(str_split($status));

        // Check for heartbeat - this can be reported with some of
        // the other statusCodes -> but I don't want multiple untap lines
        if ($statusArray[3]) {
            $toKeep['status'] = 'heartbeat';
            $line = $this->moveLine($data, 'kegdata_heartbeats');
            $toKeep['data'] = $line;
            return $toKeep;
        }

        // Check for a device error, if the sensor has errorred,
        // move the line, send notifications, reset the device slope, yIntercept
        if(!$statusArray[0]) {
            $toKeep['status'] = 'sensor problem';
            $kegreset = 1;
            $kegdevice = 0;
            $reset_code = 1;
            $line = $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice, $reset_code);
            $this->device->resetSlope($data->kegdevice->id);
            $this->device->resetYIntercept($data->kegdevice->id);
            $toKeep['data'] = $line;
            return $toKeep;
        }

        // Check for untap event, if it has occurred, use the above kegreset value and move the line
         if ($statusArray[2]) {
             $toKeep['status'] = 'keg untapped';
             $reset_code = 2;
             $line = $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice, $reset_code);
             $this->device->resetSlope($data->kegdevice->id);
             $this->device->resetYIntercept($data->kegdevice->id);
             $toKeep['data'] = $line;
             return $toKeep;
         }

         //check for a blow event, if it has occurred, use the above kegreset value and move the line
         if ($statusArray[4]) {
             $toKeep['status'] = 'blow';
             $reset_code = 4;
             $line = $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice, $reset_code);
             $this->device->resetSlope($data->kegdevice->id);
             $this->device->resetYIntercept($data->kegdevice->id);
             $toKeep['data'] = $line;
             return $toKeep;
         }

        // Check for pour event - if pour event we need to check a
        // a lot of things
        if ($statusArray[1]) {
            $kegreset = 0;
            $reset_code = 0;
            // Get Slope of device
            $slope = $data->kegdevice->slope;
            // Get yIntercept of device
            $yIntercept = $data->kegdevice->yIntercept;

            // This is the first or second line after a reset or a new
            // device has been connected
            if (is_null($slope) && is_null($yIntercept)) {
                $toKeep['status'] = 'pour saved';
                if ($lastPours->count() >= 1) {
                    // We have another pour
                    // Lets run some calculations and Move the line,
                    // We don't have enough data for comparison?!?
                    $data->name = $data->kegdevice->deviceName;
                    $data->date = $data->sent_at;
                    $data->length = $data->pour_length;

                    $data->volume = $this->getVolume($data, $data->kegdevice->voltageOffset);

                    //Lets make a collection with last pours for processing and with the data processed above
                    $pourData = $this->combinePours($data, $lastPours);


                    // Because we have more than one line, lets Calculate
                    // The slope, yIntercept, Trendline (Remaining Ounces)

                    $xArray = $this->getXArray($pourData, 'length');
                    $yArray = $this->getYArray($pourData, 'volume');

                    $data->slope = $this->getSlope($xArray, $yArray);
                    $data->yIntercept = $this->yIntercept($xArray, $yArray, $data->slope);
                    $data->remaining_ounces = $this->getTrendline($data->slope, $data->yIntercept, last($xArray));

                    if($kegreset == 1){
                        $kegdevice = 0;
                    }else{
                        $kegdevice = 1;
                    }

                    $line = $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice);
                    $toKeep['data'] = $line;
                } else {
                    // This is the first pour
                    // Remaining ounces will still be NULL
                    // Nothing will be set in kegdevices
                    // no slope or yintercept will be applied
                    // Move the line - it might be bad (but we don't know)
                    $toKeep['status'] = 'pour saved';
                    $line = $this->moveLine($data, 'kegdata_processed', 0, 0);
                    $toKeep['data'] = $line;


                }
                return $toKeep;
            } else {
                // Calculate ounces
                $data->name = $data->kegdevice->deviceName;
                $data->date = $data->sent_at;
                $data->length = $data->pour_length;

                $data->volume = $this->getVolume($data, $data->kegdevice->voltageOffset);
                // Calculate ounces changed from last position (get the last pour event for this device)
                $l = $lastPours->first();
                $pourData = $this->combinePours($data, $lastPours);


                $xArray = $this->getXArray($pourData, 'length');
                $yArray = $this->getYArray($pourData, 'volume');

                $data->slope = $this->getSlope($xArray, $yArray);
                $data->yIntercept = $this->yIntercept($xArray, $yArray, $data->slope);
                $data->remaining_ounces = $this->getTrendline($data->slope, $data->yIntercept, array_sum($xArray));


                /* This seems to exclude too much data; commented out the
                comparison until it can be reviewed */
                if(!empty($l->remaining_ounces)){
                    $cInO = $l->remaining_ounces - $data->remaining_ounces;
                }
                else {
                    $cInO = $data->remaining_ounces;
                }

                // // Calculate flow rate
                // $fr = $cInO/$data->length;
                // // This should be close to the slope
                // $slopeDifference = $fr - abs($slope);
                // if(abs($slopeDifference) > 2){
                //     $data->slope = null;
                //     $data->yIntercept = null;
                //     $data->remaining_ounces = null;
                //     $kegreset = 1;
                //     $reset_code = 6;
                // }
                /**
                 * @todo comparison of slope
                 */
                if(!empty($l->adc_pour_end)){
                    $pourDifference = $l->adc_pour_end -
                                        $data->adc_pour_start;
                }else{
                    $pourDifference = 0;
                }
                if($pourDifference <= -50 || $pourDifference >= 50){
                    $data->slope = null;
                    $data->yIntercept = null;
                    $data->remaining_ounces = null;
                    $kegreset = 1;
                    $reset_code = 7;
                }
                //Prepour & post pour comparison
                $value = $data->adc_pour_start - $slope * $data->length;

                $adcDiff = $data->adc_pour_end - $value;

                if($adcDiff > 100 || $adcDiff < -100){
                    $data->slope = null;
                    $data->yIntercept = null;
                    $data->remaining_ounces = null;
                    $kegreset = 1;
                    $reset_code = 8;
                }
                // Move the line if valid

                if($kegreset == 1){
                    $kegdevice = 0;
                }else{
                    $kegdevice = 1;
                }

                $line = $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice, $reset_code);

                // toKeep = 'ouncesUpdated' or 'lineRemoved'
                if($kegreset == 1){
                    $toKeep['status'] = 'line removed';
                    $toKeep['data'] = '';
                }else{
                    $toKeep['status'] = 'ounces updated';
                    $toKeep['data'] = $line;
                }
            }


            return $toKeep;
        }


        $toKeep['status'] = 'other';
        $reset_code = 9;
        $this->moveLine($data, 'kegdata_processed', $kegreset, $kegdevice, $reset_code);
         return $toKeep;
     }

    /**
     * @method yIntercept
     * collection $data
     * $slope.
     *
     * @return yIntercept
     */
    public function yIntercept(array $xArray,array $yArray, float $slope): float
    {
        // n = count($data)
        //e = (y1 + y2)
        //f = $slope * (x1 + x2)
        // $yIntercept = (e - f)/n
        $yIntercept = 0;

        if(count($xArray) != count($yArray)){
            throw new \InvalidArgumentException("X Array must have the same number of items as Y Array");
        }

        $n = count($xArray);

        $e = array_sum($yArray);

        $xSum = array_sum($xArray);

        $f = $slope * $xSum;

        $yIntercept = ($e - $f) / $n;

        return $yIntercept;
    } /* Close yIntercept*/
} /* Close kegdataprocessor class*/
