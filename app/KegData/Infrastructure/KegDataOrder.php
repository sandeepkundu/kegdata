<?php namespace App\KegData\Infrastructure;

/**
  * @todo OrderInterface
  */
use App;
use Stripe;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Cart;
use App\KegData\Models\Plan as Plan;
use App\KegData\Models\Product as Product;
use App\KegData\Models\Order as Order;
use App\KegData\Models\OrderItem as OrderItem;
use App\KegData\Models\OrderAddress as OrderAddress;
use App\KegData\Models\Subscription as Subscription;
use App\KegData\Models\KegDevice as KegDevice;
use App\KegData\Infrastructure\StoreLogger as  StoreLogger;
use App\KegData\Models\Account as Account;
use App\KegData\Models\User as User;
use DB;
use Log;
use Illuminate\Contracts\Auth\Guard;
use App\KegData\Repositories\EloquentCategoryRepository as Category;

class KegDataOrder{

	/**
	 * Declare Class Parameters
	 * Write methods to get these values; should only be set
	 * By the class
	 */

	protected $productCart, $planCart,$user,$account,$shippingAddress, $billingAddress,
			$stripeToken, $shipping, $tax, $subtotal, $total,
			$order, $orderItems, $plan, $customer, $subscription, $charge, $storeLogger;
		//public $stripe = Stripe::make('sk_test_jT8arnwWWjzYxIRAM4xG5g9w');
	/**
	 * Create a new KegDataOrder
	 */
	public function __construct(Stripe $stripe, Account $accountRepo, User $userRepo, Order $orderModel, OrderItem $itemModel, OrderAddress $addressModel, Product $productModel, Plan $planModel, Subscription $subscriptionModel, StoreLogger $logger,Guard $auth,Category $categories){
		$this->orderModel = $orderModel;
		$this->itemModel = $itemModel;
		$this->addressModel = $addressModel;
		$this->productModel = $productModel;
		$this->planModel = $planModel;
		$this->subscriptionModel = $subscriptionModel;
		$this->accountRepo = $accountRepo;
		$this->userRepo = $userRepo;
		$this->logger = $logger;
		$this->stripe = Stripe::make(env('stripe_key',''));
		$this->auth = $auth;
		$this->categories = $categories;
		 
	}

	/**
	 * Function to handle the entire order process
	 * @param String $stripeToken
	 * @param Cart $productCart
	 * @param Cart $planCart
	 * @param Array $addresses
	 * @return Collection $order with $relations || $error
	 */
	public function handleOrder($stripeToken, User $user, Account $account, array $addresses, $productCart, $subscriptionCart, $subtotal, $orderID = 0){
		

		$this->stripeToken = $stripeToken;
		$this->productCart = $productCart;
		$this->planCart = $subscriptionCart;
		$this->shippingAddress = $this->parseAddress($addresses);
		$this->billingAddress = $this->parseAddress($addresses, 'billing_');
		$this->user = $user;
		$this->account = $account;

		
		if(empty($this->productCart) && empty($this->planCart) == 0){

			return 'No order to process';
		}
		
		DB::BeginTransaction();

		try{
		//Process the product cart - Calculate Subtotal, shipping,  tax, total

			if($orderID != 0){

				$statusArray = [
						'charged',
						'processing',
						'shipped',
						'closed'
				];

				$oldOrder = $this->orderModel
									->with('orderItems')
									->with('orderAddress')
									->with('subscription')
									->where('id', '=', $orderID)
									->where('account_id', '=', $this->account)
									->whereNotIn('status', $statusArray )
									->get();

				$oldOrder->status = 'CLOSED_'.$oldOrder->status;
				$oldOrder->save();
			}
		

				$this->subtotal = $subtotal;
				$this->tax = ($this->billingAddress['stateCode'] == 'TX') ? ($this->subtotal * .0825) : 0;

				$this->shipping = ($this->subtotal < 199) ? ($this->subtotal * .2) : ($this->subtotal * .1);
				$this->total = $this->subtotal + $this->tax + $this->shipping;

				$this->order = $this->createOrder($this->account, $this->user, $this->shipping, $this->tax, $this->subtotal, $this->total);


			if($this->order){

				$this->shippingAddress = $this->createOrderAddress($this->shippingAddress, 's', $this->order, $this->account);
				$this->billingAddress = $this->createOrderAddress($this->billingAddress, 'b', $this->order, $this->account);
				
				if(!is_null($this->planCart) && $this->planCart->price > 0){
					
					$this->subscription = $this->createSubscription($this->planCart, $this->order, $account);
					
					$this->customer = $this->createStripeCustomer($this->account, $this->user, $this->plan, $this->billingAddress,$this->order, $this->subscription, $this->stripeToken);

					//dd($this->subscription->id);
					if($this->subscription) {

						$this->subscriptionModel->where('id', '=', $this->subscription->id)->update(['active' => 1]);
					}
					

					//dd($this->subscription->id)
					
				}else{

					
					$this->customer = $this->createStripeCustomer($this->account, $this->user, $this->plan, $this->billingAddress,$this->order, NULL, $this->stripeToken);
				}


				if(empty($this->customer)){
					DB::rollback();
					$error = $this->customer;
					$error['order_id'] = $this->order->id;
					return $error;
				}else{
					if(!empty($this->productCart) && $this->productCart->count() > 0){
						$this->orderItems = $this->createOrderItems($this->productCart, $this->order);

						foreach($this->orderItems as $item){
							$this->decreaseStock($item->id);
						}
						if(is_object($this->customer) && method_exists($this->customer, 'getErrorCode')){
							
							DB::commit();
							$error['code'] = $this->customer->getErrorCode();


							$error['message'] = $this->setErrorMessage($error['code']);
							$error['order_id'] = $this->order->id;
							return $error;
						}else{

							$this->charge = $this->createStripeCharge($this->order, $this->user, $this->account, $this->stripeToken, $this->customer, $this->total, $this->shippingAddress);
							
							if(empty($this->charge['id'])){
								DB::commit();
								$error['code'] = "CHARGE_ERROR_101 - ";
								$error['message'] = $this->charge;
								$error['order_id'] = $this->order->id;
								return $error;
							}
						}

					}
				} /* Close is_null($this->customer->id) */

				DB::commit();
			}else{
				DB::rollback();
				Log::error($e);
				$error['type'] = 'order';
				$error['code'] = 'ORDER_ERROR_101';
				$error['message'] = 'We apologize for the inconvience. An error occurred while attempting to create the order. All information for this order has been discarded.';
				$error['order_id'] = 0;
				return $error;

			} /* Close if($this->order) */
		}catch(\Exception $e){

			$this->order->status = $this->setOrderStatus($e);



			//$checkCharge = $e->getRawOutput(); //comment by S0
			$checkCharge=['carge'=>0];
			//dd($checkCharge);

			if(array_key_exists('charge', $checkCharge) && !is_null($checkCharge['charge'])){
				$this->order->charge_id = $checkCharge['charge'];
			}

			$this->order->save();
			DB::commit();
			Log::error($e);
			$error['type'] = 'order';
			$error['code'] = 'ORDER_ERROR_102';
			if(config('app.debug')){
				//$error['message'] = $e->getCode().": ".$e->getMessage();
				$error['message'] = 'Apologies, but your order could not be processed. You can try again with a different card.';
			}else{
				$error['message'] = 'Apologies, but your order could not be processed. You can try again with a different card.';
			}

			$error['order_id'] = $this->order->id;
			return $error;
		}

		 $t=$this->orderModel->with('orderAddress')->with('orderItem')
       		->with('orderItem.product')
       		->with('subscription')->with('subscription.plan')
       		->with('account')->with('user')->find($this->order->id);
       	 return $t;
	}

	/**
	 * Function to create a new order
	 * @param Cart $productCart
	 * @param Collection $account
	 * @param Collection $user
	 * @return Collection $order
	 */
	public function createOrder(Account $account, User $user,  $shipping, $tax, $subtotal, $total){


		$order =$this->orderModel->create([
			'subtotal' => $subtotal,
			'shipping' => $shipping,
			'tax' => $tax,
			'total' => $total,
			'status' => 'new'
		]);

		$order->account()->associate($account);
		$order->user()->associate($user);
		$order->save();

		return $order;
	}

	/**
	 * Function to add an order address
	 * @param Array $Address
	 * @param String $type
	 * @param Collection $order
	 * @return Collection $orderAddress
	 */
	public function createOrderAddress(array $address, $type, Order $order, Account $account){

		$orderAddress = $this->addressModel->create([]);
		$orderAddress->type = $type;
		$orderAddress->name = $address['name'];
		$orderAddress->address1 = is_null($address['address1']) ? '' : $address['address1'];
		$orderAddress->address2 = is_null($address['address2']) ? '' : $address['address2'];
		$orderAddress->city = $address['city'];
		$orderAddress->state = $address['stateCode'];
		$orderAddress->zipcode = $address['zipcode'];
		$orderAddress->country = $address['countryCode'];
		$orderAddress->account()->associate($account);
		$orderAddress->order()->associate($order);
		$orderAddress->save();
		return $orderAddress;
	}

	/**
	 * Function to add order items
	 * @param Array $items
	 * @param Collection $order
	 * @return Collection $items
	 */
	public function createOrderItems($items, Order $order){

		foreach($items as $item){
			$product = $this->productModel->find($item->id);

			$newItem = $this->itemModel->create([
				'price' => $item->price,
				'quantity' => $item->qty
			]);

			$newItem->product()->associate($product);
			$newItem->order()->associate($order);
			$newItem->save();
		}

		return $this->itemModel->where('order_id', '=', $order->id)->get();
	}

	/**
	 * Function to Create Subscription
	 * @param Array $subscription
	 * @param Collection $order
	 * @return  Collection $subscription
	 */
	public function createSubscription($subscription, Order $order, Account $account){

		$this->plan = $this->planModel->find($subscription->id);

		$quantity = 1;
		if($this->plan->is_quantity == 1){
			
			$basePrice = $this->plan->base_price;
			$planPrice = $this->plan->price;
			$quantityPrice = $this->plan->quantity_price;
			$kegs = KegDevice::where('account_id', $account->id)
							->where('showDevice', 1)->count();
			$kegcount = 0;

			foreach($this->productCart as $item){
				//dd($item->id);
				//dd($this->productModel->where('id',$item->id)->category()->get());
				//$category = $item->model->category()->get();
				//dd($category);
				/**
				 * Total Keg Count between active kegs & cart couplers
				 */
				//commented code
				/*if($category->where('slug','couplers')->count()|| $category->where('slug','systems')->count()){
					 $kegcount += $item->qty;
				}*/

				$kegcount += $item->qty;
			}

			$kegcount += $kegs;

			$totalPrice = $planPrice + ($kegcount * $quantityPrice);
			$quantity = $totalPrice/$basePrice;
		}
		
		$this->subscription = $this->subscriptionModel->create([
			//'quantity' => $quantity,
			'quantity' => 1,
			'start' => Carbon::now()->toDateTimeString(),
			//'active' => 1
		]);

		$this->subscription->plan()->associate($this->plan);
		$this->subscription->account()->associate($account);
		$this->subscription->order()->associate($order);
		$this->subscription->save();
		
		return $this->subscription;

	}

	/**
	 * Function to deactivate the old subscription in the subscriptions table
	 * Should be called before createSubscription
	 * @todo Integreate into createSubscription as it should be called before
	 * @param Account $account
	 * @return Boolean
	 */
	public function deactivateSubscription(Account $account){
		return $this->subscriptionModel->where('account_id', '=', $account->id)->update(['active' => 0]);
	}

	/**
	 * Function to decrease product stock
	 * @param Int Product $id
	 * @return Boolean
	 */
	public function decreaseStock($id){
		if($this->productModel->where('id', '=', $id)->decrement('stock')){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Function to increase product stock
	 * @param Int Product $id
	 * @return Boolean
	 */
	public function increaseStock($id){
		if($this->productModel->where('id', '=', $id)->increment('stock')){
			return true;
		}else{
			return false;
		}
	}


	/**
	 * Function to create stripe customer
	 * @todo decouple from Stripe
	 * @param Collection $account
	 * @param Collection $user
	 * @param Collection $plan
	 * @param Collection $billingAddress
	 * @param Collection $order
	 * @param String $stripeToken
	 * @return Stripe Customer
	 */
	public function createStripeCustomer(Account $account, User $user, $plan, OrderAddress $billingAddress, Order $order, $subscription, $stripeToken){

		
		$quantity = 1;
		$tax_percent = 0;
		
		if(!is_null($plan) && $plan->is_quantity == 1){

			$basePrice = $plan->base_price;
			$planPrice = $plan->price;
			$quantityPrice = $plan->quantity_price;
			$kegs = KegDevice::where('account_id', $account->id)
							->where('showDevice', 1)->count();
			$kegcount = 0;

			
			foreach($this->productCart as $item){
				//$category = $item->model->category()->get();
				$product_item = Product::find($item->id); 
				
		        $category=$product_item->category; 

		        if($category->where('slug','couplers')->count()){
		        
		            $kegcount += $item->qty;
		        }
				/**
				 * Total Keg Count between active kegs & cart couplers
				 */
				/*if($category->where('slug','couplers')->count()|| $category->where('slug','systems')->count()){
					 $kegcount += $item->qty;
				}*/

				//$kegcount += $item->qty;

			}

			// code to fetch out last order couplars
			$last_order = Order::where('account_id', $account->id)
							->where('status', 'charged')->get();
			$last_order_coupler_quantity=0;
			foreach ($last_order as $v) {
				$order_item = $v->OrderItem;
				foreach($order_item as $oi) {
					
					if($oi->product->category[0]->slug == 'couplers') {

						$last_order_coupler_quantity = $last_order_coupler_quantity + $oi->quantity;
					}

				}
			}
			
			//end code to fetch out last code couplar

			$kegcount = $last_order_coupler_quantity + $kegcount;


			
			$totalPrice = $planPrice + ($kegcount * $quantityPrice);

			if($basePrice) {
				$quantity = $totalPrice/$basePrice;	
			} else {
				$quantity = $totalPrice;	
			}
			
			
			$quantity = ($quantity) ? floor($quantity) : 0;
			
			/*  Find old number of coupler By 14 
			$data = $this->getLastUprchaseCoupler();
			 End  */


			//find out tax_percent to add as a estra charge on couplar bases
		
			$extra_recurring_fee = $totalPrice - $planPrice;
			if($extra_recurring_fee) {
				$tax_percent = ($extra_recurring_fee*100)/$planPrice;
				$tax_percent = floor($tax_percent);
			}

			
			//end find out tax_percent 

			

		}
		
		try {
		
			//Check if this customer has already been created with Stripe
			if(empty($account->stripe_customer_id)){
					
				//create plan
				//first check the plan if exist

			/*	$plan_stripe =  $this->stripe->plans()->find($plan->name);
				if($plan_stripe['name']) {
					$plan_name=$plan_stripe['name'];
					$plan_id=$plan_stripe['id'];
				} else { // create plan
					$plan_stripe = $this->stripe->plans()->create([
					    'id'                   => $plan->name,
					    'name'                 => $plan->slug,
					    'amount'               => $plan->price,
					    'currency'             => 'USD',
					    'interval'             => 'month',
					    'statement_descriptor' => 'Monthly Subscription',
					]);
				}*/

				$customer = $this->stripe->customers()->create([
					'source' => $stripeToken,
			    	'email' => $user->email,
			    	'plan' => $plan->slug,
					//'quantity' => $quantity,
					'quantity' => 1,
					'description' => $account->accountName
				]);

				//update subscription after created  customer because need to add tax percent

				$stripe_subscription_id = isset($customer['subscriptions']['data'][0]['id'])?$customer['subscriptions']['data'][0]['id']:null;
				$stripe_customer_id = isset($customer['id']) ? $customer['id']: null ;

				if($stripe_subscription_id && $stripe_customer_id) {
					$data = $this->stripe->subscriptions()->update($stripe_customer_id, $stripe_subscription_id,[
						'plan' => $plan->slug,
						'tax_percent' => $tax_percent
						]);

				}
				

				// end update subscription
				

				//create subscription of customer
				/*if($customer['id']) {
					$subscription = $this->stripe->subscriptions()->create($customer['id'], [
					    'plan' => $plan->name,
					    'tax_percent' => ($billingAddress['stateCode'] == 'TX') ? 8.25 : 0,
					    'quantity'=>$quantity

					]);
				}*/

				
		
				 //$sub = $customer->subscriptions->create(array('plan' => $planID));
				
			}else{
				
						
				$customer =$this->stripe->customers()->update($account->stripe_customer_id, [
						'email' => $user->email,
						'source' => $stripeToken
				]);
				
				if(!is_null($subscription)){

					//Deactivate the old subscriptions in the subscriptions table
					$this->deactivateSubscription($account);
					/**
					* @todo If subscriptions ever have other intervals, proration should be used on monthly to yearly, yearly to month updates
					*/	

				
 					$data = $this->stripe->subscriptions()->update($account->stripe_customer_id, $account->stripe_subscription,['plan' => $plan->slug,'tax_percent' => $tax_percent]);

 					$customer['subscriptions']['data'][0]['id']=$data['id'];
 					$customer['subscriptions']['data'][0]['plan']['id']= $data['plan']['id'];
 					$customer['subscriptions']['data'][0]['trial_end']=$data['trial_end'];
 					
				}


			}
			
			$account->stripe_active = 1;
			$account->stripe_id = $stripeToken;
			$account->stripe_customer_id = $customer['id'];
			$account->stripe_subscription = isset($customer['subscriptions']['data'][0]['id'])?$customer['subscriptions']['data'][0]['id']:null;
			//$account->stripe_subscription = null;
			$account->stripe_plan = isset($customer['subscriptions']['data'][0]['plan']['id'])?$customer['subscriptions']['data'][0]['plan']['id']:null;
			$account->last_four = isset($customer['sources']['data'][0]['last4'])?$customer['sources']['data'][0]['last4']:null;
			$account->trial_ends_at = isset($customer['subscriptions']['data'][0]['trial_end'])?$customer['subscriptions']['data'][0]['trial_end']:null;
			$account->save();

			return $customer;
			 // Use Stripe's library to make requests...
		  } catch(\Stripe\Error\Card $e) {
			 //Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - PAYMENT FAILED';
			$order->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - API ERROR';
			$order->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - SYSTEM ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    //$message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData SYSTEM Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - AUTH ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    //$message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData AUTH Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
		  	// Network communication with Stripe failed
		  	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - STRIPE ERROR';
			$order->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email

			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - STRIPE ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    //$message->to('latisha.mcneel@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData STRIPE Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Exception $e) {
		 	// Something else happened, completely unrelated to Stripe

			Log::error($e);

			//Update the status code with error
			$order->status = $e->getErrorCode();
			$order->save();
			//Return the Error
			return $e;
		}
		//End Catch Statements for Stripe Error
	}

	/**
	 * Function to create stripe charge
	 * @todo decouple from Stripe
	 * @param Collection $order with $relations
	 * @param Auth $user
	 * @param String $stripeToken
	 * @param array $customer
	 * @return Stripe Charge
	 */
	public function createStripeCharge(Order $order, User $user, Account $account, $stripeToken, array $customer, $total, $shippingAddress){
		try{
			$charge = $this->stripe->charges()->create(array(
				'amount' => $total,

				'currency' => 'USD',
				'customer' => $customer['id'],
				'metadata' => ['order_id' => $order->id],
				'statement_descriptor' => 'kegdata.com Purchase',
				'receipt_email' => App::environment() == 'local' ? 'sandeepkundu4344@gmail.com' : $user->email,
				'shipping' => [
					'name' => $shippingAddress->name,
					'address' => [
					'city' => $shippingAddress->city,
					'country' => $shippingAddress->countryCode,
					'line1' => $shippingAddress->address1,
					'line2' => $shippingAddress->address2,
					'postal_code' => $shippingAddress->zipcode,
					'state' => $shippingAddress->stateCode
				]]
			));

			$order->charge_id = $charge['id'];
			$order->status = 'charged';
			$order->save();

			return $charge;
		} catch(\Stripe\Error\Card $e) {
			 //Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - PAYMENT FAILED';
			$order->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - API ERROR';
			$order->save();

			//Return the Error
			return $err;
			// Invalid parameters were supplied to Stripe's API
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - SYSTEM ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('sandeepkundu4344@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData SYSTEM Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - AUTH ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('sandeepkundu4344@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData AUTH Error for Review');
			});

			//Return the Error
			return $err;
		} catch (\Stripe\Error\ApiConnection $e) {
		  	// Network communication with Stripe failed
		  	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - STRIPE ERROR';
			$order->save();

			//Return the Error
			return $err;
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email

			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - STRIPE ERROR';
			$order->save();

			$emailAddress = $user->email;
			$emailData['err'] = serialize($err);
			Mail::queue('emails.error', $emailData, function($message)
			{
			    $message->to('sandeepkundu4344@gmail.com')->bcc()->from('admin@kegdata.com')->subject('KegData STRIPE Error for Review');
			});

			//Return the Error
			return $err;
		} catch (Exception $e) {
		 	// Something else happened, completely unrelated to Stripe

		 	$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->logger->write($e->getHttpStatus().' | '.$err['type'].' | '.$err['code'].' | '.$err['message']);

			//Update the status code with error
			$order->status = 'PENDING - SYSTEM ERROR';
			$order->save();
			//Return the Error
			return $err;
		}
		//End Catch Statements for Stripe Error
	}


	/**
	 * Function to parse address from $addresses array
	 * @param array $addresses
	 * @param string $prefix
	 * @return $address
	 */
	public function parseAddress($addresses, $prefix = ''){
		$address['name'] = $addresses[$prefix.'name'];
		$address['address1'] = $addresses[$prefix.'address1'];
		$address['address2'] = $addresses[$prefix.'address2'];
		$address['city'] = $addresses[$prefix.'city'];
		$address['stateCode'] = $addresses[$prefix.'stateCode'];
		$address['zipcode'] = $addresses[$prefix.'zipcode'];
		$address['countryCode'] = $addresses[$prefix.'countryCode'];

		return $address;
	}

	/**
	 * @method setErrorMessage
	 *
	 * Function to set the error message to the user based on the code
	 *
	 * @param string $errorCode
	 *
	 * @return string $message
	 */
	 public function setErrorMessage($errorCode) {
		 $message = "Apologies, an error occurred processing the order. Check the dashboard to see if you can resubmit the payment.";

		 switch($errorCode){
			 case 'processing_error':
			 	$message = "Apologies, an error occurred processing the credit card. Please try a different card or try again later.";
			 case 'incorrect_cvc':
			 	$message = "Apologies, the credit card information you entered was incorrect. Please check the information and try again.";
			 break;
		 }

		 return $message;
	 }

	/**
	 * @method setOrderStatus
	 * Function to set order status
	 *
	 * @param string $errorCode
	 * @param string
	 *
	 * @return string orderStatus
	 */
	 public function setOrderStatus($e) {
		 $status = "PENDING";
		/* $error = $e->getRawOutput();
		 $code = $e->getErrorCode();

		 switch($code){
			 case 'card_declined':
			 	$reason = $error['error']['decline_code'];
				if($reason == 'fraudulent'){
					$status = 'DECLINED_FRAUDULENT';
				}else{
					$status = "DECLINED";
				}
			 break;
		 }*/

		 return $status;
	 }

	/**
	 * @method getLastUprchaseCoupler
	 * Function to Get coupler which last purchase 
	 *
	 * @param string $errorCode
	 * @param string
	 *
	 * @return string number of coupler
	 */
	public function getLastUprchaseCoupler() {
		
		/* Code for total coupler purchage in last order*/

		$userOrder_coupler_total= 0;
		if($this->auth->user()){

			$categorySlug = 'couplers';
			$current_cat = $this->categories->where('slug', $categorySlug)->get();

			$prodCat = DB::table('category_product')
			->where('category_id', '=', $current_cat['0']['id'])
			->get();

			foreach ($prodCat as $prodCat_id) {
				$current_user_order =  $this->auth->user()->account->Order()
				->orderBy('id','desc')
				->limit('1')
				->get(); 
			
				foreach ($current_user_order as $currentUseOredr) {

					if($currentUseOredr->status == 'charged') {

						$userOrder_item= $this->itemModel
                       // ->select('quantity')
						->where('order_id','=', $currentUseOredr->id)
						->where('product_id','=',$prodCat_id->product_id)
						//->orderBy('id','desc')
						->limit('1')
						->get();
						
						foreach ($userOrder_item as $qty) {
							if ($qty) {

								$userOrder_coupler_total+= $qty->quantity;
							}
						}

					}

				}   

			}
		}// End Auth::User if
		dd($userOrder_coupler_total);
	}
}
