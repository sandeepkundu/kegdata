<?php namespace App\KegData\Interfaces;

/**
 * Order Interface
 * This interface defined all the functions needed to complete an order
 * @todo convert this to a command at a later point
 */


interface OrderInterface{

	/**
	 * Function to handle the entire order process
	 * @param String $stripeToken
	 * @param Cart $productCart
	 * @param Cart $planCart
	 * @param Array $addresses
	 * @return Collection $order with $relationships
	 */
	public function handleOrder($stripeToken, $productCart, $planCart, array $addresses = array());

	/**
	 * Function to create a new order
	 * @param Cart $productCart
	 * @param Collection $account
	 * @param Collection $user
	 * @return Collection $order
	 */
	public function createOrder($productCart,  $account, $user, $order);

	/**
	 * Function to add an order address
	 * @param Array $Address
	 * @param String $type
	 * @param Collection $order
	 * @return Collection $orderAddress
	 */
	public function createOrderAddress(array $address, $type, $order);

	/**
	 * Function to add order items
	 * @param Array $items
	 * @param Collection $order
	 * @return Collection $items
	 */
	public function createOrderItems(array $items, $order);

	/**
	 * Function to Create Subscription
	 * @param Array $subscription
	 * @param Collection $order
	 * @return  Collection $subscription
	 */
	public function createSubscription(array $subscription, $order);

	/**
	 * Function to deactivate the old subscription in the subscriptions table
	 * Should be called before createSubscription
	 * @todo Integreate into createSubscription as it should be called before
	 * @param Account $account
	 * @return Boolean
	 */
	public function deactivateSubscription($account);

	/**
	 * Function to decrease product stock
	 * @param Int Product $id
	 * @return Boolean
	 */
	public function decreaseStock($id);

	/**
	 * Function to increase product stock
	 * @param Int Product $id
	 * @return Boolean
	 */
	public function increaseStock($id);


	/**
	 * Function to create stripe customer
	 * @todo decouple from Stripe
	 * @param Collection $account
	 * @param Collection $user
	 * @param Collection $plan
	 * @param Collection $billingAddress
	 * @param Collection $order
	 * @param String $stripeToken
	 * @return Stripe Customer
	 */
	public function createStripeCustomer($account, $user, $plan, $billingAddress, $order, $stripeToken);

	/**
	 * Function to create stripe charge
	 * @todo decouple from Stripe
	 * @param Collection $order with $relations
	 * @param Auth $user
	 * @param String $stripeToken
	 * @param array $customer
	 * @return Stripe Charge
	 */
	public function createStripeCharge( $order, $stripeToken, $customer);
}

?>