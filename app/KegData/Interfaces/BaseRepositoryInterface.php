<?php namespace App\KegData\Interfaces;

interface BaseRepositoryInterface{

	public function getAll();

	public function getPaginated();

	public function getForSelect($data);

	public function getById($id);

	public function getByColumn($term);

	public function getActively($term);

}

?>