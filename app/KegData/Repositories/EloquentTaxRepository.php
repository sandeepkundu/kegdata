<?php namespace App\KegData\Repositories;


class EloquentTaxRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Tax';
protected $relationships = [];


public function createTax($input){
	$tax = $this->model->create($input);
	return $tax->push();
}

public function updateTax($input, $id){

	$tax = $this->model->find($id);
	$tax->name = (is_null($input['name'])) ? $tax->name : $input['name'];
	$tax->rate = (is_null($input['rate'])) ? $tax->rate : $input['rate'];
	
	return $tax->push();
}

public function deleteTax($id){
	$tax = $this->model->find($id);
	return $tax->delete();
}

}

?>