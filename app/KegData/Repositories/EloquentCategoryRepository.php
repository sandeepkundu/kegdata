<?php namespace App\KegData\Repositories;

use App\KegData\Models\Photo as Photo;

class EloquentCategoryRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Category';
protected $relationships = [];


public function createCategory($input, $image = null){
	$public = public_path();
	$destinationPath ='/img/uploads/store/category/';
	$category = $this->model->create($input);
	if(array_key_exists('is_navigation', $input) && $input['is_navigation']){
			$category->is_navigation = 1;
	}else{
		$category->is_navigation = 0;
	}
		$category->save();

	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		$photos['path'] = $destinationPath.$image->getClientOriginalName();
		$photos['mime'] = $image->getClientMimeType();

		$photo = new Photo;
		$photo = $photo->create($photos);
		$category->photos()->attach($photo->id);
	}

	return $category->push();
}

public function updateCategory($input, $image = null, $id){
	$public = public_path();
	$destinationPath ='/img/uploads/store/category/';

	$category = $this->model->with('photos')->find($id);
	$category->name = (is_null($input['name'])) ? $category->name : $input['name'];
	$category->description = (is_null($input['description'])) ? $category->description : $input['description'];
	$category->short_description = (is_null($input['short_description'])) ? $category->short_description : $input['short_description'];
	$category->meta_description = (is_null($input['meta_description'])) ? $category->meta_description : $input['meta_description'];
	$category->meta_keywords = (is_null($input['meta_keywords'])) ? $category->meta_keywords : $input['meta_keywords'];
	if(array_key_exists('is_navigation', $input) && $input['is_navigation']){
			$category->is_navigation = 1;
	}else{
		$category->is_navigation = 0;
	}

	if(!is_null($image)){

		$image->move($public.$destinationPath, $image->getClientOriginalName());
		if(is_null($category->photos)){
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos);
			$category->photos()->attach($photo->id);
		}else{
			$relatedIds = $category->photos->lists('id');
			$category->photos()->detach($relatedIds);
			$category->photos()->delete();
			$category->push();
			$photos = Photo::wherein('id',$relatedIds)->get();
			//Photo::destroy($relatedIds);
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos->toArray());
			$category->photos()->attach($photo->id);
		}

	}

	return $category->push();
}

public function deleteCategory($id){
	$category = $this->model->with('photos')->find($id);
	$relatedIds = $category->photos->lists('id');
	$category->photos()->detach($relatedIds);
	$category->photos()->delete();
	$category->push();
	$photos = Photo::wherein('id',$relatedIds)->get();
	Photo::destroy($relatedIds);
	return $category->delete();
}

}

?>
