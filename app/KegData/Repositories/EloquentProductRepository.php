<?php namespace App\KegData\Repositories;

use App\KegData\Models\Photo as Photo;
use Stripe;


class EloquentProductRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Product';
protected $relationships = [];


public function createProduct($input, $image = null){
	$public = public_path();
	$destinationPath ='/img/uploads/store/product/';
	$product = $this->model->create($input);
	if(array_key_exists('category', $input) && !is_null($input['category']) && count($input['category'])){
		$product->category()->attach($input['category']);
	}
	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		$photos['path'] = $destinationPath.$image->getClientOriginalName();
		$photos['mime'] = $image->getClientMimeType();
		$stripeImageArray = [ 'http://www.kegdata.com'.$photos['path']];
		$photo = new Photo;
		$photo = $photo->create($photos);
		$product->photos()->attach($photo->id);
	}else{
		$stripeImageArray = [];
	}

	try{
		$stripeArray['name'] = $product->name;
		$stripeArray['active'] = true;
		$stripeArray['metadata'] = array('part_id' => $product->part_id);
		$stripeArray['shippable'] = true;
		if(!empty($product->short_description)){
			$stripeArray['caption'] = $product->short_description;
		}
		if(!empty($product->description)){
			$stripeArray['description'] = $product->description;
		}
		if(count($stripeImageArray)){
			$stripeArray['images'] = $stripeImageArray;
		}
		$stripeProduct = Stripe::products()->create($stripeArray);

		$product->stripe_id = $stripeProduct['id'];
		$success = $product->push();
	}catch(exception $e){
		$this->deleteProduct($product->id);
		$success = false;
		$stripeProduct = false;
	}

	return $success && $stripeProduct;
}

public function updateProduct($input, $image = null, $id){
	$public = public_path();
	$destinationPath ='/img/uploads/store/product/';

	$product = $this->model->with('photos')->find($id);
	$product->name = (is_null($input['name'])) ? $product->name : $input['name'];
	$product->part_id = (is_null($input['part_id'])) ? $product->part_id : $input['part_id'];
	$product->description = (is_null($input['description'])) ? $product->description : $input['description'];
	$product->short_description = (is_null($input['short_description'])) ? $product->short_description : $input['short_description'];
	$product->meta_description = (is_null($input['meta_description'])) ? $product->meta_description : $input['meta_description'];
	$product->meta_keywords = (is_null($input['meta_keywords'])) ? $product->meta_keywords : $input['meta_keywords'];
	$product->price = (is_null($input['price'])) ? $product->price : $input['price'];
	$product->stock = (is_null($input['stock'])) ? $product->stock : $input['stock'];
	$relatedIds = $product->category->lists('id');
	if(count($relatedIds)){
		$product->category()->detach($relatedIds);
	}

	if(!is_null($input['category']) && count($input['category'])){
		$product->category()->attach($input['category']);
	}
	if(!is_null($image)){

		$image->move($public.$destinationPath, $image->getClientOriginalName());
		if(is_null($product->photos) || $product->photos->count() == 0){
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$stripeImageArray = [ 'http://www.kegdata.com/'.$photos['path']];
			$photo = new Photo;
			$photo = $photo->create($photos);
			$product->photos()->attach($photo->id);
		}else{
			$relatedIds = $product->photos->lists('id');
			if(count($relatedIds)){
				$product->photos()->detach($relatedIds);
				$product->photos()->delete();
				$product->push();
			}

			$relatedIds = $product->photos->lists('id');
			if(count($relatedIds)){
				Photo::destroy($relatedIds);
			}

			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$stripeImageArray = [ 'http://www.kegdata.com'.$photos['path']];
			$photo = new Photo;
			$photo = $photo->create($photos);
			$product->photos()->attach($photo->id);
		}

	}else{
		$stripeImageArray = [ ];
	}
	$this->stripe = Stripe::make(env('stripe_key',''));
	
	$stripeProduct = $this->stripe->products()->update($product->stripe_id, [
		'name'		=> $product->name,
		'active'			=> true,
		'caption'		=> $product->short_description ? $product->short_description : "Caption not mention" ,//$product->short_description
		'description'	=>$product->description ? $product->description : "Description not mebtion" ,//$product->description
		'images'		=> $stripeImageArray,
		'metadata'		=> ['part_id' => $product->part_id],
		'shippable'		=> true,
	]);
																																																																																																																																										
	return $product->push();
}

public function deleteProduct($id){
	$product = $this->model->with('photos')->find($id);
	$relatedIds = $product->category->lists('id');
	if(count($relatedIds)){
		$product->category()->detach($relatedIds);
	}
	$relatedIds = $product->photos->lists('id');
	if(count($relatedIds)){
		$product->photos()->detach($relatedIds);
		$product->photos()->delete();
		$product->push();
		$photos = Photo::wherein('id',$relatedIds)->get();
		Photo::destroy($relatedIds);
	}


	if(!empty($product->stripe_id)){
		$stripeProduct = Stripe::products()->update($product->stripe_id, [
			'active' => false
		]);
	}

	return $product->delete();
}

}

?>
