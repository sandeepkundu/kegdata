<?php namespace App\KegData\Repositories;

class EloquentTimezoneRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Timezone';
protected $relationships = ['accountSetting'];

/**
* Public function to get timezone regions
* @return model
*/
public function getRegions(){
	return $this->model->distinct()->get(['region']);
}

/**
 * Public function to get timezones by regions
 * @param $region (String)
 * @return model
 */
public function getByRegion( $region ){
	$timezones = $this->model->where('region', '=', e($region))->orderBy('offset')->get();
	return $timezones;
}


}

?>