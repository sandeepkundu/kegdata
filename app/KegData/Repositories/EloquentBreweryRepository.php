<?php namespace App\KegData\Repositories;

class EloquentBreweryRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Brewery';
protected $relationships = ['breweryAddress', 'account', 'user', 'beer', 'kegDevice', 'mailListKey'];
protected $encryption = array('email1', 'email2','phone','fax');



}

?>