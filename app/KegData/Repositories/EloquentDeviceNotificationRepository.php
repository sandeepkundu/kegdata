<?php namespace App\KegData\Repositories;



class EloquentDeviceNotificationRepository extends BaseEloquentRepository{
	public $model = '\App\KegData\Models\Device_Notification';
	protected $relationships = ['account', 'kegdevice', 'notificationFrequency, notificationHistory'];

	public function createOrUpdate($input, $account_id){
		
		$notification = $this->model->firstOrNew(['id' => $input['id']]);
		$notification->fill($input);
		$notification->account_id = $account_id;
		$notification->kegdevice_id = $input['kegdevice_id'];
		if($input['range']) {
			$notification->operator = $input['range'];	
		}
		
		
		return $notification->save();
	}

	//Function for create notification is user select all keg for notification	
	public function createOrUpdateAll($input, $account_id,$mykegdevice ){
			
		$notification = $this->model->firstOrNew(['id' => $input['id']]);
		$notification->fill($input);
		$notification->account_id = $account_id;
		$notification->kegdevice_id = $mykegdevice->id;
		if($input['range']) {
			$notification->operator = $input['range'];	
		}
		
		
		return $notification->save();
	}

}

?>