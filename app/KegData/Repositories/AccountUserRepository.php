<?php namespace App\KegData\Repositories;

use App\KegData\Models\Account as Account;
use App\KegData\Models\AccountAddress as AccountAddress;
use App\KegData\Models\AccountSetting as AccountSetting;
use App\KegData\Models\Account_Type as Account_Type;
use App\KegData\Models\Brewery as Brewery;
use App\KegData\Models\Brewery_Address as Brewery_Address;
use App\KegData\Models\Distributor as Distributor;
use App\KegData\Models\Distributor_Address as Distributor_Address;
use App\KegData\Models\Role as Role;
use App\KegData\Models\User as User;
use App\KegData\Models\UserVerification as UserVerification;
use App\KegData\Models\Timezone as Timezone;
use GingerBread;
use Hash;
use Mail;
use Illuminate\Contracts\Auth\Guard;
use Config;

class AccountUserRepository{

	public function __construct(Guard $auth, Account $account, AccountAddress $accountAddress, AccountSetting $setting, Account_Type $accountType, Brewery $brewery, Brewery_Address $bAddress, Distributor $distributor, Distributor_Address $dAddress, Role $role, User $user, UserVerification $verification, GingerBread $gingerbread, Timezone $timezone){
		$this->auth = $auth;
		$this->account = $account;
		$this->accountAddress = $accountAddress;
		$this->setting = $setting;
		$this->accountType = $accountType;
		$this->brewery = $brewery;
		$this->bAddress = $bAddress;
		$this->distributor = $distributor;
		$this->dAddress = $dAddress;
		$this->role = $role;
		$this->user = $user;
		$this->verification = $verification;
		$this->gingerbread = $gingerbread;
		$this->timezone = $timezone;
	}

	public function create($input){
		$GingerBread = new GingerBread;

		$account = new Account;
		$account = $account->create(['accountName' => $input['accountName'], 
			'accountType' => $input['accountType'], 
			'accountPhone1' => $input['accountPhone1'],
			'accountPhone2' => $input['accountPhone2']]);

		$account->accountName = $input['accountName'];
		$account->accountType = $input['accountType'];
		$account->accountPhone1 = $input['accountPhone1'];
		$account->accountPhone2 = $input['accountPhone2'];
		
		$user = new User;
		$user = $user->create(array(
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'firstName' => $input['firstName'],
			'lastName' => $input['lastName']
		));
		$user->email = $input['email'];
		$user->password = Hash::make($input['password']);
		$user->firstName = $input['firstName'];
		$user->lastName = $input['lastName'];

		$user->isActive = 1;
		$user->isAdmin = 1;

		$user->save();


		$user->account()->associate($account);
		$user->role()->attach(['4']);
		$user->push();

		$userVerification = new UserVerification;
		$userVerification->verificationHash = md5(rand(1,1000));
		$userVerification = $user->userVerification()->save($userVerification);


		$accountAddress = new AccountAddress;
		$accountAddress->address1 = $input['address1'];
		$accountAddress->address2 = $input['address2'];
		$accountAddress->city = $input['city'];
		$accountAddress->stateCode = $input['stateCode'];
		$accountAddress->county = $input['county'];
		$accountAddress->countryCode = $input['countryCode'];
		$accountAddress->zipcode = $input['zipcode'];
		$accountAddress = $account->accountAddress()->save($accountAddress);


		$accountSetting = new AccountSetting;
		$accountSetting->currency = $input['currency'];
		$accountSetting = $account->accountSetting()->save($accountSetting);


		if(empty($input['brewDistID'])){
			switch($input['accountType']){
				case 'B':
					$brewery = Brewery::with(['brewery_address' => function($query) use ($input, $GingerBread){
						$query->where('address1','=',$GingerBread->preformEncrypt($input['address1']))->
						where('address2', '=', $GingerBread->preformEncrypt($input['address2']))->
						where('city', '=', $input['city'])->where('stateCode', '=', $input['stateCode'])->
						where('zipcode', '=', $input['zipcode']);
					}])->where('name', '=', $input['accountName'])->get();
					if(count($brewery) > 0 && $brewery->account_id == 0){
						$input['brewDistID'] = $brewery->id;
					}
				break;
				case 'D':
					$distributor = Distributor::with(['distributor_address' => function($query) use ($input, $GingerBread){
						$query->where('address1','=',$GingerBread->preformEncrypt($input['address1']))->
						where('address2', '=', $GingerBread->preformEncrypt($input['address2']))->
						where('city', '=', $input['city'])->where('stateCode', '=', $input['stateCode'])->
						where('zipcode', '=', $input['zipcode']);
					}])->where('name', '=', $input['accountName'])->get();

					if(count($distributor) > 0 && $distributor->account_id == 0){
						$input['brewDistID'] = $distributor->id;
					}
				break;
			}
		}

		switch($input['accountType']){
			case 'B':
				if(empty($input['brewDistID'])){
					$brewery = new Brewery;
					
					$brewery = $brewery->create(array(
						'name' => $input['accountName'],
						'primaryContactName' => $input['firstName'].' '.$input['lastName'],
						'phone' => $input['accountPhone1'],
						'fax' => $input['accountPhone2']
					));

					$brewery->name = $input['accountName'];
					$brewery->primaryContactName = $input['firstName'].' '.$input['lastName'];
					$brewery->phone = $input['accountPhone1'];
					$brewery->fax = $input['accountPhone2'];

					$brewery->user()->associate($user);
					$brewery->account()->associate($account);

					$brewery->save();
					
					$brewery_address = new Brewery_Address;
					$brewery_address->address1 = $input['address1'];
					$brewery_address->address2 = $input['address2'];
					$brewery_address->city = $input['city'];
					$brewery_address->stateCode = $input['stateCode'];
					$brewery_address->county = $input['county'];
					$brewery_address->zipcode = $input['zipcode'];
					$brewery_address->countryCode = $input['countryCode'];

					$brewery_address = $brewery->breweryAddress()->save($brewery_address);


				}else{
					$brewery = Brewery::find($input['brewDistID']);
					$brewery = $brewery->user()->associate($user);
					$brewery = $brewery->account()->associate($account);
				}
			break;
			case 'D':
 				if(empty($input['brewDistID'])){
 					$distributor = new Distributor;
					$distributor = $distributor->create(array(
						'name' => $input['accountName'],
						'primaryContactName' => $input['firstName'].' '.$input['lastName'],
						'phone' => $input['accountPhone1'],
						'fax' => $input['accountPhone2']
					));

					$distributor->name = $input['accountName'];
					$distributor->primaryContactName = $input['firstName'].' '.$input['lastName'];
					$distributor->phone = $input['accountPhone1'];
					$distributor->fax = $input['accountPhone2'];

					$distributor->user()->associate($user);
					$distributor->account()->associate($account);

					$distributor->save();
					
					$distributor_address = new Distributor_Address;
					$distributor_address->address1 = $input['address1'];
					$distributor_address->address2 = $input['address2'];
					$distributor_address->city = $input['city'];
					$distributor_address->stateCode = $input['stateCode'];
					$distributor_address->county = $input['county'];
					$distributor_address->zipcode = $input['zipcode'];
					$distributor_address->countryCode = $input['countryCode'];

					$distributor_address = $distributor->distributorAddress()->save($distributor_address);
 				}else{
					$distrubutor = Distrubutor::find($input['brewDistID']);
					$distrubutor = $distrubutor->user()->associate($user);
					$distrubutor = $distrubutor->account()->associate($account);
				}
			break;
		}

		$emailArray = array(
			'firstName' => $user->firstName, 
			'lastName' => $user->lastName, 
			'link' => 'http://www.kegdata.com/verification/'.$user->userVerification->verificationHash,  
			'email' => $user->email
		);

		Mail::send(['emails.welcome', 'emails.welcomeText'], $emailArray, function($message) use ($user){
    			$message->to($user->email)->bcc('latisha.mcneel@gmail.com')->subject('Welcome to KegData!');
		});


		return true;

	}

	public function destroyUser($id, $account_id){
		$what = $this->user->where('account_id', '=', $account_id);
		if($what->count() > 1){
			$which = $what->find($id);
			if($which->is_admin == 1){
				return 'User cannot be deleted while they are an admin of the account';
			}else{
				$which->isActive = 0;
				$which->save();
				return $which->delete();
			}
		}else{
			return 'Cannot delete the only user for the account';
		}
	}

	public function updateAddress($input, $id){
		$address = $this->accountAddress->find($id);
		$address->fill($input);
		return $address->save();
	}

	public function updateUser($input, $id){
		$user  = $this->user->find($id);
		$oldUser = $user;
		$user->email = $input['email'];
		if(!empty($input['password'])){
			$user->password = Hash::make($input['password']);
		}
		$user->firstName = $input['firstName'];
		$user->lastName = $input['lastName'];
		$updated = $user->save();

		$this->auth->login($user);

		if($updated && ($oldUser->email != $user->email)){
			$verify = $this->verification->firstOrNew($user->id);
			$verify->verificationConfirmation = null;
			$verify->verificationHash = md5(rand(1,1000));
			$verify->save();
			$emailArray = array(
				'firstName' => $user->firstName, 
				'lastName' => $user->lastName, 
				'link' => 'http://www.kegdata.com/verification/'.$user->userVerification->verificationHash,  
				'email' => $user->email
			);


			Mail::send(['emails.verification'], $emailArray, function($message) use ($user){
	    			$message->to($user->email)->bcc('latisha.mcneel@gmail.com')->subject('Welcome to KegData!');
			});
		}

		if($updated && ((!empty($input['password']) && $oldUser->password != $user->password) || $oldUser->email != $user->email)){
			Mail::send(['emails.loginchange'], null, function($message) use ($oldUser){
    			$message->to($oldUser->email)->bcc('latisha.mcneel@gmail.com', 'kevin@kegdata.com', 'lou@kegdata.com')->subject('Welcome to KegData!');
			});
		}

		return $updated;
	}

	public function empUpdateUser($input){
		$user  = $this->user->find($input['user_id']);
		$oldUser = $user;
		$user->email = $input['email'];
		$user->firstName = $input['firstName'];
		$user->lastName = $input['lastName'];
		$updated = $user->save();

		return $updated;
	}

	public function empUpdateUserHub($input){
	

		$user  = $this->user->find($input['user_id']);
		
		$oldUser = $user;
		$user->email = $input['email'];
		$user->firstName = $input['firstName'];
		$user->lastName = $input['lastName'];
		$updated = $user->save();
		
		return $updated;
		
	}
	/*
	Delete user when Delete from Hub

	*/
	public function destroyUserHub($id,$account_id){

		
		$what = $this->user->where('account_id', '=', $account_id);
		
		if($what->count() > 1){
			$which = $what->find($id);
			if($which->is_admin == 1){
				return 'User cannot be deleted while they are an admin of the account';
			}else{
				$which->isActive = 0;
				$which->save();
				return $which->delete();
			}
		}else{
			return 'Cannot delete the only user for the account';
		}
	}

	/*
	Delete user when Delete from Hub

	*/
	public function destroyUserHubDuplicate($id,$account_id){

		
		//$what = $this->user->where('account_id', '=', $account_id);
		$what = $this->user->where('id', '=', $id);

		if($what->count() > 0){
			$which = $what->find($id);
			
			if(($which->is_admin == 1) && ($which->account_id==$account_id) ){
				return 'User cannot be deleted while they are an admin of the account';
			}else{

				$which->isActive = 0;
				$which->save();
				return $which->delete();
			}
		}else{
			return 'Cannot delete the only user for the account';
		}
	}





	public function storeUser($input, $accountID){

		$user = $this->user->newInstance();
		$user->account_id = $accountID;
		$user->email = $input['userEmail'];
		$user->password = Hash::make($input['userPassword']);
		$user->firstName = $input['userFirstName'];
		$user->lastName = $input['userLastName'];
		$user->isActive = empty($input['isActive']) ? 0 : 1;
		$user->isAdmin = empty($input['isAdmin']) ? 0 : 1;

		$saved = $user->save();
		$user->role()->attach(['4']);
		$user->push();

		$userVerification = new UserVerification;
		$userVerification->verificationHash = md5(rand(1,1000));
		$userVerification = $user->userVerification()->save($userVerification);

		$emailArray = array(
			'firstName' => $user->firstName, 
			'lastName' => $user->lastName, 
			'link' => 'http://www.kegdata.com/verification/'.$user->userVerification->verificationHash,  
			'email' => $user->email
		);

		Mail::send('emails.newuser', $emailArray, function($message) use ($user){
    			$message->to($user->email)->subject('Welcome to KegData!');
		});
		return $saved;
	}

	public function updateAccount($input, $id) {
		$account = $this->account->find($id);
		$account->fill($input);
		$updated = $account->save();

		if($account->accountType == 'B'){
			$brewery = $this->brewery->where('account_id', '=', $id)->get()->first();
			$brewery->name = $account->accountName;
			$brewery->email1 = $account->accountEmail1;
			$brewery->email2 = $account->accountEmail2;
			$brewery->phone = $account->accountPhone1;
			$brewry->fax = $account->accountPhone2;
			$brewery->save();
		}

		if($account->accountType == 'D'){
			$distributor = $this->distributor->where('account_id', '=', $id)->get()->first();
			$distributor->name = $account->accountName;
			$distributor->email1 = $account->accountEmail1;
			$distributor->email2 = $account->accountEmail2;
			$distributor->phone = $account->accountPhone1;
			$distributor->fax = $account->accountPhone2;
			$distributor->save();
		}

		return $updated;
	}
}
