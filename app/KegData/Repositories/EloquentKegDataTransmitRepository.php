<?php namespace App\KegData\Repositories;

use Validator;
use Carbon\Carbon as Carbon;

use App\KegData\Models\HubDevice as HubDevice;
use App\KegData\Models\KegDevice as KegDevice;
use App\KegData\Models\Timezone as Timezone;
use App\KegData\Models\AccountSetting;
use App\KegData\Models\Device_Notification;
use App\KegData\Models\EmailTemplate;
use Mail;

use App\KegData\Repositories\EloquentKegDataRepository as KegData;

class EloquentKegDataTransmitRepository extends BaseEloquentRepository{
	protected $model = '\App\KegData\Models\KegData_Transmit';
	protected $relationships = ['hubdevice', 'kegdevice'];
	protected $encryption = array();

	public function addKegData($input){
		$hub =  HubDevice::firstOrCreate(['hubMac' => $input['hubmac']]);
		$keg = KegDevice::firstOrCreate(['kegMac' => $input['kegmac'], 'account_id' => $hub->account_id]);
		// save data first in transmit table
		$return_data=$this->model->create($input);
	//	$input['status']='1001101';
		$status = $input['status'];
		
		//$status = '101';
		//Calling function to check tempreture notification alert
		if(($status == '1001' || $status == '11')) {
			$this->fetch_tempreture_for_notification($input,$keg);
			//calling function to check level if there is pour event
			if( $status == '11' ) {
				// calling function to fetch level
				$this->fetch_level_for_notification($input,$keg);
				//calling function to check empty pour	
				$this->fetch_empty_level_for_notification($input,$keg);
				//calling function to check after hours pour	
				$this->fetch_after_hours_notification($input,$keg);

			}
			
		}

		//Calling function to check keg blow event
		if(($status == '10011')) {
			
			$this->fetch_for_keg_blow($input,$keg);

		}

		//calling function to check untap event 101
		if(($status == '101')) {
			
		 $respone = $this->fetch_untap($input,$keg);
			
		}

		//calling function to check tapped 1001
		if(($status == '1001')) {
			
			$this->fetch_for_tapped($input,$keg);

		}

		//calling function to check Keg Boot up
		if(($status == '1000000' || $status == '1000101' || $status == '100100'|| $status == '1001101' )) {

			$respone = $this->fetch_boot($input,$keg);
			
		}

		//calling function to check pour event 
		/*if(($status == '11')) {
			$respone = $this->fetch_pour($input,$keg);
		}*/

		return $return_data;
	}

	/**
	* function to check keg blow event notificaation alert
	*/

	public function fetch_for_keg_blow($input = null, $keg = null) {

		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='K';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];

		if($account_id) {
		//fetch user info
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$user_info = $accountSettings->account->user->first();
			$user_id = $user_info['id'];

		//fetch email_alert
		$email_alert = $accountSettings->account->email_alert;
		$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

		//send instant notification
		$cur_date = Carbon::now();
		$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	
		

		//fetch sms_alert
			$sms_alert = $accountSettings->account->sms_alert;
			$cc_1 = $accountSettings->account->optional_1;
			$cc_2 = $accountSettings->account->optional_2;

		//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

			if($check_device_exist_in_notification) { 

				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
				$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];

				if($email_notification || $email_notification_optional || $email_notification_optional_2) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					$EmailTemplate->content = "keg Blow Alert! You are being notified that your $device_name alert was triggered at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "keg Blow Alert!";
					$EmailTemplate->cron_name = "keg_blow_via_api";

					$EmailTemplate->type = 'email';
					$EmailTemplate->device_id = $keg->id;
					$t = $EmailTemplate->save();
					echo "====>keg blow mail saved";
				}

				if($sms_notification) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					
					$EmailTemplate->content = "$device_name Alert: Keg blown at $cureent_date.";

					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "keg Blow Alert!";
					$EmailTemplate->cron_name = "keg_blow_via_api";
					$EmailTemplate->device_id = $keg->id;
					$EmailTemplate->type = 'sms';
					$EmailTemplate->save();
					echo "====>keg blow sms saved";
				}


		}

	}

	}



	/**
	* function to check tempreture notificaation alert
	*/

	public function fetch_tempreture_for_notification($input = null, $keg = null) {

		$temperature_device = $input['temperature'];
		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;
		$notification='T';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];
		if(($temperature_device) && ($status == '1001' || $status == '11') ) {

			if($account_id) {
			//fetch account setting
				$accountSettings=AccountSetting::where('account_id', $account_id)->first();


				$user_email = $accountSettings->account->email_alert;
				$user_sms = $accountSettings->account->sms_alert;
				$cc_1 = $accountSettings->account->optional_1;
				$cc_2 = $accountSettings->account->optional_2;

				$user_info = $accountSettings->account->user->first();
				$user_id = $user_info['id'];
				$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);
				$temeratureType = ($accountSettings['temperatureType']) ? $accountSettings['temperatureType']:'C';

				if($temeratureType) {

				//check that if notification set on recent pour device
					$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();
					//dd($check_device_exist_in_notification);



					if($check_device_exist_in_notification) {

						$value=$check_device_exist_in_notification['value'];
						$operator=$check_device_exist_in_notification['operator'];
						$sms_notification=$check_device_exist_in_notification['sms_notification'];
						$email_notification=$check_device_exist_in_notification['email_notification'];
						$frequency=$check_device_exist_in_notification['frequency'];
						$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
						$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];




					//fetch temperature
						$fetched_tempreture = $kegData->getTemp($temperature_device, $accountSettings->temperatureType, $keg->tempOffset);
						$temp_string = $fetched_tempreture." °".$temeratureType;



						// query to get last inserted record date time for email
						$emailData = EmailTemplate::select('created_at')->where('cron_name','TemperatureAPI')->where('type','email')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();


						$is_insert_in_table_for_email= 0;
						if($emailData){
							$date = Carbon::parse($emailData->created_at);
							$now = Carbon::now();
							$diffForEmail = $date->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_email = $this->check_time_interval($diffForEmail,$frequency);

						} else { // if comes first times
							$is_insert_in_table_for_email = 1;

						}



						//query to get last inserted record date time for sms
						$smsData = EmailTemplate::select('created_at')->where('cron_name','TemperatureAPI')->where('type','sms')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();

						$is_insert_in_table_for_sms = 0;
						if($smsData){
							$smsdate = Carbon::parse($smsData->created_at);
							$now = Carbon::now();
							$diffForSms = $smsdate->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_sms = $this->check_time_interval($diffForSms,$frequency);

						} else{ // if comes first time when email_template table is blank
							$is_insert_in_table_for_sms = 1;
						}

						//echo "----------->".$is_insert_in_table_for_sms.'<-------------';
						//echo "----------->".$is_insert_in_table_for_email.'<-------------';

						if($operator == '>') {

							if($fetched_tempreture > $value) {
								$cur_date = Carbon::now();
								$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	


								if($email_notification || $email_notification_optional || $email_notification_optional_2) {

								// save into email template
									
									$EmailTemplate = new EmailTemplate;
									$EmailTemplate->user_id = $user_id;

									$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date with Temperature $temp_string. You are being sent this notification according to your settings under the notifications settings at KegData.";

									$EmailTemplate->status = 0;
									$EmailTemplate->type = 'email';
									$EmailTemplate->subject = "Temperature Alert!";
									$EmailTemplate->cron_name = "TemperatureAPI";
									$EmailTemplate->device_id = $keg->id;

									if( ($is_insert_in_table_for_email == 1)) { 
										//calling the function to send email
										$EmailTemplate->save();
										echo '====>mail send====';

									}

								} 

								if($sms_notification == '1') {
									

							// save into email template

									$EmailTemplate = new EmailTemplate;
									$EmailTemplate->user_id = $user_id;
									$EmailTemplate->content = "$device_name Alert: $temp_string at $cureent_date.";						

									$EmailTemplate->status = 0;
									$EmailTemplate->type = 'sms';
									$EmailTemplate->subject = "Temperature Alert!";
									$EmailTemplate->cron_name = "TemperatureAPI";
									$EmailTemplate->device_id = $keg->id;
									if( ($is_insert_in_table_for_sms == 1)) { 
										$EmailTemplate->save();
										echo '====>sms send=====';
										
									}

									

								} 

								return true;
							} 
						} else if($operator == '<') { // else set < lesss then

							if($value > $fetched_tempreture) {

								$cur_date = Carbon::now();
								$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	


								if($email_notification || $email_notification_optional || $email_notification_optional_2) {
									
								// save into email template
									// $cureent_date =date('M d, Y H:i a');


									$EmailTemplate = new EmailTemplate;
									$EmailTemplate->user_id = $user_id;

									$EmailTemplate->content = "Temperature Alert! You are being notified that your $device_name alert was triggered at $cureent_date with Temperature $temp_string. You are being sent this notification according to your settings under the notifications settings at KegData.";

									$EmailTemplate->status = 0;
									$EmailTemplate->type = 'email';
									$EmailTemplate->subject = "Temperature Alert!";
									$EmailTemplate->cron_name = "TemperatureAPI";
									$EmailTemplate->device_id = $keg->id;

									//echo '===>'.$is_insert_in_table_for_email;
									if( ($is_insert_in_table_for_email == 1)) { 

										//calling the function to send email
										$t = $EmailTemplate->save();
										echo '====>mail send====';
										


									}



								} 

								if($sms_notification) {
									
								// save into email template
								//	$cureent_date =date('M d, Y H:i a');

									$EmailTemplate = new EmailTemplate;
									$EmailTemplate->user_id = $user_id;

									$EmailTemplate->content = "$device_name Alert: $temp_string at $cureent_date.";						

									$EmailTemplate->status = 0;
									$EmailTemplate->type = 'sms';
									$EmailTemplate->subject = "Temperature Alert!";
									$EmailTemplate->cron_name = "TemperatureAPI";
									$EmailTemplate->device_id = $keg->id;
									if(($is_insert_in_table_for_sms == 1)) { 
										$EmailTemplate->save();
										echo '====>sms send=====';
										
									}



								} 

								return true;

							}
						}


						

					}

					

				}
				
			}

		}


	}


	/**
	* function return true if minutes difference matched from frequency
	*/

	public function check_time_interval($diffForEmail,$frequency) {




		if($frequency == '8') { // if frequency is set for 4 hours
			if($diffForEmail > 240) {
				return 1;
			} else {
				return 0;
			}
			
		} else if($frequency == '9') { // if frequency is set for 8 hours
			if($diffForEmail > 480) {
				return 1;
			} else {
				return 0;
			}
		} else if($frequency == '11') { // if frequency is set for once a day
			if($diffForEmail > 1440) {
				return 1;
			} else {
				return 0;
			}
		}else if($frequency == '16') { // if frequency is set for instant notification	
			return 1;
		} else {
			return 0;
		}


	}

	/**
	* function to check level for  notificaation alert
	*/

	public function fetch_level_for_notification($input = null, $keg = null) {


		$temperature_device = $input['temperature'];
		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='L';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];
		
		if($status == '11' ) {

			if($account_id) {
			//fetch account setting
				$accountSettings=AccountSetting::where('account_id', $account_id)->first();
				$volumeType=$accountSettings['volumeType'];

				$recent_pour_device_id=$keg->recentPour->deviceID;
				
				$user_email = $accountSettings->account->email_alert;
				$user_sms = $accountSettings->account->sms_alert;
				$cc_1 = $accountSettings->account->optional_1;
				$cc_2 = $accountSettings->account->optional_2;

				$user_info = $accountSettings->account->user->first();
				$user_id = $user_info['id'];
				$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

				
				//check that if notification set on recent pour device
				$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

				if($check_device_exist_in_notification) {

					$notification_id=$check_device_exist_in_notification['id'];
					$value=$check_device_exist_in_notification['value'];
					$frequency=$check_device_exist_in_notification['frequency'];
					$operator=$check_device_exist_in_notification['operator'];
					$sms_notification=$check_device_exist_in_notification['sms_notification'];
					$email_notification=$check_device_exist_in_notification['email_notification'];
					$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];
					$is_level_set=$check_device_exist_in_notification['is_level_set'];

					$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
					$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];
					//dd($is_level_set);
					//fetch level
					
					$adc_pour_end = $input['adc_pour_end'];
					$amount = isset($keg->kegtype->amount) ? $keg->kegtype->amount : 1984;
					$empty = isset($keg->kegtype->emptyValue) ? $keg->kegtype->emptyValue: 2252 ;

					$ounces =  $kegData->getOunces($adc_pour_end, $keg->calibration, $amount,$empty, $volumeType);

					
					if($ounces  < 0){
						$ounces = 0;
					}
					$total = $ounces.' '.$volumeType;

					$percentage =  $kegData->kegPercent($ounces,$amount);
				
					
					if($operator == '<') {

						// query to get last inserted record date time for email
						$emailData = EmailTemplate::select('created_at')->where('cron_name','LowLevelAPI')->where('type','email')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();

						$is_insert_in_table_for_email= 0;
						if($emailData){

							$date = Carbon::parse($emailData->created_at);
							$now = Carbon::now();
							$diffForEmail = $date->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_email = $this->check_time_interval($diffForEmail,$frequency);
						} else { // if comes first times
							$is_insert_in_table_for_email = 1;
						}

						// query to get last inserted record date time for email
						$smsData = EmailTemplate::select('created_at')->where('cron_name','LowLevelAPI')->where('type','sms')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();
						$is_insert_in_table_for_sms = 0;
						if($smsData){
							$smsdate = Carbon::parse($smsData->created_at);
							$now = Carbon::now();
							$diffForSms = $smsdate->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_sms = $this->check_time_interval($diffForSms,$frequency);
						} else{ // if comes first time
							$is_insert_in_table_for_sms = 1;
						}
						
						if($value > $percentage){

							$cur_date = Carbon::now();
							$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');

							$temp_var=0;
							if($email_notification || $email_notification_optional || $email_notification_optional_2){
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'email';
								$EmailTemplate->subject = "Low Level Alert!";
								$EmailTemplate->cron_name = "LowLevelAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_email == 1 && $is_level_set != 1) {
									$EmailTemplate->save();
									$temp_var=1;
									echo '====>mail send====';		
								}
							}
							if($sms_notification == 1){

								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								// $EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";
								$EmailTemplate->content = "$device_name Alert: Level has reached $percentage% at $current_date.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'sms';
								$EmailTemplate->subject = "Low Level Alert!";
								$EmailTemplate->cron_name = "LowLevelAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_sms == 1 && $is_level_set != 1) { 
									$EmailTemplate->save();
									$temp_var=1;
									echo "========>SMS Send======";
								}	


							}// Close SMS Notification

							//if this variable is set then update notification table column
							if($temp_var) {
								Device_Notification::where('id', $notification_id) ->update(['is_level_set' => 1]);
							}



							return true;
						}
						
					}
				}
			}
		}
	}


	/**
	* function to check empty level for  notificaation alert
	*/

	public function fetch_empty_level_for_notification($input = null, $keg = null) {


		$temperature_device = $input['temperature'];
		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='E';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];
		
		if($status == '11' ) {

			if($account_id) {
			//fetch account setting
				$accountSettings=AccountSetting::where('account_id', $account_id)->first();
				$volumeType=$accountSettings['volumeType'];

				$recent_pour_device_id=$keg->recentPour->deviceID;
				
				$user_email = $accountSettings->account->email_alert;
				$user_sms = $accountSettings->account->sms_alert;
				$cc_1 = $accountSettings->account->optional_1;
				$cc_2 = $accountSettings->account->optional_2;

				$user_info = $accountSettings->account->user->first();
				$user_id = $user_info['id'];
				$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

				
				//check that if notification set on recent pour device
				$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

				if($check_device_exist_in_notification) {

					$notification_id=$check_device_exist_in_notification['id'];
					$value=$check_device_exist_in_notification['value'];
					$frequency=$check_device_exist_in_notification['frequency'];
					$operator=$check_device_exist_in_notification['operator'];
					$sms_notification=$check_device_exist_in_notification['sms_notification'];
					$email_notification=$check_device_exist_in_notification['email_notification'];
					$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
					$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];
					$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];
					$is_level_set=$check_device_exist_in_notification['is_level_set'];
					//dd($is_level_set);
					//fetch level
					
					$adc_pour_end = $input['adc_pour_end'];
					$amount = isset($keg->kegtype->amount) ? $keg->kegtype->amount : 1984;
					$empty = isset($keg->kegtype->emptyValue) ? $keg->kegtype->emptyValue: 2252 ;

					$ounces =  $kegData->getOunces($adc_pour_end, $keg->calibration, $amount,$empty, $volumeType);

					
					if($ounces  < 0){
						$ounces = 0;
					}
					$total = $ounces.' '.$volumeType;

					$percentage =  $kegData->kegPercent($ounces,$amount);
					
					
					if(empty($percentage)) {

						// query to get last inserted record date time for email
						$emailData = EmailTemplate::select('created_at')->where('cron_name','EmptyLevelAPI')->where('type','email')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();

						$is_insert_in_table_for_email= 0;
						if($emailData){

							$date = Carbon::parse($emailData->created_at);
							$now = Carbon::now();
							$diffForEmail = $date->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_email = $this->check_time_interval($diffForEmail,$frequency);
						} else { // if comes first times
							$is_insert_in_table_for_email = 1;
						}

						// query to get last inserted record date time for email
						$smsData = EmailTemplate::select('created_at')->where('cron_name','EmptyLevelAPI')->where('type','sms')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();
						$is_insert_in_table_for_sms = 0;
						if($smsData){
							$smsdate = Carbon::parse($smsData->created_at);
							$now = Carbon::now();
							$diffForSms = $smsdate->diffInMinutes($now);
							//check that data insert into the email_templates table
							$is_insert_in_table_for_sms = $this->check_time_interval($diffForSms,$frequency);
						} else{ // if comes first time
							$is_insert_in_table_for_sms = 1;
						}
						

							$cur_date = Carbon::now();
							$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');

							
							if($email_notification || $email_notification_optional || $email_notification_optional_2){
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "Empty Keg Alert! You are being notified that your $device_name Empty Keg Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'email';
								$EmailTemplate->subject = "Empty Keg Alert!";
								$EmailTemplate->cron_name = "EmptyLevelAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_email == 1) {
									$EmailTemplate->save();
									
									echo '====>mail send====';		
								}
							}
							if($sms_notification == 1){

								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								// $EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";
								$EmailTemplate->content = "$device_name Empty Keg Alert: Keg reached $percentage% at $current_date.";
								


								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'sms';
								$EmailTemplate->subject = "Empty Keg Alert!";
								$EmailTemplate->cron_name = "EmptyLevelAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_sms == 1) { 
									$EmailTemplate->save();
									echo "========>SMS Send======";
								}	


							}// Close SMS Notification



							return true;
						
					}
				}
			}
		}
	}


	/**
	* function to after hours notificaation alert
	*/

	public function fetch_after_hours_notification($input = null, $keg = null) {
		

		$temperature_device = $input['temperature'];
		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='P';

		$status = $input['status'];
		
			
		if($status == '11' ) {

			if($account_id) {
			//fetch account setting
				$accountSettings=AccountSetting::where('account_id', $account_id)->first();
				$volumeType=$accountSettings['volumeType'];
			
				$recent_pour_device_id=$keg->recentPour->deviceID;
				
				$user_email = $accountSettings->account->email_alert;
				$user_sms = $accountSettings->account->sms_alert;
				$cc_1 = $accountSettings->account->optional_1;
				$cc_2 = $accountSettings->account->optional_2;

				$user_info = $accountSettings->account->user->first();
				$user_id = $user_info['id'];
				$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);
				
				//check that if notification set on recent pour device
				$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

				

				if($check_device_exist_in_notification) {

					$input_data=$input['sent_at'];

					$notification_id=$check_device_exist_in_notification['id'];
					$value=$check_device_exist_in_notification['value'];
					$frequency=$check_device_exist_in_notification['frequency'];
					$operator=$check_device_exist_in_notification['operator'];
					$sms_notification=$check_device_exist_in_notification['sms_notification'];

					

					$email_notification=$check_device_exist_in_notification['email_notification'];

					$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];

					$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];

					$kegdevice_id=$check_device_exist_in_notification['kegdevice_id'];
					//dd($is_level_set);
					//fetch level
					
					if($input_data) {
						
						//dd($dt->hour);  

						$dayStart = isset( $accountSettings['dayStart'] ) ?  Carbon::parse($accountSettings['dayStart'])->hour : null;

						$dayEnd = isset( $accountSettings['dayEnd'] ) ?  Carbon::parse($accountSettings['dayEnd'])->hour : null;

						$post_data_date_converted=$input_data->timezone($timezone->timezone)->format('H');
						//$post_data_date_converted= 1;

						if( ($post_data_date_converted < $dayStart ) || ($post_data_date_converted > $dayEnd ) ) {
							
							// query to get last inserted record date time for email
							$emailData = EmailTemplate::select('created_at')->where('cron_name','AfterHourAPI')->where('type','email')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();

							$is_insert_in_table_for_email= 0;
							if($emailData){

								$date = Carbon::parse($emailData->created_at);
								$now = Carbon::now();
								$diffForEmail = $date->diffInMinutes($now);
								//check that data insert into the email_templates table
								$is_insert_in_table_for_email = $this->check_time_interval($diffForEmail,$frequency);
							} else { // if comes first times
								$is_insert_in_table_for_email = 1;
							}


							// query to get last inserted record date time for email
							$smsData = EmailTemplate::select('created_at')->where('cron_name','AfterHourAPI')->where('type','sms')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();
							$is_insert_in_table_for_sms = 0;
							if($smsData){
								$smsdate = Carbon::parse($smsData->created_at);
								$now = Carbon::now();
								$diffForSms = $smsdate->diffInMinutes($now);
								//check that data insert into the email_templates table
								$is_insert_in_table_for_sms = $this->check_time_interval($diffForSms,$frequency);
							} else{ // if comes first time
								$is_insert_in_table_for_sms = 1;
							}

							$cur_date = Carbon::now();
							$current_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');

							
							if($email_notification || $email_notification_optional || $email_notification_optional_2){
									
								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								$EmailTemplate->content = "After Hour Alert! You are being notified that your $device_name conducted a pour after hour at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'email';
								$EmailTemplate->subject = "After Hour Alert!";
								$EmailTemplate->cron_name = "AfterHourAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_email == 1) {
									$EmailTemplate->save();
									
									echo '====>mail send====';		
								}
							}
							if($sms_notification == 1){

								$EmailTemplate = new EmailTemplate;
								$EmailTemplate->user_id = $user_id;

								// $EmailTemplate->content = "Low Level Alert! You are being notified that your $device_name Low Level Alert was triggered at $current_date. You are being sent this notification according to your settings under the notifications settings at KegData.";
								$EmailTemplate->content = "$device_name Alert: After hour pour at $current_date.";
								


								$EmailTemplate->status = 0;
								$EmailTemplate->type = 'sms';
								$EmailTemplate->subject = "After Hour Alert!";
								$EmailTemplate->cron_name = "AfterHourAPI";
								$EmailTemplate->device_id = $keg->id;
								if($is_insert_in_table_for_sms == 1) { 
									$EmailTemplate->save();
									echo "========>SMS Send======";
								}	


							}// Close SMS Notification
							
							return true;
						}

							
						
					}
				}
			}
		}
	}


	/**
	* function to check untap event notificaation alert
	*/

	public function fetch_untap($input = null, $keg = null) {

		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='U';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];

		if($account_id) {
		//fetch user info
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$user_info = $accountSettings->account->user->first();
			$user_id = $user_info['id'];

		//fetch email_alert
		$email_alert = $accountSettings->account->email_alert;
		$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

		//send instant notification
		$cur_date = Carbon::now();
		$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	
		

		//fetch sms_alert
			$sms_alert = $accountSettings->account->sms_alert;
			$cc_1 = $accountSettings->account->optional_1;
			$cc_2 = $accountSettings->account->optional_2;


			//if this variable is set then update notification table column for level
			//check that if notification set for level
			$check_notification_exist_for_level=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>'L','deleted_at'=>null])->first();
			if($check_notification_exist_for_level) { 
				$notification_id=$check_notification_exist_for_level['id']; 
				Device_Notification::where('id', $notification_id)->update(['is_level_set' => 0]);
			}
			//end level update code
			




		//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

			if($check_device_exist_in_notification) { 

				$notification_id_untap =$check_device_exist_in_notification['id'];
				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
				$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];
				$frequency=$check_device_exist_in_notification['frequency'];
				$is_untap_set=$check_device_exist_in_notification['is_untap_set'];
						
				
				$temp_var=0;
				if($email_notification || $email_notification_optional || $email_notification_optional_2) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					$EmailTemplate->content = "Your Keg was Untapped. You are being notified that your $device_name has been untapped at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "Untapped Alert!";
					$EmailTemplate->cron_name = "keg_untap_via_api";

					$EmailTemplate->type = 'email';
					$EmailTemplate->device_id = $keg->id;
					if($is_untap_set != 1){
						$t = $EmailTemplate->save();
						echo "====>untap mail saved";
						$temp_var = 1;
					}
				}

				if($sms_notification) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					
					$EmailTemplate->content = "$device_name Alert: Untapped at $cureent_date.";
					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "Untapped Alert!";
					$EmailTemplate->cron_name = "keg_untap_via_api";
					$EmailTemplate->device_id = $keg->id;
					$EmailTemplate->type = 'sms';
					if($is_untap_set != 1){
						$EmailTemplate->save();
						echo "====>untap sms saved";
						$temp_var = 1;
					}
				}
				// Update Device nootification table set is_untap_set is 1 
				if($temp_var) {
					Device_Notification::where('id', $notification_id_untap) ->update(['is_untap_set' => 1]);
				}



		}

	}

	}



	/**
	* function to check boot event notificaation alert
	*/

	public function fetch_boot($input = null, $keg = null) {

		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='B';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];

		if($account_id) {
		//fetch user info
			$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			$user_info = $accountSettings->account->user->first();
			$user_id = $user_info['id'];

		//fetch email_alert
		$email_alert = $accountSettings->account->email_alert;
		$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

		//send instant notification
		$cur_date = Carbon::now();
		$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	
		

		//fetch sms_alert
			$sms_alert = $accountSettings->account->sms_alert;
			$cc_1 = $accountSettings->account->optional_1;
			$cc_2 = $accountSettings->account->optional_2;

		//check that if notification set on recent pour device
			$check_device_exist_in_notification=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();

			if($check_device_exist_in_notification) { 

				$sms_notification=$check_device_exist_in_notification['sms_notification'];
				$email_notification=$check_device_exist_in_notification['email_notification'];
				$email_notification_optional=$check_device_exist_in_notification['email_notification_optional'];
				$email_notification_optional_2=$check_device_exist_in_notification['email_notification_optional_2'];
				$frequency=$check_device_exist_in_notification['frequency'];

				if($email_notification || $email_notification_optional || $email_notification_optional_2) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					$EmailTemplate->content = "Your Keg was boot up. You are being notified that your $device_name has been boot up at $cureent_date. You are being sent this notification according to your settings under the notifications settings at KegData.";

					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "Keg boot up Alert!";
					$EmailTemplate->cron_name = "boot_via_api";

					$EmailTemplate->type = 'email';
					$EmailTemplate->device_id = $keg->id;
					$t = $EmailTemplate->save();
					echo "====>boot up mail saved";
				}

				if($sms_notification) {
					$EmailTemplate = new EmailTemplate;
					$EmailTemplate->user_id = $user_id;
					
					$EmailTemplate->content = "$device_name Alert: Boot up at $cureent_date.";
					$EmailTemplate->status = 0;
					$EmailTemplate->subject = "Keg boot up Alert!";
					$EmailTemplate->cron_name = "boot_via_api";
					$EmailTemplate->device_id = $keg->id;
					$EmailTemplate->type = 'sms';
					$EmailTemplate->save();
					echo "====>boot up sms saved";
				}


		}

	}

	}

	/**
	* function to check keg pour event notificaation alert
	*/

	public function fetch_pour($input = null, $keg = null) {

		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='P';
		//only Once every 4 hours
		$frequency='8';
		
		$status = $input['status'];

		//fetch user info
		$accountSettings=AccountSetting::where('account_id', $account_id)->first();
		$user_info = $accountSettings->account->user->first();
		$user_id = $user_info['id'];

		//fetch email_alert
		$email_alert = $accountSettings->account->email_alert;
		$email_optional_alert = $accountSettings->account->optional_1;
		$email_optional_alert_2 = $accountSettings->account->optional_2;
		$timezone = ($accountSettings['timezone'] == 0 ) ? Timezone::where('timezone', '=', 'UTC')->first() : Timezone::find($accountSettings['timezone']);

		//send instant notification
		$cur_date = Carbon::now();
		$cureent_date=$cur_date->timezone($timezone->timezone)->format('M d, Y H:i a');	



		//fetch sms_alert
		$sms_alert = $accountSettings->account->sms_alert;
		$cc_1 = $accountSettings->account->optional_1;
		$cc_2 = $accountSettings->account->optional_2;

		// query to get last inserted record date time for email
		$emailData = EmailTemplate::select('created_at')->where('cron_name','keg_pour_via_api')->where('type','email')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();

		if($emailData){

			$date = Carbon::parse($emailData->created_at);
			$now = Carbon::now();
			$diffForEmail = $date->diffInMinutes($now);
		} else { // if comes first times
			$diffForEmail = 250;
		}

		//query to get last inserted record date time for sms
		$smsData = EmailTemplate::select('created_at')->where('cron_name','keg_pour_via_api')->where('type','sms')->where('device_id',$keg->id)->orderBy('id', 'desc')->first();
		if($smsData){
			$smsdate = Carbon::parse($smsData->created_at);
			$now = Carbon::now();
			$diffForSms = $smsdate->diffInMinutes($now);
			} else{ // if comes first time
				$diffForSms = 250;
			}

			if($email_alert || $email_optional_alert || $email_optional_alert_2) {
				$EmailTemplate = new EmailTemplate;
				$EmailTemplate->user_id = $user_id;
				$EmailTemplate->content = "You are being notified that your $device_name has shown a pour after your business hours. You are being sent this notification according to your settings under the notifications settings at KegData.";

				$EmailTemplate->status = 0;
				$EmailTemplate->subject = "After Hours pour alert!";
				$EmailTemplate->cron_name = "keg_pour_via_api";

				$EmailTemplate->type = 'email';
				$EmailTemplate->device_id = $keg->id;

				if($diffForEmail > 240) { 
					$t = $EmailTemplate->save();
					echo '====>mail send====';
				}
			}

			if($sms_alert) {
				$EmailTemplate = new EmailTemplate;
				$EmailTemplate->user_id = $user_id;
				// $EmailTemplate->content = "You are being notified that your $device_name has shown a pour after your business hours. You are being sent this notification according to your settings under the notifications settings at KegData.";
				$EmailTemplate->content = "$device_name Alert: Pour after your business hours.";

				$EmailTemplate->status = 0;
				$EmailTemplate->subject = "After Hours pour alert!";
				$EmailTemplate->cron_name = "keg_pour_via_api";
				$EmailTemplate->device_id = $keg->id;
				$EmailTemplate->type = 'sms';
				if($diffForSms > 240) { 
					$EmailTemplate->save();
					echo '====>sms send=====';
				}
			}


		}

	/*
	* Function for send email and sms
	*/
	public function send_instant_email($user_email = null, $cc_1 = null ,$cc_2 = null , $subject = null, $content = null) {

		Mail::raw($content, function($message) use ($subject,$content,$cc_1,$cc_2,$user_email)
		{
			$message->from('accounts@kegdata.com', 'Kegdata notification');

			$message->to($user_email);
			$message->subject($subject);
			if($cc_1) {
				$message->cc($cc_1);
			}
			if($cc_2) {
				$message->cc($cc_2);
			}


		});

		return count(Mail::failures());

	}

	public function send_instant_sms($content,$sender_number) {

		if(strlen($content) > 160) {
			$content = substr($content,0,160);
		}

		//echo "=====>".$sender_number;
		# Plivo AUTH ID
		$AUTH_ID = 'MAYTG0ODGZNDQ0YJGZMW';
		# Plivo AUTH TOKEN
		$AUTH_TOKEN = 'YzE3Njg0YjMyOTJkZTJiMGNhMjMzZDJmMWU2ODIx';
		# SMS sender ID.
		//$src = '18323108065';//we need src 832-310-8065. 
		$src = '18063059022';
		# SMS destination number
		$dst = $sender_number;//919457624855 - $sender_number
		# SMS text
		$text = $content;
		$url = 'https://api.plivo.com/v1/Account/'.$AUTH_ID.'/Message/';
		$data = array("src" => "$src", "dst" => "$dst", "text" => "$text");
		$data_string = json_encode($data);
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_USERPWD, $AUTH_ID . ":" . $AUTH_TOKEN);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$response = curl_exec( $ch );
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//if 202 or 200 then sms send
		//echo "===========>".$httpcode;
		return $httpcode;

		
	}


	/**
	* @method - parseContent
	* Parses content from KegDevices
	* HubMac|DateTime|KegMac|adc_pour_start|adc_pour|adc_pour_end|pour_length|temperature|status|PHP_EOL
	* @var - $input (String)
	* @return - $message
	*/
	public function parseContent($input){
		$lines = explode(PHP_EOL, $input);
		$message = '';
		$dateFormat = 'YmdGis';
		foreach($lines as $line){
			$exp = explode('|', $line);
			if(count($exp) == 10){
				$data['hubmac'] = $exp[0];
				$data['kegmac'] = $exp[2];
				$data['adc_pour_start'] = $exp[3];
				$data['adc_pour'] = $exp[4];
				$data['adc_pour_end'] = $exp[5];
				$data['pour_length'] = $exp[6];
				$data['temperature'] = $exp[7];
				$data['status'] = $exp[8];
				$data['sent_at'] = $exp[1];

				$v = Validator::make($data, [
					'hubmac' => array('required', 'regex:/[0-9a-fA-F]{12}/'),
					'kegmac' => array('required', 'regex:/[0-9a-fA-F]{12}/'),
					'adc_pour_start' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'adc_pour' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'adc_pour_end' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'pour_length' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'temperature' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'status' => array('required', 'regex:/[0-9a-fA-F]{8}/'),
					'sent_at' => array('required', 'date_format:'.$dateFormat)
					]);
				if($v->fails()){
					$message .= $line.'---- '.$v->errors().PHP_EOL;
				}else{
					$adc_pour_start = unpack("l", pack("l",hexdec($data['adc_pour_start'])));
					$adc_pour = unpack("l", pack("l",hexdec($data['adc_pour'])));
					$adc_pour_end = unpack("l", pack("l",hexdec($data['adc_pour_end'])));
					$pour_length = unpack("l", pack("l",hexdec($data['pour_length'])));
					$temperature = unpack("l", pack("l",hexdec($data['temperature'])));

					$data['adc_pour_start'] = reset($adc_pour_start);
					$data['adc_pour'] = reset($adc_pour);
					$data['adc_pour_end'] = reset($adc_pour_end);
					$data['pour_length'] = reset($pour_length);
					$data['temperature'] = reset($temperature);
					$data['status'] = decbin(hexdec($data['status']));

					$data['sent_at'] = Carbon::createFromFormat($dateFormat, $data['sent_at']);
					$success = $this->addKegData($data);
					if(!$success){
						$message .= $line.'---- LINE NOT SAVED'.PHP_EOL;
					}else{
						$message .= $line.'---- LINE SAVED'.PHP_EOL;
					}
				}
			}elseif(count($exp) > 1){
				$message .= $line.'---- DATA LENGTH SHORT'.PHP_EOL;
			}

		}

		return $message;
	}


	/**
	* function to check tapped event 
	*/

	public function fetch_for_tapped($input = null, $keg = null) {

		$device_name = $keg->deviceName;

		$kegData=new KegData;

		$account_id = (isset($keg->account_id)) ? $keg->account_id : null ;

		$notification='U';
		//only Once every 4 hours
		//$frequency='8';
		
		$status = $input['status'];

		if($account_id) {
		//fetch user info
			//$accountSettings=AccountSetting::where('account_id', $account_id)->first();
			//$user_info = $accountSettings->account->user->first();
			//$user_id = $user_info['id'];

			//if this variable is set then update notification table column for level
			//check that if notification set for level
			$check_notification_exist_for_untap=Device_Notification::where(['account_id'=>$account_id,'kegdevice_id'=>$keg->id,'notification'=>$notification,'deleted_at'=>null])->first();
		
			if($check_notification_exist_for_untap) { 
				$notification_id=$check_notification_exist_for_untap['id']; 
				$response = Device_Notification::where('id', $notification_id)->update(['is_untap_set' => 0]);
				echo "========>untap reset to null";
			}
				
		}

	}


}

?>
