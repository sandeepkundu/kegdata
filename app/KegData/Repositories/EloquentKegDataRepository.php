<?php namespace App\KegData\Repositories;

use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;
use Session;
use App\KegData\Models\LsmValue as LsmValue;

class EloquentKegDataRepository extends BaseEloquentRepository{
	protected $model = '\App\KegData\Models\KegData';
	protected $relationships = ['account','hubDevice','kegDevice', 'beer', 'kegtype'];
	protected $encryption = array();


	/**
	* Get the calibration for the coupler by the device
	* @var string $kegMac - Mac Address of the Device
	* @return int
	*
	*/
	public function getCalibratedValue($kegMac){
		/*$avg = $this->model
					->where('kegMac', $kegMac)
					->where(function($query){
						$query->where('status', '=', '101')->orWhere('status', '=', '1101');
					})
					->avg('adc_pour_start');
					return $avg - 164;*/
		return 0; // requested by kevin to comment the logic
	}

	/**
	* Get the kegdata by the mac addresses
	* @var array $kegMacs
	* @return model of KegData
	*
	*/
	public function getByMac($kegMacs){
		return $this->model
		->with($this->relationships)
		->whereIn('kegMac', $kegMacs)
		->orderBy('sent_at', 'DESC')
		->get();
	}

	/**
	* Get the formmatted mac address (only the last 6 numbers)
	* @var string mac address
	* @return string
	*
	*/
	public function getFormattedMac($mac){
		$formatted = substr($mac, 6, 2);
		$formatted .= ':';
		$formatted .= substr($mac, 8, 2);
		$formatted .= ':';
		$formatted .= substr($mac, 10, 2);
		return $formatted;
	}

	/**
	* Get Pour History
	* @var array [int] deviceID
	* @var int $skip - what page are we one
	* @var int $take - how many are we grabbing
	* @var array filters - where filters on the data
	* @return collection of temp history
	*/
	public function getPourHistory($deviceID, $skip = 0, $take = 20, $filters = []){
		if(count($deviceID)){
			$first = $deviceID[0];
		}else{
			$first = 0;
		}
		$query = $this->model->select(['deviceID', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','adc_pour'])
		->where('deviceID', '=', $first)->where('status', '=', '11');

		if(count($filters)){
			foreach($filters as $key => $filter){
				if($key == 'receivedDateTime_start'){
					$start = new Carbon($filter);
					$query->where('sent_at', '>', $start->toDateTimeString());
				}elseif($key == 'receivedDateTime_end'){
					$start = new Carbon($filter);
					$query->where('sent_at', '<', $start->toDateTimeString());
				}else{
					$query->where($key, '=', $filter);
				}
			}
		}

		$query->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

		for($i = 1; $i < count($deviceID); $i++){
			$subquery = $this->model->select(['deviceID', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','adc_pour'])
			->where('deviceID', '=', $deviceID[$i])->where('status', '=', '11');


			if(count($filters)){

				foreach($filters as $key => $filter){
					if($key == 'receivedDateTime_start'){
						$start = new Carbon($filter);
						$subquery->where('sent_at', '>', $start->toDateTimeString());
					}elseif($key == 'receivedDateTime_end'){
						$start = new Carbon($filter);
						$subquery->where('sent_at', '<', $start->toDateTimeString());
					}else{
						$subquery->where($key, '=', $filter);
					}
				}
			}

			$subquery->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

			$query = $query->unionAll($subquery->getQuery());

		}
			//print_r( $query->getBindings() );dd($query->toSql());
		$results = $query->get();

		return $results;
	}


	/*
	* Function For get most recent event of the data 
	*/

	public function mostRecentUntapEvent($deviceID){

		$query = $this->model->select(['deviceID','status','sent_at'])
		->where('deviceID', '=', $deviceID)
		->where('status', '!=', '1000')
		->where('status', '!=', '1001')
		->where('status', '!=', '1101');
		$query->orderBy('sent_at', 'DESC');
		$results = $query->first();
		return $results ;
			

	}


	/*
	* Function For get Last break event date

	*/

	public function lastBrekEvent($deviceID, $skip = 0, $take =1, $filters = []){

		$query = $this->model->select(['deviceID','created_at', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','adc_pour'])
		->where('deviceID', '=', $deviceID)
		->where('status', '!=', '11')
		//->where('status', '!=', '1001');
		->whereNotIn('status', ['1001','1101','1000']);

		$query->orderBy('sent_at', 'DESC');
		$results = $query->first();
		return $results;

			/*for($i = 1; $i < count($deviceID); $i++){
				$subquery = $this->model->select(['deviceID','created_at', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','adc_pour'])
				->where('deviceID', '=', $deviceID[$i])
				->where('status', '!=', '11')
				->where('status', '!=', '1001');
				

				$subquery->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

				$query = $query->unionAll($subquery->getQuery());

			}*/
			//print_r( $query->getBindings() );dd($query->toSql());
			
		}


	/*
	get the data from lsm_value table and return last breakevet data
	*/

	public function lastBreakFromLsmVaue($deviceID){

		$lsmvaue = new LsmValue;
		
		$query = $lsmvaue->select(['break_event_date','m','b'])
		->where('kegdevice_id', '=', $deviceID);
		$query->orderBy('break_event_date', 'DESC');
		$results = $query->first();
		
		return $results;
	}


	/*
	* Function For get pour for after Last break event 

	*/

	public function pourAfterLastBreak($deviceID, $lastBreakBack, $skip =0, $take =1000, $filters = []){

		
		$query = $this->model->select(['created_at','deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
		->where('deviceID', '=', $deviceID)
			//->where('status', '=', '11')
		->whereNotIn('status', ['1001'])
		->where('sent_at', '>=', $lastBreakBack->toDateTimeString());

		$query->orderBy('sent_at', 'ASC');
			//print_r( $query->getBindings() );dd($query->toSql());
		$results = $query->get();

		return $results;
	}

	/*
	* Function For get pour for after Last break event for LSM Value table

	*/

	public function pourAfterLastBreakLSMValue($deviceID, $lastBreakBack, $skip =0, $take =1000, $filters = []){

		
		$query = $this->model->select(['created_at','deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
		->where('deviceID', '=', $deviceID)
		->where('status', '=', '11')
		->whereNotIn('status', ['1001','1101','1000'])
		->where('sent_at', '>=', $lastBreakBack->toDateTimeString());

		$query->orderBy('sent_at', 'ASC');
			//print_r( $query->getBindings() );dd($query->toSql());
		$results = $query->get();

		return $results;
	}

	/*
	* Function For get pour for after Last break event 

	*/

	public function pourAfterLastBreakBetweenTwoDates($deviceID, $first_break_date ,$lastBreakBack){

		
		$query = $this->model->select(['created_at','deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
		->where('deviceID', '=', $deviceID)
		->where('status', '=', '11')
		->whereNotIn('status', ['1001','1101','1000']);

		if($first_break_date) {
			$query->where('sent_at', '>=', $first_break_date->toDateTimeString());
		}

		if($lastBreakBack) {
			$query->where('sent_at', '<', $lastBreakBack->toDateTimeString());
		}


		$query->orderBy('sent_at', 'ASC');
			//print_r( $query->getBindings() );dd($query->toSql());
			//die;

		if(empty($lastBreakBack)) {
			return null;
		} else {
			$results = $query->get();
			
			return $results;
		}

	}

	/*
	* //find out next date of break event
	*/

	public function find_next_date_of_break($deviceID, $lastBreakBack, $skip =0, $take =1000, $filters = []){
		//dd($lastBreakBack->toDateTimeString());
		$query = $this->model->select(['deviceID','created_at','sent_at','status'])
		->where('deviceID', '=', $deviceID)
		//->where('status', '!=', '11')
		//->where('status', '!=', '1001');
		->whereNotIn('status', ['11','1001','1101','1000']);
		
		//$query->where(DB::raw('DATE(sent_at)'), '>', $lastBreakBack->toDateString());
		$query->where('sent_at', '>', $lastBreakBack->toDateTimeString());
		$query->orderBy('sent_at', 'ASC');
		$results = $query->first();
		//print_r( $query->getBindings() );dd($query->toSql());

		return $results;
	}




	/**
	* Get Pour History demo
	* @var array [int] deviceID
	* @var int $skip - what page are we one
	* @var int $take - how many are we grabbing
	* @var array filters - where filters on the data
	* @return collection of temp history
	*/
	public function getPourHistoryDemo($deviceID, $skip = 0, $take = 20, $filters = []){
		if(count($deviceID)){
			$first = $deviceID[0];
		}else{
			$first = 0;
		}

		//print_r($filters);
		$query = $this->model->select(['deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
		->where('deviceID', '=', $first)
		->whereIn('status', ['11','10011']);

		if(count($filters)){
			foreach($filters as $key => $filter){
				if($key == 'receivedDateTime_start'){
					$start = $filter;
					$query->where('sent_at', '>', $start->toDateTimeString());
				}elseif($key == 'receivedDateTime_end'){
					$start = $filter;
					$start = $start->addHours(6);
					$query->where('sent_at', '<', $start->toDateTimeString());
				}else{
					$query->where($key, '=', $filter);
				}
			}
		}

			//$query->orderBy('sent_at', 'ASC')->skip($skip)->take($take);
		$query->orderBy('sent_at', 'DESC')->skip($skip)->take($take);


		for($i = 1; $i < count($deviceID); $i++){
			$subquery = $this->model->select(['deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
			->where('deviceID', '=', $deviceID[$i])
			->whereIn('status', ['11','10011']);


			if(count($filters)){

				foreach($filters as $key => $filter){
					if($key == 'receivedDateTime_start'){
						$start = $filter;
						$subquery->where('sent_at', '>', $start->toDateTimeString());
					}elseif($key == 'receivedDateTime_end'){
						$start = $filter;
						$start = $start->addHours(6);
						$subquery->where('sent_at', '<', $start->toDateTimeString());
					}else{
						$subquery->where($key, '=', $filter);
					}
				}
			}

				//$subquery->orderBy('sent_at', 'ASC')->skip($skip)->take($take);
			$subquery->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

			$query = $query->unionAll($subquery->getQuery());

		}
			//print_r( $query->getBindings() );dd($query->toSql());
		$results = $query->get();

		return $results;
	}

	//start peseudo code from here
	function pseudo_steps_first($pour,$lsm_m) {
		
		$status=$pour->status;

		//If it is a blow pour event then also send it to the lsm (automatically mark any blow pour as valid pours so there's no need to check if it's valid or not )
		if( $status == '10011') {
			// If a blow pour_length is more then 600 milliseconds then do not count it as a pour
			$return_value =($pour->pour_length < 601) ? true:false;
			return $return_value;
			
		}		


		if($status=='11') { // check if this is a pour go to step 2

			$t=$this->pseudo_steps_second($pour,$lsm_m);
			if($t) {
				return $t;
			} else { // got to step 3 if it is a pour but follow untap
				if(Session::get('last_pour_status')=='101') { // check last status is untap
					
					return false;
				} 
			}

		} else { // if it is not pour then clear lsm
			//return 'clear';
			return false;
		}
		
	}
	//check if it is a valid pour
	function pseudo_steps_second($pour,$lsm_m) {
		//check that previous pour is exist or not in session variable
		
		/*echo "===>pour_start==>".$pour->adc_pour_start;
		echo '<br>';
		echo "===>pour_end==>".$pour->adc_pour_end;
		echo '<br>';*/
		$lsm_m_value = ($lsm_m)? abs($lsm_m->lsm_m): null;

		$lsm_m = (!empty($lsm_m_value)) ? $lsm_m_value : "0.1";
		
		$var_1 = $pour->adc_pour_end -  $pour->adc_pour_start;
		//$var_1=abs($var_1);
		$var_2 = ($lsm_m) * ($pour->pour_length) ;
		//$var_2 = abs($var_2);
		$difference = ($var_1) - ($var_2);
		$difference = abs($difference);
		
		if( $difference <= 75){ 
			return true;
		} else {
			return false;
		}
		
	}

	//functon for old bakup graphreports2
	function pseudo_steps_first_2($pour) {
		$status=$pour->status;
		

		if($status=='11') { // check if this is a pour go to step 2
			
			$t=$this->pseudo_steps_second_2($pour);
			if($t) {
				return $t;
			} else { // got to step 3 if it is a pour but follow untap
				if(Session::get('last_pour_status')=='101') { // check last status is untap
					
					return false;
				} 
			}

		} else { // if it is not pour then clear lsm
			return 'clear';
		}
		
	}
	//check if it is a valid pour
	function pseudo_steps_second_2($pour) {
		
		//check that previous pour is exist or not in session variable
		if(empty(Session::get('last_pour'))){

			Session::set('last_pour', $pour->adc_pour_end);
			Session::set('last_pour_status', $pour->status);
			return true;

		} else { //not empty session varaible

			$current_pour_level=$pour->adc_pour_end;
			
			$last_pour_level=Session::get('last_pour');
			
			//let say if today level is less then yesterday
			if($current_pour_level > $last_pour_level) {
				$pour->adc_pour_end;
				//echo '<br>';
				Session::set('last_pour', $pour->adc_pour_end);
				Session::set('last_pour_status', $pour->status);
				return true;
			} else {
				//Session::set('last_pour', $pour->pour_length);
				//Session::set('last_pour_status', $pour->status);
				return false;
			}


		}
	}


	

	/** 
	*function used for to get date intervals between two dates
	*
	*/

	public function getDateIntervals() {

		$d1=date("Y-m-d", strtotime("first day of this month"));
		$d2=date("Y-m-d", strtotime("last day of this month"));

		$d1_t=strtotime($d1);
		$d2_t=strtotime($d2);

		$temp=array();
		$i=0;
		$new_date=$d1;
		$new_date_t=strtotime($d1);
		while($new_date_t <= $d2_t) {
			
			$new_date= date("Y-m-d H:i:s", strtotime($new_date . " +11 hours 59 minutes 59 seconds"));
			$new_date_t=strtotime($new_date);
			$temp[$i]=$new_date;
			$i++;
		}

		// return array of date intervals
		return $temp;



	}



	/**
	* Get Data for Tabular Reports
	* @var $column -> what field to search on
	* @var $data -> what to search by
	* @var $filters -> array[]; other filters to search by
	* @var $skip -> what page are we on
	* @var $take -> how many are we displaying
	* @return collection of kegdata
	*/
	public function getForReports($data, $column, $timezone = "UTC",  $filters = [],$default_date_start=null,$default_date_end=null){

		//dd($filters);

		$query = $this->model->select(['deviceID', 'kegMac', 'account_id', 'deviceName', 'typeID', 'product_id', 'showDevice', 'volumeType', 'temperatureType', 'created_at', 'tempOffset',
			'sent_at', 'adc_pour_end', 'temperature', 'pour_length', 'status', 'kegType', 'kegTypeDesc', 'empty', 'amount', 'pic_prefix'])
		->with('account')
		->with('beer')
		->where($column, $data)->where('showDevice', 1);

		//if(count($filters) && array_key_exists('reportStart', $filters) && array_key_exists('reportEnd', $filters)){

		if(isset($filters['deviceID']) && !empty($filters['deviceID'])) { 
			$query->where('deviceID', $filters['deviceID']);
		}
		

		if(isset($filters['status']) && !empty($filters['status'])) {
				//dd($filters);
			if($filters['status']=='other') {
				$query->whereNotIn('status', ['11','101','10011','1001']);
			} else if($filters['status']=='after') {
					 //echo $filters['reportStart']."---";die;
				$start = new Carbon($filters['reportStart'], $timezone);
					  //echo $start;die;
					  //$query->where(DB::raw('DATE(created_at)'), '>=', $start->toDateString());
				$end = new Carbon($filters['reportEnd'], $timezone);
					  //echo $end;die;

					  //to add time condition also
				$time_start=$start->toTimeString();
				$time_end=$end->toTimeString();
				$start = $start->toDateString();
				$end = $end->toDateString();

					 // $query->whereBetween(DB::raw('DATE(created_at)'),[$start,$end]);
					  //$query->where(DB::raw('DATE(created_at)'),'2016-11-11');


				$datediff = strtotime($end) - strtotime($start);
				$no_of_days_dif=floor($datediff / (60 * 60 * 24));

				if( $no_of_days_dif>0) {


					$query->whereBetween(DB::raw('DATE(created_at)'),[$start,$end]);

					$query->where(function($query) use ($time_end,$time_start,$start,$end){
						$query->orWhere(function($query) use ($time_end,$time_start,$start,$end){


							$query->Where(DB::raw('TIME(created_at)'), '>', $time_end);

						});

						$query->orWhere(function($query) use ($time_end,$time_start,$start,$end){
							$query->Where(DB::raw('TIME(created_at)'), '<', $time_start);

						});
					});

				} else {

					$query->where(function($query) use ($time_end,$time_start,$start,$end){
						$query->orWhere(function($query) use ($time_end,$time_start,$start,$end){

							$query->where(DB::raw('DATE(created_at)'),$end);
							$query->Where(DB::raw('TIME(created_at)'), '>', $time_end);

						});

						$query->orWhere(function($query) use ($time_end,$time_start,$start,$end){

							$query->where(DB::raw('DATE(created_at)'),$start);
							$query->Where(DB::raw('TIME(created_at)'), '<', $time_start);

						});

				  			//$query->orWhere(DB::raw('TIME(created_at)'), '>', $time_end);
			  				//$query->orWhere(DB::raw('TIME(created_at)'), '<', $time_start);

					});

				}
					 //print_r( $query->getBindings() );dd($query->toSql());
			} else {
				$query->where('status', '=', $filters['status']);
				$start = new Carbon($filters['reportStart'], $timezone);
				$query->where('created_at', '>', $start->toDateTimeString());
				$end = new Carbon($filters['reportEnd'], $timezone);
				$end = $end->addHours(6);
				$query->where('created_at', '<', $end->toDateTimeString());	
			}
			} else { //filter is not set
				
				//dd($default_date_start=$default_date_start->timezone($timezone->timezone)->toDayDateTimeString());

				
				$query->where('status', '=', '11');
				$default_date_start = new Carbon($default_date_start, $timezone);

				$query->where('created_at', '>=', $default_date_start->toDateTimeString());
				$default_date_end = new Carbon($default_date_end, $timezone);
				//dd($default_date_end->toDateTimeString());
				$query->where('created_at', '<=', $default_date_end->toDateTimeString());
				//$query->where(DB::raw('DATE(created_at)'),date('Y-m-d'));
			}


			// if event filter is not selected after then apply start and end date filters
			/*else if( isset($filters['status']) && ($filters['status']!='after')) {
				
				$start = new Carbon($filters['reportStart'], $timezone);
				$query->where('created_at', '>', $start->toDateTimeString());
				$end = new Carbon($filters['reportEnd'], $timezone);
				$query->where('created_at', '<', $end->toDateTimeString());
			}*/

			
			

		//print_r( $query->getBindings() );dd($query->toSql());
			$query->orderBy('created_at', 'DESC');



			return $query->paginate(50);


		}

		/**
	* Get Data for Tabular Reports
	* @var $column -> what field to search on
	* @var $data -> what to search by
	* @var $filters -> array[]; other filters to search by
	* @var $skip -> what page are we on
	* @var $take -> how many are we displaying
	* @return collection of kegdata
	*/
	public function getForReportExcelPour($data, $column, $timezone = "UTC",  $filters = [],$default_date_start=null,$default_date_end=null){

		//dd($filters);

		$query = $this->model->select(['deviceID', 'kegMac', 'account_id', 'deviceName', 'typeID', 'product_id', 'showDevice', 'volumeType', 'temperatureType', 'created_at', 'tempOffset',
			'sent_at', 'adc_pour_end', 'temperature', 'pour_length', 'status', 'kegType', 'kegTypeDesc', 'empty', 'amount', 'pic_prefix'])
		->with('account')
		->with('beer')
		->where($column, $data)->where('showDevice', 1);

		//if(count($filters) && array_key_exists('reportStart', $filters) && array_key_exists('reportEnd', $filters)){

		if(isset($filters['deviceID']) && !empty($filters['deviceID'])) { 
			$query->where('deviceID', $filters['deviceID']);
		}

		$query->where('status', '=', '11');
		$start = new Carbon($filters['reportStart'], $timezone);
		$query->where('created_at', '>', $start->toDateTimeString());
		$end = new Carbon($filters['reportEnd'], $timezone);
		$end = $end->addHours(6);
		$query->where('created_at', '<', $end->toDateTimeString());	

		$query->orderBy('created_at', 'ASC');
		//print_r( $query->getBindings() );dd($query->toSql());
		return $query->paginate(5000);


		}


	/*public function getForReports($data, $column, $timezone = "UTC",  $filters = []){


		$query = $this->model->select(['deviceID', 'kegMac', 'account_id', 'deviceName', 'typeID', 'product_id', 'showDevice', 'volumeType', 'temperatureType', 'created_at', 'tempOffset',
		'sent_at', 'adc_pour_end', 'temperature', 'pour_length', 'status', 'kegType', 'kegTypeDesc', 'empty', 'amount', 'pic_prefix'])
		->with('account')
		->with('beer')
		->where($column, $data)->where('showDevice', 1)->where('status', '!=', 'null');

		if(count($filters) && array_key_exists('reportStart', $filters) && array_key_exists('reportEnd', $filters)){

			
			if(isset($filters['status']) && !empty($filters['status'])) {
				//dd($filters['status']);
				$query->where('status', '=', $filters['status']);
			}

			$start = new Carbon($filters['reportStart'], $timezone);
			$query->where('created_at', '>', $start->toDateTimeString());
			$end = new Carbon($filters['reportEnd'], $timezone);
			$query->where('created_at', '<', $end->toDateTimeString());
		}

		$query->orderBy('created_at', 'DESC');

		return $query->paginate(50);
	}*/

 	/**
 	* Get the converted status
 	* @var int status
 	* @return string
 	*/
 	public function getStatus($status){
 		$statusMessage = array(
 			'sensorOK' => 0, 'untapped' => 0,
 			'pour' => 0, 'heartbeat' => 0);

 		$status = str_pad($status, 4, '0', STR_PAD_LEFT);
 		$statusArray = str_split($status);

 		if($statusArray[3] == 1){
 			$statusMessage['sensorOK'] = Config::get('constants.SENSOR');
 		}

 		if($statusArray[2] == 1){
 			$statusMessage['pour'] = Config::get('constants.POUR_EVENT');
 		}

 		if($statusArray[1] == 1){
 			$statusMessage['untapped'] = Config::get('constants.UNTAP');
 		}

 		if($statusArray[0] == 1){
 			$statusMessage['heartbeat'] = Config::get('constants.HEARTBEAT');
 		}

 		return $statusMessage;
 	}

	/**
	* Get the converted temperature
	* @var int $rawValue
	* @var string $tempType (C OR F)
	* @return float
	*/
	public function getTemp($rawValue, $tempType, $offset = 0){
		if($tempType == 'C'){
			return $this->getTempCelcius($rawValue, $offset);
		}else{
			return $this->getTempFahrenheit($rawValue, $offset);
		}
	}
	/**
	* Get Temp History
	* @var array [int] deviceID
	* @var int $skip - what page are we one
	* @var int $take - how many are we grabbing
	* @var array filters - where filters on the data
	* @return collection of temp history
	*/

	public function getTempHistory($deviceID, $skip = 0, $take = 20, $filters = []){
		if(count($deviceID)){
			$first = $deviceID[0];
		}else{
			$first = 0;
		}
		$query = $this->model->select(['deviceID', 'temperature', 'sent_at', 'temperatureType'])
		->where('deviceID', '=', $first)
		->where(function($query){
			$query->where('status', '=', '11')
			->orWhere('status', '=', '1001')
			->orWhere('status', '=', '1000')
			->orWhere('status', '=', '1101');
		});

		if(count($filters)){
			foreach($filters as $key => $filter){
				if($key == 'receivedDateTime_start'){
					$start = new Carbon($filter);
					$query->where('sent_at', '>', $start->toDateTimeString());
				}elseif($key == 'receivedDateTime_end'){
					$start = new Carbon($filter);
					$query->where('sent_at', '<', $start->toDateTimeString());
				}else{
					$query->where($key, '=', $filter);
				}
			}
		}

		$query->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

		for($i = 1; $i < count($deviceID); $i++){
			$subquery = $this->model->select(['deviceID', 'temperature', 'sent_at', 'temperatureType'])
			->where('deviceID', '=', $deviceID[$i])
			->where(function($query){
				$query->where('status', '=', '11')
				->orWhere('status', '=', '1001')
				->orWhere('status', '=', '1000')
				->orWhere('status', '=', '1101');
			});

			if(count($filters)){
				foreach($filters as $key => $filter){
					if($key == 'receivedDateTime_start'){
						$start = new Carbon($filter);
						$subquery->where('sent_at', '>', $start->toDateTimeString());
					}elseif($key == 'receivedDateTime_end'){
						$start = new Carbon($filter);
						$subquery->where('sent_at', '<', $start->toDateTimeString());
					}else{
						$subquery->where($key, '=', $filter);
					}
				}
			}

			$subquery->orderBy('sent_at', 'DESC')->skip($skip)->take($take);

			$query = $query->unionAll($subquery->getQuery());

		}
		//print_r( $query->getBindings() );dd($query->toSql());
		//die;
		$results = $query->get();
		return $results;

	}


 	/**
 	* Get the Temperature from the Analog Value and Convert to Fahrenheit
 	* @var int temp
 	* @return float $temp
 	*
 	*/
 	public function getTempFahrenheit($rawValue, $offset = 0){
 		$temp = ($rawValue) ? round((((.659 - ((5-($rawValue*.001221))/4))/.00132)-40), 1, PHP_ROUND_HALF_DOWN): 'N/A';
 		$temp = ($temp * (9/5)) + 32 + $offset;
 		$temp = round($temp,1,PHP_ROUND_HALF_DOWN);
 		return $temp;
 	}

	/**
 	* Get the temperature from the analog value in celcius
 	* @var int temp
 	* @return float $temp
 	*
 	*/
 	public function getTempCelcius($rawValue, $offset){
 		$temp = ($rawValue) ? round((((.659 - ((5-($rawValue*.001221))/4))/.00132)-40 + $offset), 1, PHP_ROUND_HALF_DOWN): 'N/A';
 		return $temp;
 	}

	/**
 	* Get the ounces from the analog value
 	* @var int $rawValue
 	* @var int $calibration
 	* @var int $capacity
 	* @var int $empty
 	* @var string $volume
 	* @return float ounces
 	*
 	*/
 	public function getOunces($rawValue, $calibration=0, $capacity=1984, $empty=2252, $volume='oz'){
 		$div = $empty - 500;
 		
 		if(isset($rawValue)){

 			$ounces = 1 - ((($rawValue - 500 + $calibration)/$div));

 			$ounces = round($ounces*$capacity, 0, PHP_ROUND_HALF_DOWN);

 		}else{
 			$ounces = 1984;
 		}

 		switch($volume){
 			case 'g':
 			$ounces = round($ounces*.0078125, 2, PHP_ROUND_HALF_DOWN);
 			break;
 			case 'l':
 			$ounces = round($ounces*.0295735, 2, PHP_ROUND_HALF_DOWN);
 			break;
 			case 'ml':
 			$ounces = round($ounces*29.5735, 2, PHP_ROUND_HALF_DOWN);
 			break;
 		}

 		if($ounces > $capacity){
 			$ounces = $capacity;
 		}
		//echo "=======>".$ounces;
 		return $ounces;
 	}


 	/**
 	* Get the percentage
 	* @var int $rawValue
 	* @var int $calibration
 	* @var int $capacity
 	* @var int $empty
 	* @var string $volume
 	* @return float ounces
 	*
 	*/
 	public function kegPercentageChanged($adc_endofpour=0, $adc_full=1984, $adcempty=2252, $volume='oz') {

 		//100 -  {[(adc_endofpour - adc_full) / (adcempty - adcfull)] * 100 } = percent left
 		/*echo "=====>".$adc_endofpour;
 		echo '<br>';
 		echo "==========>".$adcempty;*/
 		$perc= (($adc_endofpour - 350) / ($adcempty - 350)) * (100);

 		//$perc = ($perc < 0) ? abs($perc) : $perc;

 		$perc = 100 -$perc;
 		

 		$perc = ($perc < 5) ? '00' : $perc;
		$perc = ($perc < 10 && $perc >= 5) ? '05' : $perc;
		$perc = ($perc < 15 && $perc >= 10) ? '10' : $perc;
		$perc = ($perc < 20 && $perc >= 15) ? '15' : $perc;
		$perc = ($perc < 25 && $perc >= 20) ? '20' : $perc;
		$perc = ($perc < 30 && $perc >= 25) ? '25' : $perc;
		$perc = ($perc < 35 && $perc >= 30) ? '30' : $perc;
		$perc = ($perc < 40 && $perc >= 35) ? '35' : $perc;
		$perc = ($perc < 45 && $perc >= 40) ? '40' : $perc;
		$perc = ($perc < 50 && $perc >= 45) ? '45' : $perc;
		$perc = ($perc < 55 && $perc >= 50) ? '50' : $perc;
		$perc = ($perc < 60 && $perc >= 55) ? '55' : $perc;
		$perc = ($perc < 65 && $perc >= 60) ? '60' : $perc;
		$perc = ($perc < 70 && $perc >= 65) ? '65' : $perc;
		$perc = ($perc < 75 && $perc >= 70) ? '70' : $perc;
		$perc = ($perc < 80 && $perc >= 75) ? '75' : $perc;
		$perc = ($perc < 85 && $perc >= 80) ? '80' : $perc;
		$perc = ($perc < 90 && $perc >= 85) ? '85' : $perc;
		$perc = ($perc < 95 && $perc >= 90) ? '90' : $perc;
		$perc = ($perc < 99 && $perc >= 95) ? '95' : $perc;
		$perc = ($perc > 99) ? '99' : $perc;
		return $perc;

 	}

	/**
	* Get the percentage based on the number of ources
	* @var $ounces int
	* @var $capacity int
	*
	*/
	public function kegPercent($ounces, $capacity=1984){
		$value = $ounces;

 		//if not set keg capacity assume full size keg
		$cap = $capacity;

 		//should calculate the percentage of beer left in a keg
		if($cap == 0){
			$perc = (100*$value)/1984;
		}else{
			$perc = (100*$value)/$cap;
		}


		$perc = ($perc < 5) ? '00' : $perc;
		$perc = ($perc < 10 && $perc >= 5) ? '05' : $perc;
		$perc = ($perc < 15 && $perc >= 10) ? '10' : $perc;
		$perc = ($perc < 20 && $perc >= 15) ? '15' : $perc;
		$perc = ($perc < 25 && $perc >= 20) ? '20' : $perc;
		$perc = ($perc < 30 && $perc >= 25) ? '25' : $perc;
		$perc = ($perc < 35 && $perc >= 30) ? '30' : $perc;
		$perc = ($perc < 40 && $perc >= 35) ? '35' : $perc;
		$perc = ($perc < 45 && $perc >= 40) ? '40' : $perc;
		$perc = ($perc < 50 && $perc >= 45) ? '45' : $perc;
		$perc = ($perc < 55 && $perc >= 50) ? '50' : $perc;
		$perc = ($perc < 60 && $perc >= 55) ? '55' : $perc;
		$perc = ($perc < 65 && $perc >= 60) ? '60' : $perc;
		$perc = ($perc < 70 && $perc >= 65) ? '65' : $perc;
		$perc = ($perc < 75 && $perc >= 70) ? '70' : $perc;
		$perc = ($perc < 80 && $perc >= 75) ? '75' : $perc;
		$perc = ($perc < 85 && $perc >= 80) ? '80' : $perc;
		$perc = ($perc < 90 && $perc >= 85) ? '85' : $perc;
		$perc = ($perc < 95 && $perc >= 90) ? '90' : $perc;
		$perc = ($perc < 99 && $perc >= 95) ? '95' : $perc;
		$perc = ($perc > 99) ? '99' : $perc;
		return $perc;
 	}//close keg percent function
 	public function kegPercentLsm($ounces, $capacity=1984){
 		$value = $ounces;

 		//if not set keg capacity assume full size keg
 		$cap = $capacity;

 		//should calculate the percentage of beer left in a keg
 		if($cap == 0){
 			$perc = $ounces;
 		}else{
 			$perc = $ounces ;
 		}


 		$perc = ($perc < 5) ? '00' : $perc;
 		$perc = ($perc < 10 && $perc >= 5) ? '05' : $perc;
 		$perc = ($perc < 15 && $perc >= 10) ? '10' : $perc;
 		$perc = ($perc < 20 && $perc >= 15) ? '15' : $perc;
 		$perc = ($perc < 25 && $perc >= 20) ? '20' : $perc;
 		$perc = ($perc < 30 && $perc >= 25) ? '25' : $perc;
 		$perc = ($perc < 35 && $perc >= 30) ? '30' : $perc;
 		$perc = ($perc < 40 && $perc >= 35) ? '35' : $perc;
 		$perc = ($perc < 45 && $perc >= 40) ? '40' : $perc;
 		$perc = ($perc < 50 && $perc >= 45) ? '45' : $perc;
 		$perc = ($perc < 55 && $perc >= 50) ? '50' : $perc;
 		$perc = ($perc < 60 && $perc >= 55) ? '55' : $perc;
 		$perc = ($perc < 65 && $perc >= 60) ? '60' : $perc;
 		$perc = ($perc < 70 && $perc >= 65) ? '65' : $perc;
 		$perc = ($perc < 75 && $perc >= 70) ? '70' : $perc;
 		$perc = ($perc < 80 && $perc >= 75) ? '75' : $perc;
 		$perc = ($perc < 85 && $perc >= 80) ? '80' : $perc;
 		$perc = ($perc < 90 && $perc >= 85) ? '85' : $perc;
 		$perc = ($perc < 95 && $perc >= 90) ? '90' : $perc;
 		$perc = ($perc < 99 && $perc >= 95) ? '95' : $perc;
 		$perc = ($perc > 99) ? '99' : $perc;
 		return $perc;
 	}//close keg percent function

 	/*
	function for Beer remaining using LSM + New Formul
	@return beer remainning 

 	*/
	public function beerRemainningUsingLsmNewFormula($keg,$ouncess,$new_adc_pour_end){
		
		//$result = (($ouncess/(350 - $keg->pourHistory['0']['empty'])) * ($keg->pourHistory['0']['adc_pour_end'] - $keg->pourHistory['0']['empty']));
		//old code
		//$result = (($ouncess/(350 - $keg->pourHistory['0']['empty'])) * ($new_adc_pour_end - $keg->pourHistory['0']['empty']));
		$result = (($ouncess/(350 - $keg->keg_type_empty_value)) * ($new_adc_pour_end - $keg->keg_type_empty_value));
		return $result;
	}

	/*
	function for adc_end_of_pour
	@return beer remainning 

 	*/
	public function adcPourEnd($keg){

		$end_m=$keg->lms_data_for_adc_end['m'];
		$end_b= $keg->lms_data_for_adc_end['b'];
		
		$last_value  = $this->lastCommulativeValue($keg);
		// $afterFormula = 0.0889*($last_value) + 451.95; 
		/* adc_pour_end using new m and b */
		$afterFormula =$end_m*($last_value) + $end_b;

		return $afterFormula;
	}

	public function lastCommulativeValue($keg){


		$lastCooData = json_decode($keg->lms_data['json_x']);
		$t=0;
		$temp=[];
		if(isset($lastCooData->pour_x)) {
			$data_arr=$lastCooData->pour_x;
			for ($i=0;$i < (count($data_arr));$i++) {

				$t=$t+$data_arr[$i];
				$temp[]=$t;
			}
		}
		
		
		return end($temp);
		 
	}

 	/**
 	* Get the ounces from the analog value for lsm beer remainning
 	* @var int $capacity
 	* @var string $volume
 	* @return float ounces
 	*
 	*/
 	public function getOuncesLsm($capacity=1984,  $volume='oz'){
 	
 		$ounces = $capacity;
 		
 		switch($volume){
 			case 'g':
 			$ounces = round($ounces*.0078125, 2, PHP_ROUND_HALF_DOWN);
 			break;
 			case 'l':
 			$ounces = round($ounces*.0295735, 2, PHP_ROUND_HALF_DOWN);
 			break;
 			case 'ml':

 			$ounces = round($ounces*29.5735, 2, PHP_ROUND_HALF_DOWN);
 			
 			break;
 		}
 		return $ounces;
 	}

 	/**
 	* Get last valid pour from lsm values
 	* @var break date
 	*
 	*/

 	public function get_valid_pour_result_from_lsm($break_date = null) {
 		$lsmvaue = new LsmValue;
 		$query = $lsmvaue->where('break_event_date', '<', $break_date)
 		->orderBy('id', 'DESC')
 		->take(1);

 		$results = $query->first();
 		if(!empty($results)) {
 			$simple_x = $results->simple_x;
 			$simple_x = json_decode($simple_x);
 			$simple_x = $simple_x->pour_x;
 			$count_of_pours = count($simple_x);
 			$break_event_date = $results->break_event_date;
 			if($count_of_pours > 10) {
 				return $results;
 			} else {
 				$this->get_valid_pour_result_from_lsm($break_event_date);
 			}
 		}
 	}

 	/**
 	* Get previous break date from a break date
 	* @var break date
 	*
 	*/

 	public function get_previous_break_date_from_lsm($break_date = null) {
 		$lsmvaue = new LsmValue;
 		$query = $lsmvaue->where('break_event_date', '<', $break_date)
 		->orderBy('id', 'DESC')
 		->take(1);

 		$results = $query->first();
 		return $results;
 		
 	}

 	/*
	* Function For get pour betwwen two break even
	* in case of when there is no next break event found
	*/

	public function pourInCaseOfNoNextBreakFound($deviceID, $first_break_date ,$lastBreakBack){

		
		$query = $this->model->select(['created_at','deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])
		->where('deviceID', '=', $deviceID)
		->where('status', '=', '11')
		->whereNotIn('status', ['1001','1101','1000']);

		if($first_break_date) {
			$query->where('sent_at', '>=', $first_break_date->toDateTimeString());
		}

		if($lastBreakBack) {
			$query->where('sent_at', '<=', $lastBreakBack->toDateTimeString());
		}


		$query->orderBy('sent_at', 'ASC');
			//print_r( $query->getBindings() );dd($query->toSql());
			//die;

		if(empty($lastBreakBack)) {
			return null;
		} else {
			$results = $query->get();
			
			return $results;
		}

	}

	/**
 	* Get status on break date
 	* @var break date
 	*
 	*/

 	public function check_status_of_break_date($keg_id = null,$break_date = null) {
 		$query = $this->model->select(['created_at','deviceID','adc_pour_start', 'adc_pour_end', 'pour_length', 'sent_at', 'volumeType', 'empty', 'amount','status','adc_pour'])->where('sent_at', '=', $break_date)
 		->where('deviceID', '=', $keg_id)
 		->take(1);

 		$results = $query->first();
 		return $results;
 		
 	}

 	/*
	* Function For get most recent result of an event
	* params device id and second is status flag
	*/

	public function get_most_event_result($deviceID=null,$status=null,$after_date=null){

		$query = $this->model->select(['deviceID','status','sent_at'])
		->where('deviceID', '=', $deviceID)
		->where('status', '=', $status)
		->where('sent_at', '>',$after_date)
		->orderBy('sent_at', 'ASC')
		->take(1);
		$results = $query->first();
		return $results ;
			

	}


	public function get_most_recent_data_for_fetch_lsm_value($device_id = null,$pour_date=null) {
 		$lsmvaue = new LsmValue;
 		$query = $lsmvaue->where('kegdevice_id', '=', $device_id)
 		->where('pour_date', 'like', "%$pour_date%")
 		->select('m')
 		->orderBy('id', 'DESC')
 		->take(1);

 		$results = $query->first();
 		return $results;
 		
 	}
 	

 }

 ?>
