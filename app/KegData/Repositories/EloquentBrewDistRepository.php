<?php namespace App\KegData\Repositories;

class EloquentBrewDistRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Brewery_Distributor';
protected $relationships = ['account', 'user'];
protected $encryption = ['primaryContactName','email1','email2','phone','fax','address1','address2']; 

}

?>