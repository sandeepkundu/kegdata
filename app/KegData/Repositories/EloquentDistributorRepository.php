<?php namespace App\KegData\Repositories;

class EloquentDistributorRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Distributor';
protected $relationships = ['distributorAddress', 'account', 'user', 'beer', 'kegDevice', 'mailListKey'];
protected $encryption = array('email1', 'email2','phone','fax');

}

?>