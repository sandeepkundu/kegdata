<?php

namespace App\KegData\Repositories;

class EloquentKegDataHeartbeatRepository extends BaseEloquentRepository
{
    protected $model = '\App\KegData\Models\KegData_Heartbeat';
    protected $relationships = [];
    protected $encryption = array();

}
