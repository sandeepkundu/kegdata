<?php namespace App\KegData\Repositories;

class EloquentAccountRepository extends BaseEloquentRepository{
	protected $model = '\App\KegData\Models\Account';
	protected $relationships = ['accountAddress', 'accountSetting', 'account_type', 'user', 'hubDevice', 'kegDevice', 'enterpriseCode', 'primaryEnterpriseCode', 'deviceNotification', 'mailListKey', 'Brewery_Distributor', 'Brewery', 'Distributor'];
	protected $encryption = array('accountName', 'accountType', 'accountEmail1', 'accountEmail2', 'accountPhone2', 'accountPhone2');


}

?>