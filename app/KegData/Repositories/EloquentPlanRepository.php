<?php namespace App\KegData\Repositories;

use App\KegData\Models\Photo as Photo;
use Carbon\Carbon;
use Stripe;

class EloquentPlanRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Plan';
protected $relationships = [];


public function createPlan($input, $image = null){
	$public = public_path();
	$destinationPath ='/img/uploads/store/plan/';


	$plan = $this->model->create($input);
	if(array_key_exists('category', $input) && !is_null($input['category']) && count($input['category'])){
		$plan->category()->attach($input['category']);
	}
	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		$photos['path'] = $destinationPath.$image->getClientOriginalName();
		$photos['mime'] = $image->getClientMimeType();

		$photo = new Photo;
		$photo = $photo->create($photos);
		$plan->photos()->attach($photo->id);
	}
	//dd($plan);
	if($plan->is_quantity){
		
		$plan_price = $plan->price;
		$plan->base_price = .05;
		$plan->save();
	}else{
		$plan_price = $plan->price;
	}
	$success = $plan->push();
	
	try{
	
		$this->stripe = Stripe::make(env('stripe_key',''));
		$stripePlan =  $this->stripe->plans()->create([
		//$stripePlan = Stripe::plans()->create([
			// 'id'		=> $plan->slug,
			// 'name'		=> $plan->name,
			// 'amount'	=> $plan->price,
			// 'currency'	=> 'USD',
			// 'interval'	=> $plan->interval ? $plan->interval : "month",
			// 'statement_descriptor' => $plan->term_description
			/* New Line By 14*/
			'id'		=> $plan->slug,
			'name'		=> $plan->name,
			'amount'	=> $plan_price,
			'currency'	=> 'USD',
			'interval'	=> $plan->interval,
			'statement_descriptor' => $plan->term_description,
			'trial_period_days' => $plan->trial_days,
			'metadata' => ['min_kegs' => $plan->min, 'max_kegs' => $plan->max ]


			/* End New Line By 14*/
			//'trial_period_days' => $plan->trial_days,
			//'metadata' => ['min_kegs' => $plan->min, 'max_kegs' => $plan->max ]
		]);
	}catch(Exception $e){
		$this->deletePlan($plan->id);
		$success = false;
		$stripePlan = false;
	}


	return $success && $stripePlan;
}

public function updatePlan($input, $image = null, $id){
	$public = public_path();
	$destinationPath ='/img/uploads/store/plan/';
	$plan = $this->model->find($id);
	$plan->name = (is_null($input['name'])) ? $plan->name : $input['name'];
	$plan->price = (is_null($input['price'])) ? $plan->price : $input['price'];
	$plan->term_description = (is_null($input['term_description'])) ? $plan->description : $input['term_description'];
	$plan->min =  (is_null($input['min'])) ? $plan->description : $input['min'];
	$plan->max =  (is_null($input['max'])) ? $plan->description : $input['max'];
	$plan->plan_type = (is_null($input['plan_type'])) ? $plan->plan_type : $input['plan_type'];
	$plan->description = (is_null($input['description'])) ? $plan->description : $input['description'];
	$plan->short_description = (is_null($input['short_description'])) ? $plan->short_description : $input['short_description'];
	$plan->meta_description = (is_null($input['meta_description'])) ? $plan->meta_description : $input['meta_description'];
	$plan->meta_keywords = (is_null($input['meta_keywords'])) ? $plan->meta_keywords : $input['meta_keywords'];

	$relatedIds = $plan->category->lists('id');
	if(count($relatedIds)){
		$plan->category()->detach($relatedIds);
	}

	if(!is_null($input['category']) && count($input['category'])){
		$plan->category()->attach($input['category']);
	}
	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		if(is_null($plan->photos)){
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos);
			$plan->photos()->attach($photo->id);
		}else{
			$relatedIds = $plan->photos->lists('id');
			$plan->photos()->detach($relatedIds);
			$plan->photos()->delete();
			$plan->push();
			$photos = Photo::wherein('id',$relatedIds)->get();
			//Photo::destroy($relatedIds);
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos->toArray());
			$plan->photos()->attach($photo->id);
		}

	}

	$success = $plan->push();

		$this->stripe = Stripe::make(env('stripe_key',''));
		$delete_plan = $this->stripe->plans()->delete($plan->slug);


		$stripePlan =  $this->stripe->plans()->create([
		//$stripePlan = Stripe::plans()->create([
			'id'		=> $plan->slug,
			'name'		=> $plan->name,
			'amount'	=> $plan->price,
			'currency'	=> 'USD',
			'interval'	=> $plan->interval ? $plan->interval : "month",
			'statement_descriptor' => $plan->term_description,
			//'trial_period_days' => $plan->trial_days,
			'metadata' => ['min_kegs' => $plan->min, 'max_kegs' => $plan->max ]
		]);
		
	// $stripePlan = Stripe::plans()->update($plan->slug, [
	// 	'name' => $plan->name,
	// 	'amount'=> $plan->price,
	// 	'statement_descriptor' => $plan->term_description,
	// 	'metadata' => ['min_kegs' => $plan->min, 'max_kegs' => $plan->max ]
	// ]);
	return $success && $stripePlan;
}

public function deletePlan($id){
	$plan = $this->model->with('photos')->find($id);
	$relatedIds = $plan->category->lists('id');
	if(count($relatedIds)){
		$plan->category()->detach($relatedIds);
	}

	$relatedIds = $plan->photos->lists('id');
	if(count($relatedIds)){
		$plan->photos()->detach($relatedIds);
		$plan->photos()->delete();
	}

	$plan->push();
	$photos = Photo::wherein('id',$relatedIds)->get();
	if(count($relatedIds)){
		Photo::destroy($relatedIds);
	}

	$success = $plan->delete();
	$stripePlan = Stripe::plans()->delete($plan->slug);
	return $success && $stripePlan;
}

}

?>
