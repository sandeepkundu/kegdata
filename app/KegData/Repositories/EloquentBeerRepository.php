<?php namespace App\KegData\Repositories;

class EloquentBeerRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Beer';
protected $relationships = ['brewery', 'distributor', 'kegDevice'];


}

?>