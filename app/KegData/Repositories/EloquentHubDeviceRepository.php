<?php namespace App\KegData\Repositories;


use App\KegData\Models\KegDevice;

class EloquentHubDeviceRepository extends BaseEloquentRepository{
	protected $model = '\App\KegData\Models\HubDevice';
	protected $relationships = ['account'];
	protected $encryption = array();


	public function getByAccount($id){
		return $this->model
		->where('account_id', '=', $id)
		->get();
	}

	public function getDeviceByAccount($id){
		$accountSettings=KegDevice::where('account_id', $id)->get();
		return $accountSettings;
	}



	public function createOrUpdate($input){
		$hubMac = str_replace(':', '', $input['hubMac']);
		$hubDevice = $this->model->firstOrNew(['hubMac' => $hubMac]);
		$hubDevice->hubMac = $hubMac;
		$hubDevice->account_id = $input['account_id'];
		return $hubDevice->save();
	}

}

?>