<?php namespace App\KegData\Repositories;

use Carbon\Carbon as Carbon;
use App\KegData\Models\Beer as Beer;
use App\KegData\Models\Distributor as Distributor;
use App\KegData\Models\Brewery as Brewery;

/**
* Repository to interact with keg devices*
*/

class EloquentKegDeviceRepository extends BaseEloquentRepository{
	protected $model = '\App\KegData\Models\KegDevice';
	protected $relationships = ['beer','distributor', 'brewery', 'kegtype', 'kegdata'];
	protected $encryption = array();

	/**
	* Function to get keg levels for brewery or distributors
	* @param $data - where statement data
	* @param $column - what column to search the data on
	* @param $skip - what page are we on
	* @param $take - how much data are we retrieving
	* @param $account_id
	*/
	public function getForBrewDist($data, $column, $account_id = 0,  $skip = 0, $take = 100, $filters = []){

		$data = $this->model->leftJoin('beers', 'kegdevices.product_id', '=', 'beers.id')
		->select(['kegdevices.id','kegdevices.account_id', 'kegdevices.kegMac', 'kegdevices.deviceName', 'kegdevices.showDevice',
			'beers.name', 'beers.id as beerID'])
			->with('tapped')
			->with('recentPour')
			->with('temperature')
			->with('account')
			->with(['notifications' => function($query) use ($account_id){
					$query->with('notificationFrequency');
					if($account_id != 0){
						$query->where('account_id', '=', $account_id);
					}
			}])
			->where($column, '=', $data)->where('showDevice', '=', '1')
			->get();
		return $data;
	}

	/**
	* Function to get KegDevices and related data for the levels page
	* @param $data - the limit in where statement
	* @param $column - the column in the where statement
	* @param $account_id - the account_id searching
	* @param $skip - what page are we on
	* @param $take - how many should we take
	* @param $filters - input data to limit the data with
	*
	*/
	public function getForLevels($data, $column, $account_id = 0,  $skip = 0, $take = 100, $filters = []){


		$data = $this->model
			->with('tapped')
			->with('recentPour')
			->with('temperature')
			->with('beer')
			->with('brewery')
			->with('distributor')
			->with(['notifications' => function($query) use ($account_id){
					$query->with('notificationFrequency');
					if($account_id != 0){
						$query->where('account_id', '=', $account_id);
					}
			}])
			->where($column, '=', $data)
			->get();
		return $data;
	}


	/**/
	public function getForLevelsCharts($data, $column, $account_id = 0,  $skip = 0, $take = 100, $filters = []){
	
			
		$data = $this->model
		->with('tapped')
		->with('recentPour')
		->with('temperature')
		->with('beer')
		->with('brewery')
		->with('distributor')
		->with(['notifications' => function($query) use ($account_id){
				$query->with('notificationFrequency');
					if($account_id != 0){
						$query->where('account_id', '=', $account_id);
					}
				}])
				->where($column, '=', $data);
				if(isset($skip)){
				$data->where('id','=',$skip);
			}
	
		return $data->get();
			
		}


	/**/
	public function getWithKegData($data, $column, $skip = 0, $take = 1000, $account_id = 0, $input= []){
		$filters =$input;

		$data = $this->model
				->with(['kegdata' => function($query) use ($skip, $take, $filters){
					if(count($filters) &&  array_key_exists('reportStart', $filters) && array_key_exists('reportEnd', $filters)){

						$startDate = new Carbon($filters['reportStart']);
						$endDate = new Carbon($filters['reportEnd']);
						$query->where('sent_at', '>', $startDate->toDateTimeString());
						$query->where('sent_at', '<' , $endDate->toDateTimeString());
						$query->orderBy('sent_at', 'desc');

					}else{
						$query->skip($skip)->take($take)->orderBy('sent_at', 'desc');
					}
				}, 'beer', 'brewery', 'distributor', 'notifications' => function($query) use ($account_id){
					$query->with('notificationFrequency');
					if($account_id != 0){
						$query->where('account_id', '=', $account_id);
					}
				}])->where('showDevice', '=', 1)
				->where($column, '=', $data);
		return $data->get();
	}

	public function getForNotifications($data, $column, $account_id = 0, $skip = 0, $take = 20, $filters = []){
		$data = $this->model
			->with('notifications.notificationFrequency')
			->with('beer')
			->where($column, '=', $data)
			->get();
		return $data;
	}

	public function getForExcel($data, $column, $filters = []){
		$data = $this->model->leftJoin('beers', 'kegdevices.product_id', '=', 'beers.id')
		->leftJoin('kegtypes', 'kegdevices.kegType', '=', 'kegtypes.id')
		->select(['kegdevices.id', 'kegdevices.kegMac', 'kegdevices.account_id', 'kegdevices.devicename'
		, 'kegdevices.product_id', 'kegdevices.kegType', 'kegdevices.volumeType', 'kegdevices.temperatureType', 'beers.name'
		, 'kegtypes.amount', 'kegtypes.emptyValue'])
		->with('kegdata')
		->where('kegdevices.showDevice', '=', '1')
		->where('kegdevices.'.$column, '=', $data)->get();
		return $data;
	}

	public function getForSetup($data, $column){
		$data = $this->model
			->with('kegtype')
			->with('beer')
			->where($column, '=', $data)
			->get();
		return $data;
	}

	public function update_lsm($id,$lsm_value) {

		$keg = $this->model->find($id);
		$keg->lsm_m = $lsm_value;
		return $keg->save();
	}

	public function update($input){
		//dd($input);
		$keg = $this->model->find($input['id']);
		if(isset($input['kegType'])) $keg->kegType = $input['kegType'];
		if(isset($input['showDevice']) && $input['showDevice'] == 0){
			$keg->showDevice = $input['showDevice'];
		} else{
			$keg->showDevice = 1;
		}
		if(isset($input['product_id'])) $keg->product_id = $input['product_id'];
		if(isset($input['style'])) $keg->style = $input['style'];
		if(isset($input['description'])) $keg->description = $input['description'];
		if(isset($input['deviceName'])) $keg->deviceName = $input['deviceName'];

		if(empty($input['selectedDistID'])) {
			$keg->distributor_id = 0;
			$keg->brewery_id = 0;
		}

		
		if($input['distType'] == 'D'  && !empty($input['selectedDistID'])) {
			$keg->distributor_id = $input['selectedDistID'];
			if($input['product_id']){
				$beer = new Beer;
				$beer = $beer->find($keg->product_id);
				$distributor = new Distributor;
				$distributor = $distributor->find($keg->distributor_id);
				$beer->distributor()->attach($distributor);
				$beer->save();
			}
		} 

		if($input['distType'] == 'B'  && !empty($input['selectedDistID'])) {
			$keg->brewery_id = $input['selectedDistID'];
			if($input['product_id']){
				$beer = new Beer;
				$beer = $beer->find($keg->product_id);
				$brewery = new Brewery;
				$brewery = $brewery->find($keg->brewery_id);

				$beer->brewery()->associate($brewery);
				//put if because there is using breweryID column not brewery_id in beer
				if(isset($beer->brewery_id)) {
					$beer->breweryID=$beer->brewery_id;
					unset($beer->brewery_id);
				}
				$beer->save();
			}

		} 



		
		return $keg->save();
	}

}

?>
