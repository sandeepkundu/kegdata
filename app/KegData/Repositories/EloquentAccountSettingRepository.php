<?php namespace App\KegData\Repositories;

use Carbon\Carbon;
use App\KegData\Models\KegDevice as KegDevice;
use DB;

class EloquentAccountSettingRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\AccountSetting';
protected $relationships = ['account', 'timezone'];

public function update($input){
	$accountSetting = $this->model->find($input['id']);
	$kegdevice = new KegDevice();

	if(isset($input['volumeType'])) $accountSetting->volumeType = $input['volumeType'];
	if(isset($input['temperatureType'])) $accountSetting->temperatureType = $input['temperatureType'];
	if(isset($input['timezone'])) $accountSetting->timezone = $input['timezone'];
	if(isset($input['currency'])) $accountSetting->currency = $input['currency'];
	if(isset($input['weekStart'])) $accountSetting->weekStart = $input['weekStart'];
	if(isset($input['daystart12']) ){
		$str = explode(':', $input['daystart12']);
		if( $input['amStart'] == 'pm'){
			$str[0] = $str[0] + 12;
		}
		$dayStart = Carbon::createFromTime($str[0], $str[1], $str[2]);
		$accountSetting->dayStart = $dayStart->format('G:i:s');
	}

	if(isset($input['dayend12']) ){
		$str = explode(':', $input['dayend12']);
		if( $input['amEnd'] == 'pm'){
			$str[0] = $str[0] + 12;
		}
		$dayEnd = Carbon::createFromTime($str[0], $str[1], $str[2]);
		$accountSetting->dayEnd = $dayEnd->format('G:i:s');
	}

	$updated = $accountSetting->save();
	
	$affectedRows = $kegdevice->where('account_id', '=', $accountSetting->account_id)->update(['volumeType' => $accountSetting->volumeType, 'temperatureType' => $accountSetting->temperatureType]);

	return $updated;
}

}

?>