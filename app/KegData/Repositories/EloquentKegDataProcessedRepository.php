<?php

namespace App\KegData\Repositories;

use App\KegData\Models\HubDevice;
use DB;

class EloquentKegDataProcessedRepository extends BaseEloquentRepository
{
    protected $model = '\App\KegData\Models\KegData_Processed';
    protected $relationships = [];
    protected $encryption = array();

    /**
     * @method getLastPours($take, $start)
     * Function to select last pour(s) that was processed
     * Should only select data since the last status event
     * that is not a pour
     *
     * @return Eloquent Collection $lastPours
     */
    public function getLastPours($hubmac, $kegmac, $take = 1, $skip = 0): \Illuminate\Database\Eloquent\Collection
    {
        $data = NULL;
        // Lets find the account id for this hub if there is one

        $hub = HubDevice::where('hubmac', '=', $hubmac)->first();

        $account_id = $hub->account_id;


        /*
        * Lets check what status code we are processing to start the
        * logic chain
        * BITS are read from right to left
        * (Bit 0 is least significant bit).
        * Bit 0: Set if pressure sensor is installed.
        * Bit 1: Set if reporting pour event.
        * Bit 2: Set if keg is untapped.
        * Bit 3: Tags it as heart beat packet Array Position 1
        * Bit 4: Blow event.
        */

        // get the date time of the last event that was not a pour
        // this function can return an empty collection if no pours
        // have occurred since the last event

        // 10001 -> Blow Event
        // 10011 ->  Pour Event With Blow Event
        // 101 -> Untapped

        $kegreset = $this->model
                ->where('kegmac', '=', $kegmac)
                ->where('kegreset', '=', 1)
                ->orderBy('sent_at', 'desc')
                ->skip(0)->take(1)->first();

        //If we have data for kegreset - only select since the sent at
        //in kegreset

        if(is_null($kegreset) || empty($kegreset)){
            $data = $this->model->with(['kegdevice' => function($query) use($account_id){
                        $query->where('account_id', '=', $account_id);
                    }])
                    ->with('kegdevice.kegspecs')
                    ->with('kegdevice.kegspecs.kegtypes')
                    ->where('kegmac', '=', $kegmac)
                    ->orderBy('created_at', 'asc')
                    ->skip($skip)
                    ->take($take)->get();

        }else{
            $data = $this->model->with(['kegdevice' => function($query) use($account_id){
                        $query->where('account_id', '=', $account_id);
                    }])
                    ->with('kegdevice.kegspecs')
                    ->with('kegdevice.kegspecs.kegtypes')
                    ->where('kegmac', '=', $kegmac)
                    ->where('created_at', '>=', $kegreset->created_at)
                    ->where('kegreset', '=', 0)
                    ->orderBy('created_at', 'asc')
                    ->skip($skip)
                    ->take($take)->get();
        }

        return $data;
    }
}
