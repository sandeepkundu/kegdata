<?php namespace App\KegData\Repositories;


use Stripe;
use App\KegData\Repositories\EloquentAccountRepository as Account;
use App\KegData\Repositories\EloquentUserRepository as User;
use App\KegData\Repositories\EloquentPlanRepository as Plan;
use App\KegData\Repositories\EloquentProductRepository as Product;
use App\KegData\Models\OrderItem as OrderItem;
use App\KegData\Models\OrderAddress as OrderAddress;
use App\KegData\Models\Subscription as Subscription;
use Illuminate\Contracts\Auth\Guard as Guard;
use Carbon\Carbon;
use Cart;


class EloquentOrderRepository extends BaseEloquentRepository{

	protected $model = '\App\KegData\Models\Order';
	protected $relationships = [];

	public function __construct(Guard $auth, Plan $plan, Product $product){
		parent::__construct();

		$this->auth = $auth;
		$this->plan = $plan;
		$this->product = $product;
	}

	/**
	* @method public function withProducts
	* Selection of order data with product data linked to order items
	* @return collection $order
	*/
	public function withProducts($order_id){
		$order = $this->model->with('orderAddress')->with('subscription.plan')->with('orderItem.product')->find($order_id);

		return $order;
	}

	/**
	 * Select of all orders for an account with relations
	 * Set with pagination
	 */
	public function getAllWithProducts(){
		$orders = $this->model->with('orderAddress')->with('subscription.plan')->with('orderItem.product.photos')
		->where('account_id', '=', $this->auth->user()->account_id)
		->orderBy('created_at', 'desc')
		->paginate(10);

		return $orders;
	}

	/**
	 * Select of all orders for an account with relations
	 * Set with pagination
	 */
	public function getAllForEmployees(){
		$orders = $this->model->with('orderAddress')->with('subscription.plan')->with('orderItem.product')->orderBy('created_at', 'desc')->paginate(50);

		return $orders;
	}

	/**
	 * delete  of  orders for an account with relations
	 * Set with pagination
	 */
	public function deleteOrder($id){
		$order = $this->model->find($id);
		if($order->charge_id != ''){
			$order = Stripe::orders()->find($order->charge_id );

			if(isset($order)){
				$stripePlan = Stripe::orders()->delete($order->charge_id);	
			}
		}
		$success = $order->delete();
		return $success;
	}
}

?>
