<?php namespace App\KegData\Repositories;

class EloquentUserRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\User';
protected $relationships = ['account', 'primaryAccount', 'brewery', 'distributor', 'breweryDistributor', 'role', 'userLoginHistory', 'userVerification'];

protected $encryption = ['email', 'firstName', 'lastName', 'email2', 'phone1', 'phone2']; 

}

?>