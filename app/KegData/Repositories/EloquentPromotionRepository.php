<?php namespace App\KegData\Repositories;

use App\KegData\Models\Photo as Photo;
use Carbon\Carbon as Carbon;


class EloquentPromotionRepository extends BaseEloquentRepository{
protected $model = '\App\KegData\Models\Promotion';
protected $relationships = [];


public function createPromotion($input, $timezone, $image = null){
	$public = public_path();
	$destinationPath ='/img/uploads/store/promotion/';

	/**
	* @todo : change string format to match form field
	*
	*/
	$input['start_date'] = new Carbon($input['start_date'], $timezone);
	$input['start_date'] = $input['start_date']->setTimezone('UTC')->toDateTimeString();
	$input['end_date'] = new Carbon($input['end_date'], $timezone);
	$input['end_date'] =  $input['end_date']->setTimezone('UTC')->toDateTimeString(); 
	$promotion = $this->model->create($input);
	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		$photos['path'] = $destinationPath.$image->getClientOriginalName();
		$photos['mime'] = $image->getClientMimeType();

		$photo = new Photo;
		$photo = $photo->create($photos);
		$promotion->photos()->attach($photo->id);
	}

	return $promotion->push();
}

public function updatePromotion($input, $timezone, $image = null, $id){
	$public = public_path();
	$destinationPath ='/img/uploads/store/promotion/';

	$promotion = $this->model->with('photos')->find($id);
	
	/**
	* @todo : change string format to match form field
	*
	*/
	if(is_null($input['start_date'])){
		$start_date = $promotion->start_date;
	}else{
		$start_date = new Carbon($input['start_date'], $timezone);
		$start_date = $start_date->setTimezone('UTC')->toDateTimeString();
		
	}

	if(is_null($input['end_date'])){
		$end_date = $promotion->end_date;
	}else{
		$end_date = new Carbon($input['end_date'], $timezone);
		$end_date =  $end_date->setTimezone('UTC')->toDateTimeString(); 
	}


	$promotion->name = (is_null($input['name'])) ? $promotion->name : $input['name'];
	$promotion->description = (is_null($input['description'])) ? $promotion->description : $input['description'];
	$promotion->short_description = (is_null($input['short_description'])) ? $promotion->short_description : $input['short_description'];
	$promotion->meta_description = (is_null($input['meta_description'])) ? $promotion->meta_description : $input['meta_description'];
	$promotion->meta_keywords = (is_null($input['meta_keywords'])) ? $promotion->meta_keywords : $input['meta_keywords'];
	$promotion->start_date = $start_date;
	$promotion->end_date = $end_date;
	if(!is_null($image)){
		$image->move($public.$destinationPath, $image->getClientOriginalName());
		if(is_null($promotion->photos)){
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos);
			$promotion->photos()->attach($photo->id);
		}else{
			$relatedIds = $promotion->photos->lists('id');
			$promotion->photos()->detach($relatedIds);
			$promotion->photos()->delete();
			$promotion->push();
			$photos = Photo::wherein('id',$relatedIds)->get();
			//Photo::destroy($relatedIds);
			$photos['path'] = $destinationPath.$image->getClientOriginalName();
			$photos['mime'] = $image->getClientMimeType();
			$photo = new Photo;
			$photo = $photo->create($photos->toArray());
			$promotion->photos()->attach($photo->id);
		}
		
	}

	return $promotion->push();
}

public function deletePromotion($id){
	$promotion = $this->model->with('photos')->find($id);
	$relatedIds = $promotion->photos->lists('id');
	$promotion->photos()->detach($relatedIds);
	$promotion->photos()->delete();
	$promotion->push();
	$photos = Photo::wherein('id',$relatedIds)->get();
	Photo::destroy($relatedIds);
	return $promotion->delete();
}

}

?>