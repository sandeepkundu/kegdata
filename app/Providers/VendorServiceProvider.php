<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use mnshankar\LinearRegression\Regression;

class VendorServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var boolean
     */
    protected $defer = true;

    /**
     * Register Vendor Classes
     *
     * @return void
     */
    public function register()
    {
        // $this->app->singleton(Regression::class, function($app){
        //     return new Regression();
        // });
    }

    /**
     * Get the services provided by the provider
     *
     * @return array
     */
    public function provides():array {
        return [
        ];
    }
}
