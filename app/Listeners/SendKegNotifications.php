<?php

namespace App\Listeners;

use App\Events\DataWasProcessed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\KegData\Repositories\EloquentHubDeviceRepository as HubDevice;
use App\KegData\Repositories\EloquentKegDataRepository as KegData;
use DB;
use Mail as Mail;
use Twilio as Twilio;

class SendKegNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(HubDevice $hubDevice, KegData $kegdata)
    {
        //
        $this->hubdevice = $hubDevice;
        $this->kegdata = $kegdata;
    }

    /**
     * Handle the event.
     *
     * @param  DataWasProcessed  $event
     * @return void
     */
    public function handle(DataWasProcessed $event)
    {
        $processed = $event->processed;

        foreach($processed as $item){
            $account = DB::table('account');
        }

        Mail::send('emails.test', ['processed' => $processed], function($message) use ($processed)
        {
            $message->to('latisha.mcneel@gmail.com')->from('dashboard@kegdata.com')->subject('Data was processed');
        });
    }
}
