<?php

namespace App\Listeners;

use App\Events\DataWasProcessed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateKegDashboard
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DataWasProcessed  $event
     * @return void
     */
    public function handle(DataWasProcessed $event)
    {
        //
        
    }
}
