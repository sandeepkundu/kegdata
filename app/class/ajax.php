<?php 
/*Class to deal with generalized ajax stuff*/

class ajax{

	//call this to validate multiple values
	public function ajaxCall($query){
		$urlData = explode('&', $query);
		foreach ($urlData as $parameter){
			$param = explode('=', $parameter);
			if($param[0] == 'method'){
				$method = $param[1];
			}else{
				$data[$param[0]] = $param[1];
			}
		}

		if(isset($method) && method_exists($this,$method)){
			$return = $this->$method($data);
			return $return;
		}else{
			return false;
		}
	}//close public function ajaxCall

	//private function to get distributors/breweries
	private function getDists($data){
		global $db;
		global $gb;
		global $honeyandthebee;

		$searchBy = $data['searchBy'];
		$search = $data['search'];

		if(empty($searchBy) || is_null($searchBy)){
			$searchBy = 'zipcode';
		}

		$data = array('id', 'accountID', 'type','name','address1','address2','city','stateCode','zipcode');

		switch($searchBy){
			case 'zipcode':
				$where = array('zipcode' => $search,'ORDER' => array("accountID DESC",'name'));
			break;
			case 'city':
				$where = array('LIKE' => array('city' => $search),'ORDER' => array("accountID DESC",'name'));
			break;
			case 'name':
				$where = array('LIKE' => array('name' => $search),'ORDER' => array("accountID DESC",'name'));
			break;
		}

		$results = $db->select(BREW_DIST,$data,$where);
		$distributors = array();
		foreach($results as $result){
			$dist = array(
				'id' => $result['id'],
				'accountID' => $result['accountID'],
				'type' => $result['type'],
				'name' => strtoupper($result['name']),
				'address1' => $gb->eat($result['address1'],$honeyandthebee),
				'address2' => (empty($result['address2'])) ? '' :  $gb->eat($result['address2'],$honeyandthebee),
				'city' => $result['city'],
				'stateCode' => $result['stateCode'],
				'zipcode' => $result['zipcode']
			);
			array_push($distributors, $dist);
		}
		return json_encode($distributors);
	}

	//private function to get timezones for a region
	private function timezoneByRegion($region){
		global $db;

		if(isset($region['region'])){
			$data = array('id', 'timezone', 'offset');
			$where = array('region' => $region['region'], "ORDER" => 'offset');
			$results = $db->select(TIMEZONE, $data, $where);

			return json_encode($results);
		}else{
			return false;
		}
	}//close public function timezoneByRegion


}