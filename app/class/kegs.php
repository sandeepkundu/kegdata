<?php
//class for building a kegs
//dependent upon medoo.min.php -> should be called with global $db
//dependent upon error.php -> error reporting with users
//dependent upon user.php -> need user data to get keg data

 class kegs{

 	//keg is an array set with each keg
 	//and basic information for each keg
 	protected $keg = array();

 	//distKegs is array set with each keg
 	//for that distributor - created a new variable for this
 	//in case any breweries or distributors ever order their products
 	//and the system needs to expand
 	protected $distKegs = array();

 	/*
 	 *private function: buildKegs()
     *builds keg arrays with singular data for a keg that is most frequently used across dashboard
     *returns: true or false value for success
     */
 	private function buildKegs(){
 		global $db;
 		global $err;
 		global $user;

 		$select = 'SELECT ';
 		$select .='deviceID, accountID, kegMac, deviceName, typeID, volumeType, temperatureType, productID, showDevice,
 			max(sentDateTime) as sentDateTime, startVal, pourVal, endVal, tempCelcius, elapsedTenths, status,
 			receivedDateTime, kegType, kegTypeDesc, kegTypeMeasurement, amount, empty, pic_prefix, name';
 		$from = ' FROM kegdata';
 		$where = " WHERE status != '10000101' AND accountID = ".$user->getAccountNumber();
 		$where .= "  AND kegdata.receivedDateTime = (SELECT max(receivedDateTime) FROM kegdata kd2 WHERE
            kegdata.deviceID = kd2.deviceID and status != '10000101')";
 		$where.=" GROUP BY kegMac";

 		$results = $db->query($select.$from.$where)->fetchAll();
 		$error = $db->error();
 		$lastquery = $db->last_query();
 		$success = true;
		date_default_timezone_get();
 		if($error[0] == '00000' && $results != ''){
 			//TODO: More than one keg level exists for each device
 			//TODO: Need to select only most recent
 			foreach($results as $result){
 				$now = new DateTime("now");
 				//$kegTap = new DateTime($result['kegtap']);

				//$kegTapDays = $kegTap->diff($now);

 				$cal = $this->calibrate($result['deviceID']);
 				$cap = (empty($result['amount']) ? 1984 : $result['amount']);
 				$pour = $this->lastDayPours($result['kegMac'], $cal, $cap,$result['empty'],$result['volumeType']);
 				$dayOfPour = new DateTime($pour[1]);
 				$timezone = $user->getAccountTimeZone();
				$accountTimeZone = (empty($timezone)) ? new DateTimeZone('America/Chicago') : new DateTimeZone($timezone);
				$dayOfPour->setTimezone($accountTimeZone);
				$tappedDate = $this->getTappedDate($result['deviceID']);
				$tappedDate = (is_null($tappedDate)) ? 'No tapped date on record' : new DateTime($this->getTappedDate($result['deviceID']));
				if($tappedDate != 'No tapped date on record'){
					$tappedDate->setTimezone($accountTimeZone);
				}
				if($tappedDate <= $dayOfPour){
	 				$kegArray = array(
	 					"devNum" => $result['deviceID'], 
	 					"kegMac" => $result['kegMac'],
	 					"kegName" => $result['deviceName'], 
	 					"prodNum" => $result['productID'],
	 					"prodName" => $result['name'],
	 					"kegTypeNum" => $result['typeID'],
	 					"kegType" => $result['kegTypeDesc'],
	 					"kegCapacity" => $cap,
	 					"kegMeasurement" => $result['kegTypeMeasurement'],
	 					"kegTypePic" => $result['pic_prefix'],
	 					"kegStart" => ($result['status'] == '10000011') ? $this->calLevels($result['startVal'], $cal, $cap, $result['empty'],$result['volumeType']) : 'N/A',
	 					"kegPour" => ($result['status'] == '10000011') ? $result['pourVal'] : 'N/A',
	 					"kegEnd" => ($result['status'] == '10000011') ? $this->calLevels($result['endVal'],$cal, $cap, $result['empty'],$result['volumeType']) : 'N/A',
	 					"elapseTime" => $result['elapsedTenths']/60,
	 					"kegTemp" => ($result['status'] == '10000011') ? $this->calTemp($result['tempCelcius'], $result['temperatureType']) : 'N/A', 
	 					"sentDtTm" => $result['sentDateTime'],
	 					"kegStatus" => $this->getStatus($result['status']),
	 					"calibration" => $cal,
	 					"showDevice" => $result['showDevice'],
	 					"volumeType" => $result['volumeType'],
	 					"temperatureType" => $result['temperatureType'],
	 					"kegTap" => ($tappedDate == 'No tapped date on record') ? $tappedDate : $tappedDate->format('M d, Y g:i:s a'),
	 					'todayPours' => $pour[0],
	 					'dayOfPours' => $dayOfPour->format('M d, Y g:i:s a'),
	 					'flowRate' => $pour[2],
	 					'totalPourTime' => $pour[3]/10,
	 					'notifications' => $this->getNotifications($result['deviceID'],0)
	 				);
				}else{
					$kegArray = array(
	 					"devNum" => $result['deviceID'], 
	 					"kegMac" => $result['kegMac'],
	 					"kegName" => $result['deviceName'], 
	 					"prodNum" => $result['productID'],
	 					"prodName" => $result['name'],
	 					"kegTypeNum" => $result['typeID'],
	 					"kegType" => $result['kegTypeDesc'],
	 					"kegCapacity" => $cap,
	 					"kegMeasurement" => $result['kegTypeMeasurement'],
	 					"kegTypePic" => $result['pic_prefix'],
	 					"kegStart" => 'N/A',
	 					"kegPour" => 'N/A',
	 					"kegEnd" => 'N/A',
	 					"elapseTime" => 'N/A',
	 					"kegTemp" => 'N/A', 
	 					"sentDtTm" => $result['sentDateTime'],
	 					"kegStatus" => 'N/A',
	 					"calibration" => $cal,
	 					"showDevice" => $result['showDevice'],
	 					"volumeType" => $result['volumeType'],
	 					"temperatureType" => $result['temperatureType'],
	 					"kegTap" => ($tappedDate == 'No tapped date on record') ? $tappedDate : $tappedDate->format('M d, Y g:i:s a'),
	 					'todayPours' => 'N/A',
	 					'dayOfPours' => "No pours since tap",
	 					'flowRate' => 'N/A',
	 					'totalPourTime' => 'N/A',
	 					'notifications' => $this->getNotifications($result['deviceID'],0)
	 				);
				}
 				array_push($this->keg, $kegArray);
 			}

 		}else{
 			$success = false;
 			if(IS_DEBUG){
 				$err->setError('Cannot build kegs :' + $error[2] + '<br />' + $lastquery);
 			}else{
 				$err->setError('Cannot retreive kegdata at this time. Please try again');
 			}
 		}

 		return $success;
 	}

 	/*
 	 *public function: loadKegs
 	 *access point to build kegs for an account
 	 *returns: true or false value for success
 	 */
 	public function loadKegs(){
 		global $user;

 		$success = $this->buildKegs();
 		if($user->getAccountType() == 'B' || $user->getAccountType() == 'D'){
 			$success = $this->loadDistKegs();
 		}
 		return $success;
 	}

 	/*
 	 *public function: loadDistKegs
 	 *Load the kegs for a particular distributor
 	 *acceptVariable: variableDesc
 	 *returns: $kegs ARRAY on success, false on failure
 	 */
 	public function loadDistKegs(){
 		global $db;
 		global $err;
 		global $gb;
 		global $honeyandthebee;
 		global $user;

 		$where = ($user->getAccountType() == 'B') ? array('breweryID' => $user->getDistID()) : array('distributorID' => $user->getDistID()); 
 		$whichKegs = $db->select(KEGDEVICES, 'id', $where);

 		$stringKegs = "0";
 		foreach($whichKegs as $keg){

 			$stringKegs .= ", ".$keg; 
 		}


 		$select = "SELECT deviceID, kegdata.accountID, typeID, productID, a.accountName, aa.address1, aa.address2, aa.city, aa.stateCode, aa.zipcode,";
 		$select .= " endVal, receivedDateTime, kegType, kegTypeDesc, kegTypeMeasurement, amount, empty, pic_prefix, name";
		$from = " FROM kegdata";
		$join = " LEFT JOIN accounts a ON (kegdata.accountID = a.ID)";
		$join .= " LEFT JOIN account_addresses aa ON (kegdata.accountID = aa.accountID)";
		$where = " WHERE status != '10000101' AND deviceID IN (".$stringKegs.") AND showDevice = 1";
		$where .= " AND kegdata.receivedDateTime = (SELECT max(receivedDateTime) FROM kegdata kd2 WHERE";
		$where .= " kegdata.deviceID = kd2.deviceID and status != '10000101')";
		$where .= " GROUP BY kegMac ORDER BY endVal DESC";

		$results = $db->query($select.$from.$join.$where);
 		$error = $db->error();
 		if($error[0] == '00000' && $results != ''){

			foreach($results as $result){
				$now = new DateTime("now");
				$cal = $this->calibrate($result['deviceID']);
				$cap = (empty($result['amount']) ? 1984 : $result['amount']);;
				$timezone = $user->getAccountTimeZone();
				$accountTimeZone = (empty($timezone)) ? new DateTimeZone('America/Chicago') : new DateTimeZone($timezone);
				$thisKeg = array('kegEnd' => $this->calLevels($result['endVal'], $cal, $cap, $result['empty'], 'oz'), 'kegCapacity' => $cap);
				$kegArray = array(
		 					"devNum" => $result['deviceID'], 
		 					"prodNum" => $result['productID'],
		 					"prodName" => $result['name'],
		 					"kegTypeNum" => $result['typeID'],
		 					"kegType" => $result['kegTypeDesc'],
		 					"kegCapacity" => $cap,
		 					"kegMeasurement" => $result['kegTypeMeasurement'],
		 					"kegTypePic" => $result['pic_prefix'],
		 					"level" => $this->kegPercent($thisKeg),
		 					"accountName" => $gb->eat($result['accountName'], $honeyandthebee),
		 					"address1" => (empty($result['address1'])) ? "" : $gb->eat($result['address1'], $honeyandthebee),
		 					"address2" => (empty($result['address2'])) ? "" : $gb->eat($result['address2'], $honeyandthebee),
		 					'city' => $result['city'],
		 					'stateCode' => $result['stateCode'],
		 					'zipCode' => $result['zipcode']
		 				);
				array_push($this->distKegs, $kegArray);
			}
			return true;
		}

 		return false;
 	}/*Close distKegs*/

 	/*
 	 *public function: lastDayPours
 	 *Calculates the Total Amount of Volume used from the Last Day the Keg was in use
 	 *$kegMac: Mac Address of Coupler on Keg Device
 	 *$cal:	calibration value for keg
 	 *$capacity: total Capacity for Keg Type
 	 *$empty: Empty Analog Value for Keg Type
 	 *$volume: Volume Measure from Keg Settings
 	 *returns: array [Number of Ounces Used, Date of Usage, Flow Rate for the Day, Total Time Keg was pouring]
 	 *TODO: This should also be since UNTAP Event - If the keg is UNTAPPED, the next pour after that should be the start instead of the first pour of the day
 	 */
 	public function lastDayPours($kegMac, $cal, $cap,$empty,$volume){
 		global $db;

 		$date = $db->max(KEGDATA,array('receivedDateTime'),array('kegMac' => $kegMac));
 		$date2 = new DateTime($date);

 		$data = $db->select(KEGDATA, array('endVal'), array('AND' => array('kegMac' => $kegMac, 'receivedDateTime[>]' => $date2->format('Y-m-d'))));

 		$totalTime = $db->sum(KEGDATA, array('elapsedTenths'), array('AND' => array('kegMac' => $kegMac, 'receivedDateTime[>]' => $date2->format('Y-m-d'))));

 		$first = current($data);
 		$last = end($data);
 		
 		$startOz = $this->calLevels($first['endVal'],$cal, $cap,$empty,$volume);
 		$endOz = $this->calLevels($last['endVal'],$cal, $cap,$empty,$volume);
 		$totalOz = $endOz-$startOz;
 		if($totalTime != 0){
 			$dayFlowRate = round($totalOz/($totalTime/10), 2, PHP_ROUND_HALF_DOWN);
 		}else{
 			$dayFlowRate = 0;
 		}
 		return array($endOz-$startOz, $date,$dayFlowRate, $totalTime);
 	}

 	/*
 	 *public function: last50
 	 *Pulls the Last 50 Records for an Account from kegdata for received data across all kegs
 	 *returns: array of Last 50 Records with Converting or Calculated Values
 	 */
 	public function last50(){
 		global $db;
 		global $user;
 		$kegMacArr = Array();

 		foreach($this->keg as $keg){
 			array_push($kegMacArr, $keg['kegMac']);
 		}

 		$kegMacStr = implode("','", $kegMacArr);

		$table = 'kegdata';
		$data = array('hubMac','deviceID', 'accountID', 'kegMac', 'deviceName', 'typeID', 'productID', 'temperatureType', 'volumeType', 'showDevice',
 			'sentDateTime', 'startVal', 'pourVal', 'endVal', 'tempCelcius', 'elapsedTenths', 'status',
 			'receivedDateTime', 'kegType', 'kegTypeDesc', 'kegTypeMeasurement', 'amount', 'empty', 'pic_prefix', 'name'
 		);
 		//$where = array('kegMac' => $kegMacArr);
 		$where['ORDER'] = 'receivedDateTime DESC'; 
 		$where['LIMIT'] = array(0,100);

 		$results = $db->select($table,$data,$where);

		$last50 = array();
		$timezone = $user->getAccountTimeZone();

		$accountTimeZone = (empty($timezone)) ? new DateTimeZone('America/Chicago') : new DateTimeZone($timezone);

		foreach($results as $result){
			$cal = $this->calibrate($result['deviceID']);
			$cap = (empty($result['amount']) ? 1984 : $result['amount']);
 				 				
 			$rcvdDtTm = new DateTime($result['receivedDateTime']);
 			$rcvdDtTm->setTimezone($accountTimeZone);
 				//$rcvdDtTm->setTimezone($timezoneCST);
			$row = array(
				'hubMac' => $this->formatMacAddress($result['hubMac']),
				'accountID' => $result['accountID'],
				'acctDevName' => $result['deviceName'],
				'sentDtTm' => $result['sentDateTime'],
				'kegMac' => $this->formatMacAddress($result['kegMac']),
				"kegTypeNum" => $result['typeID'],
				"kegType" => $result['kegTypeDesc'],
				"kegCapacity" => $result['amount'],
				"kegMeasurement" => $result['kegTypeMeasurement'],
				"kegTypePic" => $result['pic_prefix'],
				'startVal' => $result['startVal'],
				'startOZ' => ($result['status'] == '10000011') ? $this->calLevels($result['startVal'], $cal, $cap, $result['empty'], $result['volumeType']) : 'N/A',
				'pourVal' => ($result['status'] == '10000011') ? $result['pourVal'] : 'N/A',
				'endVal' => ($result['status'] == '10000011') ? $result['endVal'] : 'N/A',
				'endOZ' => ($result['status'] == '10000011') ? $this->calLevels($result['endVal'],$cal, $cap, $result['empty'], $result['volumeType']) : 'N/A',
				'elTenths' => ($result['status'] == '10000011') ? ($result['elapsedTenths']/10) : 'N/A',
				'tempVal' => ($result['status'] == '10000011') ? $result['tempCelcius'] : 'N/A',
				'tempCelcius' => ($result['status'] == '10000011') ? $this->calTemp($result['tempCelcius'], $result['temperatureType']) : 'N/A',
				'kegStatus' => $result['status'],
				'volumeType' => $result['volumeType'],
				'temperatureType' => $result['temperatureType'],
				'statusMessage' => $this->getStatus($result['status']),
				'rcvdDtTm' => $rcvdDtTm->format('M d, Y g:i:s a')
			);

			array_push($last50, $row);
		}

		return $last50;
 	}

 	/*
 	 *public function: calLevels
 	 *Calculates the current level for a keg
 	 *$rawValue: the raw analog value of the keg (currently using Pour End)
 	 *$calibration: the calibration value of the keg
 	 *$capacity: the full capacity of the keg
 	 *$empty: the empty analog value of the keg
 	 *$volume: volume type set in keg settings for the keg
 	 *returns: int of number ounces left in keg
 	 */
 	public function calLevels($rawValue, $calibration, $capacity, $empty, $volume){
 		//2250 is empty
		//500 is full
		//1984 ounces in a full keg.
		//1690 ounces in a euro keg.
		//661 ounces in a cylinder
		//992 ounces in a pony
		//992 in a 1/4 barrel
 		//1- [(raw value - 500 +calibration)/(1750)] = fraction full
		//fraction full x 1984 = ounces  in keg

 		$div = $empty - 500;

 		if(isset($rawValue)){
			$ounces = 1 - ((($rawValue - 500 + $calibration)/$div));
			$ounces = round($ounces*$capacity, 0, PHP_ROUND_HALF_DOWN);
		}else{
			$ounces = 1984;
		}

		switch($volume){
			case 'g':
				$ounces = round($ounces*.0078125, 2, PHP_ROUND_HALF_DOWN);
			break;
			case 'l':
				$ounces = round($ounces*.0295735, 2, PHP_ROUND_HALF_DOWN);
			break;
			case 'ml':
				$ounces = round($ounces*29.5735, 2, PHP_ROUND_HALF_DOWN);
			break;
		}

		return $ounces;
 	}


 	/*public function: calTemp
	 *Calculates the temperature of the keg from the raw analog value
	 *$rawValue: raw analog value
	 *$temperature: C or F set in Keg Settings
	 *returns: int of converted temperature
	 */
 	public function calTemp($rawValue, $temperature){
 		$temp = ($rawValue) ? round((((.659 - ((5-($rawValue*.001221))/4))/.00132)-40), 1, PHP_ROUND_HALF_DOWN): 'N/A';
 		if($temperature == 'N/A' || $temperature == 'C'){
			return $temp;
		}else{
			if($temperature == "F"){
				$temp = ($temp * (9/5)) + 32;
				$temp = round($temp,1,PHP_ROUND_HALF_DOWN);
			}
		}
		return $temp;
	}

	/*public function: calibrate
	 *Calculates the calibration value for the keg to use in level calculations
	 *$deviceMac: mac address of keg device
	 *returns: int of calculated calbration value
	 *TODO: Change $deviceMAC to $deviceID
	 *TODO: Figure out how to account for Temperature Calibration OFFSET
	 */
	public function calibrate($deviceMAC){
		global $db;
		global $err;
 
		$table = 'kegdata';
		$column = 'startVal';
		$where = array('AND' => array('deviceID' => $deviceMAC, 'status' => '10000101'));

		$avg = $db->avg($table,$column,$where);

		$cal = $avg - 164;
		return $cal;
	}

	/*public function: deleteNotification
	 *Deletes a notification for a keg
	 *$id: id of notification
	 *returns:success of notification, id of notification in JSON format
	 */
	public function deleteNotification($id){
		global $db;
		global $validate;
		global $err;

		$vData = array('id' => $id);
		$vRules = array('id' => 'required|isNumber');
		$valid = $validate->validate($vRules, $vData);
		if($valid){
			$data = array('deleted' => 1);
			$where = array('id' => $id);

			$update = $db->update(DEVICE_NOTIFICATIONS,$data,$where);
			if($update == 1){
				$return = '{"success":true,"id":'.$id.'}';
			}else{
				$error = $db->error();
				$lastquery = str_replace('"', '', $db->last_query());
				$return = '{"success":false,"id":'.$id.'}';
			}
		}else{
			$return = '{"success":false,"id":'.$id.'}';
		}
		

		return $return;
	}

	/*public function: findKeg
	 *Finds a singular keg in the array based on a search value
	 *$value: any value that can be found in the protected keg array
	 *returns: array of singular keg device data
	 *TODO: allow specification of key as optional value
	 */
	public function findKeg($value){
		//$this->keg is an array that contains as associative array
		//we want to search on the associative array
		foreach($this->keg as $keg){

			foreach ($keg as $key => $val){
				if($keg[$key] == $value){
					return $keg;
				}
			}
		}

		return false;
	}

 	/*public function: formatMacAddress
 	 *formats a mac address to the last 6 characters with colons
 	 *$mac: unformatted mac address
 	 *returns: string of formatted mac address
 	 */
 	public function formatMacAddress($mac){
 		$formatted = substr($mac, 6, 2);
 		$formatted .= ':';
 		$formatted .= substr($mac, 8, 2);
 		$formatted .= ':';
 		$formatted .= substr($mac, 10, 2);
 		return $formatted;
 	}

 	/*public function: getKegTypes
 	 *gets the different types of kegs as stored in database
 	 *returns: array of keg types and definitons or false for no retreival
 	 */
 	public function getKegTypes(){
 		global $db;
 		global $err;

 		$data = array('id','kegType', 'kegTypeDesc');

 		$kegTypes = $db->select(KEGTYPES,$data);
 		$error = $db->error();
 		$lastquery = $db->last_query();

 		if($error[0] == '00000'){
 			return $kegTypes;
 		}else{
 			$err->setError("Cannot retrieve Container Types");
 		}

 		return false;
 	}//end function getKegTypes

 	/*public function: getKegContents
 	 *gets the different types of products that are stored in the products table for a keg
 	 *$prodNum: the specific id of a product
 	 *$limit: how many rows should we look for
 	 *$isJson: isJson: return array (0), return JSON structure (1)
 	 *$search: partial or full name of product to search for
 	 *returns: array or JSON structure of product data from products table
 	 */
 	public function getKegContents($prodNum = 0, $limit = -1, $isJson = 0, $search = ''){
 		global $db;
 		global $err;
 		$where = array();

 		$data = array('id', 'name');
 		if($prodNum){
 			$where['id'] = $prodNum;
 		}else if($search != ''){
 			$where['LIKE'] = array('name' => $search);
 		}

 		$where['ORDER'] = 'name';
 		if($limit != -1){
 			$where['LIMIT'] = $limit;
 		}

 		$results = $db->select(BEERS, $data, $where);
 		$error = $db->error();
 		$lastquery = $db->last_query();
 		
 		if($error[0] == '00000'){
 			if($isJson){
 				return json_encode($results);
 			}else{
 				return $results;
 			}
 		}else{
 			$err->setError("Cannot retrieve keg contents");
 		}

 		return false;
 	}//end function getKegContents


 	/*public function: getKegs
 	 *$kegNum: specific keg to get(any value in kegs array) or 0 for all kegs
 	 *isJson: return array (0), return JSON structure (1)
 	 *returns: array of keg(s) or JSON structure of keg(s)
 	 */
 	public function getKegs($kegNum = 0, $isJson = 0){
 		$kegs = ($kegNum == 0) ? $this->keg : $this->findKeg($kegNum);
 		$kegs = ($isJson == 0) ? $kegs : json_encode($kegs);
 		return $kegs;
 	}//end function getKegs

 	/*
 	 *public function: getDistKegs
 	 *gets the distKegs array
 	 *returns: $this->distKegs
 	 */
 	public function getDistKegs(){
 		return $this->distKegs;
 	}

 	/*public function: getNotifications
 	 *$device: deviceID or 'ALL'
 	 *$type: specification notification type ('L', 'E', 'U', 'P', 'T') or 0
 	 *returns: notification array data for keg(s) with type specification
 	 */
 	public function getNotifications($device,$type){
 		global $db;
 		global $err;

 		/*Notes for notification, frequency and operator
 			notification ('L','E','U','P','T')
 			L: Level Low Value -> Level < $value
 			E: Is Keg Empty
 			U: Keg has been Untapped (This listens for status 1000101 or 1000111 <- that should be error on device though)
 			P: Has pour been detected in between operating hours
 			T: Temperature <> $value

 			frequency
 			Descriptions stored in notification_frequency table

 			operator
 			< less than value
 			> greater than value
 		*/
 		$data = array(DEVICE_NOTIFICATIONS.'.id', 'kegdeviceID', 'notification', 'value', 'frequency', NOTIFICATION_FREQ.'.freqDesc', 'operator', 'sentDateTime', 'active');
 		$join = array('[>]'.NOTIFICATION_FREQ => array('frequency' => 'id'));
 		if($device == 'all'){
			$where = array('AND' => array('accountID' => $user->getAccountNumber(), 'deleted' => 0));
		}else{
			$where = array('AND' => array('kegdeviceID' => $device, 'deleted' => 0));
		}

		$results = $db->select(DEVICE_NOTIFICATIONS,$join,$data,$where);
		//var_dump(str_replace('"', '', $db->last_query()));
		$notifications = array();
		if(!empty($results)){
			foreach($results as $result){
				switch($result['notification']){
					case 'L':
						$nt = 'Low Level';
					break;
					case 'E':
						$nt = 'Empty';
					break;
					case 'U':
						$nt = 'Untap';
					break;
					case 'P':
						$nt = 'Pour After Hours';
					break;
					case 'T':
						$nt = 'Temperature';
					break;
				}
				$notification = array(
					'deviceID' => $result['kegdeviceID'],
					'notificationID' => $result['id'],
					'notification' => $result['notification'],
					'value' => $result['value'],
					'frequencyID' => $result['frequency'],
					'freqDescription' => $result['freqDesc'],
					'operator' => ('operator' == '>') ? 'Greater Than' : 'Less Than',
					'sentDateTime' => $result['sentDateTime'],
					'active' => $result['active']
				);
				array_push($notifications,$notification);
			}
		}

 		return $notifications;
 	}//end function getNotifications

 	/*public function: getPourHistory
 	 *$device: deviceID
 	 *returns: array of pour times in JSON format
 	 */
 	public function getPourHistory($device){
 		global $db;
 		global $err;
 		global $user;

 		$timezone = $user->getAccountTimeZone();
		$accountTimeZone = (empty($timezone)) ? new DateTimeZone('America/Chicago') : new DateTimeZone($timezone);

 		$pourTimes = array();
 		$data = array('receivedDateTime','elapsedTenths', 'deviceName');
 		$where = array('AND' => array('deviceID' => $device,'status' => '10000011'), 'LIMIT' => 20);
 		$results = $db->select(KEGDATA, $data, $where);
 		foreach($results as $result){
 			$dt = new DateTime($result['receivedDateTime']);
 			$dt->setTimezone($accountTimeZone);
 			$pour = array('name' => $result['deviceName'], 'date' => $dt->format('M d, Y g:i:s a'), 'length' => $result['elapsedTenths']/10);
 			array_push($pourTimes,$pour);
 		}
 		

 		return json_encode($pourTimes);
 	}/*Close getPourHistory*/

 	/*public function: getTappedDate
 	 *$device: deviceID
 	 *returns: datetime of last untap event
 	 */
 	public function getTappedDate($device){
 		global $db;
 		global $err;
 		$column = 'receivedDateTime';
 		$where = array('AND' => array('status' => '10000101', 'deviceID' => $device));
 		$tappedDate = $db->max(KEGDATA, $column, $where);

 		return $tappedDate;
 	}/*Close getTappedDate*/

 	/*public function: getTempHistory
 	 *$device: deviceID
 	 *$temperature: keg setting for temperature in C or F
 	 *returns: array of temperature data in JSON format
 	 */
 	public function getTempHistory($device){
 		global $db;
 		global $err;
 		global $user;

 		$data = array('receivedDateTime','tempCelcius', 'temperatureType', 'deviceName');
 		$where = array('AND' => array('deviceID' => $device,'status' => '10000011'), 'LIMIT' => 20);
 		$results = $db->select(KEGDATA,$data,$where);
 		$tempHistory = array();
 		$timezone = $user->getAccountTimeZone();
		$accountTimeZone = (empty($timezone)) ? new DateTimeZone('America/Chicago') : new DateTimeZone($timezone);

 		foreach($results as $result){
 			$dt = new DateTime($result['receivedDateTime']);
 			$dt->setTimezone($accountTimeZone);
 			$temp = array('name' => $result['deviceName'],'date' => $dt->format('M d, Y g:i:s a'), 'temperature' => $this->calTemp($result['tempCelcius'], $result['temperatureType']));
 			array_push($tempHistory, $temp);
 		}

 		return json_encode($tempHistory);
 	}

 	/*public function: getStatus
 	 *$status: binary representation of the status of a keg
 	 *returns: array of human readable status messages
 	 */
 	public function getStatus($status){
 		$statusArray = str_split($status);
 		$statusMessage = array("SendPacket" => 0, 
 			"x1" => 0, "x2" => 0, 'x3' => 0,
 			'StatusPacket' => 0, 'untapped' => 0,
 			'pour' => 0, 'pressureSensor' => 0);
 		if(count($statusArray) == 8){
	 		if($statusArray[0] == 1){
	 			$statusMessage['SendPacket'] = 'Yes';
	 		}

	 		if($statusArray[5] == 1){
	 			$statusMessage['untapped'] = 'Untap Event';
	 		}

	 		if($statusArray[6] == 1){
	 			$statusMessage['pour'] = 'Pour Event';
	 		}

	 		if($statusArray[7] == 1){
	 			$statusMessage['pressureSensor'] = 'OK';
	 		}
	 	}

	 	return $statusMessage;
 	}//end function getStatus


 	/*public function: kegPercent
 	 *$keg: the array data of a singular keg
 	 *returns: the percentage value of beer in a keg, rounded to 5%
 	 */
 	public function kegPercent($keg){
 		$value = $keg['kegEnd'];

 		//if not set keg capacity assume full size keg
 		$cap = (isset($keg['kegCapacity'])) ? $keg['kegCapacity'] : 1984;

 		//should calculate the percentage of beer left in a keg
 		$perc = (100*$value)/$cap;

 		$perc = ($perc < 5) ? '00' : $perc;
 		$perc = ($perc < 10 && $perc >= 5) ? '05' : $perc;
 		$perc = ($perc < 15 && $perc >= 10) ? '10' : $perc;
 		$perc = ($perc < 20 && $perc >= 15) ? '15' : $perc;
 		$perc = ($perc < 25 && $perc >= 20) ? '20' : $perc;
 		$perc = ($perc < 30 && $perc >= 25) ? '25' : $perc;
 		$perc = ($perc < 35 && $perc >= 30) ? '30' : $perc;
 		$perc = ($perc < 40 && $perc >= 35) ? '35' : $perc;
 		$perc = ($perc < 45 && $perc >= 40) ? '40' : $perc;
 		$perc = ($perc < 50 && $perc >= 45) ? '45' : $perc;
 		$perc = ($perc < 55 && $perc >= 50) ? '50' : $perc;
 		$perc = ($perc < 60 && $perc >= 55) ? '55' : $perc;
 		$perc = ($perc < 65 && $perc >= 60) ? '60' : $perc;
 		$perc = ($perc < 70 && $perc >= 65) ? '65' : $perc;
 		$perc = ($perc < 75 && $perc >= 70) ? '70' : $perc;
 		$perc = ($perc < 80 && $perc >= 75) ? '75' : $perc;
 		$perc = ($perc < 85 && $perc >= 80) ? '80' : $perc;
 		$perc = ($perc < 90 && $perc >= 85) ? '85' : $perc;
 		$perc = ($perc < 95 && $perc >= 90) ? '90' : $perc;
 		$perc = ($perc < 99 && $perc >= 95) ? '95' : $perc;
 		$perc = ($perc > 99) ? '99' : $perc;
 		return $perc;
 	}//close keg percent function

 	/*public function: newNotification
 	 *$data: array of data from form to update Notification
 	 *$rules: the rules to validate the data
 	 *returns: true on update, false on failure
 	 */
 	public function newNotification($data, $rules){
 		global $db;
 		global $err;
 		global $user;
 		global $validate;

 			$success = $validate->validate($rules,$data);
 			if($success){
 				$nData = array('accountID' => $user->getAccountNumber(),
 					'kegdeviceID' => $data['Keg'],
 					'notification' => $data['Notification'],
 					'value' => $data['Value'],
 					'frequency' => $data['Frequency'],
 					'operator' => $data['Range'],
 					'active' => 0,
 					'deleted' => 0,
 					'modified' => DATE('Y-m-d H:i:s'),
	 				'modifiedBy' => $user->getUserNum()
 				);


 				if($db->insert(DEVICE_NOTIFICATIONS,$nData) > 0){
 					$success = true;
 				}else{
 					$success = false;
 					$err->setError('DEVICE NOTIFICATION DB ERROR');
 				}
 			}

 		return $success;
 	}//close New Notifications

 	/*public function: updateKeg
 	 *$data: data to update for keg
 	 *$rules: rules to validate data for a keg
 	 *returns:true on update, false on failure
 	 */
 	public function updateKeg($data,$rules){
 		global $db;
 		global $err;
 		global $user;
 		global $validate;

 		$isValid = $validate->validate($rules, $data);

 		if($isValid){

	 		$updateData = array(
	 			'deviceName' => $data['kegName'], 
	 			'productID' => $data['prodNum'],
	 			'kegType' => $data['kegType'],
	 			'showDevice' => $data['showDevice'],
	 			'modified' => DATE('Y-m-d H:i:s'),
	 			'modifiedBy' => $user->getUserNum()
	 		);
	 		if($data['distType'] == 'B'){
	 			$updateData['breweryID'] = $data['distbrew'];
	 			$breweryData = array('breweryID' => $data['distbrew']);
	 			$where = array("AND" => array('breweryID' => 0, 'id' => $data['prodNum']));
	 			$breweryUpdate = $db->update(BEERS, $breweryData,$where);
	 		}elseif($data['distType'] == 'D'){
	 			$updateData['distributorID'] = $data['distbrew'];
	 			$count = $db->count(DIST_BEERS, array("AND" => array('beerID' => $data['prodNum'], 'distributorID' => $data['distbrew'])));
	 			if($count == 0){
	 				$dist_beer = $db->insert(DIST_BEERS, array('beerID' => $data['prodNum'], 'distributorID' => $data['distbrew']));
	 			}
	 		}


	 		$where = array('id' => $data['device']);

	 		$isKeg = $this->findKeg($data['device']);

	 		if($isKeg != false){
	 			$updated = $db->update(KEGDEVICES,$updateData,$where);
	 			$error = $db->error();
	 			$lastquery = $db->last_query();

	 			if($error[0] == '00000'){
	 				return true;
	 			}{
	 				$err->setError('Could not update keg');
	 			}
	 		}else{
	 			$err->setError('Keg does not exist.');
	 		}
		}

 		return false;
 	}//close update keg function

 	/*public function: updateKegSettings
 	 *$device: the device id for a singular keg or "all" to update all kegs for an account
 	 *$data: the data to update in the table
 	 *$rules: the rules to validate the data
 	 *returns: true on update, false on failure
 	 */
 	public function updateKegSettings($device,$data,$rules){
 		global $db;
 		global $err;
 		global $user;
 		global $validate;

 		$isValid = $validate->validate($rules,$data);
 		if($isValid){
 			if(isset($data['Temperature']) && $data['Temperature'] != '') $updateData['temperatureType'] = $data['Temperature'];
 			if(isset($data['Volume']) && $data['Volume'] != '') $updateData['volumeType'] = $data['Volume'];

 			if($device == 'all'){
 				$where = array('accountID' => $user->getAccountNumber());
 			}else{
 				$where = array('id' => $device);
 			}
 			
 			

 			$updated = $db->update(KEGDEVICES,$updateData,$where);
 			$error = $db->error();
 			$lastquery = $db->last_query();

 			if($error[0] == '00000' && is_null($error[2])){
 				return true;
 			}else{
 				$err->setError('Error 7001: Could not update keg settings');
 			}
 		
 		}else{
 			$err->setError('Error 7002: Could not validate keg settings');
 		}

 		return false;
 	}//close updateKegSettings

 	/*public function: updateNotification
 	 *$data: array of data from form to update Notification
 	 *$rules: the rules to validate the data
 	 *returns: true on update, false on failure
 	 */
 	public function updateNotification($data, $rules){
 		global $db;
 		global $err;
 		global $user;
 		global $validate;

 			$success = $validate->validate($rules,$data);
 			if($success){
 				$nData = array(
 					'kegdeviceID' => $data['Keg'],
 					'notification' => $data['Notification'],
 					'value' => $data['Value'],
 					'frequency' => $data['Frequency'],
 					'operator' => $data['Range'],
 					'active' => 0,
 					'deleted' => 0,
 					'modified' => DATE('Y-m-d H:i:s'),
	 				'modifiedBy' => $user->getUserNum()
 				);

 				$where = array('id' => $data['notificationID']);

 				if($db->update(DEVICE_NOTIFICATIONS,$nData,$where) > 0){
 					$success = true;
 				}else{
 					$success = false;
 					$err->setError('DEVICE NOTIFICATION DB ERROR');
 				}
 			}

 		return $success;
 	}//close Update Notifications
 }//close class keg