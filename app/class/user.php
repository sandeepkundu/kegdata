<?php
/*
 *KegData.com
 *USERS CLASS
 *dependent upon medoo.min.php -> should be called with global $db
 *dependent upon error.php -> error reporting with users
 *Version 1 (2014-11-16)
*/

class user{

	protected $user = array(
		"id" 		=> 	0,
		"login" 		=> 	"guest",
		"accountNum"	=> 	0,
		"firstName" 	=> 	"",
		"lastName" 		=>	"",
		"phone1"		=>	"",
		"phone2"		=>	"",
		"email2"		=>	"",
		"active"		=>	0,
		"admin"			=>	0,
		'verificationSent'	=> 0,
		'verificationDateTime' => 0
	);

	protected $account = array(
		"accountNum"	=>	0,
		"name"			=>	"",
		"type"			=> 	"",
		"email1"		=>	"",
		"email2"		=>	"",
		"phone1"		=>	"",
		"phone2"		=>	"",
		"website"		=>	"",
		"addr1"			=>	"",
		"addr2"			=>	"",
		"city"			=>	"",
		"county"		=>	"",
		"zip"			=>	"",
		"state"			=>	"",
		"country"		=>	"",
		"currency"		=>	"",
		"timeZoneID"	=>	"",
		'timezone'		=> 	"",
		'region'		=>	"",
		"DST"			=>	"",
		'dayStart'		=>  "",
		'dayEnd'		=>  "",
		'weekStart'		=>  ""
	);

	protected $brew_dist = array(
		"id"					=> 0,  //brewery or distributor id to link to the breweries or distributors table
		"accountID"				=> 0,  //link to the accounts table
		"name"					=> "", //the name of the brewery - distributor; should be the same as the account name
		"primaryContactID"		=> 0,  //link to users id
		"primaryContactName"	=> "", //link to users table name
		"primaryEmail"			=> "", //email1 in brew_dist view in db
		"reorderEmail"			=> "", //email2 in brew_dist view in the db
		"phone"					=> "", //primary phone number for the brewery/distributor 
		"fax"					=> "", //primary fax number for the brewery/distributor
		"address1"				=> "",
		"address2"				=> "",
		"city"					=> "",
		"stateCode"				=> "",
		"zipCode"				=> "",
		"county"				=> "",
		"countryCode"			=> ""
	);

	/*
	 *public function: setUser
	 *Public function to set up user data
	 *$id: user login id
	 *$pw: user password
	 *returns: $success (Boolean) - true if user set up correctly; false if user set up failed
	 */
	public function setUser($id,$pw){
		global $db;
		global $err;
		global $validate;
		$success = true;
		$rules = array(
			"User Name" => 'required|username'
		);

		if($pw != null){
			$rules["Password"] = 'required|isHash';
		}

		$data = array(
			"User Name" => $id,
			
		);

		if($pw != null){
			$data["Password"] = $pw;
		}

		$valid = $validate->validate($rules, $data);

		if($valid){

			$success = $this->userData($id, $pw);
			if($success && $this->user['accountNum'] > 0){
				$success = $this->accountData();
			}
			if($success && ($this->account['type'] == 'B' || $this->account['type'] == 'D')){
				$success = $this->distData();
			}
		}else{
			$success = false;
		}

		return $success;
	}/*Close setUser function*/

	/*
	 *private function: userDate
	 *Function that retrieves user data from database and sets up the protected user array for the user class
	 *$id: user id to search on
	 *$pw: user password to confirm user
	 *returns: $success (Boolean) true on user variable being set; false on failure
	 */
	private function userData($id, $pw){
		global $db;
		global $err;
		global $gb;
		global $honeyandthebee;

		$data = array(USERS.'.id', USERS.'.login', USERS.'.firstName', USERS.'.lastName',
				USERS.'.phone1', USERS.'.phone2', USERS.'.email2', USERS.'.isActive',
				USERS.'.isAdmin', USERS.'.account_id', USER_VERIFICATION.'.verificationSent', USER_VERIFICATION.'.verificationConfirmation');
		
		$join = array("[>]".USER_VERIFICATION => array("id" => "id"));
		if($pw != null){
			$where = array("AND" => array("login" => $gb->bake($honeyandthebee, $id), "password" => $pw, "isActive" => 1),
				"LIMIT" => 1);
		}else{
			$where = array("AND" => array("login" => $gb->bake($honeyandthebee, $id), "isActive" => 1),
				"LIMIT" => 1);
		}
		
		$results = $db->select(USERS, $join, $data, $where);
		$error = $db->error();
		$lastQuery = $db->last_query();
		$success = true;

		if($error['0'] == '00000' && $results != ''){
			$this->user['id'] = $results[0]['id'];
			$this->user['login'] = $gb->eat($results[0]['login'], $honeyandthebee);
			$this->user['accountNum'] = $results[0]['account_id'];
			$this->user['firstName'] = $gb->eat($results[0]['firstName'], $honeyandthebee);
			$this->user['lastName'] = $gb->eat($results[0]['lastName'], $honeyandthebee);
			$this->user['phone1'] = (empty($results[0]['phone1'])) ? '' :$gb->eat($results[0]['phone1'], $honeyandthebee);
			$this->user['phone2'] = (empty($results[0]['phone2'])) ? '' : $gb->eat($results[0]['phone2'], $honeyandthebee);
			$this->user['email2'] = (empty($results[0]['email2'])) ? '' :$gb->eat($results[0]['email2'], $honeyandthebee);
			$this->user['active'] = $results[0]['isActive'];
			$this->user['admin'] = $results[0]['isAdmin'];
			$this->user['verificationSent'] = $results[0]['verificationSent'];
			$this->user['verificationDateTime'] = $results[0]['verificationConfirmation'];
		}
		else{
			if(IS_DEBUG){
				$err->setError("USER ERROR" + $error[2] + '<br />' + $lastQuery);
			}else{
				$err->setError("Cannot find user. Please try again.");
			}
			$success = false;
		}
		return $success;
	}/*Close userData Function*/

	/*
	 *private function: accountData
	 *Function that retrieves account data for an user and sets it in the protected account array
	 *Dependency: $this->user['accountID'] being set from userData fucntion
	 *returns: $success (Boolean) - true on account being set; false on failure
	 */
	private function accountData(){
		global $db;
		global $err;
		global $gb;
		global $honeyandthebee;

		$join = array("[>]".ACCOUNT_ADDRESSES => array("id" => "accountID"),
			"[>]".ACCOUNT_SETTINGS => array("id" => "accountID"),
		);
		$data = array(ACCOUNTS.'.id', ACCOUNTS.'.accountName', ACCOUNTS.'.accountType', ACCOUNTS.'.primaryContact',
			ACCOUNTS.'.accountEmail1', ACCOUNTS.'.accountEmail2', ACCOUNTS.'.accountPhone1', ACCOUNTS.'.accountPhone2', ACCOUNTS.'.website',
			ACCOUNT_ADDRESSES.'.address1', ACCOUNT_ADDRESSES.'.address2',ACCOUNT_ADDRESSES.'.city',
			ACCOUNT_ADDRESSES.'.stateCode', ACCOUNT_ADDRESSES.'.zipcode', ACCOUNT_ADDRESSES.'.county',
			ACCOUNT_ADDRESSES.'.countryCode', ACCOUNT_ADDRESSES.'.latitude', 
			ACCOUNT_ADDRESSES.'.longitude', ACCOUNT_SETTINGS.'.currency', ACCOUNT_SETTINGS.'.daylightSavings', ACCOUNT_SETTINGS.'.timezone',
			ACCOUNT_SETTINGS.'.dayStart',ACCOUNT_SETTINGS.'.dayEnd', ACCOUNT_SETTINGS.'.weekStart'
		);

		$success = true;
		if($this->user['accountNum'] != 0){
			$where = array(ACCOUNTS.'.id' => $this->user['accountNum']);
			$results = $db->select(ACCOUNTS,$join, $data,$where);

			$error = $db->error();

			$lastQuery = $db->last_query();

			$timezone = $this->loadTimezone($results[0]['timezone']);

			if($error[0] == '00000' && $results != ''){
				$this->account['accountNum'] = $results[0]['id'];
				$this->account['name'] = $gb->eat($results[0]['accountName'], $honeyandthebee);
				$this->account['type'] = $results[0]['accountType'];
				$this->account['email1'] = $gb->eat($results[0]['accountEmail1'], $honeyandthebee);
				$this->account['email2'] = $gb->eat($results[0]['accountEmail2'], $honeyandthebee);
				$this->account['phone1'] = $gb->eat($results[0]['accountPhone1'], $honeyandthebee);
				$this->account['phone2'] = $gb->eat($results[0]['accountPhone2'], $honeyandthebee);
				$this->account['website'] = $results[0]['website'];
				$this->account['addr1'] = $gb->eat($results[0]['address1'], $honeyandthebee);
				$this->account['addr2'] = $gb->eat($results[0]['address2'], $honeyandthebee);
				$this->account['city'] = $results[0]['city'];
				$this->account['county'] = $results[0]['county'];
				$this->account['zip'] = $results[0]['zipcode'];
				$this->account['state'] = $results[0]['stateCode'];
				$this->account['country'] = $results[0]['countryCode'];
				$this->account['currency'] = $results[0]['currency'];
				$this->account['timeZoneID'] = $results[0]['timezone'];
				$this->account['timezone'] = $timezone['timezone'];
				$this->account['region'] = $timezone['region'];
				$this->account['DST'] = $results[0]['daylightSavings'];
				$this->account['dayStart'] = $results[0]['dayStart'];
				$this->account['dayEnd'] = $results[0]['dayEnd'];
				$this->account['weekStart'] = $results[0]['weekStart'];
			}else{
				if(IS_DEBUG){
					$err->setError("Account ERROR" + $error[2] + '<br />' + $lastQuery);
				}else{
					$err->setError("Cannot find account. Please try again.");
				}
				$success = false;
			}
		}else{
			$success = false;
		}

		return $success;
	}/*Close accountData function*/

	/*
	 *public function: distData
	 *Data that retrieves Distributor/Brewery Account Data from brew_dist view
	 *dependency: $this->account must first be set
	 *returns: $success (Boolean) - true if $this->brew_dist is set, false if not
	 */
	private function distData(){
		global $db;
		global $err;
		global $gb;
		global $honeyandthebee;

		$data = array('id', 'accountID', 'name', 'primaryContactID', 'primaryContactName', 'email1', 'email2',
			'phone', 'fax', 'address1', 'address2', 'city', 'stateCode', 'zipcode', 'county', 'countryCode'
		);

		$where = array('accountID' => $this->account['accountNum']);

		$result = $db->get(BREW_DIST, $data, $where);
		$error = $db->error();
		if(count($result) > 0 && empty($error['2'])){
			$this->brew_dist['id']= $result['id'];  //brewery or distributor id to link to the breweries or distributors table
			$this->brew_dist['accountID']= $result['accountID'];  //link to the accounts table
			$this->brew_dist['name']= $result['name']; //the name of the brewery - distributor; should be the same as the account name
			$this->brew_dist['primaryContactID']= $result['primaryContactID'];  //link to users id
			$this->brew_dist['primaryContactName']= (empty($result['primaryContactName'])) ? '' :$gb->eat($result['primaryContactName'], $honeyandthebee); //link to users table name
			$this->brew_dist['primaryEmail']= (empty($result['email1'])) ? '' :$gb->eat($result['email1'], $honeyandthebee); //email1 in brew_dist view in db
			$this->brew_dist['reorderEmail']= (empty($result['email2'])) ? '' :$gb->eat($result['email2'], $honeyandthebee); //email2 in brew_dist view in the db
			$this->brew_dist['phone']= (empty($result['phone'])) ? '' :$gb->eat($result['phone'], $honeyandthebee); //primary phone number for the brewery/distributor 
			$this->brew_dist['fax']= (empty($result['fax'])) ? '' :$gb->eat($result['fax'], $honeyandthebee); //primary fax number for the brewery/distributor
			$this->brew_dist['address1']= (empty($result['address1'])) ? '' :$gb->eat($result['address1'], $honeyandthebee);
			$this->brew_dist['address2']= (empty($result['address2'])) ? '' :$gb->eat($result['address2'], $honeyandthebee);
			$this->brew_dist['city']= $result['city'];
			$this->brew_dist['stateCode']= $result['stateCode'];
			$this->brew_dist['zipCode']= $result['zipcode'];
			$this->brew_dist['county']= $result['county'];
			$this->brew_dist['countryCode']= $result['countryCode'];

		return true;
		}
		return false;
	}/*Close distData*/

	/*
	 *private function: loadTimezone
	 *Retrieves a timezone and region from the Timezone table based on a Timezone ID
	 *$id: timeZone id number
	 *returns: $timezone (Array) ['timezone' => 'PHP Timezone String', 'region' => 'Region of the world']
	 */
	private function loadTimezone($id){
		global $db; 
		
		$tzData = array('timezone','region');
		$where = array('id' => $id);
		$timezone = $db->get(TIMEZONE,$tzData,$where);
		return $timezone;
	}/*Close loadTimezone function*/

	/*
	 *public function: loadUser
	 *Called from login function; calls set user
	 *$userID: user login id
	 *returns: Boolean (TRUE/FALSE) if setUser succeeded
	 *TODO: This is Redundant and Login could just reference $this->setUser
	 */
	public function loadUser($userID){
		if($this->setUser($userID, null)){
			return true;
		}else{
			return false;
		}
	}/*Close loadUser*/

	/*
	 *public function: getAccountType
	 *Function to the account type from the account array
	 *returns: $this->account['type'] - String (Single Letter representing 1 of 5 types of accounts allowed)
	 */
	public function getAccountType(){
		return $this->account['type'];
	}/*Close getAccountType*/

	/*
	 *public function: getAccountID
	 *Function to get the AccountID from the account Array
	 *returns: $this->account['accountNum'] - INT (ID column of Accounts Table)
	 */
	public function getAccountID(){
		return $this->account['accountNum'];
	}/*Close getAccountID*/


	/*
	 *public function: getAccountNumber
	 *Alias of getAccountID; legacy from code before table restructure when AccountID & AccountNum were different values
	 *returns: returns $this->account['accountNum'] - INT (ID column of Accounts Table)
	 *TODO: Refactor code referencing this function and remove this function
	 */
	public function getAccountNumber(){
		return $this->account['accountNum'];
	}/*Close getAccountNumber*/

	/*
	 *public function: getAccountName
	 *Function to return the account name from the account array
	 *returns: $this->account['name'] - String containing the account name
	 */
	public function getAccountName(){
		return $this->account['name'];
	}/*Close getAccountName*/

	//get the current user phone number 1
	public function getAccountPhone1(){
		return $this->account['phone1'];
	}/*Close getAccountPhone1*/

	//get the current user phone number 2
	public function getAccountPhone2(){
		return $this->account['phone2'];
	}/*Close getAccountPhone2*/

	//get the full address for the account
	public function getAccountAddress(){
		$address = $this->account['addr1']."<br />";
		$address .= $this->account['addr2']."<br />";
		$address .= $this->account['county']." County<br />";
		$address .= $this->account['city'].", ";
		$address .= $this->account['state']." ";
		$address .= $this->account['zip']."<br />";
		$address .= $this->account['country'];

		return $address;
	}/*close getAccountAddress*/

	//get the account currency setting
	public function getAccountCurrency(){
		return $this->account['currency'];
	}/*close getAccountCurrency*/

	//get the account tempeture type
	public function getAccountTemperatureType(){
		return $this->account['temp'];
	}/*Close getAccountTemperatureType*/

	//get the account volume type
	public function getAccountVolume(){
		return $this->account['volume'];
	}/*Close getAccountVolume*/

	//get the account time zone
	public function getAccountTimeZone(){
		return $this->account['timezone'];
	}/*Close getAccountTimeZone*/

	public function getAccountRegion(){
		return $this->account['region'];
	}/*Close getAccountRegion*/

	//get the account DST setting
	public function getAccountDST(){
		return $this->account['DST'];
	}/*Close getAccountDST*/

	//public function get week start
	public function getWeekStart(){
		return $this->account['weekStart'];
	}/*Close getWeekStart*/

	//public function get Day start
	public function getDayStart(){
		return $this->account['dayStart'];
	}/*Close getDayStart*/

	//public function get Day end
	public function getDayEnd(){
		return $this->account['dayEnd'];
	}/*Close getDayEnd*/

	//get the account Enterprise code
	//TODO: Need to run select query with function
	public function getAccountEnterprise(){
		return $this->account['eCode'];
	}/*Close getAccountEnterprise*/

	//get the account Mailing List Code
	//TODO: Need to run select query with function
	public function getMailListCode(){
		return $this->account['mCode'];
	}/*Close getMailListCode*/

	//get the full current account
	public function getAccount(){
		return $this->account;
	}/*Close getAccount*/

	/*
	 *public function: getDistributor
	 *get the full $brew_dist variable
	 *returns: returns $this->brew_dist;
	 */
	public function getDistributor(){
		return $this->brew_dist;
	}/*Close getDistributor*/

	/*
	 *public function: getDistID
	 *get the brewery/distributor id from $this->brew_dist
	 *returns: $this->brew_dist['id']
	 */
	public function getDistID(){
		return $this->brew_dist['id'];
	}/*Close getDistID*/

	/*
	 *public function: getDistName
	 *get the distributor/brewery name from $this->brew_dist
	 *returns: $this->brew_dist['name']
	 */
	public function getDistName(){
		return $this->brew_dist['name'];
	}/*Close getDistName*/

	/*
	 *public function: getDistPCID
	 *get the brewery/distribut primary contact id
	 *: 
	 *returns: $this->brew_dist['primaryContactID']
	 */
	public function getDistPCID(){
		return $this->brew_dist['primaryContactID'];
	}/*Close getDistPCID*/

	/*
	 *public function: getDistPCName
	 *get the brewery/distributor primary contact name
	 *: 
	 *returns: $this->brew_dist['primaryContaName']
	 */
	public function getDistPCName(){
		return $this->brew_dist['primaryContactName'];
	}/*Close getDistPCName*/

	/*
	 *public function: getDistPrimaryEmail
	 *gets the brewery/distributor primary email
	 *: 
	 *returns: $this->brew_dist['primaryEmail']
	 */
	public function getDistPrimaryEmail(){
		return $this->brew_dist['primaryEmail'];
	}/*Close getDistPrimaryEmail*/

	/*
	 *public function: getDistReorderEmail
	 *gets the brewery/distributor reorder email
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['reorderEmail']
	 */
	public function getDistReorderEmail(){
		return $this->brew_dist['reorderEmail'];
	}/*Close getDistReorderEmail*/

	/*
	 *public function: getDistPhone
	 *gets the brewery/distributor phone
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['phone']
	 */
	public function getDistPhone(){
		return $this->brew_dist['phone'];
	}/*Close getDistPhone*/

	/*
	 *public function: getDistFax
	 *gets the brewery/distributor fax
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['fax']
	 */
	public function getDistFax(){
		return $this->brew_dist['fax'];
	}/*Close getDistFax*/

	/*
	 *public function: getDistAddress1
	 *gets the brewery/distributor address 1
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['address1']
	 */
	public function getDistAddress1(){
		return $this->brew_dist['address1'];
	}/*Close getDistAddress1*/

	/*
	 *public function: getDistAddress2
	 *gets the brewery/distributor address 2
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['address2']
	 */
	public function getDistAddress2(){
		return $this->brew_dist['address2'];
	}/*Close getDistAddress2*/

	/*
	 *public function: getDistCity
	 *gets the brewery/distributor city
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['city']
	 */
	public function getDistCity(){
		return $this->brew_dist['city'];
	}/*Close getDistCity*/

	/*
	 *public function: getDistState
	 *gets the brewery/distributor state code
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['stateCode']
	 */
	public function getDistState(){
		return $this->brew_dist['stateCode'];
	}/*Close getDistState*/

	/*
	 *public function: getDistZip
	 *gets the brewery/distributor zipcode
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['zipcode']
	 */
	public function getDistZip(){
		return $this->brew_dist['zipcode'];
	}/*Close getDistZip*/

	/*
	 *public function: getDistCounty
	 *gets the brewery/distributor county
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['county']
	 */
	public function getDistCounty(){
		return $this->brew_dist['county'];
	}/*Close getDistCounty*/

	/*
	 *public function: getDistCountryCode
	 *gets the brewery/distributor country code
	 *acceptVariable: variableDesc
	 *returns: $this->brew_dist['countryCode']
	 */
	public function getDistCountryCode(){
		return $this->brew_dist['countryCode'];
	}/*Close getDistCountryCode*/

	//get the full current user
	public function getUser(){
		return $this->user;
	}/*Close getUser*/

	//get the current user num
	public function getUserNum(){
		return $this->user['id'];
	}/*Close getUserNum*/

	//get the current user id
	public function getUserId(){
		return $this->user['login'];
	}/*Close getUserId*/

	//get the current user name
	public function getUserName(){
		if($this->user['firstName'] == '' && $this->user['lastName'] == ''){
			return "Guest";			
		}else{
			return $this->user['firstName'].' '.$this->user['lastName'];
		}
	}/*Close getUserName*/

	//get user first name
	public function getUserFirstName(){
		return $this->user['firstName'];
	}/*Close getUserFirstName*/

	public function getUserLastName(){
		return $this->user['lastName'];
	}/*Close getUserLastName*/

	//get user phone 1
	public function getUserPhone1(){
		return $this->user['phone1'];
	}/*Close getUserPhone1*/

	//get user phone 2
	public function getUserPhone2(){
		return $this->user['phone2'];
	}/*Close getUserPhone2*/

	//get user email 1
	public function getUserEmail1(){
		return $this->user['email1'];
	}/*Close getUserEmail1*/

	//get user email 2
	public function getUserEmail2(){
		return $this->user['email2'];
	}/*Close getUserEmail2*/

	//is user still active
	public function isActive(){
		return $this->user['isActive'];
	}/*Close isActive*/

	//check if user isAdmin
	public function isAdmin(){
		return $this->user['admin'];
	}/*Close isAdmin*/

	//check if user isSysAdmin :: for KegData Employee Functions
	public function isSysAdmin(){
		return 0;
	}/*Close isSysAdmin*/

	//check if user has had verification sent
	public function getVerificationSent(){
		return $this->user['verificationSent'];
	}/*Close getVerificationSent*/

	//check if user has verified account
	public function getVerificationDate(){
		return $this->user['verificationDateTime'];
	}/*Close getVerificationDate*/

	//Check if account exists
	public function accountExists($name, $type){
		global $db;
		global $gb;
		global $honeyandthebee;

		$where = array('AND'=> array('accountType' => $type, 'accountName' => $gb->bake($honeyandthebee, $name)));

		$check = $db->count(ACCOUNTS,$where);

		return $check;
	}/*Close accountExists*/

	//Check if user exists
	public function userExists($email){
		global $db;
		global $gb;
		global $honeyandthebee;

		$where = array('AND' => array('login' => $gb->bake($honeyandthebee, $email)));
		$check = $db->count(USERS,$where);

		return $check;
	}/*Close userExists*/

	//Check if distributor exists
	public function distributorExists($name){
		global $db;

		$data = array('id');
		$where = array('name' => $name);
		$where['limit'] = 1;
		$distributor = $db->select(DISTRIBUTORS, $data, $where);
		return $distributor;
	}/*Close distributorExists*/

	//Check if brewery exists
	public function breweryExists($name){
		global $db;

		$data = array('id');
		$where = array('name' => $name);
		$where['limit'] = 1;
		$brewery = $db->select(BREWERIES, $data, $where);

		return $brewery;
	}/*Close breweryExists*/

	//Register a new account
	public function newAccount($data,$rules, $type){
		global $db;
		global $err;
		global $validate;
		global $gb;
		global $honeyandthebee;


		$valid = $validate->validate($rules,$data);

		if($valid){
			//check if account already exists
			$table = 'Accounts';

			$acctName = $data['Company Name'];

			
			$check = $this->accountExists($acctName,$type);
			$checkU = $this->userExists($data['Email Address']);

			if($check > 0 || $checkU > 0){
				$err->setError('Error 4000: Account already exists. Please try to login.');
				if($type == 'B' || $type == 'D'){
					$err->setError('If you do not have an account, please contact your company to be added to the account.');
				}
			}else{
				 $db->pdo->beginTransaction();
				//ACCOUNTS INSERT
				$Adata = array(
					'accountType' => $type,
					'accountName' => $gb->bake($honeyandthebee, $acctName),
					'accountPhone1' =>$gb->bake($honeyandthebee, $data['Office Phone']),
					'accountPhone2' => $gb->bake($honeyandthebee, $data['Fax']),
					'accountEmail1' => $gb->bake($honeyandthebee, $data['Email Address']),
					'modified' => DATE('Y-m-d H:i:s'),
					'modifiedBy' => 0
				);

				$accountID = $db->insert(ACCOUNTS,$Adata);
				$error = $db->error();
				$cont = 0;
				if($error[0] == '00000' && is_null($error[2])){
					$cont = 1;
				}else{
					$err->setError('Error 4007: Cannot create account.');
					$db->pdo->rollBack();
					return false;
				}

				//ACCOUNT SETTINGS INSERT
				if($cont == 1){
					$settings = array(
						'accountID' => $accountID,
						'currency' => $data['Currency'],
						'volumeType' => 'oz',
						'temperatureType' => 'C',
						'daylightSavings' => 1,
						'timezone' => 0,
						'modified' => DATE('Y-m-d H:i:s'),
						'modifiedBy' => 0
					);
					$db->insert(ACCOUNT_SETTINGS,$settings);
					if($error[0] == '00000'){
						$cont = 1;
					}else{
						$cont = 0;
						$err->setError('Error 4008: Cannot create account.');
						$db->pdo->rollBack();
						return false;
					}
				}
				//ACCOUNT ADDRESSES INSERT
				if($cont == 1){
					$addresses = array(
						'accountID' => $accountID,
						'address1' => $gb->bake($honeyandthebee, $data['Address 1']),
						'address2' => $gb->bake($honeyandthebee, $data['Address 2']),
						'city' => $data['City'],
						'county' => $data['County'],
						'stateCode' => $data['State'],
						'zipcode' => $data['Zip Code'],
						'countryCode' => $data['Country'],
						'latitude' => 0,
						'longitude' => 0,
						'modified' => DATE('Y-m-d H:i:s'),
						'modifiedBy' => 0
					);
					$db->insert(ACCOUNT_ADDRESSES,$addresses);
					if($error[0] == '00000'){
						$cont = 1;
					}else{
						$cont = 0;
						$err->setError('Error 4009: Cannot create account.');
						$db->pdo->rollBack();
						return false;
					}
				}

				//ACCOUNT ENTERPRISE CODE
				if($cont == 1){
					$entExists = $db->count(ENTERPRISE, array('enterpriseCode' => $data['Enterprise Code']));

					if($entExists >= 1){
						$primaryAccount = $db->get(ENTERPRISE, array('primaryAccountID'),
						array('enterpriseCode' => $data['Enterprise Code'], array('primaryAccountID' => 'accountID')));

						$enterprise = array(
							'primaryAccountID' => $primaryAccount['primaryAccountID'],
							'accountID' => $accountID,
							'enterpriseCode' => $data['Enterprise Code'],
							'modified' => DATE('Y-m-d H:i:s'),
							'modifiedBy' => 0
						);
					}else{
						$enterprise = array(
							'primaryAccountID' => $accountID,
							'accountID' => $accountID,
							'enterpriseCode' => $data['Enterprise Code'],
							'modified' => DATE('Y-m-d H:i:s'),
							'modifiedBy' => 0
						);
					}

					$db->insert(ENTERPRISE, $enterprise);
					$error = $db->error();
					if($error[0] == '00000'){
						$cont = 1;
					}else{
						$cont = 0;
						$err->setError('Error 4009: Cannot create account.');
						$db->pdo->rollBack();
						return false;
					}
				}
				

				if($error[0] == '00000' && $accountID > 0){
					$success = true;
					if($success == true){
						$success = $this->newUser($data,$rules,$accountID, 1);
					}	

					if($success == false || $success == 0){
						$err->setError('Error 4010: Cannot create account.');
						$db->pdo->rollBack();
						return false;
					}

					if($success > 0 && $success != false && ($type == 'D' || $type == 'B')){
						$uSuccess = false;
						if($type == 'B'){
							$brewery = $this->breweryExists($acctName);
							if(isset($brewery['id']) && $brewery['id'] > 0){
								$uSuccess = $this->updateBrewery($brewery, $accountID, $success, $data);
							}else{
								$uSuccess = $this->newBrewery($accountID, $success, $data);
							}
						}elseif($type == 'D'){
							$distributor = $this->distributorExists($acctName);
							if(isset($distributor['id']) && $distributor['id'] > 0){
								$uSuccess = $this->updateDistributor($distributor, $accountID, $success, $data);
							}else{
								$uSuccess = $this->newDistributor($accountID, $success, $data);
							}
						}

						if($uSuccess == false){
							$err->setError('Error 4003: Could not add Brewery or Distributor');
							$db->pdo->rollBack();
							return false;
						}
					}

					if($success > 0 && $success != false){
						$db->pdo->commit();
					}else{
						$db->pdo->rollBack();
						$err->setError('Error 4011: Could not create account.');
					}

					return $success;
				}else{
					$err->setError('Error 4001: Could not create account. Please try again.');
					return false;
				}
			}
		}else{
			$err->setError('Error 4002: Could not create account. Please try again.');
		}
		
		return false;
	}/*Close newAccount*/

	//Add a brewery who created an account to our list
	public function newBrewery($accountID, $success, $data){
		global $db;
		global $gb;
		global $honeyandthebee;
		global $err;
		global $validate;

		$breweryData = array('accountID' => $accountID,
				'name' => $data['Company Name'],
				'primaryContactID' => $success,
				'primaryContactName' =>	$gb->bake($honeyandthebee, $data['First Name'].' '.$data['Last Name']),
				'email1' => $gb->bake($honeyandthebee, $data['Email Address']),
				'email2' => '',
				'phone' => $gb->bake($honeyandthebee, $data['Office Phone']),
				'fax' => $gb->bake($honeyandthebee, $data['Fax']),
				'modified' => DATE('Y-m-d H:i:s'),
				'modifiedBy' => 0
		);

		$brewery = $db->insert(BREWERIES, $breweryData);

		if($brewery > 0){
			$addresses = array(
				'breweryID' => $brewery,
				'address1' => $gb->bake($honeyandthebee, $data['Address 1']),
				'address2' => $gb->bake($honeyandthebee, $data['Address 2']),
				'city' => $data['City'],
				'county' => $data['County'],
				'stateCode' => $data['State'],
				'zipcode' => $data['Zip Code'],
				'countryCode' => $data['Country'],
				'latitude' => 0,
				'longitude' => 0,
				'modified' => DATE('Y-m-d H:i:s'),
				'modifiedBy' => 0
			);
			$address = $db->insert(BREWERY_ADDRESSES, $addresses);
			if($address > 0){
				return true;
			}
		}

		return false;

	}/*Close newBrewery*/

	/*
	 *public function: newDistributor
	 *function to add a new distributor
	 *$accountID: account id of the distributor being added
	 *$success: boolean (Don't know the purpose of this)
	 *$data: data from form to add a new distributor
	 *returns: Boolean - True on sucess, false on failure
	 */
	public function newDistributor($accountID, $success , $data){
		global $db;
		global $gb;
		global $honeyandthebee;
		global $err;
		global $validate;


		$distributorData = array('accountID' => $accountID,
			'name' => $data['Company Name'],
			'primaryContactID' => $success,
			'primaryContactName' =>	$gb->bake($honeyandthebee, $data['First Name'].' '.$data['Last Name']),
			'email1' => $gb->bake($honeyandthebee, $data['Email Address']),
			'email2' => '',
			'phone' => $gb->bake($honeyandthebee, $data['Office Phone']),
			'fax' => $gb->bake($honeyandthebee, $data['Fax']),
			'modified' => DATE('Y-m-d H:i:s'),
			'modifiedBy' => 0
		);

		$distributor = $db->insert(DISTRIBUTORS, $distributorData);
		if($distributor > 0){
			$addresses = array(
				'distributorID' => $distributor,
				'address1' => $gb->bake($honeyandthebee, $data['Address 1']),
				'address2' => $gb->bake($honeyandthebee, $data['Address 2']),
				'city' => $data['City'],
				'county' => $data['County'],
				'stateCode' => $data['State'],
				'zipcode' => $data['Zip Code'],
				'countryCode' => $data['Country'],
				'latitude' => 0,
				'longitude' => 0,
				'modified' => DATE('Y-m-d H:i:s'),
				'modifiedBy' => 0
			);
			$address = $db->insert(DIST_ADDRESSES, $addresses);
			if($address > 0){
				return true;
			}
		}

		return false;
	}/*Close newDistributor*/

	/*
	 *public function: newUser
	 *Function to create a new user, one account can have multiple users
	 *$data: array of data
	 *$rules: array of rules to validate data by
	 *$acctNum: account ID to attach this user to
	 *$admin: Boolean (1 or 0) for admin or not 
	 *returns: Boolean - True on success, false on failure
	 */
	public function newUser($data,$rules,$acctNum, $admin){
		global $db;
		global $err;
		global $validate;
		global $gb;
		global $honeyandthebee;

		$valid = $validate->validate($rules,$data);

		if($valid){

			$check = $this->userExists($data['Email Address']);
			if($check > 0){
				$err->setError('Error 4004: User already exists. Please try to login.');
			}else{
				$Udata = array(
					'login' => $gb->bake($honeyandthebee, $data['Email Address']),
					'password' => $data['Password'],
					'account_id' => $acctNum,
					'firstName' => $gb->bake($honeyandthebee, $data['First Name']),
					'lastName' => $gb->bake($honeyandthebee, $data['Last Name']),
					'phone1' => $gb->bake($honeyandthebee, $data['Home Phone']),
					'phone2' => $gb->bake($honeyandthebee, $data['Cell Phone']),
					'email2' => '',
					'isActive' => 1,
					'isAdmin' => $admin,
					'modified' => DATE('Y-m-d H:i:s'),
					'modifiedBy' => 0
				);

				$userID = $db->insert(USERS, $Udata);
				$error = $db->error();

				if($error[0] == '00000'){
					$verification = array(
						'id' => $userID,
						'verificationHash' => md5(rand(0,1000)),
						'verificationSent' => DATE("Y-m-d H:i:s")
					);

					$verification = $db->insert(USER_VERIFICATION, $verification);
					$error = $db->error();
				}

				if($userID > 0 && $error[0] == '00000'){
					return $userID;
				}else{
					$err->setError('Error 4005: User could not be created.');
				}
			}
		}else{
			$err->setError('Error 4003: User could not be created. Please try again.');
		}

		return false;
	}/*Close newUser*/

	/*
	 *public function: updateAccount
	 *Function to update account information
	 *$data: Array of data to be update in accounts, account_addresses
	 *returns: Boolean - True on Success, False on Failure
	 *TODO: Form, Connection Pieces, This Function
	 */
	public function updateAccount($data){
		global $db;
		global $err;
		global $validate;
		global $user;

		return false;
	}/*Close updateAccount*/

	/*
	 *public function: updateAccountSettings
	 *function to update data found in the accounts settings table
	 *$data: Array ['currency', 'dst', 'Timezone', 'Day Start', 'AM/PM Start', 'Day End', 'AM/PM End', 'Week Start']
	 *$rules: Array of Rules to validate data on
	 *returns: Boolean True on Success, False on Failure
	 *TODO: Remove DST from here, form for settings and in DB - Not used and Redundant - PHP will calculate DST
	 */
	public function updateAccountSettings($data, $rules){
		global $db;
		global $err;
		global $validate;

		$isValid = $validate->validate($rules,$data);
		if($isValid){
			$dayStart = date('G:i:s', strtotime($data['Day Start']. ' '.$data['AM/PM Start']));
			$dayEnd = date('G:i:s', strtotime($data['Day End'].' '.$data['AM/PM End']));

			$sData = array('currency' => $data['Currency'],
				'daylightSavings' => $data['dst'],
				'timezone' => $data['Timezone'],
				'dayStart' => $dayStart,
				'dayEnd' => $dayEnd,
				'weekStart' => $data['Week Start'],
				'modified' => DATE('Y-m-d H:i:s'),
				'modifiedBy' => $this->user['id']
			);

			$where = array('accountID' => $this->account['accountNum']);

			$update = $db->update(ACCOUNT_SETTINGS,$sData,$where);
			$error = $db->error();
			if(empty($error[2])){
				$timezone = $this->loadTimezone($data['Timezone']);

				$this->account['currency'] = $sData['currency'];
				$this->account['DST'] = $sData['daylightSavings'];
				$this->account['timeZoneID'] = $sData['Timezone'];
				$this->account['timezone'] = $timezone['timezone'];
				$this->account['region'] = $timezone['region'];
				$this->account['dayStart'] = $sData['dayStart'];
				$this->account['dayEnd'] = $sData['dayEnd'];
				$this->account['weekStart'] = $sData['weekStart'];

				return true;
			}else{
				$err->setError('Error 14001: Could not update account settings');
			}
		}else{
			return false;
		}
	}/*Close updateAccountSettings*/

	/*
	 *public function: updateBrewery
	 *Function to update brewery information in the breweries table
	 *$brewery: id of the brewery
	 *$accountID: account id of the brewery account
	 *$success: Boolean - **Unsure of purpose**
	 *$data: Array - data to be updated in breweries table
	 *returns: Boolean - true on success, False on Failure
	 */
	public function updateBrewery($brewery, $accountID, $success, $data){

		return false;
	}/*Close updateBrewery*/

	/*
	 *public function: updateDistributor
	 *Function to update distributor information in distributors table
	 *$distributor: id of distributor
	 *$accountID: account id of the distributor account
	 *$success: Boolean - **Unsure of Purpose**
	 *$data:	Array - data to be updated in distributors table
	 *returns: Boolean: True on Success, False on Failure
	 */
	public function updateDistributor($distributor, $accountID, $success, $data){

		return false;
	}/*Close updateDistributor*/

	/*
	 *public function: updateUser
	 *Function to update user information in table
	 *$data: array of the data to be updated ->['firstName', 'lastName', 'email2', 'phone1', 'phone2']
	 *returns: Boolean: True on Successful update, False on Failure
	 *TODO: Verify Email2 with verification code and email address
	 *TODO: Verification Table in DB will need to be updated for the above
	 */
	public function updateUser($data){
		global $db;
		global $validate;
		global $err;
		global $gb;
		global $honeyandthebee;

		$rules = array(
			'firstName' => 'required|name',
			'lastName' => 'required|name',
			'email1' => 'required|email'
		);

		if(isset($data['email2'])) $rules['email2'] = 'email';
		if(isset($data['phone1'])) $rules['phone1'] = 'phone';
		if(isset($data['phone2'])) $rules['phone2'] = 'phone';

		if(isset($data)){
			$valid = $validate->validate($rules, $data);
			if($valid){

				$update = array(
					'firstName' => $gb->bake($honeyandthebee, $data['firstName']),
					'lastName' => $gb->bake($honeyandthebee, $data['lastName']),
					'email2' => $gb->bake($honeyandthebee, $data['email2']),
					'phone1' => $gb->bake($honeyandthebee, $data['phone1']),
					'phone2' => $gb->bake($honeyandthebee, $data['phone2'])
				);

				$where = array('id' => $this->user['id']);

				$updated = $db->update(USERS,$update,$where);
				$error = $db->error();
				$lastquery = $db->last_query();

				if($error[0] == '00000'){
					return true;
				}else{
					$err->setError('Could not update user. Please try again.');
					return false;
				}
			}else{
				$err->setError('Could not update user. Please try again.');
				return false;
			}

		}else{
			return false;
		}

	}/*Close updateUser*/


	/*
	 *public function: newVerification
	 *This function generates and stores verification codes; if one doesn't exist for the user
	 *function generates and inserts new verification; else updates existing verification
	 *returns: returns Verification Hash or False on Failure
	 */
	public function newVerification(){
		global $db;
		global  $err;

		$verifyInTable = $db->count(USER_VERIFICATION, array('id' => $this->user['id']));
		$newHash = md5(rand(1,1000));
		if($verifyInTable == 0){
			$data = array(
				'id' => $this->user['id'],
				'verificationHash' => $newHash,
				'verificationSent' => DATE('Y-m-d H-i:s')
			);

			$insert = $db->insert(USER_VERIFICATION, $data);
		}else{
			$update = array(
				'verificationHash' => $newHash,
				'verificationSent' => DATE("Y-m-d H:i:s")
			);
			$where = array('id' => $this->user['id']);

			$updated = $db->update(USER_VERIFICATION,$update,$where);
		}
		$error = $db->error();
		if($error[0] == '00000'){
			$this->user['verificationSent'] = $update['verificationSent'];
			return $newHash;
		}else{
			$err->setError('Error 5001: Could not create new verification.');
			return false;
		}
	}/*Close newVerification*/

}//close class user

?>