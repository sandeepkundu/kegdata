<?php
//Error Handling Class for graceful secure error reporting

class errors{
	//Class for storing and retrieving errors

	public $errors = array();

	/*Set an Error Message*/
	public function setError($msg){
		$this->errors[] = $msg;
	}

	/*Return errors if any exist*/
	public function getErrors(){
		return (count($this->errors) > 0 ? $this->errors : false);
	}

	/*Construct HTML error Box*/
	public function errorBox(){
		$errBox = <<<"EOD"
		<div class="alert alert-danger hide" id="error-messages"></div>
EOD;
	return $errBox;
	}

	public function resetErrors(){
		unset($this->errors);
		$this->errors = array();
	}

}