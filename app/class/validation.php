<?php
/*
 *KegData.com
 *Validation Class
 *Version 1 (2014-11-16)
*/
class validation{
	protected $invalidFields = array();

	//call this to validate multiple values
	public function validate($rules, $data){
		$valid = true;
		unset($invalidFields);
		$invalidFields = array();
		foreach ($rules as $fieldname => $rule){
			//Extract rules to callbacks
			$callbacks = explode('|', $rule);

			//Call the validation callbacks
			foreach($callbacks as $callback){
				$value = isset($data[$fieldname]) ? $data[$fieldname] : NULL;
				if($this->$callback($value, $fieldname) == FALSE) {
					array_push($invalidFields,$fieldname);
					$valid = FALSE;
				}
			} //foreach($callbacks)

		}//foreach($rules)

		return $valid;

	}//public function validate

	//public function to get invalid fields
	public function getInvalidFields(){
		return $this->invalidFields;
	}

	//validate account type()
	public function isAccountType($value,$fieldname){
		global $err;
		$typeArray = array('S','B','D','R','E');

		$valid = in_array($value, $typeArray);
		if(is_null($value)){
			$valid = TRUE;
		}
		if($valid == FALSE) $err->setError( "The ".$fieldname.' is not a valid account type');
		return $valid;
	}

	//validate value for email
	public function email($value, $fieldname){
		global $err;

		$valid = (!empty($value)) ? filter_var($value, FILTER_VALIDATE_EMAIL) : TRUE;
		if($valid == FALSE) $err->setError( "The ".$fieldname." needs to be a valid email." );
		return $valid;
	}

	//checks if value is AM/PM
	public function isAMPM($value, $fieldname){
		global $err;

		if($value == 'AM' || $value == 'PM' || $value == 'am' || $value == 'pm' || $value == ''){
			$valid = true;
		}else{
			$err->setError('The '.$fieldname.' is not valid');
			$valid = false;
		}

		return $valid;
	}
	//checks that value is a boolean value
	public function isBool($value, $fieldname){
		global $err;

		$valid = false;
		if($value == "1" || $value == "0" || $value == true || $value == false || $value == 0 || $value == 1){
			$valid = true;
		}

		if(!$valid){
			$err->setError($fieldname ." is not valid.");
		}
		return $valid;
	}

	//checks value for 3 characters
	public function isCurr($value, $fieldname){
		global $err;
		$valid = strlen($value);
		if($valid > 0){
			if($valid == 3){
				$pattern = '/[^[:alpha:]]/';
				$valid = preg_match($pattern,$value);

				if($valid != false){
					$err->setError($fieldname . ' is not valid. ');
					$valid = false;
				}else{
					$valid = true;
				}

			}else{
				$valid = false;
			}
		}else{
			$valid = true;
		}
		return $valid;
	}

	//checks value for Enterprise Code Characters
	public function isECode($value, $fieldname){
		global $err;
		$valid = strlen($value);
		if($valid > 0){
			if($valid >= 6 && $valid <= 8){
				$valid = $this->isExtText($value, $fieldname);
			}else{
				$err->setError($fieldname . ' is not valid');
				$valid = false;
			}
		}else{
			$valid = true;
		}
		return $valid;
	}

	//checks for extended text fields
	//only allows alpha, digits, -, :, #, @ signs
	public function isExtText($value, $fieldname){
		global $err;
		$pattern = "/[^[:alnum:][:blank:]-':#@\\/]/";
		$valid = preg_match($pattern, $value);

		if($valid != false){
			$err->setError( "The ".$fieldname." is not valid." );
			$valid = false;
		}else{
			$valid = true;
		}
		return $valid;
	}

	//verifies value is hash value
	public function isHash($value, $fieldname){
		global $err;
		
		$valid = preg_match('/^[a-f0-9]{32}$/', $value);

		if($valid == false) $err->setError( "The ".$fieldname." is not valid." );
		return $valid;
	}

	//verifies value is mac address
	public function isMac($value, $fieldname){
		global $err;

		$valid = preg_match('/[^A-F0-9:]/', $value);

		if($valid != false){
			$err->setError("The ".$fieldname." is not valid.");
			$valid = false;
		}else{
			$valid = true;
		}
		return $valid;
	}

	//verifies mailListKey
	//TO DO: Verification for Mail List
	public function isMailKey($value,$fieldname){
		$valid = true;
		return $valid;
	}

	//verifies value for Keg Notifications
	public function isNotification($value,$fieldname){
		global $err;
		$valid = true;
		$pattern = '/[LEUPT]/';
		$valid = preg_match($pattern,$value);
		if(!$valid){
			$err->setError('The '.$fieldname." is not valid.");
		}
		return $valid;
	}

	//verifies value is a number
	public function isNumber($value, $fieldname){
		global $err;
		$pattern = '/[^[:digit:]-]/';
		$valid = preg_match($pattern, $value);
		if($valid != false){
			 $err->setError( "The ".$fieldname." is not valid.");
			 $valid = false;
		}else{
			$valid = true;
		}
		return $valid;
	}

	//verifies value is a range character <>
	public function isRange($value, $fieldname){
		global $err;
		$pattern = '/[<>]/';
		$valid = preg_match($pattern,$value);
		if(!$valid){
			$err->setError('The '.$fieldname.' is not valid');
		}

		return $valid;
	}

	//verifies value is two characters text
	public function isState($value, $fieldname){
		global $err;
		$valid = strlen($value);
		if($valid > 0){
			if($valid == 2){
				$pattern = '/[^[:alpha:]]/';
				$valid = preg_match($pattern,$value);
				if($valid != false){
					$err->setError($fieldname. " is not valid.");
					$valid = false;
				}else{
					$valid = true;
				}
			}else{
				$err->setError($fieldname. " is not valid.");
				$valid = false;
			}
		}else{
			$valid = true;
		}
		return $valid;
	}

	//verifies for Human names - alpha characters, - and ',"
	public function name($value, $fieldname){
		global $err;

		$pattern = '/[^[:alpha:]-\'"]/';
		$valid = preg_match($pattern, $value);
		if($valid != false) {
			$err->setError('The '.$fieldname." is not valid.");
			$valid = false;
		}else{
			$valid = true;
		}

		return $valid;
	}

	//verifies that value is a phone number
	public function phone($value, $fieldname){
		global $err;

		$pattern = '/[^\d\(\)-]/';
		$valid = preg_match($pattern,$value);

		if($valid != false){
		 	$err->setError('The '.$fieldname."is not valid phone number");
		 	$valid = false;
		}else{
			$valid = true;
		}

		return $valid;
	}

	//if required, checks to make sure something is in the field
	public function required($value, $fieldname){
		global $err;

		$valid = !empty($value);
		if($valid == FALSE) $err->setError( "The ".$fieldname." is required." );
		return $valid;
	}

	//verifies field is valid temperature type
	public function temperature($value,$fieldname){
		global $err;

		if($value == 'F' || $value == 'C' || $value == ''){
			$valid = true;
		}else{
			$valid = false;
			$err->setError("The ".$fieldname." is not valid.");
		}

		return $valid;
	}

	//verified field is valid time
	public function time($value,$fieldname){
		global $err;

		$pattern = '/\d:[0-5]\d/';
		$valid = preg_match($pattern,$value);
		if($valid == false && $value != ''){
			$err->setError('The '.$fieldname.' is not a valid time');
		}else{
			$valid = true;
		}

		return $valid;


	}

	//verifies field is day of week
	public function weekday($value,$fieldname){
		global $err;
		$weekdays = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

		if(!in_array($value,$weekdays) && $value != ''){
			$err->setError('The '.$fieldname.' is not a valid weekday');
			$valid = false;
		}else{
			$valid = true;
		}

		return $valid;
	}

	//verifies that value is a user name, can be alpha characters or email
	public function username($value, $fieldname){
		global $err;

		$nonEmails = array("richandtim");

		$valid = filter_var($value, FILTER_VALIDATE_EMAIL);
		if($valid == FALSE) $valid = in_array($value, $nonEmails);
		if($valid == FALSE) $err->setError( "The ".$fieldname." is not valid." );

		return $valid;
	}

	//verifies that value is valid volume type
	public function volume($value, $fieldname){
		global $err;

		if($value == 'oz' || $value == 'g' || $value == 'l' || $value == 'ml' || $value == ''){
			$valid = true;
		}else{
			$valid = false;
			$err->setError("The ".$fieldname." is not valid.");
		}

		return $valid;
	}

	//verifies that value is a zipcode
	public function zipcode($value,$fieldname){
		global $err;
		$valid = strlen($value);
		if($valid > 0){
			if($valid == 5){
				$pattern = '/[^[:digit:]]/';
				$valid = preg_match($pattern,$value);
				if($valid != false){
					$err->setError($fieldname.' is not valid.');
					$valid = false;
				}else{
					$valid = true;
				}
			}elseif($valid == 10){
				$pattern = '/[^[:digit:]-]/';
				$valid = preg_match($pattern,$value);
				if($valid != false){
					$err->setError($fieldname. ' is not valid.');
					$valid = false;
				}else{
					$valid = true;
				}
			}else{
				$valid = false;
			}
		}else{
			$valid = true;
		}
		return $valid;
	}
} //close class