<?php
/*System Admin Class
--Dependent upon kegs and users class*/


class systemAdmin{

	//Public function to get user accounts for admin
	public function getAccounts($search=null){
		global $db;
		global $user;
		global $err;
		global $validate;
		global $gb;
		global $honeyandthebee;

		$data = array('id', 'accountType','accountName');

		if($search != null){
			$vData = array('search' => $search);
			$rules = array('search' => 'isExtText');

			$valid = $validate->$validate($rules,$vData);

			if($valid){
				$where = array('LIKE' =>
					array("OR" =>
						array('id' => $search, 'accountType' => $search, 'accountName' => $gb->bake($honeyandthebee, $search))
					)
				);

				$results = $db->select(ACCOUNTS,$data,$where);
			}else{
				$err->setError("Error 3002: Search is not valid");
			}
		}else{
			$results = $db->select(ACCOUNTS,$data);
		}
		$error = $db->error();
		$lastquery = $db->last_query();

		if($error[0] == '00000'){
			return $results;
		}else{
			$err->setError("Error 3001: Cannot retrieve accounts. Please try again.");
		}

		return false;
	}//Close get accounts

	public function getAccountData($id){
			global $db;
			global $user;
			global $err;
			global $validate;
			global $gb;
			global $honeyandthebee;

			$data = array(ACCOUNTS.'.id','accountName','accountType','accountEmail1','accountEmail2',
				'accountPhone1','accountPhone2','website',
				'address1','address2','city','stateCode','zipcode','county','countryCode','latitude','longitude',
			);

			$join = array('[>]'.ACCOUNT_ADDRESSES  => array('id' => 'accountID'));

			$where = array(ACCOUNTS.'.id' => $id, 'LIMIT' => 1);

			$results = $db->select(ACCOUNTS,$join,$data,$where);

			$account['account']['id'] = $results[0]['id'];
			$account['account']['accountName'] = $gb->eat($results[0]['accountName'],$honeyandthebee);
			$account['account']['accountType'] = $results[0]['accountType'];
			$account['account']['accountEmail1'] = (empty($results[0]['accountEmail1'])) ? '' : $gb->eat($results[0]['accountEmail1'],$honeyandthebee);
			$account['account']['accountEmail2'] = (empty($results[0]['accountEmail2'])) ? '' : $gb->eat($results[0]['accountEmail2'],$honeyandthebee);
			$account['account']['accountPhone1'] = (empty($results[0]['accountPhone1'])) ? '' : $gb->eat($results[0]['accountPhone1'],$honeyandthebee);
			$account['account']['accountPhone2'] = (empty($results[0]['accountPhone2'])) ? '' : $gb->eat($results[0]['accountPhone2'],$honeyandthebee);
			$account['account']['website'] = $results[0]['website'];

			$account['account']['address1'] = (empty($results[0]['address1'])) ? '' : $gb->eat($results[0]['address1'],$honeyandthebee);
			$account['account']['address2'] = (empty($results[0]['address2'])) ? '' : $gb->eat($results[0]['address2'], $honeyandthebee);
			$account['account']['city'] = $results[0]['city'];
			$account['account']['stateCode'] = $results[0]['stateCode'];
			$account['account']['zipcode'] = $results[0]['zipcode'];
			$account['account']['county'] = $results[0]['county'];
			$account['account']['countryCode'] = $results[0]['countryCode'];
			$account['account']['latitude'] = $results[0]['latitude'];
			$account['account']['longitude'] = $results[0]['longitude'];

			$data = array('id','login', 'firstName', 'lastName', 'email2', 'phone1', 'phone2', 'isActive', 'isAdmin');
			$where = array('account_id' => $id);

			$users = $db->select(USERS,$data,$where);

			foreach($users as $user){
				$account['users'][] = array(
					'id' => $user['id'],
					'login' => $gb->eat($user['login'],$honeyandthebee),
					'firstName' => (empty($user['firstName'])) ? '' : $gb->eat($user['firstName'],$honeyandthebee),
					'lastName' =>(empty($user['lastName'])) ? '' : $gb->eat($user['lastName'],$honeyandthebee),
					'email2' =>(empty($user['email2'])) ? '' : $gb->eat($user['email2'],$honeyandthebee),
					'phone1' =>(empty($user['phone1'])) ? '' : $gb->eat($user['phone1'],$honeyandthebee),
					'phone2' =>(empty($user['phone2'])) ? '' : $gb->eat($user['phone2'],$honeyandthebee),
					'isActive' => $user['isActive'],
					'isAdmin' => $user['isAdmin']
				);
			}

			return json_encode($account);
	}

	//function to Add Hub Dev
	public function addHub($data,$rules){
		global $db;
		global $err;
		global $validate;
		global $user;

		$valid = $validate->validate($rules,$data);

		if($valid){
			$uData = array('accountID' => $data['acctNum'],
				'hubMac' => str_replace(':', '', $data['mac']),
				'modified' => DATE('Y-m-d G:i:s'),
				'modifiedBy' => $user->getUserNum()
			);

			$success = $db->insert(HUBS,$uData);
			$error = $db->error();
			$lastquery = $db->last_query();

			if($error[0] == '00000'){
				return true;
			}
		}

		return false;
	}

	public function deleteAccount($id){
		global $db;
		$where = array('id' => $id);
		$where2 = array('accountID' => $id);

		$userWhere = array('account_id' => $id);

		$userID = $db->get(USERS, array('id'), $userWhere);
		$accountType = $db->get(ACCOUNTS,array('accountType'),$where);

		$data = array('accountID' => 0, 'primaryContactID' => '', 'primaryContactName' => '', 'email1' => '', 'email2' => '');
		if($accountType == 'D'){
			$db->update(DISTRIBUTORS,$data,$where2);
		}elseif($accountType == 'B'){
			$db->update(BREWERIES,$data,$where2);
		}

		$db->delete(USER_VERIFICATION,array('id' => $userID['id']));
		$db->delete(USER_LOGIN_HISTORY,array('id' => $userID['id']));
		$db->delete(USERS, $userWhere);

		$db->delete(ACCOUNT_SETTINGS,$where2);
		$db->delete(ACCOUNT_ADDRESSES,$where2);
		$db->delete(ACCOUNTS,$where);
		

		return true;

	}

	public function getHubs($acctNum){
		global $db;
		global $err;

		$uData = 'hubMac';
		$where = array('accountID' => $acctNum);

		$results = $db->select(HUBS,$uData,$where);

		return json_encode($results);
	}

}//close systemAdmin Class
?>