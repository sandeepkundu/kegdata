<?php
require_once VENDOR_PATH.'phpmailer'.DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'PHPMailerAutoload.php';
class kegDataMailer extends PHPMailer{
	
	
	function __construct(){

		$this->SMTPDebug = 0;                               // Enable verbose debug output
		$this->isSMTP();                                      // Set mailer to use SMTP
		$this->Host = 'vps14344.inmotionhosting.com';  // Specify main and backup SMTP servers
		$this->SMTPAuth = true;                               // Enable SMTP authentication
		$this->Username = 'accounts@kegdata.com';                 // SMTP username
		$this->Password = 'b33rma!L3R2424';                   // SMTP password
		$this->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$this->Port = 465;                                    // TCP port to connect to
	} 

 	public function sendMail($reciepient, $subject, $htmlTemplate, $textTemplate, $data){
 		global $err;
 		$this->From = 'accounts@kegdata.com';
		$this->FromName = 'KegData Account Administration';
		$this->addAddress($reciepient);     // Add a recipient
		$this->addReplyTo('accounts@kegdata.com', 'KegData Account Administration');
		$this->addCC('');
		$this->addBCC('accounts@kegdata.com');
		$this->addBCC('kevin@kegdata.com');
		$this->addBCC('lou@kegdata.com');
		$this->addBCC('latisha.mcneel@gmail.com');

		$this->WordWrap = 50;                                 // Set word wrap to 50 characters
		$this->isHTML(true);                                  // Set email format to HTML

		$htmlBody = file_get_contents(MAIL_PATH.$htmlTemplate);

		$textBody = file_get_contents(MAIL_PATH.$textTemplate);
		$styles = file_get_contents(MAIL_PATH.'email-styles.txt');
		$txtData = $data;

		$htmlBody = vsprintf($htmlBody, $data);
		$textBody = vsprintf($textBody, $txtData);

		$this->Subject = $subject;
		$this->Body    =  $htmlBody;
		$this->AltBody =  $textBody;


		if(!$this->send()) {
		  	return 'Mailer Error: ' . $this->ErrorInfo;
		} else {
		    return 'Message has been sent';
		}

 	}

}
?>