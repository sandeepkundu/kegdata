
<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="#">{{ Config::get('constants.COMPANY_NAME') }} Store Admin</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<?php if($request== "product"){?>
				<li{{ $request == "store" ? ' class=active' : '' }}><a href="/employee/store">Overview<span class="sr-only">(current)</span></a></li>
				<li{{ $request== "tax" ? ' class=active' : '' }}><a href="/employee/store/tax">Tax Rates</a></li>
				<li{{ $request =="category" ? ' class=active' : '' }}><a href="/employee/store/category">Categories</a></li>
				<li{{ $request == "plan" ? ' class=active' : '' }}><a href="/employee/store/plan">Plans</a></li>
				<li{{ $request == "product" ? ' class=active' : '' }}><a href="/employee/store/product">Products</a></li>
				
				<?php }else{?>
				
				<li{{ $request->is('*/store') ? ' class=active' : '' }}><a href="/employee/store">Overview<span class="sr-only">(current)</span></a></li>
				<li{{ $request->is('*/tax*') ? ' class=active' : '' }}><a href="/employee/store/tax">Tax Rates</a></li>
				<li{{ $request->is('*/category*') ? ' class=active' : '' }}><a href="/employee/store/category">Categories</a></li>
				<li{{ $request->is('*/plan*') ? ' class=active' : '' }}><a href="/employee/store/plan">Plans</a></li>
				<li{{ $request->is('*/product*') ? ' class=active' : '' }}><a href="/employee/store/product">Products</a></li>
				
			<?php	}?>
				
			</ul>
			
		</div><!-- /.navbar-collapse -->
  	</div><!-- /.container-fluid -->
</nav>