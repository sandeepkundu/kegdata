@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Management</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li class="active"><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
  	</ul>
  @stop

@section('tabpane')
	@include('employees/_storenav', array('request' => $request))
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Plans</h3></div>
				<table class="table">
					<tr>
						<th>Delete</th>
						<th>Edit</th>
						<th>Name</th>
						<th>Short Description</th>
						<th>Price</th>
						<th>Is Quantity</th>
						<th>Quantity Price</th>
						<th>Interval</th>
						<th>Subscribers</th>
					</tr>
					@foreach($plans as $p)
						<tr data-id="{{ $p->id }}">
							<td style="width:20px;">{!! Form::open(['action' => array('Employees\StoreController@deletePlan', $p->id), 'method'=>'delete']) !!}
								<input type="hidden" name="id" value="{{ $p->id }}" />
								<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete {{ $p->name }}?')"><i class="fa fa-trash"><span class="sr-only">Delete {{ $p->name}}</span></i></button>
							 	{!! Form::close() !!}

							</td>
							<td  style="width:20px;">
							<a href="/employee/store/plan/{{ $p->id }}" class="btn btn-info">
								<i class="fa fa-pencil-square-o"><span class="sr-only">Edit {{ $p->name }}</span></i>
							</a>
							</td>
							<td>{{ $p->name }}</td>
							<td>{{ $p->short_description }}</td>
							<td>{{ $p->price }}</td>
							<td>{{ ($p->is_quantity == 1) ? 'Yes' : 'No' }}</td>
							<td>{{ $p->quantity_price }}</td>
							<td>{{ $p->interval_count }} {{ $p->interval }}</td>
							<td></td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		@if(is_null($plan))
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Add Plan</h3></div>
				<div class="panel-body">
					{!! Form::open(['action' => 'Employees\StoreController@storePlan', 'class' => 'form-horizontal', 'method' => 'put', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Plan Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					@if(!empty($categories))
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('category') ? '' : ' has-error has-feedback' }}">
							<p class="lead">Choose Plan Categories</p>
							<div class="row">
								@foreach($categories as $cat)
								<div class="col-md-3">
									<div class="checkbox">
										<label>{!! Form::checkbox('category[]', $cat->id) !!} {{ $cat->name }}</label>
									</div>
								</div>
								@endforeach
							</div>
							@if ($errors->first('category'))
								<span class="help-block">{{ $errors->first('category') }}</span>
							@endif
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Price <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								<span class="input-group-addon">$</span>
								{!! Form::text('price', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('price'))
								<span class="help-block">{{ $errors->first('price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('is_quantity') ? '' : ' has-error has-feedback' }}">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-beer"></i>
										</span>
										<span class="input-group-addon">Is Quantity Plan</span>

										<div class="checkbox" style="padding-left:8px;">
											<label> {!! Form::checkbox('is_quantity', 1) !!} Subscribe Price * Quantity of Kegs + Base Plan Price</label>
										</div>
									</div>
								</div>
							</div>
							@if ($errors->first('is_quantity'))
								<span class="help-block">{{ $errors->first('is_quantity') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('quantity_price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Quantity Price</label>
								<span class="input-group-addon">$</span>
								{!! Form::text('quantity_price', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('quantity_price'))
								<span class="help-block">{{ $errors->first('quantity_price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('min') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="min">Minumum Number of Kegs <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('min', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('min'))
								<span class="help-block">{{ $errors->first('min') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('max') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="max">Maximum Number of Kegs <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('max', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('max'))
								<span class="help-block">{{ $errors->first('max') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('interval') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="interval">Interval<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::select('interval', ['day' => 'daily', 'month' => 'monthly', 'year' => 'yearly', 'week' => 'weekly', '3-month' => 'every 3 months', '6-months' => 'every 6 months'], null, array('class' => 'form-control','required')) !!}
							</div>
							@if ($errors->first('interval'))
								<span class="help-block">{{ $errors->first('interval') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('term_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="term_description">Term Description<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('term_description', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('term_description'))
								<span class="help-block">{{ $errors->first('term_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('trial_days') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="trial_days">Trial Period<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('trial_days', null, array('class' => 'form-control', 'required')) !!}
								<span class="input-group-addon">Days</span>
							</div>
							@if ($errors->first('trial_days'))
								<span class="help-block">{{ $errors->first('trial_days') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Plan Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Add Plan', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@else
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Edit Plan {{ $plan->name }}</h3></div>
				<div class="panel-body">
					{!! Form::model($plan, ['action' => array('Employees\StoreController@updatePlan', $plan->id), 'class' => 'form-horizontal', 'method' => 'patch', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Plan Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('category') ? '' : ' has-error has-feedback' }}">
							<p class="lead">Choose Product Categories</p>
							<div class="row">
								@foreach($categories as $cat)
								<div class="col-md-3">
									<div class="checkbox">
										<label>{!! Form::checkbox('category[]', $cat->id) !!} {{ $cat->name }}</label>
									</div>
								</div>
								@endforeach
							</div>
							@if ($errors->first('category'))
								<span class="help-block">{{ $errors->first('category') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Price<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								<span class="input-group-addon">$</span>
								{!! Form::text('price', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('price'))
								<span class="help-block">{{ $errors->first('price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('is_quantity') ? '' : ' has-error has-feedback' }}">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-beer"></i>
										</span>
										<span class="input-group-addon">Is Quantity Plan</span>

										<div class="checkbox" style="padding-left:8px;">
											<label> {!! Form::checkbox('is_quantity', 1, null, array( 'disabled')) !!} Subscribe Price * Quantity of Kegs + Base Plan Price</label>
										</div>
									</div>
								</div>
							</div>
							@if ($errors->first('is_quantity'))
								<span class="help-block">{{ $errors->first('is_quantity') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('quantity_price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Quantity Price</label>
								<span class="input-group-addon">$</span>
								{!! Form::text('quantity_price', null, array('class' => 'form-control', 'readonly')) !!}
							</div>
							@if ($errors->first('quantity_price'))
								<span class="help-block">{{ $errors->first('quantity_price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('min') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="min">Minumum Number of Kegs <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('min', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('min'))
								<span class="help-block">{{ $errors->first('min') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('max') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="max">Maximum Number of Kegs <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('max', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('max'))
								<span class="help-block">{{ $errors->first('max') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('interval') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="interval">Interval<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								
								{!! Form::text('interval', "month", array('class' => 'form-control', 'required', 'readonly')) !!}
							</div>
							@if ($errors->first('interval'))
								<span class="help-block">{{ $errors->first('interval') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('term_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="term_description">Term Description<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('term_description', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('term_description'))
								<span class="help-block">{{ $errors->first('term_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('trial_days') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="trial_days">Trial Period<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('trial_days', null, array('class' => 'form-control', 'required', 'readonly')) !!}
							</div>
							@if ($errors->first('trial_days'))
								<span class="help-block">{{ $errors->first('trial_days') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required', 'readonly')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Plan Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
						<div class="col-md-6">
							@if(!is_null($plan->photos->first()))
								<img src="{{ $plan->photos->first()->path }}" class="img-responsive" />
								<caption>{{ $plan->photos->first()->path }}</caption>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Edit Plan', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endif
	</div>
@stop

@section('jsScript')
@stop
