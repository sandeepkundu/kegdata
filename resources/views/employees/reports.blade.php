@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li><a href="/employee/store">Store</a></li>
		<li ><a href="/employee/orders">Orders</a></li>
		<li class="active"><a href="/employee/reports">Reports</a></li>
		<li ><a href="/employee/misc">Misc</a></li>
  	</ul>
  @stop

@section('tabpane')

    <div class="panel panel-primary">
        <div class="panel-heading">
					<h3 class="panel-title">Reports
						<div class="pull-right">
							
						</div>
					</h3>
		</div>
		 
     
    </div>
@stop

@section('jsScript')

@stop
