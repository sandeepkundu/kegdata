@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Management</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li><a href="/employee/store">Store</a></li>
		<li class="active"><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
  	</ul>
  @stop

@section('tabpane')
	@include('employees/_storenav', array('request' => $request))
    <div class="row">
    	<div class="col-md-6">
    		<div class="panel panel-primary">
    			<div class="panel-heading"><h3 class="panel-title">Order Details: {{ $order->id }} - Status: {{ $order->status }}</h3></div>
    			<div class="panel-body">
                <h4>Current Status: {{ $order->status }}</h4>
                {!! Form::open(['action' => 'Employees\OrdersController@updateOrder', 'class' => 'form-horizontal', 'method' => 'patch']) !!}
                <input type="hidden" name="id" value="{{ $order->id }}" />
                <div class="form-group">
                    <div class="col-md-12{{ !$errors->first('status') ? '' : ' has-error has-feedback' }}">
                        <label for="status">Status</label>
                        <select name="status">
                            <option value="new"{{($order->status == 'new') ? ' selected' : ''}}>New</option>
                            <option value="charged"{{($order->status == 'charged') ? ' selected' : ''}}>Charged</option>
                            <option value="processing"{{($order->status == 'processing') ? ' selected' : ''}}>Processing</option>
                            <option value="fulfilled"{{($order->status == 'fulfilled') ? ' selected' : ''}}>Fulfilled</option>
                            <option value="returned"{{($order->status == 'returned') ? ' selected' : ''}}>Return</option>
                            <option value="canceled"{{($order->status == 'canceled') ? ' selected' : ''}}>Canceled</option>
                        </select>

                        @if ($errors->first('status'))
                            <span class="help-block">{{ $errors->first('status') }}</span>
                        @endif
                    </div>
                </div>
                <button type='submit' class="btn btn-primary">Update Status</button>
                {!! Form::close() !!}

    			@if(!is_null($order->orderItem))
    				<h4>{{ ($order->status == 'new') ? 'Billed' : 'Charged' }} <span class="right">${{ number_format($order->total,2) }}</span></h4>
    				<table class="table">
    					<thead>
    						<tr>
    							<th>Product</th>
    							<th>Item Price</th>
    							<th>Quantity</th>
    							<th class="text-right">Subtotal</th>
    						</tr>
    					</thead>
    					<tbody>
    					@foreach($order->orderItem as $item)
    						<tr>
    							<td>{{ $item->product->name }}</td>
    							<td>${{ $item->price }}</td>
    							<td>{{ $item->quantity }}</td>
    							<td class="text-right">${{ number_format($item->price*$item->quantity, 2) }}</td>
    						</tr>
    					@endforeach
    					<tr>
    						<th colspan="3">Tax</th>
    						<td class="text-right">${{ number_format($order->tax, 2) }}</td>
    					</tr>
    					<tr>
    						<th colspan="3">Shipping</th>
    						<td  class="text-right">${{ number_format($order->shipping,2) }}</td>
    					</tr>
    					</tbody>
    					<tfoot>
    						<tr>
    							<th colspan="3">Total</th>
    							<td class="text-right">${{ number_format($order->total, 2) }}</td>
    						</tr>
    					</tfoot>
    				</table>
    			@endif

    			</div>
    		</div>
    	</div>
    	<div class="col-md-6">
    		<div class="panel price">
    			<div class="panel-heading text-center">
    				<h4>Billing/Shipping Details</h4>
    			</div>
    			<div class="panel-body">
    				@foreach($order->orderAddress as $address)
    				<div class="col-md-6">
    				@if($address->type == 'B')
    					<h5><strong>Billing Address</strong></h5>
    				@else
    					<h5><strong>Shipping Address</strong></h5>
    				@endif
    				<address>
    					{{ $address->name }}<br />
    					{{ $address->address1 }}<br />
    					@if(!empty($address->address2))
    						{{ $address->address2 }}<br />
    					@endif
    					{{ $address->city }}, {{ $address->state }} {{ $address->zipcode }}
    				</address>
    				</div>
    				@endforeach
    			</div>
    		</div>
    		@if(!is_null($order->subscription))
    			<div class="panel price">
    				<div class="panel-heading text-center">
    					<h4>Subscription Details</h4>
    				</div>
    				<div class="panel-body text-center">
    					<h4>{{ $order->subscription->plan->name }}</h4>
    					<p class="text-center"> <strong>${{ $subPrice }} </strong>
    					<br> <span class="pricing-date">per {{ $order->subscription->plan->interval }}</span>
    					</p>
    				</div>
    				<p class="lead text-center">
    					{!! $order->subscription->plan->short_description !!}
    				</p>
    				{!! $order->subscription->plan->description !!}

    				<div class="panel-footer">
    					<div class="row">
    						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    							<p class="lead" style="color:#333;">The above plan will be charged to the card provided starting in  {{ $order->subscription->plan->trial_days }} days and then continuing every {{ $order->subscription->plan->interval }}.</p>
    						</div>
    					</div>
    				</div>
    			</div>
    		@endif
    	</div>
    </div>

@stop

@section('jsScript')

@stop
