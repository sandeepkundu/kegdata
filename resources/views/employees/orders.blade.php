@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li><a href="/employee/store">Store</a></li>
		<li class="active"><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
        <li ><a href="/employee/misc">Misc</a></li>
  	</ul>
  @stop

@section('tabpane')

    <div class="panel panel-primary">
        <div class="panel-heading">
					<h3 class="panel-title">Orders
						<div class="pull-right">
							<a href="/employee/orders/excel" style="color:#fcfcfc;">
								<i class="fa fa-file-excel-o" aria-hidden="true"></i>
							</a>
						</div>
					</h3>
		</div>
		  <table class="table table-striped">
            <tr>
                <th>View</th>
                <th>Delete</th>
				<th>Edit</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Account Name</th>
                <th>User Email</th>
                <th>Subscription</th>
                <th>Items</th>
                <th>Subtotal</th>
                <th>Tax</th>
                <th>Shipping</th>
                <th>Total</th>
            </tr>
       
            @foreach($orders as $order)
            <tr>
                <td><a href="/employee/order/{{ $order->id }}"><i class="fa fa-hand-o-up"></i></a></td>
				
                   <td style="width:20px;">{!! Form::open(['action' => array('Employees\OrdersController@deleteOrder', $order->id), 'method'=>'delete']) !!}
                                <input type="hidden" name="id" value="{{ $order->id }}" />
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete ?')"><i class="fa fa-trash"><span class="sr-only">Delete {{ $order->id }}</span></i></button>
                                {!! Form::close() !!}

                            </td> 


                
                <td><a href="/employee/order/edit/{{ $order->id }}"><span class="fa fa-pencil-square-o"></span></a></td>
				<td>{{ $order->status }}</td>
                <td>{{ $order->created_at->toDayDateTimeString()   }}</td>
                <td>{{ $order->account->accountName }}</td>
                <td>{{ $order->user->email }}</td>
                <td>{{ (is_null($order->subscription)) ? 'No' : 'Yes'}}</td>
                <td>{{ (is_null($order->products)) ? 'No' : 'Yes' }}</td>
                <td>{{ $order->subtotal }}</td>
                <td>{{ $order->tax }}</td>
                <td>{{ $order->shipping }}</td>
                <td>{{ $order->total }}</td>
            </tr>
            @endforeach
        </table>
        <div class="panel-footer">{{-- $orders->links() --}}</div>
    </div>
@stop

@section('jsScript')

@stop
