@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li class="active"><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
		<li ><a href="/employee/misc">Misc</a></li>
  	</ul>
  @stop

@section('tabpane')
	@include('employees/_storenav', array('request' => $request))
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Categories</h3></div>
				<table class="table">
					<tr>
						<th>Delete</th>
						<th>Edit</th>
						<th>Name</th>
						<th>Short Description</th>
					</tr>
					@foreach($categories as $cat)
						<tr data-id="{{ $cat->id }}">
							<td style="width:20px;">{!! Form::open(['action' => array('Employees\StoreController@deleteCategory', $cat->id), 'method'=>'delete']) !!}
								<input type="hidden" name="id" value="{{ $cat->id }}" />
								<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete {{ $cat->name }}?')"><i class="fa fa-trash"><span class="sr-only">Delete {{ $cat->name}}</span></i></button>
							 	{!! Form::close() !!}

							</td>
							<td  style="width:20px;">
							<a href="/employee/store/category/{{ $cat->id }}" class="btn btn-info">
								<i class="fa fa-pencil-square-o"><span class="sr-only">Edit {{ $cat->name }}</span></i>
							</a>
							</td>
							<td>{{ $cat->name }}</td>
							<td>{{ $cat->short_description }}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		@if(is_null($category))
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Add Category</h3></div>
				<div class="panel-body">
					{!! Form::open(['action' => 'Employees\StoreController@storeCategory', 'class' => 'form-horizontal', 'method' => 'put', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Category Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('is_quantity') ? '' : ' has-error has-feedback' }}">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-bars" aria-hidden="true"></i>
										</span>
										<span class="input-group-addon">Include in Navigation</span>


										<div class="checkbox" style="padding-left:8px;">
											<label> {!! Form::checkbox('is_navigation',  1, true) !!} Front-end Store Navigation</label>
										</div>
									</div>
								</div>
							</div>
							@if ($errors->first('is_quantity'))
								<span class="help-block">{{ $errors->first('is_quantity') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Category Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Add Category', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@else
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Edit Category {{ $category->name }}</h3></div>
				<div class="panel-body">
					{!! Form::model($category, ['action' => array('Employees\StoreController@updateCategory', $category->id), 'class' => 'form-horizontal', 'method' => 'patch', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Category Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('is_quantity') ? '' : ' has-error has-feedback' }}">
							<div class="row">
								<div class="col-md-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-bars" aria-hidden="true"></i>
										</span>
										<span class="input-group-addon">Include in Navigation</span>

										<div class="checkbox" style="padding-left:8px;">
											<label> {!! Form::checkbox('is_navigation',  1, null) !!} Front-end Store navigation</label>
										</div>
									</div>
								</div>
							</div>
							@if ($errors->first('is_quantity'))
								<span class="help-block">{{ $errors->first('is_quantity') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Category Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
						<div class="col-md-6">
							@if(!is_null($category->photos) && count($category->photos))
								<img src="{{ $category->photos->first()->path }}" class="img-responsive" />
								<caption>{{ $category->photos->first()->path }}</caption>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Edit Category', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endif
	</div>
@stop

@section('jsScript')

@stop
