<script>
//On click and modal for editUser
$('.editButton').on('click keypress', function(){
    var userID = $(this).parents('tr').attr("data-userid");
    $("#user_id_edit").val(userID);
    $("#firstNameEdit").val($('tr[data-userid="' + userID + '"] .firstName').text());
    $("#lastNameEdit").val($('tr[data-userid="' + userID + '"] .lastName').text());
    $("#emailEdit").val($('tr[data-userid="' + userID + '"] .email').text());
    $("#editUserModal").modal();
});
</script>
