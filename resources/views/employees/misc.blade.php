@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li ><a href="/employee/user">User Management</a></li>
		<li><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
		<li class="active"><a href="/employee/misc">Misc</a></li>
  	</ul>
@stop



@section('tabpane')
<nav class="navbar navbar-gray">
	<div class="container-fluid">
		<div class="collapse navbar-collapse">
			<div class="navbar-brand">
				Update Keg ADC Setting 
				
			</div>
			
		</div>
	</div>
</nav>

@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif

<div class="panel panel-primary">
	{!! Form::open(['url' => 'employee/updatekegtype', 'method' => 'patch', 'class' => 'form-horizontal', 'id' => 'editKegForm']) !!}
	<div class="row form-group ">
		<div class="col-sm-6"  style="margin-left:25%;margin-top:10px">
			<div class="input-group">
				<span class="input-group-addon">Select Keg</span>
				<!-- {!! Form::select('id',$keg, old('id'), ['class' => 'form-control', 'id' => 'chooseKeg','required'=>true]) !!} -->
				<select class="form-control" id="chooseKegmisc" required="1" name="id"><option value="" selected="selected">Choose Type of Keg</option>
				<?php 
				foreach($kegType as $k) {
				?>
				<option value="{{$k->id}}" data-adcfull="{{$k->adc_full}}" data-emptyValue="{{$k->emptyValue}}">{{$k->kegTypeDesc}}</option>
				<?php } ?>
				</select>
			</div>		
		</div>
	</div>
	<div class="row form-group text-center">
		<div class="col-sm-6"  style="margin-left:25%;margin-top:10px">
			
			<div class="input-group">
				<span class="input-group-addon">ADC Full</span>
				{!! Form::input('number', 'adc_full', old('adc_full'), ['class' => 'form-control','required'=>true,'id'=>'adc_full']) !!}
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-6"  style="margin-left:25%;margin-top:10px">
			
			<div class="input-group">
				<span class="input-group-addon">ADC Empty</span>
				{!! Form::input('number', 'emptyValue', old('emptyValue'), ['class' => 'form-control','required'=>true,'id'=>'emptyValue']) !!}
			</div>
		</div>
	</div>
	<div class="row form-group text-center">
		<input name="submitDevice" value="Save" class="btn btn-primary" type="submit">
	</div>
	@stop
</div>

