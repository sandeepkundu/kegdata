@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li class="active"><a href="/employee/user">User Management</a></li>
		<li><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
		<li ><a href="/employee/misc">Misc</a></li>
  	</ul>
@stop

@section('tabpane')
<nav class="navbar navbar-gray">
	<div class="container-fluid">
		<div class="collapse navbar-collapse">
			<div class="navbar-brand">
				Search User
				
			</div>
			{!!  Form::open(['url'=>'employee/user/search','id' => 'findUser', 'class' => 'navbar-form navbar-right']) !!}
			<div class="form-group">
				{!! Form::label('selectAccount', 'Select Account', array('class' => 'sr-only')) !!}
				<div class="input-group">
					<div class="input-group-addon">
						Index
					</div>
					{!! Form::text('keyword',"", ['class'=>'form-control', 'id'=>'selectAccount']) !!}
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Go" />
			{!! Form::close() !!}
		</div>
	</div>
</nav>



<div class="panel panel-primary">
	<div class="panel-heading">
		<h2 class="panel-title">Users List
			<div class="pull-right">
				<!-- <a href="/employee/user/test/excel" style="color:#fcfcfc;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a> -->
				<a href="/employee/user/excel" style="color:#fcfcfc;"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
			</div>
		</h2>
</div>
<table class="table">
	<tr><th>Edit</th><th>First Name</th><th>Last Name</th><th>Email</th></tr>
	@foreach($users as $user)
		<tr data-userid="{{ $user->id }}">
      <td><span class="fa fa-pencil-square-o editButton" data-toggle="modal" data-target="editUserModal"></span></td>
			<td class="firstName">{{ ucwords ($user->firstName) }}</td>
			<td class="lastName">{{ ucwords ($user->lastName) }}</td>
			<td class="email">{{ $user->email }}</td>
		</tr>
	@endforeach
</table>

<!-- Edit User Modal -->
<div class="modal fade" id="editUserModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit User</h4>
      </div>
      <div class="modal-body">
          {!! Form::open(array('url' => '/employee/user/', 'method' => 'patch', 'class' => 'form-horizontal')) !!}
	      {!! Form::hidden("user_id", " ", array("class" => "form-control", "id" => "user_id_edit")) !!}
          {!! Form::text("firstName", " ", array("class" => "form-control", "id" => "firstNameEdit")) !!}
          <br>
          {!! Form::text("lastName", " ", array("class" => "form-control", "id" => "lastNameEdit")) !!}
          <br>
          {!! Form::text("email", " ", array("class" => "form-control", "id" => "emailEdit")) !!}
          <br>
          {!! Form::submit('Save Changes', array("class" => "btn btn-primary")) !!}
	      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop

@section('jsScript')
	@include('employees.userjs')
@stop
