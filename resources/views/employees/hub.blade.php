@section('tabs')
<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	<li class="active"><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	<li><a href="/employee/account">Account Management</a></li>
	<li><a href="/employee/user">User Management</a></li>
	<li><a href="/employee/products">Products</a></li>
	<li><a href="/employee/orders">Orders</a></li>
	<li><a href="/employee/reports">Reports</a></li>
	<li ><a href="/employee/misc">Misc</a></li>
</ul>
@stop

@section('tabpane')

<nav class="navbar navbar-gray">
	<div class="container-fluid">
		<div class="collapse navbar-collapse">
			<div class="navbar-brand">
				Managing Account: 
				@if(is_null($accountID))
				<span class="text-danger">Select Account</span>
				@else
				{{ $accounts->accountName }}
				@endif
			</div>
			{!!  Form::open(['url'=>'employee/hub/account','id' => 'findAccount', 'class' => 'navbar-form navbar-right']) !!}
			<div class="form-group">
				{!! Form::label('selectAccount', 'Select Account', array('class' => 'sr-only')) !!}
				<div class="input-group">
					<div class="input-group-addon">
						Select Account
					</div>
					{!! Form::select('selectAccount', $selectAccts, $accountID, ['class'=>'form-control', 'id'=>'selectAccount']) !!}
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Select Account" />
			{!! Form::close() !!}

			{!!  Form::open(['url'=>'employee','id' => 'findAccount', 'class' => 'navbar-form navbar-right','method'=>'GET']) !!}
			<div class="form-group">
				{!! Form::label('selectAccount', 'Select Account', array('class' => 'sr-only')) !!}
				<div class="input-group">
					<div class="input-group-addon">
						Search
					</div>
					{!! Form::text('keyword',$keyword, ['class'=>'form-control', 'id'=>'selectAccount']) !!}
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Go" />
			{!! Form::close() !!}
		</div>
	</div>
</nav>
@if(Session::has('msg'))
<div class="alert alert-success">
	
	<p>{{Session::get('msg')}}</p>
	
</div>
@endif
<div class="clearfix">
	@if(is_null($accountID))
	<h1 class="text-danger">Select account to manage hubs</h1>
	@else
	<div class="col-md-6">
		<div class="panel panel-danger">
			<div class="panel-heading"><h2 class="panel-title">Add Hub Controller</h2></div>
			<div class="panel-body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				{!! Form::open(['url' => 'employee/hub/account/'.$accountID, 'class' => 'form-horizontal']) !!}
				<h4 id="accountName" class="text-center">Choose Account to Add Hub</h4>
				{!! Form::hidden('account_id', $accountID) !!}
				<div class="row form-group">
					<div class="col-sm-12">
						{!! Form::label('hubMac', 'Mac Address of Hub', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<span class="input-group-addon">Hub Mac</span>
							{!! Form::text('hubMac', old('hubMac'), ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="row form-group text-center">
					<input type="submit" name="submitHub" value="Submit Hub Device" class="btn btn-primary" />
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-danger">
			<div class="panel-heading"><h2 class="panel-title">Account Hubs</h2></div>
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Delete</th>
							<th>Edit</th>
							<th>Hub Mac</th>
							<th>Keg's Name</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$device_name='';
						foreach($devices as $device) {

							$device_name .=$device->deviceName.", ";
						}
						$device_name=!empty($device_name) ?substr($device_name, 0,-2) : 'No keg added';
						?>

						@foreach($hubs as $hub)
						<tr data-id="{{ $hub->id }}" data-account-id="{{ $hub->account_id }}">
							<td style="width:20px"><span class="fa fa-trash deleteButton"></span><span class="sr-only">Delete Hub</span></td>
							<td style="width:20px"><span class="fa fa-pencil-square-o editButton"></span><span class="sr-only">Edit Hub</span></td>
							<td class="hub_mac">{{ $hub->hubMac }}</td>
							<td>{{ $device_name }}</td>
							

						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
</div>
<div class="panel panel-primary">
	<div class="panel-heading"><h2 class="panel-title">Users List</h2>
		<div class="pull-right" style=" margin-top: -18px;">
			<!-- <a href="/employee/user/test/excel" style="color:#fcfcfc;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a> -->
			<a href="/employee/user/hub/excel" style="color:#fcfcfc;"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
		</div>
	</div>
	<table class="table">

		<?php
		if($order_by == 'dsc') {
			$order_by='asc';
		}  else {
			if($order_by=='asc') {
				$order_by='dsc';
			}
		}

		?>

		<tr><th><a href="/{{Request::path()}}?order=firstName&terms={{$order_by}}">First Name</a></th><th><a href="/{{Request::path()}}?order=lastName&terms={{$order_by}}">Last Name</a></th><th><a href="/{{Request::path()}}?order=email&terms={{$order_by}}">Email</a></th><th><a href="/{{Request::path()}}?order=city&terms={{$order_by}}">City</a></th><th><a href="/{{Request::path()}}?order=stateCode&terms={{$order_by}}">State</a></th><th><a href="/{{Request::path()}}?order=zipcode&terms={{$order_by}}">Zip</a></th><th><a href="/{{Request::path()}}?order=countryCode&terms={{$order_by}}">Country</a></th><th colspan="2">Actions</th></tr>
		
		<?php if($users->count()==0) {?>
			<tr data-userid="{{ $user->id }}" data-accountid="{{ $user->account_id }}">
				<td colspan="8">
					<p style="color:red;">No Search Result Founds!</p>
				</td>
			</tr>
		<?php } ?>
		@foreach($users as $user)
		<tr data-userid="{{ $user->id }}" data-accountid="{{ $user->account_id }}"  ><td class="firstName">{{ $user->firstName }}</td><td class="lastName">{{ $user->lastName }}</td><td class="email">{{ $user->email }}</td><td>
			<?php $ad = $address->where('account_id',$user->account_id)->first()?>

			{{ $ad['city']}}</td><td>{{ $ad['stateCode']}}</td><td>{{ $ad['zipcode']}}</td><td>{{ $ad['countryCode']}}</td><td data-target="editUserModall" >
			<a href="#" class="btn btn-info" >
				<i class="fa fa-pencil-square-o editButtonn"><span class="sr-only">Edit {{ $user->name }}</span></i>
			</a>

		</td>

		<td style="width:20px;">{!! Form::open(['action' => array('Employees\UsersController@destroyUserHub', $user->id), 'method'=>'delete']) !!}
			<input type="hidden" name="id" value="{{ $user->id }}" />
			<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete ?')"><i class="fa fa-trash"><span class="sr-only">Delete </span></i></button>
			{!! Form::close() !!}

		</td>


	</td></tr>
	@endforeach
</table>


<!-- edit Hub Modal -->
<div class="modal fade" id="editUserModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Hub</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => 'employee/hub/account/'.$accountID, 'method' => 'patch', 'class' => 'form-horizontal')) !!}
				{!! Form::hidden("account_id", " ", array("class" => "form-control", "id" => "account_id_edit")) !!}
				{!! Form::hidden("id", " ", array("class" => "form-control", "id" => "hub_id")) !!}
				{!! Form::text("hubMac", " ", array("class" => "form-control", "id" => "hub_mac_id")) !!}
				<br>
				{!! Form::submit('Save Changes', array("class" => "btn btn-primary")) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<!-- Edit user modal from second call -->
<div class="modal fade" id="editUserModalAnother" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Hub</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => 'employee/hub/account/', 'method' => 'patch', 'class' => 'form-horizontal modal_update_form')) !!}
				{!! Form::hidden("account_id", " ", array("class" => "form-control", "id" => "account_id_edit_second")) !!}
				{!! Form::hidden("id", " ", array("class" => "form-control", "id" => "hub_id_second")) !!}
				{!! Form::text("hubMac", " ", array("class" => "form-control", "id" => "hub_mac_id_second")) !!}
				<br>
				{!! Form::submit('Save Changes', array("class" => "btn btn-primary")) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>


<!-- Delete Hub Modal -->
<div class="modal fade" id="deleteUserModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Are you sure!</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => 'employee/hub/account/'.$accountID, 'method' => 'delete', 'class' => 'form-horizontal')) !!}
				{!! Form::hidden("account_id", " ", array("class" => "form-control", "id" => "del_account_id_edit")) !!}
				{!! Form::hidden("id", " ", array("class" => "form-control", "id" => "del_hub_id")) !!}
				{!! Form::submit('Confirm', array("class" => "btn btn-primary")) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<!-- Delete Hub Modal on second call -->
<div class="modal fade" id="deleteUserModalSecond" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Are you sure!</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => 'employee/hub/account/', 'method' => 'delete', 'class' => 'form-horizontal modal_delete_form_second')) !!}
				{!! Form::hidden("account_id", " ", array("class" => "form-control", "id" => "del_account_id_edit_second")) !!}
				{!! Form::hidden("id", " ", array("class" => "form-control", "id" => "del_hub_id_second")) !!}
				{!! Form::submit('Confirm', array("class" => "btn btn-primary")) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<!-- Edit User Modal -->
<div class="modal fade" id="editUserModall" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit User</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(array('url' => '/employee/users/', 'method' => 'patch', 'class' => 'form-horizontal')) !!}
				{!! Form::hidden("user_id", " ", array("class" => "form-control", "id" => "user_id_edit")) !!}
				{!! Form::text("firstName", " ", array("class" => "form-control", "id" => "firstNameEdit")) !!}
				<br>
				{!! Form::text("lastName", " ", array("class" => "form-control", "id" => "lastNameEdit")) !!}
				<br>
				{!! Form::text("email", " ", array("class" => "form-control", "id" => "emailEdit")) !!}
				<br>
				{!! Form::submit('Save Changes', array("class" => "btn btn-primary")) !!}
				{!! Form::close() !!}
				<br>
				<h4 class="modal-title">Submit Hub Device</h4>
				<input class="form-control" id="addhub" name="addhub" value="" type="text">
				<input type="hidden" value="" id="acnt_hdn">
				<br>
				<span style="color:red;display:none;" id="hub_err_span"></span>
				<span style="color:green;display:none;" id="hub_succ_span"></span>
				<br>
				
				<input class="btn btn-primary" id="save_hub" value="Save Hub" type="button">
				
				<div style="display:none" id="response_div">
				<br>
				<h4 class="modal-title">Account Hubs</h4>
					<table class="table table-striped">
					<thead>
						<tr>
							<th>Delete</th>
							<th>Edit</th>
							<th>Hub Mac</th>
							<th>Keg's Name</th>
						</tr>
					</thead>
					<tbody id="response">
						
				   </tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</div>



@stop

@section('jsScript')
@include('employees.hubjs')
@stop

