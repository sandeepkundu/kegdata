@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li class="active"><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
		<li ><a href="/employee/misc">Misc</a></li>
  	</ul>
  @stop

@section('tabpane')
	@include('employees/_storenav', array('request' => "product"))
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Products</h3></div>
				<table class="table">
					<tr>
						<th>Delete</th>
						<th>Edit</th>
						<th>Name</th>
						<th>Stripe ID</th>
						<th>Short Description</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Purchases</th>
					</tr>
					@foreach($products as $p)
						<tr data-id="{{ $p->id }}"{{ empty($p->stripe_id) ? " class=bg-danger" : '' }}>
							<td style="width:20px;">{!! Form::open(['action' => array('Employees\StoreController@deleteProduct', $p->id), 'method'=>'delete']) !!}
								<input type="hidden" name="id" value="{{ $p->id }}" />
								<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete {{ $p->name }}?')"><i class="fa fa-trash"><span class="sr-only">Delete {{ $p->name}}</span></i></button>
							 	{!! Form::close() !!}

							</td>
							<td  style="width:20px;">
							<a href="/employee/store/product/{{ $p->id }}" class="btn btn-info">
								<i class="fa fa-pencil-square-o"><span class="sr-only">Edit {{ $p->name }}</span></i>
							</a>
							</td>
							<td>{{ $p->name }}</td>
							<td>{{ $p->stripe_id }}</td>
							<td>{{ $p->short_description }}</td>
							<td>{{ $p->price }}</td>
							<td>{{ $p->stock }}</td>
							<td></td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		@if(is_null($product))
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Add Product</h3></div>
				<div class="panel-body">
					{!! Form::open(['action' => 'Employees\StoreController@storeProduct', 'class' => 'form-horizontal', 'method' => 'put', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Product Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('category') ? '' : ' has-error has-feedback' }}">
							<p class="lead">Choose Product Categories</p>
							<div class="row">
								@foreach($categories as $cat)
								<div class="col-md-3">
									<div class="checkbox">
										<label>{!! Form::checkbox('category[]', $cat->id) !!} {{ $cat->name }}</label>
									</div>
								</div>
								@endforeach
							</div>
							@if ($errors->first('category'))
								<span class="help-block">{{ $errors->first('category') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('part_id') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="part_id">Part ID <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('part_id', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('part_id'))
								<span class="help-block">{{ $errors->first('part_id') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Price<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								<span class="input-group-addon">$</span>
								{!! Form::text('price', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('price'))
								<span class="help-block">{{ $errors->first('price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('stock') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="stock">Stock<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('stock', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('stock'))
								<span class="help-block">{{ $errors->first('stock') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Product Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Add Product', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@else
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Edit Product {{ $product->name }}</h3></div>
				<div class="panel-body">
					{!! Form::model($product, ['action' => array('Employees\StoreController@updateProduct', $product->id), 'class' => 'form-horizontal', 'method' => 'patch', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Product Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('category') ? '' : ' has-error has-feedback' }}">
							<p class="lead">Choose Product Categories</p>
							<div class="row">
								
								@foreach($categories as $cat)
								<div class="col-md-3">
									<div class="checkbox">
										<label>
											 <input type="checkbox" name="category[]" value="{{ $cat->id }}"
											 <?php 
											 foreach ($prodCat as  $product_cat ) {
											 	if($product_cat->category_id == $cat->id ) {
											 		echo "checked";
											 	}
											 }

											 ?>
												

											   >{{ $cat->name }}<br>
											   

											
										</label>
									</div>
								</div>
								@endforeach
							</div>
							@if ($errors->first('category'))
								<span class="help-block">{{ $errors->first('category') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('part_id') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="part_id">Part ID <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('part_id', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('part_id'))
								<span class="help-block">{{ $errors->first('part_id') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('price') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="price">Price<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								<span class="input-group-addon">$</span>
								{!! Form::text('price', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('price'))
								<span class="help-block">{{ $errors->first('price') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('stock') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="stock">Stock<i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('stock', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('stock'))
								<span class="help-block">{{ $errors->first('stock') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="slug">Slug <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('slug', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('slug'))
								<span class="help-block">{{ $errors->first('slug') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6{{ !$errors->first('image') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label for="image">Product Image</label>
								{!! Form::file('image') !!}
							</div>
							@if ($errors->first('image'))
								<span class="help-block">{{ $errors->first('image') }}</span>
							@endif
						</div>
						<div class="col-md-6">
							@if(!is_null($product->photos->first()))
								<img src="{{ $product->photos->first()->path }}" class="img-responsive" />
								<caption>{{ $product->photos->first()->path }}</caption>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="description">Description</label>
								{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('short_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="short_description">Short Description</label>
								{!! Form::text('short_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('short_description'))
								<span class="help-block">{{ $errors->first('short_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_description') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_description">Meta Description</label>
								{!! Form::text('meta_description', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_description'))
								<span class="help-block">{{ $errors->first('meta_description') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('meta_keywords') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="meta_keywords">Meta Keywords</label>
								{!! Form::text('meta_keywords', null, array('class' => 'form-control')) !!}
							</div>
							@if ($errors->first('meta_keywords'))
								<span class="help-block">{{ $errors->first('meta_keywords') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Edit Product', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endif
	</div>
@stop

@section('jsScript')
@stop

