@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop


@section('content')
		@if($tab == 'hub')
			@include('employees.hub')
		@elseif( $tab == 'account' )
			@include('employees.account')
		@elseif( $tab == 'user' )
			@include('employees.user')
		@elseif( $tab == 'products' )
			@include('employees.products')
		@elseif( $tab == 'orders' )
			@include('employees.orders')
		@elseif( $tab == 'reports' )
			@include('employees.reports')
		@elseif( $tab == 'misc' )
			@include('employees.misc')
		@else
			@include('employees.hub')
		@endif
@stop

