<html>
<head>
<title>{{ $sheetTitle }} </title>

</head>
  <body>
    <style>


  <table>{
border:1px solid #fcfcfc ;
}

th{
background:#426092 ;
border:1px solid #fcfcfc ;
color:#fcfcfc ;
font-family:Calibri, Arial, sans;
font-weight:bold;
padding:4px;
}

tbody td{
border:1px solid #fcfcfc ;
font-family:Calibri, Arial, sans;
padding:4px;
}

tbody tr:nth-child(odd){
 background: #B6D6F2 ;
}

tbody tr:nth-child(even){
  background:#D2E3F3 ;
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
  <body>
<table>
  <thead>
      <tr>
        <th>Order ID</th>
        <th>Type</th>
        <th>Name</th>
        <th>Address1</th>
        <th>Address2</th>
        <th>City</th>
        <th>State</th>
        <th>Zip Code</th>
        <th>Phone1</th>
        <th>Phone2</th>
        <th>Country</th>

      </tr>
    </thead>
    <tbody>

  @foreach($orders as $order)
  @foreach($order->orderAddress as $address)
  <tr>
        <td>{{ $order->id }}</td>
        <td>{{ $address->type=='s' ? 'Shipping' : 'Billing' }}</td>
        <td>{{ $address->name }}</td>
        <td>{{ $address->address1 }}</td>
        <td>{{ $address->address2 }}</td>
        <td>{{ $address->city }}</td>
        <td>{{ $address->tate }}</td>
        <td>{{ $address->zipcode }}</td>
        <td>{{ $address->phone1 }}</td>
        <td>{{ $address->Phone2 }}</td>
        <td>{{ $address->country}}</td>
  </tr>
  @endforeach
  @endforeach
</tbody>
  </table>
