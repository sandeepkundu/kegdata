<html>
	<head>
		<title>{{ $sheetTitle }}</title>

	</head>
			<body>

			</body>

				<style>
					<table>{
 border:1px solid #fcfcfc ;
}

th{
 background:#426092 ;
 border:1px solid #fcfcfc ;
 color:#fcfcfc ;
 font-family:Calibri, Arial, sans;
 font-weight:bold;
 padding:4px;
}

tbody td{
 border:1px solid #fcfcfc ;
 font-family:Calibri, Arial, sans;
 padding:4px;
}

tbody tr:nth-child(odd){
 background: #B6D6F2 ;
}

tbody tr:nth-child(even){
  background:#D2E3F3 ;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
		<body>

		<table>

			<thead>

			<tr>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Email</th>
				<th>Email2</th>
				<th>Phone 1</th>
				<th>Phone 2</th>
				<th>Active</th>
				<th>Admin</th>
			</tr>
			</thead>
			<tbody>
			@foreach($users as $user)
<tr data-userid="{{ $user->id }}">
					<td class="lastName">{{ ucwords ($user->lastName) }}</td>
				  <td class="firstName">{{ ucwords ($user->firstName) }}</td>
          <td class="Email">{{($user->email)}} </td>
					<td class="email2">{{ $user->email2 }}</td>
					<td class="phone1">{{ $user->phone1 }}</td>
					<td class="phone2">{{ $user->phone2 }}</td>
					<td class="isActive">{{ ($user->isActive==1) ? 'yes' : 'no' }}</td>
					<td class="isAdmin">{{ ($user->isAdmin==1) ? 'yes' : 'no' }}</td>
					</tr>
@endforeach
				</tbody>

			</table>
		</body>
</html>
