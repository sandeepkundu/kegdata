<script>
//On click and modal for editHub
$('.editButton').on('click keypress', function(){
    /*var userID = $(this).parents('tr').attr("data-userid");
    $("#user_id_edit").val(userID);
    $("#firstNameEdit").val($('tr[data-userid="' + userID + '"] .firstName').text());
    $("#lastNameEdit").val($('tr[data-userid="' + userID + '"] .lastName').text());
    $("#emailEdit").val($('tr[data-userid="' + userID + '"] .email').text());
    $("#editUserModal").modal();*/


    var hub_id = $(this).parents('tr').attr("data-id");
    $("#hub_id").val(hub_id);
    var account_id = $(this).parents('tr').attr("data-account-id");
    $("#account_id_edit").val(account_id);

     $("#hub_mac_id").val($('tr[data-id="' + hub_id + '"] .hub_mac').text());



    $("#editUserModal").modal();

});





//On click and modal for deleteHub
$('.deleteButton').on('click keypress', function(){
  
    var hub_id = $(this).parents('tr').attr("data-id");
    $("#del_hub_id").val(hub_id);

     var account_id = $(this).parents('tr').attr("data-account-id");
     $("#del_account_id_edit").val(account_id);

    $("#deleteUserModal").modal();

});



$('.editButtonn').on('click keypress', function(){
    var userID = $(this).parents('tr').attr("data-userid");
    var accountid = $(this).parents('tr').attr("data-accountid");
    $("#user_id_edit").val(userID);
    $("#firstNameEdit").val($('tr[data-userid="' + userID + '"] .firstName').text());
    $("#lastNameEdit").val($('tr[data-userid="' + userID + '"] .lastName').text());
    $("#emailEdit").val($('tr[data-userid="' + userID + '"] .email').text());
    $("#acnt_hdn").val(accountid);

    //run ajax for fetch hub mac
     var url = "/employee/fetch_device_info/abc";
     $.ajax({
        type: "GET",
        url: url ,
        data: {"account_id":accountid},
        async:false,
        success: function(result){
            var obj=JSON.parse(result);
            if(obj.status=="success"){ 
                $('#response_div').show();
                var hubmac = obj.hubs;
                var devices = obj.devices;
                //fetch device name
                var device_name='';

                for(i=0;i<devices.length;i++) {
                    var k = devices[i];
                    var deviceName = k.deviceName;
                    device_name +=deviceName+', ';
                }

                device_name = (device_name) ? device_name.slice(0, -2) : 'No keg added';

                $('#response').html('');
                var html='';
                for(i=0;i<hubmac.length;i++) {
                  var k=hubmac[i];
                  //console.log(k.id);
                  var tbl_row='<tr data-id="'+k.id+'" data-account-id="'+k.account_id+'"><td style="width:20px"><span class="fa fa-trash deleteButton"></span><span class="sr-only">Delete Hub</span></td><td style="width:20px"><span class="fa fa-pencil-square-o editButton"></span><span class="sr-only">Edit Hub</span></td><td class="hub_mac">'+k.hubMac+'</td><td>'+device_name+'</td></tr>';
                  html = html + tbl_row;
                }
                $('#response').html(html);
                calling_edit_on_ajax();
                calling_delete_on_ajax();


            } else {
                 $('#hub_err_span').show().text('');
                 $('#hub_err_span').show().text('Network Failure!').fadeOut('slow');
            }


    },

    });



    $("#editUserModall").modal();
});


$('#save_hub').on('click', function(){
    var add_hub=$('#addhub').val();
    var account_id=$('#acnt_hdn').val();
    if(add_hub=='' || add_hub == undefined || add_hub == null) {
        $('#hub_err_span').show().text('');
        $('#hub_err_span').show().text('Add hub textbox can not be empty!');
        return false;
    } else {
        var check_match = add_hub.match(/([0-9a-f]{2}:){5}[0-9a-f]{2}|[0-9a-f]{12}/);
        if(!check_match) {
            $('#hub_err_span').show().text('');
            $('#hub_err_span').show().text('Please enter a hub mac address as XX:XX:XX:XX:XX:XX or XXXXXXXXXXXX');
            return false;
        } else {
              $('#hub_err_span').hide().text('');
              var url = "/employee/addhubbymodal/"+add_hub;
                $.ajax({
                    type: "GET",
                    url: url ,
                    data: {"account_id":account_id},
                    async:false,
                    success: function(result){
                        var obj=JSON.parse(result);
                        //console.log(obj.data.account_id);
                        if(obj.status=="success"){
                            $('#addhub').val('');
                            $('#hub_succ_span').text('Hub created successfully').show().fadeOut('slow');
                            //location.reload();
                            //show response html
                            $('#response_div').show();
                            $('#response').html('');
                            var hubmac = obj.hubs;

                            var devices = obj.devices;
                            //fetch device name
                            var device_name='';

                            for(i=0;i<devices.length;i++) {
                                var k = devices[i];
                                var deviceName = k.deviceName;
                                device_name +=deviceName+', ';
                            }

                            device_name = (device_name) ? device_name.slice(0, -2) : 'No keg added';


                            var html='';
                            for(i=0;i<hubmac.length;i++) {
                              var k=hubmac[i];
                              //console.log(k.id);
                              var tbl_row='<tr data-id="'+k.id+'" data-account-id="'+k.account_id+'"><td style="width:20px"><span class="fa fa-trash deleteButton"></span><span class="sr-only">Delete Hub</span></td><td style="width:20px"><span class="fa fa-pencil-square-o editButton"></span><span class="sr-only">Edit Hub</span></td><td class="hub_mac">'+k.hubMac+'</td><td>'+device_name+'</td></tr>';
                              html = html + tbl_row;
                            }
                            $('#response').html(html);
                            calling_edit_on_ajax();
                            calling_delete_on_ajax();




                        }else{
                            $('#hub_err_span').show().text('');
                            $('#hub_err_span').show().text('Network Failure!').fadeOut('slow');
                        }
                    }

                });
        }

      
    }
   

});

function calling_edit_on_ajax() {

    $('.editButton').on('click keypress', function(){

    var hub_id = $(this).parents('tr').attr("data-id");
    $("#hub_id_second").val(hub_id);
    var account_id = $(this).parents('tr').attr("data-account-id");
    $("#account_id_edit_second").val(account_id);

     $("#hub_mac_id_second").val($('tr[data-id="' + hub_id + '"] .hub_mac').text());


     $("#editUserModall .close").click();

     var t=$('.modal_update_form').attr('action');
     $(".modal_update_form").attr("action", t+'/'+account_id);

    $("#editUserModalAnother").modal();

});
}

function calling_delete_on_ajax() {

    $('.deleteButton').on('click keypress', function(){
  
    var hub_id = $(this).parents('tr').attr("data-id");
    $("#del_hub_id_second").val(hub_id);

     var account_id = $(this).parents('tr').attr("data-account-id");

     var t=$('.modal_delete_form_second').attr('action');
     $(".modal_delete_form_second").attr("action", t+'/'+account_id);


     $("#del_account_id_edit_second").val(account_id);
     $("#editUserModall .close").click();
    $("#deleteUserModalSecond").modal();

});
}
</script>
