@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Employee Dashboard
@stop

@section('tabs')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	<li><a href="/employee/hub">Hub Coupler Mgmt</a></li>
	  	<li><a href="/employee/account">Account Management</a></li>
	  	<li><a href="/employee/user">User Management</a></li>
		<li class="active"><a href="/employee/store">Store</a></li>
		<li><a href="/employee/orders">Orders</a></li>
		<li><a href="/employee/reports">Reports</a></li>
		<li ><a href="/employee/misc">Misc</a></li>
  	</ul>
  @stop

@section('tabpane')
	@include('employees/_storenav', array('request' => $request))
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Tax Rates</h3></div>
				<table class="table">
					<tr>
						<th>Delete</th>
						<th>Edit</th>
						<th>Name</th>
						<th>Rate</th>
					</tr>
					@foreach($taxes as $p)
						<tr data-id="{{ $p->id }}">
							<td style="width:20px;">{!! Form::open(['action' => array('Employees\StoreController@deleteTax', $p->id), 'method'=>'delete']) !!}
								<input type="hidden" name="id" value="{{ $p->id }}" />
								<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete {{ $p->name }}?')"><i class="fa fa-trash"><span class="sr-only">Delete {{ $p->name}}</span></i></button>
							 	{!! Form::close() !!}

							</td>
							<td  style="width:20px;">
							<a href="/employee/store/tax/{{ $p->id }}" class="btn btn-info">
								<i class="fa fa-pencil-square-o"><span class="sr-only">Edit {{ $p->name }}</span></i>
							</a>
							</td>
							<td>{{ $p->name }}</td>
							<td>{{ $p->rate }}</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		@if(is_null($tax))
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Add Tax</h3></div>
				<div class="panel-body">
					{!! Form::open(['action' => 'Employees\StoreController@storeTax', 'class' => 'form-horizontal', 'method' => 'put', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Tax Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('rate') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="rate">Rate <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('rate', null, array('class' => 'form-control', 'required')) !!}
								<span class="input-group-addon">%</span>
							</div>
							@if ($errors->first('rate'))
								<span class="help-block">{{ $errors->first('rate') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Add Tax', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@else
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Edit Tax {{ $tax->name }}</h3></div>
				<div class="panel-body">
					{!! Form::model($tax, ['action' => array('Employees\StoreController@updateTax', $tax->id), 'class' => 'form-horizontal', 'method' => 'patch', 'files' => true]) !!}
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" for="name">Tax Name <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('name', null, array('class' => 'form-control', 'required')) !!}
							</div>
							@if ($errors->first('name'))
								<span class="help-block">{{ $errors->first('name') }}</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12{{ !$errors->first('rate') ? '' : ' has-error has-feedback' }}">
							<div class="input-group">
								<label class="input-group-addon" form="rate">Rate <i class="fa fa-asterisk text-danger"></i><span class="sr-only">Required</span></label>
								{!! Form::text('rate', null, array('class' => 'form-control', 'required')) !!}
								<span class="input-group-addon">%</span>
							</div>
							@if ($errors->first('rate'))
								<span class="help-block">{{ $errors->first('rate') }}</span>
							@endif
						</div>
					</div>
					{!! Form::submit('Edit Tax', array('class' => 'btn btn-primary')) !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endif
	</div>
@stop

@section('jsScript')
@stop

