<html>
<head>
<title>{{ $sheetTitle }} </title>
</head>
<body>


</body>

<style>
  table{
border:1px solid #fcfcfc ;
}

th{
background:#426092 ;
border:1px solid #fcfcfc ;
color:#fcfcfc ;
font-family:Calibri, Arial, sans;
font-weight:bold;
padding:4px;
}

tbody td{
border:1px solid #fcfcfc ;
font-family:Calibri, Arial, sans;
padding:4px;
}

tbody tr:nth-child(odd){
 background: #B6D6F2 ;
}

tbody tr:nth-child(even){
  background:#D2E3F3 ;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
    <body>

<table>

    <thead>


      <tr>
          <th>Status</th>
          <th>Created At</th>
          <th>Account Name</th>
          <th>User Email</th>
          <th>Subscription</th>
          <th>Items</th>
          <th>Subtotal</th>
          <th>Tax</th>
          <th>Shipping</th>
          <th>Total</th>
      </tr>
    </thead>
<tbody>
  @foreach($orders as $order)
<tr>

          <td>{{ $order->status }}</td>
          <td>{{ $order->created_at->toDayDateTimeString()   }}</td>
          <td>{{ $order->account->accountName }}</td>
          <td>{{ $order->user->email }}</td>
          <td>{{ (is_null($order->subscription)) ? 'No' : 'Yes'}}</td>
          <td>{{ (is_null($order->products)) ? 'No' : 'Yes' }}</td>
          <td>{{ $order->subtotal }}</td>
          <td>{{ $order->tax }}</td>
          <td>{{ $order->shipping }}</td>
          <td>{{ $order->total }}</td>
</tr>
      @endforeach
</tbody>
  </table>
</body>
</html>
