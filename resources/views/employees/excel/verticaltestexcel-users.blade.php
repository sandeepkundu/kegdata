<html>
{{--this is for the mockup spreadsheet--}}
	<head>
		<title>{{ $sheetTitle }}</title>
	</head>

<style>

</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<body>
<table>
$sheet->getStyle('B1')->getAlignment()->setTextRotation(45);
<thead>
<tr>
<th></th>
<th>Budweiser</th>
<th>Bud Light</th>
<th>Corona</th>
<th>Coors Light</th>
<th>Natural Light Busch</th>
<th>Cronna Light</th>
<th>Corona Extra</th>
<th>Heineken</th>
<th>Miller High Life</th>
<th>Keystone Light</th>
<th>Michelob</th>
<th>Natural Ice</th>
<th>Budweiser Select</th>
<th>Icehouse Micheob Light</th>
<th>Papst Blue Ribbon</th>
<th>Coors</th>
<th>Labatt Blue</th>
<th>Colt 45</th>
<th>King Cobra</th>
<th>Guinness Stout</th>
<th>Rolling Rock</th>
<th>Bud Ice</th>
<th>Fosters</th>
<th>Fireman's Four</th>
<th>Blue Moon</th>
<th>Pale Ale</th>
<th>Red Rock</th>
<th>Stripes</th>
<th>Glolden Beer</th>
<th>Hamster Beer</th>
<th>Yellow Tail</th>
<th>Fred's private Brew</th>
<th>Sam Adams</th>
<th>Lager Four</th>
	</tr>
		</thead>
<tbody>

<th><tr>Applebee's Neighborhood Grill &amp; Bar</tr></th>
<th><tr>Big Boy Restaurant &amp; Bakery/Frisch's Big Boy</tr></th>
<th><tr>BJ's Restaurant &amp; Brewery</tr></th>
<th><tr>Bob Evans Restaurants</tr></th>
<th><tr>Bonefish Grill</tr></th>
<th><tr>Buffalo Wild Wings Grill &amp; Bar</tr></th>
<th><tr>California Pizza Kitchen</tr></th>
<th><tr>Carrabba's Italian Grill</tr></th>
<th><tr>Cheddar's</tr></th>
<th><tr>Chili's Grill &amp; Bar</tr></th>
<th><tr>Chipotle Mexican Grill/Chipotle</tr></th>
<th><tr>Chuck E. Cheese's</tr></th>
<th><tr>CiCi's Pizza</tr></th>
<th><tr>Cracker Barrel Old Country Store</tr></th>
<th><tr>Culver's</tr></th>
<th><tr>Disney theme parks, hotels &amp; resorts</tr></th>
<th><tr>Domino's Pizza</tr></th>
<th><tr>El Pollo Loco</tr></th>
<th><tr>Famous Dave's</tr></th>
<th><tr>Golden Corral</tr></th>
<th><tr>Hilton Hotels</tr></th>
<th><tr>Holiday Inn</tr></th>
<th><tr>Hooters of America</tr></th>
<th><tr>Hyatt Hotels</tr></th>
<th><tr>Little Caesars Pizza</tr></th>
<th><tr>Logan's Roadhouse</tr></th>
<th><tr>LongHorn Steakhouse/Darden Restaurants</tr></th>
<th><tr>Marriott Hotels &amp; Resorts</tr></th>
<th><tr>O'Charley's</tr></th>
<th><tr>Olive Garden/Darden Restaurants</tr></th>
<th><tr>On the Border Mexican Grill &amp; Cantina</tr></th>
<th><tr>Outback Steakhouse</tr></th>
<th><tr>P.F. Chang's China Bistro</tr></th>
<th><tr>Panera Bread</tr></th>
<th><tr>Papa John's Pizza</tr></th>
<th><tr>Papa Murphy's Take 'N' Bake Pizza</tr></th>
<th><tr>Perkins Restaurant and Bakery</tr></th>
<th><tr>Pizza Hut</tr></th>
<th><tr>Qdoba Mexican Grill</tr></th>
<th><tr>Red Lobster/Darden Restaurants</tr></th>
<th><tr></th></tr>

		</tbody>
	</table>
</body>
</html>
