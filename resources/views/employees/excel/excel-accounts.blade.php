<html>
	<head>
			<title>{{ $sheetTitle }}</title>
			<style>
			table{
			 border:1px solid #fcfcfc ;
			}

			th{
			 background:#426092 ;
			 border:1px solid #fcfcfc ;
			 color:#fcfcfc ;
			 font-family:Calibri, Arial, sans;
			 font-weight:bold;
			 padding:4px;
			}

			tbody td{
			 border:1px solid #fcfcfc ;
			 font-family:Calibri, Arial, sans;
			 padding:4px;
			}

			tbody tr:nth-child(odd){
			 background: #B6D6F2 ;
			}

			tbody tr:nth-child(even){
			  background:#D2E3F3 ;
			}
			</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
		<table>

				<thead>
				<tr>
			<th>id</th>
			<th>Account Name</th>
			<th>Account Type</th>
		  <th>Primary Contact</th>
			<th>Account Email1</th>
			<th>Account Email2</th>
			<th>Account Phone1</th>
			<th>Account Phone2</th>
			<th>Website</th>
		  {{--<th>Stripe Active</th>--}}
		  {{--<th>Stripe Plan</th>--}}
		  <th>Trial Ends At</th>
			<th>Subscription Ends At</th>
			<th> Address1</th>
			<th> Address2</th>
			<th> City</th>
			<th> State</th>
			<th> Zip Code</th>
			<th> County</th>
			<th> Country Code</th>
</tr>
</thead>

		<tbody>
		@foreach($accounts as $account)
<tr data-accountid="{{ $account->id}}">
			<td class="id">{{ $account->id }}</td>
			<td class="accountName">{{ $account->accountName }}</td>
			<td class="account-type">{{ $account->account_Type->description }}</td>
			<td class="accountPrimaryContact">{{ ($account->primaryContact==1) ? 'yes' : 'no'  }}</td>
			<td class="accountEmail1">{{ $account->accountEmail1 }}</td>
			<td class="accountEmail2">{{ $account->accountEmail2 }}</td>
			<td class="accountPhone1">{{ $account->accountPhone1 }}</td>
			<td class="accountPhone2">{{ $account->accountPhone2 }}</td>
			<td class="website">{{ $account->website }}</td>
{{-- <td class="stripe_active">{{ $account->stripe_active}}</td>--}}
{{-- <td class="stripe_plan">{{ $account->stripe_plan }}</td>>--}}
		 <td class="trial_ends_at">{{ $account->trial_ends_at  }}</td>
		 <td class="subscription_ends_at">{{ $account->subscription_ends_at }}</td>
		 <?php
$address = $account->accountAddress->first();
 ?>
		<td class="address">{{ $account->address1 }}</td>
		<td class="address">{{ $account->address2 }}</td>
		<td class="address">{{ $account->city }}</td>
		<td class="address">{{ $account->stateCode }}</td>
		<td class="address">{{ $account->zipCode }}</td>
		<td class="address">{{ $account->county }}</td>
		<td class="address">{{ $account->countryCode }}</td>

						@endforeach
							</tr>
					</tbody>
				</table>

</html>
