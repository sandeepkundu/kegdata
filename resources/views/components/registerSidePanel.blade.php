	<div class="panel panel-primary">
		<div class="panel-heading"><h2 class="panel-title">Register for {{ Config::get('constants.COMPANY_NAME') }}</h2></div>
		<div class="panel-body">
		{!! Form::open(['class' => 'form-horizontal', 'url' => 'register']) !!}
			<div class="row form-group">
				{!! Form::label('zipcode', "Zip Code", ['class' => 'sr-only']) !!}
				<div class="col-sm-12">
					<div class="input-group">
						<div class="input-group-addon">Zip Code</div>
						{!! Form::input('number','zipcode', null, ['class' => 'form-control']) !!}
					</div>
				</div>
			</div>
			<div class="form-row">
				{!! Form::label('accountType', "Account Type") !!}
				{!! Form::select('accountType',$Account_Types, null, ['class' => 'form-control']) !!}
			</div>
			<div class="headroom-top form-row text-center">
				{!! Form::submit('Register for KegData', ['class' => 'btn btn-primary form-control']) !!}
			</div>
		</form>
		{!! Form::close() !!}
		</div>
	</div> 