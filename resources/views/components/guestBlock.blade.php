@if (Auth::guest())
<h4 id="welUser">Welcome Guest!</h4>
<div id="login-btns" class="btn-group">
	<a href="/auth/login" class="btn btn-primary" id="loginBtn" >Login</a>
	<a href="/register" class="btn btn-primary">Register</a>
</div>
@else
<h4 id="welUser">Welcome {{ Auth::user()->firstName  }} {{ Auth::user()->lastName }}!</h4>
<div id="login-btns" class="btn-group">
	<a href="/auth/logout" class="btn btn-primary" id="loginBtn" >Logout</a>
	<a href="/dashboard" class="btn btn-primary">Dashboard</a>
</div>
@endif