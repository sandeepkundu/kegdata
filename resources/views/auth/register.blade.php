@section('title')
	Register for {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info{{empty($brewDistSearch) ? ' hidden' : '' }}" id="panelSelect">
				<div class="panel-heading"><h2 class="panel-title">Are you...<i class="fa fa-times close pull-right" data-target="panelSelect"><span class="sr-only">Close Panel</span></i></h2></div>
				<div class="panel-body">
					<p class="lead">The following are the Breweries and Distributors already in our system. Are you one of the following?</p>
					<span class="text-danger"><strong>Click to select</strong></span><br />
					<div class="row" id="brewDistRow">
						@foreach($brewDistSearch as $bd)
							@if($bd->accountID == 0 && $bd->verified == 0)
							<div class="col-md-4">
								<div class="clickable box brewDistSelect" id="{{ $bd['type'].$bd['id'] }}" data-dist="{{ $bd }}">
									<address>
										<strong>{{ $bd->name }}</strong><br />
										{{ $bd->address1 }}<br />
										@if($bd->address2)
											{{ $bd->address2 }}
											<br />
										@endif
										{{ $bd->city }}, {{ $bd->stateCode }} {{ $bd->zipcode }}
									</address>
								</div>
							</div>
							@endif
						@endforeach
						<div class="col-md-4">
							<div class="clickable box brewDistSelect" id="notListed" data-dist="">
								<strong>NOT LISTED</strong>		
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-primary{{empty($brewDistSearch) ? '' : ' hidden' }}" id="panelForm">
				<div class="panel-heading"><h2 class="panel-title">Register for {{ Config::get('constants.COMPANY_NAME') }}</h2></div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					{!! Form::open(['url' => '/auth/register']) !!}
					{!! Form::hidden('_method', 'put') !!}
					{!! Form::hidden('brewDistID', '') !!}
					<fieldset><legend>Account Information</legend>
						<div class="form-row">
							<div class="col-md-4">
								<div class="form-group{{ !$errors->first('zipcode') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('zipcode', "Zip Code", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('zipcode', old('zipcode'), ['class'=>'form-control', 'placeholder' => 'Zip Code', 'required' => 'required']) !!}
										
									</div>
									@if ($errors->first('zipcode'))
										<span class="help-block">{{ $errors->first('zipcode') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group{{ !$errors->first('accountType') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('accountType', "Account Type", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong>Account Type</div>
										{!! Form::select('accountType',$accountTypeSelect, old('accountType'), ['class' => 'form-control', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('accountType'))
										<span class="help-block">{{ $errors->first('accountType') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('accountName') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('accountName', "Account Name", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('accountName', old('accountName'), ['class'=>'form-control', 'placeholder' => 'Account Name', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('accountName'))
										<span class="help-block">{{ $errors->first('accountName') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('address1') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('address1', "Address 1", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="glyphicon glyphicon-road"></i></div>
										{!! Form::text('address1', old('address1'), ['class'=>'form-control', 'placeholder' => 'Address 1', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('address1'))
										<span class="help-block">{{ $errors->first('address1') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('address2') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('address2', "Address 2", ['class' => 'sr-only']) !!}
									{!! Form::text('address2', old('address2'), ['class'=>'form-control', 'placeholder' => 'Address 2']) !!}
								</div>
								@if ($errors->first('address2'))
										<span class="help-block">{{ $errors->first('address2') }}</span>
									@endif
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4">
								<div class="form-group{{ !$errors->first('city') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('city', "City", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('city', old('city'), ['class'=>'form-control', 'placeholder' => 'City', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('city'))
										<span class="help-block">{{ $errors->first('city') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group{{ !$errors->first('stateCode') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('stateCode', "State", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong>State</div>
										{!! Form::select('stateCode',$states, old('stateCode'), ['class' => 'form-control']) !!}
									</div>
									@if ($errors->first('stateCode'))
										<span class="help-block">{{ $errors->first('stateCode') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-4">
								<div class="form-group{{ !$errors->first('county') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('county', "County", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('county', old('county'), ['class'=>'form-control', 'placeholder' => 'County', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('county'))
										<span class="help-block">{{ $errors->first('county') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('countryCode') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('countryCode', "Country", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="glyphicon glyphicon-globe"></i></div>
										@if(old('country'))
											{!! Form::select('countryCode',$countries, old('countryCode'), ['class' => 'form-control', 'required' => 'required']) !!}
										@else
											{!! Form::select('countryCode',$countries, 'US', ['class' => 'form-control', 'required' => 'required']) !!}
										@endif
									</div>
									@if ($errors->first('acountryCode'))
										<span class="help-block">{{ $errors->first('countryCode') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('currency') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('currency', "Currency", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon">Currency</div>
										@if(old('currency'))
											{!! Form::select('currency',$currencies, old('currency'), ['class' => 'form-control']) !!}
										@else
											{!! Form::select('currency',$currencies, 'USD', ['class' => 'form-control']) !!}
										@endif
										
									</div>
									@if ($errors->first('currency'))
										<span class="help-block">{{ $errors->first('currency') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('phone1') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('accountPhone1', "Phone", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="fa fa-phone"></i></div>
										{!! Form::input('tel','accountPhone1', old('accountPhone1'), ['class'=>'form-control', 'placeholder' => 'Phone', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('phone1'))
										<span class="help-block">{{ $errors->first('phone1') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('phone2') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('accountPhone2', "Fax", ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-fax"></i></div>
										{!! Form::input('tel' ,'accountPhone2', old('accountPhone2'), ['class'=>'form-control', 'placeholder' => 'Fax']) !!}
									</div>
									@if ($errors->first('phone2'))
										<span class="help-block">{{ $errors->first('phone2') }}</span>
									@endif
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset><legend>Administrator Information</legend>
						<div class="form-row">
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('firstName') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('firstName', 'First Name', ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('firstName', old('firstName'), ['class'=>'form-control', 'placeholder' => 'First Name', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('firstName'))
										<span class="help-block">{{ $errors->first('firstName') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ !$errors->first('lastName') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('lastName', 'Last Name', ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong></div>
										{!! Form::text('lastName', old('lastName'), ['class'=>'form-control', 'placeholder' => 'Last Name', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('lastName'))
										<span class="help-block">{{ $errors->first('lastName') }}</span>
									@endif
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset><legend>Login Information</legend>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('email') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('email', 'Login Email', ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="fa fa-envelope-o"></i></div>
										{!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder'=> 'email@domain.com', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('email'))
										<span class="help-block">{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('password') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('password', 'Password', ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="fa fa-key"></i></div>
										{!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder'=> 'Type Password', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('password'))
										<span class="help-block">{{ $errors->first('password') }}</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="col-md-12">
								<div class="form-group{{ !$errors->first('password_confirmation') ? '' : '  has-error has-feedback' }}">
									{!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'sr-only']) !!}
									<div class="input-group">
										<div class="input-group-addon"><strong class="fa fa-asterisk text-danger"><span class="sr-only">Required</span></strong><i class="fa fa-key"></i></div>
										{!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder'=> 'Confirm Password', 'required' => 'required']) !!}
									</div>
									@if ($errors->first('password_confirmation'))
										<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
									@endif
								</div>
							</div>
						</div>
					</fieldset>


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('jsScript')
<script>
$(document).ready(function(){

	$('#zipcode').on('blur', function(){
		if($('#zipcode').val() != '' && ($("#accountType option:selected").val() == 'B' || $("#accountType option:selected").val() == 'D')){
			findBreweries();
		}else{
			$('#brewDistID').val('');
			$("#panelSelect").addClass('hidden');
		}
	});

	$('#panelSelect').on('click', '.brewDistSelect', function(){
		if($(this).attr('id') == 'notListed'){
			$('#brewDistID').val('');
			$('#panelSelect').addClass('hidden');
			$('#panelForm').removeClass('hidden');
		}else{
			var formData = $.parseJSON($(this).attr('data-dist'));

			$('#accountType option[value="'+formData.type+'"]').prop('selected',true);
			$('input[name="brewDistID"]').val(formData.id);
			$('#accountName').val(formData.name);
			$('#accountPhone1').val(formData.phone);
			$('#accountPhone2').val(formData.fax);
			$('#address1').val(formData.address1);
			$('#address2').val(formData.address2);
			$('#city').val(formData.city);
			$('#stateCode option[value="'+formData.stateCode+'"]').prop('selected',true);
			$('#zipcode').val(formData.zipcode);
			$('#countryCode option[value="'+formData.countryCode+'"]').val(formData.countryCode).prop('selected',true);;
			$('#county').val(formData.county);
			$('#panelSelect').addClass('hidden');
			$('#panelForm').removeClass('hidden');
		}
	});

	$('#accountType').on('change',function(){
		if($('#zipcode').val() != '' && ($("#accountType option:selected").val() == 'B' || $("#accountType option:selected").val() == 'D')){
			findBreweries();
		}else{
			$('#brewDistID').val('');
			$("#panelSelect").addClass('hidden');
		}
	});

	$('.close').on('click', function(){
		var what = $(this).attr('data-target');
		$('#brewDistID').val('');
		$('#' + what).addClass('hidden');
	});
});

function findBreweries(){
	var url = '/api/brewdist/zipcode/'+$('#zipcode').val();
	$.getJSON(url, function(results){
		var data = results['data'];
		$('#brewDistRow').empty();
		for(var i = 0; i < data.length; i++){
			if(data[i].account_id == 0 && data[i].verified == 0){
			 	var name = $('<strong>').text(data[i].name.toUpperCase());
			 	var address = $('<address>').append(name).append('<br />').append(data[i].address1.toUpperCase() + '<br />');
			 	if(data[i].address2 != ''){
			 		address.append(data[i].address2.toUpperCase() + '<br />');
			 	}
			 	address.append(data[i].city.toUpperCase() + ', ' + data[i].stateCode.toUpperCase() + ' ' + data[i].zipcode);

			 	var container = $('<div>').addClass('clickable box brewDistSelect').attr('id', data[i].type + data[i].id).attr('data-dist', JSON.stringify(data[i]));
			 	container.append(address);

			 	var column = $('<div>').addClass('col-md-4');
			 	column.append(container);
			 	$('#brewDistRow').append(column);
		 	}
		}
		var notListed = $('<div>').addClass('col-md-4');
		var box = $('<div>').addClass('clickable box brewDistSelect').attr('id', 'notListed').attr('data-dist', '');
		var strong = $('<strong>').text('NOT LISTED');
		box.append(strong);
		notListed.append(box);
		$('#brewDistRow').append(notListed);
		$('#panelSelect').removeClass('hidden');
	}).fail(function(e){

	});
}
</script>
@endsection
