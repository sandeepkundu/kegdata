@section('title')
	Register for {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading"><h1 class="panel-title">{{ $title }}</h1></div>
				<div class="panel-body">
					<div class="col-md-5 col-md-offset-1 pull-right">
						<p><img src="/img/thankyou.jpg" class="img-rounded img-responsive" /></p>
					</div>
					<p class="lead">
					@if($success)
						Your account is now verified. Please <a href="/auth/login">Login</a>.
					@else
						For some reason, verification of your account has failed. Please check for more than one email and find the most recent one for verification.
						If that still fails, please contact us to assist you.
					@endif
					</p>
				</div>
			</div>
		</div>	
	</div>
@endsection