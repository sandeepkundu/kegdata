@section('title')
	Register for {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading"><h1 class="panel-title">Thank you for registering with {{ Config::get('constants.COMPANY_NAME') }}</h1></div>
				<div class="panel-body">
					<div class="col-md-5 col-md-offset-1 pull-right">
						<p><img src="/img/thankyou.jpg" class="img-rounded img-responsive" /></p>
					</div>
					<p class="lead">You will receive an email soon with further instructions about how to confirm your account.</p>
					<p>You may need to check your spam folder the email.</p>
					<p>If for some reason you don't receive the email, you can login to send a new one. However; some emails are delayed 
					so have a beer while you are waiting on the email.</p>
					<blockquote>-The {{ Config::get('constants.COMPANY_NAME') }} Staff</blockquote>
				</div>
			</div>
		</div>	
	</div>
@endsection