
@section('title')
	About Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}, The Story and the Company
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10">
	<h1 class="title">About Our Company</h1>
	<p class="lead">{{ Config::get('constants.COMPANY_NAME') }} is a Texas LLC specializing in remote fluid measuring technologies. 
	We are currently specializing in beer but have the capacity to measure any fluid remotely 
	anywhere in the world and report levels and usage autonomously.</p>

	<p>    Ikeg, LLC, the parent company of {{ Config::get('constants.COMPANY_NAME') }}, is based in Houston, Texas. Ikeg started in 
	2011 and has been in research and development for the last 3 years, making sure we have 
	designed the most advanced and cost effective way to monitor keg levels. The two 
	primary members of the company are Louis Apostolakis and Kevin Beal. The partners, 
	along with a team of engineers and software designers, promise to deliver a new product 
	to the market unprecedented in it's technology, cost effectiveness and ease of installation. 
	</p>

 	<p>{{ Config::get('constants.COMPANY_NAME') }} first produced a working system over a year ago but with advancements in technology
 	 and advanced testing the staff determined that in order to satisfy the larger customers and 
 	 to simplify the system that we would send the data wirelessly. We went back into engineering 
 	 in 2013 to reengineer the system to make it wireless and infinitely expandable.
 	</p>
    
    <p>We hope people enjoy our new system and share our love for beer.</p> 
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




