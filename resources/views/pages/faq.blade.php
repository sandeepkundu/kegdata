
@section('title')
	FAQS for {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10">
	<h1 class="title">FAQs</h1>
	<p class="lead">Below are some answers to many of the questions you may have about the Kegdata&trade; system.</p>
	<ol>
		<li>
			<h3>How does the Kegdata&trade; system work? </h3>
			<p>Kegdata&trade; works through an extremely advanced electronic sensor 
			which determines the amount of beer in a keg. This data is collected and 
			send to our server where it is posted to the user account.</p>
		</li>
		<li>
			<h3>How many kegs can I have?</h3>
			<p>Our system allows you to monitor up to 24 kegs per control board. 
			You cannot add additional boards at this point but we do plan on implementing an update that hopefully will allow the system to handle up to 100 kegs.			</p>
		</li>

		<li>
			<h3>Can I buy a keg coupler without an account?</h3>
			<p>No, we only sell to registered users.</p>
		</li>

		<li>
			<h3>What do the lights mean?</h3>
			<p>There are lights on the power supply, power distribution board, 
			control board, coupler and coupler sensor. Each one of these lights 
			helps the end user to determine if there are any problems with the system 
			and to quickly diagnose any problems quickly.  Most of the lights will 
			indicate if that particular component has power. There are secondary lights
			 for shorts in the system, lights to tell the end user if the system is 
			 transmitting data and lights to indicate a pour is occurring or the coupler is open.</p>
			</li>

		<li>
			<h3>Is there a residential system? </h3>
			<p>Yes, we sell a system with one coupler, a power supply, wiring and a control board.</p>
		</li>

		<li>
			<h3>What comes with the system?</h3>
			<p>The basic 
			system will include a control board, 1 coupler, a power splitter 
			and wiring. You can add up to 23 more couplers to the system.</p>
		</li>

		<li>
			<h3>What is enterprise?</h3>
			<p>Enterprise is a special 6-10 character code that allows a user to gather 
			data from multiple restaurants for reporting. A user at the distribution 
			level can view all data for any restaurant that has the special enterprise 
			code in their profile.</p>
		</li>

		<li>
			<h3>Why is there a distributor dropdown list?</h3>
			<p>The list is to make sure that the supplier is in our system for 
			reordering purposes and reporting for that distributor. The dropdown 
			list prevents a restaurant from inputting incorrect supplier information.</p>
		</li>

		<li>
			<h3>What if my distributor is not on the list?</h3>
			<p>The restaurant needs to contact the supplier, if they want reporting 
			and automated reordering, and have them create a distributor account.</p>
		</li>

		<li>
			<h3>What kind of reporting will be available?</h3>
			<p>You can create any number of reports including current beer levels, temperature charts
			or the amount dispensed during any particular time period.</p>
		</li>

		<li>
			<h3>Does Kegdata&trade; have anything to sense temperature?</h3>
			<p>Yes, Kegdata&trade; provides the temperature of the refrigerator and 
			also can graph the temperature over time.</p>
		</li>
		<li>
			<h3>Does Kegdata&trade; provide any alerts for high temperature warnings?</h3>
			<p>Yes, you can get emails or texts for any our of range temperature 
			determined in the user settings.</p>
		</li>
		<li>
			<h3>What services are provided for a distributor?</h3>
			<p>The distributor can check any beer level for any restaurant on the system 
			that has selected them as the distributor for that particular brand. The 
			distributor can also run any number of reports for usage by city, zip code, 
			county or state. The distributor will also get emails from the restaurant to 
			notify them that they need another keg according to a preset level. There are 
			also other special services for distributors including presetting delivery routes 
			for truck loading.</p>
		</li>

		<li>
			<h3>What does the system cost?</h3>
			<p>You will need to go the <a href="store.blade.php">Browse Shop</a> pages to see current pricing.</p>
		</li>

		<li>
			<h3>Are there discounts for multiple accounts or multiple restaurants?</h3>
			<p>You will need to go to the purchase area for your particular product to see 
			any discounts available We do offer prepaid discounts for multiple year prepaid 
			contracts.</p>
		</li>
		<li>
			<h3>How easy it is to install?</h3>
			<p>Yes. The system is very easy to install. Go to the <a href="install.blade.php">install page</a> to see the video and instalation details.</p>
		</li>

		<li>
			<h3>Installing the system</h3>
			<ol>
				<li>Receive the system and check contents to be sure your order was compete.</li>
				<li>Replace your existing couplers with the Kegdata&trade; couplers by basically unhooking 
				the existing beer and CO2 lines from the coupler and changing out the coupler. Hook CO2 and 
				Beer lines back to new coupler.</li>
				<li>Install the control board to the inside of the area where the keg(s) are located.</li>
				<li>Install the power distribution board for multiple kegs, or the splitter for residential use.</li>
				<li>Hook up the power to the couplers.</li>
				<li>Hook up power to the distribution board.</li>
				<li>Check to be sure all of the power lights are on for the couplers</li>
				<li>Using a wireless device log into the control board and put in the required information. (Specify)</li>
				<li>The board should start talking to our server.</li>
				<li>Make a list of the 4 digit code on each of the couplers and the associated beer for that coupler.</li>
				<li>Enter that information into your Kegdata&trade; web page so each coupler number corresponds 
				to the particular beer in that keg. Your kegs should automatically show the four digit number for 
				each of the couplers so you simply need to enter what type of beer is in the keg.</li>
				<li>You should be ready to go and your system will start showing the volume in each of the kegs.</li>
			</ol>
 		</li>
		<li>
			<h3>What if the levels are not the same as what is in the keg? </h3>
			<p>Yes, you can calibrate the kegs if needed. Login and go the keg setup page and 
			follow the instructions. Once this is done you should not have to do this again. It 
			is important to note that not all kegs are filled to their specified level.</p>
		</li>

		<li>
			<h3>Will the coupler untap when the keg is empty?</h3>
			<p>Yes, it will untap as soon as the keg is empty, automatically. You can bypass this by removing
			a small connector inside of the coupler.</p>
		</li>

		<li>
			<h3>How do I reset the coupler once it is untapped?</h3>
			<p>All you have to do is hook it up to the new keg and push the handle down. 
			It will latch itself.</p>
		</li>

		<li>
			<h3>How do I manually untap the keg?</h3>
			<p>Just reach under the coupler and pull the small tab sticking down away from the coupler.</p>
		</li>

		<li>
			<h3>What if the keg keeps untapping and I know there is beer in the keg?</h3>
			<p>You can unplug the power to that keg coupler which will disable the data and auto 
			uncoupling for that particular coupler. You would need to contact our service
			 department for a solution.</p>
			</li>

		<li>
			<h3>What do I do if the system goes "haywire"? </h3>
			<p>You can simply disrupt the power to the system by unplugging it. The system will 
			work just like normal keg couplers, less the data feeds and auto uncoupling.</p>
		</li>

		<li>
			<h3>Can I order more couplers?</h3>
			<p>Yes you can order couplers as long as you have an active account with us.</p>
		</li>

		<li>
			<h3>Can I order Kegdata&trade; couplers if I do not have an account?</h3>
			<p>No.</p>
		</li>

		<li>
			<h3>What do I do if there are problems with couplers or the system?</h3>
			<p>We have provided the most comprehensive problem solving tutorials and videos you 
			could image. Log into your account and click on the <a href="support.blade.php">support tab</a>.</p>
		</li>

		<li>
			<h3>Are the couplers available for all types of kegs? </h3>
			<p>Yes, we are starting with "D" Sankey couplers and euro "S" couplers to start. We will have 
			all couplers as time progresses.</p>
		</li>

		<li>
			<h3>Are there online videos for users to learn how to operate, troubleshoot and install?</h3>
			<p>   Yes, there are videos you can <a href="install.blade.php">watch here</a>.</p>
		</li>

		<li>
			<h3>Are there additional costs after purchasing the system? </h3>
			<p>Yes, there is a monthly subscription fee for cloud based monitoring and reporting. 
			For monthly fees see Browse Shop.</p>
		</li>

		<li>
			<h3>Do you charge an additional  fee for each keg sold?</h3>
			<p>Yes, there is a small fixed monthly fee per keg along with the monthly fee. 
			Register and go to the store for pricing.</p>
		</li>

		<li>
			<h3>What if I do not like the system? Can I return it?</h3>
			<p>Absolutely! We will refund you the entire cost of system during the first 60 days 
			after purchase. The system must be returned to us complete and in good condition. 
			Prepaid plans will be prorated according to what you have used at the higher rate 
			for the 30 day period. Systems returned within 60 days of receipt will receive a full 
			refund, including any subscription fees.</p>
		</li>

		<li>
			<h3>Is there an app for this system?</h3>
			<p>You can log into the web with your phone and see your kegs. The web is designed for smartphone viewing.</p>
		</li>

		<li>
			<h3>Can the breweries or distributors see each others data?</h3>
			<p>No. The restaurants, bars, distributors and breweries are all linked together by 
			brand and suppliers. All of these parties are segregated according to who they service.
			 As an example if a bar served Budweiser and Coors draft beer, both of these brewers could
			  run a report for that bar but each of them could not see the others data. Also if these
			   two brands were supplied by two different distributors then each of these suppliers 
			   could only see the brands they supply.</p>
			  </li>

		<li>
			<h3>What is the accuracy?</h3>
			<p>We expect the accuracy to be about 20 oz per keg but we are working to make the levels as accurate as possible.</p>
		</li>

		<li>
			<h3>What is the warranty?</h3>
			<p>We guarantee that all of our products will be free from defects and workmanship for 
			one year from the original date of purchase. Kegdata&trade; will refund or replace any 
			product that is defective during the warranty period upon return of the defective product.</p>
		</li>

		<li>
			<h3>What does the system cost?</h3>
			<p>Please go to the <a href="store.blade.php">store</a> for pricing and availability.</p>
		</li>
</ol>
<p>More information will be posted soon.<br />
<small>October 24, 2016</small></p>
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




