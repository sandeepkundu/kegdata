
@section('title')
	Install Instructions for {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10" id="top">
	<div class="row">
		<div class="col-md-12">
			<h1 class="title">KegData Installation</h1>
				<p>The installation of the KegData system is 
			simple. Below are the steps and information to install a system and the 
		  tools needed.</p>
			<nav class="navbar navbar-default">

				<ul class="nav navbar-nav">
					<li><a href="#tools">Tools Required</a></li>
					<li><a href="#layout">System Layout</a></li>
					<li><a href="#system">Putting in the system</a></li>
					<li><a href="#hub">Setting up your hub</a></li>
					<li><a href="#testing">Testing your system</a></li>
				</ul>
			</nav><p>Below is a typical installation of a 6 keg system in a restaurant. You may also follow the step by step instructions below.</p>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/EHTH35TQvAk" frameborder="0" allowfullscreen></iframe>
		
		</div>		
	</div>
	<section class="clearfix">
		<h2 id="tools" class="text-uppercase">Tools Required</h2>
		<ol>
			<li>One large crescent wench</li>
			<li>One power drill with 1/2&quot; metal bit</li>
			<li>One pair of diagonal pliers</li>
			<li>Two pair of vice-grip pliers (optional)</li>
			<li>Phillips Screwdriver, or bit for drill</li>
		</ol>
		<a href="#top" class="pull-right">Return to top<span class="glyphicon glyphicon-arrow-up"></span></a>
	</section>
	<hr />
	<section class="clearfix">
		<h2 id="layout" class="text-uppercase"> System Layout</h2>
		<p class="lead">There are two basic types of setups as far as where the 
		keg(s) are located, a walk-in cooler or a refrigerator, and how many you have. 
		We will deal with both types of installations.</p>
		<h3 class="text-uppercase">Refrigerator</h3>
		<p>If you are installing this system in a refrigerator it is 
		often not necessary to put the Hub Controller into the refrigerator, but rather 
		next to it. This all depends on the insulation characteristics of the refrigerator and if the signal from 
		the coupler can reach the hub through the refrigerator wall. You can power up 
		the system with the hub outside of the refrigerator to see if an exterior location will work. If 
		not the installation is very similar to the setup for a walk-in.</p>

		<h3  class="text-uppercase">Walk-in Installation</h3>
		<p>There are several things to consider before you start. How 
		many couplers will you be installing, antenna location and providing power to 
		the unit. We will be looking at a normal 4 keg installation. If you can get power from within the 
		walk-in cooler that is best.The first photo below FIG 1 is the simple 
		installation of 4 kegs in a typical walk-in. The power for 
		this system comes from a hole under the Hub Controller 
		&quot;HUB&quot;. You will see the purple wire that runs the power to the Power Distribution 
		Board &quot;PDB&quot;. From here the power goes 
		to the kegs and back to the HUB. Also note that in most 
		installations the beer line and CO<sub>2</sub> lines are tied together with zip ties. When 
		you install the KegData system you will attach the power wire to the CO<sub>2</sub> line 
		with zip ties.</p>
		<div class="col-md-12 text-center">
			<img  src="/mainart/parts/setup1.jpg" class="img-responsive">
			<p class="text-center"><span class="text-uppercase">Fig 1</span></p>
			<p class="text-center">Installation of 4 Kegs in typical walk-in</p>
		</div>

		<p >Now in figure 2 below you will see that the 
		power wire will go in and the antenna wire out. If you do not want to drill a 
		hole in your walk-in you can see FIG 3 below.</p>
		<div class="col-md-12 text-center">
			<img src="/mainart/parts/setup4.jpg" class="img-responsive">
			<p class="text-center"><span class="text-uppercase">Fig 2</span></p>
			<p class="text-center">Power Wire and Antenna Wire</p>
		</div>
		<p>In the photo below the power wire goes in and the antenna 
		wire comes out through the hole already in the ceiling where the beer lines go 
		out. </p>
		<p>The HUB and the PDB are next to each other in the walk in. 
		The antenna can lay on top of the roof or attached to a wall of structure to get 
		the best reception.</p>
		<div class="col-md-12 text-center">
			<img src="/mainart/parts/setup2.jpg" class="img-responsive">
			<p class="text-center"><span class="text-uppercase">Fig 3</span></p>
			<p class="text-center">Power Wire and Antenna Wire</p>
		</div>
		<h3 class="text-uppercase">The Power Distribution System</h3>
		<p>There are three items that can be used to 
		provide power to  the HUB and the Couplers. There is the 3 
		way splitter, the 8 port power distribution board &quot;PDB&quot;&nbsp; and the 
		24 port PDB. Depending on the number of kegs, you can chose the appropriate part 
		or combination of parts to keep your wiring 
		organized. All three of the power distribution items can be &quot;daisy chained&quot;. In 
		other words if you had 4 kegs on one wall and 8 on the other, you would use two 8 port 
		PDBs as shown in FIG 4 below. You would also need to order the power jumper wire 
	  shown in yellow.</p>
		<div class="col-md-12 text-center">
			<img src="/mainart/parts/Assembly3.jpg" class="img-responsive center-block" >
			<p class="text-center"><span class="text-uppercase">Fig 4</span></p>
			<p class="text-center">Two 8-port PDBs</p>
		</div>
		<p>Now you should know what you need as far as 
		the power distribution system is concerned. Remember that the green power wires to each of the couplers will need to be zip 
		tied to the CO<sub>2</sub> lines. You can see an installation below in FIG 5, this makes 
		for a nice clean installation. </p>
		<p>You will need to estimate the length of the 
		wires shown in green, FIG 4. The standards are 12' and 18' lengths. There is a 
		3' wire to go from the PDB to the HUB. If you want a longer wire you 
		will need to select the proper one for your application.</p>
		<div class="col-md-12 text-center">
			<img src="/mainart/parts/tonykegs.jpg" class="img-responsive">
			<p class="text-center"><span class="text-uppercase">Fig 5</span></p>
			<p class="text-center">Installation at a restaurant</p>
		</div>
		<p>Figure 5 above shows an installation at a 
		restaurant. You can see how clean the installation is and how the wires are 
		attached to the CO<sub>2</sub> lines.</p>
		<a href="#top" class="pull-right">Return to top<span class="glyphicon glyphicon-arrow-up"></span></a>
	</section>
	<hr />
	<section class="clearfix">
		<h2 class="text-uppercase" id="system">PUTTING IN THE SYSTEM</h2>
		<p>We have also included an installation video  to help you better understand the installation.</p>
		<ul class="list-unstyled">
		  <li><strong>Step 1</strong>
				<p>The first step will be to shut off the  CO<sub>2</sub>. </p>
				<p>You can also use the two needle nose vice grips (optional tools) to squeeze off 
					the  CO<sub>2</sub> line and beer line. </p>
			</li>
			<li><strong>Step 2</strong>
			 	<p>Uncoupler the keg and take the old keg 
				coupler off of the keg.</p>
			</li>
			<li><strong>Step 3</strong>
				<p>Remove the beer line and  CO<sub>2</sub> line from the 
				old coupler and set it to the side. In your kit you will have a new  CO<sub>2</sub> check 
				valve and a washer for the top beer line out. </p>
				<div class="row">
					<div class="col-md-2">
						<img border="0" src="/mainart/parts/KDM-01-1032.jpg" class="img-responsive">
					</div>
					<div class="col-md-2">
						<img border="0" src="/mainart/parts/KDM-01-1010.jpg" class="img-responsive">
					</div>
					<p class="text-danger"><strong class="text-uppercase">DO NOT FORGET TO PUT THESE IN!!</strong><br />
					You can also use the old ones if they are in good condition</p>
				</div>
			</li>
			<li><strong>Step 4</strong>
				<p>Reattach up the beer line out to the top of 
				the coupler and the  CO<sub>2</sub> line in. Make sure the washer seats properly. Repeat 
				this until all couplers are replaced.</p>
			</li>
			<li><strong>Step 5</strong>
				<p>Put the new KegData couplers back on the kegs 
				and push the handle(s) down to retap the kegs. You are now done with the coupler 
				section.</p>
			</li>
			<li><strong>Step 6</strong>
				<p>To mount the Hub Controller select the spot to put your HUB from the 
				guidance above. Drill two holes in the wall, 1/16&quot; in diameter and 
			  3&quot; apart, one directly below the other. </p>
				<p>Put in the two screws provided leaving them 
				sticking out about 1/8&quot;. You can adjust this distance after you test fit the 
				HUB.</p>
				<p>Mount the hub controller to the wall by 
				sliding down over the two screws. </p>
			</li>
			<li><strong>Step 7</strong>
				<p>Now we need to mount your PDB. It needs to be 
				within 3 feet of the HUB if you want to use the factory 3' wire. </p>
				<p>If you are using the 3 way splitter there is no mounting for this item.</p>
				<p> If you are using the 8 PDB just mount with the two 
				screws provided through the tabs diagonally. The other holes are provided in case 
				the board is damaged and a tab is broken off.</p>
				<p>On the&nbsp; 24 port boards, drill two holes 
				in the wall, 1/16&quot; in diameter and 6&quot; apart, one directly below the other.&nbsp; 
				Put in the two screws provided leaving them sticking out about 1/8&quot;. </p>
				<p>You can adjust this distance after you test 
				fit the 24 port PDB.</p>
			</li>
			<li><strong>Step 8</strong>
				<p>If you are using more that one power 
				distribution device you will now need to run a power wire between the two or 
				more devices. These are optional, be sure you have ordered these.</p>
				<p>These power wires have a bigger end than the 
				wires that go to the couplers so they are not interchangeable. </p>
			</li>
			<li><strong>Step 9</strong>
				<p>Now run the wires from the couplers to the 
				PDBs or splitters, use the straight end at the coupler. </p>
				<p>Use the zip ties to 
				attach the power with to the  CO<sub>2</sub> lines. Bunch extra cable with a zip tie.</p>
			</li>
			<li><strong>Step 10</strong>
				<p>If you have not done so yet, run the power 
				wire form the PDB or splitter to the HUB.</p>
			</li>
			<li><strong>Step 11</strong>
				<p>You should have already decided where you are 
				bringing in the power, from a wall or through the roof, or from within the 
				walk-in if it is available. Run the power wire to the PDB or splitter. 
				</p>
			</li>
			<li><strong>Step 12</strong>
				<p>Now you will run the antenna out through a 
				selected hole in the walk-in. This antenna must be able to reach a Wifi signal. 
				If it cannot reach a signal it will not be able to send data. </p>
				<p>Now attach the antenna to the HUB. This completes 
				the installation of your system.</p>
			</li>
		</ul>
		<a href="#top" class="pull-right">Return to top<span class="glyphicon glyphicon-arrow-up"></span></a>
	</section>
	<hr />
	<section class="clearfix">
	<h2 id="hub" class="text-uppercase">Setting up your Hub, WPS or Manual</h2>
		<ul class="list-unstyled">
		
		
				<p>There are two ways to connect your hub to the internet, manually or WPS. WPS is an automatic system
				that is a function of most new routers. If there is a WPS button on your router you would press that button to
				allow it to connect to WPS configured devices or go into the config pages in your router to press the button from your
				router software. Please follow the steps below for the type of connection you want to use. </p>
	<h3 id="hub" class="text-uppercase">Using WPS</h3>

		<ul class="list-unstyled">
				<p>Quick Directions that work for most wireless access points equipped with WPS.</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/kr2fiY3aB80" frameborder="0" allowfullscreen></iframe>				

				<li><strong>Step 1</strong><p>Push the WPS button on your wireless access point or router (you may need to push and hold the button for a few seconds).</p>
				<li><strong>Step 2</strong><p>Now, waiting no more than a minute, plug in power to your hub making sure the antenna is attached. 
				Push and hold the red internet button on the Kegdata hub for 10-15 seconds, then release. You should see a solid yellow light come on, light B on hub., after a few seconds.
				It will soon go off once the device pairs with your router.</p> 
				<li><strong>Step 3</strong><p>If successful, within a couple seconds the Kegdata hub internet bank (A-C) of leds on hub should have a slow blinking green led on A and the red led off.</p>
 				<li><strong>Step 4</strong><p>If this is unsucessful unlug the hub and start over from Step 1. </p>
				<li><strong>NOTES ABOUT WPS:</strong></p>
				<li><strong>Note 1</strong><p>There are 2 types, PIN and push button.  Kegdata only works with push button.  PIN mode WPS is not secure and we recommend you have it disabled on your access point.</p>
				<li><strong>Note 2</strong><p>Your wireless access point may come from the factory with WPS function turned off.  If this is the case you would need to turn it on from within its configuration pages.</p>
				<li><strong>Note 3</strong><p>Your wireless access point may have a soft button.  Meaning that it is not a physical button, but rather a button you press when logged into its configuration page.</p>
				<li><strong>Note 4</strong><p>If you wait too long after pushing the wireless access point button before pushing the Kegdata button, your wireless access point will have timed out the WPS and you will need to start the process over.</p>
				<li><strong>Note 5</strong><p>If all else fails, you may need to read the directions for your wireless access point!</p>
				
		<h3 id="hub" class="text-uppercase">Manual Setup</h3>				
				<li><strong>Step 1</strong><p>Your system should now be installed. You can 
				now power it up. You will see a series of lights start flashing on the hub and 
				also on the couplers, this is normal and we will talk about what these lights 
				mean in a minute.</p>
			</li>
		
				<img border="0" src="/mainart/parts/KDE-02-1000.jpg" width="292" height="273">
					<li><strong>Step 2</strong><p>Your HUB will have lights that will start 
				blinking. The right column of lights D, E &amp; F deal with the keg communications 
				and left lights A, B &amp; C deal with the communications to the internet, we will deal with the 
				lights in more detail later. You will now push in the Internet setup red button for 3-4 seconds. After you
			  release it after a few seconds  lights A &amp; C start blinking a Red and Green alternately. This may take up to 20 seconds. You can see the video below of the HUB in pairing mode.</p>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/jFQ_OO3TTvk" frameborder="0" allowfullscreen></iframe>
				<p>&nbsp;</p>
			</li>
			<li><strong>Step 3</strong>
				<p>Now you will go to your laptop or smartphone 
				to finish the set up. We will start with a smart phone.</p> 
				<p>Go to your Wifi 
				connections and you should see KegData, if not scan again. If you still do not 
				see it go back and check your HUB to be sure A &amp; C lights are blinking Green and 
				Red back and forth. If not go back to Step 2. If there is a password for KegData 
				it will be KegData123. You should be able to connect. REMEMBER that your KegData 
				antenna must not only be able to access your phone or laptop but it must also be 
				able to access your Wifi network. If you are still having problems here 
				disconnect the power to your hub and repeat step 2. If you do not see the 
				KegData on the network you may not be in a place where you cannot contact the 
				antenna. You might want to move positions.
				Note: After you connect to the hub through your smart phone or laptop, the yellow light B on your hub will start to blink slowly. </p>
				<img border="0" src="/mainart/Screenshot_2015-02-23-16-32-06.png" width="336" height="592">
				<p>Now that you are connected to the HUB light B 
				will be blinking yellow showing you are connected to the HUB.</p>
			</li>
			<li><strong>Step 4</strong>
				<p>Now that you are connected to the hub you 
				will now login to the setup screen. It looks like the photo below. On a 
				Smartphone you will go to your internet browser and type a know web site like www.google.com.  Because you are not
				connected to the internet it will default to the configuration page on the hub. On a laptop you would enter IP address 192.168.1.254. You can also try 
				typing config in the browser bar, or it that fails, try www.google.com in your browser bar on your laptop. If you are on your laptop you 
				can sometimes just close your browser and reopen and this page will show up. If 
				you are using multiple tabs this may show up on one of the other tabs.</p>
				<p>When you look at the image below you will see 
				the Refresh List. Click this and the hub will look for your Wifi network at your 
				business of home. If it does not find it that means that your antenna can see 
				your phone but not your Wifi LAN network. You would need to relocate your 
				antenna. </p>
				<img border="0" src="/mainart/Screenshot_2015-02-23-16-33-59.png" width="532" height="946">
				<p>Now you will see the image below Enter your 
				password and hit Save &amp; Reboot button. </p>
				<img border="0" src="/mainart/Screenshot_2015-02-23-16-37-54.png" width="528" height="932">
			</li>
			<li><strong>Step 5</strong>
				<p>Your system should now be connected to your 
				network. You should have a slow blinking green light A on the HUB. You may also 
				get some sporadic yellow lights blinking on HUB light B. This is normal during 
				startup. You should also see a blue light on HUB light E and the Coupler light 
				A. These show that they are talking to each other. This can sometimes take a few minutes to light up. The slow blinking light is shown below.</p>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/9l07uQqjw1g" frameborder="0" allowfullscreen></iframe>
			</li>
			<li><strong>Step 6</strong>
				<p>Unplug the system for a reboot. Wait 15 
				seconds and plug the system back in. Time for a beer! The couplers start working 
				when they contact beer so you will need to a little beer from each coupler one 
				time. When you tap a new keg it will may not register the replacement until you 
				open the tap for a few seconds. That depends on how much beer is in the line.</p>
			</li>
			<li><strong>Step 7</strong>
				<p>When you pour a beer you will see the yellow 
				light B on the HUB blink briefly. This tells you that the HUB just sent 
				information to the server. If ou do not see the yellow light come on after the pour try rebooting by unplugging the system for 15 seconds and try again.</p>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/y737UGA1zdQ" frameborder="0" allowfullscreen></iframe>
			</li>
			<li><strong>Step 8</strong>
				<p>Now it is time to set up your kegs on the KegData server. If you have not registered please do so first. The HUB controller will have to be registered to you so if you have not registered already you may have to contact KegData to tell them you HUB mac address located on the back of the HUB. When you 
				login into the website you will immediately go to your &quot;Dashboard&quot;. You will see 
				your keg levels for all couplers that are transmitting. On each coupler there is 
				a 6 character string, this identifies that coupler. If you have a lot of kegs it 
				will save time to write down this identification number and the size of keg and contents for 
				each of the couplers. Go to keg setup on your dashboard and set up each of your 
				kegs. If you want to use the auto reorder portion, you will need to see if 
				your distributor is registered. If not, ask them to register so they can see 
				your keg levels.</p>
			</li>
		</ul>
		<a href="#top" class="pull-right">Return to top<span class="glyphicon glyphicon-arrow-up"></span></a>
	</section>
	<hr />
	<section class="clearfix">
		<h2 class="text-uppercase" id="testing">Testing your System</h2>
		<p>Your system should be working now. Every time 
		you pour a beer you will see light B on your HUB blink yellow for a a brief 
		moment and this will show up on your web page.&nbsp; You will also see the third green light, light C, on the coupler light up when your tap handle is opened, see the video below. Everything form here is 
		pretty much on auto.</p>
		<p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/AHnP8xxMPEw" frameborder="0" allowfullscreen></iframe>&nbsp;</p>
		<a href="#top" class="pull-right">Return to top<span class="glyphicon glyphicon-arrow-up"></span></a>
	</section>
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




