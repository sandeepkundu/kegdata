
@section('title')
	News for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10">
	<h1 class="title">News</h1>
	<p class="lead">
		{{ Config::get('constants.COMPANY_NAME') }} is a finalist in  MEMS Industry Group's (MIG)'s MEMS & Sensors Technology Showcase.
	</p>
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




