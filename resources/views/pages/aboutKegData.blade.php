
@section('title')
	About {{ Config::get('constants.COMPANY_NAME') }}, an autonomous device for measuring beer
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10">
	<h1 class="title">How does {{ Config::get('constants.COMPANY_NAME') }} Work?</h1>
	<p class="lead">{{ Config::get('constants.COMPANY_NAME') }} has been working behind the scenes to create the most advanced and
	 cost effective way to measure the amount of beer in a keg and what was dispensed during
	 any period of time. The key to {{ Config::get('constants.COMPANY_NAME') }} is the patented keg coupler which knows how much 
	 beer is in the keg the second you hook it up. It runs from advanced electronics, microprocessors 
	 and sensors that can accurately know what is in any keg at any time.</p>

	<p>A {{ Config::get('constants.COMPANY_NAME') }} system is comprised of a power supply, keg couplers, and a main control board.  
	Our couplers also use Miwi to connect to the main control board which collects the data and 
	then sends that information to our main server for recording.
	</p>

 	<p>To install the system you simply replace your existing keg couplers with the {{ Config::get('constants.COMPANY_NAME') }} couplers, 
 	mount the controller and hook up power to the couplers. Most systems can be set up in less than
 	an hour, depending on how many couplers your business requires.</p>
    
    <p>{{ Config::get('constants.COMPANY_NAME') }} does not use flow meters or scales as we have found early on that they are prone 
    to failure, are inaccurate or just cost too much.</p>
 
    <p>{{ Config::get('constants.COMPANY_NAME') }} is also the first company to make the auto coupler release standard on all couplers.
    This prevents the beer line from filling with foam and costing time and money to refill the lines.</p>
 
    <p>To learn more you can go to <a href="/faq">FAQ</a> for more detailed information.</p>
 
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




