
@section('title')
	Support for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
	<nav class="col-md-2 left-nav">
		<h4>Navigate to Section</h4>
		<ul class="nav nav-pills nav-stacked">
			<li><a href="#basic">The Basic System</a></li>
			<li><a href="#troubleshooting">Troubleshooting</a></li>
			<li><a href="#indicator">Indicator Lights</a></li>
			<li><a href="#tutorials">Videos and Tutorials</a></li>
	</nav>
	<article class="col-md-8">
	<h1 class="title">{{ Config::get('constants.COMPANY_NAME') }} Support</h1>
	<p class="lead">Welcome to the support section for {{ Config::get('constants.COMPANY_NAME') }}. There are important 
	things for you to know about our goals and what we hope to achieve in 
	customer support. Firstly you, the customer, is the most important part of our 
	business and we will do everything in our power to help you with any issues and to 
	try to improve our product from customer comments and feedback. If you would like 
	to suggest something for the website, the products or make a comment please do so at 
	<a href="mailto:sales@kegdata.com">support@kegdata.com</a>. Tell us the good, the bad and 
	the ugly, we want to know what you 
	think and make our products better!</p>

	<p>One of our principal goals is to make our support pages and videos so comprehensive that 
	they will answer all of your questions quickly and  easily. We have included several indicator 
	lights to help you with many of the most common problems expected. By looking at the lights 
	you can reference them below to find out why your system may not be working. As you may know, 
	paying for a team of customer support specialists is expensive and ultimately guess who gets 
	to pay for it? The customer, of course. In order to keep your costs down we ask that all customers
	 go through the checklist and videos below to help you solve your problems before contacting our office. 
	 Thank you for your cooperation.
	</p>

	<section>
	<h2 id="basic">The Basic System</h2>
 	<p>The system is comprised of 4 components. Each one of these components must be 
 	working in order for the system, 
 	to operate. Our system is wireless and requires a wireless access point.
 	For general support questions you can email <a href="support@kegdata.com">support@kegdata.com</a>.</p>
    </section>
    <section id="troubleshooting">
    <h2>Troubleshooting</h2>
    <p>We have included information below to find out the problem with your system. Before trouble shooting anything, <strong>unplug the power supply</strong> from a receptical, wait 15 seconds and then plug the power supply back in.</p> 
    <nav>
    	<ol>
    		<li><strong>Unplug the power supply and wait 15 seconds and plug back in!</strong> - Before you test anything, if you have a problem or after a new install, reboot your system by unplugging and plugging pack in your power supply. If you are installing a new system go to the <a href="install.blade.php">install tab</a> to see what you need and how to install.</li>
    		<li><strong>No light on the power supply</strong>. Check your plug and be sure the wire coming out of the power supply &quot;brick&quot; is inserted all of the way in. Try plugging something else into the receptical to be sure the receptical is working. The green light should be illuminated on the &quot;brick&quot; when it has power. If the light will not come on and the receipical is good you have a bad power supply.</li>
    		<li><strong>I have a splitter not a power distribution hub.</strong> A splitter will power two couplers and the HUB. The splitter also has a &quot;main power wire&quot; female jack so that you can hook up another splitter of power distribnution board.</li>
    		<li><strong>I have a power distribution hub and there is no light on it.</strong> This means it has no power. You should plug in the main power supply directly into it or a main power wire from another power distribution board or from a splitter.</li>
    		<li><strong>I have no power indicator light on the control board.</strong> This means that it has no power and will not work. Check your connections and switch wires from a coupler power wire (same wire type) to see if it will light up. If you are sure it has power and will not light up, you will need to contact <a href="support@kegdata.com">support@kegdata.com</a>. </li>
    		<li><strong>I have no power indicator light on the keg coupler handle.</strong>This means that it has no power and will not work. Check your connections and switch wires from another coupler power wire (same wire type) to see if it will light up. If you are sure it has power and will not light up, you will need to contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>If all of  the lights are on for all of the foregoing items continue to next step 8.</strong></li>
			<li><strong>I hooked up the system but cannot log into the system to set it up to my wireless network.</strong> There are several possibilities why your system will not connect to a wireless network. a. If your wireless network has an verification page when you log into your network from your phone then you will not be able to log our system into that wireless network. A verification page is a second step that requires user intervention. In this case you need to create a new wireless network, ususlly by purchasing a new wireless router or access point. b. You cannot see the KegData wireless HUB on your phone or laptop. Be sure the lights on the hub are blinking green and red as shown below. If you still cannot see the network on your laptop or smartphone then try to relocate your antenna. Remember that the antenna from the HUB must be able to contact your wireless router or access point. Go to <a href="install.blade.php">install tab</a> if you are still having problems with connecting to your wireless network.</li>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/jFQ_OO3TTvk" frameborder="0" allowfullscreen></iframe><p></p>
			<li><strong>I have logged into the system but none of the couplers are  showing up on the web page. </strong>Unplug the couplers, wait 15 seconds and plug them back in. Be sure that the blue lights are illuminated on the couplers and that light B is green. As soon as you pour a beer you should see light C turn green on the coupler when the beer is pouring. As soon as you stop pouring you will see the yellow light illuminate on the HUB, see video below. If you still do not see your couplers show up on the internet please contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/y737UGA1zdQ" frameborder="0" allowfullscreen></iframe><p></p>
			<li><strong>I have logged into the system and only a few of the couplers are showing up on the web page.</strong> Unplug the couplers that are not transmitting and plug them back in. You should have a blue light come on the couplers when they are connected to the Hub. Sometimes it may take a few minutes for the couplers to &quot;pair&quot; to the HUB and the blue light to come on. Try rebooting the whole system to see if that works. If the blue light is on pour some beer. See the transmit video above to see if transmitting. If your coupler will still not show up on your account please contact support. <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>I have added new or replaced couplers and they do not show up on the web page.</strong> Be sure the lights on the HUB are on and it is connected to your wireless network. Please see #10 above.</li>
			<li><strong>I have the system hooked up and the beer levels are not showing up.</strong> Check #8 and #9 above to be sure your hub is connected and transmitting. It is likely that your HUB is not connected to the internet. Go to the <a href="install.blade.php">install tab</a> and verify that your system is hooked up to the internet and transmitting.</li>
			<li><strong>I have the system hooked up and the level is wrong.</strong> This is very unlikely but there may be some type of calibration issue we are not aware of. Our system is designed to self calibrate. Please contact support about any issues here <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>The keg coupler is untapping when the keg is not empty.</strong> The system is designed so that it will untap as soon as the pickup tube inside of the keg senses that it is pickuping up air. This may be a calibration issue that will auto correct in time. You can unplug the power to that coupler to allow the rest of the keg to be drawn. If this issue persists please contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>The keg coupler will not untap when it is empty.</strong> If this issue persists please contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>I am having trouble on the web page with my account.</strong> If this issue persists please contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>I am having trouble on the web page entering my kegs.</strong> If this issue persists please contact <a href="support@kegdata.com">support@kegdata.com</a></li>
			<li><strong>I do not know which keg is which on the web site. </strong>All of the KegData couplers have an identification number on the side. When you log into the webside you will automatically be redirected to the KegData dashboard. Click on the Keg Setup tab. There on the upper left side you will see the numbers of your couplers. Select the one you want and then you can edit the settings for this keg on the right side of the Keg Setup page. Be sure you write down the contects of each keg and the associated coupler number. </li>
			<li><strong>The system is going haywire, what do I do?</strong> You can just unplug the system and the couplers will work just like any normal coupler. <a href="support@kegdata.com">support@kegdata.com</a></li>
    	</ol>
    </nav>
    </section>
    <section id="indicator">
	    <h2>Indicator Lights</h2>
	    <p>
	    	<ol>
	    		<li>The power supply</li>
				<li>The power distribution hub (where used)</li>
				<li>The handle on the keg coupler</li>
				<li>The sensor on the keg coupler</li>
	    	</ol>
	    </p>
    </section>
    <section id="tutorials">
   	  <h2>Videos & Tutorials</h2>
    	<ol>
			<li>Setting up the system.<p></p>
			  <iframe width="560" height="315" src="https://www.youtube.com/embed/EHTH35TQvAk" frameborder="0" allowfullscreen></iframe>
			</li>
			<li>Using our website, www.kegdata.com
				<ol>
					<li>User Preferences </li>  
					<li>Billing   </li>  
					<li>Board Setup</li>     
					<li>Keg Setup </li>    
					<li>Reports </li>
				</ol>
			</li>
			<li>I am a Distributor</li>
			<li>I am a Brewery</li>
    	</ol>
    </section>
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




