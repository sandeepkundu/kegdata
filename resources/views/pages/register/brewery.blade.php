	<p class="lead">The key for {{ Config::get('constants.COMPANY_NAME') }} for the breweries is the reporting, but also 
	the auto reorder system that {{ Config::get('constants.COMPANY_NAME') }} provides. This will help restaurants and 
	bars to keep your products in stock and with this new technology should promote more keg sales.</p>

	<p>With {{ Config::get('constants.COMPANY_NAME') }} breweries can access any data nationwide for their brands at any time. 
	Lets say a Brewery ran a promotion on the 4th of July weekend in Austin Texas.  
	They could find out how much beer they sold during the promotion.
	</p>
	<p>
	As a Brewery you will receive free access to all reports for any restaurant
	 or bar that is using our system, who sells your brands. You will 
	 be able to generate reports by any number of filters including by brand,
	  state, county, city or zip code. You can also run the whole United States 
	  or any country in the world where our system is used. You can also filter 
	  by years, months or days. You can also search custom dates. Again this is all 
	  free but depends on restaurants that are on the system.
	</p>
 
 	<p>
	A Brewery will also have access to all restaurant chain or Enterprise 
	reports by searching with their respective Enterprise number. Of 
	course you will only be able to see your brands.
	</p>

	<p>
	By registering you will be able to start seeing restaurants that are on the 
	system and can start generating reports. We strongly recommend that you notify 
	your distributors to help get their customers on the {{ Config::get('constants.COMPANY_NAME') }} system to help 
	your reporting and automate reordering of your products.
	</p>
