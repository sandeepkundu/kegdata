	<p class="lead">Keg Data&trade; offers a home system with a single keg sensor. This is perfect 
	for the home user that wants to know how much beer they have left in their keg and what 
	temperature it is. This is the problem that created Kegdata in the first place, unexpectedly 
	running out of beer.</p>

	<p>We will have an app for your iphone and android, so before you invite all of your friends 
	over for a game and a few beers be sure you have enough beer to keep them happy.
	</p>
