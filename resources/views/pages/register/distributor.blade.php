
		<p class="lead"> One important feature for Distributors is that {{ Config::get('constants.COMPANY_NAME') }} will generate emails 
		or texts to the distributor from the restaurant or bar when a keg is at a reorder point, 
		set by the restaurant or bar. This will save hundreds of thousands in sending pre-sales 
		personnel to check keg levels. It will basically automate ordering for any restaurant or
		bar on our system.</p>

		<p>As an example, a distributor in Houston has 5 route salesmen. They make $80k per year, 
		plus the truck maintenance, fuel, health insurance, payroll taxes, worker's comp, 401ks 
		and so on. Our auto-order system will eliminate the need to have route sales people go to 
		the restaurants to check their inventory.</p>

		<p> Our unique system will allow the distributor to go as far as 
		scheduling the deliveries in loading order for the trucks.</p>
		<p>
			<strong>The services provided to you are as follows:</strong>
			<ol> 
				<li>Free access to all data for all restaurants that are on our system for your brands.</li>
				<li>Providing reporting for any period of time for all restaurants on the system.</li>
				<li>Provide auto reordering at beer levels set by the user via email or text to distributor.</li>
				<li>Allow endless kegs to be added to any restaurant or bar.</li>
				<li>Generate reports on how many kegs need to be delivered by any number of filters.</li>
				<li>Allow the Distributor to actually set the delivery route schedule so truck loading is managed.</li>
				<li>Encourages keg sales and should create new demand for the sales of keg beer.</li>
			</ol>
		</p>
		<p>
			The key to the system for the Distributor is to get all of the restaurants or bars 
			they serve to use the system. The Distributor will obviously not be able to see anybody 
			not on our system. The system is inexpensive and easy to install. We strongly suggest 
			that your sales staff encourage all of your customers to start by registering on our web site. 
			Registration is free.
		</p>

