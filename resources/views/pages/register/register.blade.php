
@section('title')
	Register with {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
	<ul class="nav nav-tabs nav-justified tabs" id="register-tabs" role="tablist">
	  	<li class="{{$tab == 'residential' ? 'active' : '' }}"><a href="#residential">Residential User</a></li>
	  	<li class="{{$tab == 'bar' ? 'active' : '' }}"><a href="#bar">Restaurant or Bar</a></li>
	  	<li class="{{$tab == 'distributor' ? 'active' : '' }}"><a href="#distributor">Beer Distributor</a></li>
		<li class="{{$tab == 'brewery' ? 'active' : '' }}"><a href="#brewery">Brewery</a></li>
		<li class="{{$tab == 'enterprise' ? 'active' : '' }}"><a href="#enterprise">Chains or Enterprise</a></li>
  	</ul>
  	<div class="container">
  	<div class="col-md-4 col-md-push-8 headroom-top" style="inline-block">
		@include('components.registerSidePanel', array('Account_Types' => $Account_Types))
	</div>
  	<div class="tab-content col-md-8 col-md-pull-4" style="inline-block">
	  	<div class="tab-pane{{$tab == 'residential' ? ' active' : '' }}" id="residential">
	  			@include('pages.register.residential')
	  	</div>
	  	<div class="tab-pane{{$tab == 'bar' ? ' active' : '' }}" id="bar">
	  			@include('pages.register.bar')
	  	</div>
	  	<div class="tab-pane{{$tab == 'distributor'  ? ' active' : '' }}" id="distributor">
	  			@include('pages.register.distributor')
	  	</div>
		<div class=" tab-pane{{$tab == 'brewery' ? ' active' : '' }}" id="brewery">
	  			@include('pages.register.brewery')
	  	</div>
		<div class="tab-pane{{$tab == 'enterprise' ? ' active' : '' }}" id="enterprise">
		   		@include('pages.register.enterprise')
	    </div>
  	</div>
  	</div>

@stop






