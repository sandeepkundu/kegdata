
	<p class="lead">This category is for restaurant or bar chain management to look over one or all of 
	their restaurants in single or multiple reports. Each of the restaurants will have an 
	identification code to allow the Enterprise management to view all restaurants or bars
	 with that identification number. </p>
	<p>
	Each restaurant on the system will have the following capacity:<br />
	<ol>
		<li>Providing reporting for any period of time</li>
		<li>Charts and graphs to track details of pours and levels</li>
		<li>Enable high temperature warnings by text or phone</li>
		<li>Provide auto reordering at beer levels set by the user</li>
		<li>Auto untapping when the keg is empty to prevent foam in beer lines</li>
		<li>Allow endless kegs to be added to our system</li>
		<li>Email user of low levels and empty</li>
		<li>Check register sales against beer usage</li>
		<li>Stacking kegs or setting items on kegs okay</li>
		<li>Wireless data transmission</li>
		<li>Easy installation</li>
		<li>Phone apps coming soon</li>
	</ol></p>
 	<p>As a Enterprise customer  you will receive free access to all reports for any restaurant
 	 or bar on your Enterprise number. You will be able to generate reports by any number of 
 	 filters including by brand, state, county, city or zip code. You can also run the whole 
 	 United States or any country in the world where our system is used and you have stores. 
 	 You can also filter by years, months or days. You can also search custom dates. Again this is 
 	 all free but depends on restaurants that are on the system.</p>
 
    <p>By registering you will be able to start seeing restaurants that are on the system and 
    can start generating reports. We strongly recommend that you notify all of your restaurants 
    to register. This is by far the most advanced keg beer inventory system available today!</p>
