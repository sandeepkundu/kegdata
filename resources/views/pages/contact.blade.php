
@section('title')
	Contact Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-10">
	<h1 class="title">Contact Us</h1>
	<p class="lead">{{ Config::get('constants.COMPANY_NAME') }} is a Texas LLC and based in Austin, Texas with additional offices in Houston Texas.</p>
	<section class="text-center">
		<h4>{{ Config::get('constants.COMPANY_NAME') }} can be contacted at:</h4>
		<address>
			<strong>Ikeg, LLC</strong><br />
			PO Box 90102<br />
			Houston, Texas 77290
		</address>
		<address>
		<img src="mainart/phone1.jpg" width="227" height="45" alt="844-KEG-DATA" />
		</address>
		<address>
		<strong>Ikeg, LLC, dba KegData</strong><br />
			Austin, Texas   
		</address>
	</section>
	<p align="center">
	Sales <a href="mailto:sales@kegdata.com"><u>sales@kegdata.com</u></a></p>
	<p align="center">Support <a href="../layouts/support@kegdata.com"><u>support@kegdata.com</u></a></p>
	<p align="center">Website issues <a href="../layouts/support@kegdata.com"><u>support@kegdata.com</u></a></p>
	<p align="center">&nbsp;</p>
	<p align="center">&nbsp;</p>
	<p align="center">&nbsp;</p>
	<p align="center">&nbsp;</p>
	<p align="center">&nbsp;</p>
</article>
@stop

@section('sidebar')
	@include('components.sideAd')
@stop




