@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<article class="col-md-12">

	<p>
	The monthly subscription price for KegData monitoring is only $9.95 for residential customers and 34.95/mo for commercial accounts, with 2 or more kegs!</p>
</div>
<div class="alert alert-danger text-capitalize">
  <div align="center"><strong>PLEASE ALLOW 4-6 WEEKS FOR DELIVERY!</strong></div>
</div>
<div class="center-block"><img border="0" src="/mainart/phone1.jpg" width="227" height="45"></div>
<h3>KD-1000-D Basic USA System</h3>
<div class="row">
	<div class="col-md-3">
		<p>Contains: <br />
		<ul class="list-unstyled">
			<li>Power Supply</li>
			<li>Hub Controller</li>
			<li>Antenna</li>
			<li>3 way splitter</li>
			<li>one 12' coupler power wire</li>
			<li>one 3' hub power wire</li>
			<li>one "D" style coupler</li>
		</ul>
		</p>
		<p class="text-danger">Difference between USA and Euro is the Euro has the European coupler for beers like Heineken. This is standard US Sankey</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/homekit1_tn.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
	  <p class="lead"><span class="text-danger"></span>$349</p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
		  <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="Complete one US Sankey coupler kit">
			<input type="hidden" name="item_number" value="KD-1000-D">
			<input type="hidden" name="amount" value="349.00">
			<input type="hidden" name="no_note" value="1"> 
			<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form> 
	</div>		
</div>
<hr />
<h3>KD-1000-S Basic Euro System</h3>
<div class="row">
	<div class="col-md-3">
		<p>Contains: <br />
		<ul class="list-unstyled">
			<li>Power Supply</li>
			<li>Hub Controller</li>
			<li>Antenna</li>
			<li>3 way splitter</li>
			<li>one 12' coupler power wire</li>
			<li>one 3' hub power wire</li>
			<li>one "S" style coupler</li>
		</ul>
		</p>
		<p class="text-danger">The difference between USA and Euro coupler is the Euro has the European coupler for beers like Heineken. This is standard Euro Type &quot;S&quot; Sankey</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/homekit1_tn.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead"><span class="text-danger">$359</span></p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			 <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="Complete one EURO Sankey coupler kit">
			<input type="hidden" name="item_number" value="KD-1000-S">
			<input type="hidden" name="amount" value="359.00">
			<input type="hidden" name="no_note" value="1"> 
			<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form> 	
	</div>		
</div>
<hr />
<h3>Additional Couplers</h3>
<div class="row">
	<div class="col-md-3">
		<h4> KD-1001-D</h4>
		<p class="lead">US KegData "D" Sankey</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KD-1000-D_tn.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead"><br />
		  <span class="text-danger">On Sale: $99.00</span></p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="One US Sankey coupler ">
			<input type="hidden" name="item_number" value="KD-1001-D">
			<input type="hidden" name="amount" value="99.00">
			<input type="hidden" name="no_note" value="1"> 
			 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form>  
	</div>		
</div>
<hr />
<div class="row">
	<div class="col-md-3">
		<h4> KD-1001-S</h4>
		<p class="lead">US KegData "S" Sankey</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KD-1000-D_tn.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead"><br />
    <span class="text-danger">On Sale: $109.00</span></p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="Complete one EURO Sankey coupler kit">
			<input type="hidden" name="item_number" value="KD-1000-S">
			<input type="hidden" name="amount" value="109.00">
			<input type="hidden" name="no_note" value="1"> 
			 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form> 
	</div>		
</div>
<hr />
<h3>8 Way Power Splitter</h3>
<div class="alert alert-danger">You will need one additional power wire KCD-01-1008  for each one of these you order!</div>
<div class="row">
	<div class="col-md-3">
		<h4> KDC-01-1010-8</h4>
		<p class="lead">This will allow you to hook up 8 additional couplers.</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KDC-01-1010-8.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead">$39.62</p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="8 Port Power Distribution Board">
			<input type="hidden" name="item_number" value="KDC-01-1010-8">
			<input type="hidden" name="amount" value="39.62">
			<input type="hidden" name="no_note" value="1"> 
			<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form>  
	</div>		
</div>
<hr />
<h3>24 Way Power Splitter</h3>
<div class="alert alert-danger">You will need one additional power wire KCD-01-1008  for each one of these you order!</div>
<div class="row">
	<div class="col-md-3">
		<h4> KDC-01-1010-24</h4>
		<p class="lead">This will allow you to hook up 24 additional couplers</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KDC-01-1010-24.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead">$73.40</p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="24 Port Power Distribution Board">
			<input type="hidden" name="item_number" value="KDC-01-1010-24">
			<input type="hidden" name="amount" value="73.40">
			<input type="hidden" name="no_note" value="1"> 
			<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form>
	</div>		
</div>
<hr />
<h3>3 Way Power Splitter</h3>
<div class="alert alert-danger">You will need one additional power wire KCD-01-1008  for each one of these you order!</div>
<div class="row">
	<div class="col-md-3">
		<h4>KDC-01-1011</h4>
		<p class="lead">This will allow you to hook up 3 additional couplers, or 2 couplers and the hub controller.</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KDC-01-1011.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<p class="lead">$16.60</p>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="add" value="1"> 
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="Three way power splitter">
			<input type="hidden" name="item_number" value="KDC-01-1011">
			<input type="hidden" name="amount" value="16.60">
			<input type="hidden" name="no_note" value="1"> 
			 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
		</form> 
	</div>		
</div>
<hr />
<h3>Main Power Wire</h3>
<div class="alert alert-danger">You will need one additional power wire for any of these you order <br  />
<ul class="list-unstyled">
<li><strong>KDC-01-1010-8</strong></li>
<li><strong>KDC-01-1008-24</strong></li>
<li><strong>KDC-01-1011</strong></li>
</ul>
</div>
<div class="row">
	<div class="col-md-3">
		<h4>KDC-01-1008</h4>
		<p class="lead">This connects power between a 3 way splitter, an 8 port Power Distribution board or a 24 port power distribution board.</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KDC-01-1008.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<table class="table table-bordered">
		<tr><th>Length</th><th>Price</th><th>Purchase</th></tr>
		<tr>
			<td><p class="lead">3'</p></td>
			<td><p class="lead">$8.95</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="add" value="1"> 
					<input type="hidden" name="lc" value="US">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="item_name" value="3 foot power distribution wire">
					<input type="hidden" name="item_number" value="KDC-01-1008-3">
					<input type="hidden" name="amount" value="8.95">
					<input type="hidden" name="no_note" value="1"> 
					<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>	
			</td>
		</tr>
		<tr>
			<td><p class="lead">6'</p></td>
			<td><p class="lead">$12.80</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					  <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					  <input type="hidden" name="add" value="1"> 
					 <input type="hidden" name="lc" value="US">
					 <input type="hidden" name="currency_code" value="USD">
					 <input type="hidden" name="item_name" value="6 foot power distribution wire">
					 <input type="hidden" name="item_number" value="KDC-01-1008-6">
					  <input type="hidden" name="amount" value="12.80">
					  <input type="hidden" name="no_note" value="1"> 
					 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>
			</td>
		</tr>
		<tr>
			<td><p class="lead">12'</p></td>
			<td><p class="lead">$18.68</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="add" value="1"> 
					<input type="hidden" name="lc" value="US">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="item_name" value="12 foot power distribution wire">
					<input type="hidden" name="item_number" value="KDC-01-1008-12">
					<input type="hidden" name="amount" value="18.68">
					<input type="hidden" name="no_note" value="1"> 
					 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>	
			</td>
		</tr>
		<tr>
			<td><p class="lead">24'</p></td>
			<td><p class="lead">$23.78</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					<input type="hidden" name="add" value="1"> 
					<input type="hidden" name="lc" value="US">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="item_name" value="24 foot power distribution wire">
					<input type="hidden" name="item_number" value="KDC-01-1008-24">
					<input type="hidden" name="amount" value="23.78">
					<input type="hidden" name="no_note" value="1"> 
					<input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>
			</td>
		</tr>
		</table>
	</div>		
</div>
<hr />
<h3>Coupler Power Wire</h3>
<div class="alert alert-danger">You will need one additional coupler power wire for each additional coupler you order.
</div>
<div class="row">
	<div class="col-md-3">
		<h4>KDC-01-1012</h4>
		<p class="lead">This will allow you to hook up a extra coupler to a Power Distribution splitter,8 port, or 24 port board.</p>
	</div>
	<div class="col-md-6">
		<img border="0" src="/mainart/parts/KDC-01-1012.jpg" class="img-responsive" />
	</div>
	<div class="col-md-3">
		<table class="table table-bordered">
		<tr><th>Length</th><th>Price</th><th>Purchase</th></tr>
		<tr>
			<td><p class="lead">3'</p></td>
			<td><p class="lead">$6.95</p></td>
			<td>
				  <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					  <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					  <input type="hidden" name="add" value="1"> 
					 <input type="hidden" name="lc" value="US">
					 <input type="hidden" name="currency_code" value="USD">
					 <input type="hidden" name="item_name" value="3 foot coupler power wire">
					 <input type="hidden" name="item_number" value="KDC-01-1012-3">
					  <input type="hidden" name="amount" value="6.95">
					  <input type="hidden" name="no_note" value="1"> 
					 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>		
			</td>
		</tr>
		<tr>
			<td><p class="lead">12'</p></td>
			<td><p class="lead">$8.83</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					  <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					  <input type="hidden" name="add" value="1"> 
					 <input type="hidden" name="lc" value="US">
					 <input type="hidden" name="currency_code" value="USD">
					 <input type="hidden" name="item_name" value="12 foot coupler power wire">
					 <input type="hidden" name="item_number" value="KDC-01-1012-12">
					  <input type="hidden" name="amount" value="8.83">
					  <input type="hidden" name="no_note" value="1"> 
					 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>
			</td>
		</tr>
		<tr>
			<td><p class="lead">18'</p></td>
			<td><p class="lead">$12.67</p></td>
			<td>
				<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					  <input type="hidden" name="business"value="onlinesales@i-keg.com"> 
					<input type="hidden" name="cmd" value="_cart">
					  <input type="hidden" name="add" value="1"> 
					 <input type="hidden" name="lc" value="US">
					 <input type="hidden" name="currency_code" value="USD">
					 <input type="hidden" name="item_name" value="18 foot coupler power wire">
					 <input type="hidden" name="item_number" value="KDC-01-1012-18">
					  <input type="hidden" name="amount" value="12.67">
					  <input type="hidden" name="no_note" value="1"> 
					 <input type="image" src="/mainart/buyit.jpg" border="0" name="I35" alt="Make payments with PayPal - it's fast, free and secure!" align="right" width="122" height="35">
				</form>	
			</td>
		</tr>
		</table>
	</div>		
</div>
</article>
@stop




