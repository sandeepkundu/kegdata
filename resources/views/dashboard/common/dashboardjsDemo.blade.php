		<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}

	var newGraph = "{{ @$newGraph}}";
	
	if(newGraph == 0){
		$("#paginate").css("display","none");
	}
	else{
		$("#paginate").css("display","block");	
	}

	//disable prev if offset is zero
	var offset_value=$('#offset_setting').val();
	var offset_setting_pour=$('#offset_setting_pour').val();
	var active_tab_hdn=$('#active_tab_hdn').val();
	$('#page_temp').hide();

	if(offset_value == 0) {
		$('#prev').css("display","none");
	} else {
		$('#prev').css("display","block");
	}

	if(offset_setting_pour == 0) {
		$('#prev').css("display","none");
	} else {
		$('#prev').css("display","block");
	}
	//end disble code prev


	function submitFilterForm(type){

		$('#identify_page_btn').val(type);
		var page_value = $('#page').val();
		page_value = parseInt(page_value);

		var page_value_temp = $('#page_temp').val();
		page_value_temp = parseInt(page_value_temp);

		var offset_setting=$('#offset_setting').val();
		offset_setting = parseInt(offset_setting);

		var offset_setting_pour=$('#offset_setting_pour').val();
		offset_setting_pour = parseInt(offset_setting_pour);

		//identify the active tab
		var active_tab='';
		if($('#show-pourcharts').hasClass("active")) {
			active_tab='pour';
		} else if($('#show-tempcharts').hasClass("active")) {
			active_tab='temp';
		} else {
			active_tab='pour';
		}
		//push the value in hdn tab
		$('#active_tab_hdn').val(active_tab);

		if(type == 'next') {
			if(active_tab == 'temp') {
				offset_setting = offset_setting+page_value_temp;
				$('#offset_setting').val(offset_setting);
			} else if(active_tab == 'pour') {
				offset_setting_pour = offset_setting_pour+page_value;
				$('#offset_setting_pour').val(offset_setting_pour);
			} else {
				offset_setting = offset_setting+page_value;
				$('#offset_setting').val(offset_setting);
			}
			
		}
		if(type == 'prev') {
			//alert(offset_setting+'==>'+page_value);
			if(active_tab == 'temp') { 
				if(offset_setting >= page_value_temp) {
					offset_setting = offset_setting-page_value_temp;
					$('#offset_setting').val(offset_setting);
				}
			} else if(active_tab == 'pour') {
				if(offset_setting_pour >= page_value) {
					offset_setting_pour = offset_setting_pour-page_value;
					$('#offset_setting_pour').val(offset_setting_pour);
				}
			} else {
				offset_setting = offset_setting-page_value;
				$('#offset_setting').val(offset_setting);
			}

			
			
		}

		$('#button-submit').trigger('click');
		
	}

	</script>