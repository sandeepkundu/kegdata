<nav class="navbar navbar-gray">
   <div class="container-fluid">
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#report-filters">
         <span class="sr-only">Toggle navigation</span>
         <span class="fa-menu-bar"></span>
         </button>
      </div>
      <div class="collapse navbar-collapse" id="report-filters">
         {!!  Form::open(['url' => '/dashboard/graph_reports', 'class' => 'navbar-form navbar-left', 'onsubmit'=> 'return validateForm()']) !!}
         <div class="col-md-12">
            <div class="form-group">
               <div class="input-group">
                  <?php $quickDates=empty($quickDates) ? "thismonth": $quickDates; ?>
                  {!! Form::select('quickDates',$reportDates, $quickDates, ['class' => 'form-control', 'id' => 'quickDates']) !!}
               </div>
            </div>
            <div class="form-group">
               <div class='input-group'>
                  <div class="input-group date" id='startDate'>
                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                     </span>
                     {!! Form::text('reportStart', $reportStart, ['class' =>'form-control']) !!}
                  </div>
                  <span class="input-group-addon"><strong>through</strong></span>
                  <div class='input-group date' id='endDate'>
                     {!! Form::text('reportEnd', $reportEnd, ['class' => 'form-control']) !!}
                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                     </span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12" style="margin-top:8px;">
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group">
                     <div class="input-group-addon">Select Keg</div>
                     <?php 
                        $kegsForSelect[""]="All";
                        ?>
                     {!! Form::select('deviceID', $kegsForSelect, $deviceID, ['class' => 'form-control', 'id' => 'deviceID']) !!}
                     <input type="hidden" name="ident_hdn" id="ident_hdn" value="<?php echo $ident_hdn; ?>">
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="input-group form-group" id="new-graph-limit">
                  <input type="hidden" name="identify_page_btn" id="identify_page_btn" value="">
                  <input type="hidden" name="offset_setting" id="offset_setting" value="{{$offset_setting}}">
                  <input type="hidden" name="offset_setting_pour" id="offset_setting_pour" value="{{$offset_setting_pour}}">
                  <input type="hidden" name="active_tab_hdn" id="active_tab_hdn" value="">
                  <div class="input-group-addon">Plotted points per page</div>
                  <input type="number" name="page_limit" id="page" value="{{$page_limit}}" class="form-control" min="0" max="10000" >
                  <input type="number" name="page_limit_temp" id="page_temp" value="{{$page_limit_temp}}" class="form-control" min="0" max="10000" style="display:none;">
               </div>
            </div>
            <div class="form-group">
               <div class="input-group">
                  <div class="input-group">
                     <input type="submit" id="button-submit" class="btn btn-primary btn-sm" name="submit" value="Run Chart" />
                  </div>
               </div>
            </div>

            <?php if($reportStart) { ?>
            <div class="form-group">
               <div class="input-group">
                  <a class="btn btn-primary btn-sm" href="/dashboard/reports/getReportPour?reportStart={{ $reportStart }}&reportEnd={{$reportEnd}}&quickDates={{ $quickDates }}&status={{$status}}&deviceID={{$deviceID}}&page={{ isset($input['page'])?$input['page']:1  }}">Export In Excel</a>
               </div>
            </div>
            <?php } ?>

         </div>
         <div class="col-md-12" id="paginate" style="margin-top:8px;">
            <div class="col-md-2 pull-right">
               <div class="input-group ">
                  <a href="javascript:void(0)" onclick="submitFilterForm('next')"  class="" id="next" name="Next" value="Next " />
                     Older <!-- <span>&nbsp  >> <span> --> 
                  </a>
               </div>
               <div class="input-group pull-right">
                  <a href="javascript:void(0)" onclick="submitFilterForm('prev')" class=" "  id="prev" name="Prev" value="<< Prev" />
                     <!-- <span value="<<" name="<<"> << &nbsp <span> --> Newer 
                  </a>
               </div>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</nav>