		<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		}
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}

	var newGraph = "{{ @$newGraph}}";
	
	if(newGraph == 0){
		$("#paginate").css("display","none");
	}
	else{
		$("#paginate").css("display","block");	
	}

	//disable prev if offset is zero
	var offset_value=$('#offset_setting').val();
	if(offset_value == 0) {
		$('#prev').css("display","none");
	} else {
		$('#prev').css("display","block");
	}


	function submitFilterForm(type){

		$('#identify_page_btn').val(type);
		var page_value = $('#page').val();
		page_value = parseInt(page_value);
		var offset_setting=$('#offset_setting').val();
		offset_setting = parseInt(offset_setting);

		if(type == 'next') {
			offset_setting = offset_setting+page_value;
			$('#offset_setting').val(offset_setting)
		}
		if(type == 'prev') {
			//alert(offset_setting+'==>'+page_value);
			if(offset_setting >= page_value) {
				offset_setting = offset_setting-page_value;
				$('#offset_setting').val(offset_setting)
			}
			
		}

		$('#button-submit').trigger('click');
		
	}

	</script>