<nav class="navbar navbar-gray">
	<div class="container-fluid">
		  <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#report-filters">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="fa-menu-bar"></span>
		      </button>
		    </div>
		<div class="collapse navbar-collapse" id="report-filters">
			{!!  Form::open(['url' => '/dashboard/reports', 'class' => 'navbar-form navbar-left', 'onsubmit'=> 'return validateForm()']) !!}

			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">

						{!! Form::select('quickDates',$reportDates, $quickDates, ['class' => 'form-control', 'id' => 'quickDates']) !!}
	
					</div>
				</div>
				<div class="form-group">
			                		<div class='input-group'>
					                	<div class="input-group date" id='startDate'>
						                	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                    {!! Form::text('reportStart', $reportStart, ['class' =>'form-control']) !!}
					                 </div>
				                    	<span class="input-group-addon"><strong>through</strong></span>

					                <div class='input-group date' id='endDate'>
					                	{!! Form::text('reportEnd', $reportEnd, ['class' => 'form-control']) !!}
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						                    </span>
					                </div>
				            </div>
			        	</div>

			       <div class="form-group">
			       		<div class="input-group">
			       			{!! Form::select('status', array('11' => 'Pour Event', '1001' => 'Heartbeat','10011' => 'Keg Blow','101'=>'Untap Event','after'=>'After Hours','other'=>'Boot Event'), $reportStatus,array('class' => 'form-control')); !!}
			       		</div>
			       	</div>

			        
			</div>
			<div class="col-md-12" style="margin-top:8px;">
					<div class="form-group">
								<div class="input-group">
									<div class="input-group">
										<div class="input-group-addon">Select Keg</div>
										<?php 
										 $kegsForSelect[""]="All";
										 ?>
										{!! Form::select('deviceID', $kegsForSelect, $deviceID, ['class' => 'form-control', 'id' => 'deviceID']) !!}
									</div>
								</div>
							</div>

					
					<div class="form-group">
						<div class="input-group">
							<div class="input-group">
							<input type="submit" class="btn btn-primary btn-sm" name="submit" value="Run Report" />
							</div>
						</div>
					</div>
					<!-- div class="form-group">
						<div class="input-group">
							<div class="input-group">
							<input type="submit" class="btn btn-primary btn-sm" name="submit" value="Run Graph" />
							</div>
						</div>
					</div -->
			        	{{--
			       	<div class="form-group">
			       		<div class="input-group">
			       			<span class="input-group-addon">
			       			Sort By
			       			</span>
			       			<select class="form-control" name="reportsSort" id="reportsSort">
			       				<option value="receivedDateTime">Default (Date)</option>
			       				<option value="kegMac">Mac Address</option>
			       				<option value="deviceName">Device Name</option>
			       				<option value="status">Event</option>
			       				<option value="tempCelcius">Temperature</option>
			       				<option value="endVal">Level</option>
			       				<option value="elapsedTenths">Pour Length</option>
			       			</select>
			       		</div>
			       	</div>
			       	--}}


			       	<div class="form-group">
			       		<div class="input-group">
			       			<a class="btn btn-primary btn-sm" href="/dashboard/reports/getReport?reportStart={{ $reportStart }}&reportEnd={{$reportEnd}}&quickDates={{ $quickDates }}&status={{$status}}&deviceID={{$deviceID}}&page={{ isset($input['page'])?$input['page']:1  }}">Export In Excel</a>
			       		</div>
			       	</div>
			</div>
			       	
			
			<!--div class="btn-group navbar-btn">
				<button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa fa-file-excel-o"></span><span class="sr-only">Export to Excel</span><span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu">
					<li><a href="/dashboard/reports/excel?reportStart={{ $reportStart }}&amp;reportEnd={{ $reportEnd }}">Download .xls</a></li>
					<li><a href="#">Download .xlsx</a></li>
					<li><a href="#">Download .csv</a></li>
				</ul>
			</div--> 

			{!! Form::close() !!}
			<form class="navbar-form navbar-right">
				<div class="form-group">
			        		<div class="input-group">
			        		<span class="input-group-addon"><span class="fa fa-search"></span></span>
			        		<input type="text" class="form-control" name="tableSearch" id="tableSearch" placeholder="Search Results"/>
			        		</div>
			        	</div>
			</form>
			
		</div>
	</div>
</nav>