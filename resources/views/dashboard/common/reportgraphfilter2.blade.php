<nav class="navbar navbar-gray">
	<div class="container-fluid">
		  <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#report-filters">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="fa-menu-bar"></span>
		      </button>
		    </div>
		<div class="collapse navbar-collapse" id="report-filters">
			{!!  Form::open(['url' => '/dashboard/graph_reports2', 'class' => 'navbar-form navbar-left']) !!}

			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<?php $quickDates=empty($quickDates) ? "thisweek": $quickDates; ?>

						{!! Form::select('quickDates',$reportDates, $quickDates, ['class' => 'form-control', 'id' => 'quickDates']) !!}
	
					</div>
				</div>
				<div class="form-group">
			                		<div class='input-group'>
					                	<div class="input-group date" id='startDate'>
						                	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						                    </span>
						                    {!! Form::text('reportStart', $reportStart, ['class' =>'form-control']) !!}
					                 </div>
				                    	<span class="input-group-addon"><strong>through</strong></span>

					                <div class='input-group date' id='endDate'>
					                	{!! Form::text('reportEnd', $reportEnd, ['class' => 'form-control']) !!}
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						                    </span>
					                </div>
				            </div>
			        	</div>
			        
			</div>
			<div class="col-md-12" style="margin-top:8px;">
					<div class="form-group">
								<div class="input-group">
									<div class="input-group">
										<div class="input-group-addon">Select Keg</div>
										<?php 
										 $kegsForSelect[""]="All";
										 ?>
										{!! Form::select('deviceID', $kegsForSelect, $deviceID, ['class' => 'form-control', 'id' => 'deviceID']) !!}

										<input type="hidden" name="ident_hdn" id="ident_hdn" value="<?php echo $ident_hdn; ?>">
									</div>
								</div>
							</div>

					
					
					<div class="form-group">
						<div class="input-group">
							<div class="input-group">
							<input type="submit" class="btn btn-primary btn-sm" name="submit" value="Update Graph" />
							</div>
						</div>
					</div>
			        	
			</div>
			       	
			
			

			{!! Form::close() !!}
			
			
		</div>
	</div>
</nav>
