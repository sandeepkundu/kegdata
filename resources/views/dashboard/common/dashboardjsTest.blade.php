	<script>
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}
	</script>