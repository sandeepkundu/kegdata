@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
    <li><a href="/dashboard/levels">Keg Levels</a></li>
    <li class="active"><a href="/dashboard/reports">Reports</a></li>
    @if($user->isAdmin)
    <li><a href="/dashboard/notifications">Notifications</a></li>
  <li><a href="/dashboard/kegsetup">Keg Setup</a></li>
  <li><a href="/dashboard/settings">Settings</a></li>
  @endif
  <li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')
<!-- tab code start -->

<div class="row">
    <nav class="nav-bar nav-bar-default">
      <div class="container-fluid">
        <div class="btn-group btn-group-sm">
          <button class="btn btn-primary navbar-btn" id="run_reports">Run Reports</button>
          <!-- <button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button> -->
          <button class="btn btn-primary navbar-btn" id="show-levelcharts">Show Level Charts</button>
          <button class="btn btn-primary navbar-btn" id="show-tempcharts">Show Temperature Charts</button>
          <button class="btn btn-primary navbar-btn" id="show-pourcharts">Show Pour Charts</button>
        </div>
                                    
                                
        <!--
        <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
           </form>-->
        </div>
    </nav>
</div>
<div id="tbl_Section">

@include('dashboard.common.reportfilter', array('reportDates' => $reportDates, 'input' => $input))
</div>
<div class="row" id="dbPanels" style="display:none;">
    <?php $count = 0; ?>
      @foreach($kegs as $keg)
      @if($keg->showDevice)
      <div class="col-sm-3">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title text-center">{{ $keg->deviceName }}</h3></div>
            <div class="panel-body">
             
              <div class="hide tempChart">
                <canvas id="tempChart{{ $keg->id }}" width="300px" height="300px"></canvas>
              </div>
              <div class="hide pourChart">
                <canvas id="pourChart{{ $keg->id }}" width="300px" height="300px"></canvas>
              </div>
              <div class="hide levelChart">
                <canvas id="levelChart{{ $keg->id }}" width="300px" height="300px"></canvas>
                <!--div id="container_level_chart" style="min-width: 300px; height: 300px; margin: 0 auto"></div-->
              </div>
                                                            
                                                        
            </div>
                                                    
                                                    
           
          </div>
        </div>
      @endif
      @endforeach

    <small>*All levels are approximate values</small>
    </div>
<!-- tab code end -->




<div class="text-center">
{!! $reportData->appends(['reportStart' => $reportStart, 'reportEnd' => $reportEnd, 'quickDates' => $quickDates,'status'=>$status,'deviceID'=>$deviceID])->render() !!}
</div>
<table class="table table-striped table-bordered" id="kegdataReports">
	<thead>
	<tr>
		<th>When<span class="pull-right"></span></th><th>Keg Mac<span class="pull-right"></span></th><th>Device Name<span class="pull-right"></span></th><th>Event<span class="pull-right"></span></th><th>Temperature<span class="pull-right"></span></th><th>Level<span class="pull-right"></span></th><th>Pour Length<span class="pull-right"></span></th>
	</tr>
	</thead>
	<tbody>
	@foreach($reportData as $data)
	@if($data->showDevice)
	<?php

    $calibration =  $kegdata->getCalibratedValue($data->kegMac);
   
		$ounces = $kegdata->getOunces($data->adc_pour_end, $calibration, $data->amount, $data->empty, $data->volumeType);
    
		$percentage = $kegdata->kegPercent($ounces, $data->amount); 
	?>
	<tr{{ ($data->status == '101') ? ' class=danger' : '' }}>
  <td>{{ $data->sent_at->timezone($timezone->timezone)->toDayDateTimeString() }}</td>
	<!--td>{{ $data->sent_at->toDayDateTimeString() }}</td-->
		<td>{{ $kegdata->getFormattedMac($data->kegMac) }}</td>
		<td>{{ $data->deviceName }}</td>
	  <td>@if($data->status == '11')
      Pour Event
              @elseif($data->status == '101')
                Untap Event
              @elseif($data->status == '10011')
                Keg Blow
              @elseif($data->status == '1001')
                Temperature
              @else
                Boot Event
              @endif
    </td>
		<td>{{ $kegdata->getTemp($data->temperature, $data->temperatureType ) }} &deg; {{ $data->temperatureType }}</td>
		<td>{!! HTML::image('/img/levels/'.$data->pic_prefix.'/'.$data->pic_prefix.$percentage.'.jpg', $percentage .'%', array('width' => '20px')  ) !!}</td>
		<td>{{ $data->pour_length }}</td>
	</tr>
	@endif
	@endforeach
	</tbody>
</table>
<div class="text-center">
{!! $reportData->render() !!}
</div>
</div> <!-- end div tbl section -->
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjs')
  @include('dashboard.restaurant.reportsjs')
  @include('dashboard.restaurant.levelsjs', array('kegs' => $kegs,'newGraph'=>$newGraph))
@stop