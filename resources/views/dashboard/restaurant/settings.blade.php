@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Settings
@stop

@section('tabs')
  	<li><a href="/dashboard/levels">Keg Levels</a></li>
  	<li><a href="/dashboard/reports">Reports</a></li>
  	@if($user->isAdmin)
  	<li><a href="/dashboard/notifications">Notifications</a></li>
	<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
	<li class="active"><a href="/dashboard/settings">Settings</a></li>
	@endif
	<li><a href="/dashboard/account">Account Management</a></li>
@stop


@section('tabpane')

<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><h2 class="panel-title">Update Account Settings</h2></div>
		<div class="panel-body">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			{!! Form::model($settings, ['url'=>'dashboard/settings', 'method' => 'patch', 'class' => 'form-horizontal' ]) !!}
			{!! Form::hidden('id') !!}
			<div class="form-group">
				<div class="col-md-4{{ !$errors->first('volumeType') ? '' : 'has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('volumeType', 'Select Volume Measurement', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Volume Measurement</div>
						{!!  Form::select('volumeType',  ['oz' => 'ounces (oz)', 'g' => 'gallons (g)', 'l' => 'liters (l)', 'ml' => 'millerliters (ml)'], null, ['class' => 'form-control']) !!}
					</div>
					@if ($errors->first('volumeType'))
						<span class="help-block">{{ $errors->first('volumeType') }}</span>
					@endif
				</div>
				<div class="col-md-4{{ !$errors->first('temperatureType') ? '' : 'has-error has-feedback' }}">
					<div class="col-md-3 form-control-static">Set Temperature</div>
					<div class="col-md-9">
						<label for="celcius" class="radio-inline">
							{!! Form::radio('temperatureType', 'C', null, ['id' => 'celcius']) !!}Celcius</label>
						<label for="fahrenheit" class="radio-inline">
							{!! Form::radio('temperatureType', 'F', null, ['id' => 'fahrenheit']) !!}Fahrenheit</label>
					</div>
					@if ($errors->first('temperatureType'))
						<span class="help-block">{{ $errors->first('temperatureType') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4{{ !$errors->first('region') ? '' : '  has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('region', 'Select Timezone Region', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Timezone Region</div>
						<!--form field-->
						{!!  Form::select('region', $regionsForSelect,  null, ['class'=> 'form-control',  'id' => 'tzRegion'])  !!}
					</div>
					@if ($errors->first('region'))
						<span class="help-block">{{ $errors->first('region') }}</span>
					@endif
				</div>
				<div class="col-md-4{{ !$errors->first('timezone') ? '' : '  has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('timezone', 'Select Timezone', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Timezone</div>
						<!--form field-->
						{!!  Form::select('timezone', $zonesForSelect,  null, ['class'=> 'form-control',  'id' => 'tz'])  !!}
					</div>
					@if ($errors->first('timezone'))
						<span class="help-block">{{ $errors->first('timezone') }}</span>
					@endif
				</div>
				<div class="col-md-4{{ !$errors->first('currency') ? '' : '  has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('currency', 'Select Currency',  ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Currency</div>
						<!--form field-->
						{!!  Form::select('currency', $currencies, null, ['class'=> 'form-control',  'id' => 'tz'])  !!}
					</div>
					@if ($errors->first('currency'))
						<span class="help-block">{{ $errors->first('currency') }}</span>
					@endif
				</div>

			</div>
			<div class="form-group">
				<div class="col-md-4{{ !$errors->first('weekStart') ? '' : ' has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('weekStart', 'Select when your week starts', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Week Start</div>
						{!! Form::select('weekStart', ['Monday' => 'Monday', 'Tuesday' => 'Tuesday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday', 'Friday' => 'Friday', 'Saturday' => 'Saturday', 'Sunday' => 'Sunday'], null, ['class' => 'form-control']) !!}
					</div>
					@if ($errors->first('weekStart'))
						<span class="help-block">{{ $errors->first('weekStart') }}</span>
					@endif
				</div>
				<div class="col-md-2{{ !$errors->first('daystart12') ? '' : ' has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('daystart12', 'Select when your day starts', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Day Start</div>
						{!! Form::select('daystart12', $timeArray, null, ['class' => 'form-control']) !!}
					</div>
					@if ($errors->first('daystart12'))
						<span class="help-block">{{ $errors->first('daystart12') }}</span>
					@endif
				</div>
				<div class="col-md-1{{ !$errors->first('amStart') ? '' : ' has-error has-feedback' }}">
					<label class="radio-inline">
					{!! Form::radio('amStart', 'am', null, ['id' => 'amStart']) !!}am</label>
					<label class="radio-inline">
					{!! Form::radio('amStart', 'pm', null, ['id' => 'pmStart']) !!}
					pm</label>
					@if ($errors->first('amStart'))
						<span class="help-block">{{ $errors->first('amStart') }}</span>
					@endif
				</div>
				<div class="col-md-2 col-md-offset-1{{ !$errors->first('dayend12') ? '' : ' has-error has-feedback' }}">
					<div class="input-group">
						{!! Form::label('dayend12', 'Select when your day end', ['class' => 'sr-only']) !!}
						<div class="input-group-addon">Day End</div>
						{!! Form::select('dayend12', $timeArray, null, ['class' => 'form-control']) !!}
					</div>
					@if ($errors->first('dayend12'))
						<span class="help-block">{{ $errors->first('dayend12') }}</span>
					@endif
				</div>
				<div class="col-md-1{{ !$errors->first('amEnd') ? '' : ' has-error has-feedback' }}">
					<label class="radio-inline">
					{!! Form::radio('amEnd', 'am', null, ['id' => 'amStart']) !!}am</label>
					<label class="radio-inline">
					{!! Form::radio('amEnd', 'pm', null, ['id' => 'pmStart']) !!}
					pm</label>
					@if ($errors->first('amEnd'))
						<span class="help-block">{{ $errors->first('amEnd') }}</span>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 text-right">
					<input type="submit" value="Update User Settings" name="update-u-settings" class="btn btn-primary">
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>		

@stop

@section('jsScript')
	@include('dashboard.common.dashboardjs')
	@include('dashboard.restaurant.settingsjs')
@stop