<script src="/js/bindWithDelay.js" type="text/javascript"></script>
<script src="/js/jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="/js/jquery.filtertable.min.js" type="text/javascript"></script>
<script src="/js/moment.min.js" type="text/javascript"></script>
<script src="/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>
var weekStart = '{{ $settings->weekStart }}';

var dateStart = moment('{{ $settings->dayStart }}', 'HH:mm:ss');
var dateEnd = moment('{{ $settings->dayEnd }}', 'HH:mm:ss');

if(dateEnd.hours() < 12){
	dateEnd.add(1, 'days');
}
var week = dateStart.clone().subtract(7, 'days');
var weekToDate = dateStart.clone().day(weekStart);
var month = dateStart.clone().subtract(1, 'month');
var monthToDate = dateStart.clone().startOf('month');
var year = dateStart.clone().subtract(1, 'year');
var yearToDate = dateStart.clone().startOf('year');
var yesterdayStart = dateStart.clone().subtract(1,'days');
var yesterdayEnd = dateEnd.clone().subtract(1,'days');
var lastWeek = week.clone().subtract(7, 'days');
var lastWeekEnd = week.clone();
var lastWeekToDate = weekToDate.clone().subtract(7,'days');
var lastWeekToDateEnd = dateEnd.clone().subtract(7,'days');
var lastMonth = month.clone().startOf('month');
var lastMonthEnd = lastMonth.clone().endOf('month');
var lastMonthToDate = monthToDate.clone().subtract(1,'month');
var lastMonthToDateEnd = dateEnd.clone().subtract(1, 'month');
var lastYear = year.clone().startOf('year');
var lastYearEnd = lastYear.clone().endOf('year');
var lastYearToDate = yearToDate.clone().subtract(1, 'year');
var lastYearToDateEnd = dateEnd.clone().subtract(1, 'year');

$(document).ready(function(){
	$('#kegdataReports').filterTable({'inputSelector':'#tableSearch' }); 
	$("#kegdataReports").tablesorter(); 

	$(function () {
	                $('#startDate').datetimepicker({
		                	defaultDate:dateStart,
		                	maxDate:dateEnd,
	                		sideBySide: true
	                });
	                $('#endDate').datetimepicker({
	                		defaultDate: dateEnd,
	                		minDate:dateStart,
               			sideBySide: true
	                	});
		$("#startDate").on("dp.change", function (e) {
			$('#endDate').data("DateTimePicker").minDate(e.date);
			 checkQuickDates($('#startDate').data("DateTimePicker").date(), $('#endDate').data("DateTimePicker").date());
		});
		$("#endDate").on("dp.change", function (e) {
			$('#startDate').data("DateTimePicker").maxDate(e.date);
			 checkQuickDates($('#startDate').data("DateTimePicker").date(), $('#endDate').data("DateTimePicker").date());
		});
	});

	$('#quickDates').on('change', function(e){
		var quick = $('#quickDates option:selected').val();
		switch(quick){
			case 'today':
				$('#startDate').data("DateTimePicker").date(dateStart);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thisweek':
				$('#startDate').data("DateTimePicker").date(week);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thisweektodate':
				$('#startDate').data("DateTimePicker").date(weekToDate);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thismonth':
				$('#startDate').data("DateTimePicker").date(month);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thismonthtodate':
				$('#startDate').data("DateTimePicker").date(monthToDate);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thisyear':
				$('#startDate').data("DateTimePicker").date(year);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'thisyeartodate':
				$('#startDate').data("DateTimePicker").date(yearToDate);
				$('#endDate').data("DateTimePicker").date(dateEnd);
			break;
			case 'yesterday':
				$('#startDate').data("DateTimePicker").date(yesterdayStart);
				$('#endDate').data("DateTimePicker").date(yesterdayEnd);
			break;
			case 'lastweek':
				$('#startDate').data("DateTimePicker").date(lastWeek);
				$('#endDate').data("DateTimePicker").date(lastWeekEnd);
			break;
			case 'lastweektodate':
				$('#startDate').data("DateTimePicker").date(lastWeekToDate);
				$('#endDate').data("DateTimePicker").date(lastWeekToDateEnd);
			break;
			case 'lastmonth':
				$('#startDate').data("DateTimePicker").date(lastMonth);
				$('#endDate').data("DateTimePicker").date(lastMonthEnd);
			break;
			case 'lastmonthtodate':
				$('#startDate').data("DateTimePicker").date(lastMonthToDate);
				$('#endDate').data("DateTimePicker").date(lastMonthToDateEnd);
			break;
			case 'lastyear':
				$('#startDate').data("DateTimePicker").date(lastYear);
				$('#endDate').data("DateTimePicker").date(lastYearEnd);
			break;
			case 'lastyeartodate':
				$('#startDate').data("DateTimePicker").date(lastYearToDate);
				$('#endDate').data("DateTimePicker").date(lastYearToDateEnd);
			break;
			case 'custom':
				$('#startDate').focus();
			break;
		}
	});

});

function checkQuickDates(start, end){
	if(start.diff(dateStart)== 0 &&end.diff(dateEnd)== 0){
		$('#quickDates option[value="today"]').prop('selected', true);
	}else if(start.diff(week) == 0 && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thisweek"]').prop('selected', true);
	}else if(start.diff(weekToDate) == 0 && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thisweektodate"]').prop('selected', true);
	}else if(start.diff(month) == 0  && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thismonth"]').prop('selected', true);
	}else if(start.diff(monthToDate) == 0  && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thismonthtodate"]').prop('selected', true);
	}else if(start.diff(year) == 0 && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thisyear"]').prop('selected', true);
	}else if(start.diff(yearToDate) == 0  && end.diff(dateEnd) == 0){
		$('#quickDates option[value="thisyeartodate"]').prop('selected', true);
	}else if(start.diff(yesterdayStart) == 0 && end.diff(yesterdayEnd) == 0){
		$('#quickDates option[value="yesterday"]').prop('selected', true);
	}else if(start.diff(lastWeek) == 0 && end.diff(lastWeekEnd) == 0){
		$('#quickDates option[value="lastweek"]').prop('selected', true);
	}else if(start.diff(lastWeekToDate) == 0 && end.diff(lastWeekToDateEnd) == 0){
		$('#quickDates option[value="lastweektodate"]').prop('selected', true);
	}else if(start.diff(lastMonth) == 0 && end.diff(lastMonthEnd) == 0){
		$('#quickDates option[value="lastmonth"]').prop('selected', true);
	}else if(start.diff(lastMonthToDate) == 0 && end.diff(lastMonthToDateEnd) == 0){
		$('#quickDates option[value="lastmonthtodate"]').prop('selected', true);
	}else if(start.diff(lastYear) == 0 && end.diff(lastYearEnd) == 0){	
		$('#quickDates option[value="lastyear"]').prop('selected', true);
	}else if(start.diff(lastYearToDate) == 0 && end.diff(lastYearToDateEnd) == 0){
		$('#quickDates option[value="lastyeartodate"]').prop('selected', true);
	}else{
		$('#quickDates option[value="custom"]').prop('selected', true);
	}


}

</script>