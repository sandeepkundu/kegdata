<script type="text/javascript">
	$(document).ready(function(){
		$('.delUser').on('click', function(){
			console.log('click');
			var userID = $(this).parent('tr').attr('data-userID');
			var deleteConfirm = confirm('Deleting this user will be permanent. Continue?');
			if(deleteConfirm){
				var url = "/dashboard/account/user/" + userID;
					$.ajax(url,{'type' : 'delete'}).done(
   						function(results){
							if(results['Success'] == true){
								activate_alert('User Deleted');
								$('tr[data-userID="'+results['id']+'"]').remove();
							}else{
								activate_alert('Could not delete user');
							}
						}).fail(function(jqXHR, textStatus){
							activate_alert(textStatus);
						});	
				}
		});
	});
</script>
<script type='text/javascript' src='/js/bootstrap-formhelper.js'></script>