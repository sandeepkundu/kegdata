<html>
	<head>
		<title>KegData Report</title>
		{!! HTML::style('css/custom-bootstrap.css') !!}
	</head>
	<body>
		<table class="table table-bordered">
			<!-- START ADDRESS/PRODUCTS ROWS -->
			<tr>
				<th colspan="2">{{ strtoupper($account->account_name) }}</th>
				<td></td>
				<td></td>
				<td></td>
				<!-- START PRODUCTS LOOP -->
				<th  headers="kegInformation" class="vertical-text" v-align="bottom" {{ $account->address2 ? 'rowspan=11' : 'rowspan=10' }}>Product Name</th>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<th colspan="2">{{ strtoupper($accountAddress->address1) }}</th>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			@if($account->address2)
			<tr>
				<th colspan="2">{{ strtoupper($accountAddress->address2) }}</th>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			@endif
			<tr>
				<th colspan="2">{{ strtoupper($accountAddress->city) }}, {{ strtoupper($accountAddress->state) }} {{ $accountAddress->zipcode }}</th>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<th colspan="2">{{ $account->accountPhone1 }}</th>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr style="height:1em;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<th id="time"><b>DATE OF REPORT</b></th>
				<td headers="date">{{ $reportDT->format('n-j-Y') }}</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<th id="time"><b>TIME OF REPORT</b></th>
				<td headers="time">{{ $reportDT->format('h:i A') }}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr style="height:1em;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<th id="lastreport">Last Report</th>
				<td headers="lastreport"></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<!-- END ADDRESS/PRODUCTS ROWS -->
			<!-- START DATA ROWS< -->
			<tr>
				<th id="period">PERIOD</th>
				<th id="totalDispensed">TOTAL DISPENSED</th>
				<th id="totalCharged">TOTAL CHARGED</th>
				<th id="difference">DIFFERENCE</th>
				<th id="kegInformation">KEG INFORMATION</th>
			</tr>
			<tr>
				<th id="today">TODAY</th>
				<td headers="totalDispensed today"></td>
				<td headers="totalCharged today"></td>
				<td headers="difference today"></td>
				<th headers="kegInformation" id="currentInventory">CURRENT INVENTORY</th>
				<!-- START PRODUCTS LOOP -->
				<td headers="currentInventory"></td>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<th id="yesterday">YESTERDAY</th>
				<td headers="totalDispensed yesterday"></td>
				<td headers="totalCharged yesterday"></td>
				<td headers="difference yesterday"></td>
				<th headers="kegInformation" id="daysSinceTapped">DAYS SINCE TAPPED</th>
				<!-- START PRODUCTS LOOP -->
				<td headers="daysSinceTapped"></td>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<th id="thisWeek">THIS WEEK</th>
				<td headers="totalDispensed thisWeek"></td>
				<td headers="totalCharged thisWeek"></td>
				<td headers="difference thisWeek"></td>
				<th headers="kegInformation" id="avgOzPerDay">AVG OZ PER DAY</th>
				<!-- START PRODUCTS LOOP -->
				<td headers="avgOzPerDay"></td>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<th id="thisMonth">THIS MONTH</th>
				<td headers="totalDispensed thisMonth"></td>
				<td headers="totalCharged thisMonth"></td>
				<td headers="difference thisMonth"></td>
				<th headers="kegInformation" id="avgDaysPerKeg">AVG DAYS PER KEG</th>
				<!-- START PRODUCTS LOOP -->
				<td headers="avgDaysPerKeg"></td>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<th id="thisYear">THIS YEAR</th>
				<td headers="totalDispensed thisYear"></td>
				<td headers="totalCharged thisYear"></td>
				<td headers="difference thisYear"></td>
				<td></td>
				<!-- START PRODUCTS LOOP -->
				<td></td>
				<!-- END PRODUCTS LOOP -->
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<th id="totalInventory">TOTAL CURRENT INVENTORY</th>
				<td headers="totalInventory"></td>
				<!-- START PRODUCTS LOOP -1 -->
				<!-- <td></td> -->
				<!-- END PRODUCTS LOOP -1 -->
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<th id="avgOzPerDayTotal">AVERAGE OZ PER DAY</th>
				<td headers="avgOzPerDayTotal"></td>
				<!-- START PRODUCTS LOOP -1 -->
				<!-- <td></td> -->
				<!-- END PRODUCTS LOOP -1 -->
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<th id="avgDaysPerKegTotal">AVG DAYS PER KEG</th>
				<td headers="avgDaysPerKegTotal"></td>
				<!-- START PRODUCTS LOOP -1 -->
				<!-- <td></td> -->
				<!-- END PRODUCTS LOOP -1 -->
			</tr>
		</table>
	</body>
</html>