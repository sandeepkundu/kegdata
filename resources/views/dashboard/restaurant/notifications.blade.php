<?php
$hasNotifications = array_flatten($kegs->fetch('notifications')->toArray());
?>
@section('title')
{{ Config::get('constants.COMPANY_NAME') }} Notifications
@stop

@section('tabs')
<li><a href="/dashboard/levels">Keg Levels</a></li>
<li><a href="/dashboard/reports">Reports</a></li>
@if($user->isAdmin)
<li class="active"><a href="/dashboard/notifications">Notifications</a></li>
<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
<li><a href="/dashboard/settings">Settings</a></li>
@endif
<li><a href="/dashboard/account">Account Management</a></li>
@stop


@section('tabpane')
<div class="alert alert-info alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only">Close</span>
	</button>
	<p class="lead text-center">On this panel you can set up or change alarms or notifications for keg monitoring. <br />Go to your Accounts Management 
tab to set your default text number and default email notification information.</p>
	<ul>
		<li><strong>Low Level Notification</strong>: Receive a notification when a keg drops below a specific level of beer</li>
		<li><strong>Empty Notification</strong>: Receive a notification when a keg is empty</li>
		<li><strong>Untap Notification</strong>: Receive a notification when a keg is untapped</li>
		<li><strong>Pour Notification</strong>: Receive a notification when a beer is poured outside of operation hours</li>
		<li><strong>Temperature Notification</strong>: Receive a notification when the temperature of the keg is greater than or less than a specific temperature; will use C or F setting from account</li>
	</ul>
	<br />
	<p>*For notifications, one notification immediately and then notifications 
		will be sent at the specified frequency. Notifications will stop sending once the keg is back in an acceptable level for Notifications. 
		Only one notice will be sent per untap event or if "Only one notification" is requested.
	</p>
	<p>**Certain Notifications will only ever send once</p>
	<p>***To stop notifications, please delete the notification you wish to stop</p>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading"><h2 class="panel-title">Current Notifications</h2></div>
			<div class="panel-body">
				@if(empty($hasNotifications))
				<div class="row">
					<div class="col-sm-11">
						<p class="lead">You have no notifications. Use the panel on the right to get started!</p>
					</div>
					<div class="col-sm-1">
						<p class="lead"><i class="fa fa-arrow-circle-right"></i></p>
					</div>
				</div>
				
				@else
				<table class="table table-striped table-hover" id="notifications">
					<thead>
						<tr><th></th><th></th><th>Keg Mac Address</th><th>Keg Name</th><th>Notification Type</th><th>Range</th><th>Level</th></tr>
					</thead>
					<tbody>
						@foreach($kegs as $keg)
						@foreach($keg->notifications as $notification)
						<tr id="keg-{{ $keg->id }}" data-notification="{{ $notification->notification }}" data-notificationID="{{ $notification->id }}" data-frequency="{{ $notification->frequency }}" data-operator="{{ $notification->operator }}" data-value="{{ $notification->value }}" data-smscheck="{{ $notification->sms_notification }}" data-emailcheck="{{ $notification->email_notification }}">
							<td class="delNotification"><i class="fa fa-trash"><span class="sr-only">Delete Notification</span></i></td>
							<td style="cursor:pointer" class="editNotification"><i class="fa fa-pencil-square-o"><span class="sr-only">Edit Notification</span></i></td>
							<td>{{ $kegdata->getFormattedMac( $keg->kegMac ) }}</td>
							<td>{{ $keg->deviceName }}</td>
							<td>{{ $freqTypes[$notification->notification] }}</td>
							<td>{{ $notification->operator }}</td>
							<td>{{ $notification->value }} </td>
						</tr>
						@endforeach
						@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default" id="editAlarms">
			<div class="panel-heading"><h2 class="panel-title">Set Notifications and Alarms</h2></div>
			<div class="panel-body">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				{!! Form::open(['url' => 'dashboard/notifications', 'class' => 'form-horizontal', 'method' => 'put']) !!}
				{!! Form::hidden('id', 0, ['id' => 'notificationID']) !!}
				<div class="form-group">
					<div class="col-md-12{{ !$errors->first('kegdevice_id') ? '' : 'has-error has-feedback' }}">
						<div class="input-group">
							{!! Form::label('kegdevice_id', 'Select Keg', ['class' => 'sr-only']) !!}
							<div class="input-group-addon">Select Keg</div>
							{!! Form::select('kegdevice_id', $kegsForSelect, null, ['class' => 'form-control', 'id' => 'alarmKeg']) !!}
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12{{ !$errors->first('notification_type') ? '' : 'has-error has-feedback' }}">
						<div class="input-group">
							{!! Form::label('notification_type', 'Notification Type', ['class'=>'sr-only']) !!}
							<div class="input-group-addon">Notification Type</div>
							<!--form field-->
							{!! Form::select('notification', ['' => 'Select Notification Type', 'L' => 'Low Level', 'E' => 'Empty Keg', 'U' => 'Keg was untapped', 'P' => 'Pour Conducted after hours', 'T' => 'Temperature'], null, ['class' => 'form-control', 'id' => 'notification']) !!}
						</div>
					</div>
				</div>
				<div class="form-group {{ in_array(old('notification_type'), ['L', 'E']) ? '' : ' hidden' }}" id="range-control">
					<div class="col-md-4{{ !$errors->first('volumeType') ? '' : 'has-error has-feedback' }}">
						<div class="radio text-center{{ old('notification_type') == 'L' ? hidden : '' }}" id="range-radios">
							{!! Form::label('range', 'Range Indicator', ['class' => 'sr-only']) !!}
							<!--form field-->
							<label for="lessThan">
								{!! Form::radio('range', "<", true, ['id' => 'lessThan']) !!} Less Than</label>
								<label for="greaterThan"> <input type="radio" name="range" id="greaterThan" value=">" /> 
									{!! Form::radio('range', '>', false, ['id' => 'greaterThan']) !!}Greater Than</label>
								</div>
								<div id="range-text" class="text-center{{ old('notification_type') != 'L' ? ' hidden' : '' }}">
									<strong>Less Than</strong>
								</div>
							</div>
							<div class="col-md-8{{ !$errors->first('value') ? '' : 'has-error has-feedback' }}">
								<div class="input-group">
									{!! Form::label('value', 'Value', ['class' => 'sr-only']) !!}
									<div class="input-group-addon">Value</div>
									{!! Form::input('number', 'value', null, ['class' => 'form-control', 'id' => 'value']) !!}
									<div class="input-group-addon" id="valueSuffix">{{$temperatureType}}</div>
								</div>
							</div>
						</div>
						<div class="form-group {{ in_array(old('notification_type'), ['L', 'E', 'T']) ?  '' : ' hidden' }}" id="freq-control">
							<div class="col-md-12{{ !$errors->first('frequency') ? '' : 'has-error has-feedback' }}">
								<div class="input-group">
									{!! Form::label('frequency', 'Frequency to receive notifications', ['class' => 'sr-only']) !!}
									<div class="input-group-addon">Frequency</div>
									<!--form field-->
									{!! Form::select('frequency', $freqs, null, ['class' => 'form-control', 'id' => 'frequency']) !!}
								</div>
							</div>
						</div>

						<!-- change by admin -->
					<div class="form-group">

					<div class="col-md-4  text-right">
						<div class="col-md-7 "><span>Notify by</span></div>
						<div class="checkbox col-md-5  text-right	">
							{!! Form::checkbox('sms_notification', 1, null, ['class' => '','id' => 'sms_notification']) !!}
						</div>
					</div>
				<div class="col-md-6">
					{!! Form::label('sms_alert', 'Sms Alert', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
						Text
						</div>
						{!! Form::text('sms_alert', Auth::user()->account->sms_alert, ['class' => 'form-control', 'id' => 'smsAlert', 'required' => 'required','disabled' => 'disabled',]) !!}
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4  text-right">
						<div class="col-md-7 "></div>
						<div class="checkbox col-md-5  text-right	">
							{!! Form::checkbox('email_notification', 1, null, ['class' => '','id' => 'email_notification']) !!}
						</div>
					</div>
			

				<div class="col-md-6">
					{!! Form::label('email_alert', 'Email Alert', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							Email
						</div>
						{!! Form::email('email_alert',Auth::user()->account->email_alert, ['class' => 'form-control', 'id' => 'emailAlert','disabled' => 'disabled',]) !!}
					</div>
					
				</div>
			</div>


					<!-- end -->

						<div class="row form-group text-center">
							<input type="submit" name="submitNotification" id="submitNotification" value="Add Notification" class="btn btn-primary" />
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@stop

		@section('jsScript')
		@include('dashboard.common.dashboardjs')
		@include('dashboard.residential.notificationsjs', array('kegs' => $kegs))
		@stop