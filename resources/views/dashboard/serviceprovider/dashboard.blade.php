@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Dashboard
@stop


@section('content')
	<ul class="nav nav-tabs nav-justified tabs" id="employee-tabs" role="tablist">
		<li class="{{$tab == 'account' ? 'active' : '' }}"><a href="#account">Account Management</a></li>
  	</ul>
  	<div class="container-fluid">
	  	<div class="tab-content col-md-12" style="inline-block">
			<div class="tab-pane{{$tab == 'account' ? ' active' : '' }}" id="account">
				@include($tabcontent['account'])
			 </div>
	  	</div>
  	</div>

@stop


@section('jsScript')
	<script>
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}
	</script>
	@include($tabcontent['accountjs'])
@stop
