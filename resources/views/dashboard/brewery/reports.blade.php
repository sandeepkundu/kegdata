@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
  	<li><a href="/dashboard/levels">Keg Levels</a></li>
  	<li class="active"><a href="/dashboard/reports">Reports</a></li>
  	@if($user->isAdmin)
	<li><a href="/dashboard/settings">Settings</a></li>
	@endif
	<li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')
<nav class="navbar navbar-gray">
	<div class="container-fluid">
		  <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#report-filters">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="fa-menu-bar"></span>
		      </button>
		    </div>
		<div class="collapse navbar-collapse" id="report-filters">
	<form id="filterReports" action="" method="post" enctype="multipart/form-data" class="navbar-form navbar-left"/>

			<label class="sr-only">Select Keg</label>
			<div class="input-group">
				<span class="input-group-addon"><strong class="fa fa-beer"></strong></span>
				{!! Form::select('selectDistKeg', $kegsForSelect, null, ['class'=>'form-control', 'id' => 'selectDistKeg']) !!}
			</div>

			<div class="btn-group">
				<button class="btn btn-primary" id="blown">Blown Kegs</button>
				<button class="btn btn-primary" id="all">All Kegs</button>
			</div>

		        	<!--<button class="btn btn-primary"><span class="fa fa-file-excel-o"></span><span class="sr-only">Export to Excel</span></button>-->
	</form>
		</div>
	</div>
</nav>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h2 class="panel-title">Data Received - Most Recent 50</h2></div>
			<div class="panel-body">
				<table class="table table-striped table-hover" id="kegDistTable">
				<thead>
					<tr><th><span></span></th><th>Product</th><th>Customer<span></span></th><th>Address 1<span></span></th><th>Address2<span></span></th><th>City<span></span></th>
					<th>State<span></span></th><th>Zip Code<span></span></th><th>Percentage<span></span></th></tr>
				</thead>
				<tbody>
					@foreach($kegs as $keg)
					@if($keg->showDevice)
						<?php
							$ounces = $kegdata->getOunces($keg->recentPour->adc_pour_end, 0, $keg->recentPour->amount, $keg->recentPour->empty,$keg->recentPour->volumeType);
							$percentage = $kegdata->kegPercent($ounces, $keg->recentPour->amount); 
						?>
					@if($percentage < 15)
						<tr class="danger" id="keg{{ $keg->id }}"><td><i class="fa fa-truck"><span class="sr-only">Keg is below reorder percentage</span></i></td>
					@else
						<tr id="keg{{ $keg->id }}"><td></td>
					@endif
						<td>{{ $keg->name }}</td>
						<td>{{ $keg->account->accountName }}</td>
						<td>{{ $keg->account->accountAddress->first()->address1 }}</td>
						<td>{{ $keg->account->accountAddress->first()->address2 }}</td>
						<td>{{ $keg->account->accountAddress->first()->city }}</td>
						<td>{{ $keg->account->accountAddress->first()->stateCode }}</td>
						<td>{{ $keg->account->accountAddress->first()->zipcode }}</td>
						<td>{!! HTML::image('/img/levels/keg/keg'.$percentage.'.jpg', $percentage.'%', array('width' => '20px')  ) !!} {{ $percentage }}%</td>
						</tr>
					@endif
					@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjs')
  @include('dashboard.brewery.reportsjs')
@stop