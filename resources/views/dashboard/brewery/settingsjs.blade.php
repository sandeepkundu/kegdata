<script type="text/javascript">
	$(document).ready(function(){
		$('#tzRegion').on('change', function(){
			var region = $('#tzRegion option:selected').val();
			var url='/api/timezone/' + region;

			$.getJSON(url, function(results){
				$('#tz').empty();
				var fOption = $('<option>').attr('value', "").html('Select Timezone');
				$('#tz').append(fOption);
				var html = '';
				var offset = 0;
				for(var i = 0; i < results.length; i++){
					var option = $('<option>').attr('value', results[i]['id']).html(results[i]['offsetTimezone']);
					$('#tz').append(option);
				}
				$('#tz').focus();
			}).fail(function(){
				activate_alert('Could not load timezones');
			});
		});
	});
</script>