@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
    <li><a href="/dashboard/levels">Keg Levels</a></li>
    <li class="active"><a href="/dashboard/reports">Reports</a></li>
    @if($user->isAdmin)
    <li><a href="/dashboard/notifications">Notifications</a></li>
  <li><a href="/dashboard/settings">Settings</a></li>
  @endif
  <li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')
@include('dashboard.common.reportfilter', array('reportDates' => $reportDates, 'input' => $input))

<div class="text-center">
{!! $reportData->appends(['reportStart' => $reportStart, 'reportEnd' => $reportEnd, 'quickDates' => $quickDates])->render() !!}
</div>
<table class="table table-striped table-bordered" id="kegdataReports">
	<thead>
	<tr>
		<th>When<span class="pull-right"></span></th><th>Keg Mac<span class="pull-right"></span></th><th>Device Name<span class="pull-right"></span></th><th>Event<span class="pull-right"></span></th><th>Temperature<span class="pull-right"></span></th><th>Level<span class="pull-right"></span></th><th>Pour Length<span class="pull-right"></span></th>
	</tr>
	</thead>
	<tbody>
	@foreach($reportData as $data)
	@if($data->showDevice)
	<?php
		$ounces = $kegdata->getOunces($data->endVal, 0, $data->amount, $data->empty, $data->volumeType);
		$percentage = $kegdata->kegPercent($ounces, $data->amount); 
	?>
	<tr{{ ($data->status == '10000101') ? ' class=danger' : '' }}>
		<td>{{ $data->receivedDateTime->timezone($timezone->timezone)->toDayDateTimeString() }}</td>
		<td>{{ $kegdata->getFormattedMac($data->kegMac) }}</td>
		<td>{{ $data->deviceName }}</td>
		<td>@if($data->status == '10000011')
			Pour Event
		          @elseif($data->status == '10000101')
		          	Untap Event
		          @endif	
		</td>
		<td>{{ $kegdata->getTemp($data->tempCelcius, $data->temperatureType ) }} &deg; {{ $data->temperatureType }}</td>
		<td>{!! HTML::image('/img/levels/'.$data->pic_prefix.'/'.$data->pic_prefix.$percentage.'.jpg', $percentage .'%', array('width' => '20px')  ) !!}</td>
		<td>{{ $data->elapsedTenths/10 }}</td>
	</tr>
	@endif
	@endforeach
	</tbody>
</table>
<div class="text-center">
{!! $reportData->render() !!}
</div>
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjs')
  @include('dashboard.restaurant.reportsjs')
@stop