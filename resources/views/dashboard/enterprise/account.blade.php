@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Account Management
@stop

@section('tabs')
  	<li><a href="/dashboard/levels">Keg Levels</a></li>
  	<li><a href="/dashboard/reports">Reports</a></li>
  	@if($user->isAdmin)
  	<li><a href="/dashboard/notifications">Notifications</a></li>
	<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
	<li><a href="/dashboard/settings">Settings</a></li>
	@endif
	<li class="active"><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')

<div class="col-md-12">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
</div>
@if($user->isAdmin)
<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading"><h2 class="panel-title">Edit Account Information</h2></div>
		<div class="panel-body">
			{!! Form::model($account, ['url' => '/dashboard/account/'.$account->id, 'class' => 'form-horizontal', 'method' => 'patch']) !!}
			<div class="form-group{{ !$errors->first('accountName') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('accountName', 'Change Account Name', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							Account Name
						</div>
						{!!  Form::text('accountName', null, ['class' => 'form-control' , 'id' => 'accountName', 'required' => 'required']) !!}
					</div>
					@if ($errors->first('accountName'))
						<span class="help-block">{{ $errors->first('accountName') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('accountEmail1') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('accountEmail1', 'Change Alternate Email 1', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-envelope-o"></i> Primary Contact Email
						</div>
						{!!  Form::email('accountEmail1', null, ['class' => 'form-control' , 'id' => 'accountEmail1']) !!}
					</div>
					@if ($errors->first('accountEmail1'))
						<span class="help-block">{{ $errors->first('accountEmail1') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('accountEmail2') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('accountEmail2', 'Change Alternate Email 2', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-envelope-o"></i> Alternate Email 2
						</div>
						{!!  Form::email('accountEmail2', null, ['class' => 'form-control' , 'id' => 'accountEmail2']) !!}
					</div>
					@if ($errors->first('accountEmail2'))
						<span class="help-block">{{ $errors->first('accountEmail2]') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6{{ !$errors->first('accountPhone1') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('accountPhone1', 'Phone Number', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-phone"></i> Phone
						</div>

						{!! Form::input('tel', 'accountPhone1', null, ['class' => 'form-control', 'id' => 'accountPhone1', 'required' => 'required']) !!}
					</div>
					@if ($errors->first('accountPhone1'))
						<span class="help-block">{{ $errors->first('accountPhone1]') }}</span>
					@endif
				</div>
				<div class="col-md-6{{ !$errors->first('accountPhone2') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('accountPhone2', 'Fax Number', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-phone"></i> Fax
						</div>
						{!! Form::input('tel', 'accountPhone2', null, ['class' => 'form-control', 'id' => 'accountPhone2']) !!}
					</div>
					@if ($errors->first('accountPhone2'))
						<span class="help-block">{{ $errors->first('accountPhone2') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('website') ? '' : ' has-error has-feedback' }}">
				<div class='col-md-12'>
					{!! Form::label('website', 'Website', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">Website</div>
						{!! Form::text('website', null, ['class' => 'form-control', 'id' => 'website']) !!}
					</div>
					@if( $errors->first('website'))
						<span class="help-block">{{ $errors->first('website') }}</span>
					@endif
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Save Account" />
			{!! Form::close() !!}
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h2 class="panel-title">Edit Account Address</h2></div>
		<div class="panel-body">
			{!! Form::model($accountAddress, ['url' => 'dashboard/account/address/'.$accountAddress->id, 'class' => 'form-horizontal', 'method' => 'patch']) !!}
			<div class="form-group{{ !$errors->first('address1') ?  '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('address1', 'Edit Address 1', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							Address 1
						</div>
						{!! Form::text('address1', null, ['class' => 'form-control', 'id' => 'address1', 'required' => 'required']) !!}
					</div>
					@if( $errors->first('address1') )
						<span class="help-block">{{ $errors->first('address1') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('address2') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('address2', 'Edit Address 2', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							Address 2
						</div>
						{!! Form::text('address2', null, ['class' => 'form-control', 'id' => 'address2']) !!}
					</div>
					@if( $errors->first('address2'))
						<span class="help-block">{{ $errors->first('address2') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4{{ !$errors->first('city') ? '' : ' has-error has-feedback' }}">
					{!!  Form::label('city', 'Edit City', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							City
						</div>
						{!! Form::text('city', null, ['class' => 'form-control', 'id' => 'city', 'required' => 'required']) !!}
					</div>
					@if ( $errors->first('city') )
						<span class="help-block">{{ $errors->first('city') }}</span>
					@endif
				</div>
				<div class="col-md-4{{ !$errors->first('stateCode') ? '' : ' has-error has-feedback' }}">
					{!!  Form::label('stateCode', 'State', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							State
						</div>
						{!! Form::select('stateCode', $states, null, ['class' => 'form-control', 'id' => 'stateCode']) !!}
					</div>
					@if( $errors->first('stateCode'))
						<span class="help-block">{{ $errors->first('stateCode') }}</span>
					@endif
				</div>
				<div class="col-md-4{{ !$errors->first('zipcode') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('zipcode', 'Zip Code', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">
							Zip Code
						</div>
						{!! Form::text('zipcode', null, ['class' => 'form-control', 'id' => 'zipcode', 'required' => 'required']) !!}
					</div>
					@if($errors->first('zipcode'))
						<span class="help-block">{{ $errors->first('zipcode') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6{{ !$errors->first('county') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('county', 'County', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">County</div>
						{!! Form::text('county', null, ['class' => 'form-control', 'id' => 'county', 'required' => 'required']) !!}
					</div>
					@if($errors->first('county'))
						<span class="help-block">{{ $errors->first('county') }}</span>
					@endif
				</div>
				<div class="col-md-6{{ !$errors->first('countryCode') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('countryCode', 'Country', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">Country</div>
						{!! Form::select('countryCode', $countries, null, ['class' => 'form-control', 'id' => 'countryCode', 'required' => 'required']) !!}
					</div>
					@if($errors->first('countryCode'))
						<span class="help-block">{{ $errors->first('countryCode') }}</span>
					@endif
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Save Address" />
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endif
<div class="{{ $user->isAdmin ? 'col-md-6' : 'col-md-12' }}">
	<div class="panel panel-default">
		<div class="panel panel-heading"><h2 class="panel-title">User Information</h2></div>
		<div class="panel-body">
			{!! Form::model($user, ['url' => '/dashboard/account/user/'.$user->id, 'method' => 'patch',  'class' => 'form-horizontal']   ) !!}
			<div class="form-group{{ !$errors->first('email') ? '' : ' has-error has-feedback' }} ">
				<div class="col-md-12">
					{!! Form::label('email', 'Change Login Email Address', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-envelope"></i>Login Email</div>
						{!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'required' => 'required']) !!}
					</div>
					@if( $errors->first('email') )
						<span class="help-block"> {{ $errors->first('email') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('password') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('password', "Password", ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-key"></i>Password</div>
						{!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
					</div>
					@if( $errors->first('password') )
						<span class="help-block"> {{ $errors->first('password') }} </span>
					@endif
				</div>
			</div>
			<div class="form-group{{ !$errors->first('password_confirmation') ? '' : ' has-error has-feedback' }}">
				<div class="col-md-12">
					{!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-key"></i>Confirm Password</div>
						{!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation']) !!}
					</div>
					@if( $errors->first('password_confirmation') )
						<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6{{ !$errors->first('firstName') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('firstName', 'First Name', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">First Name</div>
						{!! Form::text('firstName', null, ['class' => 'form-control', 'id' => 'firstName', 'required' => 'required']) !!}
					</div>
					@if( $errors->first('firstName') )
						<span class="help-block">{{ $errors->first('firstName') }}</span>
					@endif
				</div>
				<div class="col-md-6{{ !$errors->first('lastName') ? '' : ' has-error has-feedback' }}">
					{!! Form::label('lastName', 'Last Name', ['class' => 'sr-only']) !!}
					<div class="input-group">
						<div class="input-group-addon">Last Name</div>
						{!! Form::text('lastName', null, ['class' => 'form-control', 'id' => 'lastName', 'required' => 'required']) !!}
					</div>
					@if( $errors->first('lastName') )
						<span class="help-block">{{ $errors->first('lastName') }}</span>
					@endif
				</div>
			</div>
			<input type="submit" class="btn btn-primary" value="Save User Information" />
			{!! Form::close() !!}
		</div>
	</div>
	@if($user->isAdmin)
	<div class="panel panel-default">
		<div class="panel-heading"><h2 class="panel-title">Manage Account Users</h2></div>
		<div class="panel-body">
		@if($accountUsers->user->count() > 1)
			<table class="table table-striped">
				<tr> 
					<th></th>
					<th></th>
					<th>Email</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Administrator</th>
					<th>Active</th>
				</tr>
				@foreach($accountUsers->user as $aUser)
					@if($aUser->email != $user->email)
					<tr data-userID="{{ $aUser->id }}">
						<td class="delUser"><i class="fa fa-trash"><span class="sr-only">Delete User</span></i></td>
						<td class="editUser"><i class="fa fa-pencil-square-o"><span class="sr-only">Edit User</span></i></td>
						<td>{{ $aUser->email }}</td>
						<td>{{ $aUser->firstName }}</td>
						<td>{{ $aUser->lastName }}</td>
						<td>@if( $aUser->isAdmin == 1)
							Yes
						         @else
						         	No
						         @endif
						</td>
						<td>@if( $aUser->isActive == 1)
							Yes
						         @else
						         	No
						         @endif
						</td>
					</tr>
					@endif
				@endforeach
			</table>
		@else
			<p class="lead">No additional users are on this account.</p>
		@endif
		</div>
	</div>
	<div class="panel panel-default" id="addUser">
		<div class="panel-body">
			<fieldset><legend>Add New User</legend>
			{!! Form::open(['url' => '/dashboard/account/user', 'method' => 'put', 'class' => 'form-horizontal']) !!}
			{!! Form::hidden('_newUser', 1) !!}
				<div class="form-group{{ !$errors->first('userEmail') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						{!! Form::label('userEmail', 'Login Email', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-envelope"></i>Login Email</div>
							{!! Form::email('userEmail', null, ['class' => 'form-control', 'id' => 'newEmail', 'required' => 'required']) !!}
						</div>
						@if( $errors->first('userEmail') )
							<span class="help-block">{{ $errors->first('userEmail') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('userPassword') ? '' : ' has-errors has-feedback' }}">

					<div class="col-md-12">
					<div class="bg-primary text-center"><strong><small>This will be sent to the user by email and therefore should be a temporary password.</small></strong></div>
						{!! Form::label('userPassword', 'Password', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-key"></i>Password</div>
							{!! Form::password('userPassword', ['class' => 'form-control', 'id' => 'userPassword', 'required' => 'required']) !!}
						</div>
						@if($errors->first('userPassword'))
							<span class="help-block">{{ $errors->first('userPassword') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('userPassword_confirmation') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						{!! Form::label('userPassword_confirmation', 'Password Confirmation', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-key"></i>Password Confirmation</div>
							{!! Form::password('userPassword_confirmation', ['class' => 'form-control', 'id' => 'userPassword_confirmation', 'required' => 'required']) !!}
						</div>
						@if($errors->first('userPassword_confirmation') )
							<span class="help-block">{{ $errors->first('userPassword_confirmation') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('userFirstName') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						{!! Form::label('userFirstName', 'First Name', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<div class="input-group-addon">First Name</div>
							{!! Form::text('userFirstName', null, ['class' => 'form-control', 'id' =>'userFirstName', 'required' => 'required']) !!}
						</div>
						@if($errors->first('userFirstName'))
							<span class="help-block">{{ $errors->first('userFirstName') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('userLastName') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						{!! Form::label('userLastName', 'Last Name', ['class' => 'sr-only']) !!}
						<div class="input-group">
							<div class="input-group-addon">Last Name</div>
							{!! Form::text('userLastName', null, ['class' => 'form-control', 'id' => 'userLastName', 'required' => 'required']) !!}
						</div>
						@if($errors->first('userLastName'))
							<span class="help-block">{{ $errors->first('userPassword_confirmation') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('isActive') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						<div class="checkbox">
							<label>{!! Form::checkbox('isActive', '1', true) !!}This user is active</label>
						</div>
						@if( $errors->first("isActive"))
							<span class="help-block">{{ $errors->first('isActive') }}</span>
						@endif
					</div>
				</div>
				<div class="form-group{{ !$errors->first('isAdmin') ? '' : ' has-errors has-feedback' }}">
					<div class="col-md-12">
						<div class="checkbox">
							<label>{!! Form::checkbox('isAdmin', '1') !!}This user has administrative privileges</label>
						</div>
						@if( $errors->first("isAdmin") )
							<span class="help-block">{{ $errors->first('isAdmin') }}</span>
						@endif
					</div>
				</div>
				<input type="submit" class="btn btn-primary" value="Create New User" >
			{!!  Form::close() !!}
			</fieldset>
		</div>
	</div>
	@endif
</div>
@stop
@section('jsScript')
	@include('dashboard.common.dashboardjs')
	@include('dashboard.enterprise.accountjs')
@stop