<script src="/js/typeahead.bundle.min.js"></script>
<script src="/js/kegContents.js"></script>;
<script type="text/javascript">
	$(document).ready(function(){
		//event listener for What type of Keg do I have

		$( '.showKegTypes' ).click(function () {
		  if ( $( "#kegTypePics" ).is( ":hidden" ) ) {
		    $( "#kegTypePics" ).slideDown( "fast" );
		  } else {
		  	$('#kegTypePics').slideUp("fast");
		  }
		});

		$('#fullkeg').click(function () {
			$('#kegType option[value="1"]').prop('selected',true);
			$('#kegTypePics').slideUp("fast");
		});

		$('#quarterkeg').click(function () {
			$('#kegType option[value="2"]').prop('selected',true);
			$('#kegTypePics').slideUp("fast");
		});

		$('#cylinder').click(function () {
			$('#kegType option[value="3"]').prop('selected',true);
			$('#kegTypePics').slideUp("fast");
		});

		$('#ponykeg').click(function () {
			$('#kegType option[value="4"]').prop('selected',true);
			$('#kegTypePics').slideUp("fast");
		});

		$('#euro').click(function () {
			$('#kegType option[value="5"]').prop('selected',true);
			$('#kegTypePics').slideUp("fast");
		});

		$('#changeDist').click(function(){
			$('#dist-search').removeClass('hidden');
			$('#distSelected').addClass('hidden');
			$('#selectedDistID').val(0);
			$('#distType').val(0);
		});
 		//event listener for dist_brew
 		$('#distSearch').click(function(){
 			if($('#distSearchValue').val() == ''){
 				activateAlert('Please enter a value to search on');
 			}else{
 				var search_val=$('#distSearchValue').val();
 				search_val=search_val.trim();
 				var search_val = search_val.replace(/ /g, "ooooo");

 				var url = '/api/brewdist/'+$('input[name="distsearch"]:checked').val()+'/'+search_val;

 				$.getJSON(url, function(results){
 					var container = '';
 					var data = {};
 					var address = '';
 					var column = '';
 					var results = results.data;
 					$('#dist-modal .modal-body .row').empty();
 					for(var i = 0; i < results.length; i++){
 						data = {
 							'id' : results[i]['id'],
 							'type' : results[i]['type'],
 							'accountID' : results[i]['account_id']
 						};

 						container = $('<div>').data(data).addClass('chooseDistCont clickable box');
 						address = $('<address>');
 						if(results[i]['account_id'] != 0){
 							address.html('<i class="fa fa-bolt pull-right"></i>');
 						}
 						address.append('<strong>' + results[i]['name'] + '</strong><br />' + results[i]['address1'] + '<br />' + results[i]['address2'] + '<br />' + results[i]['city'] + ', ' + results[i]['stateCode'] + ' ' + results[i]['zipcode']);
 						container.append(address);
 						column = $('<div>').addClass('col-md-4');
 						column.append(container);
 						$('#dist-modal .modal-body .row').append(column);
 					}
 					$('#dist-modal').modal();
 					$('.chooseDistCont').click(function(){
						$('.chooseDistCont').removeClass('selected');
						$(this).addClass('selected');
					});
					$('#saveDist').click(function(){
						var dist = $('.selected').data();
						var address = $('.selected').html();
						$('#dist-search').addClass('hidden');
						$('#distSelected').removeClass('hidden');
						$('#selectedDistID').val(dist['id']);
						$('#distType').val(dist['type']);
						$('#distSelectedAddress').html(address);
						if(dist['account_id'] == 0){
							$("#distWarning").removeClass('hidden');
						}else{
							$('#distWarning').addClass('hidden');
						}
						$('#dist-modal').modal('hide');
					});
 				}).fail(function(jqXHR,textstatus){
 					activate_alert("Can't search for Distributors " + textstatus);
 				});
 			}
 		});/*close $(distSearch)*/


		//event listener for selection of kegs on table rows
		$('#setupTable tr').on('click',function(){
			var num = $(this).attr('id');
			num = num.split('-');
			ajaxKegs(num[1]);
		});//close tr click event listener

		//event listener for keg dropdown in device form
		$('#chooseKeg').on('change',function(){
			var num = $('#chooseKeg option:selected').val();

			if(typeof num != undefined && num != ''){
				ajaxKegs(num);
			}

		});//close chooseKeg event lister

		//event listener for Keg Contents change to fill Name
		$('#kegContents').on('blur', function(){
			if($('#contentsID').val() == '' || $('#contentsID').val() == 0){
				$('#contentsID').val(0);
				$('#kegContents').val('Not in database');
				activate_alert('Please contact your distributor and ask them to add your beer<br />', 'warning');
				//$('#distSearchContainer').addClass('hidden');
				$('#distSearchContainer').removeClass('hidden');
				$('#selectedDistID').val(0);
				$('#distType').val(0);
			}else{
				if($('#kegName').val() == ''){
					if($('#kegContents').val() != ''){
						$('#kegName').val($('#kegContents').val());

					}
				}
				$('#distSearchContainer').removeClass('hidden');
			}

			if($('#kegContents').val() == ''){
				$('#contentsID').val('');
			}

			$('#kegName').prop('disabled',false);
		});//close kegContents blur event listener

		//form validation
		$('input[name="submitDevice"]').on('click',function(){

			if($('#kegContents').val() == ''){
				$('#contentsID').val('');
			}

			return true;
		});//close input[name="submitDevice"] Validation
	});//close $(document).ready()

	/**
	* @function ajaxKegs 
	* Function for selecting kegdata through ajax
	* @param id
	*/
	function ajaxKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			var data = results.data;
						
			$('#chooseKeg option[value="'+data['id']+'"]').prop('selected',true);
			$('#contentsID').val(data['product_id']);
			$('#style').val(data['style']);
			$('#description').val(data['description']);

			/** Have to check to see if a beer has been set for this keg before retreiving the name */
			if(null !== data['beer']){
				$('#kegContents').val(data['beer']['name']);
			}
			
			if(data['distributor_id']==0 || data['distributor_id'] ==null) {
				$('#none_selected_chk').prop('checked', true);
			} else {
				$('#none_selected_chk').prop('checked', false);
			}


			$('#kegName').val(data['deviceName']);

			/** Show Device is a checkbox */
			if(data['showDevice'] == 0){
				$('#showDevice').prop('checked', true);
			}else if(data['showDevice'] == 1){
				$('#showDevice').prop('checked',false);
			}

			/** Keg Type is a select box*/
			if(data['kegType'] != 0){
				$('#kegType option[value="'+data['kegType']+'"]').prop('selected',true);
			}else{
				$('#kegType option[value=""]').prop('selected',true);
			}

			/** Prod Name can only be set once a product has been set */
			if(data['product_id'] != null){
				$('#kegName').prop('disabled',false);
				$('#distSearchContainer').removeClass('hidden');
			}else{
				$('#kegName').prop('disabled',true);

			}

			/** Distributor/Brewery */
			if((0 != data['distributor_id'] || 0 != data['brewery_id']) && (null !== data['distributor'] || null !== data['brewery'] )){

				if(0 != data['distributor_id']){
					$('#selectedDistID').val(data['distributor_id']);
					$('#distType').val('D');
					var address = $('<address>');
					var name = $('<strong>').text(data['distributor']['name']);
					address.append(name);
					address.append('<br>' + data['distributor']['distributor_address']['address1'] + '<br>');
					if(data['distributor']['distributor_address']['address2'].length){
						address.append(data['distributor']['distributor_address']['address2'] + '<br>');
					}
					address.append(data['distributor']['distributor_address']['city'] + ', ' + data['distributor']['distributor_address']['stateCode'] + ' ' + data['distributor']['distributor_address']['zipcode']);
				}else{
					$('#selectedDistID').val(data['brewery_id']);
					$('#distType').val('B');
					var address = $('<address>');
					var name = $('<strong>').text(data['brewery']['name']);
					address.append(name);
					address.append('<br>' + data['brewery']['brewery_address']['address1'] + '<br>');
					if(data['brewery']['brewery_address']['address2'].length){
						address.append(data['brewery']['brewery_address']['address2'] + '<br>');
					}
					address.append(data['brewery']['brewery_address']['city'] + ', ' + data['brewery']['brewery_address']['stateCode'] + ' ' + data['brewery']['brewery_address']['zipcode']);
				}
				
				
				
				$('#distSelectedAddress').html(address);
				$('#distSearchContainer').removeClass('hidden');
				$('#dist-search').addClass('hidden');
				$('#distSelected').removeClass('hidden');
			}else{
				$('#distSelectedAddress').html('');
				$('#distSearchContainer').removeClass('hidden');
				$('#dist-search').removeClass('hidden');
				$('#distSelected').addClass('hidden');
			}
		}).fail(function() {
			activate_alert('Could not load keg');
		});

	}
</script>




