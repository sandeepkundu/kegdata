<html>
	<head>
		<title>KegData Report</title>
	</head>
	<body>
		<table class="table table-bordered">
			<!-- START ADDRESS/PRODUCTS ROWS -->
			<thead>
				<tr>
					<th>When</th>
					<th>Ounces Poured</th>
				</tr>
			</thead>
			<tbody>
			  <?php $i=0; ?>
			  @foreach($reportData as $data)
			  @if($data->showDevice)
			  <?php
			    
			    $calibration =  $kegdata->getCalibratedValue($data->kegMac);
			    $ounces = $kegdata->getOunces($data->adc_pour_end, $calibration, $data->amount, $data->empty, $data->volumeType);
			    
			    //fetch flow rate
			     $lsm_record=$kegdata->get_most_recent_data_for_fetch_lsm_value($data->deviceID,$data->sent_at);
			     
			     $m_adc=isset($lsm_record->m) ? $lsm_record->m : 0.15;
			     $flow_rate = (($data->amount)/($data->empty-350))*($m_adc);
			     $exact_value = $flow_rate * $data->pour_length;
			     $exact_value = round($exact_value);
			  ?>

			  <tr>
			    <td>{{ $data->sent_at->timezone($timezone->timezone)->toDayDateTimeString() }}</td>
			    <td>{{ $exact_value." ".$data->volumeType }}</td>
			  </tr>
			  @endif
			  <?php $i++; ?>
			  @endforeach
			    <?php 
			    if(empty($i)) {
			      echo '<tr><td colspan="2"><p style="text-align:center;color:red;">No results found with this time frame!</p></td></tr>';
			    }
			    ?>
			  </tbody>
		</table>
	</body>
</html>
