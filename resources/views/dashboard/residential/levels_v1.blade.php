

@section('title')
{{ Config::get('constants.COMPANY_NAME') }} Keg Levels
@stop
@section('tabs')
<li class="active"><a href="/dashboard/levels">Keg Levels</a></li>
<li><a href="/dashboard/reports">Charts & Graphs<!-- Reports --></a></li>
@if($user->isAdmin)
<li><a href="/dashboard/notifications">Notifications</a></li>
<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
<li><a href="/dashboard/settings">Settings</a></li>
@endif
<li><a href="/dashboard/account">Account Management</a></li>
@stop
<style>
   .newtable {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   .newtable td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
   }
   .newtable tr:nth-child(even) {
   background-color: #dddddd;
   }
</style>
@section('tabpane')
<div class="col-md-12">
   <div class="row">
      <nav class="nav-bar nav-bar-default">
         <div class="container-fluid">
            <div class="btn-group btn-group-sm">
               <!--button class="btn btn-primary navbar-btn" id="collapse-remaining">Collapse Remaining Amounts</button -->
               <!--<button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button>
                  <button class="btn btn-primary navbar-btn" id="show-tempcharts">Show Temperature Charts</button>
                  <button class="btn btn-primary navbar-btn" id="show-pourcharts">Show Pour Charts</button> 
                  <button class="btn btn-primary navbar-btn" id="show-levelcharts">Show Level Charts</button>-->
               <!-- <button class="btn btn-primary navbar-btn" id="show-treadlinecharts">Trendline</button> -->
            </div>
            <!--
               <form class="navbar-form navbar-right" role="search">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                      </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                   </form>-->
         </div>
      </nav>
   </div>
   <div class="row" id="dbPanels">
      <?php $count = 0; ?>
      @foreach($kegs as $keg)
      @if($keg->showDevice)
      <?php //dd($keg->recentPour); ?>
      <div class="col-sm-3">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title text-center">{{ $keg->deviceName }}</h3>
            </div>
            <div class="panel-body">
               <div class="levels">
                  <h4 class="text-center">Temperature: {{ is_null($keg->temperature) ? 'NO DATA' : $keg->temperature->tempFormatted  }}&deg; {{ $settings->temperatureType }}</h4>

                  <div class="keg-pic text-center">
                     <img src="/img/levels/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}{{  (!empty($keg->recentPour)) ? $keg->image: '00' }}.jpg" alt="{{  (!empty($keg->recentPour)) ? $keg->recentPour->percentage : 0 }}% Beer Left" title="{{ (!empty($keg->recentPour)) ? $keg->recentPour->percentage : 0 }}% Beer Left" height="150px"/>
                  </div>
                  <table class="table table-hover table-condensed small table-levels">
                     <thead>
                        <tr>
                           <th colspan="2" class="text-center" data-toggle="tbody{{ $count }}">Remaining Amount </th>
                        </tr>
                     </thead>
                     <tbody id="tbody{{ $count }}" style="display:block;">
                        <tr>
                           <th>Contents</th>
                           <td><strong>{{ isset($keg->beer->name) ? $keg->beer->name : 'Set beer in Keg Setup' }}</strong></td>
                        </tr>
                      <!--   <tr>
                           <th>Beer remaining*</th>
                           <td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->total : '0' }}</th>
                        </tr> -->
                         <tr>
                           <th>Beer remaining </th>
                           <td><?php  (!empty( $keg->beerlft)) ? print_r( number_format($keg->beerlft,2)." ".$keg->volumeType) :   print_r("--"); ?></td>
                        </tr>
                        <tr>
                           <th>Percentage remaining*</th>

                           <td>{{  (!empty($keg->recentPour)) ? $keg->recentPour->percentage : '0' }}%</td>
                        </tr>
                        <?php  

                        


                        $level_text_one = '';
                        $level_text_two = '';
                        $level_volume_one=0;
                        $level_volume_two=0;

                           switch ($keg->volumeType) {
                               case "oz":
                                   $level_text_one= '16 oz pours remaining*';
                                   $level_text_two= '12 oz pours remaining*';

                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/16,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/12,2);
                                   break;
                               case "ml":
                                  $level_text_one= '500 ml pours remaining*';
                                   $level_text_two= '1000 ml pours remaining*';
                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/500,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/1000,2);
                                   break;
                               
                               default:
                                   $level_text_one= '16 oz pours remaining*';
                                   $level_text_two= '12 oz pours remaining*';

                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/16,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/12,2);
                           }
                            
                           ?>
                        <tr>
                           <th>{{ $level_text_one  }}</th>
                           <td>{{  $level_volume_one }}</td>
                        </tr>
                        <tr>
                           <th>{{ $level_text_two  }}</th>
                           <td>{{  $level_volume_two }}</td>
                        </tr>
                        <tr>
                           <th>Tapped on</th>
                           <td>{{ (!empty($keg->tapped)) ? $keg->tapped->sent_at->toDayDateTimeString() : 'No Tapped Date Available' }}</td>
                        </tr>
                        <tr>
                           <th>Last pour on</th>
                           <td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->sent_at->toDayDateTimeString() : 'No pour time' }}</td>
                        </tr>
                       <!--  <tr>
                           <th>Most Recent Break Event</th>
                           <?php 
                           //$convertedDate = $utills->changeTimeZone($keg->lastBreak);
                           ?>
                           <td>{{ (!empty($convertedDate)) ? $convertedDate->toDayDateTimeString() : 'No pour time' }}</td>
                        </tr> -->
                    <!--   <tr>
                           <th> ADC Flow Rate </th>  -->
                           <?php 
                              //fetch second step adc ma nd b values
                              $adc_m_value =isset($keg->lms_data_for_adc_end['m'])?
                             $keg->lms_data_for_adc_end['m'] : 0;
                            ?>
                           <!---td>{{ (!empty($keg->lms_data )) ?  number_format (number_format(-10 * $keg->lms_data['m']),2)  : '--' }}</td-->
                        <!--   <td>{{ number_format($adc_m_value,3) }}</td>
                        </tr>  -->

                         <tr>
                           <th>Flow Rate </th> 
                           <?php 
                              
                            ?>
                           <!---td>{{ (!empty($keg->lms_data )) ?  number_format (number_format(-10 * $keg->lms_data['m']),2)  : '--' }}</td-->
                           <?php
                           	$new_flow_rate=(!empty($keg->new_flow_rate) || $keg->new_flow_rate != '0.0') ? number_format($keg->new_flow_rate *10,2) : '0.0';
                           ?>
                           <td>{{ $new_flow_rate." ".$keg->volumeType }}</td>
                        </tr> 

                       <!--  <tr>
                           <th>ADC B</th>
                           td>{{ (!empty($keg->lms_data )) ? number_format($keg->lms_data['b'],2) ." ".$keg->volumeType  : '--' }}</td
                           <td>{{ number_format($keg->actual_value_b_from_step_2,2)}}</td>
                        </tr> -->
                     <!--    <tr>
                           <th>Value of B (S1)</th>
                           td>{{ (!empty($keg->lms_data )) ? number_format($keg->lms_data['b'],2) ." ".$keg->volumeType  : '--' }}</td
                           <td>{{ number_format($keg->lms_data['b'],2)}}</td>
                        </tr> -->
                       <!--  <tr>
                           <th>Value Of b </th>
                           td>{{ (!empty($keg->lms_data )) ? number_format($keg->lms_data['b'],2) ." ".$keg->volumeType  : '--' }}</td
                           <td>{{ number_format($keg->new_value_of_b,2)}}</td>
                        </tr>
 -->                        <!-- <tr>
                           <th>Beer remaining using LSM</th>
                           <td></td>
                        </tr> -->
                       
                     </tbody>
                     </tfoot>
                     <tr>
                        <td colspan="2"><small>*These are calculated values, actual amounts could vary slightly</small></td>
                     </tr>
                     </tfoot>
                  </table>
                  <!-- New table for lms -->
                  <!-- New table end  -->
               </div>
               <div class="hide tempChart">
                  <canvas id="tempChart{{ $keg->id }}" width="300px" height="300px"></canvas>
               </div>
               <div class="hide pourChart">
                  <canvas id="pourChart{{ $keg->id }}" width="300px" height="300px"></canvas>
               </div>
               <!-- Data From New LSM Table -->
               <table class="newtable" style=" display:none; font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;" >
                  <thead>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;" >Date</th>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;">m</th>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;">b</th>
                  <thead>
                     @foreach( $keg->allLsmData as $lsmData )
                     <tr style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;" title="Comm_x ={{ (!empty($lsmData->simple_x )) ? $lsmData->simple_x  : '--'   }}">
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;"> {{ (!empty($lsmData->break_event_date)) ? $lsmData->break_event_date : '--'   }}</td>
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;">{{ (!empty($lsmData->m )) ? number_format($lsmData->m ,2)  : '--'   }}</td>
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;">{{ (!empty($lsmData->b)) ? number_format($lsmData->b,2)  : '--'   }}</td>
                        <!-- <td>{{  substr($lsmData->comm_x,0 ,5)  }}</td> -->
                     <tr>
                        <?php 
                           $simple_x_array = json_decode($lsmData->simple_x);
                           
                           $t=0;
                           $temp=[];
                           $data_arr=$simple_x_array->pour_x;
                           for ($i=0;$i < (count($data_arr));$i++) {
                           
                           		$t=$t+$data_arr[$i];
                           		$temp[]=$t;
                           }
                           
                           ?>
                     <tr >
                        <td>
                           <p><?php echo json_encode($temp); ?></p>
                        </td>
                        <td ></td>
                        <td></td>
                     </tr>
                     @endforeach
               </table>
               <!-- 	<div class="hide levelChart">
                  <canvas id="levelChart{{ $keg->id }}" width="300px" height="300px"></canvas>
                  </div> -->
            </div>
            <!--div class="panel-footer text-center">
               <small><small>Keg ID:</small><small>{{ $kegdata->getFormattedMac($keg->kegMac) }}</small><br /><small>Calibration Calculation {{ $keg->calibration }}</small></small >
               </div -->
         </div>
      </div>
      @endif
      @endforeach
      <!--small>*All levels are approximate values</small-->
   </div>
   <div class="treadlineCharts">
   </div>
</div>
@stop 
<?php $newGraph = 0 ;?>
@section('jsScript')
@include('dashboard.common.dashboardjs_newlsm')
@include('dashboard.residential.levelsjs_newlsm', array('kegs' => $kegs,'newGraph'=>$newGraph))
@stop

