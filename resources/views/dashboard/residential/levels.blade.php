@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Keg Levels
@stop

@section('tabs')
  	<li class="active"><a href="/dashboard/levels">Keg Levels</a></li>
  	<li><a href="/dashboard/reports">Reports</a></li>
  	@if($user->isAdmin)
  	<li><a href="/dashboard/notifications">Notifications</a></li>
  	<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
	<li><a href="/dashboard/settings">Settings</a></li>
	@endif
	<li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')
	<div class="col-md-12">
	<div class="row">
		<nav class="nav-bar nav-bar-default">
			<div class="container-fluid">
				<div class="btn-group btn-group-sm">
					<!--button class="btn btn-primary navbar-btn" id="collapse-remaining">Collapse Remaining Amounts</button -->
					<!--<button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button>
					 <button class="btn btn-primary navbar-btn" id="show-tempcharts">Show Temperature Charts</button>
					<button class="btn btn-primary navbar-btn" id="show-pourcharts">Show Pour Charts</button> 
					<button class="btn btn-primary navbar-btn" id="show-levelcharts">Show Level Charts</button>-->
                    <!-- <button class="btn btn-primary navbar-btn" id="show-treadlinecharts">Trendline</button> -->
				</div>
                                    
                                
				<!--
				<form class="navbar-form navbar-right" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default">Submit</button>
			     </form>-->
		    </div>
		</nav>
		</div>
		<div class="row" id="dbPanels">
		<?php $count = 0; ?>
			@foreach($kegs as $keg)
			@if($keg->showDevice)

			<?php //dd($keg->recentPour); ?>
			<div class="col-sm-3">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title text-center">{{ $keg->deviceName }}</h3></div>
						<div class="panel-body">
							<div class="levels">
								<h4 class="text-center">Temperature: {{ is_null($keg->temperature) ? 'NO DATA' : $keg->temperature->tempFormatted  }}&deg; {{ $settings->temperatureType }}</h4>
								<div class="keg-pic text-center">
									<img src="/img/levels/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}{{  (!empty($keg->recentPour)) ? $keg->image: '00' }}.jpg" alt="{{  (!empty($keg->recentPour)) ? $keg->recentPour->percentage : 0 }}% Beer Left" title="{{ (!empty($keg->recentPour)) ? $keg->recentPour->percentage : 0 }}% Beer Left" height="150px"/>
								</div>
								<table class="table table-hover table-condensed small table-levels">
									<thead>
										<tr>
											<th colspan="2" class="text-center" data-toggle="tbody{{ $count }}">Remaining Amount </th>
										</tr>
									</thead>
									<tbody id="tbody{{ $count }}" style="display:block;">
										<tr>
											<th>Contents</th><td><strong>{{ isset($keg->beer->name) ? $keg->beer->name : 'Set beer in Keg Setup' }}</strong></td>
										</tr>
										<tr>
											<th>Beer remaining*</th><td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->total : '0' }}</th>
										</tr>
										<tr>
											<th>Percentage remaining*</th><td>{{  (!empty($keg->recentPour)) ? $keg->recentPour->percentage : '0' }}%</td>
										</tr>
										<tr>
											<th>16 oz pours remaining*</th><td>{{  (!empty($keg->recentPour)) ? round($keg->recentPour->ounces/16) : '0' }}</td>
										</tr>
										<tr>
											<th>12 oz pours remaining*</th><td>{{  (!empty($keg->recentPour)) ? round($keg->recentPour->ounces/12) : '0' }}</td>
										</tr>
										<tr>
											<th>Tapped on</th><td>{{ (!empty($keg->tapped)) ? $keg->tapped->sent_at->toDayDateTimeString() : 'No Tapped Date Available' }}</td>
										</tr>
										<tr>
											<th>Last pour on</th><td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->sent_at->toDayDateTimeString() : 'No pour time' }}</td>
										</tr>
										<tr>
											<th>Most Recent Break Event</th><td>{{ (!empty($keg->lastBreak)) ? $keg->lastBreak->toDayDateTimeString() : 'No pour time' }}</td>
										</tr>
										<tr>
											
											<th>Flow Rate {{ $keg->volumeType }}/sec </th><td>{{ (!empty($keg->lms_data )) ?  number_format (number_format(-10 * $keg->lms_data['m']),2)  : '--' }}</td>
										</tr>
										<tr>
											<th>Value Of b </th><td>{{ (!empty($keg->lms_data )) ? number_format($keg->lms_data['b'],2)  : '--' }}</td>
										</tr>
										<tr>
											<th>Beer remaining using LSM</th><td><?php  (!empty($keg->value_y )) ? print_r(number_format($keg->value_y,2)." ".$keg->volumeType) :   print_r("--"); ?></td>
										</tr>


									</tbody>
									</tfoot>
										<tr>
											<td colspan="2"><small>*These are calculated values, actual amounts could vary slightly</small></td>
										</tr>
									</tfoot>
								</table>
								
							</div>
							<div class="hide tempChart">
								<canvas id="tempChart{{ $keg->id }}" width="300px" height="300px"></canvas>
							</div>
							<div class="hide pourChart">
								<canvas id="pourChart{{ $keg->id }}" width="300px" height="300px"></canvas>
							</div>
						<!-- 	<div class="hide levelChart">
								<canvas id="levelChart{{ $keg->id }}" width="300px" height="300px"></canvas>
							</div> -->
                                                            
                                                        
						</div>
                                                    
                                                    
						<!--div class="panel-footer text-center">
							<small><small>Keg ID:</small><small>{{ $kegdata->getFormattedMac($keg->kegMac) }}</small><br /><small>Calibration Calculation {{ $keg->calibration }}</small></small >
						</div -->
					</div>
				</div>
			@endif
			@endforeach

		<!--small>*All levels are approximate values</small-->
		</div>
                    
                <div class="treadlineCharts">
                           
                </div>    
                    
	</div>
@stop 
<?php $newGraph = 0 ;?>
@section('jsScript')
	@include('dashboard.common.dashboardjs')
	@include('dashboard.residential.levelsjs', array('kegs' => $kegs,'newGraph'=>$newGraph))
@stop