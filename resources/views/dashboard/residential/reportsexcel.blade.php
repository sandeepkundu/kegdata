<html>
	<head>
		<title>KegData Report</title>
	</head>
	<body>
		<table class="table table-bordered">
			<!-- START ADDRESS/PRODUCTS ROWS -->
			<thead>
				<tr>
					<th>When</th>
					<th>Keg Mac</th>
					<th>Device Name</th>
					<th>Event</th>
					<th>Temperature</th>
					<th>Beer Remaining</th>
					<th>Pour Length</th>
				</tr>
			</thead>
			<tbody>
			  <?php $i=0; ?>
			  @foreach($reportData as $data)
			  @if($data->showDevice)
			  <?php
			    
			    $calibration =  $kegdata->getCalibratedValue($data->kegMac);
			   

			    $ounces = $kegdata->getOunces($data->adc_pour_end, $calibration, $data->amount, $data->empty, $data->volumeType);
			    
			    $percentage = $kegdata->kegPercent($ounces, $data->amount); 
			    
			  ?>

			  <tr{{ ($data->status == '101') ? ' class=danger' : '' }}>
			    <td>{{ $data->sent_at->timezone($timezone->timezone)->toDayDateTimeString() }}</td>
			    <td>{{ $kegdata->getFormattedMac($data->kegMac) }}</td>
			    <td>{{ $data->deviceName }}</td>
			    <td>@if($data->status == '11')
			      Pour Event
			              @elseif($data->status == '101')
			                Untap Event
			              @elseif($data->status == '1000')
			                Keg Blow
			              @elseif($data->status == '1001')
			                Heartbeat
			              @else
			                Boot Event
			              @endif
			    </td>
			    <td><?php echo number_format((float)$kegdata->getTemp($data->temperature, $data->temperatureType, $data->tempOffset ), 2, '.', ''); ?> &deg; {{ $data->temperatureType }}</td>

			    <?php
			    //if(file_exists(public_path().'/img/levels/'.$data->pic_prefix.'/'.$data->pic_prefix.$percentage.'.jpg')) { 
			    ?>
			    <!--TD><img src="{{ public_path().'/img/levels/'.$data->pic_prefix.'/'.$data->pic_prefix.$percentage.'.jpg' }}" width="50px" height="50px" alt="{{$percentage .'%'}}" /></TD-->
				<td>{{ @$ounces." ".@$data->volumeType }}</td>
			    <td>{{ $data->pour_length }}</td>
			  </tr>
			  @endif
			  <?php $i++; ?>
			  @endforeach
			    <?php 
			    if(empty($i)) {
			      echo '<tr><td colspan="7"><p style="text-align:center;color:red;">No results found with this time frame!</p></td></tr>';
			    }
			    ?>
			  </tbody>
		</table>
	</body>
</html>
