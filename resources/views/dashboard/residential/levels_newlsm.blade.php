

@section('title')
{{ Config::get('constants.COMPANY_NAME') }} Keg Levels
@stop
@section('tabs')
<li class="active"><a href="/dashboard/levels">Keg Levels</a></li>
<li><a href="/dashboard/graph_reports?chart=pour">Charts & Graphs<!-- Reports --></a></li>
@if($user->isAdmin)
<li><a href="/dashboard/notifications">Notifications</a></li>
<li><a href="/dashboard/kegsetup">Keg Setup</a></li>
<li><a href="/dashboard/settings">Settings</a></li>
@endif
<li><a href="/dashboard/account">Account Management</a></li>
@stop
<style>
   .newtable {
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   }
   .newtable td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px;
   }
   .newtable tr:nth-child(even) {
   background-color: #dddddd;
   }
</style>
@section('tabpane')
<div class="col-md-12">
   <div class="row">
      <nav class="nav-bar nav-bar-default">
         <div class="container-fluid">
            <div class="btn-group btn-group-sm">
               <!--button class="btn btn-primary navbar-btn" id="collapse-remaining">Collapse Remaining Amounts</button -->
               <!--<button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button>
                  <button class="btn btn-primary navbar-btn" id="show-tempcharts">Show Temperature Charts</button>
                  <button class="btn btn-primary navbar-btn" id="show-pourcharts">Show Pour Charts</button> 
                  <button class="btn btn-primary navbar-btn" id="show-levelcharts">Show Level Charts</button>-->
               <!-- <button class="btn btn-primary navbar-btn" id="show-treadlinecharts">Trendline</button> -->
            </div>
            <!--
               <form class="navbar-form navbar-right" role="search">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                      </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                   </form>-->
         </div>
      </nav>
   </div>
   <div class="row" id="dbPanels">
      <?php $count = 0; ?>
      @foreach($kegs as $keg)
      @if($keg->showDevice)
      <?php //dd($keg->recentPour); ?>
      <div class="col-sm-3">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title text-center">{{ $keg->deviceName }}</h3>
            </div>
            <div class="panel-body">
               <div class="levels">
                  <h4 class="text-center">Temperature: {{ is_null($keg->temperature) ? 'NO DATA' : $keg->temperature->tempFormatted  }}&deg; {{ $settings->temperatureType }}</h4>

                  <div class="keg-pic text-center">
                     <img src="/img/levels/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}/{{ (!empty($keg->recentPour)) ? $keg->recentPour->pic_prefix :'keg' }}{{  (!empty($keg->recentPour)) ? $keg->image: '00' }}.jpg" alt="{{  (!empty($keg->recentPour)) ? round($keg->recentPour->percentage) : 0 }}% Beer Left" title="{{ (!empty($keg->recentPour)) ? round($keg->recentPour->percentage) : 0 }}% Beer Left" height="150px"/>
                  </div>
                  <table class="table table-hover table-condensed small table-levels">
                     <thead>
                        <tr>
                           <th colspan="2" class="text-center" data-toggle="tbody{{ $count }}">Remaining Amount </th>
                        </tr>
                     </thead>
                     <tbody id="tbody{{ $count }}" style="display:block;">
                        <tr title="{{ isset($keg->beer->name) ? $keg->beer->name : 'Set beer in Keg Setup' }}" style="height: 55px;">
                           <th>Contents</th>
                           <td><strong>{{ isset($keg->beer->name) ? substr($keg->beer->name,0,22) : 'Set beer in Keg Setup' }}</strong></td>
                        </tr>
                        <tr title="{{ isset($keg->style_name->name) ? $keg->style_name->name : '--' }}">
                           <th>Beer Style</th>
                            <td><strong>{{ isset($keg->style_name->name) ? substr($keg->style_name->name,0,15) : '--' }}</strong></td>
                        </tr>
                        <tr title="{{ isset($keg->description) ? $keg->description : '--' }}" style="height: 55px;">
                           <th>Beer Description</th>
                            <td><strong>{{ isset($keg->description) ? substr($keg->description,0,20) : '--' }}</strong></td>
                        </tr>
                      <!--   <tr>
                           <th>Beer remaining*</th>
                           <td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->total : '0' }}</th>
                        </tr> -->
                         <tr>
                           <th>Beer remaining </th>
                           <td><?php  
                           $var_1 = strtotime($keg->tapped->sent_at);
                           $var_2 = strtotime($keg->tapped->second_var);
                           $result='';
                           $result_2='';
                           if($var_2 > $var_1) {
                             $result = round($keg->beerlft)." ".$keg->volumeType;
                             $result_2 = $keg->tapped->second_var->toDayDateTimeString();
                           } else {
                              if($keg->getLastDataEvent->status == "101")
                              {
                                $result='Untapped';
                                $result_2='Untapped';
                              } else {
                                $result='Calibrating';
                                $result_2='Calibrating';
                              }
                           }

                           //$result = (!empty( $keg->beerlft)) ?  round($keg->beerlft)." ".$keg->volumeType :  (($keg->getLastDataEvent->status == "101" ) ?  "Untapped" : "Calibrating" ) ;
                           echo $result; 

                           ?></td>
                        </tr>
                        <tr>
                           <th>Percent<br> remaining*</th>

                           <td>{{  (!empty($keg->recentPour)) ? round($keg->recentPour->percentage) : '0' }}%</td>
                        </tr>
                        <?php 
                        $level_text_one = '';
                        $level_text_two = '';
                        $level_volume_one=0;
                        $level_volume_two=0;

                           switch ($keg->volumeType) {
                               case "oz":
                                   $level_text_one= '16 oz pours remaining*';
                                   $level_text_two= '12 oz pours remaining*';

                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/16,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/12,2);
                                   break;
                               case "ml":
                                  $level_text_one= '500 ml pours remaining*';
                                   $level_text_two= '1000 ml pours remaining*';
                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/500,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/1000,2);
                                   break;
                               
                               default:
                                   $level_text_one= '16 oz pours remaining*';
                                   $level_text_two= '12 oz pours remaining*';

                                   $level_volume_one=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/16,2);

                                   $level_volume_two=(empty($keg->beerlft)) ? '--': number_format($keg->beerlft/12,2);
                           }
                            
                           ?>
                        <tr>
                           <th>{{ $level_text_one  }}</th>
                           <td>{{  round($level_volume_one) }}</td>
                        </tr>
                        <tr>
                           <th>{{ $level_text_two  }}</th>
                           <td>{{  round($level_volume_two) }}</td>
                        </tr>
                        <tr style="height: 55px;">
                           <th>Tapped on</th>
                           <td>
                           <?php
                           if($keg->getLastDataEvent->status == "101"){
                            $show_tapped='Untapped';
                           } else {
                              $show_tapped=(!empty($keg->tapped)) ? $keg->tapped->sent_at->toDayDateTimeString() : 'No Tapped Date Available';
                           }
                           ?>
                           {{ $show_tapped }}
                           </td>
                        </tr>
                        <!--tr>
                           <th>Updated Tapped On</th>
                           <td>
                           {{ $result_2 }}
                           </td>
                        </tr-->
                        <tr style="height: 55px;">
                           <th>Last pour on</th>
                           <td>{{ (!empty($keg->recentPour)) ? $keg->recentPour->sent_at->toDayDateTimeString() : 'No pour time' }}</td>
                        </tr>

                         <tr>
                           <th>Flow Rate </th> 
                           <?php
                           	$new_flow_rate=(!empty($keg->new_flow_rate) || $keg->new_flow_rate != '0.0') ? number_format($keg->new_flow_rate *10,2) : '0.0';
                           ?>
                           <td>{{ $new_flow_rate." ".$keg->volumeType }}<br> per second</td>
                        </tr> 
                       
                     </tbody>
                     </tfoot>
                     <tr>
                        <td colspan="2"><small>*These are calculated values, actual amounts could vary slightly. {{substr($keg->kegMac,-4)}}</small></td>
                     </tr>
                     </tfoot>
                  </table>
                  <!-- New table for lms -->
                  <!-- New table end  -->
               </div>
               <div class="hide tempChart">
                  <canvas id="tempChart{{ $keg->id }}" width="300px" height="300px"></canvas>
               </div>
               <div class="hide pourChart">
                  <canvas id="pourChart{{ $keg->id }}" width="300px" height="300px"></canvas>
               </div>
               <!-- Data From New LSM Table -->
               <table class="newtable" style=" display:none; font-family: arial, sans-serif;
                  border-collapse: collapse;
                  width: 100%;" >
                  <thead>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;" >Date</th>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;">m</th>
                     <th style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;">b</th>
                  <thead>
                     @foreach( $keg->allLsmData as $lsmData )
                     <tr style=" border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;" title="Comm_x ={{ (!empty($lsmData->simple_x )) ? $lsmData->simple_x  : '--'   }}">
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;"> {{ (!empty($lsmData->break_event_date)) ? $lsmData->break_event_date : '--'   }}</td>
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;">{{ (!empty($lsmData->m )) ? number_format($lsmData->m ,2)  : '--'   }}</td>
                        <td style=" border: 1px solid #dddddd;
                           text-align: left;
                           padding: 8px;">{{ (!empty($lsmData->b)) ? number_format($lsmData->b,2)  : '--'   }}</td>
                        <!-- <td>{{  substr($lsmData->comm_x,0 ,5)  }}</td> -->
                     <tr>
                        <?php 
                           $simple_x_array = json_decode($lsmData->simple_x);
                           
                           $t=0;
                           $temp=[];
                           $data_arr=isset($simple_x_array->pour_x) ? $simple_x_array->pour_x : null;
                           for ($i=0;$i < (count($data_arr));$i++) {
                           
                           		$t=$t+$data_arr[$i];
                           		$temp[]=$t;
                           }
                           
                           ?>
                     <tr >
                        <td>
                           <p><?php echo json_encode($temp); ?></p>
                        </td>
                        <td ></td>
                        <td></td>
                     </tr>
                     @endforeach
               </table>
               <!-- 	<div class="hide levelChart">
                  <canvas id="levelChart{{ $keg->id }}" width="300px" height="300px"></canvas>
                  </div> -->
            </div>
            <!--div class="panel-footer text-center">
               <small><small>Keg ID:</small><small>{{ $kegdata->getFormattedMac($keg->kegMac) }}</small><br /><small>Calibration Calculation {{ $keg->calibration }}</small></small >
               </div -->
         </div>
      </div>
      @endif
      @endforeach
      <!--small>*All levels are approximate values</small-->
   </div>
   <div class="treadlineCharts">
   </div>
</div>
@stop 
<?php $newGraph = 0 ;?>
@section('jsScript')
@include('dashboard.common.dashboardjs_newlsm')
@include('dashboard.residential.levelsjs_newlsm', array('kegs' => $kegs,'newGraph'=>$newGraph))
@stop

