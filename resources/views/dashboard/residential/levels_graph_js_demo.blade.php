<script type='text/javascript' src='/js/Chart.min.js'></script>
<script type='text/javascript' src='/js/moment.min.js'></script>
<script type='text/javascript' src='/js/bootstrap-datetimepicker.min.js'></script>
<script src="https://code.highcharts.com/highcharts.js" type="text/javascript"></script>
<script type="text/javascript">
var level_setting="{!! $level_setting !!}";
var level_settin_temp = "{!! $level_setting_temp !!}";
Chart.defaults.global = {
	    // Boolean - Whether to animate the chart
	    animation: true,
	    // Number - Number of animation steps
	    animationSteps: 60,
	    // String - Animation easing effect
	    animationEasing: "easeOutQuart",
	    // Boolean - If we should show the scale at all
	    showScale: true,
	    // Boolean - If we want to override with a hard coded scale
	    scaleOverride: false,
	    // ** Required if scaleOverride is true **
	    // Number - The number of steps in a hard coded scale
	    scaleSteps: null,
	    // Number - The value jump in the hard coded scale
	    scaleStepWidth: null,
	    // Number - The scale starting value
	    scaleStartValue: null,
	    // String - Colour of the scale line
	    scaleLineColor: "rgba(0,0,0,.1)",
	    // Number - Pixel width of the scale line
	    scaleLineWidth: 1,
	    // Boolean - Whether to show labels on the scale
	    scaleShowLabels: true,
	    // Interpolated JS string - can access value
	    scaleLabel: "<%=value%>",
	    // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
	    scaleIntegersOnly: true,
	    // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	    scaleBeginAtZero: false,
	    // String - Scale label font declaration for the scale label
	    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Scale label font size in pixels
	    scaleFontSize: 12,
	    // String - Scale label font weight style
	    scaleFontStyle: "normal",
	    // String - Scale label font colour
	    scaleFontColor: "#666",
	    // Boolean - whether or not the chart should be responsive and resize when the browser does.
	    responsive: true,
	    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	    maintainAspectRatio: true,
	    // Boolean - Determines whether to draw tooltips on the canvas or not
	    showTooltips: true,
	    // Array - Array of string names to attach tooltip events
	    tooltipEvents: ["mousemove", "touchstart", "touchmove"],
	    // String - Tooltip background colour
	    tooltipFillColor: "rgba(0,0,0,0.8)",
	    // String - Tooltip label font declaration for the scale label
	    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Tooltip label font size in pixels
	    tooltipFontSize: 14,
	    // String - Tooltip font weight style
	    tooltipFontStyle: "normal",
	    // String - Tooltip label font colour
	    tooltipFontColor: "#fff",
	    // String - Tooltip title font declaration for the scale label
	    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Tooltip title font size in pixels
	    tooltipTitleFontSize: 14,
	    // String - Tooltip title font weight style
	    tooltipTitleFontStyle: "bold",
	    // String - Tooltip title font colour
	    tooltipTitleFontColor: "#fff",
	    // Number - pixel width of padding around tooltip text
	    tooltipYPadding: 6,
	    // Number - pixel width of padding around tooltip text
	    tooltipXPadding: 6,
	    // Number - Size of the caret on the tooltip
	    tooltipCaretSize: 8,
	    // Number - Pixel radius of the tooltip border
	    tooltipCornerRadius: 6,
	    // Number - Pixel offset from point x to tooltip edge
	    tooltipXOffset: 10,
	    // String - Template string for single tooltips
	   // tooltipTemplate: "<%if (label){%><%=label%> - <%}%><%= value %> oz" ,
	    // String - Template string for single tooltips
	    multiTooltipTemplate: "<%= value %>",
	    // Function - Will fire on animation progression.
	    onAnimationProgress: function(){},
	    // Function - Will fire on animation completion.
	    onAnimationComplete: function(){}
	};
	</script>

	<script type="text/javascript">
	
	$(document).ready(function(){


		/* Check if chart is pour chatr*/
		var chart_type  = getUrlParameter('chart');
		//alert(chart_type);
		/*if(chart_type == "pour"){
			$('#new-graph-limit').hide();
		}*/


		var url=$(location).attr('href');
				var default_load_map_var="{!! $ident_hdn !!}";
		//variable to check that form is submit or not
		var is_form_submit="{!! $newGraph !!}";

		if(is_form_submit==1) {
			if(default_load_map_var=='temp') {
				setTimeout(function(){ 
					$('#show-tempcharts').trigger('click');
				}, 10);
			} else if(default_load_map_var=='pour') {

				setTimeout(function(){ 
					$('#show-pourcharts').trigger('click');
				}, 10);
			}
}



		/*if(default_load_map_var=='level') {
			setTimeout(function(){ 
				$('#show-levelcharts').trigger('click');
			 }, 10);
		} else if(default_load_map_var=='temp') {
			setTimeout(function(){ 
				$('#show-tempcharts').trigger('click');
			 }, 10);
		} else if(default_load_map_var=='pour') {
			setTimeout(function(){ 
				$('#show-pourcharts').trigger('click');
			 }, 10);
}*/





$('#show-tempcharts').click(function(){
	
	if(is_form_submit==1) {
		$('#paginate').show();
		//disable prev if offset is zero
		$('#active_tab_hdn').val('temp');
		var offset_value=$('#offset_setting').val();
		if(offset_value == 0) {
			$('#prev').css("display","none");
		} else {
			$('#prev').css("display","block");
		}
		//end disble code prev
	}
	$('#page').hide();
	$('#page_temp').show();
	

	$('#show-pourcharts').removeClass('active');
	
	$('#show-tempcharts').addClass('active');
	$('#new-graph-limit').show()
	$('.regression').hide();
	$('#ident_hdn').val('temp');
	$('#tbl_Section').show();
	$('#dbPanels').show();
	$('.treadlineCharts').hide();	
	$('#kegdataReports').hide();
	$('.tempChart').removeClass('hide');
	$('.levels').addClass('hide');
	$('.levelChart').addClass('hide');
	$('.pourChart').addClass('hide');
		//	$('.col-sm-3').addClass('col-sm-6').removeClass('col-sm-3');
			//Build Temp Charts

			var temphist = $('#show-tempcharts').data();
			var obj_length_temp=Object.keys(temphist).length;

			/*if(obj_length_temp==1) {
				
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');	
			} else {
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');
			}*/

			//console.log(temphist);
			var labels = new Array();
			var data = new Array();
			var tmpctx = new Array();
			var tmpLineChart = new Array();
			var counter = 0;
			var lineData = new Array();
			var options_tooltip = { // tooltip setting
				pointHitDetectionRadius : 3,
				responsive: true,
				maintainAspectRatio: false,
				scaleLabel: "<%=value%>"+'  °'+level_settin_temp
				//tooltipTemplate: "<%if (label){%><%=label%> - <%}%><%= value %> °"+level_settin_temp,


			};

			for(var p in temphist){

				var keg = temphist[p];

				if(keg.length > 0){
					labels[counter] = new Array();
					data[counter] = new Array();
					for(var i = 0; i < keg.length; i++){
						labels[counter].push(keg[i]['date']);
						data[counter].push(keg[i]['tempFormatted']);

					}
					labels[counter].reverse();
					data[counter].reverse();

					var label_length = labels[counter].length;
					var t=labels[counter];
                    var temp_array = new Array();
					if(label_length > 10) {
						var frequency = label_length/10;
						frequency=Math.round(frequency);
						//alert(frequency);
						var k=0;
						for( var i=0;i<t.length;i++ ) {

						    var check_integer=Number.isInteger(i/frequency);

						   var last_index=(t.length)-1;
							if((i== 0) || (check_integer) || (i==last_index) ) {
								temp_array[i]=t[i];
							}  else {
								temp_array[i]='';
							}

						}


					}  else {
						temp_array=t;
					}
					

					lineData[counter] =  {
						labels: temp_array,
						datasets: [
						{
							label: keg[0]['name'],
							fillColor: "rgba(151,187,205,0.2)",
							strokeColor: "rgba(151,187,205,1)",
							pointColor: "rgba(151,187,205,1)",
							pointStrokeColor: "#fff",
							pointHighlightFill: "#fff",
							pointHighlightStroke: "rgba(151,187,205,1)",
							data: data[counter]
						}
						]
					};

					tmpctx[counter] = document.getElementById(p).getContext("2d");
					tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
					
					counter++;
				}
			}
		});/*Close show-tempcharts*/

$('#show-pourcharts').click(function(){


	$('#page_temp').hide();
	$('#page').show();
	
	$('#show-tempcharts').removeClass('active');
	$('#show-pourcharts').addClass('active');
	$('#active_tab_hdn').val('pour');
	var offset_setting_pour=$('#offset_setting_pour').val();
	if(is_form_submit==1) {
		$('#paginate').show();
		if(offset_setting_pour == 0) {
			$('#prev').css("display","none");
		} else {
			$('#prev').css("display","block");
		}
	}
	//$('#paginate').hide();
	
	$('#new-graph-limit').show()
	$('.regression').show();
	$('#ident_hdn').val('pour');
	$('#tbl_Section').show();
	$('#kegdataReports').hide();
	$('.pagination').hide();
	$('#dbPanels').show();
	$('.treadlineCharts').hide();
	$('#kegdataReports').hide();
	$('.tempChart').addClass('hide');
	$('.levels').addClass('hide');
	$('.levelChart').addClass('hide');
	$('.pourChart').removeClass('hide');

	var pourhist = $('#show-pourcharts').data();
	var obj_length=Object.keys(pourhist).length;
			/*if(obj_length==1) {
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');	
			} else {
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');
			}*/
			
			//alert(level_setting);

			var labels = new Array();
			var data = new Array();
			var labels_date = new Array();
			var pourctx = new Array();
			var pourBarChart = new Array();
			var counter = 0;
			var barData = new Array();
			var pour_new_m_array = new Array();
			var pour_new_b_array = new Array();
			for(var p in pourhist){

				var graph_name=p;
				//console.log(graph_name);
				var keg = pourhist[p];
				//console.log("===>"+keg.length);
				if(keg.length > 0){
					
					labels[counter] = new Array();
					data[counter] = new Array();
					labels_date[counter]=new Array();
					pour_new_m_array[counter]=new Array();
					pour_new_b_array[counter]=new Array();
					var is_graph_shown=true;
					var m_db_value=$("#m_db_value_hdn_"+graph_name).val();
					m_db_value = m_db_value.replace (/,/g, "");
					//var m=keg[i]['new_lsm_m_from_new'];
					for(var i = 0; i < keg.length; i++){

						//if it is a valid pour then push

						if(keg[i]['is_valid_pour'] == true) {

							var flow_rate = keg[i]['flow_rate'];
							var flow_length=(flow_rate)*(keg[i]['length']);
							
							//mention check to start the y axis from 0 and up 
							if(flow_length < 1) {
								flow_length=0.5;
							}
							


							labels[counter].push(flow_length);
							//labels[counter].push(keg[i]['length']);
							data[counter].push(keg[i]['level']);

							labels_date[counter].push(keg[i]['date']);


							//push lsm values table ma and b in array
							if((keg[i]['new_lsm_m_from_new']== null
								) || (keg[i]['new_lsm_m_from_new']== undefined) || (keg[i]['new_lsm_m_from_new']== '')){
								pour_new_m_array[counter].push(m_db_value);

							}else{
								pour_new_m_array[counter].push(keg[i]['new_lsm_m_from_new']);

							}
							pour_new_b_array[counter].push(keg[i]['new_lsm_b_from_new']);

							//labels[counter].unshift(keg[i]['date']);
							//data[counter].unshift(keg[i]['level']);
						} 
					}
					//console.log(labels[counter]);
					//console.log(data[counter]);
					var labels_temp = new Array();
					labels_temp=labels[counter];//remove orignal in temp

					//assign lsm_values array in a variable
					var labels_temp_m_arr = new Array();
					labels_temp_m_arr=pour_new_m_array[counter];

					//assign lsm_values array in a variable
					var labels_temp_b_arr = new Array();
					labels_temp_b_arr=pour_new_b_array[counter];

					var count_of_pours=labels[counter].length;
					//console.log(count_of_pours);
					
					//find m and b values
					/*var m=$("#m_value_hdn_"+graph_name).val();
					var b=$("#b_value_hdn_"+graph_name).val();
					var m_db_value=$("#m_db_value_hdn_"+graph_name).val();
					var keg_id_value=$("#keg_id_hdn_"+graph_name).val();*/
					//var m_db_value=$("#m_db_value_hdn_"+graph_name).val();
				
					
					
					/*b = b.replace (/,/g, "");
					m = m.replace (/,/g, "");
					m_db_value = m_db_value.replace (/,/g, "");*/
					
					//if m_db_value is not blank
					/*if(m_db_value != '') {
						//console.log('in not blank');
						if(count_of_pours < 10) {
							m=m_db_value;
						} else if(count_of_pours > 20) {
							m=m;
							//calling function for update lsm_m column
							//update_lsm_db_value(m,keg_id_value);
						} 
					}*/

					//console.log("====>"+m_db_value);

					//make x axis comunlative
					/*var t=0;
					var temp_x_axis=[];
					for(var i = 0; i < labels[counter].length; i++){ 
						var add=t+parseInt(labels[counter][i]);
						t=add;
						temp_x_axis[i]=t;
					}

					labels[counter]=temp_x_axis;*/
					//console.log(labels[counter]);
					//console.log(data[counter]);
					//console.log(labels_temp);

					//fetch flow rate
					//formula is: (full keg volume in desired units)/(ADCempty-ADCfull)*m_adc

					//var flow_rate =

					//fetch y axis according to equation y=mx+b
					var calculative_y_axis=[];
					//var positive_m=Math.abs(m);
					var cumulative_ounces=0;
					//console.log(labels_temp);
					for(var i = 0; i < labels_temp.length; i++){
						//var m= labels_temp_m_arr[i];
						//var positive_m=Math.abs(m);
						//alert(positive_m);
						//var mx = parseFloat(positive_m) * parseFloat(labels_temp[i]);
						//var y= parseFloat(mx)+parseFloat(b);
						//var y=mx;
						//y=y.toFixed(2);
						//y=Math.round(y); 

						//because we calulate mx on top
						var y = parseFloat(labels_temp[i]);
						y=y.toFixed(2);
						y=Math.round(y); 
						calculative_y_axis[i]=y;
						// fetch total of cumulative_ounces
						cumulative_ounces=cumulative_ounces+y;
					}
					//assign this array to data counter
					data[counter]=calculative_y_axis;
					//console.log(data[counter]);


						
					if(count_of_pours < 1) {
						is_graph_shown=false;
					} else if(count_of_pours > 10 && count_of_pours < 20) {
						is_graph_shown=true;
					} else if(count_of_pours > 20 && cumulative_ounces > (level_setting == "oz") ? 200 : 200) {
						is_graph_shown=true;
						//calling function for update lsm_m column
						//update_lsm_db_value(m,keg_id_value);
					}


					//summarry of pours
					var device_id=keg[0]['deviceID'];
					var volumeTypeOfKeg=keg[0]['volumeType'];
					var sumarry_xhtml='';
					if(count_of_pours && is_graph_shown) {
						sumarry_html='<p>Summary of Selected Time<br> Total Pours: '+count_of_pours+'<br> Total Volume: '+cumulative_ounces+' '+volumeTypeOfKeg+'</p>';
						$('#summary_div_'+device_id).html(sumarry_html);
					}

					//set graph height width of main div
					if(count_of_pours <= 3) {
						$("#main_div_"+device_id).attr("class", "col-sm-4");
					} else if(count_of_pours >= 4 && count_of_pours < 7) {
						$("#main_div_"+device_id).attr("class", "col-sm-8");
					} else {
						$("#main_div_"+device_id).attr("class", "col-sm-12");
					}
					//alert(keg[0]['deviceID']);
					
					
					
					//console.log(data[counter]);

					if(is_graph_shown) {
						var options = {
							scaleBeginAtZero: false,
							responsive: true,
							maintainAspectRatio: false,
							scaleLabel: "<%=value%>"+' '+level_setting,
							tooltipTemplate: "<%if (label){%><%=label%> - <%}%><%= value %> "+level_setting,
							   /*title: {
						            display: true,
						            text: 'Custom Chart Title'
						        }*/

						    };
						    barData[counter] =  {
						    	labels: labels_date[counter].reverse(),
						    	seriesType: "stepline",
						    	datasets: [
						    	{
						    		label: keg[0]['name'].reverse(),
						    		fillColor: "rgba(151,187,205,0.5)",
						    		strokeColor: "rgba(151,187,205,0.8)",
						    		highlightFill: "rgba(151,187,205,0.75)",
						    		highlightStroke: "rgba(151,187,205,1)",
						    		data: data[counter].reverse()

						    	}
						    	]
						    };

						    pourctx[counter] = document.getElementById(p).getContext("2d");	

						    pourBarChart[counter] = new Chart(pourctx[counter]).Bar(barData[counter],options);
						} else {

							$('#gerror_'+p).show();
						}




						counter++;

					} else {
						$('#gerror_'+p).show();
					}
				}
		});//Close show-pourcharts

$('#show-levelcharts').click(function(){

	$('#ident_hdn').val('level');
	$('#dbPanels').show();
	$('#tbl_Section').show();
	$('.treadlineCharts').hide();
	$('#kegdataReports').hide();
	$('.pagination').hide();
	$('.tempChart').addClass('hide');
	$('.levels').addClass('hide');
	$('.pourChart').addClass('hide');
	$('.levelChart').removeClass('hide');
			//$('.col-sm-3').addClass('col-sm-6').removeClass('col-sm-3');
			$('.regression').hide();
			
			var levelhist = $('#show-levelcharts').data();
			var obj_length_level=Object.keys(levelhist).length;

			/*if(obj_length_level==1) {
				
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');	
			} else {
				$('.col-sm-3').addClass('col-sm-12').removeClass('col-sm-3');
			}*/
			
			var labels = new Array();
			var data = new Array();
			var tmpctx = new Array();
			var tmpLineChart = new Array();
			var counter = 0;
			var lineData = new Array();
			var highchart_level=[];
			var highchart_data=[];
			var options_tooltip = { // tooltip setting
				pointHitDetectionRadius : 3,
				responsive: true,
				maintainAspectRatio: false,
				scaleLabel: "<%=value%>"+' '+level_setting,
			};

			for(var p in levelhist){
				var graph_name=p;
				var keg = levelhist[p];

				if(keg.length>1) {
					highchart_data[0]=keg[(keg.length)-1]['date'];
					highchart_data[1]=keg[0]['date'];
					highchart_level[0]=keg[(keg.length)-1]['level'];
					highchart_level[1]=keg[0]['level'];
				}


				if(keg.length > 0){


					labels[counter] = new Array();
					data[counter] = new Array();
					/*	for(var i = 0; i < keg.length; i++){
							labels[counter].unshift(keg[i]['date']);
							data[counter].unshift(keg[i]['level']);
						}*/
						for(var i = 0; i < keg.length; i++){

							//if it is a valid pour then push
							if(keg[i]['is_valid_pour'] == true) {
								//labels[counter].push(keg[i]['length']);
								data[counter].unshift(keg[i]['level']);
								labels[counter].unshift(keg[i]['date']);
								//data[counter].unshift(keg[i]['level']);
							} else if(keg[i]['is_valid_pour']=='clear') { //if we get clear then do blank all array
								
								labels[counter].splice(0,labels[counter].length);
								data[counter].splice(0,data[counter].length);
							}
						}




						//console.log(data[counter]);
						//console.log(data[counter]);
/*
						for(var i = 0; i < data[counter].length; i++){ 
							if(data[counter][i+1]>data[counter][i]){
								data[counter][i+1]=data[counter][i];
							}
						}*/

						//console.log(data[counter]);

						lineData[counter] =  {
							labels: labels[counter],
							datasets: [
							{
								label: keg[0]['name'],
								fillColor: "rgba(151,187,205,0.2)",
								strokeColor: "rgba(151,187,205,1)",
								pointColor: "rgba(151,187,205,1)",
								pointStrokeColor: "#fff",
								pointHighlightFill: "#fff",
								pointHighlightStroke: "rgba(151,187,205,1)",
								data: data[counter]
							}
							]
						};

						tmpctx[counter] = document.getElementById(p).getContext("2d");
						tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
						counter++;
					}
				}

				//old chart
				// for(var p in levelhist){

				// 	var keg = levelhist[p];

				// 	if(keg.length>1) {
				// 		highchart_data[0]=keg[(keg.length)-1]['date'];
				// 		highchart_data[1]=keg[0]['date'];
				// 		highchart_level[0]=keg[(keg.length)-1]['level'];
				// 		highchart_level[1]=keg[0]['level'];
				// 	}


				// 	if(keg.length > 0){
				// 		labels[counter] = new Array();
				// 		data[counter] = new Array();
				// 		for(var i = 0; i < keg.length; i++){
				// 			labels[counter].unshift(keg[i]['date']);
				// 			data[counter].unshift(keg[i]['level']);
				// 		}
				// 		//console.log(data[counter]);
				// 		//console.log(data[counter]);



				// 		//console.log(data[counter]);

				// 		lineData[counter] =  {
				// 			labels: labels[counter],
				// 		    datasets: [
				// 		        {
				// 		            label: keg[0]['name'],
				// 		            fillColor: "rgba(151,187,205,0.2)",
				// 		            strokeColor: "rgba(151,187,205,1)",
				// 		            pointColor: "rgba(151,187,205,1)",
				// 		            pointStrokeColor: "#fff",
				// 		            pointHighlightFill: "#fff",
				// 		            pointHighlightStroke: "rgba(151,187,205,1)",
				// 		            data: data[counter]
				// 		        }
				// 		    ]
				// 		};

				// 		var id_old=p;
				// 		id_old = id_old.replace(/levelChart/gi, "levelChartOld");

				// 		tmpctx[counter] = document.getElementById(id_old).getContext("2d");
				// 		tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
				// 		counter++;
				// 	}
				// }
				//end old chart


				//console.log(highchart_data);
				//console.log(highchart_level);

				//start code for highchart
				 /*var date_from='';
				if(highchart_data.length) {
					date_from='Trendline of '+highchart_data[0]+'-'+highchart_data[1];
				}
				var data_high_arr=[];
				var set_max_val;
				if(highchart_level.length) {
					data_high_arr[0]=[0,highchart_level[0]];
					data_high_arr[1]=[highchart_level[1],highchart_level[1]];
					set_max_val=highchart_level[0]+100;
				}
				//console.log(data_high_arr);
				
				   Highcharts.chart('container_level_chart', {
				        xAxis: {
				            min: 0,
				            max: set_max_val
				        },
				        yAxis: {
				            min: 0
				        },
				        title: {
				            text: date_from
				        },
				        series: [{
				            type: 'line',
				            name: 'Trendline',
				            data: data_high_arr,
				            marker: {
				                enabled: false
				            },
				            states: {
				                hover: {
				                    lineWidth: 0
				                }
				            },
				            enableMouseTracking: false
				        }, ]
				    });*/

				//end code for highchart



			});

$('#show-treadlinecharts').click(function(){
	$('#tbl_Section').hide();
	$('#dbPanels').hide();
	$('.treadlineCharts').show();



});
});

function update_lsm_db_value(lsm_m,id) {
	

	var url = "/dashboard/updatedevice/user/" + lsm_m+'~'+id;
	$.ajax(url,{'type' : 'GET'}).done(
		function(results){
			if(results['Success'] == true){
				console.log('success');
				//activate_alert('User Deleted');
				//$('tr[data-userID="'+results['id']+'"]').remove();
			}else{
				//activate_alert('Could not delete notification');
				console.log('failed');
			}
		}).fail(function(jqXHR, textStatus){
			//activate_alert(textStatus);
		});	



	}


	var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        } 
    }
}
	</script>

	@foreach($kegs as $keg)
	<script type="text/javascript">
	$(document).ready(function(){
		$('#show-tempcharts').data("tempChart{{ $keg->id }}", {!! $keg->tempHistory->flatten() !!});
		$('#show-pourcharts').data("pourChart{{ $keg->id }}", {!! $keg->pourHistory->flatten() !!});
		$('#show-levelcharts').data("levelChart{{ $keg->id }}", {!! $keg->pourHistory->flatten() !!});
	});
	</script>
	@endforeach
