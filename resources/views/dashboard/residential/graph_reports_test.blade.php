

@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
    <li><a href="/dashboard/levels">Keg Levels</a></li>
    <li class="active"><a href="/dashboard/reports">Reports</a></li>
    @if($user->isAdmin)
    <li><a href="/dashboard/notifications">Notifications</a></li>
  <li><a href="/dashboard/kegsetup">Keg Setup</a></li>
  <li><a href="/dashboard/settings">Settings</a></li>
  @endif
  <li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')


<!-- tab code start -->

<div class="row">
    <nav class="nav-bar nav-bar-default">
      <div class="container-fluid">
        <div class="btn-group btn-group-sm">
          <a class="btn btn-primary navbar-btn"  href="/dashboard/reports">Run Reports</a>
          <!-- <button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button> -->
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='level') ? 'active':''; ?>" id="show-levelcharts">Show Level Charts</button>
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='temp') ? 'active':''; ?>" id="show-tempcharts">Show Temperature Charts</button>
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='pour') ? 'active':''; ?>" id="show-pourcharts">Show Pour Charts</button>
        </div>
                                    
                                
        <!--
        <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
           </form>-->
        </div>
    </nav>
</div>
<div id="tbl_Section">

@include('dashboard.common.reportgraphfiltertest', array('reportDates' => $reportDates, 'input' => $input,'ident_hdn'=>$ident_hdn))
</div>
<div class="row" id="dbPanels">
    <?php $count = 0; ?>
      @foreach($kegs as $keg)
      @if($keg->showDevice)
      <div class="col-sm-3">

        <?php
        
        $m='';
        $b='';
        if(isset($keg->lms_data)) {
          $m=number_format($keg->lms_data['m'],2);
          $b=number_format($keg->lms_data['b'],2);
        }

        ?>
          <div class="panel panel-default">
            <div class="panel-heading"><h6  class="regression  panel-title text-left">LSM Value :m= <?php echo $m; ?>, b= <?php echo $b; ?></h6><h3 class="panel-title text-center">{{ $keg->deviceName }}</h3></div>
            <div class="panel-body">
              <div class="hide tempChart">
                <canvas id="tempChart{{ $keg->id }}" width="300px" height="300px"></canvas>
              </div>
              <div class="hide pourChart">
                <canvas id="pourChart{{ $keg->id }}" width="300px" height="300px"></canvas>
               <!--  <div class="text-center verticaltext_content">ounces</div> -->
              </div>
              <div class="hide levelChart">
                <canvas id="levelChart{{ $keg->id }}" width="300px" height="300px"></canvas>
                
              </div>                                        
                                                        
            </div>
                                                    
                                                    
            <div class="panel-footer text-center">
              <small><small>Keg ID:</small><small>{{ $kegdata->getFormattedMac($keg->kegMac) }}</small><br /><small>Calibration Calculation {{ $keg->calibration }}</small></small>
            </div>
          </div>
        </div>
      @endif
      @endforeach

    <small>*All levels are approximate values</small>
    </div>
    
<!-- tab code end -->




</div> <!-- end div tbl section -->
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjs')
  @include('dashboard.residential.reports_graph_js_test')
  @include('dashboard.residential.levels_graph_js_test', array('kegs' => $kegs,'newGraph'=>$newGraph,'ident_hdn'=>$ident_hdn))
@stop
