@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
    <li><a href="/dashboard/levels">Keg Levels</a></li>
    <li class="active"><a href="/dashboard/reports?chart=pour">Charts & Graphs<!-- Reports --></a></li>
    @if($user->isAdmin)
    <li><a href="/dashboard/notifications">Notifications</a></li>
  <li><a href="/dashboard/kegsetup">Keg Setup</a></li>
  <li><a href="/dashboard/settings">Settings</a></li>
  @endif
  <li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')


<!-- tab code start -->

<div class="row">
    <nav class="nav-bar nav-bar-default">
      <div class="container-fluid">
        <div class="btn-group btn-group-sm">
          <a class="btn btn-primary navbar-btn "  href="/dashboard/graph_reports?chart=pour">Pour Charts</a>
          <a class="btn btn-primary navbar-btn"  href="/dashboard/graph_reports?chart=temp">Temperature Charts</a>
          <a class="btn btn-primary navbar-btn active " id="run_reports" href="/dashboard/reports">Export Reports</a>
          <!-- <button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button> -->
          <!--a class="btn btn-primary navbar-btn"  href="/dashboard/graph_reports?chart=level">Level Charts</a-->
          <!-- <a class="btn btn-primary navbar-btn"  href="/dashboard/graph_reports?chart=temp">Temperature Charts</a>
          <a class="btn btn-primary navbar-btn"  href="/dashboard/graph_reports?chart=pour">Pour Charts</a> -->
        </div>
                                    
                                
        <!--
        <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
           </form>-->
        </div>
    </nav>
</div>
<div id="tbl_Section">

@include('dashboard.common.reportfilter', array('reportDates' => $reportDates, 'input' => $input))
</div>

<div class="text-center">
{!! $reportData->appends(['reportStart' => $reportStart, 'reportEnd' => $reportEnd, 'quickDates' => $quickDates,'status'=>$status,'deviceID'=>$deviceID])->render() !!}
</div>

<table class="table table-striped table-bordered" id="kegdataReports">
  <thead>
  <tr>
     <th>When ( IN {{ $timezone->timezone}} )<span class="pull-right"></span></th><th>When ( IN UTC )<span class="pull-right"></span></th><th>Keg Mac<span class="pull-right"></span></th><th>Device Name<span class="pull-right"></span></th><th>Event<span class="pull-right"></span></th><th>Temperature<span class="pull-right"></span></th><th>Beer Remaining<span class="pull-right"></span></th><th>Pour Length<span class="pull-right"></span></th>
  </tr>
  </thead>
  <tbody>
  <?php $i=0; ?>
  @foreach($reportData as $data)
  @if($data->showDevice)
  <?php
    
    $calibration =  $kegdata->getCalibratedValue($data->kegMac);
   

   $ounces = $kegdata->getOunces($data->adc_pour_end, $calibration, $data->amount, $data->empty, $data->volumeType);
    
    $percentage = $kegdata->kegPercent($ounces, $data->amount); 
    //dd($timezone->timezone);
  ?>

  <tr{{ ($data->status == '101') ? ' class=danger' : '' }}>
   <td>{{ $data->sent_at->timezone($timezone->timezone)->toDayDateTimeString() }}</td> 
  <td>{{ $data->sent_at->toDayDateTimeString() }}</td> 
    <td>{{ $kegdata->getFormattedMac($data->kegMac) }}</td>
    <td>{{ $data->deviceName }}</td>
    <td>@if($data->status == '11')
      Pour Event
              @elseif($data->status == '101')
                Untap Event
              @elseif($data->status == '10011')
                Keg Blow
              @elseif($data->status == '1001')
                Heartbeat
              @else
                Boot Event
              @endif
    </td>
    <td>{{ $kegdata->getTemp($data->temperature, $data->temperatureType, $data->tempOffset ) }} &deg; {{ $data->temperatureType }}</td>
    <!--td>{!! HTML::image('/img/levels/'.$data->pic_prefix.'/'.$data->pic_prefix.$percentage.'.jpg', $percentage .'%', array('width' => '20px')  ) !!}</td-->
    <td>{{ $ounces." ".$data->volumeType }}</td>
    <td>{{ $data->pour_length }}</td>
  </tr>
  @endif
  <?php $i++; ?>
  @endforeach

    <?php 
   
    if(empty($i)) {
      echo '<tr><td colspan="7"><p style="text-align:center;color:red;">No results found with this time frame!</p></td></tr>';
      
    }

    ?>


  </tbody>
</table>

<div class="text-center">
{!! $reportData->render() !!}
</div>

</div> <!-- end div tbl section -->
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjs')
  @include('dashboard.residential.reportsjs')
  @include('dashboard.residential.levelsjs', array('kegs' => $kegs,'newGraph'=>$newGraph))
@stop
