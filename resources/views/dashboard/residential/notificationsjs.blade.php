<script type='text/javascript'>
		$(document).ready(function(){

			$("#notification").on('change',function(){
				switch($('#notification option:selected').val()){
					case 'L':
						$('#range-radios').addClass('hidden');
						$('#range-text').removeClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
						$('#valueSuffix').text('%');
						
						$("#frequency option[value*='11']").prop('selected',true);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").show();
						$("#frequency option[value*='9']").show();
						$("#frequency option[value*='8']").show();
					break;
					case 'E':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$('#freq-control').removeClass('hidden');
						$("#frequency option[value*='11']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',true);
						$("#frequency option[value*='11']").prop('disabled',false);
						$("#frequency option[value*='9']").prop('disabled',false);
						$("#frequency option[value*='8']").prop('disabled',false);
					break;
					case 'U':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();

					break;
					case 'K':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();
						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'B':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();
						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'P':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
					
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();

						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'T':
						$('#range-radios').removeClass('hidden');
						$('#range-text').addClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
						$('#valueSuffix').html('&deg;');
						$("#frequency option[value*='8']").prop('disabled',false);
						$("#frequency option[value*='8']").prop('selected',true);
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='16']").hide();
					break;
				}

				$('#submitNotification').val('Add Notification');
				$('#notificationID').val(0);
			});

			$('#frequency').on('change',function(){
				switch ($('#frequency option:selected').val()) {
					case "6":
					case "7":
					case "8":
						$('#freq-control').removeClass('has-warning');
						$('#freq-control').addClass('has-error');
					break;
					case "9":
					case "10":
					case "11":
						$('#freq-control').removeClass('has-error');
						$('#freq-control').addClass('has-warning');
					break;
					default:
						$('#freq-control').removeClass('has-warning');
						$('#freq-control').removeClass('has-error');
					break;
				}
			});

			$(".editNotification").on('click',function(){



			
				$("#alarmKeg option[value='2']").remove();
  				var row = $(this).parent().parent().children().index($(this).parent());
 				row=row+1;
				$('#selected-row').text('Note- You have selected row number  '+ row  );
				var data = $(this).parent('tr');
				var keg = data.attr('id').split('-');
				keg = keg[1];
				var notificationID = data.attr('data-notificationID');
				var notification = data.attr('data-notification');
				var operator = data.attr('data-operator');
				var value = data.attr('data-value');
				var frequency = data.attr('data-frequency');

				var emailcheck = data.attr('data-emailcheck');
				var emailOptional = data.attr('data-emailoptional');
				var emailOptional_2 = data.attr('data-emailoptional_2');
				var smscheck = data.attr('data-smscheck');

				var temptypes = data.attr('data-temptypes');
				

				
				$('#valueSuffix').text('');
				if(notification=='T') {
					
				temptypes == 'C' ? $('#valueSuffix').html("&#8451;"):$('#valueSuffix').html("&#8457;");
				
				} else {
					$('#valueSuffix').text('%');
				}

				$('#email_notification').prop('checked', false);
				$('#email_notification_optional').prop('checked', false);
				$('#sms_notification').prop('checked', false);
				if(emailcheck==1) {
					$('#email_notification').prop('checked', true);
				} else {
					$('#email_notification').prop('checked', false);
				}

				if(emailOptional==1) {
					$('#email_notification_optional').prop('checked', true);
				} else {
					$('#email_notification_optional').prop('checked', false);
				}

				if(emailOptional_2==1) {
					$('#email_notification_optional_2').prop('checked', true);
				} else {
					$('#email_notification_optional_2').prop('checked', false);
				}

				if(smscheck==1) {
					$('#sms_notification').prop('checked', true);
				} else {
					$('#sms_notification').prop('checked', false);
				}

				$('#notificationID').val(notificationID);
				$('#alarmKeg option[value="' + keg + '"]').prop('selected',true);
				$('#notification option[value="' + notification + '"]').prop('selected',true);
				$('input[name="range"][value="' + operator + '"]').prop('checked',true);
				$('#value').val(value);
				$('#frequency option[value="' + frequency + '"]').prop('selected',true);

				switch(notification){
					case 'L':
						$('#range-radios').addClass('hidden');
						$('#range-text').removeClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
					break;
					case 'E':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$('#freq-control').removeClass('hidden');
					break;
					case 'U':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'K':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'B':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'P':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'T':
						$('#range-radios').removeClass('hidden');
						$('#range-text').addClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
					break;
				}

				var freq_option_val = $('#notification option:selected').val();

				switch(freq_option_val){
					case 'L':
						$("#frequency option[value*='11']").prop('selected',false);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").show();
						$("#frequency option[value*='9']").show();
						$("#frequency option[value*='8']").show();
					break;
					case 'E':
						
						$("#frequency option[value*='11']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',true);
						$("#frequency option[value*='11']").prop('disabled',false);
						$("#frequency option[value*='9']").prop('disabled',false);
						$("#frequency option[value*='8']").prop('disabled',false);
					break;
					case 'U':
						
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();

					break;
					case 'K':
						
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();
						
					break;
					case 'B':
						
						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").prop('disabled',false);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();
						/*if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}*/
					break;
					case 'P':
						

						$("#frequency option[value*='16']").prop('selected',true);
						$("#frequency option[value*='16']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='8']").hide();

						
					break;
					case 'T':
						
						$("#frequency option[value*='8']").prop('disabled',false);
						$("#frequency option[value*='8']").prop('selected',true);
						$("#frequency option[value*='8']").show();
						$("#frequency option[value*='11']").hide();
						$("#frequency option[value*='9']").hide();
						$("#frequency option[value*='16']").hide();
					break;
				}

				$('submitNotification').val('Update Notification');
			});

			$('.delNotification').on('click',function(){
				var data = $(this).parent('tr');

				var keg = data[0].children[3].innerHTML;
				var notification = data[0].children[4].innerHTML;
				var deleteConfirm = confirm('Delete Notification ' + notification + ' for ' + keg + '?');
				if(deleteConfirm == true){
					var url = "/dashboard/notifications/" + data.attr('data-notificationID');
					$.ajax(url,{'type' : 'POST',
						data: { _method:"DELETE" },
						beforeSend: function(request) {
        							return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    						}}).done(
   						function(results){
							if(results['Success'] == true){
								activate_alert('Notification Deleted');
								$('tr[data-notificationID="'+results['id']+'"]').remove();
							}else{
								activate_alert('Could not delete notification');
							}
						}).fail(function(jqXHR, textStatus){
							activate_alert(textStatus);
						});	
				}
			});
		});


	</script>