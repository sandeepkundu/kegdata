<script type='text/javascript' src='/js/Chart.min.js'></script>
<script type='text/javascript' src='/js/moment.min.js'></script>
<script type='text/javascript' src='/js/bootstrap-datetimepicker.min.js'></script>
<script src="https://code.highcharts.com/highcharts.js" type="text/javascript"></script>
<script type="text/javascript">
		Chart.defaults.global = {
	    // Boolean - Whether to animate the chart
	    animation: true,
	    // Number - Number of animation steps
	    animationSteps: 60,
	    // String - Animation easing effect
	    animationEasing: "easeOutQuart",
	    // Boolean - If we should show the scale at all
	    showScale: true,
	    // Boolean - If we want to override with a hard coded scale
	    scaleOverride: false,
	    // ** Required if scaleOverride is true **
	    // Number - The number of steps in a hard coded scale
	    scaleSteps: null,
	    // Number - The value jump in the hard coded scale
	    scaleStepWidth: null,
	    // Number - The scale starting value
	    scaleStartValue: null,
	    // String - Colour of the scale line
	    scaleLineColor: "rgba(0,0,0,.1)",
	    // Number - Pixel width of the scale line
	    scaleLineWidth: 1,
	    // Boolean - Whether to show labels on the scale
	    scaleShowLabels: true,
	    // Interpolated JS string - can access value
	    scaleLabel: "<%=value%>",
	    // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
	    scaleIntegersOnly: true,
	    // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	    scaleBeginAtZero: false,
	    // String - Scale label font declaration for the scale label
	    scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Scale label font size in pixels
	    scaleFontSize: 12,
	    // String - Scale label font weight style
	    scaleFontStyle: "normal",
	    // String - Scale label font colour
	    scaleFontColor: "#666",
	    // Boolean - whether or not the chart should be responsive and resize when the browser does.
	    responsive: true,
	    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	    maintainAspectRatio: true,
	    // Boolean - Determines whether to draw tooltips on the canvas or not
	    showTooltips: true,
	    // Array - Array of string names to attach tooltip events
	    tooltipEvents: ["mousemove", "touchstart", "touchmove"],
	    // String - Tooltip background colour
	    tooltipFillColor: "rgba(0,0,0,0.8)",
	    // String - Tooltip label font declaration for the scale label
	    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Tooltip label font size in pixels
	    tooltipFontSize: 14,
	    // String - Tooltip font weight style
	    tooltipFontStyle: "normal",
	    // String - Tooltip label font colour
	    tooltipFontColor: "#fff",
	    // String - Tooltip title font declaration for the scale label
	    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
	    // Number - Tooltip title font size in pixels
	    tooltipTitleFontSize: 14,
	    // String - Tooltip title font weight style
	    tooltipTitleFontStyle: "bold",
	    // String - Tooltip title font colour
	    tooltipTitleFontColor: "#fff",
	    // Number - pixel width of padding around tooltip text
	    tooltipYPadding: 6,
	    // Number - pixel width of padding around tooltip text
	    tooltipXPadding: 6,
	    // Number - Size of the caret on the tooltip
	    tooltipCaretSize: 8,
	    // Number - Pixel radius of the tooltip border
	    tooltipCornerRadius: 6,
	    // Number - Pixel offset from point x to tooltip edge
	    tooltipXOffset: 10,
	    // String - Template string for single tooltips
	    tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
	    // String - Template string for single tooltips
	    multiTooltipTemplate: "<%= value %>",
	    // Function - Will fire on animation progression.
	    onAnimationProgress: function(){},
	    // Function - Will fire on animation completion.
	    onAnimationComplete: function(){}
	};
</script>

<script type="text/javascript">
	$(document).ready(function(){
		
   		
		
		var url=$(location).attr('href');

		if(url.match(/report/gi)) {
			
			if({!! $newGraph !!} == 1){
				
			
			setTimeout(function(){ 

				$('#show-levelcharts').trigger('click');
			 }, 10);
				
			}

		
			

		} else {
			$('#tbl_Section').hide();
			$('#dbPanels').show();
			$('.tempChart').addClass('hide');
			$('.levels').removeClass('hide');
			$('.pourChart').addClass('hide');
			$('.levelChart').addClass('hide');
			$('.col-sm-4').addClass('col-sm-3').removeClass('col-sm-4');
			
		}





		$('#run_reports').click(function(){
			$('#dbPanels').hide();
			$('#tbl_Section').show();
			$('.pagination').show();
			$('#kegdataReports').show();
			$('.tempChart').addClass('hide');
			$('.treadlineCharts').hide();	
			$('.levels').addClass('hide');
			$('.levelChart').addClass('hide');
			$('.pourChart').addClass('hide');
			$('.col-sm-3').addClass('col-sm-4').removeClass('col-sm-3');
		});//End show-keglevels



		$('#show-keglevels').click(function(){
			$('#tbl_Section').hide();
			$('#dbPanels').show();
			$('.tempChart').addClass('hide');
			$('.levels').removeClass('hide');
			$('.pourChart').addClass('hide');
			$('.levelChart').addClass('hide');
			$('.col-sm-4').addClass('col-sm-3').removeClass('col-sm-4');
		});//End show-keglevels


		$('#show-tempcharts').click(function(){
			$('#tbl_Section').show();
			$('#dbPanels').show();
			$('.treadlineCharts').hide();	
			$('#kegdataReports').hide();
			$('.tempChart').removeClass('hide');
			$('.levels').addClass('hide');
			$('.levelChart').addClass('hide');
			$('.pourChart').addClass('hide');
			$('.col-sm-3').addClass('col-sm-6').removeClass('col-sm-3');
			//Build Temp Charts

			var temphist = $('#show-tempcharts').data();
			console.log(temphist);
			var labels = new Array();
			var data = new Array();
			var tmpctx = new Array();
			var tmpLineChart = new Array();
			var counter = 0;
			var lineData = new Array();
			var options_tooltip = { // tooltip setting
	            pointHitDetectionRadius : 3
	        };

				for(var p in temphist){

					var keg = temphist[p];

					if(keg.length > 0){
						labels[counter] = new Array();
						data[counter] = new Array();
						for(var i = 0; i < keg.length; i++){
							labels[counter].unshift(keg[i]['date']);
							data[counter].unshift(keg[i]['tempFormatted']);
						}
						lineData[counter] =  {
							labels: labels[counter],
						    datasets: [
						        {
						            label: keg[0]['name'],
						            fillColor: "rgba(151,187,205,0.2)",
						            strokeColor: "rgba(151,187,205,1)",
						            pointColor: "rgba(151,187,205,1)",
						            pointStrokeColor: "#fff",
						            pointHighlightFill: "#fff",
						            pointHighlightStroke: "rgba(151,187,205,1)",
						            data: data[counter]
						        }
						    ]
						};

						tmpctx[counter] = document.getElementById(p).getContext("2d");
						tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
						counter++;
					}
				}
		});/*Close show-tempcharts*/

		$('#show-pourcharts').click(function(){
			$('#tbl_Section').show();
			$('#kegdataReports').hide();
			$('.pagination').hide();
			$('#dbPanels').show();
			$('.treadlineCharts').hide();
			$('#kegdataReports').hide();
			$('.tempChart').addClass('hide');
			$('.levels').addClass('hide');
			$('.levelChart').addClass('hide');
			$('.pourChart').removeClass('hide');
			$('.col-sm-3').addClass('col-sm-6').removeClass('col-sm-3');

			var pourhist = $('#show-pourcharts').data();
			var labels = new Array();
			var data = new Array();
			var pourctx = new Array();
			var pourBarChart = new Array();
			var counter = 0;
			var barData = new Array();
			for(var p in pourhist){
				var keg = pourhist[p];
				if(keg.length > 0){
					labels[counter] = new Array();
					data[counter] = new Array();
					for(var i = 0; i < keg.length; i++){
						labels[counter].unshift(keg[i]['date']);
						data[counter].unshift(keg[i]['length']);
					}
					barData[counter] =  {
						labels: labels[counter],
						   seriesType: "stepline",
					    datasets: [
					        {
					        	
					        
					            label: keg[0]['name'],
					             fill: false,
					            fillColor: "rgba(151,187,205,0.5)",
	   		 					strokeColor: "rgba(151,187,205,0.8)",
					            highlightFill: "rgba(151,187,205,0.75)",
					            highlightStroke: "rgba(151,187,205,1)",

					         	
					            data: data[counter]
					           
					        }
					    ]
					};
			
					pourctx[counter] = document.getElementById(p).getContext("2d");	

					pourBarChart[counter] = new Chart(pourctx[counter]).Line(barData[counter]);
	

					counter++;

				}
			}
		});//Close show-pourcharts

		$('#show-levelcharts').click(function(){

			$('#dbPanels').show();
			$('#tbl_Section').show();
			$('.treadlineCharts').hide();
			$('#kegdataReports').hide();
			$('.pagination').hide();
			$('.tempChart').addClass('hide');
			$('.levels').addClass('hide');
			$('.pourChart').addClass('hide');
			$('.levelChart').removeClass('hide');
			$('.col-sm-3').addClass('col-sm-6').removeClass('col-sm-3');

			
			var levelhist = $('#show-levelcharts').data();
			
			var labels = new Array();
			var data = new Array();
			var tmpctx = new Array();
			var tmpLineChart = new Array();
			var counter = 0;
			var lineData = new Array();
			var highchart_level=[];
			var highchart_data=[];
			var options_tooltip = { // tooltip setting
	            pointHitDetectionRadius : 3
	        };

				for(var p in levelhist){

					var keg = levelhist[p];

					if(keg.length>1) {
						highchart_data[0]=keg[(keg.length)-1]['date'];
						highchart_data[1]=keg[0]['date'];
						highchart_level[0]=keg[(keg.length)-1]['level'];
						highchart_level[1]=keg[0]['level'];
					}
					

					if(keg.length > 0){
						labels[counter] = new Array();
						data[counter] = new Array();
						for(var i = 0; i < keg.length; i++){
							labels[counter].unshift(keg[i]['date']);
							data[counter].unshift(keg[i]['level']);
						}
						//console.log(data[counter]);
						//console.log(data[counter]);

						for(var i = 0; i < data[counter].length; i++){ 
							if(data[counter][i+1]>data[counter][i]){
								data[counter][i+1]=data[counter][i];
							}
						}

						//console.log(data[counter]);

						lineData[counter] =  {
							labels: labels[counter],
						    datasets: [
						        {
						            label: keg[0]['name'],
						            fillColor: "rgba(151,187,205,0.2)",
						            strokeColor: "rgba(151,187,205,1)",
						            pointColor: "rgba(151,187,205,1)",
						            pointStrokeColor: "#fff",
						            pointHighlightFill: "#fff",
						            pointHighlightStroke: "rgba(151,187,205,1)",
						            data: data[counter]
						        }
						    ]
						};

						tmpctx[counter] = document.getElementById(p).getContext("2d");
						tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
						counter++;
					}
				}

				//old chart
				// for(var p in levelhist){

				// 	var keg = levelhist[p];

				// 	if(keg.length>1) {
				// 		highchart_data[0]=keg[(keg.length)-1]['date'];
				// 		highchart_data[1]=keg[0]['date'];
				// 		highchart_level[0]=keg[(keg.length)-1]['level'];
				// 		highchart_level[1]=keg[0]['level'];
				// 	}
					

				// 	if(keg.length > 0){
				// 		labels[counter] = new Array();
				// 		data[counter] = new Array();
				// 		for(var i = 0; i < keg.length; i++){
				// 			labels[counter].unshift(keg[i]['date']);
				// 			data[counter].unshift(keg[i]['level']);
				// 		}
				// 		//console.log(data[counter]);
				// 		//console.log(data[counter]);

						

				// 		//console.log(data[counter]);

				// 		lineData[counter] =  {
				// 			labels: labels[counter],
				// 		    datasets: [
				// 		        {
				// 		            label: keg[0]['name'],
				// 		            fillColor: "rgba(151,187,205,0.2)",
				// 		            strokeColor: "rgba(151,187,205,1)",
				// 		            pointColor: "rgba(151,187,205,1)",
				// 		            pointStrokeColor: "#fff",
				// 		            pointHighlightFill: "#fff",
				// 		            pointHighlightStroke: "rgba(151,187,205,1)",
				// 		            data: data[counter]
				// 		        }
				// 		    ]
				// 		};
						
				// 		var id_old=p;
				// 		id_old = id_old.replace(/levelChart/gi, "levelChartOld");
						
				// 		tmpctx[counter] = document.getElementById(id_old).getContext("2d");
				// 		tmpLineChart[counter] = new Chart(tmpctx[counter]).Line(lineData[counter],options_tooltip);
				// 		counter++;
				// 	}
				// }
				//end old chart


				//console.log(highchart_data);
				//console.log(highchart_level);

				//start code for highchart
				 /*var date_from='';
				if(highchart_data.length) {
					date_from='Trendline of '+highchart_data[0]+'-'+highchart_data[1];
				}
				var data_high_arr=[];
				var set_max_val;
				if(highchart_level.length) {
					data_high_arr[0]=[0,highchart_level[0]];
					data_high_arr[1]=[highchart_level[1],highchart_level[1]];
					set_max_val=highchart_level[0]+100;
				}
				//console.log(data_high_arr);
				
				   Highcharts.chart('container_level_chart', {
				        xAxis: {
				            min: 0,
				            max: set_max_val
				        },
				        yAxis: {
				            min: 0
				        },
				        title: {
				            text: date_from
				        },
				        series: [{
				            type: 'line',
				            name: 'Trendline',
				            data: data_high_arr,
				            marker: {
				                enabled: false
				            },
				            states: {
				                hover: {
				                    lineWidth: 0
				                }
				            },
				            enableMouseTracking: false
				        }, ]
				    });*/
				
				//end code for highchart



		});
		
		$('#show-treadlinecharts').click(function(){
			$('#tbl_Section').hide();
			$('#dbPanels').hide();
			$('.treadlineCharts').show();
		
				
		
		});
	});
</script>

@foreach($kegs as $keg)
<script type="text/javascript">
	$(document).ready(function(){
		$('#show-tempcharts').data("tempChart{{ $keg->id }}", {!! $keg->tempHistory->flatten() !!});
		$('#show-pourcharts').data("pourChart{{ $keg->id }}", {!! $keg->pourHistory->flatten() !!});
		$('#show-levelcharts').data("levelChart{{ $keg->id }}", {!! $keg->pourHistory->flatten() !!});
	});
</script>
@endforeach
