@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Keg Setup
@stop

@section('tabs')
  	<li><a href="/dashboard/levels">Keg Levels</a></li>
  	<li><a href="/dashboard/graph_reports?chart=pour">Charts & Graphs<!-- Reports --></a></li>
  	@if($user->isAdmin)
  	<li><a href="/dashboard/notifications">Notifications</a></li>
	<li class="active"><a href="/dashboard/kegsetup">Keg Setup</a></li>
	<li><a href="/dashboard/settings">Settings</a></li>
	@endif
	<li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')
<div class="modal" id="dist-modal" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Choose your distributor</h4>
      </div>
      <div class="modal-body" style="height:500px;overflow-y:scroll;">
      	<div class="row">

      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveDist">Select Distributor</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="clearfix">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading"><h2 class="panel-title">Keg Setup Summary<!-- Keg Devices --></h2></div>
			<div class="panel-body">
				<table class="table table-striped table-hover" id="setupTable">
					<thead>
						<tr><th>Keg Mac Address</th><th>Keg Name</th><th>Contents</th><th>Keg Type</th></tr>
					</thead>
					<tbody>
					@foreach($kegs as $keg)
					<tr id="keg-{{ $keg->id }}">
						<td>{{ $kegdata->getFormattedMac( $keg->kegMac ) }}</td>
						<td>{{ $keg->deviceName }}</td>
						<td>{{ isset($keg->beer) ? $keg->beer->name : '' }}</td>
						<td>{{ isset($keg->kegtype) ? $keg->kegtype->kegTypeDesc  : '' }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default" id="editKegs">
			<div class="panel-heading"><h2 class="panel-title">Edit Keg</h2></div>
			<div class="panel-body">	
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				{!! Form::open(['url' => 'dashboard/kegsetup', 'method' => 'patch', 'class' => 'form-horizontal', 'id' => 'editKegForm']) !!}
					<div class="row form-group{{ !$errors->first('id') ? '' : '  has-error has-feedback' }}">
						{!! Form::label('id', 'Choose Keg To Edit', array('class' => 'sr-only')) !!}
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon"><strong class="fa fa-asterisk text-danger"></strong>Select Keg</span>
								{!! Form::select('id',$kegsForSelect, old('id'), ['class' => 'form-control', 'id' => 'chooseKeg']) !!}
							</div>
							@if ($errors->first('id'))
								<span class="help-block">{{ $errors->first('id') }}</span>
							@endif
						</div>
					</div>
					<div class="col-sm-4 col-sm-offset-2">
						<div class="form-group">
							<div class="checkbox">
								{!! Form::checkbox('showDevice', 0, old('showDevice'), array('id' => 'showDevice')) !!}
								{!! Form::label('showDevice', 'Hide Keg on Reports') !!}
							</div>
						</div>
					</div>

					<div class="row form-group{{ !$errors->first('kegType') ? '' : '  has-error has-feedback' }}">
						{!! Form::label('kegType', 'Choose Type of Keg' ,['class' => 'sr-only']) !!}
						<label class="sr-only" for="kegType">Choose type of keg</label>
						<div class="col-sm-12">
							<div class="bg-primary text-center clickable showKegTypes"><strong><small>Select Keg Size and Shape<!--  What type of keg do I have? --></small></strong></div>
							<div class="infoBox text-center" id="kegTypePics" style="display:none;height:300px;">
							        <span class="clickable kegtype" id="fullkeg">
							        	<img src="/img/info/fullkeg-desc.jpg" height="300px" alt="1/2 Barrel Description" title="1/2 Barrel Description"/>
							        </span>
					 			<span class="clickable kegtype" id="quarterkeg">
							        	<img src="/img/info/tallQuarter-desc.jpg" height="300px" alt="1/4 Barrel Description" title="1/4 Barrel Description"/>
							        </span>
							      	<span class="clickable kegtype" id="ponykeg">
							        	<img src="/img/info/ponyKeg-desc.jpg" height="300px" alt="1/4 Barrel, Squat Style Description" title="1/4 Barrel, Squat Style Description"/>
							        </span>
							      	<span class="clickable kegtype" id="cylinder">
							        	<img src="/img/info/cylinder-desc.jpg" height="300px" alt="1/6 Barrel Description" title="1/6 Barrel Description"/>
							        </span>
							      	<span class="clickable kegtype" id="euro">
							        	<img src="/img/info/euro-desc.jpg" height="300px" alt="50 Liter Barrel Description" title="50 Liter Barrel Description"/>
							        </span>
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-question-circle text-info clickable showKegTypes"></i>Help</span>
								{!! Form::select('kegType', $typesForSelect, old('kegType'), ['class' => 'form-control', 'id' => 'kegType']) !!}
							</div>	
							@if ($errors->first('kegType'))
								<span class="help-block">{{ $errors->first('kegType') }}</span>
							@endif			
						</div>
					</div>
					<div class="row form-group{{ !$errors->first('kegContents') ? '' : '  has-error has-feedback' }}">
						<div class="col-sm-12" id="kegBloudhound">
							{!! Form::label('kegContents', 'Search for contents of keg', ['class'=>'sr-only']) !!}
							{!! Form::hidden('product_id', old('product_id'), ['id' => 'contentsID']) !!}	
							<div class="bg-primary text-center"><strong><small>Enter Beer Name To Select From Options</small></strong></div>
							<div class="scrollable-dropdown-menu">
								{!! Form::text('kegContents', old('kegContents'), ['class' => "form-control typeahead", 'id' => 'kegContents', 'placeholder' => 'Search for Contents of Keg']) !!}
							</div>
							@if ($errors->first('kegContents'))
								<span class="help-block">{{ $errors->first('kegContents') }}</span>
							@endif
						</div>
					</div>
					<div class="row form-group{{ !$errors->first('distSearchValue') ? '' : '  has-error has-feedback' }}" id="distSearchContainer">
						<div class="col-md-12">
						<fieldset id="dist-search" class="fieldset-group"><legend>Search for Distributor</legend>
							<label>&nbsp;&nbsp;&nbsp;<input type="checkbox" value="" id="none_selected_chk">&nbsp;None Selected</label>
							{!! Form::label('distSearchValue', 'Search for Distributor', ['class' => 'sr-only']) !!}
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
									 {!! Form::text('distSearchValue', old('distSearchValue'), ['class' => 'form-control', 'id' => 'distSearchValue','placeholder'=>'None Selected']) !!} 
									<span class="input-group-btn"><a class="btn btn-default btn-block" id="distSearch"><i class="fa fa-arrow-circle-right"></i></a></span>
								</div>
							</div>
							<div class="col-md-12 radio text-center">
								<label for="distzip"> {!! Form::radio('distsearch', 'zipcode', true, ['id' => 'distzip']) !!}  By Zip</label>
								<label for="distname"> {!! Form::radio('distsearch', 'name', false, ['id' => 'distname']) !!} By Name</label>
								<label for="distcity"> {!! Form::radio('distsearch', 'city', false, ['id' => 'distcity']) !!} By City</label>
								
							</div>
							@if ($errors->first('distSearchValue'))
								<span class="help-block">{{ $errors->first('distSearchValue') }}</span>
							@endif
						</fieldset>	
						<div class="hidden" id="distSelected">
							<div class="col-md-4" id="distSelectedAddress">
							</div>
							<div class="col-md-8">
								<a class="btn btn-info btn-block" id="changeDist">Change Distributor</a>
								{!! Form::hidden('selectedDistID', old('selectedDistID'), ['id' => 'selectedDistID']) !!}
								{!! Form::hidden('distType', old('distType'), ['id' => 'distType']) !!}
								<p id="distWarning" class="text-danger hidden">This distributor does not have an account with KegData. Some distributor features, such as reordering will not be available. Please contact this distributor and have them create an account with us to enable these features.</p>
							</div>
						</div>
						</div>
					</div>
					<div class="row form-group{{ !$errors->first('style') ? '' : '  has-error has-feedback' }}">
						{!! Form::label('style', 'Enter Beer Style', array('class' => 'sr-only')) !!}
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon">Beer Style</span>
								<!--{!! Form::text('style', old('style'), ['class' => 'form-control', 'id' => 'style']) !!} -->
								{!! Form::select('style', $beerStyleForSelect, old('style'), ['class'=>'form-control', 'id' => 'style']) !!}

							</div>
							@if ($errors->first('style'))
								<span class="help-block">{{ $errors->first('style') }}</span>
							@endif
						</div>
					</div>
					<div class="row form-group{{ !$errors->first('description') ? '' : '  has-error has-feedback' }}">
						<div class="col-sm-12">
							{!! Form::label('description', 'Enter a description for this beer') !!}
						</div>
						<div class="col-sm-12">
							{!! Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description']) !!}
							@if ($errors->first('description'))
								<span class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>		
					<div class="row form-group">
						<div class="col-sm-12">
							{!! Form::label('deviceName', 'Change Name of Keg', ['class' => 'sr-only']) !!}
							<div class="bg-primary text-center"><strong><small>Enter Keg Nickname<!-- Select contents to name keg --></small></strong></div>
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								{!! Form::text('deviceName', old('deviceName'), ['class' => 'form-control', 'id' => 'kegName', 'disabled']) !!}
							</div>
						</div>
					</div>
					<div class="row form-group text-center">
						<input type="submit" name="submitDevice" value="Save Keg Setup" class="btn btn-primary" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('jsScript')
	@include('dashboard.common.dashboardjs')
	@include('dashboard.residential.kegsetupjs', array('kegs' => $kegs))
@stop