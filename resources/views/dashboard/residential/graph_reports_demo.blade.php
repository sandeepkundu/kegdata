

@section('title')
  {{ Config::get('constants.COMPANY_NAME') }} Keg Reports
@stop

@section('tabs')
    <li><a href="/dashboard/levels">Keg Levels</a></li>
    <li class="active"><a href="/dashboard/graph_reports?chart=pour">Charts & Graphs<!-- Reports --></a></li>
    @if($user->isAdmin)
    <li><a href="/dashboard/notifications">Notifications</a></li>
  <li><a href="/dashboard/kegsetup">Keg Setup</a></li>
  <li><a href="/dashboard/settings">Settings</a></li>
  @endif
  <li><a href="/dashboard/account">Account Management</a></li>
@stop

@section('tabpane')

<!-- tab code start -->
<style>

/*.pourChart {
   
   overflow-x: scroll;
}

.pourChart > canvas {
   width: 1500px !important;
   overflow-x: scroll;
}*/


</style>


<div class="row">
    <nav class="nav-bar nav-bar-default">
      <div class="container-fluid">
        <div class="btn-group btn-group-sm">
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='pour') ? 'active':''; ?>" id="show-pourcharts">Pour Charts</button>
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='temp') ? 'active':''; ?>" id="show-tempcharts">Temperature Charts</button>
          <a class="btn btn-primary navbar-btn"  href="/dashboard/reports">Export Reports</a>
          <!-- <button class="btn btn-primary navbar-btn" id="show-keglevels">Show Keg Levels</button> -->
          <!--button class="btn btn-primary navbar-btn <?php //echo ($ident_hdn=='level') ? 'active':''; ?>" id="show-levelcharts">Level Charts</button -->
         <!--  <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='temp') ? 'active':''; ?>" id="show-tempcharts">Temperature Charts</button>
          <button class="btn btn-primary navbar-btn <?php echo ($ident_hdn=='pour') ? 'active':''; ?>" id="show-pourcharts">Pour Charts</button> -->
        </div>
                                    
                                
        <!--
        <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
           </form>-->
        </div>
    </nav>
</div>
<div id="tbl_Section">

@include('dashboard.common.reportgraphfilter', array('reportDates' => $reportDates, 'input' => $input,'ident_hdn'=>$ident_hdn))
</div>
 <?php
   //newgraph is variable to identify form is submit or not
   if($newGraph) {
  ?>
<div class="row" id="dbPanels">
    <?php $count = 0; ?>
      @foreach($kegs as $keg)
      @if($keg->showDevice)
      <div class="col-sm-12" id="main_div_{{ $keg->id }}">

        <?php
        //echo "m===>".$keg->new_lsm_m_from_new."=====>b".$keg->new_lsm_b_from_new;
        $m=0.01;
        $b=0;
        if(isset($keg->lms_data)) {
          $m=number_format($keg->lms_data->m,2);
          $b=number_format($keg->lms_data->b,2);
        }

        ?>
          <div class="panel panel-default">

          
            <div class="panel-heading"><h6  class="regression  panel-title text-left"><span style="display:none" >LSM Value :m= <?php echo $m; ?>, b= <?php echo $b; ?> </span></h6><h3 class="panel-title text-center">{{ $keg->deviceName }}</h3>

            <input type="hidden" name="" id="m_value_hdn_pourChart{{ $keg->id }}" value="<?php echo $m; ?>">
            <input type="hidden" name="" id="b_value_hdn_pourChart{{ $keg->id }}" value="<?php echo $b; ?>">
             <input type="hidden" name="" id="m_db_value_hdn_pourChart{{ $keg->id }}" value="<?php echo $m; ?>">
             <input type="hidden" name="" id="keg_id_hdn_pourChart{{ $keg->id }}" value="<?php echo $keg->id; ?>">
            </div>
            
            <div class="panel-body  ">

              <div class="hide tempChart">
                <canvas id="tempChart{{ $keg->id }}" width="300px" height="400px"></canvas>
              </div>
              
              <div class="hide pourChart">
              <div id="summary_div_{{ $keg->id }}" style="color:green;">
              </div>
                 <span style="color:red;display:none;float:left" id="gerror_pourChart{{ $keg->id }}">No data to display during this time period</span>
                <canvas id="pourChart{{ $keg->id }}" width="300px" height="400px"></canvas>
               <!--  <div class="text-center verticaltext_content">ounces</div> -->
              </div>
              <!-- div class="hide levelChart">
                <canvas id="levelChart{{ $keg->id }}" width="300px" height="400px"></canvas>
              </div -->                                        
                                                        
            </div>
                                                    
                                                    
            <div class="panel-footer text-center">
              <small><small>Keg ID:</small><small>{{ $kegdata->getFormattedMac($keg->kegMac) }}</small><br /><small>Calibration Calculation {{ $keg->calibration }}</small></small>
            </div>
          
          </div>
        </div>
      @endif
      @endforeach

    <small>*All levels are approximate values</small>
    </div>
<!-- end if to check form submit -->
<?php }  else {?>   
<span style="color:red;">Please select filters to show the graph!</span>
<?php } ?>

<!-- tab code end -->




</div> <!-- end div tbl section -->
@stop

@section('jsScript')
  @include('dashboard.common.dashboardjsDemo')
  @include('dashboard.residential.reports_graph_js_demo')
  @include('dashboard.residential.levels_graph_js_demo', array('kegs' => $kegs,'newGraph'=>$newGraph,'ident_hdn'=>$ident_hdn,'level_setting'=>$settings->volumeType,'level_setting_temp'=>$settings->temperatureType))
@stop
