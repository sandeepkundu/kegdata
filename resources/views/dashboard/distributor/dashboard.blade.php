@section('title')
	{{ Config::get('constants.COMPANY_NAME') }} Dashboard
@stop


@section('content')
	<ul class="nav nav-tabs nav-justified tabs" id="employee-tabs" role="tablist">
	  	<li class="{{$tab == 'levels' ? 'active' : '' }}"><a href="#levels">Keg Levels</a></li>
	  	<li class="{{$tab == 'reports' ? 'active' : '' }}"><a href="#reports">Reports</a></li>
	  	@if($user->isAdmin)
	  	<li class="{{$tab == 'notifications' ? 'active' : '' }}"><a href="#notifications">Notifications</a></li>
		<li class="{{$tab == 'settings' ? 'active' : '' }}"><a href="#settings">Settings</a></li>
		@endif
		<li class="{{$tab == 'account' ? 'active' : '' }}"><a href="#account">Account Management</a></li>
  	</ul>
  	<div class="container-fluid">
	  	<div class="tab-content col-md-12" style="inline-block">
		  	<div class="tab-pane{{$tab == 'levels' ? ' active' : '' }}" id="levels">
		  		@include($tabcontent['levels'], array('kegs' => $kegs, 'kegdata' => $kegdata))
		  	</div>
		  	<div class="tab-pane{{$tab == 'reports' ? ' active' : '' }}" id="reports">
		  		@include($tabcontent['reports'], array('kegs' => $kegs, 'kegdata' => $kegs))
		  	</div>
		  	@if($user->isAdmin)
		  	<div class="tab-pane{{$tab == 'notifications'  ? ' active' : '' }}" id="notifications">
		  		@include($tabcontent['notifications'], array('kegs' => $kegs, 'kegdata' => $kegdata, 'kegsForSelect' => $kegsForSelect ))
		  	</div>
			<div class="tab-pane{{$tab == 'settings' ? ' active' : '' }}" id="settings">
			   	@include($tabcontent['settings'], array('settings' => $settings))
		  	</div>
		  	@endif
			<div class="tab-pane{{$tab == 'account' ? ' active' : '' }}" id="account">
				@include($tabcontent['account'])
			 </div>
	  	</div>
  	</div>

@stop


@section('jsScript')
	<script>
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}
	</script>
	@include($tabcontent['levelsjs'], array('kegs' => $kegs))
	@include($tabcontent['kegsetupjs'])
	@include($tabcontent['settingsjs'])
	@include($tabcontent['notificationsjs'])
	@include($tabcontent['accountjs'])
@stop
