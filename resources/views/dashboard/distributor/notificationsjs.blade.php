<script type='text/javascript'>
		$(document).ready(function(){
			$("#notification").on('change',function(){
				switch($('#notification option:selected').val()){
					case 'L':
						$('#range-radios').addClass('hidden');
						$('#range-text').removeClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
						$('#valueSuffix').text('%');
					break;
					case 'E':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$('#freq-control').removeClass('hidden');
					break;
					case 'U':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}
					break;
					case 'P':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}
					break;
					case 'T':
						$('#range-radios').removeClass('hidden');
						$('#range-text').addClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
						$('#valueSuffix').html('&deg;');
					break;
				}

				$('#submitNotification').val('Add Notification');
				$('#notificationID').val(0);
			});

			$('#frequency').on('change',function(){
				switch ($('#frequency option:selected').val()) {
					case "6":
					case "7":
					case "8":
						$('#freq-control').removeClass('has-warning');
						$('#freq-control').addClass('has-error');
					break;
					case "9":
					case "10":
					case "11":
						$('#freq-control').removeClass('has-error');
						$('#freq-control').addClass('has-warning');
					break;
					default:
						$('#freq-control').removeClass('has-warning');
						$('#freq-control').removeClass('has-error');
					break;
				}
			});

			$(".editNotification").on('click',function(){
				var data = $(this).parent('tr');
				var keg = data.attr('id').split('-');
				keg = keg[1];
				var notificationID = data.attr('data-notificationID');
				var notification = data.attr('data-notification');
				var operator = data.attr('data-operator');
				var value = data.attr('data-value');
				var frequency = data.attr('data-frequency');

				$('#notificationID').val(notificationID);
				$('#alarmKeg option[value="' + keg + '"]').prop('selected',true);
				$('#notification option[value="' + notification + '"]').prop('selected',true);
				$('input[name="range"][value="' + operator + '"]').prop('checked',true);
				$('#value').val(value);
				$('#frequency option[value="' + frequency + '"]').prop('selected',true);

				switch(notification){
					case 'L':
						$('#range-radios').addClass('hidden');
						$('#range-text').removeClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
					break;
					case 'E':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}
						$('#freq-control').removeClass('hidden');
					break;
					case 'U':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}
					break;
					case 'P':
						if(!$('#range-control').hasClass('hidden')){
							$('#range-control').addClass('hidden');
						}

						if(!$('#freq-control').hasClass('hidden')){
							$('#freq-control').addClass('hidden');
						}
					break;
					case 'T':
						$('#range-radios').removeClass('hidden');
						$('#range-text').addClass('hidden');
						$('#range-control').removeClass('hidden');
						$('#freq-control').removeClass('hidden');
					break;
				}

				$('submitNotification').val('Update Notification');
			});

			$('.delNotification').on('click',function(){
				var data = $(this).parent('tr');

				var keg = data[0].children[3].innerHTML;
				var notification = data[0].children[4].innerHTML;
				var deleteConfirm = confirm('Delete Notification ' + notification + ' for ' + keg + '?');
				if(deleteConfirm == true){
					var url = "/dashboard/notifications/" + data.attr('data-notificationID');
					$.ajax(url,{'type' : 'POST',
						data: { _method:"DELETE" },
						beforeSend: function(request) {
        							return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    						}}).done(
   						function(results){
							if(results['Success'] == true){
								activate_alert('Notification Deleted');
								$('tr[data-notificationID="'+results['id']+'"]').remove();
							}else{
								activate_alert('Could not delete notification');
							}
						}).fail(function(jqXHR, textStatus){
							activate_alert(textStatus);
						});	
				}
			});
		});


	</script>