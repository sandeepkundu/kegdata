@extends('layouts.dashboard')


@section('content')
	<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  	@yield('tabs')
  	</ul>
  	<div class="container-fluid">
	  	<div class="tab-content col-md-12" style="inline-block">
		  	<div class="tab-pane active" id="levels">
		  		@yield('tabpane')
		  	</div>
	  	</div>
  	</div>
@stop


@section('jsScript')
	<script>
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	function getKegs(id){
		var url = '/api/keg/'+id;

		/** jQuery getJson Call */
		$.getJSON(url,function(results){
			return results.data;
		}).fail(function() {
			activate_alert('Could not load keg');
		});
	}
	</script>
	
@show
