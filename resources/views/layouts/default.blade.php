<?php
/*
|--------------------------------------------------------------------------
| Default Layout Template for KegData.com
|--------------------------------------------------------------------------
|
| This is the default template seen on the all access pages of kegdata.com
| 
| February 2015 V1.0
*/
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title>KegData :: @yield('title')</title>
		<link rel="apple-touch-icon-precomposed" href="<?php echo  asset('/img/icon/favicon.ico') ?>">
<link rel="shortcut icon" href="<?php echo  asset('/img/icon/favicon.ico') ?>">


		<!-- <link type="image/x-icon" href="{{ URL::to('/img/icon/favicon.ico') }}" rel="shortcut icon"> -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="description" content="@section('meta-description') KegData is the most advanced monitoring system for beer kegs. @show">
		<meta name="keywords" content="@section('meta-keywords')
		beer,Beer Inventory Control,Beer inventory management, Beer Control, Beer Controls,beer flowmeter,Beer Flow Meter, Beer Flow Meters,Beverage Management System,  Beer Meter,beer management, Beer Monitoring, Beer Tracking, Beersaver,beverage dispensing equipment, Draft Beer Control, Draft Beer Control System, Draft Beer Controls, Draft Beer Meter, Draft Beer Meters, Draft Beer keg tracking,flowmeter,keg,kegbot,keg beer,keg beer monitoring,keg beer management,keg management,keg systems,ikeg,kegdata,micros,how to monitor the level of beer in a keg,how much beer is left in a keg, how much beer is left in my keg, kegmeter, keg meter, keg smartstrip,Beer Control, Beer Controls, Beer Flow Meter, Beer Flow Meters, Beer Meter, Beer Monitoring, Beer Tracking, Beersaver, Draft Beer Control, Draft Beer Control System, Draft Beer Controls, Draft Beer Meter, Draft Beer Meters, Draft Beer Tracking,Beer Control, Beer Control System, Beer Controls, Beer Flow Meter, Beer Flow Meters, Beer Flowmeter, Beer Inventory Control, Beer Monitoring, Beer Tap Control, Beverage Control System, Draft Beer Control, Draft Beer Flow Meter, Draft Beer Metering, Draft Beer Monitoring, Draft Beer Monitoring System,Beer Control, Beer Inventory Control, Beer Tap Control, Beer Control System, Beer Control, beer level, beer saver, beer savers, usbeersavers, usbeersaver, keg level monitiring, keg level monitoring systems, kegbot, kegbots, tapmate, Draft Beer Monitoring System,Beerboard,US Beverage Net, Beer Monitoring System, craft menu, beer chalkboard, beer menu, draft menu, draft sign, beer signage, digital beer menu, digital beerd isplay, beerboard,keg meter monitoring system,beer keg monitor, monitor keg volume,keg beer thermometer monitor
		@show">
		<link rel="SHORTCUT ICON" href="favicon.ico" />
		{!! HTML::style('css/custom-bootstrap.css') !!}
		{!! HTML::style('css/font-awesome.min.css') !!}
		{!! HTML::script('js/modernizr-2.6.2.min.js') !!}
	</head>
	<body>
	<header id="page-header">
		<nav class="navbar navbar-default">
			<div class="container">
			
			<div class="row">	
				<div class="col-md-9">
					<div class="logo" role="banner">
						<a href="/"><img src="/img/kegdata.png" alt="KegData.com" title="KegData.com" /></a>
					</div>


					<ul  class="nav navbar-nav" role="menu">
						<li role="menu-item"><a href="/">Home</a></li>
						<li role="menu-item"><a href="/about-keg-data">How Keg Data Works</a></li>
						<li role="menu-item"><a href="/about-us">Company</a></li>
						<li role="menu-item"><a href="/support">Support</a></li>
						<li role="menu-item"><a href="/faq">FAQ</a></li>
						<li role="menu-item"><a href="/install">Install</a></li>
						 <!-- <li role="menu-item"><a href="/store/stripe">Browse Shop</a></li> -->
						<li role="menu-item"><a href="/register">Register</a></li>
						<li role="menu-item"><a href="/contact-us">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-md-3" id="welcome-block">
					<div class="social-sidebar hidden-print hidden-xs">
						<a href="https://www.facebook.com/kegdata" class="fa fa-facebook-square"></a>
						<a href="https://www.twitter.com/@kevinbeal6" class="fa fa-twitter-square"></a>
					</div>
					@yield('login-block')
				</div>
			</div>
			</div>
		</nav>
	</header>
	<div class="{{{$bodyClass or 'container'}}}">
		<div class="row">
			<div id="global-errors" class="alert col-sm-6 col-sm-offset-3 clearfix hide" role="alert">
				<p id="error-message" class="pull-left"></p>
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
		</div>
		<div class="clearfix">
			@yield('content')
			@yield('sidebar')
		</div>

	</div>
	<div class="container">
		<footer class="text-center">
		<small>
	        	Copyright&nbsp; Ikeg, LLC 2015 All rights reserved&nbsp;&nbsp;&nbsp;
	        	</small>
			<a href="http://www.freecounterstat.com" title="html hit counter code"><img src="http://counter6.statcounterfree.com/private/freecounterstat.php?c=17afb6208febd6c2d539357188de6ab8" border="0" title="html hit counter code" alt="html hit counter code"></a>
		</footer>
	</div>
	<!--JS @ bottom of page for load-->
	{!! HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') !!}
	<script>
		window.jQuery || document.write('<script type="text/javascript" src="js/jquery-1.11.1.min.js"><\/script>');
	</script>
	{!! HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') !!}
	{!! HTML::script('js/kegdata.js') !!}
	{!! HTML::script('js/json2.js') !!}
	<script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="js/bootstrap.min.js"><\/script>')}</script>
	<script type="text/javascript">
		function activate_alert(msg, type){
			type = typeof type !== 'undefined' ? type : 'danger';
			$('#error-message').append(msg);
			$('#global-errors').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success');
			$('#global-errors').addClass('alert-' + type);
			$('#global-errors').removeClass('hide').fadeIn(500).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
			$('#global-errors').alert();
		}
	</script>

	@yield('jsScript')
 		
	</body>
</html>

