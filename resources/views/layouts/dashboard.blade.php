<?php
/*
|--------------------------------------------------------------------------
| Default Layout Template for KegData.com
|--------------------------------------------------------------------------
|
| This is the default template seen on the all access pages of kegdata.com
| 
| February 2015 V1.0
*/
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title>KegData :: @yield('title')</title>
		<link rel="apple-touch-icon-precomposed" href="<?php echo  asset('/img/icon/favicon.ico') ?>">
		<link rel="shortcut icon" href="<?php echo  asset('/img/icon/favicon.ico') ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta name="description" content="@section('meta-description') KegData is the most advanced monitoring system for beer kegs. @stop">
		<meta name="keywords" content="@section('meta-keywords')
		beer,Beer Inventory Control,Beer inventory management, Beer Control, Beer Controls,beer flowmeter,Beer Flow Meter, Beer Flow Meters,Beverage Management System,  Beer Meter,beer management, Beer Monitoring, Beer Tracking, Beersaver,beverage dispensing equipment, Draft Beer Control, Draft Beer Control System, Draft Beer Controls, Draft Beer Meter, Draft Beer Meters, Draft Beer Tracking,flowmeter,keg,kegbot,keg beer,keg beer monitoring,keg beer management,keg management,keg systems,ikeg,kegdata,micros
		@stop">
		<meta name="csrf-token" content="{{{ csrf_token() }}}">
		<link rel="SHORTCUT ICON" href="/favicon.ico" />
		{!! HTML::style('css/custom-bootstrap.css') !!}
		{!! HTML::style('css/font-awesome.min.css') !!}
		{!! HTML::style('css/bootstrap-datetimepicker.min.css') !!}
		{!! HTML::script('js/modernizr-2.6.2.min.js') !!}
	</head>
	<body>
	<header id="dashboard-header">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			
			<div class="row">	
				<div class="col-md-4 col-sm-7">
					<div class="logo navbar-brand" role="banner">
						<a href="/"><img src="/img/kegdata.png" alt="KegData.com" title="KegData.com" height="35px"/></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 text-center hidden-sm hidden-xs">
					<h4 id="welUser">Welcome {{ $user->firstName }} {{ $user->lastName }}!</h4>
					<small>Viewing Account {{ $account->accountName }}</small>
				</div>
				<div class="col-md-4 col-sm-1"  id="welcome-block">
					<button class="btn btn-primary menu-button pull-right" data-toggle="logout-btns">
						<span class="sr-only">Expand Menu</span>
						<span class="fa fa-bars"></span>
					</button>
					<div id="logout-btns" class="btn-group btn-group-xs">
						<a href="/" class="btn btn-primary text-center">Home</a>
						<a href="/dashboard" class="btn btn-primary text-center">Dashboard</a>
						@if( in_array( 'Account Admin', $roles ) )
							<a href="/employee" class="btn btn-primary text-center">Employee</a>
						@endif
						<div class="btn-group btn-group-xs">
						  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						    Help<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li><a href="/support">Support</a></li>
						    <li><a href="/faq">FAQ</a></li>
						    <li><a href="/contact-us">Contact Us</a></li>
						  </ul>
						</div>
						<!--a href="/store/plans" class="btn btn-primary">Browse Shop</a-->
						<a href="/auth/logout" class="btn btn-primary" id="loginBtn">Logout</a>
					</div>
				</div>
			</div>
			</div>
		</nav>
	</header>
	@unless($verification && $verification->verificationConfirmation)
		<div class="bg-danger text-center" id="verifyBox" role="alert">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
					<button type="button" class="close right" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<p id="verificationMessage">
							<strong>Your account has not been verified.</strong>
							@if(!empty($verification))
							<strong>The verification email was sent {{ $verification->updated_at->diffForHumans() }}.</strong>
							@endif
							<strong>Please check your email for the verification link.</strong>
							@if(empty($verification) || $verification->updated_at->diffInMinutes() > 30)
								<br /><strong>Did something go wrong with the email? <button class="btn btn-xs btn-danger" id="resendVerification">Resend Verification</button></strong>
							@endif
						</p>
					</div>
				</div>
			</div>
		</div>
	@endunless
	<div class="container-fluid" id="dashboard">
		<div class="row">
			<div id="global-errors" class="alert col-sm-6 col-sm-offset-3 clearfix hide" role="alert">
				<p id="error-message" class="pull-left"></p>
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
		</div>
		<div class="clearfix">
			<ul class="nav nav-tabs nav-justified" id="employee-tabs" role="tablist">
	  			@yield('tabs')
  			</ul>
  			<div class="container-fluid">
	  			<div class="tab-content col-md-12" style="inline-block">
		  			<div class="tab-pane active" id="levels">
		  				@yield('tabpane')
		  			</div>
	  			</div>
  			</div>
		</div>

	</div>
	<div class="container">
		<footer class="text-center">
			<font size="1" face="Arial">
	        	Copyright&nbsp; Ikeg, LLC 2014 All rights reserved&nbsp;&nbsp;&nbsp;
	        </font>
			<a href="http://www.freecounterstat.com" title="html hit counter code"><img src="http://counter6.statcounterfree.com/private/freecounterstat.php?c=17afb6208febd6c2d539357188de6ab8" border="0" title="html hit counter code" alt="html hit counter code"></a>
		</footer>
	</div>
	
	<!-- AJAX MODAL -->
	<div class="modal fade" id="ajaxModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-body">
	        <p>Please wait while we complete your action...</p>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!--JS @ bottom of page for load-->
	{!! HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') !!}
	<script>
		window.jQuery || document.write('<script type="text/javascript" src="js/jquery-1.11.1.min.js"><\/script>');
	</script>
	{!! HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') !!}
	{!! HTML::script('js/kegdata.js') !!}
	{!! HTML::script('js/json2.js') !!}
	<script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="js/bootstrap.min.js"><\/script>')}</script>
	<script type="text/javascript">
		function activate_alert(msg, type){
			type = typeof type !== 'undefined' ? type : 'danger';
			$('#error-message').append(msg);
			$('#global-errors').removeClass('alert-danger').removeClass('alert-warning').removeClass('alert-success');
			$('#global-errors').addClass('alert-' + type);
			$('#global-errors').removeClass('hide').fadeIn(500).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
			$('#global-errors').alert();
		}
	</script>
	<script>
		$(document).on({
		    ajaxStart: function() { 
		    	//$('#ajaxModal').modal('show');
		    },
		    ajaxStop: function() { 
		    	//$('#ajaxModal').modal('hide'); 
		    }    
		});
	</script>
	@unless($verification && $verification->verificationConfirmation)
		<script>
			$(document).ready(function(){
				$('#resendVerification').on('click', function(){
					@if(empty($verification))
					var url = "/verification/{{ $user->id }}"; 
					@else
					var url = "/verification/{{ $user->id }}/{{ $verification->verificationHash }}"; 
					@endif
					$.ajax(url).done(function( data ){
						if(data == 'Mail Spam'){
							$('#verificationMessage').html('<strong>Please check your email. We just sent one to for you to verify your account with.</strong>');
						}else if(data == 'Mail Failure'){
							$('#verificationMessage').html('<strong>For some reason, the email failed to send to {{ $user->email }}. Do you need to update your email address first?');
						}else if(data == 'Mail Success'){
							$('#verificationMessage').html('<strong>Please check your email to verify your account.</strong>');
						}
					}).fail(function(error){
						$('#verificationMessage').html('<strong>Something went wrong on our server. Please try again in a bit or contact support. Thanks!');
					});
				});
			});
		</script>
	@endunless
	@yield('jsScript')
 		
	</body>
	<script>
		$(document).ready(function(){
			//three minute call
			//setInterval(function(){ insert_lsm_values_data(); }, 20000);
		});

		function insert_lsm_values_data() {
			//alert("Hello");
			var url = "/dashboard/calling_console_via_ajax/user/abc";
			$.ajax(url,{'type' : 'get'}).done(
					function(results){
					//console.log(results);
				}).fail(function(jqXHR, textStatus){
					//activate_alert(textStatus);
				});
		}
	</script>
</html>

