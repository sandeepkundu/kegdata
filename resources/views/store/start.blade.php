@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')


{{-- */ $count = 0;/* --}}





<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store">KegData&trade; Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
			<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
															<li><a href="/store/stripe/systems">Systems<span class="sr-only">(current)</span></a></li>
											<li><a href="/store/stripe/couplers">Couplers<span class="sr-only">(current)</span></a></li>
											<li><a href="/store/stripe/*">Power Distribution<span class="sr-only">(current)</span></a></li>
											<li><a href="/store/stripe/w">Wires<span class="sr-only">(current)</span></a></li>
													<!-- <li class='active'><a href="/store/stripe">All Products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>

<article class="start">
<div class="row">

	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title text-center">New Customer</h3></div>
			<div class="panel-body">
			<p>
			If this is your first time here then here are a few tips.
			</p>
			<p>
			#1 Make sure to select a base plan from the "Plans" page so that we know how many couplers your account will support.
			</p>
			<p>
			#2 Make sure to add a system to your shopping cart so that you have all of the parts and items needed to link your Keg online.
			</p>
		</div>
		<div class="panel-footer">
			<a  class="btn btn-primary" href="/store/plans" style="margin-left:50px;width:65%">Click Here To Select Plan</a>
		</div>
       </div>
	</div>


	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title text-center">Existing Customer</h3></div>
			<div class="panel-body">
			<p>
		If you're an existing customer and need to update your plan or purchase additional items then please click the login button below. 	
		</p>
		</div><br><br><br><br><br><br>
		<div class="panel-footer">
			<a id="loginBtn" class="btn btn-primary" href="/auth/login" style="margin-left:50px;width:65%">Login</a>
		</div>
       </div>
	</div>



	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title text-center">Existing Customer: Need Additional System</h3></div>
			<div class="panel-body">
			<p>
			If you've already purchased a system but need an additional system then please register for a new account.  Each system requires it's own separate account.
		</p>
		</div><br><br><br><br><br>
		<div class="panel-footer">
			<a  class="btn btn-primary" href="/register" style="margin-left:50px;width:65%">Register For Another Account</a>
		</div>
       </div>
	</div>











</div>




	
</article>
<br><br><br><br><br><br>

@stop
