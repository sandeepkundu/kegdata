@section('title')
Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
@include('components.guestBlock')
@stop

@section('content')
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li class="active"><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $category)
				<li><a href="/store/stripe/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif($categories)
				<!-- <li><a href="/store/stripe">All products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>


<article class="store">
	<h2>Subscription Plans</h2>
	

	@if(Session::has('msg'))
	

	<div class="alert alert-success">
		
		<p>{{Session::get('msg')}}</p>
		
	</div>
	@endif
	<div class="alert alert-success">
		<p>Any purchase of a {{ Config::get('constants.COMPANY_NAME') }} system or coupler requires a subscription for keg monitoring. Subscriptions are billed on a monthly basis 14 days after initial purchase. The subscription plan is based on the number of couplers purchased.</p>
	</div>
	<div class="row">
		
		@foreach($plans as $plan)

		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="panel price">
				<div class="panel-heading text-center">
					<h4>{{ $plan->name }}</h4>
				</div>
				<div class="panel-body text-center">
					<p  class="text-center"> <strong id="price{{$plan->plan_type}}">${{ $plan->price }} </strong>
						@if($plan->is_quantity)
						<br><i class="fa fa-plus"></i><span class="sr-only">+</span>
						<strong>${{ $plan->quantity_price }} per coupler</strong>
						@endif
						<br> <span class="pricing-date">per {{ $plan->interval }}</span>
					</p>

					<?php

					$name  =  $plan->name; 
					$match_name = preg_match("/Residential/i", $name)	;
					
					?>
					<div class="text-center ">
					<form    name="add-product-{{ $plan->id }}" action="/store/cart/add/plan" method="post" >
					<!-- <input id="inp{{$plan->plan_type}}" style="width:40%" <?php if($match_name){ echo "max=2 min=0 value=0"; } else{ echo "min=0 value=0 " ;}  ?> class="" type="number" name="plan_quantity"  ?> -->
				</div>
				</div>
				
				<p class="lead text-center">
					{!! $plan->short_description !!}
				</p>
				{!! $plan->description !!}

				<div class="panel-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 center-block text-center">
							
								<!-- csrf_field() --> 
								
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="id" value="{{ $plan->id }}" />
							
								<input type="hidden" id="pname{{ $plan->plan_type }}" name="pname" value="{{ $plan->plan_type }}" />

								<!--  <input  type="submit" value="Submit"> -->
								<button id="sub-btn" class="btn btn-info" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>

	
	@if($subscription > 0)
	<p class="text-right">
		<a class="btn btn-primary" href="/store/stripe/systems"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else 
	<p class="text-right">
		<a class="btn btn-primary disabled" href="#">Next<i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif 
</article>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li class="active"><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $category)
				<li><a href="/store/stripe/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif
				<!-- <li><a href="/store/stripe">All products</a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>



<script>

jQuery( document ).ready(function() {

	

    $('#inpResidential').on('change', function() {
    	if(this.value == 2)
    	{
    		var pr = 14.95;
    		$('#price_total_Residential').val(pr );
    	}
    	else{
    		var pr = 9.95;
    		$('#price_total_Residential').val(pr );
    	}
    	$('#priceResidential').html("$"+ pr );
  		//alert( this.value );
	})

	 $('#inpCommercial').on('change', function() {
	 	var cnt = this.value;
		var pr = 24.95;

	 	if(cnt > 0) {
			//cnt = cnt - 3;
			pr =(24.95+(cnt*2 ) );
			$('#price_total_Commercial').val(pr );
		} else {
			pr = 24.95;
			$('#price_total_Commercial').val(pr );
		}
	
    	$('#priceCommercial').html("$"+ pr );
    	
    	
  		//alert( this.value );
	})


});



		 
function checkValidation(type, max) {
	
	var qty = $('#inpResidential').val();
	var plan_name = $('#pnameResidential').val();
	//var res = plan_name.match(/Residential/gi);
	
	if(type == "Residential"){
		var res_val = $('#inpResidential').val();
		if( res_val == ""  ){
			return false;
		}
		
		if (qty > max || res_val > 2 )	{
			$('#newAlert').css("display", "block");
			document.location.href = "#"; 
			return false;		
		}else{
			return true;
		}
	}else {
		var com_val = $('#inpCommercial').val();
		if(com_val < 3 || com_val == ""  ){
			return false;
		}

		return true;
	}	

}

</script >

@stop