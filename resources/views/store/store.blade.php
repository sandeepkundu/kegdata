@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')


{{-- */ $count = 0;/* --}}


<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
					@foreach($categories as $category)
						<li><a href="/store/stripe/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
					@endforeach
				@endif
				<!-- <li class='active'><a href="/store/stripe">All Products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>

<article class="store">
@if(Session::has('msg'))
<div class="alert alert-success">
	 
        <p>{{Session::get('msg')}}</p>
  
    </div>
	  @endif

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

		<?php
			$systems = $categories->where('slug', 'systems')->first();
		?>
		@if(!is_null($systems) && count($systems) > 0)
			<div class="row">
			<h2>{{ $systems->name }}</h2>
			<?php $count = 0;	?>

				@foreach($systems->products as $product)
					@if($count == 0)
						<div class="row">
					@endif
				@if(!empty($product->stripe_id))
					<div class="col-md-6 product">
						<div class="col-md-10 col-md-offset-1 clearfix">
							<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
							@if(count($product->photos))
							<div class="bg-primary clearfix">
								<div class="col-md-8 col-md-offset-2">
									<img class="img-responsive" src="{{ $product->photos->first()->path }}">
								</div>
							</div>
							@endif
							<h4 class="text-center text-danger">${{ $product->price }}</h4>
								{!! $product->description !!}

								<form name="add-product-{{ $product->id }}" action="/store/cart/add/product" method="post">
									<!--csrf_field()  -->
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="id" value="{{ $product->id }}" />
									<div class="form-group">
										<label for="quantity{{ $product->id }}">Quantity</label>
										<div class="input-group">
											<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
											<div class="input-group-addon has-button">
												<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
											</div>
										</div>
									</div>

								</form>

						</div>
					</div>
					<?php $count++; ?>
				@endif
					@if($count ==2)
						<?php $count = 0; ?>
						</div>
					@endif
				@endforeach
			</div>
		@endif
		<?php
			$couplers = $categories->where('slug', 'couplers')->first();
			$count == 0;
		?>
		@if(!is_null($couplers) && count($couplers) > 0)
			<div class="row">
			<h2>{{ $couplers->name }}</h2>

				@foreach($couplers->products as $product)
				@if($count == 0)
					<div class="row">
				@endif
					@if(!empty($product->stripe_id))
						<div class="col-md-6 product headroom-top">
							<div class="col-md-10 col-md-offset-1 clearfix">
								<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
								@if(count($product->photos))
								<div class="bg-primary clearfix">
									<div class="col-md-8 col-md-offset-2">
										<img class="img-responsive" src="{{ $product->photos->first()->path }}">
									</div>
								</div>
								@endif
								<h4 class="text-center text-danger">${{ $product->price }}</h4>
									{!! $product->description !!}

									<form name="add-product-{{ $product->id }}" action="/store/cart/add/product/" method="post">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="id" value="{{ $product->id }}" />
										<div class="form-group">
											<label for="quantity{{ $product->id }}">Quantity</label>
											<div class="input-group">
												<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
												<div class="input-group-addon has-button">
													<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
												</div>
											</div>
										</div>

									</form>

							</div>
						</div>
						<?php $count++; ?>
					@endif

				@if($count == 2)
					<?php $count=0; ?>
					</div>
				@endif
				@endforeach
			</div>
		@endif
		<?php
			$parts = $categories->where('slug', 'parts')->first();
		?>

		@if(!is_null($parts) && count($parts) > 0)
		<div class="row">
			<h2>{{ $parts->name }}</h2>
			<?php $count = 0; ?>

				@foreach($parts->products as $product)
				@if($count == 0)
					<div class="row">
				@endif
				@if(!empty($product->stripe_id))
					<div class="col-md-4 product headroom-top">
						<div class="col-md-10 col-md-offset-1 clearfix">
							<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
							@if(count($product->photos))
							<div class="bg-primary clearfix">
								<div class="col-md-8 col-md-offset-2">
									<img class="img-responsive" src="{{ $product->photos->first()->path }}">
								</div>
							</div>
							@endif
							<h4 class="text-center text-danger">${{ $product->price }}</h4>
								{!! $product->description !!}

								<form name="add-product-{{ $product->id }}" action="/store/cart/add/product/" method="post">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="id" value="{{ $product->id }}" />
									<div class="form-group">
										<label for="quantity{{ $product->id }}">Quantity</label>
										<div class="input-group">
											<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
											<div class="input-group-addon has-button">
												<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
											</div>
										</div>
									</div>

								</form>

						</div>
					</div>

					<?php $count++; ?>
				@endif

				@if($count == 3)
				<?php $count = 0;?>
					</div>
				@endif
				@endforeach
			</div>
		@endif
</article>

<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $category)
					<li><a href="/store/stripe{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif
				<!-- <li class="active"><a href="/store/stripe">All Products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>
@stop
