@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		<div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store/stripe">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@foreach($categories as $category)
					<li><a href="/store/stripe/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
			<!-- 	<li><a href="/store/stripe">All Products</a></li> -->
				<li class="active"><a>Check Out <i class="fa fa-chevron-circle-right"></i><span class="sr-only">(current)</span></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>


@include('flash::message')
@if (count($errors) > 0)
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with submitting your order<br><br>
				<ul>

					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
@endif

@if($cartCount)
{!!  Form::open(['url'=>'/store/checkout/pay','id' => 'checkout']) !!}
@if(isset($orderID) && !is_null($orderID))
	<input type="hidden" name="orderID" value="{{ $orderID }}" />
@else
	<input type="hidden" name="orderID" value="0" />
@endif
<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-location-arrow"></i> Shipping Address</h3></div>
	<div class="panel-body">
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('name') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="name">Name</label>
					@if(old('name'))
						{!! Form::text('name', old('name'), ['id' => 'name', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('name', $account->accountName, ['id' => 'name', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('name'))
					<span class="help-block">{{ $errors->first('name') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('address1') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="address1">Address 1</label>
					@if(old('address1'))
						{!! Form::text('address1', old('address1'), ['id' => 'address1', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('address1', $accountAddress->address1, ['id' => 'address1', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('address1'))
					<span class="help-block">{{ $errors->first('address1') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('address2') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for='address2'>Address 2</label>
					@if(old('address2'))
						{!! Form::text('address2',old('address2'), ['id' => 'address2', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('address2', $accountAddress->address2, ['id' => 'address2', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('address2'))
					<span class="help-block">{{ $errors->first('address2') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-3{{ !$errors->first('city') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="city">City</label>
					@if(old('city'))
						{!! Form::text('city', old('city'), ['id' => 'city', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('city', $accountAddress->city, ['id' => 'city', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('city'))
					<span class="help-block">{{ $errors->first('city') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('stateCode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="stateCode">State</label>
					@if(old('stateCode'))
						{!! Form::select('stateCode',$states, old('stateCode'), ['class' => 'form-control', 'id' => 'stateCode']) !!}
					@else
						{!! Form::select('stateCode',$states, $accountAddress->stateCode, ['class' => 'form-control', 'id' => 'stateCode']) !!}
					@endif
				</div>
				@if ($errors->first('stateCode'))
					<span class="help-block">{{ $errors->first('stateCode') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('zipcode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="zipcode">Zip Code</label>
					@if(old('zipcode'))
						{!! Form::text('zipcode', old('zipcode'), ['class'=>'form-control', 'placeholder' => 'Zip Code', 'required' => 'required', 'id' => 'zipcode']) !!}
					@else
						{!! Form::text('zipcode', $accountAddress->zipcode, ['class'=>'form-control', 'placeholder' => 'Zip Code', 'required' => 'required', 'id' => 'zipcode']) !!}
					@endif
				</div>
				@if ($errors->first('zipcode'))
					<span class="help-block">{{ $errors->first('zipcode') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('countryCode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="countryCode">Country</label>
					@if(old('countryCode'))
						{!! Form::select('countryCode',$countries, old('countryCode'), ['class' => 'form-control', 'required' => 'required', 'id' => 'countryCode']) !!}
					@else
						{!! Form::select('countryCode',$countries, $accountAddress->countryCode, ['class' => 'form-control', 'required' => 'required', 'id' => 'countryCode']) !!}
					@endif
				</div>
				@if ($errors->first('countryCode'))
					<span class="help-block">{{ $errors->first('countryCode') }}</span>
				@endif
			</div>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-location-arrow"></i> Billing Address </h3></div>
	<div class="panel-body">
		<div class="form-row">
			<div class="col-md-12">
				<label>{!! Form::checkbox('useShipping', '1', false, ['id' => 'useShipping']) !!} Same as Shipping Address</label>
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('billing_name') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_name">Name</label>
					@if(old('billing_name'))
						{!! Form::text('billing_name', old('name'), ['id' => 'billing_name', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('billing_name', $account->accountName, ['id' => 'billing_name', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('billing_name'))
					<span class="help-block">{{ $errors->first('billing_name') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('billing_address1') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_address1">Address 1</label>
					@if(old('billing_address1'))
						{!! Form::text('billing_address1', old('address1'), ['id' => 'billing_address1', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('billing_address1', $accountAddress->address1, ['id' => 'billing_address1', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('billing_address1'))
					<span class="help-block">{{ $errors->first('billing_address1') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-12{{ !$errors->first('billing_address2') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for='address2'>Address 2</label>
					@if(old('billing_address2'))
						{!! Form::text('billing_address2',old('address2'), ['id' => 'billing_address2', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('billing_address2', $accountAddress->address2, ['id' => 'billing_address2', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('billing_address2'))
					<span class="help-block">{{ $errors->first('billing_address2') }}</span>
				@endif
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-3{{ !$errors->first('city') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_city">City</label>
					@if(old('billing_city'))
						{!! Form::text('billing_city', old('city'), ['id' => 'billing_city', 'class' => 'form-control']) !!}
					@else
						{!! Form::text('billing_city', $accountAddress->city, ['id' => 'billing_city', 'class' => 'form-control']) !!}
					@endif
				</div>
				@if ($errors->first('city'))
					<span class="help-block">{{ $errors->first('city') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('billing_stateCode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_stateCode">State</label>
					@if(old('billing_stateCode'))
						{!! Form::select('billing_stateCode',$states, old('stateCode'), ['class' => 'form-control', 'id' => 'billing_stateCode']) !!}
					@else
						{!! Form::select('billing_stateCode',$states, $accountAddress->stateCode, ['class' => 'form-control', 'id' => 'billing_stateCode']) !!}
					@endif
				</div>
				@if ($errors->first('billing_stateCode'))
					<span class="help-block">{{ $errors->first('billing_stateCode') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('billing_zipcode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_zipcode">Zip Code</label>
					@if(old('billing_zipcode'))
						{!! Form::text('billing_zipcode', old('billing_zipcode'), ['class'=>'form-control', 'placeholder' => 'Zip Code', 'required' => 'required', 'id' => 'billing_zipcode']) !!}
					@else
						{!! Form::text('billing_zipcode', $accountAddress->zipcode, ['class'=>'form-control', 'placeholder' => 'Zip Code', 'required' => 'required', 'id' => 'billing_zipcode']) !!}
					@endif
				</div>
				@if ($errors->first('billing_zipcode'))
					<span class="help-block">{{ $errors->first('billing_zipcode') }}</span>
				@endif
			</div>
			<div class="col-md-3{{ !$errors->first('billing_countryCode') ? '' : ' has-error has-feedback' }}">
				<div class="form-group">
					<label for="billing_countryCode">Country</label>
					@if(old('billing_countryCode'))
						{!! Form::select('billing_countryCode',$countries, old('billing_countryCode'), ['class' => 'form-control', 'required' => 'required', 'id' => 'billing_countryCode']) !!}
					@else
						{!! Form::select('billing_countryCode',$countries, $accountAddress->countryCode, ['class' => 'form-control', 'required' => 'required', 'id' => 'billing_countryCode']) !!}
					@endif
				</div>
				@if ($errors->first('billing_countryCode'))
					<span class="help-block">{{ $errors->first('billing_countryCode') }}</span>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Cart <span class="pull-right">{{ $cartCount }} items</span></h3></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
		{{--
		{!! Form::open(['url' => '/store/cart/promo', 'id' => 'promoForm', 'class' => 'form-inline']) !!}
			<div class="form-group{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
				<label for="slug">Promotion Code</label>
				<input type="text" class="form-control" id="slug" name="slug" placeholder="Promo Code">
				<button type="submit" class="btn btn-default">Apply Promotion</button>
				@if ($errors->first('slug'))
					<span class="help-block">{{ $errors->first('slug') }}</span>
				@endif
			</div>

		{!! Form::close() !!}
		--}}
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>Subscription</th>
				<th class="text-right">Rate</th>
				<th>Term</th>
				<th class="text-right">Delete</th>
			</tr>
		</thead>
		<tbody>
		
			<?php $new_price = 0 ;?>
						@foreach($cartSub as $row)
						<?php $new_price =  $row->price + ($kegcount * $row->options->quantity_price) ;?>
			<tr>
				<td>
					<p><strong>{{ $row->name }}</strong></p>
				</td>
				
				<td class="text-right">${{ $row->price }}
					@if($row->options->is_quantity)
						+ ${{ ($kegcount * $row->options->quantity_price) }} <small>For {{ $kegcount }} Kegs</small>
					=
					${{ $row->price + ($kegcount * $row->options->quantity_price) }}
					@endif

				</td>
				<td>per {{ $row->options->term }}</td>
				<td class="text-right"><a href="/store/cart/remove/subscription/{{ $row->rowid }}" class="btn btn-danger"><i class="fa fa-times"></i><span class="sr-only">Remove  {{ $row->name }}</span></a></td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<table class="table">
		<thead>
			<tr>
				<th>Product</th>
				<th class="text-right">Item Price</th>
				<th>Qty</th>
				<th class="text-right">Subtotal</th>
				<th class="text-right">Delete</th>
			</tr>
		</thead>

		<tbody>

			@foreach($cartItems as $row)

			<tr>
				<td>
					<p><strong>{{ $row->name }}</strong></p>
				</td>
				<td class="text-right">${{ $row->price }}</td>
				<td><input type="hidden" name="{{ $row->rowid }}" value="{{ $row->qty }}"> {{ $row->qty }} </td>
				<td class="text-right">${{ number_format($row->subtotal, 2) }}</td>
				<td class="text-right"><a href="/store/cart/remove/shopping/{{ $row->rowid }}" class="btn btn-danger"><i class="fa fa-times"></i><span class="sr-only">Remove  {{ $row->name }}</span></a></td>
			</tr>

			@endforeach

		</tbody>
		<tfoot>
				<tr>
					<th>Subtotal</th>
					<td colspan="3" class="text-right">${{ number_format($cartTotal+$new_price,2) }}</td>
					<td></td>
				</tr>
				<tr>
					<th>Tax</th>
					<td colspan="3" class="text-right">+ ${{ number_format($tax, 2) }}
					</td>
					<td></td>
				</tr>
				<tr>
					<th>Shipping</th>
					<td colspan="3" class="text-right"> + ${{ number_format($shipping, 2) }}</td>
					<td></td>
				</tr>
				<tr class="bg-success">
					<th >Grand Total</th>
					<td colspan="3" class="text-right"><strong>${{ number_format(($tax + $shipping + $cartTotal +$new_price), 2)  }}</strong></td>
					<td></td>
				</tr>
		</tfoot>
	</table>
{!! Form::close() !!}
	<div class="panel-footer">
		<!-- check if system have system already -->
		@if($sys_count > 0) 
			<div class='alert alert-success col-md-12'>
				<p>Checkout not possible for! Your account already has 1 system assigned.  To purchase additional systems please log out of your current account and register a new account!</p>
			</div>&nbsp;
		@else
		<div class="text-right">
			<button class="btn btn-primary" data-toggle="modal" data-target="#stripeCheckout" type="button">Pay with card!</button>
		</div>
		@endif
	</div>
</div>

<div class="modal fade" id="stripeCheckout" tabindex="-1" role="dialog" aria-labelledby="Pay with card">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		<form action="/store/checkout/pay" method="POST" id="payment-form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-h
					idden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">{{ Config::get('constants.COMPANY_NAME') }} Checkout with Credit Card!</h4>
			</div>
			<div class="modal-body">
				
				<p>Any purchase of a {{ Config::get('constants.COMPANY_NAME') }} system or coupler requires a subscription for keg monitoring. Subscriptions are billed on a monthly basis 14 days after initial purchase. The subscription plan is based on the number of couplers purchased.</p>

				<div class="alert alert-info">
					<p>Your card will be charged <strong>${{ number_format(($tax + $shipping + $cartTotal), 2) }}</strong> today. </p>
					@foreach($cartSub as $row)
					<p>After the trial period, your card will be charged <strong>@if($row->options->is_quantity)
								${{ number_format($row->price + ($kegcount * $row->options->quantity_price),2) }}
						   @else
						   		${{ number_format($row->price,2) }}
						@endif</strong> and then will be charged <strong>@if($row->options->is_quantity)
									${{ number_format($row->price + ($kegcount * $row->options->quantity_price),2) }}
							   @else
							   		${{ number_format($row->price,2) }}
							@endif</strong> every <strong>{{ $row->options->term }}</strong> after unless you cancel your subscription</p>
					@endforeach
				</div>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="payment-errors alert alert-danger hidden"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
						<div class="well">
							<div class="row">
								<div class="form-group">
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
										<label for="number"><i class="fa fa-credit-card"></i> CARD NUMBER</label>
										<input type="text" size="20" data-stripe="number" class="form-control" />
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
										<label for="cvc">CVC</label>
										<input type="text" size="4" data-stripe="cvc"  class="form-control" />
									</div>
								</div>
							</div>
							<div class="row voffset3">
								<div class="form-group">
									<div class="col-lg-4">
										<label for="month">MONTH</label>
										<select data-stripe="exp-month"  class="form-control" >
											<option value="01"{{ ($now->month == 1) ? ' selected' : ''}}>01</option>
											<option value="02"{{ ($now->month == 2) ? ' selected' : ''}}>02</option>
											<option value="03"{{ ($now->month == 3) ? ' selected' : ''}}>03</option>
											<option value="04"{{ ($now->month == 4) ? ' selected' : ''}}>04</option>
											<option value="05"{{ ($now->month == 5) ? ' selected' : ''}}>05</option>
											<option value="06"{{ ($now->month == 6) ? ' selected' : ''}}>06</option>
											<option value="07"{{ ($now->month == 7) ? ' selected' : ''}}>07</option>
											<option value="08"{{ ($now->month == 8) ? ' selected' : ''}}>08</option>
											<option value="09"{{ ($now->month == 9) ? ' selected' : ''}}>09</option>
											<option value="10"{{ ($now->month == 10) ? ' selected' : ''}}>10</option>
											<option value="11"{{ ($now->month == 11) ? ' selected' : ''}}>11</option>
											<option value="12"{{ ($now->month == 12) ? ' selected' : ''}}>12</option>
										</select>
									</div>
									<div class="col-lg-6">
										<label for="year">YEAR</label>
										<select data-stripe="exp-year"  class="form-control" >
										@for($i = 0; $i < 10; $i++)
											<option value="{{ $now->year + $i }}">{{ $now->year + $i }}</option>
										@endfor
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit Payment</button>
					</div>
				</div>

				<table class="table">
					<thead>
						<tr>
							<th>Subscription</th>
							<th class="text-right">Rate</th>
							<th>Term</th>
						</tr>
					</thead>
					<tbody>
						<?php $new_price = 0 ;?>
						@foreach($cartSub as $row)
						<?php  $new_price =  $row->price + ($kegcount * $row->options->quantity_price) ;?>
						<tr>
							<td>
								<p><strong>{{ $row->name }}</strong></p>
							</td>
							<td class="text-right">${{ $row->price }}
								@if($row->options->is_quantity)
									+ ${{ ($kegcount * $row->options->quantity_price) }} <small>For {{ $kegcount }} Kegs</small>
								=
								${{ $row->price + ($kegcount * $row->options->quantity_price) }}
								@endif

							</td>
							<td>per {{ $row->options->term }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				<table class="table">
					<thead>
						<tr>
							<th>Product</th>
							<th class="text-right">Item Price</th>
							<th>Qty</th>
							<th class="text-right">Subtotal</th>
						</tr>
					</thead>

					<tbody>

						@foreach($cartItems as $row)
						
						<tr>
							<td>
								<p><strong>{{ $row->name }}</strong></p>
							</td>
							<td class="text-right">${{ $row->price }}</td>
							<td><input type="hidden" name="{{ $row->rowid }}" value="{{ $row->qty }}"> {{ $row->qty }} </td>
							<td class="text-right">${{ number_format($row->subtotal, 2) }}</td>

						</tr>

						@endforeach

					</tbody>
					<tfoot>
							<tr>
								<th>Subtotal</th>
								<td colspan="3" class="text-right">+ {{ number_format($cartTotal +$new_price ,2) }}</td>
							</tr>
							<tr>
								<th>Tax</th>
								<td colspan="3" class="text-right">+ ${{ number_format($tax, 2) }}
								</td>
							</tr>
							<tr>
								<th>Shipping</th>
								<td colspan="3" class="text-right"> + ${{ number_format($shipping, 2) }}</td>
							</tr>
							<tr class="bg-success">
								<th>Grand Total</th>
								<td colspan="3" class="text-right"><strong>${{ number_format(($tax + $shipping + $cartTotal + $new_price), 2)  }}</strong></td>
							</tr>
					</tfoot>
				</table>


			</div>

		</form>
		</div>
	</div>
</div>
@else
	<div class="panel panel-primary">
	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Checkout <span class="pull-right">{{ $cartCount }} items</span></h3></div>
	<div class="panel-body">
		<p class="lead">You have no items in the cart! Return to the <a href="/store/plans">Store <span class="fa fa-chevron-right"></span></a></p>
	</div>
	</div>
@endif
<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@foreach($categories as $category)
					<li><a href="/store/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				<!-- <li><a href="/store">All Products<span class="sr-only">(current)</span></a></li> -->
				<li class="active"><a>Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>
@stop

@section('jsScript')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	@include('store.checkoutjs')

@stop
