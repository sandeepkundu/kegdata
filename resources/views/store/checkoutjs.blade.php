<script>
$(document).ready(function(){
	$('#useShipping').on('change', function(){
		if($('#useShipping').prop('checked')){
			$('#billing_name').val($('#name').val());
			$('#billing_address1').val($('#address1').val());
			$('#billing_address2').val($('#address2').val());
			$('#billing_city').val($('#city').val());
			$('#billing_stateCode').val($('#stateCode').val());
			$('#billing_zipcode').val($('#zipcode').val());
			$('#billing_countryCode').val($('#countryCode').val());
		}else{
			$('#billing_name').val('');
			$('#billing_address1').val('');
			$('#billing_address2').val('');
			$('#billing_city').val('');
			$('#billing_stateCode').val('');
			$('#billing_zipcode').val('');
			$('#billing_countryCode').val('');
		}
	});


});

</script>

<script type="text/javascript">
  // This identifies your website in the createToken call below
  $(document).ready(function(){

	Stripe.setPublishableKey('{{ env('STRIPE_API_PUBLISH') }}');

	jQuery(function($) {
		$('#payment-form').submit(function(event) {

			var $form = $(this);

			// Disable the submit button to prevent repeated clicks
			$form.find('button').prop('disabled', true);

			Stripe.card.createToken($form, stripeResponseHandler);

			// Prevent the form from submitting with the default action
			return false;
		});
	});

	function stripeResponseHandler(status, response) {
		var $form = $('#payment-form');
		var $checkoutForm = $('#checkout');

		if (response.error) {
			// Show the errors on the form
			$form.find('.payment-errors').text(response.error.message).removeClass('hidden');
			$form.find('button').prop('disabled', false);
		} else {
		// response contains id and card, which contains additional card details
		var token = response.id;
		//alert(token);
		// Insert the token into the form so it gets submitted to the server
		$checkoutForm.append($('<input type="text" name="stripeToken" />').val(token));
		// and submit
		$checkoutForm.get(0).submit();
		}
	}

  });
</script>
