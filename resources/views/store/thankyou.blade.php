@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')

@include('flash::message')
<h2>Thank you for your purchase!</h2>


<div class="row">
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title">Order Details</h3></div>
			<div class="panel-body">
			@if(!is_null($order->orderItem))
				<h4>Charged today: <span class="right">${{ number_format($order->total,2) }}</span></h4>
				<table class="table">
					<thead>
						<tr>
							<th>Product</th>
							<th>Item Price</th>
							<th>Quantity</th>
							<th class="text-right">Subtotal</th>
						</tr>
					</thead>
					<tbody>
						<?php $new_total = 0;?>
					@foreach($order->orderItem as $item)
						<tr>
							<td>{{ $item->product->name }}</td>
							<td>${{ $item->price }}</td>
							<td>{{ $item->quantity }}</td>

							<?php 
							$new_total =  $new_total + $item->price*$item->quantity ;
							 ?>
							<td class="text-right">${{   number_format( $item->price*$item->quantity, 2) }}</td>
						</tr>
					@endforeach
					<tr>
						<th colspan="3">tax</th>
						<td class="text-right">${{ number_format($order->tax, 2) }}</td>
					</tr>
					<tr>
						<th colspan="3">Shipping</th>
						<td  class="text-right">${{ number_format($order->shipping,2) }}</td>
					</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3">Total</th>
							<td class="text-right">${{ number_format($new_total + $order->shipping 	+ $order->tax , 2) }}</td>
						</tr>
					</tfoot>
				</table>
			@endif

			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel price">
			<div class="panel-heading text-center">
				<h4>Billing/Shipping Details</h4>
			</div>
			<div class="panel-body">
				@foreach($order->orderAddress as $address)
				<div class="col-md-6">
				@if($address->type == 'B')
					<h5><strong>Billing Address</strong></h5>
				@else
					<h5><strong>Shipping Address</strong></h5>
				@endif
				<address>
					{{ $address->name }}<br />
					{{ $address->address1 }}<br />
					@if(!empty($address->address2))
						{{ $address->address2 }}<br />
					@endif
					{{ $address->city }}, {{ $address->state }} {{ $address->zipcode }}
				</address>
				</div>
				@endforeach
			</div>
		</div>
		@if(!is_null($order->subscription))
			<div class="panel price">
				<div class="panel-heading text-center">
					<h4>Subscription Details</h4>
				</div>
				<div class="panel-body text-center">
					<h4>{{ $order->subscription->plan->name }}</h4>
					<p class="text-center"> <strong>${{ $order->subscription->plan->price }} </strong>
					<br><span>+</span>
					<br> <span class="pricing-date">${{ $order->subscription->plan->quantity_price }} / Keg per {{ $order->subscription->plan->interval }}</span>
					</p>
				</div>
				<p class="lead text-center">
					{!! $order->subscription->plan->short_description !!}
				</p>
				{!! $order->subscription->plan->description !!}

				<div class="panel-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p class="lead" style="color:#333;">The above plan will be charged to the card provided starting in  {{ $order->subscription->plan->trial_days }} days and then continuing every {{ $order->subscription->plan->interval }}. If you need to change your plan, this can been done from the account tab in the Keg Dashboard.</p>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>

<article class="text-center">
<p>If you have any questions or concerns regarding this order, please contact <a href="mailto:{{ Config::get('constants.SUPPORT_EMAIL')  }} ">{{ Config::get('constants.COMPANY_NAME')  }} Support</a>.</p>

</article>
@stop
