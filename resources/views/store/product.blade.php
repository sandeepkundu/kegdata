@section('title')
Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
@include('components.guestBlock')
@stop

@section('content')
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $cat)
				<li{{ $cat->slug == $category? ' class=active' : '' }}><a href="/store/stripe/{{ $cat->slug }}">{{ $cat->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif
				<!-- <li><a href="/store/stripe">All Products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>
<article class="store">

	<h2>{{ $current_cat->name }}</h2>

	@if(Session::has('msg'))
	<div class="alert alert-success">

		<p>{{Session::get('msg')}}</p>

	</div>
	@endif
	
	<!-- 	include('flash::message') -->
	@if (count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> There were some problems with your input.<br><br>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<?php
	$count = 0;

	$p_count = count($current_cat->products);
	$p_css = "";
	if($p_count == 1 ){
		$p_css = "product col-xs-4 col-xs-offset-4";
	}
	?>

	@foreach($current_cat->products as $product)
	@if($count == 0)


	<?php
	if($current_cat->name == "Systems")  {
		if(isset($current_plan ) && $current_plan != "" && $current_plan != "No_plan"  ){
		echo "<div class='alert alert-success'>
		<p>You already have a $current_plan. Do you want to buy another system?  </p>
		</div>";	
	}if($current_plan == "No_plan"  ){
		echo "";
	}

	}

	?>

	<div class="row ">
		@endif
		@if(!empty($product->stripe_id))
		<div class="col-md-4 product {!! $p_css  !!}  ">
			<div class="col-md-10 col-md-offset-1 clearfix">
				<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
				@if(count($product->photos))
				<div class=" clearfix">
					<div class="">
						<img style="max-width:270px !important; width:270px !important ; height: 270px !important; "  class="img-responsive" src="{{ $product->photos->first()->path }}">
					</div>
				</div>
				@endif
				<h4 class="text-center text-danger">${{ $product->price }}</h4>
				{!! $product->description !!}

				@if($sys_count == 0)
				<form name="add-product-{{ $product->id }}" action="/store/cart/add/product" method="post">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{{ $product->id }}" />
					<div class="form-group">
						<label for="quantity{{ $product->id }}">Quantity</label>
						<div class="input-group">
							<input type="number" min="1" {!! $max !!} value="{!! $subscription_qty !!}" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
							<div class="input-group-addon has-button">
								<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>

				</form>
				
				@endif

			</div>
		</div>

		@if($current_cat->name == "Systems")  
			@if($sys_count > 0) 
				<div class='alert alert-success col-md-12'>
					<p>Add to cart not possible for system! Your account already has 1 system assigned.  To purchase additional systems please log out of your current account and register a new account!</p>
				</div>;
			@endif
		@endif


		<?php
		$count++;
		?>
		@endif
		@if($count == 3)
	</div>
	<?php $count = 0; ?>
	@endif
	@endforeach


</article>

@if( $current_cat->name == "Systems")
	@if($product_system >0)
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/plans"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right" style="float:rigit">
		<a class="btn btn-primary" href="/store/stripe/couplers"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/plans"><i class="fa fa-arrow-circle-left"> Previous </i></a>
	</p>
	<p class="text-right" style="float:rigit">
		<a class="btn btn-primary " href="/store/stripe/couplers">Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif
	@endif
	@if( $current_cat->name == "Couplers")
	@if($product_system >0)
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/systems"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary" href="/store/stripe/*"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/systems"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary disabled" href="#">Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif
	@endif
	@if( $current_cat->name == "Power Distribution")
	@if($product_system >0)
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/couplers"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary" href="/store/stripe/w"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/couplers"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary disabled" href="#">Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif
	@endif

	@if( $current_cat->name == "Wires")
	@if($product_system >0)
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/*"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary" href="/store/cart"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else
	<p class="text-left" style="float:left">
		<a class="btn btn-primary" href="/store/stripe/*"> <i class="fa fa-arrow-circle-left"></i> Previous </a>
	</p>
	<p class="text-right">
		<a class="btn btn-primary disabled" href="#">Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif
	@endif






@if($current_cat->name=="Electrical & Power")
@if($product_system >0)
	<p class="text-right">
		<a class="btn btn-primary" href="/store/cart"> Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@else
	<p class="text-right">
		<a class="btn btn-primary disabled" href="#">Next <i class="fa fa-arrow-circle-right"></i></a>
	</p>
	@endif

@endif
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $cat)
				<li{{ $cat->slug == $category ? ' class=active' : '' }}><a href="/store/{{ $cat->slug }}">{{ $cat->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif
				<!-- <li><a href="/store">All Products<span class="sr-only">(current)</span></a></li> -->
				<li><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>

@stop
