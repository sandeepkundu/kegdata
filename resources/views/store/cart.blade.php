@section('title')
	Store for Ikeg, LLC and {{ Config::get('constants.COMPANY_NAME') }}
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')
@if($cartCount > 2)
<!-- <div style="display:none" class="alert alert-danger margin-top newMsg">
				<p class="lead">
					You have no permission.
				</p>
			</div> -->

@endif
<nav class="navbar navbar-default">
  	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
  		 <div class="navbar-header">
    			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
     			 </button>
    			<a class="navbar-brand" href="/store">{{ Config::get('constants.COMPANY_NAME') }} Store</a>
    		</div>

    		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/store/stripe">Start Here<span class="sr-only">Start Here</span></a></li>
				<li><a href="/store/plans">Plans<span class="sr-only">Plans</span></a></li>
				<!-- <li><a href="/store/stripe">All Products<span class="sr-only">(current)</span></a></li> -->
				@if(!is_null($categories) && count($categories) > 0)
				@foreach($categories as $category)
					<li><a href="/store/stripe/{{ $category->slug }}">{{ $category->name }}<span class="sr-only">(current)</span></a></li>
				@endforeach
				@endif
				
				<li class="active"><a href="/store/cart">Check Out <i class="fa fa-chevron-circle-right"></i></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>

<?php
		
		if(isset($current_plan ) && $current_plan != "" && $current_plan != "No_plan"  ){

			if($cartCount > 0){
		// echo "<div class='alert alert-success'>
		// <p>You already have a $current_plan. Do you want to buy another system?  </p>
		// </div>";
		}	
	}else if($current_plan == null  )  {
		if( $subscription_count< 1){
		echo "<div class='panel pull-right'>
				<a href='/store/plans' class='btn btn-primary'>Click Here To See Available Plan</a>
			</div></br></br>";
		}
	}if($current_plan == "No_plan"  ){

		if( $subscription_count < 1){
		echo "<div class='panel pull-right'>
				<a href='/store/plans' class='btn btn-primary'>Click Here To See Available Plan</a>
			</div></br></br>";
		}

		echo " ";
	}
		

	?>

	<div style="display:none" class="alert alert-danger margin-top newMsg">
		<p class="lead">
			Please select New Plan to match your coupler amount.
		</p>
	</div>
<div class="panel panel-primary">
	<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Shopping Cart <span class="pull-right">{{ $cartCount }} items</span></h3></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>
	</div>

	@if($cartCount)
	{!!  Form::open(['url'=>'/store/cart/update','id' => 'updateCart']) !!}
	<table class="table">
		<thead>
			<tr>
				<th>Subscription</th>
				<th class="text-right">Rate</th>
				<th>Term</th>
				<!--th class="text-center">Delete</th-->
			</tr>
		</thead>
		<tbody>
			<!-- Start new row -->
			<?php $new_price_old = 0 ;?>
			@if(Auth::check() && !empty($user_subscribe_plan) )
			<?php $new_price_old = $user_subscribe_plan['price'];  ?>
				<tr>
				<td>
				
					<p><input type="radio" name="Cplan" value="old"> <strong>Active Plan - {{
					$user_subscribe_plan['name']}}</strong>
					<br>
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount of Couplers Supported: {{ $userOrder_coupler_total}} </strong>
					</p>
				</td>
				<td class="text-right"> <!-- ${{ number_format($user_subscribe_plan['price'], 2) }} -->
					@if($user_subscribe_plan['is_quantity'])
					<!-- 	+ ${{ number_format($kegcount * $user_subscribe_plan['quantity_price'], 2) }} <small>For {{ $kegcount }} Kegs</small>
						= -->
						${{ number_format(($user_subscribe_plan['price'] + ($userOrder_coupler_total*2) ) , 2) }}
					@endif
				
				</td>
				<td>per {{ $user_subscribe_plan['interval'] }} </td>
				<td class="text-center"><!-- <a href="/store/cart/remove/subscription/" class="text-danger"><i class="fa fa-times"></i><span class="sr-only">Remove  </span></a> --></td>
			</tr>
			
			@endif
			<!-- End New row -->
			<?php $new_price=0; ?>
			@foreach($subscription as $row)
			<?php 
			$new_price= $row->price + $kegcount * $row->options->quantity_price;
			?>
			<tr>
				<td style="border-top: 0px solid #ddd;">
					<p><input type="radio" name="Cplan"  value="new" checked="checked"   /><strong>&nbsp;New Plan - {{ $row->name }}</strong>
					<br>
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;New Amount of Couplers Supported:&nbsp;{{$kegcount}}</strong>
					</p>
				</td>
				
				<td class="text-right"> <!-- ${{ number_format($row->price, 2) }} -->
					@if($row->options->is_quantity)
					<!-- 	 + ${{ number_format($kegcount * $row->options->quantity_price, 2) }} <small>For {{ $kegcount }} Kegs</small> 
						= -->
						${{ number_format($row->price + ($kegcount * $row->options->quantity_price), 2) }}
					@endif
				</td>
				<td>per {{ $row->options->term }}</td>
				<!--td class="text-center"><a href="/store/cart/remove/subscription/{{ $row->rowid }}" class="text-danger"><i class="fa fa-times"></i><span class="sr-only">Remove  {{ $row->name }}</span></a>
					<input type="hidden" name="plan_id" id="plan_id" value="{{ $row->rowid }}">
				</td-->
				<div style="display:none"><p id="plan_id">{{ $row->rowid }}</p></div>

				<!-- <input type="hidden" name="plan_id" id="plan_id" value="{{ $row->rowid }}"> -->

			</tr>
		
			@endforeach
		
			<tr><td><p><strong></strong></p></td><td></td><td></td><td></td></tr>		
				
		</tbody>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>Product</th>
				<th class="text-right">Item Price</th>
				<th>Qty</th>
				<th class="text-right">Subtotal</th>
				<th class="text-center">Delete</th>
			</tr>
		</thead>
				<?php 
				$tt = 0;

			?>
		@foreach($cartItems as $row)
		@if($row->id ==5 || $row->id ==6 )
			<?php 
				$tt = $row->qty ;

			?>

		@endif

		@endforeach
		<?php // dd($tt );?>
		<tbody>

			@foreach($cartItems as $row)
			
			<tr>
				<td>
					<p><strong>{{ $row->name }}</strong></p>
				</td>
				<td class="text-right">${{ number_format($row->price, 2) }}</td>
				<td><input type="text" name="shopping_{{ $row->rowid }}" value="{{ $row->qty }}"></td>
				<td class="text-right">${{ number_format($row->subtotal, 2) }}</td>
				<td class="text-center"><a href="/store/cart/remove/shopping/{{ $row->rowid }}" class="text-danger"><i class="fa fa-times"></i><span class="sr-only">Remove  {{ $row->name }}</span></a></td>
			</tr>

			@endforeach

		</tbody>
		<tfoot class="bg-success">
			<th>Product Subtotal</th>
			<td colspan="3" class="text-right"><strong>${{ number_format($cartTotal,2) }}</strong></td>
			<td></td>
		</tfoot>
	</table>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<a href="/store/stripe" class="btn btn-primary">Continue Shopping</a>
			</div>
			<div class="col-md-2 col-md-offset-8 text-right">
				<button type="submit" class="btn btn-default "><i class="fa fa-refresh"></i> Update Cart</button>
			</div>
		</div>
		@if($relatedProducts)
			<div class="alert alert-danger margin-top">
				<p class="lead">
					The items in your cart indicate that you may require extra parts.
					Take a look below and see if you need any of the items.
				</p>
			</div>
			@if(!empty($splitterProducts) and $splitterProducts->products->count())
				<h3>Power Splitters</h3>
				<p class="lead">
					We show a total of {{ $kegcount }} kegs for your account including
					the ones in the cart. Do you have a big enough splitter for
					that many couplers?
				</p>
				<?php $count = 0; ?>
				@foreach($splitterProducts->products as $product)
					@if($count == 0)
						<div class="row">
					@endif
					@if(!is_null($product->stripe_id))
					<div class="col-md-4 product" id="product{{ $product->id }}" >
						<div class="col-md-10 col-md-offset-1 clearfix">
							<div class="hidden description">
								{!! $product->description !!}
							</div>
							<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
							@if(count($product->photos))
							<div class="bg-primary clearfix">
								<div class="col-md-8 col-md-offset-2">
									<img class="img-responsive" src="{{ $product->photos->first()->path }}">
								</div>
							</div>
							@endif
							<h4 class="text-center text-danger">${{ $product->price }}</h4>
							<form name="add-product-{{ $product->id }}" action="/store/cart/add/product/" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{ $product->id }}" />
								<div class="form-group">
									<label for="quantity{{ $product->id }}">Quantity</label>
									<div class="input-group">
										<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
										<div class="input-group-addon has-button">
											<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

					<?php $count++; ?>
					@endif
					@if($count == 3)
						<?php $count = 0; ?>
						</div>
					@endif
				@endforeach
				@if($count != 3 && $count != 0)
					</div>
				@endif
			@endif

			@if(!empty($mpwProducts) and $mpwProducts->products->count())
				<h3>Main Power Wires</h3>
				<p class="lead">
					Each splitter a main power wire. We show enough couplers to need
					another splitter. Do you need a main power wire?
				</p>
				<?php $count = 0; ?>
				@foreach($mpwProducts->products as $product)
					@if($count == 0)
						<div class="row">
					@endif
					@if(!is_null($product->stripe_id))
					<div class="col-md-4 product" id="product{{ $product->id }}" >
						<div class="col-md-10 col-md-offset-1 clearfix">
							<div class="hidden description">
								{!! $product->description !!}
							</div>
							<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
							@if(count($product->photos))
							<div class="bg-primary clearfix">
								<div class="col-md-8 col-md-offset-2">
									<img class="img-responsive" src="{{ $product->photos->first()->path }}">
								</div>
							</div>
							@endif
							<h4 class="text-center text-danger">${{ $product->price }}</h4>
							<form name="add-product-{{ $product->id }}" action="/store/cart/add/product/" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{ $product->id }}" />
								<div class="form-group">
									<label for="quantity{{ $product->id }}">Quantity</label>
									<div class="input-group">
										<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
										<div class="input-group-addon has-button">
											<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

					<?php $count++; ?>
					@endif
					@if($count == 3)
						<?php $count = 0; ?>
						</div>
					@endif
				@endforeach
			@endif
			@if($count != 3 && $count != 0)
				</div>
			@endif
			@if(!empty($cpwProducts) and $cpwProducts->products->count())
				<h3>Coupler Power Wires</h3>
				<p class="lead">
					Each coupler requires its own power wire to connect to
					the splitter. We show {{ $couplerCount }} couplers in
					cart. Do you need more coupler power wires?
				</p>
				<?php $count = 0; ?>
				@foreach($cpwProducts->products as $product)
					@if($count == 0)
						<div class="row">
					@endif
					@if(!is_null($product->stripe_id))
					<div class="col-md-4 product" id="product{{ $product->id }}" >
						<div class="col-md-10 col-md-offset-1 clearfix">
							<div class="hidden description">
								{!! $product->description !!}
							</div>
							<h3 class="clearfix">{{ $product->name }}<small class="pull-right">{{ $product->part_id }}</small></h3>
							@if(count($product->photos))
							<div class="bg-primary clearfix">
								<div class="col-md-8 col-md-offset-2">
									<img class="img-responsive" src="{{ $product->photos->first()->path }}">
								</div>
							</div>
							@endif
							<h4 class="text-center text-danger">${{ $product->price }}</h4>
							<form name="add-product-{{ $product->id }}" action="/store/cart/add/product/" method="post">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{ $product->id }}" />
								<div class="form-group">
									<label for="quantity{{ $product->id }}">Quantity</label>
									<div class="input-group">
										<input type="number" value="1" name="quantity" id="quantity{{ $product->id }}" class="form-control"/>
										<div class="input-group-addon has-button">
											<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

					<?php $count++; ?>
					@endif
					@if($count == 3)
						<?php $count = 0; ?>
						</div>
					@endif
				@endforeach
			@endif
			@if($count != 3 && $count != 0)
				</div>
			@endif
		@endif

	</div>

	{!! Form::close()!!}
	@else
		<div class="panel-body">
			<p class="lead">You have no items in the cart! Return to the <a href="/store/plans">Store <span class="fa fa-chevron-right"></span></a></p>
		</div>
	@endif

</div>
@if($cartCount)
<div class="row">
	<div class="col-md-4 col-md-offset-2">
		{{--
		<div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title">Apply Discount</h3></div>
			<div class="panel-body">
				{!! Form::open(['url' => '/store/cart/promo', 'id' => 'promoForm']) !!}
					<div class="form-group{{ !$errors->first('slug') ? '' : ' has-error has-feedback' }}">
						<label for="slug">Promotion Code</label>
						<input type="text" class="form-control" id="slug" name="slug" placeholder="Promo Code">
						@if ($errors->first('slug'))
							<span class="help-block">{{ $errors->first('slug') }}</span>
						@endif
					</div>
					<div class="form-row">
						<button type="submit" class="btn btn-default">Apply Promotion</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>--}}
	</div>
	<div class="col-md-8 col-md-offset-4">
		<div class="panel panel-primary">
			<div class="panel-heading"><h3 class="panel-title">Order Total</h3></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<strong>Subtotal</strong>
					</div>
					<div class="col-md-6 text-right">
						${{ number_format($cartTotal + $new_price, 2) }}
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-md-6">
						<p class="lead">Grand Total</p>
					</div>
					<div class="col-md-6">
						<p class="lead text-danger text-right">${{ number_format($cartTotal + $new_price,2) }}</p>
					</div>
				</div>
				<hr />
				<!-- <div class="row">
					<div class="col-md-6">
						<p class="lead">Grand Total</p>
					</div>
					<div class="col-md-6">
						<p class="lead text-danger text-right">${{ number_format($newTotalCount,2) }}</p>
					</div>
				</div>
					<hr /> -->
				<div  style=" overflow-y: scroll;height: 300px;width: 100%;border: 1px solid #DDD;padding: 10px;"  	class="col-md-12">
					<p><stong><b>TERMS OF USE</b></stong><br>
         		Ikeg, LLC, hereinafter called "Ikeg", provides equipment and sends data, via the internet, that is delivered to a web page for access by customers, clients or end users. Although Ikeg will use its best efforts to assure accurate data, Ikeg shall not be liable or responsible for the accuracy or loss of any data, or for any losses or damages based upon the  use of any of said data by the end user. . The end user also acknowledges and accepts that Ikeg may provide some or all of the data to third parties including but not limited to breweries, enterprise clients and distributors. The end user also acknowledges the fact that electronic data transmitted over the internet is subject to being intercepted or "hacked" and used for unlawful purposes. Ikeg will take reasonable steps to prevent this from happening, but the end user agrees that Ikeg shall have no liability  in the event that  the database is hacked and customer information is stolen or misused
				<br><br><stong><b>LIMITED WARRANTY</b></stong> <br>

				Ikeg warranties our products to be free of manufacturing defects for a period of one (1)  year from the date of original purchase. This warranty does not cover wear and tear, physical damages or failure from misuse. THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. This limited warranty is issued to the original purchaser only. If there is a defect in workmanship or materials during the warranty period, we will repair or replace the product or refund the full purchase price, at our option. This warranty is voided if the product is used in any manner or for any purpose other than that for which it was designed , if modified in any way, or if overloaded, leased or rented, unreasonably used, improperly assembled, or damaged by accident, negligence, or misuse. THIS LIMITED WARRANTY EXCLUDES shipping costs, compensation for inconvenience or loss of use/data, consumables or any gas propellant, and any and all consequential or incidental damages. RETURNS must be authorized by Ikeg must be returned within the warranty period and must be returned to our facilities for repair, replacement or refund.
				<br><br><stong><b>DISCLAIMER AND INDEMNITY</b></stong> <br>

				Purchaser/user/assigns shall indemnify, protect, defend and hold harmless Ikeg its partners, officers, subsidiaries, employees and agents, from and against any and all claims, costs, liabilities, losses, damages, injuries, judgments and expenses asserted by any third parties (collectively, the "Claims") including,  but not limited to, attorney’s fees, court costs, including those incurred at the trial and appellate levels and in any bankruptcy, reorganization, insolvency or other similar proceedings, and other legal expenses, arising out of or resulting from: (i) accidents, injuries or death from the use of Ikeg products: (ii) any misrepresentation, breach of warranty or negligence of Ikeg,  its directors, officers, managers, employees or agents, and (iii) incidental or consequential damages or losses due to directly or indirectly from the use of their products. By purchasing products from Ikeg, buyer agrees that jurisdiction and venue for any and all legal claims against company shall be in Harris County, Texas.
				<br><br><stong><b>RETURNS</b></stong> <br>

				A purchaser may return any product within 60 days from the date purchase for a full refund, including any monthly fees, for any reason whatsoever without question. The returned product must be in new and undamaged condition. Purchaser is responsible for returned freight costs to Ikeg, LLC

				</p></div>
				

				<!-- check to see if there is subscription count -->

				@if($subscription_count >0)

					<!-- check if system have system already -->
					@if( $couplerCount && $sys_count > 0 ) 
						<div class='alert alert-success col-md-12'>
							<p>Checkout not possible! Your account already has 1 system assigned.  To purchase additional systems please log out of your current account and register a new account!</p>
							</div>
						@elseif($couplerCount==0 && $sys_count == 0) 
							<div class='alert alert-success col-md-12'>
								<p>Checkout not possible! You must have atleast 1 system in your cart!</p>
							</div>
						@else
						<label><input class="field" name="terms" id="term" type="checkbox" value="" required> <a style="color:black; text-decoration: none; " href="#">Accept terms of services</label>
						<p id="msg1" style="color:red"></p>
						<p class="text-right">
							<a id="proceed" href="/store/checkout" class="btn btn-primary">Proceed to Checkout <i class="fa fa-arrow-circle-right"></i></a>
						</p>
					@endif

					@else
					<label><input class="field" name="terms" id="term" type="checkbox" value="" required> <a style="color:black; text-decoration: none; " href="#">Accept terms of services</label>
						<p id="msg1" style="color:red"></p>
						<p class="text-right">
							<a id="proceed" href="/store/checkout" class="btn btn-primary">Proceed to Checkout <i class="fa fa-arrow-circle-right"></i></a>
						</p>

				@endif

				
				

			</div>
		</div>
	</div>
</div>

@endif

<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script>
jQuery( document ).ready(function() {
	$('#newAddCup').text($('input[name="shopping_468399581342505c47f4615b81bedaa9"]').val())
    $('input[name="Cplan"]:last').attr("checked", "checked");	
	var atLeastOneIsChecked = $('input[name="terms"]:checked').length > 0;
	if(!atLeastOneIsChecked){
		
		$('#proceed').addClass('disabled');
	}else{
		$('#proceed').removeClass('disabled');


	}
	 $('#term').change(function() {
	 if ($(this).prop('checked')) {
            $('#proceed').removeClass('disabled');
        }
        else {
           $('#proceed').addClass('disabled');
        }
    });

	 $('#proceed').click(function() {


	 	var red_value = $('input[name="Cplan"]:checked').val();
	 	var new_plan_id = $('#plan_id').text();
	 	//alert(new_plan_id);
	 
	 	if(new_plan_id && red_value== 'old'){
	 		
	 		window.scrollTo(0,0);
	 		$('.newMsg').css("display", "block");	
	 		setTimeout(function(){
	 			document.location.href = "#"; 
	 		}, 1000);
			return false;
	 		if(new_plan_id){
	 			var url = "/store/cart/remove/subscription/"+new_plan_id;
	 			$.ajax({
	 				type: "GET",
	 				url: url ,
	 				data: {id : new_plan_id},
	 				async:false,
	 				success: function(result){
	 					if(result){
	 						return true;
	 					}else{
	 						return false;
	 					}


	 				}

	 			});

	 						
	 		}else{
	 			return true;
	 		}

	 	

	 	}


	 	if(!$('#term').prop('checked')){

	 		$('#msg1').text('Please Accept terms of services.');
	 		return false;
	 	}

	 });
	
});

</script>
@stop
