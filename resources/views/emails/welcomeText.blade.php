Hi {{ $firstName }} {{ $lastName }},
Thank you for registering with KegData.
Login: {{ $email }}

Please confirm your account by clicking on
<a href="{{ $link }}">{{ $link }}</a>