@section('title')
	The love of beer and never running out
@stop

@section('login-block')
	@include('components.guestBlock')
@stop

@section('content')<article class="col-md-10">
  <div class="row">
		<div class="col-md-12 text-center" role="banner">
			<!--<p class="lead">Do you know how much beer is in your keg?</p>-->
			<div class="banner"><img src="img/homepage-banner02.jpg" /></div>
		</div>
	</div>

	<div class="row">
		<section class="col-md-12">
		  <h1 align="left" class="title">Get ready, the store is about to open!  Register now!</h1>
		  <div align="center"><img src="mainart/boxes1.jpg" width="300" height="208" alt="Sensor Board"/>

	      </div>
		  <h1 class="title">Why do you need keg monitoring? </h1>
               <p>{{ Config::get('constants.COMPANY_NAME')}}is all about saving you money as a bar owner or operator. It finds out if your employees are pouring too much, or pouring beer that is not billed &quot;shrinkage&quot;. Our system also stops foaming in the lines when you change kegs. Of course our system does so much more, like auto reordering, low level alerts and temperature alerts. What does it save a bar or restaurant owner?</p>
		  <p>Lets look at shrinkage. If a bartender pours just a couple of free beers per day and does not ring them up, we will say $6 each or $12/day. Now over a month that is $360/month, $4,320/yr in losses, and that is with just 2 beers a day! A more realistic number may be 10 or more beers a day, that is $21,600/yr!</p>
			 <p>Now lets look at foaming. If you lose a pitcher of foam, about 64oz on every keg change, at $150/keg that is about $5/keg. Multiply this times how many kegs you sell a year. If you go through 500 kegs a year that is $2,500/yr. These two losses do not include the employee time to pour foam and the loss of C02 for foaming and pulling the tap on an empty keg.</p>
<p>How much money do you lose when you run out of beer? This is difficult to determine but there is really no reason why you should run out of one of your draft beers. You lose money, the distributor loses money and of course the brewery loses money. {{ Config::get('constants.COMPANY_NAME')}} has an auto reorder system built in. Just set the level you want to reorder and the system will notify you, your supplier or both when your keg hits its preset level for replacement. This auto fulfillment system is unique to KegData and helps keep your customers happy and your cash register ringing!		</p>
		  <h1 class="title">Why {{ Config::get('constants.COMPANY_NAME') }} ?</h1>
		{{ Config::get('constants.COMPANY_NAME') }} moves to the next level of technology in keg monitoring. Five things that separate us from previous technology<a href="http://www.kegdata.com/store"></a>:</p>
		<ul>
		  <li>Monitoring is only $9.95/mo for residential and $24.95/mo for commercial accounts, plus $2.00/keg per month.</li>
		  <li>The sensor has no moving parts, which means long term reliable service.</li>
		  <li>No user reset buttons to push, it knows what is in your keg as soon as you hook it up and pour.</li>
		  <li>Cost effective! A single keg system starts at only $199!</li>
		  <li>Built in FOB! Yes, we have a built in auto uncoupler to stop foam in your lines when your keg is empty.</li>
		  </ul>
		<p>Why pay for a Beer FOB and the installation when it is built into the KegData coupler free? <img src="/mainart/fob1.jpg" alt="Beer FOB" width="100" height="100" lass="pull-right" /></p>

	  </section>
		<section class="col-md-12 clearfix">
<h1 class="title">What is {{ Config::get('constants.COMPANY_NAME') }} </h1>

			<p>{{ Config::get('constants.COMPANY_NAME') }}  is the most advanced new keg monitoring system available today, which allows users to know how much beer is in their kegs and how much was sold/used. Our system is designed for restaurants but also works for home users with a single keg. Complete systems start at only $199.00 with
			web access from any computer or smart phone with internet access!
			See your keg status from your phone or your computer instantly.</p><p>Our new {{ Config::get('constants.COMPANY_NAME') }} couplers are extraordinary and so simple to install
		that almost anybody can install it in a few minutes. What does that mean
		for bar owners? Savings! To install you simply change out your current
		coupler with the new {{ Config::get('constants.COMPANY_NAME') }} coupler, provide power and log into your
		network, done! No more cumbersome scales or costly flow meters. We also currently offer the US &quot;D&quot; style Sankey and the Euro type &quot;S&quot; Sankey.</p><img border="0" src="mainart/keglevel1.jpg" width="432" height="612" class="pull-center">
		</section>
		<section class="col-md-12">
		<h1 class="title">Key Features</h1>
		<ul>
			<li>Knows how much beer in in your keg as soon as you hook it up and
			pour a beer</li>
			<li>Sends you temperature alerts if your refrigerator is warming up</li>
			<li>Has an auto order feature that will notify your supplier you
			need a new keg</li>
			<li>The coupler auto releases when your keg is empty so no foam in
			the lines (commercial units only)</li>
			<li>Graphs your pours and temperature</li>
			<li>Works autonomously so you do not have to do anything</li>
			<li>Has wireless communications between the kegs and our web</li>
			<li>Can monitor up to 24 kegs at once</li>
			<li>Helps prevent theft and pours that are not billed</li>
			<li>How easy is it to install? Replace your existing couplers.</li>
		</ul>

          	<p>There are many special features such as auto reordering, auto untapping when a keg is empty and high temperature alerts for your cooler or walk-in. We know there are several old keg monitoring systems out there that use scales and flow meters. These older systems are problematic, costly and require resetting and user intervention. Nothing compares to our new advanced autonomous technology, its so advanced its simple. Below is a
			photo of our new coupler.</p>
		</section>
		<section class="col-md-12">
          	<h1 class="title">The New Patented Coupler</h1>
          	<img src="mainart/couplers/KD1000-D-2tn.jpg" width="600px" class="pull-center"/>
            <h1 class="title">Its all about the coupler</h1>
          	<p>{{ Config::get('constants.COMPANY_NAME') }}  has several patents issued and more pending on this new exciting coupler that knows exactly how much beer is in your keg the second you hook it up
			and draw a beer. This coupler took over three years to develop and is the most advanced system in the world in beer monitoring. There are several microprocessors, including advanced wireless connectivity.
			Don't fall for people trying to sell you the old bathroom scale to
			weigh your kegs or flow meters, it is outdated technology. </p>

			<p>The {{ Config::get('constants.COMPANY_NAME') }}  store is open and other functionality will be open soon. Please register so we can update you on news and more advances in our technology. For other questions please contact sales@kegdata.com . Finally, an   inexpensive way to monitor your keg beer usage inexpensively.</p>
			<p>{{ Config::get('constants.COMPANY_NAME') }} works worldwide with our new type "S" Euro couplers with metric settings, including Celsius temperature and measurement by the liter. Want to be a global distributor? Email sales@kegdata.com.</p>
		  <p>&nbsp;</p>
		  <p align="center"><strong>REGISTRATION IS OPEN AND ITS FREE, NO CREDIT CARD REQUIRED, NO SPAM.</strong></p>
		  <p align="center">&nbsp;</p>
		</section>

	</div>
</article>
@stop

@section('sidebar')
{{-- Removed 2 lines of new development code (July 24) 'here', and reverted back to previous code (July 15) due to persistent Laravel 'RuntimeException' (FatalErrorException Class 'Form' not found) failure and application interruption.	--}}
	@include('components.sideAd')
@stop
