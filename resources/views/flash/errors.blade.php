<div class="row">
<div class="col-md-7 col-md-push-3" style="padding: 5px 24px!important;">
    @if($errors->any())
        <ul class="alert alert-danger text-center rtl ur fsize26" style="list-style: none;">
            @foreach($errors->all() as $error)
                <li style="color: #000 !important;"> {{ $error }} </li>
            @endforeach
        </ul>
    @endif
</div>
