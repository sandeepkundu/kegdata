module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "public/css/custom-bootstrap.css": "resources/assets/less/bootstrap/custom-bootstrap.less" // destination file and source file
        }
      }
    },
    watch: {
      styles: {
        files: ['resources/assets/less/bootstrap/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};