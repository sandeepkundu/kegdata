<?php namespace Orchestra\Support\Providers\Traits;

/**
 * @deprecated v3.2.x
 */
trait EventProviderTrait
{
    use EventProvider;
}
