<?php namespace Orchestra\Support\Traits;

/**
 * @deprecated v3.2.x
 */
trait MacroableTrait
{
    use Macroable;
}
