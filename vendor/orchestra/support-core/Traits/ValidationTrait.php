<?php namespace Orchestra\Support\Traits;

/**
 * @deprecated v3.2.x
 */
trait ValidationTrait
{
    use Validation;
}
