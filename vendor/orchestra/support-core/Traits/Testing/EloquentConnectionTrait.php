<?php namespace Orchestra\Support\Traits\Testing;

/**
 * @deprecated v3.2.x
 */
trait EloquentConnectionTrait
{
    use MockEloquentConnection;
}
