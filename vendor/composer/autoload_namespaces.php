<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Way\\Generators' => array($vendorDir . '/way/generators/src'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Security\\Core\\' => array($vendorDir . '/symfony/security-core'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Psy\\' => array($vendorDir . '/psy/psysh/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PhpSpec' => array($vendorDir . '/phpspec/phpspec/src'),
    'Payum\\Paypal\\Rest' => array($vendorDir . '/payum/paypal-rest'),
    'Payum\\Paypal\\ProCheckout\\Nvp' => array($vendorDir . '/payum/paypal-pro-checkout-nvp'),
    'Payum\\Paypal\\ExpressCheckout\\Nvp' => array($vendorDir . '/payum/paypal-express-checkout-nvp'),
    'Payum\\LaravelPackage' => array($vendorDir . '/payum/payum-laravel-package/src'),
    'Payum\\Core\\' => array($vendorDir . '/payum/core'),
    'PayPal' => array($vendorDir . '/paypal/rest-api-sdk-php/lib'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'Laravel\\Cashier\\' => array($vendorDir . '/laravel/cashier/src'),
    'Laracasts\\Flash' => array($vendorDir . '/laracasts/flash/src'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Imagine' => array($vendorDir . '/imagine/imagine/lib'),
    'Gloudemans\\Shoppingcart' => array($vendorDir . '/gloudemans/shoppingcart/src'),
    'Dotenv' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Cron' => array($vendorDir . '/mtdowling/cron-expression/src'),
    'Buzz' => array($vendorDir . '/kriswallsmith/buzz/lib'),
);
