<?

  include("./include/dbconnect.php");

  $errmsg = "OK";

  if (! isset($_GET['recNo']))
  {
    echo "no data";
    exit;
  }
  $recNo       = $_GET['recNo'];
  $retstr      = "";
  $kegMonID    = "";
  $sentDtTm    = "";
  $keg1Level   = "";
  $keg2Level   = "";
  $keg3Level   = "";
  $keg4Level   = "";
  $keg5Level   = "";
  $keg6Level   = "";
  $keg7Level   = "";
  $keg8Level   = "";
  $tempCelcius = "";

  $connect = ConnectDB();  // returns connection or "ERR|Short Msg|Errno|DB Error Description"
  if (substr($connect,0,3) == "ERR")
  {
    $errmsg = $connect;
  }

  if ($errmsg == "OK")
  {
    $sql = "SELECT "
        ."kegMonID, sentDtTm, "
        ."keg1Level, keg2Level, keg3Level, keg4Level, "
        ."keg5Level, keg6Level, keg7Level, keg8Level, "
        ."tempCelcius "
        ." FROM KegDataRcvd "
        ." ORDER BY kegMonID, sentDtTm "
        ." LIMIT ". $recNo .",1 ";
    $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"readKegDataRec","SELDATA");
    if ($errmsg == "OK")
    {
      $lcol = @mysql_fetch_assoc($result);

      $kegMonID    = $lcol['kegMonID'];
      $sentDtTm    = $lcol['sentDtTm'];
      $keg1Level   = $lcol['keg1Level'];
      $keg2Level   = $lcol['keg2Level'];
      $keg3Level   = $lcol['keg3Level'];
      $keg4Level   = $lcol['keg4Level'];
      $keg5Level   = $lcol['keg5Level'];
      $keg6Level   = $lcol['keg6Level'];
      $keg7Level   = $lcol['keg7Level'];
      $keg8Level   = $lcol['keg8Level'];
      $tempCelcius = $lcol['tempCelcius'];
      $sentDtTm    = str_replace("-","",$sentDtTm);
      $sentDtTm    = str_replace("/","",$sentDtTm);
      $sentDtTm    = str_replace(":","",$sentDtTm);
      $sentDtTm    = str_replace(" ","",$sentDtTm);
      $retstr      = $kegMonID."|".$sentDtTm."|"
      .$keg1Level."|".$keg2Level."|".$keg3Level."|".$keg4Level."|"
      .$keg5Level."|".$keg6Level."|".$keg7Level."|".$keg8Level."|"
      .$tempCelcius."|K";
    }
  }
  CloseDB($connect);
  if ($errmsg == "OK")
    echo $retstr;
  else
    echo $errmsg;
?>
