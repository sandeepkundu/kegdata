<?


define("ACCTDEV_HUB","H");
define("ACCTDEV_CPL","C");

  $acctDev  = array('isValid'         => false,
                    'acctDevNum'      => RECNUM_NONE,
                    'acctNum'         => RECNUM_NONE,
                    'acctDevType'     => '',
                    'acctDevName'     => '',
                    'acctDevMAC'      => '',
                    'kegTypeNum'      => RECNUM_NONE,
                    'prodNum'         => RECNUM_NONE,
                    'acctDevAlarm'    => '0',
                    'acctDevModified' => '',
                    'acctDevModBy'    => ''
                    );

function initAcctDevice()
{
  global $acctDev;
  foreach($acctDev as $key => $val)
    $acctDev[$key] = "";
  $acctDev['isValid'] = false;
  $acctDev['acctDevNum'] = RECNUM_NONE;
}

function getAcctDeviceSelectSQL()
{
  global $acctDev;
  $sql = "SELECT ";
  foreach($acctDev AS $col => $val)
  {
    if ($sql != "SELECT ")
      $sql .= ", ";

    switch ($col)
    {
      case "isValid" :
        $sql .= "1 AS isValid";
        break;
      case "acctDevModified" :
        $sql .= "DATE_FORMAT(acctDevModified,'%Y/%m/%d %H:%i:%s') AS acctDevModified";
        break;
      default:
        $sql .= $col;
        break;
    }
  }
  $sql .= " FROM AcctDevices ";
  return $sql;
}

function getAcctDeviceInfo($connect, $acctDevNum)
{
  global $acctDev;
  initAcctDevice();
  $errmsg = "OK";
  $sql  = getAcctDeviceSelectSQL()." WHERE acctDevNum = ".$acctDevNum;
  $result = DBQuery($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getAcctDeviceInfo","SELDEV");
  if ($errmsg == "OK")
    if (($lcol = DBFetchAssoc($result)))
      foreach($lcol as $key => $val)
        $acctDev[$key] = $lcol[$key];
  $acctDev['acctDevMAC'] = formatMACfordisplay($acctDev['acctDevMAC']);
  return $errmsg;

}

function getAcctDeviceByMAC($connect, $acctDevMAC,$acctDevType="X")
{
  global $acctDev;
  initAcctDevice();
  $errmsg = "OK";
  $sql  = getAcctDeviceSelectSQL()." WHERE acctDevMAC = '".$acctDevMAC."'";
  if ($acctDevType != "X")
    $sql .= " AND acctDevType = '".$acctDevType."'";
  $result = DBQuery($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getAcctDeviceInfo","SELDEV");
  if ($errmsg == "OK")
    if (($lcol = DBFetchAssoc($result)))
      foreach($lcol as $key => $val)
        $acctDev[$key] = $lcol[$key];
  $acctDev['acctDevMAC'] = formatMACfordisplay($acctDev['acctDevMAC']);
  return $errmsg;

}

function getCurrentKegLevel($connect,$acctDevNum)
{
  $errmsg = "OK";
  $retval = array('success'=>false,'reason'=>'init');
  $sql = "SELECT kegLevelValue, DATE_FORMAT(sentDtTm,'%m/%d/%y %h:%i:%s') AS sentDtTm FROM KegLevels WHERE acctDevNum = ".$acctDevNum." ORDER BY sentDtTm DESC LIMIT 1 ";
  $result = DBQuery($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getCurrentKegLevel","SELLEVEL");
  if ($errmsg != "OK")
  {
    $retval = array('success'=>false,'reason'=>$errmsg);
  }
  else
  {
    if (($lcol = DBFetchAssoc($result)))
      $retval = array('success'=>true,'level'=>$lcol['kegLevelValue'],'dttm'=>$lcol['sentDtTm']);
    else
      $retval = array('success'=>true,'level'=>0,'dttm'=>'(No Date)');
  }
  return $retval;
}


?>
