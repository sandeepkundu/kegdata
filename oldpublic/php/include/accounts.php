<?

define("ACCTTYPE_NONE","X");
define("ACCTTYPE_PRIV","S");
define("ACCTTYPE_REST","R");
define("ACCTTYPE_DIST","D");
define("ACCTTYPE_BREW","B");
define("ACCTTYPE_ENT" ,"E");

   $acctInfo = array('isValid'           => false,
                     'acctNum'           => RECNUM_NONE,
                     'mailListKey'       => '',
                     'enterpriseCode'    => '',
                     'acctType'          => ACCTTYPE_NONE,
                     'acctID'            => '',
                     'acctName'          => '',
                     'acctAddr1'         => '',
                     'acctAddr2'         => '',
                     'acctCity'          => '',
                     'acctCounty'        => '',
                     'acctState'         => '',
                     'acctZip'           => '',
                     'acctCountry'       => '',
                     'acctCurrency'      => '',
                     'acctVolumeType'    => 'OZ',
                     'acctTempType'      => 'F',
                     'acctTimezone'      => '0',
                     'acctDST'           => '0',
                     'acctLat'           => '',
                     'acctLng'           => '',
                     'acctPhone1'        => '',
                     'acctPhone2'        => '',
                     'acctEmail1'        => '',
                     'acctEmail2'        => '',
                     'acctWebsite'       => '',
                     'acctModified'      => '',
                     'acctModBy'         => '',
                    );

/////////////////////////////////////////////////////////
// this function MUST be called inside a transaction!!
// returns array('success'=>{boolean},'val'=>{value});
// if 'success' is true 'val' is a 5-digit number
// if 'success' is false 'val' is a $errmsg
function getNextAcctID($acctType=ACCTTYPE_NONE,$configNum='1')
{
  $retval  = array('success'=>1,'val'=>'00000');
  $errmsg  = "OK";
  $colName = "";

  switch($acctType)
  {
    case ACCTTYPE_PRIV: $colName = "nextPrivID"; break;
    case ACCTTYPE_REST: $colName = "nextCustID"; break;
    case ACCTTYPE_DIST: $colName = "nextDistID"; break;
    case ACCTTYPE_BREW: $colName = "nextBrewID"; break;
    case ACCTTYPE_ENT : $colName = "nextEntID"; break;
  }
  $sql = "UPDATE Config SET ".$colName." = ".$colName." + 1 WHERE configNum = ".$configNum;
  DBQuery($sql, $connect) or $errmsg = "ERR|config|".GetDatabaseError($connect,"getNextAcctID","SELCFG");
  if ($errmsg == "OK")
  {
    $sql = "SELECT ".$colName." FROM Config WHERE configNum = ".$configNum;
    $result = DBQuery($sql, $connect) or $errmsg = "ERR|config|".GetDatabaseError($connect,"getNextAcctID","UPDCFG");
  }
  if ($errmsg != "OK")
    $retval = array('success'=>0,'val'=>$errmsg);
  else
  {
    if (! $lcol = DBFetchAssoc($result))
      $retval = array('success'=>0,'val'=>"ERR|Fetch Error|-1|getNextAcctID");
    else
      $retval = array('success'=>1,'val'=>substr("00000".$lcol[$colName],-5));
  }
  return $retval;
}
/////////////////////////////////////////////////////////

function initAcctInfo()
{
  global $acctInfo;
  foreach($acctInfo as $key => $val)
    $acctInfo[$key] = "";

  $acctInfo['acctNum']           = RECNUM_NONE;
  $acctInfo['isValid']           = false;
  $acctInfo['acctType']          = ACCTTYPE_NONE;
}

function getAcctSelectSQL()
{
  global $acctInfo;
  $sql = array();
  foreach($acctInfo as $col => $val)
  {
    switch ($col)
    {
      case "isValid" :
        //$sql .= "1 AS isValid";
        break;
      default:
        $sql[] = $col;
        break;
    }
  }

  return $sql;
}

function getAcctInfo($acctNum)
{
  global $acctInfo;
  global $connect;

  initAcctInfo();
  $errmsg = "OK";
  $sql = getAcctSelectSQL();
  $where = ["acctNum" => $acctNum];
  $result = $connect->select("accounts",$sql,$where);
  $errmsg = $connect->error();
  if($errmsg[0] == "00000"){
      foreach($result[0] as $key => $val)
        $acctInfo[$key] = $result[0][$key];
      $acctInfo["isValid"] = 1;
      $errmsg["status"] = "OK";
  }else{
    $errmsg["status"] = "ERROR";
    $errmsg["report"] = $errmsg[2];
  }
  return $errmsg;
}

function getAcctByID($connect,$acctID)
{
  global $acctInfo;
  initAcctInfo();
  $errmsg = "OK";
  $sql = getAcctSelectSQL()." WHERE acctID = '".$acctID."'";
  $result = DBQuery($sql, $connect) or $errmsg = "ERR|Select Error.|".$sql."|".GetDatabaseError($connect,"getAcctByID","SELACCTID");
  if ($errmsg == "OK")
    if (($lcol = DBFetchAssoc($result)))
      foreach($lcol as $key => $val)
        $acctInfo[$key] = $lcol[$key];
  return $errmsg;
}

function getAcctByMAC($connect,$MAC)
{
  global $acctInfo;
  initAcctInfo();
  $errmsg = "OK";
  $sql = getAcctSelectSQL()
  ." WHERE acctNum IN (SELECT acctNum FROM AcctDevices WHERE acctDevMAC = '".$MAC."')";
  $result = DBQuery($sql, $connect) or $errmsg = "ERR|Select Error.|".$sql."|".GetDatabaseError($connect,"getAcctByMAC","SELACCTMAC");
  if ($errmsg == "OK")
    if (($lcol = DBFetchAssoc($result)))
      foreach($lcol as $key => $val)
        $acctInfo[$key] = $lcol[$key];
  return $errmsg;
}

function getAcctUserList($acctNum, $showInactive=true)
{
  global $connect;
  $retval = array();
  $errMsg["status"] = "OK";
  $inact = ($showInactive == true ? "" : " AND userIsActive = 1 ");

  $table = "AuthUsers";
  $data = ["usernum", "userID", "userFirstName", "userLastName"];
  $where =["acctNum" => $acctNum.$inact, "ORDER" => ["userLastName", "userFirstName"]];

  $results = $connect->select($table,$data,$where);
  $errmsg = $connect->error();
  if ($errmsg[0] == "00000"){
    foreach($results as $result)
      array_push($retval, $result);
  }
  return $retval;
}

function getAcctDeviceList($connect,$acctNum,$acctDevType='C')
{
  $errmsg = "OK";
  $deviceList = array();
  $sql = "SELECT acctDevNum, acctDevName, acctDevMAC, ad.kegTypeNum, kegTypeDesc, ad.prodNum, prodName, acctDevAlarm  "
         ." FROM AcctDevices AS ad"
         ." LEFT JOIN Products AS p "
         ." ON p.prodNum = ad.prodNum "
         ." LEFT JOIN KegTypes AS kt "
         ." ON kt.kegTypeNum = ad.kegTypeNum "
         ." WHERE acctNum = ".$acctNum
         ." AND acctDevType = '".$acctDevType."'"
         ." ORDER BY acctDevName, prodName, acctDevMAC";
  $result = DBQuery($sql,$connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,$filename,"SELDEVICE");
  if ($errmsg == "OK")
    while ($lcol = DBFetchAssoc($result))
      array_push($deviceList, $lcol);

  return $deviceList;
}

function getAcctDeviceActionList($connect,$acctDevNum)
{
  $actionList = array();

  $sql = "SELECT devActNum, devActLevel, devActType, devActVal  "
         ." FROM AcctDeviceActions "
         ." WHERE acctDevNum = ".$acctDevNum
         ." ORDER BY devActType, devActVal";
  $result = DBQuery($sql,$connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,$filename,"SELDEVACT");
  if ($errmsg == "OK")
    while ($lcol = DBFetchAssoc($result))
      array_push($actionList, $lcol);

  return $actionList;
}

function getAcctProfileEdit($acct)
{
  $namehdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Full Name"  : "Company";
  $aph1hdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Home Phone" : "Office Phone";
  $aph2hdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Work Phone" : "Fax Phone";
 if ($acct['mailListKey'] != ""){
  $mailHTML = <<<EOT
  <div class="col-md-4">
    <div class="form-group">
      <label for="txtacctmailListKey">Postcard #</label>
      <input type='text' id='txtacctmailListKey' readonly='readonly' value="{$acct['mailListKey']}"/>
    </div>
  </div>
EOT;
 }else{
  $mailHTML = "";
 }
$stateOptions=getStateSelectOptions($acct['acctState']);
$countryOptions ="<select id='selacctCountry' class='form-control'>"
            ."<option value='USA'".($acct['acctCountry']=='USA'?" selected='selected' ":"").">United States</option>"
            ."<option value='OTH'".($acct['acctCountry']=='OTH'?" selected='selected' ":"").">Other</option>"
        ."</select>";

$currencyOptions = "<select id='selacctCurrency' class='form-control'>
                <option selected='selected' value='USD'>USD</option>
                <option value='EUD'".($acct['acctCurrency']=='EUD'?" selected='selected' ":"").">EURO</option>
                <option value='AUS'".($acct['acctCurrency']=='AUS'?" selected='selected' ":"").">AUD</option>
            </select>";
$tempOptions = getTempTypeSelectBox('selacctTempType');
$volumeOptions = getVolumeTypeSelectBox('selacctVolumeType');
$timezoneOptions = getTimezoneSelectBox('selacctTimezone');
$retval = <<<"EOT"
  <div class="panel panel-default">
    <div class="panel-heading"><h2 class="panel-title">Account Information</h2></div>
    <div class="divAcctEdit panel-body">
      <input type="hidden" id="txtacctType" value="{$acct['acctType']}" />
      <input type="hidden" id="txtacctNum" value="{$acct['acctNum']}" />
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctID">KegData ID</label>
            <input type='text' id='txtacctID' class="form-control" readonly='readonly' value="{$acct['acctID']}" />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctName">$namehdg<span class="text-danger fa fa-asterisk"></span></label>
            <input type='text' id='txtacctName' class="form-control" value="{$acct['acctName']}" required/>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctAddr1">Address 1<span class="text-danger fa fa-asterisk"></span></label>
            <input type='text' id='txtacctAddr1' class="form-control" value="{$acct['acctAddr1']}" required/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctAddr2">Address 2</label>
            <input type='text' id='txtacctAddr2' class="form-control" value="{$acct['acctAddr2']}" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="txtacctCity">City<span class="text-danger fa fa-asterisk"></span></label>
            <input type='text' id='txtacctCity' class="form-control" value="{$acct['acctCity']}" required/>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="selacctState">State<span class="text-danger fa fa-asterisk"></span></label>
            <select id='selacctState' class="form-control" required>
              {$stateOptions}
              <option value='EX'>Non USA</option>
            </select> 
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="txtacctZip">Zip Code</label>
            <input type='number' id='txtacctZip' class="form-control" value="{$acct['acctZip']}"/>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctCounty">County</label>
            <input type='text' id='txtacctCounty' class="form-control" value="{$acct['acctCounty']}"/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="selacctCountry">Country</label>
            $countryOptions
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctPhone1">$aph1hdg<span class="text-danger fa fa-asterisk"></span></label>
            <input type='text' id='txtacctPhone1' class="form-control" value="{$acct['acctPhone1']}" required/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctPhone2">$aph2hdg</label>
            <input type='text' id='txtacctPhone2' class="form-control" value="{$acct['acctPhone2']}"/>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="txtacctEntCode">Enterprise Code</label>
            <input type='text' id='txtacctEntCode' class="form-control" value="{$acct['enterpriseCode']}"/>
          </div>
        </div>
        $mailHTML
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="selacctCurrency">Currency</label>
            $currencyOptions
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="selacctTimezone">Timezone</label>
            {$timezoneOptions}
            <input type='checkbox' id='chkacctDST' /><label for='chkacctDST' >Adjust for DST</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="selacctTempType">Temperature</label>
            {$tempOptions}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="selacctVolumeType">Volume</label>
            {$volumeOptions}
          </div>
        </div>
      </div>
    </div>
  </div>
EOT;
  return $retval;
}
function getAcctProfileDisp($acct)
{
  $namehdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Full Name"  : "Company";
  $aph1hdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Home Phone" : "Office Phone";
  $aph2hdg = $acct['acctType'] == ACCTTYPE_PRIV ? "Work Phone"  : "Fax Phone";
  $entCode = ($acct['enterpriseCode'] == "" ? "None" : $acct['enterpriseCode']);
  $mailKey = $acct['mailListKey'] != "" ? "<tr><td class='label label-default'>Postcard</td><td>".$acct['mailListKey']."</td></tr>" : "";
  $addr2 = $acct['acctAddr2'] != "" ? $acct['acctAddr2']."<br />" : "";

$retVal = <<<EOT
    <div class="panel panel-default">
      <input type='hidden' id='txtacctType' value="{$acct['acctType']}"/>
      <input type='hidden' id='txtacctNum' value='{$acct['acctNum']}'/  >
      <div class="panel-heading"><h2 class="panel-title">Account Information</h2></div>
      <table class="table">
        <tr><th>KegData ID</th><td>{$acct['acctID']}</td></tr>
        {$mailKey}
        <tr><th>$namehdg</th><td>{$acct['acctName']}</td></tr>
        <tr><th>Address</th><td><address>{$acct['acctAddr1']}<br />
                                         {$addr2}
                                         {$acct['acctCity']}, {$acct['acctState']} {$acct['acctZip']}<br />
                                         {$acct['acctCountry']}</address> </td></tr>
        <tr><th>Currency</th><td>{$acct['acctCurrency']}</td></tr>
        <tr><th>$aph1hdg</th><td>{$acct['acctPhone1']}</td></tr>
        <tr><th>$aph2hdg</th><td>{$acct['acctPhone2']}</td></tr>
        
        <tr><th>Enterprise Code</th><td>$entCode</td></tr>
      </table>
    </div>
EOT;
  return $retVal;
}





?>
