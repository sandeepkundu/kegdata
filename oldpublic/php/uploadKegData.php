<?
header('Connection: close');
  if($_SERVER['SERVER_NAME'] == 'kegdata.local'){
    define('HOME_PATH', DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'www'.DIRECTORY_SEPARATOR.'public_html');
    define('dbServer', 'localhost');
  }else{
    define('HOME_PATH', '/var/www/html/kegdata'); 
    define('WEB_ROOT', 'http://localhost:1114');
    define('dbServer', 'localhost');
  }


    require_once(HOME_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'config.php');
    require_once(HOME_PATH.DIRECTORY_SEPARATOR.'beerkegs'.DIRECTORY_SEPARATOR.'config.php');
    require_once(HOME_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'include'.DIRECTORY_SEPARATOR.'tables.php');
    require_once(HOME_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'medoo.min.php');

  function formatMACforsql($astring){
    $astring = trim($astring);
    $astring = strtoupper($astring);
    while (strstr($astring,":"))
      $astring = str_replace(":","",$astring);
    while (strstr($astring,"-"))
      $astring = str_replace("-","",$astring);
    $astring = substr($astring,0,12);
    return $astring;
  }

  function logData($string,$msg){
    global $db;
    $table = 'kegdata_logs';
    $data = array('string' => $string, 'message' => $msg, 'time' => date('Y-m-d H:i:s'));

    $result = $db->insert($table,$data);
    $error = $db->error();
    if($error[0] == '00000'){
      return 'log complete';
    }else{
      return $error[2];
    }

  }

  if (! isset($_REQUEST['a'])){
    $err = "NOT OK";
    $echo .= "ERR|no data";
    $echo .= "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    $echo .= "<br/>The string must end with |K to ensure complete reception";
    $echo .= "<br/>or with |KV to echo back data received";
    exit();
  }

  $dataIn = $_REQUEST['a'];
  $dataParts = explode("|",$dataIn);

  if ($dataIn == "GETTIME"){
    echo date("Y-m-d H:i:s");
    exit();
  }


  if (Count($dataParts) != 10){
    $err = "NOT OK";
    $echo .= "ERR|incomplete data";
    $echo .= "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    $echo .= "<br/>The string must end with |K to ensure complete reception";
    $echo .= "<br/>or with |KV to echo back data received";
    exit;
  }

  if (strtoupper($dataParts[9]) != "K" && strtoupper($dataParts[9]) != "KV"){
    $err = "NOT OK";
    $echo .= "ERR|invalid data";
    $echo .= "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    $echo .= "<br/>The string must end with |K to ensure complete reception";
    $echo .= "<br/>or with |KV to echo back data received";
    exit;
  }

  //HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K
  $hubMAC      = formatMACforsql($dataParts[0]);
  $sentDtTm    = $dataParts[1];
  $kegMAC      = formatMACforsql($dataParts[2]);
  $startVal    = hexdec($dataParts[3]);
  $pourVal     = hexdec($dataParts[4]);
  $endVal      = hexdec($dataParts[5]);
  $tempCelcius = hexdec($dataParts[6]); // tenths of a second,i.e. 0020 is 2 seconds
  $elTenths    = hexdec($dataParts[7]);
  $kegStatus   = decbin(hexdec($dataParts[8]));
  $endOfData   = strtoupper($dataParts[9]);
  $sentYear    = substr($sentDtTm, 0,4);
  $sentMonth   = substr($sentDtTm, 4,2);
  $sentDay     = substr($sentDtTm, 6,2);
  $sentHour    = substr($sentDtTm, 8,2);
  $sentMin     = substr($sentDtTm,10,2);
  $sentSec     = substr($sentDtTm,12,2);
  $sentDtTm    = $sentYear."-".$sentMonth."-".$sentDay." ".$sentHour.":".$sentMin.":".$sentSec;

  $echo = '';
  $db = new medoo();
  $err = "OK";

  $hubExists = $db->get(HUBS, array('hubMac', 'account_id' ), array('hubMac' => $hubMAC));
  if($hubExists != false){
    $kegExists = $db->count(KEGDEVICES, array('kegMac' => $kegMAC));
    if($kegExists == 0){
      $data = array(
        'kegMac' => $kegMAC,
        'account_id' => $hubExists['account_id'],
        'deviceName' => '',
        'kegType' => 1,
        'productID' => 0,
        'showDevice' => 1,
        'modified' => date('Y-m-d H:i:s'),
        'modifiedBy' => 0
      );

      $db->insert(KEGDEVICES, $data);
      $dbError = $db->error();
      if($dbError[0] != '00000'){
        $err = $dbError[2];
        $echo .= 'ISSUE WITH COUPLER INIT';
      }
    } //Close coupler init

    if($err == 'OK'){
      $data = array(
        'hubMac' => $hubMAC,
        'kegMac' => $kegMAC,
        'sentDateTime' => $sentDtTm,
        'startVal' => $startVal,
        'pourVal' => $pourVal,
        'endVal' => $endVal,
        'tempCelcius' => $tempCelcius,
        'elapsedTenths' => $elTenths,
        'status' => $kegStatus,
        'receivedDateTime' => date('Y-m-d H:i:s')
      );
      $db->insert(KEGDATARECEIVED, $data);
      $dbError = $db->error();
      if($dbError[0] != '00000'){
        $err = $dbError[2];
        $echo .= 'ISSUE WITH RECEIVING DATA';
      }

    }//Close insert into KegDataRecieved


  }else{
     $err = 'ISSUE WITH HUB MAC';
     $echo .= 'HUB MAC NOT FOUND';
  }

  logData($dataIn, $err);

  if ($endOfData == "KV")
  {
    $echo .= "<br/><div style='clear:both; float:left; text-align:left'>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>hubMAC     </label><label style='float:left;'>".formatMACfordisplay($hubMAC)."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>sentDtTm   </label><label style='float:left;'>".$sentDtTm   ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>kegMAC     </label><label style='float:left;'>".formatMACfordisplay($kegMAC)."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>startVal   </label><label style='float:left;'>".$startVal   ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>pourVal    </label><label style='float:left;'>".$pourVal    ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>endVal     </label><label style='float:left;'>".$endVal     ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>elTenths   </label><label style='float:left;'>".$elTenths   ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>tempCelcius</label><label style='float:left;'>".$tempCelcius."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>kegStatus  </label><label style='float:left;'>".$kegStatus  ."</label>";
    $echo .= "<label style='clear:both; float:left; width:100px;'>endOfData  </label><label style='float:left;'>".$endOfData  ."</label>";
    $echo .= "</div>";
  }

  if($err != 'OK' || $endOfData == "KV"){
    echo $echo;
  }else{
    echo $err;
  }
  $db = null;
  exit();
?>
