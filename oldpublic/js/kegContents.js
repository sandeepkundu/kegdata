//reliant on typeahead.bundle.min.js
var kegs = new Bloodhound({
	datumTokenizer: function(obj) {
	    return Bloodhound.tokenizers.whitespace(obj.prodName);
	},
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	limit:50,
	prefetch: {
		url: '/api/beer',
		cacheKey: 'http://www.kegdata.com/api/beer',
		ajax : {global: false}
	},
	remote: {
		url: '/api/beer/%QUERY',
		ajax : {global: false}
	}
});

kegs.initialize();

var emptyDatum = {name: 'Not in database', id: 0};

var sourceWithEmptySelectable = function(q, cb) {
  kegs.get(q, injectEmptySelectable);

  function injectEmptySelectable(suggestions) {
    if (suggestions.length === 0) {
      cb([emptyDatum]);
    }else {
    	var check = $.grep(suggestions, function(e){return e.prodNum == 0});

    if(check.length == 0){suggestions.unshift(emptyDatum);}
      cb(suggestions);
    }
  }
};

$('#kegContents').typeahead({
	hint: true,
	highlight: true,
	minLength: 0
},
{
	name: 'kegs',
	displayKey: 'name',
	valueKey: 'id',
	source: sourceWithEmptySelectable
}
);

//Set prodNum in hidden field when selected
$('#kegContents').on("typeahead:selected typeahead:autocompleted", function(e,datum) { 
	$('#contentsID').val(datum.id);
});