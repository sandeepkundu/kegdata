<h3>Calculating m and b values of the equation y=mx+b</h3><br><br>
<form action="" method="post">

<label>Note: please give comma seprated value</label>
X-Axis <input type="text" name="x" value="<?php echo (isset($_POST['x'])) ? $_POST['x']:'1, 2, 3, 4,5,6' ?>">
<br><br>
<label>Note: please give comma seprated value</label>
Y-Axis <input type="text" name="y" value="<?php echo (isset($_POST['y'])) ? $_POST['y']: '662,662,654,651,642,608' ?>">
<br><br>
<input type="submit" name="submit" value="submit">

</form>

<?php
/**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */
function linear_regression($x, $y) {

  // calculate number points
  $n = count($x);
  
  // ensure both arrays of points are the same size
  if ($n != count($y)) {

    trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
  
  }

  // calculate sums
  $x_sum = array_sum($x);
  $y_sum = array_sum($y);

  $xx_sum = 0;
  $xy_sum = 0;
  
  for($i = 0; $i < $n; $i++) {
  
    $xy_sum+=($x[$i]*$y[$i]);
    $xx_sum+=($x[$i]*$x[$i]);
    
  }
  
  // calculate slope
  $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
  
  // calculate intercept
  $b = ($y_sum - ($m * $x_sum)) / $n;
    
  // return result
  return array("m"=>$m, "b"=>$b);

}
//var_dump( linear_regression(array(1, 2, 3, 4,5,6), array(608,642,651,654,662,662)) );

if(isset($_POST['submit'])) {
  extract($_POST);
  
  $x_arr=explode(",",$x);
  $y_arr=explode(",",$y);
  $output=linear_regression($x_arr, $y_arr) ;  

} else {
  $output=linear_regression(array(1, 2, 3, 4,5,6), array(662,662,654,651,642,608)) ;  
}



echo 'Output is:';
echo '<br><br>';
echo 'Value of m is: '.number_format($output['m'],2);
echo '<br><br>';
echo 'Value of b is: '.number_format($output['b'],2);
echo '<br><br>';
//var_dump( linear_regression(array(1, 2, 3, 4,5,6), array(19.8, 36.8,45.2, 520.8,35,38.8)) );
?>


