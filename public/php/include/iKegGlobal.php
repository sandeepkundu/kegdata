<?
//date_default_timezone_set("UTC");
date_default_timezone_set("America/Chicago");
define("IKEG_GLOBAL_PHP", true);
define("IKEG_PURPLE", "#62055E");

define("RECNUM_NONE","-1");

define("IKEG_FULL",  "0417");
define("IKEG_EMPTY", "0078");

define("ORDER_NEW",  "0");
define("ORDER_PAID", "1");
define("ORDER_RCVD", "2");
define("ORDER_SHIP", "3");

define("ORDER_ITEM_NEW",    "0");
define("ORDER_ITEM_SHIP",   "1");
define("ORDER_ITEM_CANCEL", "2");

define("ORDER_KIT_NEW",    "0");
define("ORDER_KIT_SHIP",   "1");
define("ORDER_KIT_CANCEL", "2");


define("MEASURE_OZ", "oz");
define("MEASURE_ML", "ml");
define("MEASURE_CC", "cc");
define("MEASURE_LTR", "L");
define("MEASURE_GAL", "Gal");

function convertMeasurement($inVal,$inType,$outType)
{
  $outVal = 0;
  switch ($inType)
  {
    case MEASURE_OZ:
      switch ($outType)
      {
        case MEASURE_OZ :
          $outVal = $inVal;
          break;
        case MEASURE_ML :
        case MEASURE_CC :
          $outVal = 29.5735 * $inVal;
          break;
        case MEASURE_LTR:
          $outVal = .0295735 * $inVal;
          break;
        case MEASURE_GAL:
          $outVal = $inVal / 128;
          break;
      }
      break;
    case MEASURE_ML:
    case MEASURE_CC:
      switch ($outType)
      {
        case MEASURE_OZ :
          $outVal = $inVal * .033814;
          break;
        case MEASURE_ML :
        case MEASURE_CC :
          $outVal = $inVal;
          break;
        case MEASURE_LTR:
          $outVal = $inVal * 1000;
          break;
        case MEASURE_GAL:
          $outVal = $inVal * .000264172;
          break;
      }
      break;
    case MEASURE_LTR:
      switch ($outType)
      {
        case MEASURE_OZ :
          $outVal = $inVal * 33.814;
          break;
        case MEASURE_ML :
        case MEASURE_CC :
          $outVal = $inVal * 1000;
          break;
        case MEASURE_LTR:
          $outVal = $inVal;
          break;
        case MEASURE_GAL:
          $outVal = $inVal * .264172;
          break;
      }
      break;
    case MEASURE_GAL:
      switch ($outType)
      {
        case MEASURE_OZ :
          $outVal = $inVal * 128;
          break;
        case MEASURE_ML :
        case MEASURE_CC :
          $outVal = $inVal * 3785.41;
          break;
        case MEASURE_LTR:
          $outVal = $inVal * 3.78541;
          break;
        case MEASURE_GAL:
          $outVal = $inVal;
          break;
      }
      break;
    default:
      $outVal = $inVal;
      break;
  }
}


function iKegPWDEncrypt($pwdtoencrypt)
{
  return md5($pwdtoencrypt);
}

function formatMACfordisplay($astring)
{
  $astring = strtoupper($astring);
  $astring = substr($astring, 0,2)
        .":".substr($astring, 2,2)
        .":".substr($astring, 4,2)
        .":".substr($astring, 6,2)
        .":".substr($astring, 8,2)
        .":".substr($astring,10,2);
  return $astring;
}

function formatMACforsql($astring)
{
  $astring = trim($astring);
  $astring = strtoupper($astring);
  while (strstr($astring,":"))
    $astring = str_replace(":","",$astring);
  while (strstr($astring,"-"))
    $astring = str_replace("-","",$astring);
  $astring = substr($astring,0,12);
  return $astring;
}

function formatquoteforsql($astring)
{
//echo "sql $astring ";
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("'","''",$astring);
//echo $astring . "<br>";
  return $astring;
}

function formatquotefordisplay($astring)
{
//echo "disp $astring ";
  $astring = str_replace("''","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
  $astring = str_replace("\'","'",$astring);
//echo $astring . "<br>";
  return $astring;
}


function getShiftedNumber($aval)
{
  $retval = "X";
  switch(substr($aval,-1))
  {
    case "0": $retval = ")"; break;
    case "1": $retval = "!"; break;
    case "2": $retval = "@"; break;
    case "3": $retval = "#"; break;
    case "4": $retval = "$"; break;
    case "5": $retval = "%"; break;
    case "6": $retval = "^"; break;
    case "7": $retval = "&"; break;
    case "8": $retval = "*"; break;
    case "9": $retval = "("; break;
  }
  return $retval;
}

function getContentLogoTitle($titleStr)
{
  return "<div class='divContentSection'>
           <div class='divTitle' >
		     <div class='divTitleImg'>
               <img class='imgTitle' src='mainart/logo_tn.jpg'>
             </div>
             <div class='divTitleText'>
               $titleStr
             </div>
           </div>
          </div>";
}




function getCreditCardTypeSelectionBox($selectId, $myVal)
{
  $retval = "<select id='".$selectId."' class='entryDropDown'>"
 ."<option value='0' ".($myVal == "0"   ? " selected='selected' " : "")."></option>"
 ."<option value='4' ".($myVal == "4" ? " selected='selected' " : "").">Visa</option>"
 ."<option value='5' ".($myVal == "5" ? " selected='selected' " : "").">Mastercard</option>"
 ."<option value='6' ".($myVal == "6" ? " selected='selected' " : "").">Discover</option>"
 ."<option value='3' ".($myVal == "3" ? " selected='selected' " : "").">Am Ex</option>"
 ."</select>";

 return $retval;
}

function getMeasurementSelectionBox($selectId, $myVal)
{
  $retval = "<select id='".$selectId."' class='entryDropDown'>"
 ."<option value=''   ".($myVal == ""   ? " selected='selected' " : "")."></option>"
 ."<option value='oz' ".($myVal == "oz" ? " selected='selected' " : "").">oz</option>"
 ."<option value='cc' ".($myVal == "cc" ? " selected='selected' " : "").">cc</option>"
 ."<option value='ml' ".($myVal == "ml" ? " selected='selected' " : "").">ml</option>"
 ."<option value='Gal' ".($myVal == "Gal" ? " selected='selected' " : "").">Gal</option>"
 ."<option value='L' ".($myVal == "L" ? " selected='selected' " : "").">L</option>"
 ."</select>";

 return $retval;
}

function getUserTypeSelectionBox($selectId, $myVal)
{
  if ($myVal != "C" && $myVal != "D" && $myVal != "M" && $myVal != "S")
    $myVal = "";

  $retval = ""
    ."<select id='".$selectId."'  class='entryDropDown'>"
      ."<option id='userTypeNone'  value='X' ".($myVal == ""   ? " selected='selected' " : "").">...(select)...</option>"
      ."<option id='userTypeCust'  value='C' ".($myVal == "C"   ? " selected='selected' " : "").">Customer</option>"
      ."<option id='userTypeDist'  value='D' ".($myVal == "D"   ? " selected='selected' " : "").">Distributor</option>"
      ."<option id='userTypeMfg'   value='M' ".($myVal == "M"   ? " selected='selected' " : "").">Manufacturer</option>"
      ."<option id='userTypeStaff' value='S' ".($myVal == "S"   ? " selected='selected' " : "").">iKeg Staff</option>"
    ."</select>";

 return $retval;
}

function getNumberSelectBox($selID,$disabled="N",$minnum="1",$maxnum="10",$myval="1")
{
  $readonly = "";
  if ($disabled == "Y")
    $readonly = " disabled='Y' ";
  $retval = "<select id='".$selID."' class='entrySelect' $readonly >";
  for ($x = $minnum; $x <= $maxnum; $x++)
  {
    $retval .= "<option value='".$x."' ". (($x == $myval) ? "selected='selected' " : "") .">".$x."</option>";
  }
  $retval .= "</select>";
  return $retval;
}

function getItemCategoryName($myVal="0")
{
  $retval = "";
  switch ($myVal)
  {
    case "1":
      $retval = "Control Unit";
      break;
    case "2":
      $retval = "IKeg Tap";
      break;
    case "3":
      $retval = "Standard Tap";
      break;
    case "4":
      $retval = "Accessory";
      break;
    case "5":
      $retval = "Faucet";
      break;
    case "6":
      $retval = "IKeg Tap Part";
      break;
    case "7":
      $retval = "Std. Tap Part";
      break;
    case "8":
      $retval = "Other Item";
      break;
    default :
      $retval = "unspecified";
      break;
  }
  return $retval;
}


function getItemCategorySelectOptions($myVal="0")
{
  $retval = ""
 ."<option value='0' ".($myVal == "0" ? " selected='selected' " : "").">...(select)... </option>"
 ."<option value='1' ".($myVal == "1" ? " selected='selected' " : "").">".getItemCategoryName("1")."</option>"
 ."<option value='2' ".($myVal == "2" ? " selected='selected' " : "").">".getItemCategoryName("2")."</option>"
 ."<option value='3' ".($myVal == "3" ? " selected='selected' " : "").">".getItemCategoryName("3")."</option>"
 ."<option value='4' ".($myVal == "4" ? " selected='selected' " : "").">".getItemCategoryName("4")."</option>"
 ."<option value='5' ".($myVal == "5" ? " selected='selected' " : "").">".getItemCategoryName("5")."</option>"
 ."<option value='6' ".($myVal == "6" ? " selected='selected' " : "").">".getItemCategoryName("6")."</option>"
 ."<option value='7' ".($myVal == "7" ? " selected='selected' " : "").">".getItemCategoryName("7")."</option>"
 ."<option value='8' ".($myVal == "8" ? " selected='selected' " : "").">".getItemCategoryName("8")."</option>"
 ."";
 return $retval;
}

function getItemCategorySelectBox($selectId, $myVal="0")
{
  $retval = "<select id='".$selectId."' class='entryDropDown' >"
 .getItemCategorySelectOptions($myVal)
 ."</select>";
 return $retval;
}

function getMileageSelectBox($selectId, $myVal="")
{
  $retval = "<select id='".$selectId."' class='entryDropDown' >"
  ."<option value=''"   .($myVal == ""    ? " selected='selected' " : "")."></option>"
  ."<option value='5'"  .($myVal == "5"   ? " selected='selected' " : "").">5</option>"
  ."<option value='10'" .($myVal == "10"  ? " selected='selected' " : "").">10</option>"
  ."<option value='25'" .($myVal == "25"  ? " selected='selected' " : "").">25</option>"
  ."<option value='50'" .($myVal == "50"  ? " selected='selected' " : "").">50</option>"
  ."<option value='100'".($myVal == "100" ? " selected='selected' " : "").">100</option>"
  ."<option value='150'".($myVal == "150" ? " selected='selected' " : "").">150</option>"
  ."<option value='200'".($myVal == "200" ? " selected='selected' " : "").">200</option>"
  ."<option value='250'".($myVal == "250" ? " selected='selected' " : "").">250</option>"
 ."</select>";
 return $retval;
}

function getStateCodeSelectionBox($selectId, $myVal="")
{
  $retval = "<select id='".$selectId."' class='entryDropDown' >"
 ."<option value=''   ".($myVal == ""   ? " selected='selected' " : "")."></option>"
 ."<option value='AK' ".($myVal == "AK" ? " selected='selected' " : "").">AK</option>"
 ."<option value='AL' ".($myVal == "AL" ? " selected='selected' " : "").">AL</option>"
 ."<option value='AR' ".($myVal == "AR" ? " selected='selected' " : "").">AR</option>"
 ."<option value='AZ' ".($myVal == "AZ" ? " selected='selected' " : "").">AZ</option>"
 ."<option value='CA' ".($myVal == "CA" ? " selected='selected' " : "").">CA</option>"
 ."<option value='CO' ".($myVal == "CO" ? " selected='selected' " : "").">CO</option>"
 ."<option value='CT' ".($myVal == "CT" ? " selected='selected' " : "").">CT</option>"
 ."<option value='DC' ".($myVal == "DC" ? " selected='selected' " : "").">DC</option>"
 ."<option value='DE' ".($myVal == "DE" ? " selected='selected' " : "").">DE</option>"
 ."<option value='FL' ".($myVal == "FL" ? " selected='selected' " : "").">FL</option>"
 ."<option value='GA' ".($myVal == "GA" ? " selected='selected' " : "").">GA</option>"
 ."<option value='HI' ".($myVal == "HI" ? " selected='selected' " : "").">HI</option>"
 ."<option value='IA' ".($myVal == "IA" ? " selected='selected' " : "").">IA</option>"
 ."<option value='ID' ".($myVal == "ID" ? " selected='selected' " : "").">ID</option>"
 ."<option value='IL' ".($myVal == "IL" ? " selected='selected' " : "").">IL</option>"
 ."<option value='IN' ".($myVal == "IN" ? " selected='selected' " : "").">IN</option>"
 ."<option value='KS' ".($myVal == "KS" ? " selected='selected' " : "").">KS</option>"
 ."<option value='KY' ".($myVal == "KY" ? " selected='selected' " : "").">KY</option>"
 ."<option value='LA' ".($myVal == "LA" ? " selected='selected' " : "").">LA</option>"
 ."<option value='MA' ".($myVal == "MA" ? " selected='selected' " : "").">MA</option>"
 ."<option value='MD' ".($myVal == "MD" ? " selected='selected' " : "").">MD</option>"
 ."<option value='ME' ".($myVal == "ME" ? " selected='selected' " : "").">ME</option>"
 ."<option value='MI' ".($myVal == "MI" ? " selected='selected' " : "").">MI</option>"
 ."<option value='MN' ".($myVal == "MN" ? " selected='selected' " : "").">MN</option>"
 ."<option value='MO' ".($myVal == "MO" ? " selected='selected' " : "").">MO</option>"
 ."<option value='MS' ".($myVal == "MS" ? " selected='selected' " : "").">MS</option>"
 ."<option value='MT' ".($myVal == "MT" ? " selected='selected' " : "").">MT</option>"
 ."<option value='NC' ".($myVal == "NC" ? " selected='selected' " : "").">NC</option>"
 ."<option value='ND' ".($myVal == "ND" ? " selected='selected' " : "").">ND</option>"
 ."<option value='NE' ".($myVal == "NE" ? " selected='selected' " : "").">NE</option>"
 ."<option value='NH' ".($myVal == "NH" ? " selected='selected' " : "").">NH</option>"
 ."<option value='NJ' ".($myVal == "NJ" ? " selected='selected' " : "").">NJ</option>"
 ."<option value='NM' ".($myVal == "NM" ? " selected='selected' " : "").">NM</option>"
 ."<option value='NV' ".($myVal == "NV" ? " selected='selected' " : "").">NV</option>"
 ."<option value='NY' ".($myVal == "NY" ? " selected='selected' " : "").">NY</option>"
 ."<option value='OH' ".($myVal == "OH" ? " selected='selected' " : "").">OH</option>"
 ."<option value='OK' ".($myVal == "OK" ? " selected='selected' " : "").">OK</option>"
 ."<option value='OR' ".($myVal == "OR" ? " selected='selected' " : "").">OR</option>"
 ."<option value='PA' ".($myVal == "PA" ? " selected='selected' " : "").">PA</option>"
 ."<option value='RI' ".($myVal == "RI" ? " selected='selected' " : "").">RI</option>"
 ."<option value='SC' ".($myVal == "SC" ? " selected='selected' " : "").">SC</option>"
 ."<option value='SD' ".($myVal == "SD" ? " selected='selected' " : "").">SD</option>"
 ."<option value='TN' ".($myVal == "TN" ? " selected='selected' " : "").">TN</option>"
 ."<option value='TX' ".($myVal == "TX" ? " selected='selected' " : "").">TX</option>"
 ."<option value='UT' ".($myVal == "UT" ? " selected='selected' " : "").">UT</option>"
 ."<option value='VA' ".($myVal == "VA" ? " selected='selected' " : "").">VA</option>"
 ."<option value='VT' ".($myVal == "VT" ? " selected='selected' " : "").">VT</option>"
 ."<option value='WA' ".($myVal == "WA" ? " selected='selected' " : "").">WA</option>"
 ."<option value='WV' ".($myVal == "WV" ? " selected='selected' " : "").">WV</option>"
 ."<option value='WI' ".($myVal == "WI" ? " selected='selected' " : "").">WI</option>"
 ."<option value='WY' ".($myVal == "WY" ? " selected='selected' " : "").">WY</option>"
 ."</select>";

 return $retval;
}

function getStateSelectShortOptions($myVal="")
{
  return ""
 ."<option value=''   ".($myVal == ""   ? " selected='selected' " : "")."></option>"
 ."<option value='AK' ".($myVal == "AK" ? " selected='selected' " : "").">AK</option>"
 ."<option value='AL' ".($myVal == "AL" ? " selected='selected' " : "").">AL</option>"
 ."<option value='AR' ".($myVal == "AR" ? " selected='selected' " : "").">AR</option>"
 ."<option value='AZ' ".($myVal == "AZ" ? " selected='selected' " : "").">AZ</option>"
 ."<option value='CA' ".($myVal == "CA" ? " selected='selected' " : "").">CA</option>"
 ."<option value='CO' ".($myVal == "CO" ? " selected='selected' " : "").">CO</option>"
 ."<option value='CT' ".($myVal == "CT" ? " selected='selected' " : "").">CT</option>"
 ."<option value='DC' ".($myVal == "DC" ? " selected='selected' " : "").">DC</option>"
 ."<option value='DE' ".($myVal == "DE" ? " selected='selected' " : "").">DE</option>"
 ."<option value='FL' ".($myVal == "FL" ? " selected='selected' " : "").">FL</option>"
 ."<option value='GA' ".($myVal == "GA" ? " selected='selected' " : "").">GA</option>"
 ."<option value='HI' ".($myVal == "HI" ? " selected='selected' " : "").">HI</option>"
 ."<option value='IA' ".($myVal == "IA" ? " selected='selected' " : "").">IA</option>"
 ."<option value='ID' ".($myVal == "ID" ? " selected='selected' " : "").">ID</option>"
 ."<option value='IL' ".($myVal == "IL" ? " selected='selected' " : "").">IL</option>"
 ."<option value='IN' ".($myVal == "IN" ? " selected='selected' " : "").">IN</option>"
 ."<option value='KS' ".($myVal == "KS" ? " selected='selected' " : "").">KS</option>"
 ."<option value='KY' ".($myVal == "KY" ? " selected='selected' " : "").">KY</option>"
 ."<option value='LA' ".($myVal == "LA" ? " selected='selected' " : "").">LA</option>"
 ."<option value='MA' ".($myVal == "MA" ? " selected='selected' " : "").">MA</option>"
 ."<option value='MD' ".($myVal == "MD" ? " selected='selected' " : "").">MD</option>"
 ."<option value='ME' ".($myVal == "ME" ? " selected='selected' " : "").">ME</option>"
 ."<option value='MI' ".($myVal == "MI" ? " selected='selected' " : "").">MI</option>"
 ."<option value='MN' ".($myVal == "MN" ? " selected='selected' " : "").">MN</option>"
 ."<option value='MO' ".($myVal == "MO" ? " selected='selected' " : "").">MO</option>"
 ."<option value='MS' ".($myVal == "MS" ? " selected='selected' " : "").">MS</option>"
 ."<option value='MT' ".($myVal == "MT" ? " selected='selected' " : "").">MT</option>"
 ."<option value='NC' ".($myVal == "NC" ? " selected='selected' " : "").">NC</option>"
 ."<option value='ND' ".($myVal == "ND" ? " selected='selected' " : "").">ND</option>"
 ."<option value='NE' ".($myVal == "NE" ? " selected='selected' " : "").">NE</option>"
 ."<option value='NH' ".($myVal == "NH" ? " selected='selected' " : "").">NH</option>"
 ."<option value='NJ' ".($myVal == "NJ" ? " selected='selected' " : "").">NJ</option>"
 ."<option value='NM' ".($myVal == "NM" ? " selected='selected' " : "").">NM</option>"
 ."<option value='NV' ".($myVal == "NV" ? " selected='selected' " : "").">NV</option>"
 ."<option value='NY' ".($myVal == "NY" ? " selected='selected' " : "").">NY</option>"
 ."<option value='OH' ".($myVal == "OH" ? " selected='selected' " : "").">OH</option>"
 ."<option value='OK' ".($myVal == "OK" ? " selected='selected' " : "").">OK</option>"
 ."<option value='OR' ".($myVal == "OR" ? " selected='selected' " : "").">OR</option>"
 ."<option value='PA' ".($myVal == "PA" ? " selected='selected' " : "").">PA</option>"
 ."<option value='RI' ".($myVal == "RI" ? " selected='selected' " : "").">RI</option>"
 ."<option value='SC' ".($myVal == "SC" ? " selected='selected' " : "").">SC</option>"
 ."<option value='SD' ".($myVal == "SD" ? " selected='selected' " : "").">SD</option>"
 ."<option value='TN' ".($myVal == "TN" ? " selected='selected' " : "").">TN</option>"
 ."<option value='TX' ".($myVal == "TX" ? " selected='selected' " : "").">TX</option>"
 ."<option value='UT' ".($myVal == "UT" ? " selected='selected' " : "").">UT</option>"
 ."<option value='VA' ".($myVal == "VA" ? " selected='selected' " : "").">VA</option>"
 ."<option value='VT' ".($myVal == "VT" ? " selected='selected' " : "").">VT</option>"
 ."<option value='WA' ".($myVal == "WA" ? " selected='selected' " : "").">WA</option>"
 ."<option value='WI' ".($myVal == "WI" ? " selected='selected' " : "").">WI</option>"
 ."<option value='WV' ".($myVal == "WV" ? " selected='selected' " : "").">WV</option>"
 ."<option value='WY' ".($myVal == "WY" ? " selected='selected' " : "").">WY</option>";
}

function getStateSelectOptions($myVal="")
{
  return ""
 ."<option value=''   ".($myVal == ""   ? " selected='selected' " : "")."></option>"
 ."<option value='AL' ".($myVal == "AL" ? " selected='selected' " : "").">Alabama         </option>"
 ."<option value='AK' ".($myVal == "AK" ? " selected='selected' " : "").">Alaska          </option>"
 ."<option value='AZ' ".($myVal == "AZ" ? " selected='selected' " : "").">Arizona         </option>"
 ."<option value='AR' ".($myVal == "AR" ? " selected='selected' " : "").">Arkansas        </option>"
 ."<option value='CA' ".($myVal == "CA" ? " selected='selected' " : "").">California      </option>"
 ."<option value='CO' ".($myVal == "CO" ? " selected='selected' " : "").">Colorado        </option>"
 ."<option value='CT' ".($myVal == "CT" ? " selected='selected' " : "").">Connecticut     </option>"
 ."<option value='DE' ".($myVal == "DE" ? " selected='selected' " : "").">Delaware        </option>"
 ."<option value='DC' ".($myVal == "DC" ? " selected='selected' " : "").">Dist of Columbia</option>"
 ."<option value='FL' ".($myVal == "FL" ? " selected='selected' " : "").">Florida         </option>"
 ."<option value='GA' ".($myVal == "GA" ? " selected='selected' " : "").">Georgia         </option>"
 ."<option value='HI' ".($myVal == "HI" ? " selected='selected' " : "").">Hawaii          </option>"
 ."<option value='ID' ".($myVal == "ID" ? " selected='selected' " : "").">Idaho           </option>"
 ."<option value='IL' ".($myVal == "IL" ? " selected='selected' " : "").">Illinois        </option>"
 ."<option value='IN' ".($myVal == "IN" ? " selected='selected' " : "").">Indiana         </option>"
 ."<option value='IA' ".($myVal == "IA" ? " selected='selected' " : "").">Iowa            </option>"
 ."<option value='KS' ".($myVal == "KS" ? " selected='selected' " : "").">Kansas          </option>"
 ."<option value='KY' ".($myVal == "KY" ? " selected='selected' " : "").">Kentucky        </option>"
 ."<option value='LA' ".($myVal == "LA" ? " selected='selected' " : "").">Louisiana       </option>"
 ."<option value='ME' ".($myVal == "ME" ? " selected='selected' " : "").">Maine           </option>"
 ."<option value='MD' ".($myVal == "MD" ? " selected='selected' " : "").">Maryland        </option>"
 ."<option value='MA' ".($myVal == "MA" ? " selected='selected' " : "").">Massachusetts   </option>"
 ."<option value='MI' ".($myVal == "MI" ? " selected='selected' " : "").">Michigan        </option>"
 ."<option value='MN' ".($myVal == "MN" ? " selected='selected' " : "").">Minnesota       </option>"
 ."<option value='MS' ".($myVal == "MS" ? " selected='selected' " : "").">Mississippi     </option>"
 ."<option value='MO' ".($myVal == "MO" ? " selected='selected' " : "").">Missouri        </option>"
 ."<option value='MT' ".($myVal == "MT" ? " selected='selected' " : "").">Montana         </option>"
 ."<option value='NE' ".($myVal == "NE" ? " selected='selected' " : "").">Nebraska        </option>"
 ."<option value='NV' ".($myVal == "NV" ? " selected='selected' " : "").">Nevada          </option>"
 ."<option value='NH' ".($myVal == "NH" ? " selected='selected' " : "").">New Hampshire   </option>"
 ."<option value='NJ' ".($myVal == "NJ" ? " selected='selected' " : "").">New Jersey      </option>"
 ."<option value='NM' ".($myVal == "NM" ? " selected='selected' " : "").">New Mexico      </option>"
 ."<option value='NY' ".($myVal == "NY" ? " selected='selected' " : "").">New York        </option>"
 ."<option value='NC' ".($myVal == "NC" ? " selected='selected' " : "").">North Carolina  </option>"
 ."<option value='ND' ".($myVal == "ND" ? " selected='selected' " : "").">North Dakota    </option>"
 ."<option value='OH' ".($myVal == "OH" ? " selected='selected' " : "").">Ohio            </option>"
 ."<option value='OK' ".($myVal == "OK" ? " selected='selected' " : "").">Oklahoma        </option>"
 ."<option value='OR' ".($myVal == "OR" ? " selected='selected' " : "").">Oregon          </option>"
 ."<option value='PA' ".($myVal == "PA" ? " selected='selected' " : "").">Pennsylvania    </option>"
 ."<option value='RI' ".($myVal == "RI" ? " selected='selected' " : "").">Rhode Island    </option>"
 ."<option value='SC' ".($myVal == "SC" ? " selected='selected' " : "").">South Carolina  </option>"
 ."<option value='SD' ".($myVal == "SD" ? " selected='selected' " : "").">South Dakota    </option>"
 ."<option value='TN' ".($myVal == "TN" ? " selected='selected' " : "").">Tennessee       </option>"
 ."<option value='TX' ".($myVal == "TX" ? " selected='selected' " : "").">Texas           </option>"
 ."<option value='UT' ".($myVal == "UT" ? " selected='selected' " : "").">Utah            </option>"
 ."<option value='VT' ".($myVal == "VT" ? " selected='selected' " : "").">Vermont         </option>"
 ."<option value='VA' ".($myVal == "VA" ? " selected='selected' " : "").">Virginia        </option>"
 ."<option value='WA' ".($myVal == "WA" ? " selected='selected' " : "").">Washington      </option>"
 ."<option value='WV' ".($myVal == "WV" ? " selected='selected' " : "").">West Virginia   </option>"
 ."<option value='WI' ".($myVal == "WI" ? " selected='selected' " : "").">Wisconsin       </option>"
 ."<option value='WY' ".($myVal == "WY" ? " selected='selected' " : "").">Wyoming         </option>";
}

function getStateSelectionBox($selectId, $myVal="")
{
  $retval = "<select id='".$selectId."' class='entryDropDown' >"
  .getStateSelectOptions($myVal)
 ."</select>";

 return $retval;
}

function getSelectBox($selID,$selOpt = "0")
{
$retval = "<select id='".$selID."' class='entrySelect'>"
."<option value='0' ". ($selOpt == '0' ? " selected='selected' " : "") . ">...(none)...</option>"
."<option value='1' ". ($selOpt == '1' ? " selected='selected' " : "") . ">Half Barrel</option>"
."<option value='2' ". ($selOpt == '2' ? " selected='selected' " : "") . ">Pony Keg</option>"
."<option value='3' ". ($selOpt == '3' ? " selected='selected' " : "") . ">Qtr Barrel</option>"
."<option value='4' ". ($selOpt == '4' ? " selected='selected' " : "") . ">Cylinder</option>"
 ."</select>";

 return $retval;
}


function getCountrySelectionBox($connect,$selID, $defVal='-1')
{
  $errmsg = "OK";
  $sql = "SELECT countryISO, countryName FROM CountryList ORDER BY countryName ";
  $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"iKegGlobal","SELCOUNTRYLIST");
  $retval  = "<select id='".$selID."'  class='entrySelect' >";
  if ($errmsg != "OK")
    $retval .= "<option value='-1' selected='selected'>System Error ".$errmsg."</option>";
  else
  {
    $retval .= "<option value='-1' ".(($defVal=='AF') ? " selected='selected' " : "").">...(select)...</option>";
    while ($lcol = @mysql_fetch_assoc($result))
    {
      $retval .= "<option value='".$lcol['countryISO']."' ".(($defVal==$lcol['countryISO']) ? " selected='selected' " : "")." >".$lcol['countryName']."</option>";
    }
  }
  $retval  .= "</select>";
  return $retval;
}

function getDBKegTypeSelectOptions($connect,$selOpt = "0")
{
  $errmsg = "OK";
  $retval = ""
          ."<option value='0' ". ($selOpt == "0" ? " selected='selected' " : ""). ">...(No Keg)...</option>";

  $sql = "SELECT kegTypeNum, kegTypeDesc "
        ." FROM KegTypes "
        ." ORDER BY kegTypeNum ";

  $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"iKegGlobal","SELKEGTYPE");

  if ($errmsg != "OK")
    $retval .= "<option value='-1' selected='selected'>System Error $errmsg</option>";
  else
  {
    while ($lcol = @mysql_fetch_assoc($result))
    {
      $kegTypeNum  = $lcol['kegTypeNum'];
      $kegTypeDesc = formatquotefordisplay($lcol['kegTypeDesc']);
      $retval .= "<option value='".$kegTypeNum."' ". ($selOpt == $kegTypeNum ? " selected='selected' " : "") .">".$kegTypeDesc."</option>";
    }
  }
  return $retval;
}

function getDBKegTypeSelectBox($connect,$selID, $selOpt = "0")
{
  $errmsg = "OK";
  $retval = "<select id='".$selID."' class='form-control'>";
  $retval .= getDBKegTypeSelectOptions($connect,$selOpt);
  $retval .= "</select>";
  return $retval;
}


function getTempTypeSelectBox($selID,$selOpt = "F")
{
  return "<select id='".$selID."' class='form-control' >"
  ."<option value='F' ".(($selOpt == 'F') ? " selected='selected'" : "")." >Farenheit</option>"
  ."<option value='C' ".(($selOpt == 'C') ? " selected='selected'" : "")." >Celcius</option>"
  ."</select>";
}

function getVolumeTypeSelectBox($selID,$selOpt = "OZ")
{
  return "<select id='".$selID."' class='form-control' >"
  ."<option value='OZ' ".(($selOpt == 'OZ') ? " selected='selected'" : "")." >OZ</option>"
  ."<option value='CC' ".(($selOpt == 'CC') ? " selected='selected'" : "")." >CC</option>"
  ."</select>";
}

function getTimezoneSelectBox($selID,$selOpt = "0")
{
  return "<select id='".$selID."' class='form-control' >"
  ."<option value='-12' ".(($selOpt == '-12') ? " selected='selected'" : "")." >UTC-12</option>"
  ."<option value='-10' ".(($selOpt == '-10') ? " selected='selected'" : "")." >UTC-11</option>"
  ."<option value='-10' ".(($selOpt == '-10') ? " selected='selected'" : "")." >UTC-10</option>"
  ."<option value='-9'  ".(($selOpt == '-9' ) ? " selected='selected'" : "")." >UTC-9</option>"
  ."<option value='-8'  ".(($selOpt == '-8' ) ? " selected='selected'" : "")." >UTC-8</option>"
  ."<option value='-7'  ".(($selOpt == '-7' ) ? " selected='selected'" : "")." >UTC-7</option>"
  ."<option value='-6'  ".(($selOpt == '-6' ) ? " selected='selected'" : "")." >UTC-6</option>"
  ."<option value='-5'  ".(($selOpt == '-5' ) ? " selected='selected'" : "")." >UTC-5</option>"
  ."<option value='-4'  ".(($selOpt == '-4' ) ? " selected='selected'" : "")." >UTC-4</option>"
  ."<option value='-3'  ".(($selOpt == '-3' ) ? " selected='selected'" : "")." >UTC-3</option>"
  ."<option value='-2'  ".(($selOpt == '-2' ) ? " selected='selected'" : "")." >UTC-2</option>"
  ."<option value='-1'  ".(($selOpt == '-1' ) ? " selected='selected'" : "")." >UTC-1</option>"
  ."<option value='0'   ".(($selOpt == '0'  ) ? " selected='selected'" : "")." >UTC</option>"
  ."<option value='+1'  ".(($selOpt == '+1' ) ? " selected='selected'" : "")." >UTC+1</option>"
  ."<option value='+2'  ".(($selOpt == '+2' ) ? " selected='selected'" : "")." >UTC+2</option>"
  ."<option value='+3'  ".(($selOpt == '+3' ) ? " selected='selected'" : "")." >UTC+3</option>"
  ."<option value='+4'  ".(($selOpt == '+4' ) ? " selected='selected'" : "")." >UTC+4</option>"
  ."<option value='+5'  ".(($selOpt == '+5' ) ? " selected='selected'" : "")." >UTC+5</option>"
  ."<option value='+6'  ".(($selOpt == '+6' ) ? " selected='selected'" : "")." >UTC+6</option>"
  ."<option value='+7'  ".(($selOpt == '+7' ) ? " selected='selected'" : "")." >UTC+7</option>"
  ."<option value='+8'  ".(($selOpt == '+8' ) ? " selected='selected'" : "")." >UTC+8</option>"
  ."<option value='+9'  ".(($selOpt == '+9' ) ? " selected='selected'" : "")." >UTC+9</option>"
  ."<option value='+10' ".(($selOpt == '+10') ? " selected='selected'" : "")." >UTC+10</option>"
  ."<option value='+11' ".(($selOpt == '+11') ? " selected='selected'" : "")." >UTC+11</option>"
  ."<option value='+12' ".(($selOpt == '+12') ? " selected='selected'" : "")." >UTC+12</option>"
  ."<option value='+13' ".(($selOpt == '+13') ? " selected='selected'" : "")." >UTC+13</option>"
  ."<option value='+14' ".(($selOpt == '+14') ? " selected='selected'" : "")." >UTC+14</option>"
  ."</select>";
}

function getAlarmLevelSelectOptions($selOpt = "0")
{
  return ""
."<option value='0'  ". ($selOpt == '0'  ? " selected='selected' " : "") . ">Empty</option>"
."<option value='1'  ". ($selOpt == '1'  ? " selected='selected' " : "") . ">1%</option>"
."<option value='2'  ". ($selOpt == '2'  ? " selected='selected' " : "") . ">2%</option>"
."<option value='3'  ". ($selOpt == '3'  ? " selected='selected' " : "") . ">3%</option>"
."<option value='4'  ". ($selOpt == '4'  ? " selected='selected' " : "") . ">4%</option>"
."<option value='5'  ". ($selOpt == '5'  ? " selected='selected' " : "") . ">5%</option>"
."<option value='6'  ". ($selOpt == '6'  ? " selected='selected' " : "") . ">6%</option>"
."<option value='7'  ". ($selOpt == '7'  ? " selected='selected' " : "") . ">7%</option>"
."<option value='8'  ". ($selOpt == '8'  ? " selected='selected' " : "") . ">8%</option>"
."<option value='9'  ". ($selOpt == '9'  ? " selected='selected' " : "") . ">9%</option>"
."<option value='10' ". ($selOpt == '10' ? " selected='selected' " : "") . ">10%</option>"
."<option value='11' ". ($selOpt == '11' ? " selected='selected' " : "") . ">11%</option>"
."<option value='12' ". ($selOpt == '12' ? " selected='selected' " : "") . ">12%</option>"
."<option value='13' ". ($selOpt == '13' ? " selected='selected' " : "") . ">13%</option>"
."<option value='14' ". ($selOpt == '14' ? " selected='selected' " : "") . ">14%</option>"
."<option value='15' ". ($selOpt == '15' ? " selected='selected' " : "") . ">15%</option>"
."<option value='16' ". ($selOpt == '16' ? " selected='selected' " : "") . ">16%</option>"
."<option value='17' ". ($selOpt == '17' ? " selected='selected' " : "") . ">17%</option>"
."<option value='18' ". ($selOpt == '18' ? " selected='selected' " : "") . ">18%</option>"
."<option value='19' ". ($selOpt == '19' ? " selected='selected' " : "") . ">19%</option>"
."<option value='20' ". ($selOpt == '20' ? " selected='selected' " : "") . ">20%</option>"
."<option value='21' ". ($selOpt == '21' ? " selected='selected' " : "") . ">21%</option>"
."<option value='22' ". ($selOpt == '22' ? " selected='selected' " : "") . ">22%</option>"
."<option value='23' ". ($selOpt == '23' ? " selected='selected' " : "") . ">23%</option>"
."<option value='24' ". ($selOpt == '24' ? " selected='selected' " : "") . ">24%</option>"
."<option value='25' ". ($selOpt == '25' ? " selected='selected' " : "") . ">25%</option>"
."<option value='26' ". ($selOpt == '26' ? " selected='selected' " : "") . ">26%</option>"
."<option value='27' ". ($selOpt == '27' ? " selected='selected' " : "") . ">27%</option>"
."<option value='28' ". ($selOpt == '28' ? " selected='selected' " : "") . ">28%</option>"
."<option value='29' ". ($selOpt == '29' ? " selected='selected' " : "") . ">29%</option>"
."<option value='30' ". ($selOpt == '30' ? " selected='selected' " : "") . ">30%</option>"
."<option value='31' ". ($selOpt == '31' ? " selected='selected' " : "") . ">31%</option>"
."<option value='32' ". ($selOpt == '32' ? " selected='selected' " : "") . ">32%</option>"
."<option value='33' ". ($selOpt == '33' ? " selected='selected' " : "") . ">33%</option>"
."<option value='34' ". ($selOpt == '34' ? " selected='selected' " : "") . ">34%</option>"
."<option value='35' ". ($selOpt == '35' ? " selected='selected' " : "") . ">35%</option>"
."<option value='36' ". ($selOpt == '36' ? " selected='selected' " : "") . ">36%</option>"
."<option value='37' ". ($selOpt == '37' ? " selected='selected' " : "") . ">37%</option>"
."<option value='38' ". ($selOpt == '38' ? " selected='selected' " : "") . ">38%</option>"
."<option value='39' ". ($selOpt == '39' ? " selected='selected' " : "") . ">39%</option>"
."<option value='40' ". ($selOpt == '40' ? " selected='selected' " : "") . ">40%</option>"
."<option value='41' ". ($selOpt == '41' ? " selected='selected' " : "") . ">41%</option>"
."<option value='42' ". ($selOpt == '42' ? " selected='selected' " : "") . ">42%</option>"
."<option value='43' ". ($selOpt == '43' ? " selected='selected' " : "") . ">43%</option>"
."<option value='44' ". ($selOpt == '44' ? " selected='selected' " : "") . ">44%</option>"
."<option value='45' ". ($selOpt == '45' ? " selected='selected' " : "") . ">45%</option>"
."<option value='46' ". ($selOpt == '46' ? " selected='selected' " : "") . ">46%</option>"
."<option value='47' ". ($selOpt == '47' ? " selected='selected' " : "") . ">47%</option>"
."<option value='48' ". ($selOpt == '48' ? " selected='selected' " : "") . ">48%</option>"
."<option value='49' ". ($selOpt == '49' ? " selected='selected' " : "") . ">49%</option>"
."<option value='50' ". ($selOpt == '50' ? " selected='selected' " : "") . ">50%</option>"
."<option value='51' ". ($selOpt == '51' ? " selected='selected' " : "") . ">51%</option>"
."<option value='52' ". ($selOpt == '52' ? " selected='selected' " : "") . ">52%</option>"
."<option value='53' ". ($selOpt == '53' ? " selected='selected' " : "") . ">53%</option>"
."<option value='54' ". ($selOpt == '54' ? " selected='selected' " : "") . ">54%</option>"
."<option value='55' ". ($selOpt == '55' ? " selected='selected' " : "") . ">55%</option>"
."<option value='56' ". ($selOpt == '56' ? " selected='selected' " : "") . ">56%</option>"
."<option value='57' ". ($selOpt == '57' ? " selected='selected' " : "") . ">57%</option>"
."<option value='58' ". ($selOpt == '58' ? " selected='selected' " : "") . ">58%</option>"
."<option value='59' ". ($selOpt == '59' ? " selected='selected' " : "") . ">59%</option>"
."<option value='60' ". ($selOpt == '60' ? " selected='selected' " : "") . ">60%</option>"
."<option value='61' ". ($selOpt == '61' ? " selected='selected' " : "") . ">61%</option>"
."<option value='62' ". ($selOpt == '62' ? " selected='selected' " : "") . ">62%</option>"
."<option value='63' ". ($selOpt == '63' ? " selected='selected' " : "") . ">63%</option>"
."<option value='64' ". ($selOpt == '64' ? " selected='selected' " : "") . ">64%</option>"
."<option value='65' ". ($selOpt == '65' ? " selected='selected' " : "") . ">65%</option>"
."<option value='66' ". ($selOpt == '66' ? " selected='selected' " : "") . ">66%</option>"
."<option value='67' ". ($selOpt == '67' ? " selected='selected' " : "") . ">67%</option>"
."<option value='68' ". ($selOpt == '68' ? " selected='selected' " : "") . ">68%</option>"
."<option value='69' ". ($selOpt == '69' ? " selected='selected' " : "") . ">69%</option>"
."<option value='70' ". ($selOpt == '70' ? " selected='selected' " : "") . ">70%</option>"
."<option value='71' ". ($selOpt == '71' ? " selected='selected' " : "") . ">71%</option>"
."<option value='72' ". ($selOpt == '72' ? " selected='selected' " : "") . ">72%</option>"
."<option value='73' ". ($selOpt == '73' ? " selected='selected' " : "") . ">73%</option>"
."<option value='74' ". ($selOpt == '74' ? " selected='selected' " : "") . ">74%</option>"
."<option value='75' ". ($selOpt == '75' ? " selected='selected' " : "") . ">75%</option>"
."<option value='76' ". ($selOpt == '76' ? " selected='selected' " : "") . ">76%</option>"
."<option value='77' ". ($selOpt == '77' ? " selected='selected' " : "") . ">77%</option>"
."<option value='78' ". ($selOpt == '78' ? " selected='selected' " : "") . ">78%</option>"
."<option value='79' ". ($selOpt == '79' ? " selected='selected' " : "") . ">79%</option>"
."<option value='80' ". ($selOpt == '80' ? " selected='selected' " : "") . ">80%</option>"
."<option value='81' ". ($selOpt == '81' ? " selected='selected' " : "") . ">81%</option>"
."<option value='82' ". ($selOpt == '82' ? " selected='selected' " : "") . ">82%</option>"
."<option value='83' ". ($selOpt == '83' ? " selected='selected' " : "") . ">83%</option>"
."<option value='84' ". ($selOpt == '84' ? " selected='selected' " : "") . ">84%</option>"
."<option value='85' ". ($selOpt == '85' ? " selected='selected' " : "") . ">85%</option>"
."<option value='86' ". ($selOpt == '86' ? " selected='selected' " : "") . ">86%</option>"
."<option value='87' ". ($selOpt == '87' ? " selected='selected' " : "") . ">87%</option>"
."<option value='88' ". ($selOpt == '88' ? " selected='selected' " : "") . ">88%</option>"
."<option value='89' ". ($selOpt == '89' ? " selected='selected' " : "") . ">89%</option>"
."<option value='90' ". ($selOpt == '90' ? " selected='selected' " : "") . ">90%</option>"
."<option value='91' ". ($selOpt == '91' ? " selected='selected' " : "") . ">91%</option>"
."<option value='92' ". ($selOpt == '92' ? " selected='selected' " : "") . ">92%</option>"
."<option value='93' ". ($selOpt == '93' ? " selected='selected' " : "") . ">93%</option>"
."<option value='94' ". ($selOpt == '94' ? " selected='selected' " : "") . ">94%</option>"
."<option value='95' ". ($selOpt == '95' ? " selected='selected' " : "") . ">95%</option>"
."<option value='96' ". ($selOpt == '96' ? " selected='selected' " : "") . ">96%</option>"
."<option value='97' ". ($selOpt == '97' ? " selected='selected' " : "") . ">97%</option>"
."<option value='98' ". ($selOpt == '98' ? " selected='selected' " : "") . ">98%</option>"
."<option value='99' ". ($selOpt == '99' ? " selected='selected' " : "") . ">99%</option>";
}

function getAlarmLevelSelectBox($selID,$selOpt = "0")
{
$retval = "<select id='".$selID."' class='entrySelect'>"
         .getAlarmLevelSelectOptions($selOpt)
 ."</select>";

 return $retval;
}

function getAlarmActionSelectBox($selID,$selOpt = "1")
{
$retval = "<select id='".$selID."' class='entrySelect'>"
."<option value='1' ". ($selOpt == '1' ? " selected='selected' " : "") .">None</option>"
."<option value='2' ". ($selOpt == '2' ? " selected='selected' " : "") .">Email Supplier</option>"
."<option value='3' ". ($selOpt == '3' ? " selected='selected' " : "") .">Email User</option>"
."<option value='4' ". ($selOpt == '4' ? " selected='selected' " : "") .">Email Both</option>"
 ."</select>";

 return $retval;
}

function getEmptyActionSelectBox($selID,$selOpt = "1")
{
$retval = "<select id='".$selID."' class='entrySelect'>"
."<option value='1' ". ($selOpt == '1' ? " selected='selected' " : "") .">None</option>"
."<option value='2' ". ($selOpt == '2' ? " selected='selected' " : "") .">Email Supplier</option>"
."<option value='3' ". ($selOpt == '3' ? " selected='selected' " : "") .">Email User</option>"
."<option value='4' ". ($selOpt == '4' ? " selected='selected' " : "") .">Email Both</option>"
 ."</select>";

 return $retval;
}

function getProductSelectBox($selID,$selOpt = "0")
{
$retval = "<select id='".$selID."' class='entrySelect'>"
."<option value='0'  ". ($selOpt ==  '0'  ? " selected='selected' " : "") .">...(not used)...</option>"
."<option value='100'". ($selOpt == '100' ? " selected='selected' " : "") .">Abby White </option>"
."<option value='101'". ($selOpt == '101' ? " selected='selected' " : "") .">Abita</option>"
."<option value='102'". ($selOpt == '102' ? " selected='selected' " : "") .">Abita Amber</option>"
."<option value='103'". ($selOpt == '103' ? " selected='selected' " : "") .">Abita Andygator</option>"
."<option value='104'". ($selOpt == '104' ? " selected='selected' " : "") .">Abita Christmas Ale</option>"
."<option value='105'". ($selOpt == '105' ? " selected='selected' " : "") .">Abita Jockamo IPA</option>"
."<option value='106'". ($selOpt == '106' ? " selected='selected' " : "") .">Abita Mardi Gras</option>"
."<option value='107'". ($selOpt == '107' ? " selected='selected' " : "") .">Abita Pecan Harvest</option>"
."<option value='108'". ($selOpt == '108' ? " selected='selected' " : "") .">Abita Purple Haze</option>"
."<option value='109'". ($selOpt == '109' ? " selected='selected' " : "") .">Abita Satsuma Harvest</option>"
."<option value='110'". ($selOpt == '110' ? " selected='selected' " : "") .">Abita Strawberry Harvest</option>"
."<option value='111'". ($selOpt == '111' ? " selected='selected' " : "") .">Abita Turbo Dog</option>"
."<option value='112'". ($selOpt == '112' ? " selected='selected' " : "") .">�bro</option>"
."<option value='113'". ($selOpt == '113' ? " selected='selected' " : "") .">Acme Brewing</option>"
."<option value='114'". ($selOpt == '114' ? " selected='selected' " : "") .">Adelscott</option>"
."<option value='115'". ($selOpt == '115' ? " selected='selected' " : "") .">Adelshoffen</option>"
."<option value='116'". ($selOpt == '116' ? " selected='selected' " : "") .">Adnams</option>"
."<option value='117'". ($selOpt == '117' ? " selected='selected' " : "") .">Albani</option>"
."<option value='118'". ($selOpt == '118' ? " selected='selected' " : "") .">Alexander Keith</option>"
."<option value='119'". ($selOpt == '119' ? " selected='selected' " : "") .">Allg�uer Brauhaus</option>"
."<option value='120'". ($selOpt == '120' ? " selected='selected' " : "") .">Almaza</option>"
."<option value='121'". ($selOpt == '121' ? " selected='selected' " : "") .">Alsfelder</option>"
."<option value='122'". ($selOpt == '122' ? " selected='selected' " : "") .">Amber</option>"
."<option value='123'". ($selOpt == '123' ? " selected='selected' " : "") .">Amstel Light</option>"
."<option value='124'". ($selOpt == '124' ? " selected='selected' " : "") .">Amstel Light</option>"
."<option value='125'". ($selOpt == '125' ? " selected='selected' " : "") .">Amstel S</option>"
."<option value='126'". ($selOpt == '126' ? " selected='selected' " : "") .">Anchor Steam </option>"
."<option value='127'". ($selOpt == '127' ? " selected='selected' " : "") .">Anderson Valley</option>"
."<option value='128'". ($selOpt == '128' ? " selected='selected' " : "") .">Anderson Valley Brewing</option>"
."<option value='129'". ($selOpt == '129' ? " selected='selected' " : "") .">Asahi</option>"
."<option value='130'". ($selOpt == '130' ? " selected='selected' " : "") .">Astra</option>"
."<option value='131'". ($selOpt == '131' ? " selected='selected' " : "") .">Augustiner Br�u</option>"
."<option value='132'". ($selOpt == '132' ? " selected='selected' " : "") .">Aventinus M German Slider</option>"
."<option value='133'". ($selOpt == '133' ? " selected='selected' " : "") .">Ayinger A German Slider</option>"
."<option value='134'". ($selOpt == '134' ? " selected='selected' " : "") .">Bacardi Silver</option>"
."<option value='135'". ($selOpt == '135' ? " selected='selected' " : "") .">Bacardi Silver Black </option>"
."<option value='136'". ($selOpt == '136' ? " selected='selected' " : "") .">Bacardi Silver Raz</option>"
."<option value='137'". ($selOpt == '137' ? " selected='selected' " : "") .">Bad Frog</option>"
."<option value='138'". ($selOpt == '138' ? " selected='selected' " : "") .">Bahia</option>"
."<option value='139'". ($selOpt == '139' ? " selected='selected' " : "") .">Ballantine</option>"
."<option value='140'". ($selOpt == '140' ? " selected='selected' " : "") .">Banks\'s</option>"
."<option value='141'". ($selOpt == '141' ? " selected='selected' " : "") .">Barbakan from Krakow</option>"
."<option value='142'". ($selOpt == '142' ? " selected='selected' " : "") .">Bard\'s Gold</option>"
."<option value='143'". ($selOpt == '143' ? " selected='selected' " : "") .">Bartles & Jaymes</option>"
."<option value='144'". ($selOpt == '144' ? " selected='selected' " : "") .">Bass</option>"
."<option value='145'". ($selOpt == '145' ? " selected='selected' " : "") .">Bass Ale</option>"
."<option value='146'". ($selOpt == '146' ? " selected='selected' " : "") .">Bass Pale Ale</option>"
."<option value='147'". ($selOpt == '147' ? " selected='selected' " : "") .">Bavaria Brewery</option>"
."<option value='148'". ($selOpt == '148' ? " selected='selected' " : "") .">Bayhawk</option>"
."<option value='149'". ($selOpt == '149' ? " selected='selected' " : "") .">Beck\'s Dark</option>"
."<option value='150'". ($selOpt == '150' ? " selected='selected' " : "") .">Beck\'s Premier Light</option>"
."<option value='151'". ($selOpt == '151' ? " selected='selected' " : "") .">Becker\'s</option>"
."<option value='152'". ($selOpt == '152' ? " selected='selected' " : "") .">Becks</option>"
."<option value='153'". ($selOpt == '153' ? " selected='selected' " : "") .">Becks Premier Light</option>"
."<option value='154'". ($selOpt == '154' ? " selected='selected' " : "") .">BeerLao</option>"
."<option value='155'". ($selOpt == '155' ? " selected='selected' " : "") .">Beerna Beer</option>"
."<option value='156'". ($selOpt == '156' ? " selected='selected' " : "") .">Beers/Gambrinus) </option>"
."<option value='157'". ($selOpt == '157' ? " selected='selected' " : "") .">Belhaven</option>"
."<option value='158'". ($selOpt == '158' ? " selected='selected' " : "") .">Belikin</option>"
."<option value='159'". ($selOpt == '159' ? " selected='selected' " : "") .">Belle-Vue</option>"
."<option value='160'". ($selOpt == '160' ? " selected='selected' " : "") .">Bemish</option>"
."<option value='161'". ($selOpt == '161' ? " selected='selected' " : "") .">Berliner Kind l</option>"
."<option value='162'". ($selOpt == '162' ? " selected='selected' " : "") .">Bernard</option>"
."<option value='163'". ($selOpt == '163' ? " selected='selected' " : "") .">Big Sky IPA</option>"
."<option value='164'". ($selOpt == '164' ? " selected='selected' " : "") .">Big Sky Moose Drool</option>"
."<option value='165'". ($selOpt == '165' ? " selected='selected' " : "") .">Big Sky Trout Slayer</option>"
."<option value='166'". ($selOpt == '166' ? " selected='selected' " : "") .">Bigfoot</option>"
."<option value='167'". ($selOpt == '167' ? " selected='selected' " : "") .">BigRock</option>"
."<option value='168'". ($selOpt == '168' ? " selected='selected' " : "") .">BillyBeer</option>"
."<option value='169'". ($selOpt == '169' ? " selected='selected' " : "") .">Binding</option>"
."<option value='170'". ($selOpt == '170' ? " selected='selected' " : "") .">Birra Tirana</option>"
."<option value='171'". ($selOpt == '171' ? " selected='selected' " : "") .">Bitburger Pilsner</option>"
."<option value='172'". ($selOpt == '172' ? " selected='selected' " : "") .">Black Dog</option>"
."<option value='173'". ($selOpt == '173' ? " selected='selected' " : "") .">Blackthorne Cider </option>"
."<option value='174'". ($selOpt == '174' ? " selected='selected' " : "") .">Blanche de Chambly</option>"
."<option value='175'". ($selOpt == '175' ? " selected='selected' " : "") .">Blatz</option>"
."<option value='176'". ($selOpt == '176' ? " selected='selected' " : "") .">Blue Moon Belgian White</option>"
."<option value='177'". ($selOpt == '177' ? " selected='selected' " : "") .">Blue Moon Full Moon</option>"
."<option value='178'". ($selOpt == '178' ? " selected='selected' " : "") .">Blue Moon Harvest Moon</option>"
."<option value='179'". ($selOpt == '179' ? " selected='selected' " : "") .">Blue Moon Honey Moon</option>"
."<option value='180'". ($selOpt == '180' ? " selected='selected' " : "") .">Blue Moon</option>"
."<option value='181'". ($selOpt == '181' ? " selected='selected' " : "") .">Blue Moon Rising Moon</option>"
."<option value='182'". ($selOpt == '182' ? " selected='selected' " : "") .">Boags</option>"
."<option value='183'". ($selOpt == '183' ? " selected='selected' " : "") .">Boddingtons Pub Ale</option>"
."<option value='184'". ($selOpt == '184' ? " selected='selected' " : "") .">Bohemia</option>"
."<option value='185'". ($selOpt == '185' ? " selected='selected' " : "") .">B�lkstoff</option>"
."<option value='186'". ($selOpt == '186' ? " selected='selected' " : "") .">Bosman</option>"
."<option value='187'". ($selOpt == '187' ? " selected='selected' " : "") .">Boulevard Bob\'s 47</option>"
."<option value='188'". ($selOpt == '188' ? " selected='selected' " : "") .">Boulevard Boss Tom\'s</option>"
."<option value='189'". ($selOpt == '189' ? " selected='selected' " : "") .">Boulevard Dark Truth</option>"
."<option value='190'". ($selOpt == '190' ? " selected='selected' " : "") .">Boulevard Double Wide</option>"
."<option value='191'". ($selOpt == '191' ? " selected='selected' " : "") .">Boulevard Irish Ale</option>"
."<option value='192'". ($selOpt == '192' ? " selected='selected' " : "") .">Boulevard Long Strange Tripel</option>"
."<option value='193'". ($selOpt == '193' ? " selected='selected' " : "") .">Boulevard Nutcracker Ale</option>"
."<option value='194'". ($selOpt == '194' ? " selected='selected' " : "") .">Boulevard Pale Ale</option>"
."<option value='195'". ($selOpt == '195' ? " selected='selected' " : "") .">Boulevard Single Wide IPA</option>"
."<option value='196'". ($selOpt == '196' ? " selected='selected' " : "") .">Boulevard Sixth Glass</option>"
."<option value='197'". ($selOpt == '197' ? " selected='selected' " : "") .">Boulevard Two Jokers</option>"
."<option value='198'". ($selOpt == '198' ? " selected='selected' " : "") .">Boulevard Unfiltered Wheat</option>"
."<option value='199'". ($selOpt == '199' ? " selected='selected' " : "") .">Boulevard Zon</option>"
."<option value='200'". ($selOpt == '200' ? " selected='selected' " : "") .">Brakina</option>"
."<option value='201'". ($selOpt == '201' ? " selected='selected' " : "") .">Braniewo Pils</option>"
."<option value='202'". ($selOpt == '202' ? " selected='selected' " : "") .">Branik</option>"
."<option value='203'". ($selOpt == '203' ? " selected='selected' " : "") .">Breckenridge</option>"
."<option value='204'". ($selOpt == '204' ? " selected='selected' " : "") .">Bridgeport</option>"
."<option value='205'". ($selOpt == '205' ? " selected='selected' " : "") .">BridgePort IPA</option>"
."<option value='206'". ($selOpt == '206' ? " selected='selected' " : "") .">Brinkhoffs</option>"
."<option value='207'". ($selOpt == '207' ? " selected='selected' " : "") .">Brok</option>"
."<option value='208'". ($selOpt == '208' ? " selected='selected' " : "") .">Brooklyn Brewery</option>"
."<option value='209'". ($selOpt == '209' ? " selected='selected' " : "") .">Brouwerij DeMolen</option>"
."<option value='210'". ($selOpt == '210' ? " selected='selected' " : "") .">Bruin Pale Ale</option>"
."<option value='211'". ($selOpt == '211' ? " selected='selected' " : "") .">Buckler</option>"
."<option value='212'". ($selOpt == '212' ? " selected='selected' " : "") .">Bud Ice</option>"
."<option value='213'". ($selOpt == '213' ? " selected='selected' " : "") .">Bud Light</option>"
."<option value='214'". ($selOpt == '214' ? " selected='selected' " : "") .">Budweiser</option>"
."<option value='215'". ($selOpt == '215' ? " selected='selected' " : "") .">Budweiser Select</option>"
."<option value='216'". ($selOpt == '216' ? " selected='selected' " : "") .">Bull Ice</option>"
."<option value='217'". ($selOpt == '217' ? " selected='selected' " : "") .">Busch</option>"
."<option value='218'". ($selOpt == '218' ? " selected='selected' " : "") .">Busch Light</option>"
."<option value='219'". ($selOpt == '219' ? " selected='selected' " : "") .">Cabro</option>"
."<option value='220'". ($selOpt == '220' ? " selected='selected' " : "") .">Caffrey\'s</option>"
."<option value='221'". ($selOpt == '221' ? " selected='selected' " : "") .">Campeon Light</option>"
."<option value='222'". ($selOpt == '222' ? " selected='selected' " : "") .">Cantillon Rose (Belgium) </option>"
."<option value='223'". ($selOpt == '223' ? " selected='selected' " : "") .">Capital Brewery</option>"
."<option value='224'". ($selOpt == '224' ? " selected='selected' " : "") .">Cardinal</option>"
."<option value='225'". ($selOpt == '225' ? " selected='selected' " : "") .">Carling</option>"
."<option value='226'". ($selOpt == '226' ? " selected='selected' " : "") .">Carling Black Label</option>"
."<option value='227'". ($selOpt == '227' ? " selected='selected' " : "") .">Carlsburg</option>"
."<option value='228'". ($selOpt == '228' ? " selected='selected' " : "") .">Carlton</option>"
."<option value='229'". ($selOpt == '229' ? " selected='selected' " : "") .">Carta Blanca</option>"
."<option value='230'". ($selOpt == '230' ? " selected='selected' " : "") .">Cascade</option>"
."<option value='231'". ($selOpt == '231' ? " selected='selected' " : "") .">Castle Lager</option>"
."<option value='232'". ($selOpt == '232' ? " selected='selected' " : "") .">Castle Maine</option>"
."<option value='233'". ($selOpt == '233' ? " selected='selected' " : "") .">Celis</option>"
."<option value='234'". ($selOpt == '234' ? " selected='selected' " : "") .">Celtia</option>"
."<option value='235'". ($selOpt == '235' ? " selected='selected' " : "") .">Cerveza Bock</option>"
."<option value='236'". ($selOpt == '236' ? " selected='selected' " : "") .">Cerveza Cardenal</option>"
."<option value='237'". ($selOpt == '237' ? " selected='selected' " : "") .">Cerveza Frya</option>"
."<option value='238'". ($selOpt == '238' ? " selected='selected' " : "") .">Cerveza India</option>"
."<option value='239'". ($selOpt == '239' ? " selected='selected' " : "") .">Cerveza Pasera</option>"
."<option value='240'". ($selOpt == '240' ? " selected='selected' " : "") .">Cerveza Santiago</option>"
."<option value='241'". ($selOpt == '241' ? " selected='selected' " : "") .">Cerveza Zulia</option>"
."<option value='242'". ($selOpt == '242' ? " selected='selected' " : "") .">CervezaNacional</option>"
."<option value='243'". ($selOpt == '243' ? " selected='selected' " : "") .">Chang</option>"
."<option value='244'". ($selOpt == '244' ? " selected='selected' " : "") .">Charger</option>"
."<option value='245'". ($selOpt == '245' ? " selected='selected' " : "") .">Chimay Trappist Ales</option>"
."<option value='246'". ($selOpt == '246' ? " selected='selected' " : "") .">Cider Jack</option>"
."<option value='247'". ($selOpt == '247' ? " selected='selected' " : "") .">Clausthaler</option>"
."<option value='248'". ($selOpt == '248' ? " selected='selected' " : "") .">Colt</option>"
."<option value='249'". ($selOpt == '249' ? " selected='selected' " : "") .">Colt 45</option>"
."<option value='250'". ($selOpt == '250' ? " selected='selected' " : "") .">Columbia Brewing</option>"
."<option value='251'". ($selOpt == '251' ? " selected='selected' " : "") .">Cooper\'s</option>"
."<option value='252'". ($selOpt == '252' ? " selected='selected' " : "") .">Coors</option>"
."<option value='253'". ($selOpt == '253' ? " selected='selected' " : "") .">Coors Extra Gold</option>"
."<option value='254'". ($selOpt == '254' ? " selected='selected' " : "") .">Coors Light</option>"
."<option value='255'". ($selOpt == '255' ? " selected='selected' " : "") .">Corona Extra</option>"
."<option value='256'". ($selOpt == '256' ? " selected='selected' " : "") .">Corona Light</option>"
."<option value='257'". ($selOpt == '257' ? " selected='selected' " : "") .">Courage</option>"
."<option value='258'". ($selOpt == '258' ? " selected='selected' " : "") .">Crispin Brut</option>"
."<option value='259'". ($selOpt == '259' ? " selected='selected' " : "") .">Crispin Honey Crisp</option>"
."<option value='260'". ($selOpt == '260' ? " selected='selected' " : "") .">Crispin Light</option>"
."<option value='261'". ($selOpt == '261' ? " selected='selected' " : "") .">Crispin Original</option>"
."<option value='262'". ($selOpt == '262' ? " selected='selected' " : "") .">Crispin The Saint</option>"
."<option value='263'". ($selOpt == '263' ? " selected='selected' " : "") .">Cristal</option>"
."<option value='264'". ($selOpt == '264' ? " selected='selected' " : "") .">Crown</option>"
."<option value='265'". ($selOpt == '265' ? " selected='selected' " : "") .">Cusquena</option>"
."<option value='266'". ($selOpt == '266' ? " selected='selected' " : "") .">CXX (Olvi)</option>"
."<option value='267'". ($selOpt == '267' ? " selected='selected' " : "") .">D B Export Gold</option>"
."<option value='268'". ($selOpt == '268' ? " selected='selected' " : "") .">DAB </option>"
."<option value='269'". ($selOpt == '269' ? " selected='selected' " : "") .">De Koninck Ale(Belgium) </option>"
."<option value='270'". ($selOpt == '270' ? " selected='selected' " : "") .">Delirium Tremens(Belgium) </option>"
."<option value='271'". ($selOpt == '271' ? " selected='selected' " : "") .">Deschutes Black Butte</option>"
."<option value='272'". ($selOpt == '272' ? " selected='selected' " : "") .">Desperados</option>"
."<option value='273'". ($selOpt == '273' ? " selected='selected' " : "") .">Devil Mountain</option>"
."<option value='274'". ($selOpt == '274' ? " selected='selected' " : "") .">Diapivo</option>"
."<option value='275'". ($selOpt == '275' ? " selected='selected' " : "") .">Diebels</option>"
."<option value='276'". ($selOpt == '276' ? " selected='selected' " : "") .">Dinkelacker</option>"
."<option value='277'". ($selOpt == '277' ? " selected='selected' " : "") .">Dixie</option>"
."<option value='278'". ($selOpt == '278' ? " selected='selected' " : "") .">Dojlidy</option>"
."<option value='279'". ($selOpt == '279' ? " selected='selected' " : "") .">Dom K�lsch</option>"
."<option value='280'". ($selOpt == '280' ? " selected='selected' " : "") .">Dommelsch</option>"
."<option value='281'". ($selOpt == '281' ? " selected='selected' " : "") .">Dortmunder Kronen</option>"
."<option value='282'". ($selOpt == '282' ? " selected='selected' " : "") .">Dortmunder Union </option>"
."<option value='283'". ($selOpt == '283' ? " selected='selected' " : "") .">Dos Equis Ambar</option>"
."<option value='284'". ($selOpt == '284' ? " selected='selected' " : "") .">Dos Equis</option>"
."<option value='285'". ($selOpt == '285' ? " selected='selected' " : "") .">Dos Equis Special Lager</option>"
."<option value='286'". ($selOpt == '286' ? " selected='selected' " : "") .">Double Diamond </option>"
."<option value='287'". ($selOpt == '287' ? " selected='selected' " : "") .">Dreber</option>"
."<option value='288'". ($selOpt == '288' ? " selected='selected' " : "") .">Duckstein</option>"
."<option value='289'". ($selOpt == '289' ? " selected='selected' " : "") .">Duessel Alt</option>"
."<option value='290'". ($selOpt == '290' ? " selected='selected' " : "") .">Dutch Gold</option>"
."<option value='291'". ($selOpt == '291' ? " selected='selected' " : "") .">E B beer</option>"
."<option value='292'". ($selOpt == '292' ? " selected='selected' " : "") .">Eel River Brewing</option>"
."<option value='293'". ($selOpt == '293' ? " selected='selected' " : "") .">Efes Pils</option>"
."<option value='294'". ($selOpt == '294' ? " selected='selected' " : "") .">Eggenberg</option>"
."<option value='295'". ($selOpt == '295' ? " selected='selected' " : "") .">Einbecker </option>"
."<option value='296'". ($selOpt == '296' ? " selected='selected' " : "") .">Eku</option>"
."<option value='297'". ($selOpt == '297' ? " selected='selected' " : "") .">Ellicottville</option>"
."<option value='298'". ($selOpt == '298' ? " selected='selected' " : "") .">Emu Bitter</option>"
."<option value='299'". ($selOpt == '299' ? " selected='selected' " : "") .">Erdinger</option>"
."<option value='300'". ($selOpt == '300' ? " selected='selected' " : "") .">Estrella</option>"
."<option value='301'". ($selOpt == '301' ? " selected='selected' " : "") .">Fagerhult (Banco)</option>"
."<option value='302'". ($selOpt == '302' ? " selected='selected' " : "") .">Falcon</option>"
."<option value='303'". ($selOpt == '303' ? " selected='selected' " : "") .">Farny</option>"
."<option value='304'". ($selOpt == '304' ? " selected='selected' " : "") .">Fat Tire Amber Ale</option>"
."<option value='305'". ($selOpt == '305' ? " selected='selected' " : "") .">Feldschlosschen</option>"
."<option value='306'". ($selOpt == '306' ? " selected='selected' " : "") .">Ferdinand</option>"
."<option value='307'". ($selOpt == '307' ? " selected='selected' " : "") .">Fischer</option>"
."<option value='308'". ($selOpt == '308' ? " selected='selected' " : "") .">Flensburger</option>"
."<option value='309'". ($selOpt == '309' ? " selected='selected' " : "") .">Flying Dog</option>"
."<option value='310'". ($selOpt == '310' ? " selected='selected' " : "") .">Foster\'s Lager</option>"
."<option value='311'". ($selOpt == '311' ? " selected='selected' " : "") .">Foster\'s Premium Ale</option>"
."<option value='312'". ($selOpt == '312' ? " selected='selected' " : "") .">Frankenheimer</option>"
."<option value='313'". ($selOpt == '313' ? " selected='selected' " : "") .">Franziskaner</option>"
."<option value='314'". ($selOpt == '314' ? " selected='selected' " : "") .">Full Sail</option>"
."<option value='315'". ($selOpt == '315' ? " selected='selected' " : "") .">Fuller\'s ESB</option>"
."<option value='316'". ($selOpt == '316' ? " selected='selected' " : "") .">Gaffel K�lsch</option>"
."<option value='317'". ($selOpt == '317' ? " selected='selected' " : "") .">Gallo</option>"
."<option value='318'". ($selOpt == '318' ? " selected='selected' " : "") .">Gambrinus</option>"
."<option value='319'". ($selOpt == '319' ? " selected='selected' " : "") .">Garrison</option>"
."<option value='320'". ($selOpt == '320' ? " selected='selected' " : "") .">Genesee Lager</option>"
."<option value='321'". ($selOpt == '321' ? " selected='selected' " : "") .">Genesee Light</option>"
."<option value='322'". ($selOpt == '322' ? " selected='selected' " : "") .">George Killians Irish Red</option>"
."<option value='323'". ($selOpt == '323' ? " selected='selected' " : "") .">German Pils</option>"
."<option value='324'". ($selOpt == '324' ? " selected='selected' " : "") .">Gie�ener</option>"
."<option value='325'". ($selOpt == '325' ? " selected='selected' " : "") .">Gilden-Koelsch</option>"
."<option value='326'". ($selOpt == '326' ? " selected='selected' " : "") .">Goose Island</option>"
."<option value='327'". ($selOpt == '327' ? " selected='selected' " : "") .">Gordon Biersch</option>"
."<option value='328'". ($selOpt == '328' ? " selected='selected' " : "") .">Gosser</option>"
."<option value='329'". ($selOpt == '329' ? " selected='selected' " : "") .">Grain Belt</option>"
."<option value='330'". ($selOpt == '330' ? " selected='selected' " : "") .">Grant\'s</option>"
."<option value='331'". ($selOpt == '331' ? " selected='selected' " : "") .">Green Mountain Cidery</option>"
."<option value='332'". ($selOpt == '332' ? " selected='selected' " : "") .">Greenall\'s</option>"
."<option value='333'". ($selOpt == '333' ? " selected='selected' " : "") .">Greene King</option>"
."<option value='334'". ($selOpt == '334' ? " selected='selected' " : "") .">Grolsch</option>"
."<option value='335'". ($selOpt == '335' ? " selected='selected' " : "") .">Guinness </option>"
."<option value='336'". ($selOpt == '336' ? " selected='selected' " : "") .">Guinness 250</option>"
."<option value='337'". ($selOpt == '337' ? " selected='selected' " : "") .">Guinness Extra Stout</option>"
."<option value='338'". ($selOpt == '338' ? " selected='selected' " : "") .">Guinness Stout</option>"
."<option value='339'". ($selOpt == '339' ? " selected='selected' " : "") .">Haake-Beck</option>"
."<option value='340'". ($selOpt == '340' ? " selected='selected' " : "") .">Hacker Phsorr</option>"
."<option value='341'". ($selOpt == '341' ? " selected='selected' " : "") .">Hacker-Pschorr</option>"
."<option value='342'". ($selOpt == '342' ? " selected='selected' " : "") .">Hahn </option>"
."<option value='343'". ($selOpt == '343' ? " selected='selected' " : "") .">Haizhu</option>"
."<option value='344'". ($selOpt == '344' ? " selected='selected' " : "") .">Halden Gut</option>"
."<option value='345'". ($selOpt == '345' ? " selected='selected' " : "") .">Hamms</option>"
."<option value='346'". ($selOpt == '346' ? " selected='selected' " : "") .">Hansa</option>"
."<option value='347'". ($selOpt == '347' ? " selected='selected' " : "") .">Hansa Pilsener</option>"
."<option value='348'". ($selOpt == '348' ? " selected='selected' " : "") .">Harbin</option>"
."<option value='349'". ($selOpt == '349' ? " selected='selected' " : "") .">H�rle</option>"
."<option value='350'". ($selOpt == '350' ? " selected='selected' " : "") .">Harp</option>"
."<option value='351'". ($selOpt == '351' ? " selected='selected' " : "") .">Hartwall</option>"
."<option value='352'". ($selOpt == '352' ? " selected='selected' " : "") .">Harveys</option>"
."<option value='353'". ($selOpt == '353' ? " selected='selected' " : "") .">HasenBr�u</option>"
."<option value='354'". ($selOpt == '354' ? " selected='selected' " : "") .">Haywards</option>"
."<option value='355'". ($selOpt == '355' ? " selected='selected' " : "") .">Heineken Dark</option>"
."<option value='356'". ($selOpt == '356' ? " selected='selected' " : "") .">Heineken</option>"
."<option value='357'". ($selOpt == '357' ? " selected='selected' " : "") .">Heineken Premium Light</option>"
."<option value='358'". ($selOpt == '358' ? " selected='selected' " : "") .">Hengelo Bier</option>"
."<option value='359'". ($selOpt == '359' ? " selected='selected' " : "") .">Henninger</option>"
."<option value='360'". ($selOpt == '360' ? " selected='selected' " : "") .">Henry Weinhard</option>"
."<option value='361'". ($selOpt == '361' ? " selected='selected' " : "") .">Hertog Jan</option>"
."<option value='362'". ($selOpt == '362' ? " selected='selected' " : "") .">High Falls</option>"
."<option value='363'". ($selOpt == '363' ? " selected='selected' " : "") .">Hinano</option>"
."<option value='364'". ($selOpt == '364' ? " selected='selected' " : "") .">Hite</option>"
."<option value='365'". ($selOpt == '365' ? " selected='selected' " : "") .">Hobarden</option>"
."<option value='366'". ($selOpt == '366' ? " selected='selected' " : "") .">Hoegaarden - Belgian White</option>"
."<option value='367'". ($selOpt == '367' ? " selected='selected' " : "") .">Holba</option>"
."<option value='368'". ($selOpt == '368' ? " selected='selected' " : "") .">Holsten</option>"
."<option value='369'". ($selOpt == '369' ? " selected='selected' " : "") .">Holy Cow Red</option>"
."<option value='370'". ($selOpt == '370' ? " selected='selected' " : "") .">Honey Brown Lager</option>"
."<option value='371'". ($selOpt == '371' ? " selected='selected' " : "") .">Hornsby\'s</option>"
."<option value='372'". ($selOpt == '372' ? " selected='selected' " : "") .">Hudepohl</option>"
."<option value='373'". ($selOpt == '373' ? " selected='selected' " : "") .">Hudson Valley</option>"
."<option value='374'". ($selOpt == '374' ? " selected='selected' " : "") .">Hue Beer</option>"
."<option value='375'". ($selOpt == '375' ? " selected='selected' " : "") .">Huiquan</option>"
."<option value='376'". ($selOpt == '376' ? " selected='selected' " : "") .">Humboldt Brewing</option>"
."<option value='377'". ($selOpt == '377' ? " selected='selected' " : "") .">Hurlimann</option>"
."<option value='378'". ($selOpt == '378' ? " selected='selected' " : "") .">Hurricane Malt Liquor</option>"
."<option value='379'". ($selOpt == '379' ? " selected='selected' " : "") .">Icehouse</option>"
."<option value='380'". ($selOpt == '380' ? " selected='selected' " : "") .">Indio</option>"
."<option value='381'". ($selOpt == '381' ? " selected='selected' " : "") .">Iron City</option>"
."<option value='382'". ($selOpt == '382' ? " selected='selected' " : "") .">Iron City Light</option>"
."<option value='383'". ($selOpt == '383' ? " selected='selected' " : "") .">Isenbeck</option>"
."<option value='384'". ($selOpt == '384' ? " selected='selected' " : "") .">Ithaca</option>"
."<option value='385'". ($selOpt == '385' ? " selected='selected' " : "") .">James Squire</option>"
."<option value='386'". ($selOpt == '386' ? " selected='selected' " : "") .">Jenlain</option>"
."<option value='387'". ($selOpt == '387' ? " selected='selected' " : "") .">Jever</option>"
."<option value='388'". ($selOpt == '388' ? " selected='selected' " : "") .">Jianyang</option>"
."<option value='389'". ($selOpt == '389' ? " selected='selected' " : "") .">Jilin</option>"
."<option value='390'". ($selOpt == '390' ? " selected='selected' " : "") .">John Courage </option>"
."<option value='391'". ($selOpt == '391' ? " selected='selected' " : "") .">John Smith\'s</option>"
."<option value='392'". ($selOpt == '392' ? " selected='selected' " : "") .">Jopen</option>"
."<option value='393'". ($selOpt == '393' ? " selected='selected' " : "") .">Julius Echter</option>"
."<option value='394'". ($selOpt == '394' ? " selected='selected' " : "") .">Jupiler</option>"
."<option value='395'". ($selOpt == '395' ? " selected='selected' " : "") .">JW Dundees Honey Brown Lager</option>"
."<option value='396'". ($selOpt == '396' ? " selected='selected' " : "") .">Kaliber</option>"
."<option value='397'". ($selOpt == '397' ? " selected='selected' " : "") .">Karhu</option>"
."<option value='398'". ($selOpt == '398' ? " selected='selected' " : "") .">Karjala</option>"
."<option value='399'". ($selOpt == '399' ? " selected='selected' " : "") .">Karlsberg</option>"
."<option value='400'". ($selOpt == '400' ? " selected='selected' " : "") .">Karlsquell</option>"
."<option value='401'". ($selOpt == '401' ? " selected='selected' " : "") .">Keith\'s</option>"
."<option value='402'". ($selOpt == '402' ? " selected='selected' " : "") .">Keystone Ice</option>"
."<option value='403'". ($selOpt == '403' ? " selected='selected' " : "") .">Keystone Light</option>"
."<option value='404'". ($selOpt == '404' ? " selected='selected' " : "") .">Keystone</option>"
."<option value='405'". ($selOpt == '405' ? " selected='selected' " : "") .">Kilkenny </option>"
."<option value='406'". ($selOpt == '406' ? " selected='selected' " : "") .">Killarney</option>"
."<option value='407'". ($selOpt == '407' ? " selected='selected' " : "") .">Killian\'s Red</option>"
."<option value='408'". ($selOpt == '408' ? " selected='selected' " : "") .">King Cobra</option>"
."<option value='409'". ($selOpt == '409' ? " selected='selected' " : "") .">Kingfisher</option>"
."<option value='410'". ($selOpt == '410' ? " selected='selected' " : "") .">Kirin</option>"
."<option value='411'". ($selOpt == '411' ? " selected='selected' " : "") .">Kirin Ichiban</option>"
."<option value='412'". ($selOpt == '412' ? " selected='selected' " : "") .">Koff</option>"
."<option value='413'". ($selOpt == '413' ? " selected='selected' " : "") .">Kokanee</option>"
."<option value='414'". ($selOpt == '414' ? " selected='selected' " : "") .">K�lsch</option>"
."<option value='415'". ($selOpt == '415' ? " selected='selected' " : "") .">Kommodori</option>"
."<option value='416'". ($selOpt == '416' ? " selected='selected' " : "") .">Konrad</option>"
."<option value='417'". ($selOpt == '417' ? " selected='selected' " : "") .">K�strizer</option>"
."<option value='418'". ($selOpt == '418' ? " selected='selected' " : "") .">Kriska</option>"
."<option value='419'". ($selOpt == '419' ? " selected='selected' " : "") .">Kr�lewskie</option>"
."<option value='420'". ($selOpt == '420' ? " selected='selected' " : "") .">Krombacher</option>"
."<option value='421'". ($selOpt == '421' ? " selected='selected' " : "") .">Kronenbourg</option>"
."<option value='422'". ($selOpt == '422' ? " selected='selected' " : "") .">Krusovice</option>"
."<option value='423'". ($selOpt == '423' ? " selected='selected' " : "") .">Labatt Blue</option>"
."<option value='424'". ($selOpt == '424' ? " selected='selected' " : "") .">Labatt Blue Light</option>"
."<option value='425'". ($selOpt == '425' ? " selected='selected' " : "") .">Laivuri</option>"
."<option value='426'". ($selOpt == '426' ? " selected='selected' " : "") .">Lakeport</option>"
."<option value='427'". ($selOpt == '427' ? " selected='selected' " : "") .">Landshark</option>"
."<option value='428'". ($selOpt == '428' ? " selected='selected' " : "") .">Lapin Kulta</option>"
."<option value='429'". ($selOpt == '429' ? " selected='selected' " : "") .">Laziza</option>"
."<option value='430'". ($selOpt == '430' ? " selected='selected' " : "") .">Lech</option>"
."<option value='431'". ($selOpt == '431' ? " selected='selected' " : "") .">Leffe </option>"
."<option value='432'". ($selOpt == '432' ? " selected='selected' " : "") .">Leffe Blonde</option>"
."<option value='433'". ($selOpt == '433' ? " selected='selected' " : "") .">Legenda</option>"
."<option value='434'". ($selOpt == '434' ? " selected='selected' " : "") .">Leinenkugel Original Premium </option>"
."<option value='435'". ($selOpt == '435' ? " selected='selected' " : "") .">Leinenkugel\'s 1888 Bock</option>"
."<option value='436'". ($selOpt == '436' ? " selected='selected' " : "") .">Leinenkugel\'s Berry Weiss</option>"
."<option value='437'". ($selOpt == '437' ? " selected='selected' " : "") .">Leinenkugel\'s Classic Amber</option>"
."<option value='438'". ($selOpt == '438' ? " selected='selected' " : "") .">Leinenkugel\'s Fireside Nut Brown</option>"
."<option value='439'". ($selOpt == '439' ? " selected='selected' " : "") .">Leinenkugel\'s Honey Weiss</option>"
."<option value='440'". ($selOpt == '440' ? " selected='selected' " : "") .">Leinenkugel\'s Oktoberfest</option>"
."<option value='441'". ($selOpt == '441' ? " selected='selected' " : "") .">Leinenkugel\'s Summer Shandy</option>"
."<option value='442'". ($selOpt == '442' ? " selected='selected' " : "") .">Leinenkugel\'s Sunset Wheat</option>"
."<option value='443'". ($selOpt == '443' ? " selected='selected' " : "") .">Leon</option>"
."<option value='444'". ($selOpt == '444' ? " selected='selected' " : "") .">Licher</option>"
."<option value='445'". ($selOpt == '445' ? " selected='selected' " : "") .">Lindeman\'s </option>"
."<option value='446'". ($selOpt == '446' ? " selected='selected' " : "") .">Lion Lager</option>"
."<option value='447'". ($selOpt == '447' ? " selected='selected' " : "") .">Lion Nathan </option>"
."<option value='448'". ($selOpt == '448' ? " selected='selected' " : "") .">Litovel</option>"
."<option value='449'". ($selOpt == '449' ? " selected='selected' " : "") .">Little Kings</option>"
."<option value='450'". ($selOpt == '450' ? " selected='selected' " : "") .">Little Kings Cream Ale</option>"
."<option value='451'". ($selOpt == '451' ? " selected='selected' " : "") .">Live Oak</option>"
."<option value='452'". ($selOpt == '452' ? " selected='selected' " : "") .">Lobkowicz</option>"
."<option value='453'". ($selOpt == '453' ? " selected='selected' " : "") .">Lone Star Beer</option>"
."<option value='454'". ($selOpt == '454' ? " selected='selected' " : "") .">Lone Star Light</option>"
."<option value='455'". ($selOpt == '455' ? " selected='selected' " : "") .">Lost Coast Brewery</option>"
."<option value='456'". ($selOpt == '456' ? " selected='selected' " : "") .">Lowenbrau (Import)</option>"
."<option value='457'". ($selOpt == '457' ? " selected='selected' " : "") .">Lowenbrau (US)</option>"
."<option value='458'". ($selOpt == '458' ? " selected='selected' " : "") .">Mac Tarnahan\'s</option>"
."<option value='459'". ($selOpt == '459' ? " selected='selected' " : "") .">Mad River Brewing</option>"
."<option value='460'". ($selOpt == '460' ? " selected='selected' " : "") .">Magners U U Listing UEC</option>"
."<option value='461'". ($selOpt == '461' ? " selected='selected' " : "") .">Magnum</option>"
."<option value='462'". ($selOpt == '462' ? " selected='selected' " : "") .">Maisel</option>"
."<option value='463'". ($selOpt == '463' ? " selected='selected' " : "") .">Maredsous Abbey Ale</option>"
."<option value='464'". ($selOpt == '464' ? " selected='selected' " : "") .">Marston\'s Pedigree </option>"
."<option value='465'". ($selOpt == '465' ? " selected='selected' " : "") .">Maudite</option>"
."<option value='466'". ($selOpt == '466' ? " selected='selected' " : "") .">McEwan\'s </option>"
."<option value='467'". ($selOpt == '467' ? " selected='selected' " : "") .">McSorley\'s</option>"
."<option value='468'". ($selOpt == '468' ? " selected='selected' " : "") .">Meckatzer</option>"
."<option value='469'". ($selOpt == '469' ? " selected='selected' " : "") .">Medalla</option>"
."<option value='470'". ($selOpt == '470' ? " selected='selected' " : "") .">Melbourne Bitter</option>"
."<option value='471'". ($selOpt == '471' ? " selected='selected' " : "") .">Memminger</option>"
."<option value='472'". ($selOpt == '472' ? " selected='selected' " : "") .">Miami Trail Brewing</option>"
."<option value='473'". ($selOpt == '473' ? " selected='selected' " : "") .">Michael Shea\'s</option>"
."<option value='474'". ($selOpt == '474' ? " selected='selected' " : "") .">Michelob Amber Bock</option>"
."<option value='475'". ($selOpt == '475' ? " selected='selected' " : "") .">Michelob</option>"
."<option value='476'". ($selOpt == '476' ? " selected='selected' " : "") .">Michelob Golden Draft Light</option>"
."<option value='477'". ($selOpt == '477' ? " selected='selected' " : "") .">Michelob Light</option>"
."<option value='478'". ($selOpt == '478' ? " selected='selected' " : "") .">Michelob Speciality</option>"
."<option value='479'". ($selOpt == '479' ? " selected='selected' " : "") .">Michelob Ultra</option>"
."<option value='480'". ($selOpt == '480' ? " selected='selected' " : "") .">Michelob Ultra</option>"
."<option value='481'". ($selOpt == '481' ? " selected='selected' " : "") .">Mickeys</option>"
."<option value='482'". ($selOpt == '482' ? " selected='selected' " : "") .">Middle Ages</option>"
."<option value='483'". ($selOpt == '483' ? " selected='selected' " : "") .">Mike\'s Hard Cranberry Lemonade</option>"
."<option value='484'". ($selOpt == '484' ? " selected='selected' " : "") .">Mike\'s Hard Lemonade</option>"
."<option value='485'". ($selOpt == '485' ? " selected='selected' " : "") .">Mike\'s Hard Lime</option>"
."<option value='486'". ($selOpt == '486' ? " selected='selected' " : "") .">Mike\'s Hard Mango Punch</option>"
."<option value='487'". ($selOpt == '487' ? " selected='selected' " : "") .">Mike\'s Hard Pink Lemonade</option>"
."<option value='488'". ($selOpt == '488' ? " selected='selected' " : "") .">Mike\'s Hard Pomegranate Lemonade</option>"
."<option value='489'". ($selOpt == '489' ? " selected='selected' " : "") .">Mike\'s Harder Cranberry Lemonade</option>"
."<option value='490'". ($selOpt == '490' ? " selected='selected' " : "") .">Mike\'s Harder Lemonade</option>"
."<option value='491'". ($selOpt == '491' ? " selected='selected' " : "") .">Mike\'s Light Hard Cranberry Lemonade</option>"
."<option value='492'". ($selOpt == '492' ? " selected='selected' " : "") .">Mike\'s Light Hard Lemonade</option>"
."<option value='493'". ($selOpt == '493' ? " selected='selected' " : "") .">Miller Chill</option>"
."<option value='494'". ($selOpt == '494' ? " selected='selected' " : "") .">Miller Genuine Draft 64</option>"
."<option value='495'". ($selOpt == '495' ? " selected='selected' " : "") .">Miller Genuine Draft Light</option>"
."<option value='496'". ($selOpt == '496' ? " selected='selected' " : "") .">Miller Genuine Draft</option>"
."<option value='497'". ($selOpt == '497' ? " selected='selected' " : "") .">Miller High Life</option>"
."<option value='498'". ($selOpt == '498' ? " selected='selected' " : "") .">Miller High Life Light</option>"
."<option value='499'". ($selOpt == '499' ? " selected='selected' " : "") .">Miller Lite</option>"
."<option value='500'". ($selOpt == '500' ? " selected='selected' " : "") .">Milwaukees Best</option>"
."<option value='501'". ($selOpt == '501' ? " selected='selected' " : "") .">Milwaukees Best Ice</option>"
."<option value='502'". ($selOpt == '502' ? " selected='selected' " : "") .">Milwaukees Best Light</option>"
."<option value='503'". ($selOpt == '503' ? " selected='selected' " : "") .">Modelo Especial</option>"
."<option value='504'". ($selOpt == '504' ? " selected='selected' " : "") .">Molson Canadian</option>"
."<option value='505'". ($selOpt == '505' ? " selected='selected' " : "") .">Molson Golden</option>"
."<option value='506'". ($selOpt == '506' ? " selected='selected' " : "") .">Molson Ice</option>"
."<option value='507'". ($selOpt == '507' ? " selected='selected' " : "") .">Moninger</option>"
."<option value='508'". ($selOpt == '508' ? " selected='selected' " : "") .">Montejo</option>"
."<option value='509'". ($selOpt == '509' ? " selected='selected' " : "") .">Moosehead</option>"
."<option value='510'". ($selOpt == '510' ? " selected='selected' " : "") .">Moosehead Moosehead USA (Gambrinus) </option>"
."<option value='511'". ($selOpt == '511' ? " selected='selected' " : "") .">Moretti </option>"
."<option value='512'". ($selOpt == '512' ? " selected='selected' " : "") .">Murphy\'s Irish Stout </option>"
."<option value='513'". ($selOpt == '513' ? " selected='selected' " : "") .">Murphy\'s Red </option>"
."<option value='514'". ($selOpt == '514' ? " selected='selected' " : "") .">Murree Beer</option>"
."<option value='515'". ($selOpt == '515' ? " selected='selected' " : "") .">M�tzig</option>"
."<option value='516'". ($selOpt == '516' ? " selected='selected' " : "") .">Natural Ice</option>"
."<option value='517'". ($selOpt == '517' ? " selected='selected' " : "") .">Natural Light</option>"
."<option value='518'". ($selOpt == '518' ? " selected='selected' " : "") .">Negra Modelo</option>"
."<option value='519'". ($selOpt == '519' ? " selected='selected' " : "") .">New Amsterdam</option>"
."<option value='520'". ($selOpt == '520' ? " selected='selected' " : "") .">New Belgium 1554</option>"
."<option value='521'". ($selOpt == '521' ? " selected='selected' " : "") .">New Belgium 2 Below</option>"
."<option value='522'". ($selOpt == '522' ? " selected='selected' " : "") .">New Belgium Abbey</option>"
."<option value='523'". ($selOpt == '523' ? " selected='selected' " : "") .">New Belgium Blue Paddle</option>"
."<option value='524'". ($selOpt == '524' ? " selected='selected' " : "") .">New Belgium Fat Tire</option>"
."<option value='525'". ($selOpt == '525' ? " selected='selected' " : "") .">New Belgium Hoptober</option>"
."<option value='526'". ($selOpt == '526' ? " selected='selected' " : "") .">New Belgium Mighty Arrow</option>"
."<option value='527'". ($selOpt == '527' ? " selected='selected' " : "") .">New Belgium Mothership Wit</option>"
."<option value='528'". ($selOpt == '528' ? " selected='selected' " : "") .">New Belgium Ranger IPA</option>"
."<option value='529'". ($selOpt == '529' ? " selected='selected' " : "") .">New Belgium Skinny Dip</option>"
."<option value='530'". ($selOpt == '530' ? " selected='selected' " : "") .">New Belgium Sunshine</option>"
."<option value='531'". ($selOpt == '531' ? " selected='selected' " : "") .">New Belgium Trippel</option>"
."<option value='532'". ($selOpt == '532' ? " selected='selected' " : "") .">New Zeland Steinlager</option>"
."<option value='533'". ($selOpt == '533' ? " selected='selected' " : "") .">Newcastle Brown Ale</option>"
."<option value='534'". ($selOpt == '534' ? " selected='selected' " : "") .">Nikolai</option>"
."<option value='535'". ($selOpt == '535' ? " selected='selected' " : "") .">Noche Buena</option>"
."<option value='536'". ($selOpt == '536' ? " selected='selected' " : "") .">Norrlands Guld</option>"
."<option value='537'". ($selOpt == '537' ? " selected='selected' " : "") .">North Coast Brewing</option>"
."<option value='538'". ($selOpt == '538' ? " selected='selected' " : "") .">Northern Breweries</option>"
."<option value='539'". ($selOpt == '539' ? " selected='selected' " : "") .">Nor\'Wester</option>"
."<option value='540'". ($selOpt == '540' ? " selected='selected' " : "") .">OB</option>"
."<option value='541'". ($selOpt == '541' ? " selected='selected' " : "") .">O\'Doul\'s</option>"
."<option value='542'". ($selOpt == '542' ? " selected='selected' " : "") .">Oettinger</option>"
."<option value='543'". ($selOpt == '543' ? " selected='selected' " : "") .">Okanagan Spring</option>"
."<option value='544'". ($selOpt == '544' ? " selected='selected' " : "") .">Okocim</option>"
."<option value='545'". ($selOpt == '545' ? " selected='selected' " : "") .">Old Milwaukee</option>"
."<option value='546'". ($selOpt == '546' ? " selected='selected' " : "") .">Old Milwaukee Light</option>"
."<option value='547'". ($selOpt == '547' ? " selected='selected' " : "") .">Old Speckeled Hen</option>"
."<option value='548'". ($selOpt == '548' ? " selected='selected' " : "") .">Old Style Light</option>"
."<option value='549'". ($selOpt == '549' ? " selected='selected' " : "") .">Old Style</option>"
."<option value='550'". ($selOpt == '550' ? " selected='selected' " : "") .">Old Vienna</option>"
."<option value='551'". ($selOpt == '551' ? " selected='selected' " : "") .">Olde English</option>"
."<option value='552'". ($selOpt == '552' ? " selected='selected' " : "") .">Olde English 800</option>"
."<option value='553'". ($selOpt == '553' ? " selected='selected' " : "") .">Olvi</option>"
."<option value='554'". ($selOpt == '554' ? " selected='selected' " : "") .">Oranjeboom</option>"
."<option value='555'". ($selOpt == '555' ? " selected='selected' " : "") .">Orion</option>"
."<option value='556'". ($selOpt == '556' ? " selected='selected' " : "") .">Pabst Blue Ribbon</option>"
."<option value='557'". ($selOpt == '557' ? " selected='selected' " : "") .">Pacifico</option>"
."<option value='558'". ($selOpt == '558' ? " selected='selected' " : "") .">Pacifico Clara</option>"
."<option value='559'". ($selOpt == '559' ? " selected='selected' " : "") .">Pacifico Crown Imports</option>"
."<option value='560'". ($selOpt == '560' ? " selected='selected' " : "") .">Papst - Check D-HS 8010 / 8030</option>"
."<option value='561'". ($selOpt == '561' ? " selected='selected' " : "") .">Parrot Bay Wave Runner</option>"
."<option value='562'". ($selOpt == '562' ? " selected='selected' " : "") .">Paulaner</option>"
."<option value='563'". ($selOpt == '563' ? " selected='selected' " : "") .">Paulaner Hefe-Weizen</option>"
."<option value='564'". ($selOpt == '564' ? " selected='selected' " : "") .">Pearl Light</option>"
."<option value='565'". ($selOpt == '565' ? " selected='selected' " : "") .">Pelforth</option>"
."<option value='566'". ($selOpt == '566' ? " selected='selected' " : "") .">Pernstejn</option>"
."<option value='567'". ($selOpt == '567' ? " selected='selected' " : "") .">Peroni Nastro Azzurro</option>"
."<option value='568'". ($selOpt == '568' ? " selected='selected' " : "") .">Peroniand Nastro Azzruro Peroni</option>"
."<option value='569'". ($selOpt == '569' ? " selected='selected' " : "") .">Pete\'s Wicked Ale</option>"
."<option value='570'". ($selOpt == '570' ? " selected='selected' " : "") .">Pete\'s Wicked Strawberry Blonde</option>"
."<option value='571'". ($selOpt == '571' ? " selected='selected' " : "") .">Petermannchen</option>"
."<option value='572'". ($selOpt == '572' ? " selected='selected' " : "") .">Pete\'s Wicked Ale</option>"
."<option value='573'". ($selOpt == '573' ? " selected='selected' " : "") .">Piast</option>"
."<option value='574'". ($selOpt == '574' ? " selected='selected' " : "") .">Pilsener of El Salvador Lager Bier</option>"
."<option value='575'". ($selOpt == '575' ? " selected='selected' " : "") .">Pilsner Urquell - Bohemian Pilsner</option>"
."<option value='576'". ($selOpt == '576' ? " selected='selected' " : "") .">PilsnerUrquell</option>"
."<option value='577'". ($selOpt == '577' ? " selected='selected' " : "") .">Piraat</option>"
."<option value='578'". ($selOpt == '578' ? " selected='selected' " : "") .">Piwo Bractwa Kurkowego</option>"
."<option value='579'". ($selOpt == '579' ? " selected='selected' " : "") .">Platan</option>"
."<option value='580'". ($selOpt == '580' ? " selected='selected' " : "") .">Polar</option>"
."<option value='581'". ($selOpt == '581' ? " selected='selected' " : "") .">Porter & Summerfest</option>"
."<option value='582'". ($selOpt == '582' ? " selected='selected' " : "") .">Portland Brewing Co</option>"
."<option value='583'". ($selOpt == '583' ? " selected='selected' " : "") .">Power\'s</option>"
."<option value='584'". ($selOpt == '584' ? " selected='selected' " : "") .">Poznanski Porter</option>"
."<option value='585'". ($selOpt == '585' ? " selected='selected' " : "") .">Presidente</option>"
."<option value='586'". ($selOpt == '586' ? " selected='selected' " : "") .">Primator</option>"
."<option value='587'". ($selOpt == '587' ? " selected='selected' " : "") .">Primus</option>"
."<option value='588'". ($selOpt == '588' ? " selected='selected' " : "") .">Pripps Bl�</option>"
."<option value='589'". ($selOpt == '589' ? " selected='selected' " : "") .">Propeller</option>"
."<option value='590'". ($selOpt == '590' ? " selected='selected' " : "") .">Pyramid Hefeweizen</option>"
."<option value='591'". ($selOpt == '591' ? " selected='selected' " : "") .">Radeberger</option>"
."<option value='592'". ($selOpt == '592' ? " selected='selected' " : "") .">Radegast</option>"
."<option value='593'". ($selOpt == '593' ? " selected='selected' " : "") .">Rahr Blind Salamander</option>"
."<option value='594'". ($selOpt == '594' ? " selected='selected' " : "") .">Rahr Blonde Lager</option>"
."<option value='595'". ($selOpt == '595' ? " selected='selected' " : "") .">Rahr Bucking Bock</option>"
."<option value='596'". ($selOpt == '596' ? " selected='selected' " : "") .">Rahr Buffalo Butt</option>"
."<option value='597'". ($selOpt == '597' ? " selected='selected' " : "") .">Rahr Iron Thistle</option>"
."<option value='598'". ($selOpt == '598' ? " selected='selected' " : "") .">Rahr Oktoberfest</option>"
."<option value='599'". ($selOpt == '599' ? " selected='selected' " : "") .">Rahr Stormcloud IPA</option>"
."<option value='600'". ($selOpt == '600' ? " selected='selected' " : "") .">Rahr Summertime Wheat</option>"
."<option value='601'". ($selOpt == '601' ? " selected='selected' " : "") .">Rahr Ugly Pug</option>"
."<option value='602'". ($selOpt == '602' ? " selected='selected' " : "") .">Rahr\'s Winter Warmer</option>"
."<option value='603'". ($selOpt == '603' ? " selected='selected' " : "") .">Rainier</option>"
."<option value='604'". ($selOpt == '604' ? " selected='selected' " : "") .">Ratsherrn</option>"
."<option value='605'". ($selOpt == '605' ? " selected='selected' " : "") .">Razors Edge</option>"
."<option value='606'". ($selOpt == '606' ? " selected='selected' " : "") .">Red Ale</option>"
."<option value='607'". ($selOpt == '607' ? " selected='selected' " : "") .">Red Dog</option>"
."<option value='608'". ($selOpt == '608' ? " selected='selected' " : "") .">Red Hook</option>"
."<option value='609'". ($selOpt == '609' ? " selected='selected' " : "") .">Red Horse</option>"
."<option value='610'". ($selOpt == '610' ? " selected='selected' " : "") .">Red Stripe</option>"
."<option value='611'". ($selOpt == '611' ? " selected='selected' " : "") .">Red Wolf</option>"
."<option value='612'". ($selOpt == '612' ? " selected='selected' " : "") .">Redback</option>"
."<option value='613'". ($selOpt == '613' ? " selected='selected' " : "") .">Redhook ESB</option>"
."<option value='614'". ($selOpt == '614' ? " selected='selected' " : "") .">Regent</option>"
."<option value='615'". ($selOpt == '615' ? " selected='selected' " : "") .">Reschs</option>"
."<option value='616'". ($selOpt == '616' ? " selected='selected' " : "") .">Rheingold</option>"
."<option value='617'". ($selOpt == '617' ? " selected='selected' " : "") .">Rialto</option>"
."<option value='618'". ($selOpt == '618' ? " selected='selected' " : "") .">Riegele</option>"
."<option value='619'". ($selOpt == '619' ? " selected='selected' " : "") .">Ringwood Brewery</option>"
."<option value='620'". ($selOpt == '620' ? " selected='selected' " : "") .">Rock Green Light</option>"
."<option value='621'". ($selOpt == '621' ? " selected='selected' " : "") .">Rock Light</option>"
."<option value='622'". ($selOpt == '622' ? " selected='selected' " : "") .">Rogue Brewing</option>"
."<option value='623'". ($selOpt == '623' ? " selected='selected' " : "") .">Roling</option>"
."<option value='624'". ($selOpt == '624' ? " selected='selected' " : "") .">Rolling Rock</option>"
."<option value='625'". ($selOpt == '625' ? " selected='selected' " : "") .">Rothaus</option>"
."<option value='626'". ($selOpt == '626' ? " selected='selected' " : "") .">Rouge-Mogal</option>"
."<option value='627'". ($selOpt == '627' ? " selected='selected' " : "") .">Rychtar</option>"
."<option value='628'". ($selOpt == '628' ? " selected='selected' " : "") .">Sagres</option>"
."<option value='629'". ($selOpt == '629' ? " selected='selected' " : "") .">Saigon Export</option>"
."<option value='630'". ($selOpt == '630' ? " selected='selected' " : "") .">Saigon Lager</option>"
."<option value='631'". ($selOpt == '631' ? " selected='selected' " : "") .">Saigon333 Export</option>"
."<option value='632'". ($selOpt == '632' ? " selected='selected' " : "") .">Sam Adams Light</option>"
."<option value='633'". ($selOpt == '633' ? " selected='selected' " : "") .">Samichlaus (SantaClaus)</option>"
."<option value='634'". ($selOpt == '634' ? " selected='selected' " : "") .">Samson</option>"
."<option value='635'". ($selOpt == '635' ? " selected='selected' " : "") .">Samuel Adams Blackberry Wit</option>"
."<option value='636'". ($selOpt == '636' ? " selected='selected' " : "") .">Samuel Adams Boston Ale</option>"
."<option value='637'". ($selOpt == '637' ? " selected='selected' " : "") .">Samuel Adams Boston Lager</option>"
."<option value='638'". ($selOpt == '638' ? " selected='selected' " : "") .">Samuel Adams Cherry Wheat</option>"
."<option value='639'". ($selOpt == '639' ? " selected='selected' " : "") .">Samuel Adams Double Bock</option>"
."<option value='640'". ($selOpt == '640' ? " selected='selected' " : "") .">Samuel Adams Imperial Stout</option>"
."<option value='641'". ($selOpt == '641' ? " selected='selected' " : "") .">Samuel Adams Imperial White</option>"
."<option value='642'". ($selOpt == '642' ? " selected='selected' " : "") .">Samuel Adams Noble Pils</option>"
."<option value='643'". ($selOpt == '643' ? " selected='selected' " : "") .">Samuel Adams Octoberfest</option>"
."<option value='644'". ($selOpt == '644' ? " selected='selected' " : "") .">Samuel Adams Summer Ale</option>"
."<option value='645'". ($selOpt == '645' ? " selected='selected' " : "") .">Samuel Adams Winter Brew</option>"
."<option value='646'". ($selOpt == '646' ? " selected='selected' " : "") .">Samuel Smith\'s Taddy Porter</option>"
."<option value='647'". ($selOpt == '647' ? " selected='selected' " : "") .">San Miguel</option>"
."<option value='648'". ($selOpt == '648' ? " selected='selected' " : "") .">Sandels (Olvi)</option>"
."<option value='649'". ($selOpt == '649' ? " selected='selected' " : "") .">Sanwald Weizenbier</option>"
."<option value='650'". ($selOpt == '650' ? " selected='selected' " : "") .">Sapporo</option>"
."<option value='651'". ($selOpt == '651' ? " selected='selected' " : "") .">Saranac Amber/Golden/Pale Ale</option>"
."<option value='652'". ($selOpt == '652' ? " selected='selected' " : "") .">Saxer Brewing</option>"
."<option value='653'". ($selOpt == '653' ? " selected='selected' " : "") .">Schaefer Light Pabst Brewing </option>"
."<option value='654'". ($selOpt == '654' ? " selected='selected' " : "") .">Schaefer Pabst Brewing </option>"
."<option value='655'". ($selOpt == '655' ? " selected='selected' " : "") .">Schaeffer HS� Hoff Stevens</option>"
."<option value='656'". ($selOpt == '656' ? " selected='selected' " : "") .">Schlappeseppel</option>"
."<option value='657'". ($selOpt == '657' ? " selected='selected' " : "") .">Schlitz</option>"
."<option value='658'". ($selOpt == '658' ? " selected='selected' " : "") .">Schlitz Malt Liquor</option>"
."<option value='659'". ($selOpt == '659' ? " selected='selected' " : "") .">Schmitt\'s</option>"
."<option value='660'". ($selOpt == '660' ? " selected='selected' " : "") .">Schneider </option>"
."<option value='661'". ($selOpt == '661' ? " selected='selected' " : "") .">Sch�fferhofer</option>"
."<option value='662'". ($selOpt == '662' ? " selected='selected' " : "") .">Schutz Jubilator</option>"
."<option value='663'". ($selOpt == '663' ? " selected='selected' " : "") .">Schwaben Brau</option>"
."<option value='664'". ($selOpt == '664' ? " selected='selected' " : "") .">Scottish & Newcastle </option>"
."<option value='665'". ($selOpt == '665' ? " selected='selected' " : "") .">Scottish Tennents </option>"
."<option value='666'". ($selOpt == '666' ? " selected='selected' " : "") .">Seagrams Cooler</option>"
."<option value='667'". ($selOpt == '667' ? " selected='selected' " : "") .">Semeuse</option>"
."<option value='668'". ($selOpt == '668' ? " selected='selected' " : "") .">Sharp\'s NA</option>"
."<option value='669'". ($selOpt == '669' ? " selected='selected' " : "") .">Shepherd Neame</option>"
."<option value='670'". ($selOpt == '670' ? " selected='selected' " : "") .">Shiner Blonde</option>"
."<option value='671'". ($selOpt == '671' ? " selected='selected' " : "") .">Shiner Bock</option>"
."<option value='672'". ($selOpt == '672' ? " selected='selected' " : "") .">Shiner Bohemian Black Lager</option>"
."<option value='673'". ($selOpt == '673' ? " selected='selected' " : "") .">Shiner Frost</option>"
."<option value='674'". ($selOpt == '674' ? " selected='selected' " : "") .">Shiner Hefeweizen</option>"
."<option value='675'". ($selOpt == '675' ? " selected='selected' " : "") .">Shiner Holiday Cheer</option>"
."<option value='676'". ($selOpt == '676' ? " selected='selected' " : "") .">Shiner Light</option>"
."<option value='677'". ($selOpt == '677' ? " selected='selected' " : "") .">Shiner Smokehaus</option>"
."<option value='678'". ($selOpt == '678' ? " selected='selected' " : "") .">Shipyard</option>"
."<option value='679'". ($selOpt == '679' ? " selected='selected' " : "") .">Sierra Nevada Pale Ale</option>"
."<option value='680'". ($selOpt == '680' ? " selected='selected' " : "") .">Singha</option>"
."<option value='681'". ($selOpt == '681' ? " selected='selected' " : "") .">Sir Perry William\'s</option>"
."<option value='682'". ($selOpt == '682' ? " selected='selected' " : "") .">Sleemans</option>"
."<option value='683'". ($selOpt == '683' ? " selected='selected' " : "") .">Sleeman\'s</option>"
."<option value='684'". ($selOpt == '684' ? " selected='selected' " : "") .">Smirnoff Ice</option>"
."<option value='685'". ($selOpt == '685' ? " selected='selected' " : "") .">Smirnoff Ice Green Apple Bite</option>"
."<option value='686'". ($selOpt == '686' ? " selected='selected' " : "") .">Smirnoff Ice Mango</option>"
."<option value='687'". ($selOpt == '687' ? " selected='selected' " : "") .">Smirnoff Ice Passion Fruit</option>"
."<option value='688'". ($selOpt == '688' ? " selected='selected' " : "") .">Smirnoff Ice Pomegranate Fusion</option>"
."<option value='689'". ($selOpt == '689' ? " selected='selected' " : "") .">Smirnoff Ice Raspberry Burst</option>"
."<option value='690'". ($selOpt == '690' ? " selected='selected' " : "") .">Smirnoff Ice Strawberry Acai</option>"
."<option value='691'". ($selOpt == '691' ? " selected='selected' " : "") .">Smirnoff Ice Triple Black</option>"
."<option value='692'". ($selOpt == '692' ? " selected='selected' " : "") .">Smirnoff Ice Watermelon</option>"
."<option value='693'". ($selOpt == '693' ? " selected='selected' " : "") .">Smirnoff Ice Wild Grape</option>"
."<option value='694'". ($selOpt == '694' ? " selected='selected' " : "") .">Smirnoff Twisted V</option>"
."<option value='695'". ($selOpt == '695' ? " selected='selected' " : "") .">Smithwicks</option>"
."<option value='696'". ($selOpt == '696' ? " selected='selected' " : "") .">So.b.bra</option>"
."<option value='697'". ($selOpt == '697' ? " selected='selected' " : "") .">Sol</option>"
."<option value='698'". ($selOpt == '698' ? " selected='selected' " : "") .">Sol Cerveza</option>"
."<option value='699'". ($selOpt == '699' ? " selected='selected' " : "") .">SolHeineken USA </option>"
."<option value='700'". ($selOpt == '700' ? " selected='selected' " : "") .">Southpaw</option>"
."<option value='701'". ($selOpt == '701' ? " selected='selected' " : "") .">Spanish Peaks Black Dog</option>"
."<option value='702'". ($selOpt == '702' ? " selected='selected' " : "") .">Sparks   Miller Brewing </option>"
."<option value='703'". ($selOpt == '703' ? " selected='selected' " : "") .">Spaten A German Slider</option>"
."<option value='704'". ($selOpt == '704' ? " selected='selected' " : "") .">Speight\'s</option>"
."<option value='705'". ($selOpt == '705' ? " selected='selected' " : "") .">Splugen</option>"
."<option value='706'". ($selOpt == '706' ? " selected='selected' " : "") .">St Austell Breweries</option>"
."<option value='707'". ($selOpt == '707' ? " selected='selected' " : "") .">St. Galler Klosterbr�u</option>"
."<option value='708'". ($selOpt == '708' ? " selected='selected' " : "") .">St. Ides Malt Liquor</option>"
."<option value='709'". ($selOpt == '709' ? " selected='selected' " : "") .">St. Pauli Girl Crown Imports</option>"
."<option value='710'". ($selOpt == '710' ? " selected='selected' " : "") .">St. Pauli Girl Special Dark</option>"
."<option value='711'". ($selOpt == '711' ? " selected='selected' " : "") .">Starobrno</option>"
."<option value='712'". ($selOpt == '712' ? " selected='selected' " : "") .">Staropramen</option>"
."<option value='713'". ($selOpt == '713' ? " selected='selected' " : "") .">Starr Hill Brewery</option>"
."<option value='714'". ($selOpt == '714' ? " selected='selected' " : "") .">States Beverage</option>"
."<option value='715'". ($selOpt == '715' ? " selected='selected' " : "") .">Steel Reserve</option>"
."<option value='716'". ($selOpt == '716' ? " selected='selected' " : "") .">Steinlager</option>"
."<option value='717'". ($selOpt == '717' ? " selected='selected' " : "") .">Stella Artois</option>"
."<option value='718'". ($selOpt == '718' ? " selected='selected' " : "") .">Stichting Noordhollandse</option>"
."<option value='719'". ($selOpt == '719' ? " selected='selected' " : "") .">Stiegl</option>"
."<option value='720'". ($selOpt == '720' ? " selected='selected' " : "") .">Stone</option>"
."<option value='721'". ($selOpt == '721' ? " selected='selected' " : "") .">Strohs  Pabst Brewing </option>"
."<option value='722'". ($selOpt == '722' ? " selected='selected' " : "") .">Strongbow Cider </option>"
."<option value='723'". ($selOpt == '723' ? " selected='selected' " : "") .">Superior</option>"
."<option value='724'". ($selOpt == '724' ? " selected='selected' " : "") .">Suttons</option>"
."<option value='725'". ($selOpt == '725' ? " selected='selected' " : "") .">Svijany</option>"
."<option value='726'". ($selOpt == '726' ? " selected='selected' " : "") .">Swan Lager</option>"
."<option value='727'". ($selOpt == '727' ? " selected='selected' " : "") .">Taranaki</option>"
."<option value='728'". ($selOpt == '728' ? " selected='selected' " : "") .">Tecate</option>"
."<option value='729'". ($selOpt == '729' ? " selected='selected' " : "") .">Tecate Light</option>"
."<option value='730'". ($selOpt == '730' ? " selected='selected' " : "") .">Tennants</option>"
."<option value='731'". ($selOpt == '731' ? " selected='selected' " : "") .">Tetley</option>"
."<option value='732'". ($selOpt == '732' ? " selected='selected' " : "") .">Tetley\'s </option>"
."<option value='733'". ($selOpt == '733' ? " selected='selected' " : "") .">Theakstons</option>"
."<option value='734'". ($selOpt == '734' ? " selected='selected' " : "") .">Thomas Kemper</option>"
."<option value='735'". ($selOpt == '735' ? " selected='selected' " : "") .">Thunderbolt</option>"
."<option value='736'". ($selOpt == '736' ? " selected='selected' " : "") .">Thunderhead IPA</option>"
."<option value='737'". ($selOpt == '737' ? " selected='selected' " : "") .">Tiger</option>"
."<option value='738'". ($selOpt == '738' ? " selected='selected' " : "") .">Tocayo</option>"
."<option value='739'". ($selOpt == '739' ? " selected='selected' " : "") .">Tooheys</option>"
."<option value='740'". ($selOpt == '740' ? " selected='selected' " : "") .">Trappistbeer</option>"
."<option value='741'". ($selOpt == '741' ? " selected='selected' " : "") .">Trois Pistoles</option>"
."<option value='742'". ($selOpt == '742' ? " selected='selected' " : "") .">Tsingtao Pure Draft</option>"
."<option value='743'". ($selOpt == '743' ? " selected='selected' " : "") .">Tuborg</option>"
."<option value='744'". ($selOpt == '744' ? " selected='selected' " : "") .">Tucher</option>"
."<option value='745'". ($selOpt == '745' ? " selected='selected' " : "") .">Tyskie</option>"
."<option value='746'". ($selOpt == '746' ? " selected='selected' " : "") .">Unibroue</option>"
."<option value='747'". ($selOpt == '747' ? " selected='selected' " : "") .">UpperCanada</option>"
."<option value='748'". ($selOpt == '748' ? " selected='selected' " : "") .">Urho (Hartwall)</option>"
."<option value='749'". ($selOpt == '749' ? " selected='selected' " : "") .">Ursus</option>"
."<option value='750'". ($selOpt == '750' ? " selected='selected' " : "") .">Valstar</option>"
."<option value='751'". ($selOpt == '751' ? " selected='selected' " : "") .">Van Steenberge</option>"
."<option value='752'". ($selOpt == '752' ? " selected='selected' " : "") .">Velkopopovicky Kozel</option>"
."<option value='753'". ($selOpt == '753' ? " selected='selected' " : "") .">Veltins M German Slider</option>"
."<option value='754'". ($selOpt == '754' ? " selected='selected' " : "") .">Victoria</option>"
."<option value='755'". ($selOpt == '755' ? " selected='selected' " : "") .">Victoria Bitter</option>"
."<option value='756'". ($selOpt == '756' ? " selected='selected' " : "") .">Victoria Bitter A German Slider</option>"
."<option value='757'". ($selOpt == '757' ? " selected='selected' " : "") .">Vratislav</option>"
."<option value='758'". ($selOpt == '758' ? " selected='selected' " : "") .">Warsteiner</option>"
."<option value='759'". ($selOpt == '759' ? " selected='selected' " : "") .">Warteck</option>"
."<option value='760'". ($selOpt == '760' ? " selected='selected' " : "") .">Wasatch</option>"
."<option value='761'". ($selOpt == '761' ? " selected='selected' " : "") .">Watney\'s </option>"
."<option value='762'". ($selOpt == '762' ? " selected='selected' " : "") .">Weidmar</option>"
."<option value='763'". ($selOpt == '763' ? " selected='selected' " : "") .">Weihenstephaner</option>"
."<option value='764'". ($selOpt == '764' ? " selected='selected' " : "") .">Weinhard\'s</option>"
."<option value='765'". ($selOpt == '765' ? " selected='selected' " : "") .">Weltenburger Kloster</option>"
."<option value='766'". ($selOpt == '766' ? " selected='selected' " : "") .">Whitbread Ale</option>"
."<option value='767'". ($selOpt == '767' ? " selected='selected' " : "") .">Widmer Hefeweizen</option>"
."<option value='768'". ($selOpt == '768' ? " selected='selected' " : "") .">Windhoek Lager</option>"
."<option value='769'". ($selOpt == '769' ? " selected='selected' " : "") .">Wood Chuck</option>"
."<option value='770'". ($selOpt == '770' ? " selected='selected' " : "") .">Woodpecker</option>"
."<option value='771'". ($selOpt == '771' ? " selected='selected' " : "") .">Wyder\'s</option>"
."<option value='772'". ($selOpt == '772' ? " selected='selected' " : "") .">Xinjiang</option>"
."<option value='773'". ($selOpt == '773' ? " selected='selected' " : "") .">X</option>"
."<option value='774'". ($selOpt == '774' ? " selected='selected' " : "") .">Yanjing L</option>"
."<option value='775'". ($selOpt == '775' ? " selected='selected' " : "") .">Yinpu</option>"
."<option value='776'". ($selOpt == '776' ? " selected='selected' " : "") .">Youngs</option>"
."<option value='777'". ($selOpt == '777' ? " selected='selected' " : "") .">Young\'s</option>"
."<option value='778'". ($selOpt == '778' ? " selected='selected' " : "") .">Young\'s Oatmeal Stout</option>"
."<option value='779'". ($selOpt == '779' ? " selected='selected' " : "") .">Young\'s Ram Rod Bitter </option>"
."<option value='780'". ($selOpt == '780' ? " selected='selected' " : "") .">Yuengling</option>"
."<option value='781'". ($selOpt == '781' ? " selected='selected' " : "") .">Yuengling Traditional Lager</option>"
."<option value='782'". ($selOpt == '782' ? " selected='selected' " : "") .">Zebra</option>"
."<option value='783'". ($selOpt == '783' ? " selected='selected' " : "") .">Zima</option>"
."<option value='784'". ($selOpt == '784' ? " selected='selected' " : "") .">Zipfer</option>"
."<option value='785'". ($selOpt == '785' ? " selected='selected' " : "") .">Zlatopramen</option>"
."<option value='786'". ($selOpt == '786' ? " selected='selected' " : "") .">Zubr</option>"
."<option value='787'". ($selOpt == '787' ? " selected='selected' " : "") .">Zywiecbeer</option>"
."</select>";
return $retval;
}

function getDBProductSelectOptions($connect, $mfgNum = "-1", $selOpt = "0")
{
  $errmsg = "OK";
  $retval = ""
          ."<option value='0' ". ($selOpt == "0" ? " selected='selected' " : ""). ">...(none)...</option>";
  $sql = "SELECT prodNum, prodName "
    ." FROM Products ";
  if ($mfgNum != "-1" && $mfgNum != "")
    $sql .= " WHERE mfgNum = $mfgNum ";
  $sql .= " ORDER BY prodName ";
  $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getDistMfgList","SELPICKLIST");
  if ($errmsg != "OK")
    $retval .= "<option value='-1|x' selected='selected'>System Error $errmsg</option>";
  else
  {
    while ($lcol = @mysql_fetch_assoc($result))
    {
      $prodNum  = $lcol['prodNum'];
      $prodName = formatquotefordisplay($lcol['prodName']);
      $retval .= "<option value='".$prodNum."' ". ($selOpt == $prodNum ? " selected='selected' " : "") .">".$prodName."</option>";
    }
  }
  return $retval;
}

function getDBProductSelectBox($connect,$selID,$mfgNum = "-1", $selOpt = "0")
{
  $errmsg = "OK";
  $retval = "<select id='".$selID."' class='entrySelect'>";
  $retval .= getDBProductSelectOptions($selOpt);
  $retval .= "</select>";
  return $retval;
}

function getDBItemJsonOptions($connect, $category = "0", $selOpt = "0")
{
  $errmsg = "OK";
  $retarray = Array();
  $isDefault = $selOpt == '0' ? true : false;
  array_push($retarray, array("selval"  => "0",
                              "seltext" => "...(select)...",
                              "selected"=> $isDefault));

  $sql = "SELECT itemNum, itemID, itemName "
    ." FROM Items "
    ." WHERE itemCategory = '$category' "
    ." ORDER BY itemName ";
  $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getDBItemJsonOptions","SELITEMLIST");

  if ($errmsg != "OK")
  {
    $retarray[0]["selected"] = false;
    array_push($retarray, array("selval"  => "-1",
                                "seltext" => "system error",
                                "selected"=> true));
  }
  else
  {
    while ($lcol = @mysql_fetch_assoc($result))
    {
      $itemNum   = $lcol['itemNum'];
      $itemID    = $lcol['itemID'];
      $isDefault = $itemNum == $selOpt ? true : false;
      $itemName = formatquotefordisplay($lcol['itemName']);
      array_push($retarray, array("selval"  => $itemNum,
                                  "seltext" => $itemName." (".$itemID.")",
                                  "selected"=> $isDefault));
    }
  }
  $retval = json_encode($retarray);
  return $retval;
}

function getDBItemSelectOptions($connect, $category = "0", $selOpt = "0")
{
  $errmsg = "OK";
  $retval = ""
          ."<option value='0' ". ($selOpt == "0" ? " selected='selected' " : ""). ">...(select)...</option>";
  $sql = "SELECT itemNum, itemID, itemName "
    ." FROM Items "
    ." WHERE itemCategory = '$category' "
    ." ORDER BY itemName ";
  $result = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getDBItemSelectOptions","SELITEMLIST");

  if ($errmsg != "OK")
    $retval .= "<option value='-1' selected='selected'>System Error $errmsg</option>";
  else
  {
    while ($lcol = @mysql_fetch_assoc($result))
    {
      $itemNum  = $lcol['itemNum'];
      $itemID   = $lcol['itemID'];
      $itemName = formatquotefordisplay($lcol['itemName']);
      $retval .= "<option value='".$itemNum."' ". ($selOpt == $itemNum ? " selected='selected' " : "") .">".$itemName." (".$itemID.")</option>";
    }
  }
  return $retval;
}

function getDBItemSelectBox($connect,$selID, $category = "0", $selOpt = "0")
{
  $errmsg = "OK";
  $retval = "<select id='".$selID."' class='entrySelect'>";
  $retval .= getDBItemSelectOptions($connect,$category,$selOpt);
  $retval .= "</select>";
  return $retval;
}

function getNextCounterNumber($connect,$counterID)
{ // this function needs to be called from within the transaction
  // creating the thing that the counter is used for
  // counter must already be initialized elsewhere (need admin screen)
  $errmsg = "OK";
  $lastUsedNum = "-1";

  if ($counterID != "")
  {
    // increment the counter
    $sql = "UPDATE IKegCounters SET "
                 ."lastUsedNum = lastUsedNum + 1 "
                 ."WHERE counterID = '$counterID'";
    @mysql_query($sql, $connect) or $errmsg = "ERR|Update Error.|".$sql.GetDatabaseError($connect,"getNextCounterNumber","UPDCOUNT");
    // read the new value
    if ($errmsg == "OK")
    {
      $sql = "SELECT lastUsedNum FROM IKegCounters  "
                   ."WHERE counterID = '$counterID'";
      $ctrResult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".$sql.GetDatabaseError($connect,"getNextCounterNumber","SELCOUNT");
    }
    if ($errmsg == "OK")
    {
      if (! $lcol = @mysql_fetch_assoc($ctrResult))
        $errmsg = "ERR|Internal Error.|-1|Unable to locate updated counter";
      else
        $lastUsedNum = $lcol['lastUsedNum'];
    }
  }
  $retval = array("status"=>$errmsg,"nextNum"=>$lastUsedNum);

  return $retval;
}

function getOrderInfo($connect,$orderNum = "-1")
{
  $errmsg = "OK";
  $retval = Array('status'=>'OK','orderNum'=>'-1','orderID'=>'-1','orderStartDtTm'=>'');
  $orderID  = "-1";
  $orderStartDtTm = "";
  $sql = "SELECT orderID,orderStartDtTm FROM Orders WHERE orderNum = $orderNum";
  $ordresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"createNewOrder","SELORDER");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_assoc($ordresult))
    {
      $orderID = $lcol['orderID'];
      $orderStartDtTm = $lcol['orderStartDtTm'];
    }
  }
  $retval['status'] = $errmsg;
  $retval['orderNum'] = $orderNum;
  $retval['orderID']  = $orderID;
  $retval['orderStartDtTm']  = $orderStartDtTm;
  return $retval;
}

function createNewOrder($connect,$userNum = "-1")
{
  $errmsg = "OK";
  $retval = Array('status'=>'OK','orderNum'=>'-1','orderID'=>'-1','orderStartDtTm'=>'');
  $orderNum = "-1";
  $orderID  = "-1";
  $orderStartDtTm = "";
  if ($userNum != "-1")
  {
    $errmsg = beginTrans($connect);
    if ($errmsg == "OK")
    {
      $nextCounter = getNextCounterNumber($connect,"ORD");
      $errmsg = $nextCounter['status'];
    }
    if ($errmsg == "OK")
    {
      $orderID = $nextCounter['nextNum'];
      $orderID = substr("0000000".$orderID,-7);
      $sql = "INSERT INTO Orders "
            ."(orderID,orderUserNum,orderStatus,orderStartDtTm)"
            ." VALUES "
            ."('$orderID',$userNum,".ORDER_NEW.",Now())";
      @mysql_query($sql, $connect) or $errmsg = "ERR|Insert Error.|".GetDatabaseError($connect,"createNewOrder","INSORDER");
    }
    if ($errmsg == "OK")
    {
      $sql = "SELECT orderNum,orderStartDtTm FROM Orders WHERE orderID = '$orderID'";
      $ordresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"createNewOrder","SELORDER");
      if ($errmsg == "OK")
      {
        if (! $lcol = @mysql_fetch_assoc($ordresult))
          $errmsg = "ERR|Internal Error.|-1|Unable to locate created order";
        else
        {
          $orderNum = $lcol['orderNum'];
          $orderStartDtTm = $lcol['orderStartDtTm'];
        }
      }
    }
    if ($errmsg == "OK")
    {
      commitTrans($connect);
      $retval['orderNum'] = $orderNum;
      $retval['orderID']  = $orderID;
      $retval['orderStartDtTm']  = $orderStartDtTm;
    }
    else
    {
      rollbackTrans($connect);
      $retval['status'] = $errmsg;
    }
  }
  return $retval;
}

function getUserOpenOrder($connect,$userNum)
{
  $errmsg = "OK";
  $retval = array('status'=>'OK','orderNum'=>'-1');
  $sql = "SELECT orderNum FROM Orders WHERE orderUserNum = $userNum AND orderStatus = ".ORDER_NEW."";
  $ordresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"getUserOpenOrder","SELORDER");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_assoc($ordresult))
      $retval['orderNum'] = $lcol['orderNum'];
  }
  $retval['status'] = $errmsg;
  return $retval;
}

function countOrderKits($connect,$orderNum,$kitNum="-1")
{
  $errmsg = "OK";
  $retval = array('status'=>'OK','qty'=>'0');
  $sql = "SELECT COUNT(*) FROM OrderKits "
        ." WHERE orderNum = $orderNum ";
  if ($kitNum != "-1")
    $sql .= " AND kitNum = $kitNum";
  $qtyresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"countOrderKits","SELCOUNT");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_array($qtyresult))
      $retval['qty'] = $lcol[0];
  }
  $retval['status'] = $errmsg;
  return $retval;
}

function getOrderKitQty($connect,$orderNum,$kitNum="-1")
{
  $errmsg = "OK";
  $retval = array('status'=>'OK','qty'=>'0');
  $sql = "SELECT SUM(orderQty) FROM OrderKits "
        ." WHERE orderNum = $orderNum ";
  if ($kitNum != "-1")
    $sql .= " AND kitNum = $kitNum";
  $qtyresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"countOrderKits","SELCOUNT");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_array($qtyresult))
      $retval['qty'] = $lcol[0];
  }
  $retval['status'] = $errmsg;
  return $retval;
}

function countOrderItems($connect,$orderNum,$itemNum="-1",$kitNum="-1")
{
  $errmsg = "OK";
  $retval = array('status'=>'OK','qty'=>'0');
  $sql = "SELECT COUNT(*) FROM OrderItems "
        ." WHERE orderNum = $orderNum ";
  if ($itemNum != "-1")
    $sql .= " AND itemNum = $itemNum";
  if ($kitNum != "-1")
    $sql .= " AND kitNum = $kitNum";
  $qtyresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"countOrderItems","SELCOUNT");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_array($qtyresult))
      $retval['qty'] = $lcol[0];
  }
  $retval['status'] = $errmsg;
  return $retval;
}

function getOrderItemQty($connect,$orderNum,$itemNum="-1",$kitNum="-1")
{
  $errmsg = "OK";
  $retval = array('status'=>'OK','qty'=>'0');
  $sql = "SELECT orderQty FROM OrderItems "
        ." WHERE orderNum = $orderNum ";
  if ($itemNum != "-1")
    $sql .= " AND itemNum = $itemNum";
 // if ($kitNum != "-1")
    $sql .= " AND kitNum = $kitNum";
  $qtyresult = @mysql_query($sql, $connect) or $errmsg = "ERR|Select Error.|".GetDatabaseError($connect,"countOrderItems","SELCOUNT");
  if ($errmsg == "OK")
  {
    if ($lcol = @mysql_fetch_array($qtyresult))
      $retval['qty'] = $lcol[0];
  }
  $retval['status'] = $errmsg;
  return $retval;
}


function getContentTitle($titleString,$marginLeft=0)
{
  $styleString = ($marginLeft == 0 ? '' : ' style="margin-left:'.$marginLeft.'px;"');

  return '<h1 class="divTitleText">'.$titleString.'</h1>';
}






?>
