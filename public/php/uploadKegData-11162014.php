<?

  include("./include/iKegGlobal.php");
  include("./include/dbconnect.php");
  include("./include/accounts.php");
  include("./include/acctdevices.php");
  include("./logKegData.php");

  $errmsg = "OK";

  if (! isset($_REQUEST['a']))
  {
    echo "ERR|no data";
    echo "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    echo "<br/>The string must end with |K to ensure complete reception";
    echo "<br/>or with |KV to echo back data received";
    exit;
  }
  $dataIn = $_REQUEST['a'];
  if ($dataIn == "GETTIME")
  {
    echo date("Y-m-d H:i:s");
    exit;
  }
  $dataParts = explode("|",$dataIn);
  if (Count($dataParts) != 10)
  {
    echo "ERR|incomplete data";
    echo "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    echo "<br/>The string must end with |K to ensure complete reception";
    echo "<br/>or with |KV to echo back data received";
    exit;
  }
  if (strtoupper($dataParts[9]) != "K" && strtoupper($dataParts[9]) != "KV")
  {
    echo "ERR|invalid data";
    echo "<br/>Format is ?a=HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K";
    echo "<br/>The string must end with |K to ensure complete reception";
    echo "<br/>or with |KV to echo back data received";
    exit;
  }
  //HUBMAC|YYYYMMDDHHMMSS|KEGMAC|START|DURING|STOP|ELTIME|TEMP|STATUS|K
  $hubMAC      = formatMACforsql($dataParts[0]);
  $sentDtTm    = $dataParts[1];
  $kegMAC      = formatMACforsql($dataParts[2]);
  $startVal    = hexdec($dataParts[3]);
  $pourVal     = hexdec($dataParts[4]);
  $endVal      = hexdec($dataParts[5]);
  $tempCelcius = hexdec($dataParts[6]); // tenths of a second,i.e. 0020 is 2 seconds
  $elTenths    = hexdec($dataParts[7]);
  $kegStatus   = decbin(hexdec($dataParts[8]));
  $endOfData   = strtoupper($dataParts[9]);
  $sentYear    = substr($sentDtTm, 0,4);
  $sentMonth   = substr($sentDtTm, 4,2);
  $sentDay     = substr($sentDtTm, 6,2);
  $sentHour    = substr($sentDtTm, 8,2);
  $sentMin     = substr($sentDtTm,10,2);
  $sentSec     = substr($sentDtTm,12,2);
  $sentDtTm    = $sentYear."-".$sentMonth."-".$sentDay." ".$sentHour.":".$sentMin.":".$sentSec;

  $connect = ConnectDB();  // returns connection or "ERR|Short Msg|Errno|DB Error Description"
  if (is_string($connect))
  {
    $errparts = explode("|",$connect);
    $errmsg = $errparts[1];
  }

  if ($errmsg == "OK")
    $errmsg = getAcctDeviceByMAC($connect, $kegMAC,ACCTDEV_CPL);
  $acctDevNum = $acctDev['acctDevNum'];

  if ($errmsg == "OK" && $acctDev['acctDevNum'] == RECNUM_NONE)
  {
    $errmsg = getAcctDeviceByMAC($connect, $hubMAC,ACCTDEV_HUB);
    if ($errmsg == "OK")
    {
      if ($acctDev['acctDevNum'] == RECNUM_NONE)
        $errmsg = "HUB MAC NOT FOUND";
      else
      {
        // need to insert a coupler
        $sql = "INSERT INTO AcctDevices "
              ."(acctNum, acctDevType, acctDevMAC, "
              ." kegTypeNum, prodNum, acctDevAlarm, acctDevModified, acctDevModBy) "
              ." VALUES "
              ."(".$acctDev['acctNum'].", '".ACCTDEV_CPL."', '".$kegMAC."',"
              ."0,0,0,NOW(),-1) ";

        @mysql_query($sql, $connect) or $errmsg = "ERR|Insert Error.|".GetDatabaseError($connect,"uploadKegData","INSCOUPLER");
        if ($errmsg == "OK")
          $errmsg = getAcctDeviceByMAC($connect, $kegMAC,ACCTDEV_CPL);
        $acctDevNum = $acctDev['acctDevNum'];
      }
    }
  }

  if ($errmsg == "OK")
  {
    if ($startVal == 0 && $pourVal == 0 && $endVal == 0)
    {
      $errmsg = "COUPLER INIT COMPLETE";
    }
    else
    {
      $sql = "INSERT INTO KegDataRcvd "
            ."(hubMAC, sentDtTm, kegMAC, "
            ." startVal, pourVal, endVal, elTenths, "
            ." tempCelcius, kegStatus, rcvdDtTm) "
            ." VALUES "
            ."('".$hubMAC."', '".$sentDtTm."', '".$kegMAC."',"
            .$startVal.", ".$pourVal.", ".$endVal.", ".$elTenths.", "
            .$tempCelcius.", '".$kegStatus."',NOW()) ";

      @mysql_query($sql, $connect) or $errmsg = $sql."ERR|Insert Error.|".GetDatabaseError($connect,"uploadKegData","INSDATA");
      if ($errmsg != "OK")
      {
        $msgparts = explode("|",$errmsg);
        if ($msgparts[2] == "1062")  // skip duplicates
        {
          $errmsg = "DUP";
        }
      }
      if ($errmsg == "OK")
      {
        $sql = "INSERT INTO KegLevels "
              ."(acctDevNum, sentDtTm, kegLevelValue, kegLevelTemp, kegLevelModified, kegLevelModBy) "
              ." VALUES "
              ."(".$acctDevNum.", '".$sentDtTm."', ".$endVal.",".$tempCelcius.", NOW(),-1) ";

        @mysql_query($sql, $connect) or $errmsg = $sql."ERR|Insert Error.|".GetDatabaseError($connect,"uploadKegData","INSLEVEL");
      }
    }
  }
  CloseDB($connect);
  logData($dataIn, $errmsg);
  if ($endOfData == "KV")
  {
    $errmsg .= "<br/><div style='clear:both; float:left; text-align:left'>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>hubMAC     </label><label style='float:left;'>".formatMACfordisplay($hubMAC)."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>sentDtTm   </label><label style='float:left;'>".$sentDtTm   ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>kegMAC     </label><label style='float:left;'>".formatMACfordisplay($kegMAC)."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>startVal   </label><label style='float:left;'>".$startVal   ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>pourVal    </label><label style='float:left;'>".$pourVal    ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>endVal     </label><label style='float:left;'>".$endVal     ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>elTenths   </label><label style='float:left;'>".$elTenths   ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>tempCelcius</label><label style='float:left;'>".$tempCelcius."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>kegStatus  </label><label style='float:left;'>".$kegStatus  ."</label>";
    $errmsg .= "<label style='clear:both; float:left; width:100px;'>endOfData  </label><label style='float:left;'>".$endOfData  ."</label>";
    $errmsg .= "</div>";
  }
  echo $errmsg;
?>
