<?php

class RegisterViewTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/register');

		$this->assertEquals(200, $response->getStatusCode());
	}

}
