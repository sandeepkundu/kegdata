<?php

use App\KegData\Infrastructure\GingerBread;

class GingerBreadTest extends TestCase {

	/**
	 * Test Encrypt Method
	 *
	 * @return void
	 */
	public function testEncrypt()
	{
		$GingerBread = new GingerBread;

		$Encrypted = $GingerBread->preformEncrypt('8322573666');

		$this->assertEquals('BAVD5CCLeGv48Vv1J84SyV4USOLu3bYDlnjgZwYC0cg=', $Encrypted);
	}

	/**
	 * Test Decrypt Method
	 *
	 * @return void
	 */
	public function testDecrypt()
	{
		$GingerBread = new GingerBread;

		$Decrypted = $GingerBread->preformDecrypt('BAVD5CCLeGv48Vv1J84SyV4USOLu3bYDlnjgZwYC0cg=');

		$this->assertEquals('8322573666', $Decrypted);
	}


}
